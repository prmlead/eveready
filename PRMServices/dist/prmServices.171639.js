﻿prmApp
    .service('apmcService', ["apmcDomain", "userService", "httpServices",
        function (apmcDomain, userService, httpServices) {
            //var domain = 'http://182.18.169.32/services/';
            var apmcService = this;

            apmcService.getapmclist = function () {
                var url = apmcDomain + 'getapmclist?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            apmcService.getapmcvendorlist = function () {
                var url = apmcDomain + 'getapmcvendorlist?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            apmcService.getApmc = function (apmcid) {
                var url = apmcDomain + 'getapmc?apmcid='+ apmcid + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            apmcService.getapmcnegotiation = function (apmcnegid) {
                let url = apmcDomain + 'getapmcnegotiation?apmcnegid=' + apmcnegid + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            apmcService.getnegotiation = function (apmcnegid) {
                let url = apmcDomain + 'getnegotiation?apmcnegid=' + apmcnegid + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            }

            apmcService.getapmcaudit = function (apmcnegid, vendorid) {
                let url = apmcDomain + 'getapmcaudit?apmcnegid=' + apmcnegid + '&vendorid=' + vendorid + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            }

            apmcService.getapmcnegotiationlist = function (fromdate, todate) {
                let url = apmcDomain + 'getapmcnegotiationlist?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken() + '&fromdate=' + fromdate + '&todate=' + todate;
                return httpServices.get(url);
            }

            apmcService.getnegotiation = function (apmcnegid) {
                let url = apmcDomain + 'getnegotiation?apmcnegid=' + apmcnegid + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            }


            apmcService.getapmcvendorquantitylist = function (apmcnegid, vendorid) {
                let url = apmcDomain + 'getapmcvendorquantitylist?apmcnegid=' + apmcnegid + '&vendorid=' + vendorid + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            }


            apmcService.saveApmc = function (params) {
                let url = apmcDomain + 'saveapmc';
                return httpServices.post(url, params);
            };  

            apmcService.savevendorquantitylist = function (params) {
                let url = apmcDomain + 'saveapmcvendorquantitylist';
                return httpServices.post(url, params);
            };

            apmcService.saveapmcinput = function (params) {
                let url = apmcDomain + 'saveapmcinput';
                return httpServices.post(url, params);
            };

            apmcService.saveapmcnegotiation = function (params) {
                let url = apmcDomain + 'saveapmcnegotiation';
                return httpServices.post(url, params);
            };

            apmcService.saveapmcnegotiationlist = function (params) {
                let url = apmcDomain + 'saveapmcnegotiationlist';
                return httpServices.post(url, params);
            };

            apmcService.importentity = function (params) {
                let url = apmcDomain + 'importentity';
                return httpServices.post(url, params);
            };

            apmcService.CheckUniqueIfExists = function (params) {
                let url = apmcDomain + 'checkuniqueifexists';
                return httpServices.post(url, params);
            };
            
            return apmcService;
        }]);prmApp

    .service('catalogReportsServices', ["catalogReportsServicesDomain", "userService", "httpServices", function (catalogReportsServicesDomain, userService, httpServices) {

        var catalogReportsServices = this;
        
        catalogReportsServices.GetUserDepartments = function (params) {
            let url = catalogReportsServicesDomain + 'getuserdepartments?u_id=' + params.u_id + '&is_super_user=' + params.is_super_user + '&sessionid=' + userService.getUserToken();
             return httpServices.get(url);
        };

        catalogReportsServices.GetDepartmentCategories = function (params) {
            let url = catalogReportsServicesDomain + 'getdepartmentcategories';
            return httpServices.post(url, params);
        };

        catalogReportsServices.GetCategorySubcategories = function (params) {
            let url = catalogReportsServicesDomain + 'getcategorysubcategories';
            return httpServices.post(url, params);
        };

        catalogReportsServices.GetCategoryProducts = function (params) {
            let url = catalogReportsServicesDomain + 'getcategoryproducts';
            return httpServices.post(url, params);
        };

        catalogReportsServices.GetProductAnalysis = function (params) {
            let url = catalogReportsServicesDomain + 'getproductanalysis?u_id=' + params.u_id + '&prod_id=' + params.prod_id + '&call_code=' + params.call_code + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogReportsServices.GetVendorsAndOrders = function (params) {
            let url = catalogReportsServicesDomain + 'getvendorsandorders?u_id=' + params.u_id + '&prod_id=' + params.prod_id + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogReportsServices.GetMostOrdersAndVendors = function (params) {
            let url = catalogReportsServicesDomain + 'getmostordersandvendors?u_id=' + params.u_id + '&prod_id=' + params.prod_id + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogReportsServices.GetProductRequirements = function (params) {
            let url = catalogReportsServicesDomain + 'getproductrequirements?u_id=' + params.u_id + '&prod_id=' + params.prod_id + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogReportsServices.GetProductPriceHistory = function (params) {
            let url = catalogReportsServicesDomain + 'getproductpricehistory?u_id=' + params.u_id + '&prod_id=' + params.prod_id + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        return catalogReportsServices;
}]);prmApp

    .service('catalogService', ["catalogDomain", "userService", "httpServices", "$q", "$window", "SAPIntegrationServicesDomain", function (catalogDomain, userService, httpServices, $q, $window, SAPIntegrationServicesDomain) {
        //var domain = 'http://182.18.169.32/services/';
        var catalogService = this;
        catalogService.myCatalog = [];
        //storeService.savestore = function (params) {
        //    let url = storeDomain + 'savestore';
        //    return httpServices.post(url, params);
        //};


        catalogService.getcategories = function (compId, PageSize, NumberOfRecords) {
            let url = catalogDomain + 'getcategories?compId=' + compId + '&sessionid=' + userService.getUserToken() + '&PageSize=' + PageSize + '&NumberOfRecords=' + NumberOfRecords;
            return httpServices.get(url);
        };

        catalogService.GetMaterialProducts = function (compId) {
            let url = catalogDomain + 'getmaterialproducts?compId=' + compId + '&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.GetNonCoreProducts = function (compId) {
            let url = catalogDomain + 'getNonCoreproducts?compId=' + compId + '&userId=' + userService.getUserId() + '&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.GetMyPendingContracts = function (productName, supplierName, status, startDate, endDate,search) {
            let url = catalogDomain + 'getpendingcontracts?compId=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken()
                + '&productName=' + productName + '&supplierName=' + supplierName + '&status=' + status + '&startDate=' + startDate + '&endDate=' + endDate + '&search=' + search;
            return httpServices.get(url);
        };

        catalogService.GetProductContracts = function (productids, includeCondition) {
            if (!includeCondition)
            {
                includeCondition = 1;
            }
            let url = catalogDomain + 'getproductcontracts?productids=' + productids + '&sessionid=' + userService.getUserToken() + '&includeCondition=' + includeCondition;
            return httpServices.get(url);
        };

        catalogService.getSubCatagories = function (parentCatId, compId) {
            let url = catalogDomain + 'getSubCategories?parentCatId=' + parentCatId + '&companyId=' + compId + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.GetProductSubCategories = function (prodId, parentCatId, compId) {
            let url = catalogDomain + 'GetProductSubCategories?prodId=' + prodId+'&parentCatId=' + parentCatId + '&companyId=' + compId + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.getVendorCategories = function (vendorId, parentCatId, compId, type, PageSize, NumberOfRecords, searchString) {
            let url = catalogDomain + 'GetVendorCategories?vendorId=' + vendorId + '&parentCatId=' + parentCatId + '&companyId=' + compId + '&sessionid=' + userService.getUserToken() + '&type=' + type + '&PageSize=' + PageSize + '&NumberOfRecords=' + NumberOfRecords + '&searchString=' + searchString;
            return httpServices.get(url);
        };

        catalogService.getVendorProducts = function (vendorId, compId) {
            let url = catalogDomain + 'GetVendorProducts?vendorId=' + vendorId + '&companyId=' + compId + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.getUserMyCatalogFile = function (userid) {
            let url = catalogDomain + 'getvendormycatalogfile?userid=' + userid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.getproductVendors = function (prodId, sessionId) {
            let url = catalogDomain + 'getproductVendors?productID=' + prodId + '&sessionId=' + sessionId;
            return httpServices.get(url);
        };

        catalogService.addcategory = function (params) {
            let url = catalogDomain + 'addcategory';
            return httpServices.post(url,params);
        };

        
        catalogService.updatecategory = function (params) {
            let url = catalogDomain + 'updatecategory';
            return httpServices.post(url, params);
        };

        catalogService.deletecategory = function (params) {
            let url = catalogDomain + 'deletecategory';
            return httpServices.post(url, params);
        };
        
        catalogService.getproductbyid = function (compId,prodId) {
            let url = catalogDomain + 'getproductbyid?compId=' + compId + '&prodId=' + prodId + '&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.getProducts = function (compId, PageSize, NumberOfRecords, searchString, deactiveParams) {
            if (!deactiveParams)
            {
                deactiveParams = 1;
            }
            let url = catalogDomain + 'GetProducts?compId=' + compId + '&sessionId=' + userService.getUserToken() + '&PageSize=' + PageSize + '&NumberOfRecords=' + NumberOfRecords + '&searchString=' + searchString + '&deactiveParams=' + deactiveParams;
            return httpServices.get(url);
        };

        catalogService.getUserProducts = function (compId, userId) {
            let url = catalogDomain + 'GetUserProducts?compId=' + compId + '&userId=' + userId + '&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.getproductsbyfilter = function (params) {
            let url = catalogDomain + 'getproductsbyfilters';
            return httpServices.post(url, params);
        };

        catalogService.GetProductsByCategory = function (compId,catId) {
            let url = catalogDomain + 'GetProductsByCategory?compId=' + compId + '&catIds='+catId + '&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };
        catalogService.GetAllProductsByCategories = function (compId, catId) {
            let url = catalogDomain + 'GetAllProductsByCategories?compId=' + compId + '&catIds=' + catId + '&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.addproduct = function (params) {
            let url = catalogDomain + 'addproduct';
            return httpServices.post(url, params);
        };
        catalogService.updateProductDetails = function (params) {
            let url = catalogDomain + 'updateproduct';
            return httpServices.post(url, params);
        };
        catalogService.isProdEditAllowed = function (params) {
            let url = catalogDomain + 'isProdudtEditAllowed';
            return httpServices.post(url, params);
        };
        catalogService.deleteProduct = function (params) {
            let url = catalogDomain + 'DeleteProduct';
            return httpServices.post(url, params);
        };
        catalogService.updateProductCategories = function (params) {
            let url = catalogDomain + 'updateproductcategories';
            return httpServices.post(url, params);
        };

        catalogService.getAttributes = function (compId) {
            let url = catalogDomain + 'GetAttribute?CompanyId=' + compId + '&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.getProperties = function (compId, entityId, entityType) {
            let url = catalogDomain + 'GetProperties?CompanyId=' + compId + '&entityId=' + entityId+'&entityType=' + entityType+'&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.savecatalogproperty = function (params) {
            let url = catalogDomain + 'SaveCatalogProperty';
            return httpServices.post(url, params);
        };

        catalogService.saveEntityProperties = function (params) {
            let url = catalogDomain + 'saveEntityProperties';
            return httpServices.post(url, params);
        };

        catalogService.importentity = function (params) {
            let url = catalogDomain + 'importentity';
            return httpServices.post(url, params);
        };

        catalogService.importcataloguecategories = function (params) {
            let url = catalogDomain + 'importcataloguecategories';
            return httpServices.post(url, params);
        };

        catalogService.importvendoritemcategories = function (params) {
            let url = catalogDomain + 'importvendoritemcategories';
            return httpServices.post(url, params);
        };
        

        //catalogService.savefilters = function (params) {
        //    let url = catalogDomain + 'addfilter';
        //    return httpServices.post(url, params);
        //};

        catalogService.saveVendorCatalog = function (params) {
            let url = catalogDomain + 'saveVendorCatalog';
            return httpServices.post(url, params);
        };

        catalogService.saveVendorCatalog1 = function (params) {
            let url = catalogDomain + 'saveVendorCatalog1';
            return httpServices.post(url, params);
        };

        catalogService.GetCompanyConfiguration = function (compid, configkey, sessionid) {
            let url = catalogDomain + 'getcompanyconfiguration?compid=' + compid + '&configkey=' + configkey + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        catalogService.GetProdVendorData = function (params) {
            let url = catalogDomain + 'getprodvendordata?vendorid=' + params.vendorid + '&catitemid=' + params.catitemid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        catalogService.bulkUpdateContractStatus = function (params) {
            let url = catalogDomain + 'bulkUpdateContractStatus?status=' + params.status + '&sessionId=' + params.sessionId + '&uId=' + params.uId + '&contractIds=' + params.contractIds;
            return httpServices.get(url);
        };


        catalogService.SaveProductQuotationTemplate = function (params) {
            let url = catalogDomain + 'saveproductquotationtemplate';
            return httpServices.post(url, params);
        };

        catalogService.GetProductQuotationTemplate = function (params) {
            let url = catalogDomain + 'getproductquotationtemplate?catitemid=' + params.catitemid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        catalogService.getUserProducts = function (PageSize, NumberOfRecords, searchString,isCas,isMfcd, isnamesearch) {
            var def = $q.defer();
            catalogService.myCatalog = [];
            if (catalogService.myCatalog && catalogService.myCatalog.length > 0) {
                def.resolve(catalogService.myCatalog);
            } else {

                var params = {
                    catCompID: userService.getUserCatalogCompanyId(),
                    userId: userService.getUserId(),
                    sessionId: userService.getUserToken()
                };

                httpServices.get(catalogDomain + 'getuserproducts?compId=' + params.catCompID + '&userId=' + params.userId.trim() + ' &sessionId=' + params.sessionId +
                    '&PageSize=' + PageSize + '&NumberOfRecords=' + NumberOfRecords + '&searchString=' + searchString + '&isCas=' + isCas + '&isMfcd=' + isMfcd + '&isnamesearch=' + isnamesearch)
                    .then(function (response) {
                        def.resolve(response);
                        if (response && response.length > 0) {
                            var ListUserProducts = response;
                            catalogService.myCatalog = ListUserProducts;
                        }
                    });
            }

            return def.promise;
        };

        catalogService.GetProdDataReport = function (params) {
            let url = catalogDomain + 'getproddatareport?reportType=' + params.reportType + '&catitemid=' + params.catitemid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        catalogService.GetMaterialDetailsFromSAP = function (params) {
            let url = SAPIntegrationServicesDomain + 'getmaterialfromsap?materialcode=' + params.productCode + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        catalogService.getContractUtilisations = function (params) {
            let url = catalogDomain + 'getContractUtilisations?compId=' + params.compId + '&userId=' + params.userId + '&sessionid=' + params.sessionid + '&contracts=' + params.contracts + '&prs=' + params.prs + '&products=' + params.products + '&vendors=' + params.vendors + '&pos=' + params.pos + '&status=' + params.status + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate + '&pcID=' + params.pcID + '&PageSize=' + params.PageSize + '&NumberOfRecords=' + params.NumberOfRecords + '&searchString=' + params.searchString;
            return httpServices.get(url);
        };

        catalogService.getContractUtilisationsFilters = function (params) {
            let url = catalogDomain + 'getContractUtilisationsFilters?compId=' + params.compId + '&userId=' + params.userId + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        catalogService.SaveProductContract = function (params) {
            let url = catalogDomain + 'saveproductcontact';
            return httpServices.post(url, params);
        };

        catalogService.UploadContractsFromExcel = function (params) {
            var url = catalogDomain + 'uploadcontractsfromexcel';
            return httpServices.post(url, params);
        };
        catalogService.getItemDetailsforContract = function (params) {
            let url = catalogDomain + 'getItemDetailsforContract?REQ_ID=' + params.REQ_ID + '&QCS_ID=' + params.QCS_ID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        catalogService.downloadContractTemplate = function (productid, vendorid, compid, includedata) {
            if (!compid) {
                compid = 0;
            }

            if (!vendorid) {
                vendorid = 0;
            }

            if (!includedata) {
                includedata = false;
            }

            let url = catalogDomain + 'getcontracttemplate?productid=' + productid + '&vendorid=' + vendorid + '&compid=' + compid + '&includedata=' + includedata + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", "Contract.xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });

                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }
        
        return catalogService;
    }]);prmApp

    .service('cijIndentService', ["cijindentDomain", "userService", "httpServices", function (cijindentDomain, userService, httpServices) {
        //var domain = 'http://182.18.169.32/services/';
        var cijIndentService = this;
        
        //storeService.savestore = function (params) {
        //    let url = storeDomain + 'savestore';
        //    return httpServices.post(url, params);
        //};

        cijIndentService.EditCijIndentDetails = function (params) {

            var url = cijindentDomain + 'editcijindentdetails';
            return httpServices.post(url, params);
        };
        cijIndentService.GetCIJList = function (userid, sessionid) {

            var url = cijindentDomain + 'getcijlist?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        cijIndentService.deleteCIJ = function (params) {
            var url = cijindentDomain + 'deletecij';
            return httpServices.post(url, params);
        };

        cijIndentService.saveCIJ = function (params) {
            var url = cijindentDomain + 'savecij';
            return httpServices.post(url, params);
        };
        cijIndentService.GetCIJ = function (cijid, sessionid) {

            var url = cijindentDomain + 'getcij?cijid=' + cijid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        cijIndentService.GetCijItems = function (id, sessionid) {

            var url = cijindentDomain + 'getcijitems?id=' + id + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        cijIndentService.GetIndentList = function (userid, sessionid) {

            var url = cijindentDomain + 'getindentlist?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        cijIndentService.deleteIndent = function (params) {
            var url = cijindentDomain + 'deleteindent';
            return httpServices.post(url, params);
        };

        cijIndentService.SaveIndentDetails = function (params) {
            var url = cijindentDomain + 'saveindentdetails';
            return httpServices.post(url, params);
        };

        cijIndentService.saveFileUpload = function (params) {
            var url = cijindentDomain + 'savefileupload';
            return httpServices.post(url, params);
        };


        cijIndentService.GetSeries = function (series, seriestype, deptid) {

            var url = cijindentDomain + 'getseries?series=' + series + '&seriestype=' + seriestype + '&compid=' + userService.getUserCompanyId() + '&deptid=' + deptid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        cijIndentService.GetItemsList = function (moduleid, module, sessionid) {
           
                var url = cijindentDomain + 'getitemslist?moduleid=' + moduleid + '&module=' + module + '&sessionid=' + sessionid;
                return httpServices.get(url);
        };

        cijIndentService.GetIndentDetails = function (indentID, sessionid) {
           
                var url = cijindentDomain + 'getindentdetails?indentid=' + indentID + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };
        
        return cijIndentService;
    }]);﻿prmApp
    // =========================================================================
    // Auction Details Services
    // =========================================================================
    .service('fwdauctionsService', ["$http", "fwddomain", "httpServices", function ($http, fwddomain, httpServices) {
        //var domain = 'http://182.18.169.32/services/';
        var fwdauctions = this;
        fwdauctions.getdate = function () {
            var date = new Date();
            var time = date.getTime();
            return $http({
                method: 'GET',
                url: fwddomain + 'getdate?time=' + time,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
                //data: params
            }).then(function (response) {
                var list = [];
                if (response && response.data) {
                    if (response.data.length > 0) {
                        list = response.data;
                    }
                    return list;
                } else {
                    //console.log(response.data[0].errorMessage);
                }
            }, function (result) {
                //console.log("date error");
            });
        }

        fwdauctions.isnegotiationended = function (reqid, sessionid) {
            let url = fwddomain + 'isnegotiationended?reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.selectVendor = function (params) {
            var url = fwddomain + 'selectvendor';
            return httpServices.post(url, params);
        };

        fwdauctions.getauctions = function (params) {
            let url = fwddomain + 'getrunningauctions?section=' + params.section + '&userid=' + params.userid + '&sessionid=' + params.sessionid + '&limit=' + params.limit;
            return httpServices.get(url);
        };

        fwdauctions.rateVendor = function (params) {
            var url = fwddomain + 'userratings';
            return httpServices.post(url, params);
        };

        fwdauctions.updateStatus = function (params) {
            var url = fwddomain + 'updatestatus';
            return httpServices.post(url, params);
        };

        fwdauctions.uploadquotationsfromexcel = function (params) {
            var url = fwddomain + 'uploadquotationsfromexcel';
            return httpServices.post(url, params);
        };

        fwdauctions.itemwiseselectvendor = function (params) {
            var url = fwddomain + 'itemwiseselectvendor';
            return httpServices.post(url, params);
        };


        fwdauctions.updatedeliverdate = function (params) {
            var url = fwddomain + 'updateexpdelandpaydate';
            return httpServices.post(url, params);
        };

        fwdauctions.reqTechSupport = function (params) {
            var url = fwddomain + 'reqtechsupport';
            return httpServices.post(url, params);
        };

        fwdauctions.SaveRunningItemPrice = function (params) {
            var url = fwddomain + 'saverunningitemprice';
            return httpServices.post(url, params);
        };

        fwdauctions.updatepaymentdate = function (params) {
            var url = fwddomain + 'updateexpdelandpaydate';
            return httpServices.post(url, params);
        };


        fwdauctions.StartNegotiation = function (params) {
            var url = fwddomain + 'startNegotiation';
            return httpServices.post(url, params);
        };

        fwdauctions.RestartNegotiation = function (params) {
            var url = fwddomain + 'restartnegotiation';
            return httpServices.post(url, params);
        };

        fwdauctions.DeleteVendorFromAuction = function (params) {
            var url = fwddomain + 'deletevendorfromauction';
            return httpServices.post(url, params);
        };

        fwdauctions.generatepo = function (params) {
            var data = {
                reqPO: params
            };
            var url = fwddomain + 'generatepoforuser';
            return httpServices.post(url, data);
        };

        fwdauctions.vendorreminders = function (params) {
            var url = fwddomain + 'vendorreminders';
            return httpServices.post(url, params);
        };

        fwdauctions.SaveCompanyDepartment = function (params) {
            var url = fwddomain + 'savecompanydepartment';
            return httpServices.post(url, params);
        };

        fwdauctions.SaveCompanyDesignations = function (params) {
            var url = fwddomain + 'savecompanydesignations';
            return httpServices.post(url, params);
        };

        fwdauctions.DeleteDepartment = function (params) {
            var url = fwddomain + 'deletedepartment';
            return httpServices.post(url, params);
        };

        fwdauctions.DeleteDesignation = function (params) {
            var url = fwddomain + 'deletedesignation';
            return httpServices.post(url, params);
        };

        fwdauctions.GetCompanyDepartments = function (userid, sessionid) {
            var url = fwddomain + 'getcompanydepartments?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetCompanyDesignations = function (userid, sessionid) {
            var url = fwddomain + 'getcompanydesignations?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetUserDepartments = function (userid, sessionid) {
            var url = fwddomain + 'getuserdepartments?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetUserDesignations = function (userid, sessionid) {
            var url = fwddomain + 'getuserdesignations?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };


        fwdauctions.GetUserDeptDesig = function (userid, sessionid) {
            var url = fwddomain + 'getuserdeptdesig?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetCompDeptDesig = function (compid, sessionid) {
            var url = fwddomain + 'getcompdeptdesig?compid=' + compid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.SaveUserDeptDesig = function (params) {
            var url = fwddomain + 'saveuserdeptdesig';
            return httpServices.post(url, params);
        };

        fwdauctions.GetReqDeptDesig = function (userid, reqid, sessionid) {
            var url = fwddomain + 'getreqdeptdesig?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.SaveReqDeptDesig = function (params) {
            var url = fwddomain + 'savereqdeptdesig';
            return httpServices.post(url, params);
        };

        fwdauctions.SaveUserDepartments = function (params) {
            var url = fwddomain + 'saveuserdepartments';
            return httpServices.post(url, params);
        };

        fwdauctions.GetDepartmentUsers = function (deptid, userid, sessionid) {
            var url = fwddomain + 'getdepartmentusers?deptid=' + deptid + '&userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetReqDepartments = function (userid, reqid, sessionid) {
            let url = fwddomain + 'getreqdepartments?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.SaveReqDepartments = function (params) {
            let url = fwddomain + 'savereqdepartments';
            return httpServices.post(url, params);
        };

        fwdauctions.AssignVendorToCompany = function (params) {
            let url = fwddomain + 'assignvendortocompany';
            return httpServices.post(url, params);
        };

        fwdauctions.GetCompanyConfiguration = function (compid, configkey, sessionid) {
            let url = fwddomain + 'getcompanyconfiguration?compid=' + compid + '&configkey=' + configkey + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetIsAuthorized = function (userid, reqid, sessionid) {
            let url = fwddomain + 'getisauthorized?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.materialdispatch = function (params) {
            var data = {
                reqMat: params
            };
            let url = fwddomain + 'materialdispatch';
            return httpServices.post(url, data);
        };

        fwdauctions.materialReceived = function (params) {
            var data = {
                materialreceived: params
            };
            let url = fwddomain + 'materialreceived';
            return httpServices.post(url, data);
        };

        fwdauctions.paymentdetails = function (params) {
            var data = {
                paymentDets: params
            };
            let url = fwddomain + 'paymentdetails';
            return httpServices.post(url, data);
        };

        fwdauctions.generatePOinServer = function (params) {
            let url = fwddomain + 'generatepo';
            return httpServices.post(url, params);
        };

        fwdauctions.getDashboardStats = function (params) {
            let url = fwddomain + 'getdashboardstats?userid=' + params.userid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.getCategories = function (params) {
            let url = fwddomain + 'getcategories?userid=' + (params ? params : -1);
            return httpServices.get(url);
        };

        fwdauctions.getKeyValuePairs = function (params) {
            let url = fwddomain + 'getkeyvaluepairs?parameter=' + params;
            return httpServices.get(url);
        };

        fwdauctions.getmyAuctions = function (params) {
            let url = fwddomain + 'getmyauctions?userid=' + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.getactiveleads = function (params) {
            let url = fwddomain + 'getactiveleads?userid=' + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.getrequirementdata = function (params) {
            let url = fwddomain + 'getrequirementdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        fwdauctions.GetReportsRequirement = function (params) {
            let url = fwddomain + 'getreportsrequirement?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetVendorReminders = function (params) {
            let url = fwddomain + 'getvendorreminders?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetBidHistory = function (params) {
            let url = fwddomain + 'getbidhistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        fwdauctions.getpodetails = function (params) {
            let url = fwddomain + 'getpodetails?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        fwdauctions.getcomments = function (params) {
            let url = fwddomain + 'getcomments?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.getpricecomparison = function (params) {
            let url = fwddomain + 'getpricecomparison?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetPriceComparisonPreNegotiation = function (params) {
            let url = fwddomain + 'getpricecomparisonprenegotiation?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.savecomment = function (params) {
            var comment = {
                "com": {
                    "requirementID": params.reqID,
                    "firstName": "",
                    "lastName": "",
                    "userID": params.userID,
                    "commentText": params.commentText,
                    "replyCommentID": -1,
                    "commentID": -1,
                    "errorMessage": "",
                    "sessionID": params.sessionID
                }
            };

            let url = fwddomain + 'savecomment';
            return httpServices.post(url, comment);
        };


        fwdauctions.deleteAttachment = function (Attaachmentparams) {
            let url = fwddomain + 'deleteattachment';
            return httpServices.post(url, Attaachmentparams);
        };


        fwdauctions.postrequirementdata = function (params) {
            var myDate = new Date(); // Your timezone!
            var myEpoch = parseInt(myDate.getTime() / 1000);
            params.minBidAmount = 0;
            params.postedOn = "/Date(" + myEpoch + "000+0000)/";
            params.timeLeft = -1;
            params.price = -1;
            var requirement = {
                "requirement": {
                    "title": params.title,
                    "description": params.description,
                    "category": params.category,
                    "subcategories": params.subcategories,
                    "urgency": params.urgency,
                    "budget": params.budget,
                    "attachmentName": params.attachmentName,
                    "deliveryLocation": params.deliveryLocation,
                    "taxes": params.taxes,
                    "paymentTerms": params.paymentTerms,
                    "requirementID": params.requirementID,
                    "customerID": params.customerID,
                    "isClosed": params.isClosed,
                    "endTime": null,
                    "sessionID": params.sessionID,
                    "errorMessage": "",
                    "timeLeft": -1,
                    "price": -1,
                    "auctionVendors": params.auctionVendors,
                    "startTime": null,
                    "status": "",
                    "postedOn": "/Date(" + myEpoch + "000+0000)/",
                    "custLastName": params.customerLastname,
                    "custFirstName": params.customerFirstname,
                    "deliveryTime": params.deliveryTime,
                    "includeFreight": params.includeFreight,
                    "inclusiveTax": params.inclusiveTax,
                    "minBidAmount": 0,
                    "checkBoxEmail": params.checkBoxEmail,
                    "checkBoxSms": params.checkBoxSms,
                    "quotationFreezTime": params.quotationFreezTime,
                    "currency": params.currency,
                    "timeZoneID": params.timeZoneID,
                    "listRequirementItems": params.listRequirementItems,
                    "isTabular": params.isTabular,
                    "reqComments": params.reqComments,
                    "itemsAttachment": params.itemsAttachment,
                    "isSubmit": params.isSubmit,
                    "isQuotationPriceLimit": params.isQuotationPriceLimit,
                    "quotationPriceLimit": params.quotationPriceLimit,
                    "noOfQuotationReminders": params.noOfQuotationReminders,
                    "remindersTimeInterval": params.remindersTimeInterval,
                    "custCompID": params.custCompID,
                    "customerCompanyName": params.customerCompanyName,
                    "deleteQuotations": params.deleteQuotations,
                    "indentID": params.indentID,
                    "expStartTime": params.expStartTime
                }, "attachment": params.attachment
            };
            let url = fwddomain + 'requirementsave';
            return httpServices.post(url, requirement);
        };

        fwdauctions.updatebidtime = function (params) {
            let url = fwddomain + 'updatebidtime';
            return httpServices.post(url, params);
        };

        fwdauctions.makeabid = function (params) {
            let url = fwddomain + 'makeabid';
            return httpServices.post(url, params);
        };

        fwdauctions.uploadQuotation = function (params) {
            let url = fwddomain + 'uploadquotation';
            return httpServices.post(url, params);
        };

        fwdauctions.revquotationupload = function (params) {
            let url = fwddomain + 'revquotationupload';
            return httpServices.post(url, params);
        };

        fwdauctions.QuatationAprovel = function (params) {
            let url = fwddomain + 'quatationaprovel';
            return httpServices.post(url, params);
        };

        fwdauctions.savepricecap = function (params) {
            let url = fwddomain + 'savepricecap';
            return httpServices.post(url, params);
        };

        fwdauctions.GetCompanyLeads = function (params) {
            let url = fwddomain + 'getcompanyleads?userid=' + params.userid + '&searchstring=' + params.searchstring + '&searchtype=' + params.searchtype + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.updateauctionstart = function (params) {
            let url = fwddomain + 'updateauctionstart';
            return httpServices.post(url, params);
        };

        fwdauctions.GetMaterialReceivedData = function (params) {
            let url = fwddomain + 'getmaterialreceiveddata?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        fwdauctions.SaveCompanyCategories = function (params) {
            let url = fwddomain + 'savecompanycategories';
            return httpServices.post(url, params);
        };

        fwdauctions.GetCompanyCategories = function (compID, sessionID) {
            let url = fwddomain + 'getcompanycategories?compid=' + compID + '&sessionid=' + sessionID;
            return httpServices.get(url);
        };

        fwdauctions.newDetails = function (params) {
            let url = fwddomain + 'savealternatecontacts';
            return httpServices.post(url, params);
        };

        fwdauctions.getalternatecontacts = function (userid, compid, sessionID) {
            let url = fwddomain + 'getalternatecontacts?userid=' + userid + '&compid=' + compid + '&sessionid=' + sessionID;
            return httpServices.get(url);
        };

        fwdauctions.SaveRequirementTerms = function (params) {
            let url = fwddomain + 'saverequirementterms';
            return httpServices.post(url, params);
        };

        fwdauctions.GetRequirementTerms = function (userid, reqid, sessionID) {
            let url = fwddomain + 'getrequirementterms?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionID;
            return httpServices.get(url);
        };

        fwdauctions.DeleteRequirementTerms = function (params) {
            let url = fwddomain + 'deleterequirementterms';
            return httpServices.post(url, params);
        };

        fwdauctions.saveCIJ = function (params) {
            let url = fwddomain + 'savecij';
            return httpServices.post(url, params);
        };

        fwdauctions.GetCIJ = function (cijid, sessionid) {
            let url = fwddomain + 'getcij?cijid=' + cijid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetIndentDetails = function (indentID, sessionid) {
            let url = fwddomain + 'getindentdetails?indentid=' + indentID + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.SaveIndentDetails = function (params) {
            let url = fwddomain + 'saveindentdetails';
            return httpServices.post(url, params);
        };

        fwdauctions.GetCIJList = function (compid, sessionid) {
            let url = fwddomain + 'getcijlist?compid=' + compid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.GetIndentList = function (compid, sessionid) {
            let url = fwddomain + 'getindentlist?compid=' + compid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        fwdauctions.saveFileUpload = function (params) {
            let url = fwddomain + 'savefileupload';
            return httpServices.post(url, params);
        };

        fwdauctions.GetUserDetails = function (userid, sessionid) {
            let url = fwddomain + 'getuserinfo?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        return fwdauctions;
    }]);prmApp
    .service('httpServices', ["$http", "$log", "store", function ($http, $log, store) {
        var httpServices = this;

        var _header = {
            'Content-Type': 'application/json',
        }

        if (store.get('currentUser') != undefined) {
            _header.cid = store.get('currentUser').companyId ;
            _header.uid = store.get('currentUser').userID;
        } else
        {
            _header.cid = 349;
        }


        if (store.get('sessionid') != undefined)
            _header.token = store.get('sessionid')





        httpServices.get = function (URL) {
            if (URL.indexOf('?') < 0) {
                URL = URL + '?'
            }
            var d = new Date();
            var timeToken = String(d.getFullYear()) + d.getMonth() + d.getDate() + d.getHours() + d.getMinutes() + d.getMilliseconds();



            return $http({
                method: 'GET',
                url: URL + '&token=' + timeToken,
                encodeURI: true,
                headers: _header
            }).then(function (response) {
                return response.data;
            });
        };

        httpServices.post = function (URL, params) {
            return $http({
                method: 'POST',
                url: URL,
                encodeURI: true,
                headers: _header,
                data: params
            }).then(function (response) {
                return response.data;
            });
        };

        return httpServices;
    }]);prmApp
    .service('itemTableServices', ["domain", "userService", "httpServices",
        function (domain, userService, httpServices) {
            var itemTableService = this;
            var customerTable = {
                unconfirmed: {
                    companyName: "Company Name"
                }
            }

            var vendorDiscountBiddingTable = {
                "unconfirmed": {
                    "productNameOrID": {
                        "displayName": "Product Name",
                        "type": "string",
                        "disabled": false
                    },
                    "productNo": {
                        "displayName": "Product No.",
                        "type": "string",
                        "disabled": false
                    },
                    "productBrand": {
                        "displayName": "Brand",
                        "type": "string",
                        "disabled": "auctionItem.auctionVendors[0].isQuotationRejected == 0"
                    },
                    "productQuantity": {
                        "displayName": "Quantity",
                        "type": "number",
                        "disabled": true
                    },
                    "productQuantityIn": {
                        "displayName": "Units",
                        "type": "string",
                        "disabled": true
                    },
                    "Gst": {
                        "displayName": "GST",
                        "type": "percentage",
                        "disabled": "auctionItem.auctionVendors[0].isQuotationRejected == 0"
                    },
                    "unitPrice": {
                        "displayName": "Unit Price",
                        "type": "currency",
                        "disabled": "auctionItem.auctionVendors[0].isQuotationRejected == 0"
                    },
                    "unitDiscount": {
                        "displayName": "Discount (%)",
                        "type": "percentage",
                        "disabled": "auctionItem.auctionVendors[0].isQuotationRejected == 0"
                    },
                    "totalItemPrice": {
                        "displayName": "Total Item Price",
                        "type": "currency",
                        "disabled": true
                    },
                    "revUnitDiscount": {
                        "displayName": "Rev. Discount (%)",
                        "type": "percentage",
                        "disabled": true
                    },
                    "revItemPrice": {
                        "displayName": "Revised Item Price",
                        "type": "currency",
                        "disabled": true
                    }
                }
            }

            var vendorMarginBiddingTable = {
                unconfirmed: {
                    "productNameOrID": "Product Name",
                    "productNo": "HSN Code",
                    "productBrand": "Brand",
                    "productQuantity": "Quantity",
                    "costPrice": "Cost Price",
                    "Gst": "GST",
                    "unitMRP": "MRP",
                    "netPrice": "Net Price",
                    "marginAmount": "Margin Amount",
                    "unitDiscount": "Margin (%)",
                    "totalItemPrice": "Total Item Price",
                    "revUnitDiscount": "Rev. Discount (%)",
                    "revItemPrice": "Revised Item Price"
                }
            }

            itemTableService.getTableColumns = function (status, biddingType) {
                if (userService.getUserType() == "CUSTOMER") {
                    return customerTable[status];
                } else {
                    if (biddingType == 1) {
                        return vendorDiscountBiddingTable[status];
                    } else if (biddingType == 2) {
                        return vendorMarginBiddingTable[status];
                    } else if (biddingType == 0) {
                        return vendorNonUnitBiddingTable[status];
                    }

                }
            };

            return itemTableService;
        }]);prmApp
    .service('logisticServices', ["logisticDomain", "userService", "httpServices",
        function (logisticDomain, userService, httpServices) {
            var logisticServices = this;

            logisticServices.GetRequirementData = function (params) {
                let url = logisticDomain + 'getrequirementdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
                return httpServices.get(url);
            };

            logisticServices.getactiveleads = function (params) {
                let url = logisticDomain + 'getactiveleads?userid=' + params.userid + "&sessionid=" + params.sessionid;
                return httpServices.get(url);
            };

            logisticServices.getmyAuctions = function (params) {
                let url = logisticDomain + 'getmyauctions?userid=' + params.userid + "&sessionid=" + params.sessionid;
                return httpServices.get(url);
            };

            logisticServices.GetBidHistory = function (params) {
                let url = logisticDomain + 'getbidhistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
                return httpServices.get(url);
            };

            logisticServices.GetVendorReminders = function (params) {
                let url = logisticDomain + 'getvendorreminders?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid;
                return httpServices.get(url);
            };

            logisticServices.getPreviousItemPrice = function (itemDetails) {
                let url = logisticDomain + 'itempreviousprice?companyid=' + itemDetails.compID + '&productIDorName=' + itemDetails.productIDorName.trim() + '&netWeight=' + itemDetails.netWeight + '&natureOfGoods=' + itemDetails.natureOfGoods + '&finalDestination=' + itemDetails.finalDestination + '&sessionid=' + itemDetails.sessionID;
                return httpServices.get(url);
            };

            //logisticServices.getPreviousItemPriceReport = function (itemDetails) {
            //    let url = logisticDomain + 'itempreviouspricereport?companyid=' + itemDetails.compID + '&productId=' + itemDetails.productId.trim() + '&netWeight=' + itemDetails.netWeight + '&natureOfGoods=' + itemDetails.natureOfGoods + '&finalDestination=' + itemDetails.finalDestination + '&sessionid=' + itemDetails.sessionID;
            //    return httpServices.get(url);
            //};




            logisticServices.signalRFrontEnd = function (params) {
                let url = logisticDomain + 'signalrfrontend';
                return httpServices.post(url, params);
            };
            
            logisticServices.RequirementSave = function (params) {
                var url = logisticDomain + 'requirementsave';
                return httpServices.post(url, params);
            };

            logisticServices.SubmitQuotation = function (params) {
                var url = logisticDomain + 'submitquotation';
                return httpServices.post(url, params);
            };

            logisticServices.SaveAirlines = function (params) {
                var url = logisticDomain + 'saveairlines';
                return httpServices.post(url, params);
            };

            logisticServices.RejectQuotation = function (params) {
                var url = logisticDomain + 'rejectquotation';
                return httpServices.post(url, params);
            };

            logisticServices.updateauctionstart = function (params) {
                let url = logisticDomain + 'updateauctionstart';
                return httpServices.post(url, params);
            };

            logisticServices.updateauctionstart = function (params) {
                let url = logisticDomain + 'updateauctionstart';
                return httpServices.post(url, params);
            };

            logisticServices.StartNegotiation = function (params) {
                let url = logisticDomain + 'startnegotiation';
                return httpServices.post(url, params);
            };

            logisticServices.UpdateBidTime = function (params) {
                let url = logisticDomain + 'updatebidtime';
                return httpServices.post(url, params);
            };

            logisticServices.StopBids = function (params) {
                let url = logisticDomain + 'stopbids';
                return httpServices.post(url, params);
            };

            logisticServices.RestartNegotiation = function (params) {
                let url = logisticDomain + 'restartnegotiation';
                return httpServices.post(url, params);
            };

            logisticServices.MakeBid = function (params) {
                let url = logisticDomain + 'makebid';
                return httpServices.post(url, params);
            };

            logisticServices.savepricecap = function (params) {
                let url = logisticDomain + 'savepricecap';
                return httpServices.post(url, params);
            };

            logisticServices.UpdatePriceCap = function (params) {
                let url = logisticDomain + 'updatepricecap';
                return httpServices.post(url, params);
            };

            logisticServices.vendorreminders = function (params) {
                var url = logisticDomain + 'vendorreminders';
                return httpServices.post(url, params);
            };
            
            return logisticServices;

        }]);﻿prmApp

    .service('opsServices', ["opsDomain", "userService", "httpServices", "$window", function (opsDomain, userService, httpServices,$window) {
        
        var opsServices = this;

       

        opsServices.GetClients = function (from,to) {
            let url = opsDomain + 'GetClients?from=' + from + '&to=' + to + '&sessionId=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetClientDetails = function (domain) {
            let url = opsDomain + 'GetClientDetails?domain=' + domain + '&sessionID=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetClientsRequirements = function (clientid, domain,from,to,auctionType) {
            let url = opsDomain + 'GetClientsRequirements?clientid=' + clientid + '&domain=' + domain + '&from=' + from + ' &to=' + to + '&auctionType=' + auctionType + '&sessionid=' + userService.getUserToken() ;
            return httpServices.get(url);
        };

        opsServices.GetClientsVendors = function (reqid, domain, clientid, auctionType) {
            let url = opsDomain + 'GetClientsVendors?reqid=' + reqid + '&domain=' + domain + '&clientid=' + clientid + '&auctionType=' + auctionType + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetInfo = function (reqid, type, domain, from, to) {
            let url = opsDomain + 'GetInfo?reqid=' + reqid + '&type=' + type + '&domain=' + domain + '&from=' + from + ' &to=' + to;
            return httpServices.get(url);
        };

        opsServices.GetVendorsOnline = function (reqid, userid, domain, cldomain, auctionType) {
            let url = opsDomain + 'GetVendorsOnline?reqid=' + reqid + '&userid=' + userid + '&domain=' + domain + '&auctionType=' + auctionType + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetPendingQuotations = function (reqid, clientid, domain, auctionType) {
            let url = opsDomain + 'GetPendingQuotations?reqid=' + reqid + '&clientid=' + clientid + '&domain=' + domain + '&auctionType=' + auctionType + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetAllQuotationsVendors = function (reqid, clientid, domain, auctionType) {
            let url = opsDomain + 'GetAllQuotationsVendors?reqid=' + reqid + '&clientid=' + clientid + '&domain=' + domain + '&auctionType=' + auctionType + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        

        opsServices.GetReminders = function (reqid, clientid, domain) {
            let url = opsDomain + 'GetReminders?reqid=' + reqid + '&clientid=' + clientid + '&domain=' + domain + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
            
        };

        opsServices.GetVendorDetails = function (vendorID, domain) {
            let url = opsDomain + 'GetVendorDetails?vendorID=' + vendorID + '&domain=' + domain + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);

        };

        //opsServices.GetReportsTemplates = function (name, domain, from, to) {
        //    let url = opsDomain + 'GetReportsTemplates?name=' + name + '&domain=' + domain + '&from=' + from + ' &to=' + to + '&sessionID=' + userService.getUserToken();
        //    return httpServices.get(url);

        //};

        opsServices.GetReportsTemplates = function (name, domain, from, to) {
            //let url = reportingDomain + 'getconsolidatedtemplates?template=' + template + '&from=' + from + '&to=' + to + '&userid=' + userid + '&userType=' + userType + '&sessionid=' + userService.getUserToken();
            let url = opsDomain + 'GetReportsTemplates?name=' + name + '&domain=' + domain + '&from=' + from + ' &to=' + to + '&sessionID=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", name + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }


        opsServices.GetDomain = function (params) {
            let url = opsDomain + 'getDomain?domain=' + params.domain;
            return httpServices.get(url);

        };

        opsServices.SendOTP = function (params) {
            let url = opsDomain + 'SendOTP';
            return httpServices.post(url, params);

        };

        opsServices.updatePassword = function (params) {
            let url = opsDomain + 'changepassword';
            return httpServices.post(url, params);

        };

        opsServices.SendPassword = function (params) {
            let url = opsDomain + 'SendPassword';
            return httpServices.post(url, params);

        };

        opsServices.GetVendorNeedTraining = function (clientid, domain, from, to, auctionType) {
            let url = opsDomain + 'GetVendorNeedTraining?clientid=' + clientid + '&domain=' + domain + '&from=' + from + ' &to=' + to + '&auctionType=' + auctionType + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetReports = function (domain, from, to) {
            let url = opsDomain + 'GetReports?domain=' + domain + '&from=' + from + ' &to=' + to + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.SaveVendorTraining = function (params) {
            let url = opsDomain + 'SaveVendorTraining';
            return httpServices.post(url, params);

        };

        opsServices.GetVendorTrainingHistory = function (vtID, reqID, vendorID, domain) {
            let url = opsDomain + 'GetVendorTrainingHistory?vtID=' + vtID + '&reqID=' + reqID + ' &vendorID=' + vendorID + '&domain=' + domain;
            return httpServices.get(url);
        };

        opsServices.GetALLVendorTrainingDetails = function (domain, from, to) {
            let url = opsDomain + 'GetALLVendorTrainingDetails?sessionID=' + userService.getUserToken() + '&domain=' + domain + '&from=' + from + ' &to=' + to;
            return httpServices.get(url);
        };


        opsServices.GetRegularRfqReport = function (domain, from, to) {
            let url = opsDomain + 'GetRegularRfqReport?domain=' + domain + '&from=' + from + '&to=' + to + '&sessionID=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetLogisticsRfqReport = function (domain, from, to) {
            let url = opsDomain + 'GetLogisticsRfqReport?domain=' + domain + '&from=' + from + '&to=' + to + '&sessionID=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetSubUserDataReport = function (domain, from, to) {
            let url = opsDomain + 'GetSubUserDataReport?domain=' + domain + '&from=' + from + '&to=' + to + '&sessionID=' + userService.getUserToken();
            return httpServices.get(url);
        };

        opsServices.GetVendorRegisteredReport = function (domain, from, to) {
            let url = opsDomain + 'GetVendorRegisteredReport?domain=' + domain + '&from=' + from + '&to=' + to + '&sessionID=' + userService.getUserToken();
            return httpServices.get(url);
        };

        return opsServices;
    }]);prmApp
    .service('poService', ["poDomain", "userService", "httpServices", function (poDomain, userService, httpServices) {
        //var domain = 'http://182.18.169.32/services/';
        var poService = this;

        poService.getpoinformation = function (reqID) {
            let url = poDomain + 'getpoinformation?userid=' + userService.getUserId() + '&reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.savepoinfo = function (params) {
            let url = poDomain + 'savepoinfo';
            return httpServices.post(url, params);
        };

        poService.getdespoinfo = function (reqID) {
            let url = poDomain + 'getdespoinfo?userid=' + userService.getUserId() + '&reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.getAssignedPO = function () {
            let url = poDomain + 'getAssignedPO?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.getDeliverySchedules = function (poNumbers) {
            let url = poDomain + 'getDeliverySchedules?poNumbers=' + poNumbers + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.GetMyPendingPOS = function (isVendor) {
            let url = poDomain + 'getCompanyPendingPOS?compId=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken() + '&isVendor=' + isVendor + '&userid=' + userService.getUserId();
            return httpServices.get(url);
        };

        poService.GetPendingPayments = function () {
            let url = poDomain + 'getpendingpayments?compId=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.GetFiltersOnLoadData = function () {
            let url = poDomain + 'GetFiltersOnLoadData?compId=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };
        
        poService.GetVendors = function (reqID) {
            let url = poDomain + 'getvendors?&reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };


        poService.GetFiltersOnLoadData = function () {
            let url = poDomain + 'GetFiltersOnLoadData?compId=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.saveDescPoInfo = function (params) {
            let url = poDomain + 'savedescpoinfo';
            return httpServices.post(url, params);
        };

        poService.UpdatePOStatus = function (params) {
            let url = poDomain + 'updatepostatus';
            return httpServices.post(url, params);
        };

        poService.GetDescDispatch = function (poid, dtid) {
            let url = poDomain + 'getdescdispatch?poid=' + poid + '&dtid=' + dtid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.SaveDesDispatchTrack = function (params) {
            let url = poDomain + 'savedesdispatchtrack';
            return httpServices.post(url, params);
        };

        poService.GetDispatchTrackList = function (poorderid, dCode) {
            let url = poDomain + 'getdispatchtracklist?poorderid=' + poorderid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.getDispatchTrack = function (poorderid, dCode) {
            let url = poDomain + 'getdispatchtrack?poorderid=' + poorderid + '&dcode=' + dCode + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.SaveDispatchTrack = function (params) {
            let url = poDomain + 'savedispatchtrack';
            return httpServices.post(url, params);
        };

        poService.GetVendorPoList = function (reqID, vendorID, poid) {
            let url = poDomain + 'getvendorpolist?reqid=' + reqID + '&userid=' + vendorID + '&poid=' + poid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.GetVendorPoInfo = function (reqID, vendorID, poid) {
            let url = poDomain + 'getvendorpoinfo?reqid=' + reqID + '&userid=' + vendorID + '&poid=' + poid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.SaveVendorPOInfo = function (params) {
            let url = poDomain + 'savevendorpoinfo';
            return httpServices.post(url, params);
        };

        poService.GetPaymentTrack = function (vendorID, poID) {
            let url = poDomain + 'getPaymentTrack?&vendorid=' + vendorID + '&poid=' + poID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.SavePaymentInfo = function (params) {
            let url = poDomain + 'savepaymentinfo';
            return httpServices.post(url, params);
        };

        poService.CheckUniqueIfExists = function (params) {
            let url = poDomain + 'checkuniqueifexists';
            return httpServices.post(url, params);
        };

        poService.getPOSchedule = function (params) {
            let url = poDomain + 'getposchedule?ponumber=' + params.ponumber + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.SavePOSchedule = function (params) {
            let url = poDomain + 'saveposchedule';
            return httpServices.post(url, params);
        };

        poService.SaveVendorAck = function (params) {
            let url = poDomain + 'savepovendorack';
            return httpServices.post(url, params);
        };

        poService.SavePOVendorQuantityAck = function (params) {
            let url = poDomain + 'savepovendorquantityack';
            return httpServices.post(url, params);
        };

        return poService;
    }]);prmApp
    .service('priceCapServices', ["priceCapDomain", "userService", "httpServices",
        function (priceCapDomain, userService, httpServices) {
            var priceCapServices = this;

            priceCapServices.GetReqData = function (params) {
                let url = priceCapDomain + 'getreqdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid;
                return httpServices.get(url);
            };

            priceCapServices.SavePriceCap = function (params) {
                let url = priceCapDomain + 'savepricecap';
                return httpServices.post(url, params);
            };
            
            return priceCapServices;

        }]);prmApp
    .service('prmCompanyService', ["PRMCompanyDomain", "userService", "httpServices",
        function (PRMCompanyDomain, userService, httpServices) {
            var prmCompanyService = this;

            prmCompanyService.getBranchProjects = function (params) {
                var url = PRMCompanyDomain + 'project?compid=' + params.compid + '&branchid=' + params.branchid + '&projectid=' + params.projectid + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            prmCompanyService.SaveProjectRequirement = function (params) {
                let url = PRMCompanyDomain + 'saveprojectrequirement';
                return httpServices.post(url, params);
            };
            /*
            techevalService.savequestionnaire = function (params) {
                let url = techevalDomain + 'savequestionnaire';
                return httpServices.post(url, params);
            };

            techevalService.assignquestionnaire = function (params) {
                let url = techevalDomain + 'assignquestionnaire';
                return httpServices.post(url, params);
            };

            techevalService.savequestion = function (params) {
                let url = techevalDomain + 'savequestion';
                return httpServices.post(url, params);
            };

            techevalService.deletequestion = function (params) {
                let url = techevalDomain + 'deletequestion';
                return httpServices.post(url, params);
            };

            techevalService.importentity = function (params) {
                let url = techevalDomain + 'importentity';
                return httpServices.post(url, params);
            };

            techevalService.getquestionnaire = function (evalid, loadquestions) {
                var url = techevalDomain + 'getquestionnaire?evalid=' + evalid + '&loadquestions=' + loadquestions + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.getreqquestionnaire = function (reqid, loadquestions) {
                var url = techevalDomain + 'gettechevalforrequirement?reqid=' + reqid + '&loadquestions=' + loadquestions + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.saveresponse = function (params) {
                let url = techevalDomain + 'saveresponse';
                return httpServices.post(url, params);
            };

            techevalService.saveTechApproval = function (params) {
                let url = techevalDomain + 'saveresponse';
                return httpServices.post(url, params);
            };

            techevalService.getresponses = function (reqid, evalid, userid) {
                var url = techevalDomain + 'getresponses?evalid=' + evalid + '&userid=' + userid + '&reqid=' + reqid + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.GetTechEvalution = function (evalid, reqid, userid) {
                let url = techevalDomain + 'gettechevalution?evalid=' + evalid + '&reqid=' + reqid + '&userid=' + userid + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.GetVendorsForTechEval = function (evalid) {
                let url = techevalDomain + 'getvendorsfortecheval?evalid=' + evalid + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.saveTechApproval = function (params) {
                let url = techevalDomain + 'savetechevaluation';
                return httpServices.post(url, params);
            };

            techevalService.saveVendorsForQuestionnaire = function (params) {
                let url = techevalDomain + 'savevendorforquestionnaire';
                return httpServices.post(url, params);
            };

            techevalService.deletequestionnaire = function (params) {
                let url = techevalDomain + 'deletequestionnaire';
                return httpServices.post(url, params);
            };

            techevalService.sendquestionnairetovendors = function (params) {
                let url = techevalDomain + 'sendquestionnairetovendors';
                return httpServices.post(url, params);
            };*/

            return prmCompanyService;
        }]);prmApp

    .service('PRMPRServices', ["PRMPRServicesDomain", "userService", "httpServices", function (PRMPRServicesDomain,userService, httpServices) {

        var PRMPRServices = this;


        PRMPRServices.GetSeries = function (series, seriestype, deptid) {
            var url = PRMPRServicesDomain + 'getseries?series=' + series + '&seriestype=' + seriestype + '&compid=' + userService.getUserCompanyId() + '&deptid=' + deptid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };
        
        PRMPRServices.getprdetails = function (params) {
            let url = PRMPRServicesDomain + 'getprdetails?prid=' + params.prid+ '&sessionid=' + userService.getUserToken();
             return httpServices.get(url);
        };

        PRMPRServices.getprlist = function (params) {
            let url = PRMPRServicesDomain + 'getprlist?userid=' + params.userid + '&sessionid=' + params.sessionid + '&deptid=' + params.deptid + '&desigid=' + params.desigid + '&depttypeid=' + params.depttypeid + '&fromdate=' + params.fromDate + '&todate=' + params.toDate;
            return httpServices.get(url);
        };

        PRMPRServices.savePrDetails = function (params) {
            let url = PRMPRServicesDomain + 'saveprdetails';
            return httpServices.post(url, params);
        };

        PRMPRServices.getreqprlist = function (params) {
            let url = PRMPRServicesDomain + 'getreqprlist?userid=' + params.userid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.GetCompanyRFQCreators = function (params) {
            let url = PRMPRServicesDomain + 'getcompanyrfqcreators?u_id=' + userService.getUserId() + '&pr_id=' + params.pr_id + '&sessionid=' + userService.getUserToken();// + '&dept_id=' + params.dept_id
            return httpServices.get(url);
        };

        PRMPRServices.SaveCompanyRFQCreators = function (params) {
            let url = PRMPRServicesDomain + 'savecompanyrfqcreators';
            return httpServices.post(url, params);
        };

        PRMPRServices.getpritemslist = function (params) {
            let url = PRMPRServicesDomain + 'getpritemslist?prid=' + params.prid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };


        return PRMPRServices;
}]);prmApp

    .service('fileUpload', ['$http', function ($http) {
        this.uploadFileToUrl = function (file, uploadUrl) {
            var fd = new FormData();
            fd.append('file', file);

            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            })

                .success(function () {
                })

                .error(function () {
                });
        }
    }])
    // =========================================================================
    // Auction Details Services
    // =========================================================================
    .service('auctionsService', function ($http, store, $state, $rootScope, domain, version) {
        //var domain = 'http://182.18.169.32/services/';
        var auctions = this;
        auctions.getdate = function () {
            var date = new Date();
            var time = date.getTime();
            return $http({
                method: 'GET',
                url: domain + 'getdate?time=' + time,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
                //data: params
            }).then(function (response) {
                var list = [];
                if (response && response.data) {
                    if (response.data.length > 0) {
                        list = response.data;
                    }
                    return list;
                } else {
                    //console.log(response.data[0].errorMessage);
                }
            }, function (result) {
                //console.log("date error");
            });
        }

        auctions.postfwdrequirementdata = function (params) {
            //params.action="requirementsave";
            var myDate = new Date(); // Your timezone!
            var myEpoch = parseInt(myDate.getTime() / 1000);
            //console.log(myEpoch);
            params.minBidAmount = 0;
            params.postedOn = "/Date(" + myEpoch + "000+0000)/";
            params.timeLeft = -1;
            params.price = -1;
            var requirement = {
                "requirement": {
                    "title": params.title,
                    "description": params.description,
                    "category": params.category,
                    "subcategories": params.subcategories,
                    "urgency": params.urgency,
                    "budget": params.budget,
                    "attachmentName": params.attachmentName,
                    "deliveryLocation": params.deliveryLocation,
                    "taxes": params.taxes,
                    "paymentTerms": params.paymentTerms,
                    "requirementID": params.requirementID,
                    "customerID": params.customerID,
                    "isClosed": params.isClosed,
                    "endTime": null,
                    "sessionID": params.sessionID,
                    "errorMessage": "",
                    "timeLeft": -1,
                    "price": -1,
                    "auctionVendors": params.auctionVendors,
                    "startTime": null,
                    "status": "",
                    "postedOn": "/Date(" + myEpoch + "000+0000)/",
                    "custLastName": params.customerLastname,
                    "custFirstName": params.customerFirstname,
                    "deliveryTime": params.deliveryTime,
                    "includeFreight": params.includeFreight,
                    "inclusiveTax": params.inclusiveTax,
                    "minBidAmount": 0,
                    "checkBoxEmail": params.checkBoxEmail,
                    "checkBoxSms": params.checkBoxSms,
                    "quotationFreezTime": params.quotationFreezTime,
                    "currency": params.currency,
                    "timeZoneID": params.timeZoneID,
                    "listRequirementItems": params.listRequirementItems,
                    "isTabular": params.isTabular,
                    "reqComments": params.reqComments,
                    "itemsAttachment": params.itemsAttachment,
                    "isSubmit": params.isSubmit,
                    "isQuotationPriceLimit": params.isQuotationPriceLimit,
                    "quotationPriceLimit": params.quotationPriceLimit,
                    "noOfQuotationReminders": params.noOfQuotationReminders,
                    "remindersTimeInterval": params.remindersTimeInterval,
                    "custCompID": params.custCompID,
                    "customerCompanyName": params.customerCompanyName,
                    "deleteQuotations": params.deleteQuotations
                }, "attachment": params.attachment
            };
            return $http({
                method: 'POST',
                url: fwddomain + 'requirementsave',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: requirement
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };

        auctions.isnegotiationended = function (reqid, sessionid) {
            return $http({
                method: 'GET',
                url: domain + 'isnegotiationended?reqid=' + reqid + '&sessionid=' + sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
                //data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }


        auctions.selectVendor = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'selectvendor',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        auctions.getauctions = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getrunningauctions?section=' + params.section + '&userid=' + params.userid + '&sessionid=' + params.sessionid + '&limit=' + params.limit,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
                //data: params
            }).then(function (response) {
                var list = [];
                if (response && response.data) {
                    if (response.data.length > 0) {
                        list = response.data;
                    }
                    return list;
                } else {
                    //console.log(response.data[0].errorMessage);
                }
            }, function (result) {
                //console.log("there is no current Negotiations");
            });
        };

        auctions.rateVendor = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'userratings',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }



        auctions.updateStatus = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'updatestatus',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        auctions.uploadquotationsfromexcel = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'uploadquotationsfromexcel',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        auctions.itemwiseselectvendor = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'itemwiseselectvendor',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        auctions.updatedeliverdate = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'updateexpdelandpaydate',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        auctions.reqTechSupport = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'reqtechsupport',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        auctions.SaveRunningItemPrice = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'saverunningitemprice',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        auctions.updatepaymentdate = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'updateexpdelandpaydate',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        auctions.StartNegotiation = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'startNegotiation',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
            });
        }


        auctions.RestartNegotiation = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'restartnegotiation',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
            });
        }

        auctions.DeleteVendorFromAuction = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'deletevendorfromauction',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {

            });
        }

        auctions.generatepo = function (params) {
            var data = {
                reqPO: params
            };
            return $http({
                method: 'POST',
                url: domain + 'generatepoforuser',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: data
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        auctions.vendorreminders = function (params) {
            var data = params;
            return $http({
                method: 'POST',
                url: domain + 'vendorreminders',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: data
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        auctions.SaveCompanyDepartment = function (params) {
            var data = params;
            return $http({
                method: 'POST',
                url: domain + 'savecompanydepartment',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: data
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        auctions.DeleteDepartment = function (params) {
            var data = params;
            return $http({
                method: 'POST',
                url: domain + 'deletedepartment',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: data
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        auctions.GetCompanyDepartments = function (userid, sessionid) {
            return $http({
                method: 'GET',
                url: domain + 'getcompanydepartments?userid=' + userid + '&sessionid=' + sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        auctions.GetUserDepartments = function (userid, sessionid) {
            return $http({
                method: 'GET',
                url: domain + 'getuserdepartments?userid=' + userid + '&sessionid=' + sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        auctions.SaveUserDepartments = function (params) {
            var data = params;
            return $http({
                method: 'POST',
                url: domain + 'saveuserdepartments',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: data
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        auctions.GetDepartmentUsers = function (deptid, userid, sessionid) {
            return $http({
                method: 'GET',
                url: domain + 'getdepartmentusers?deptid=' + deptid + '&userid=' + userid + '&sessionid=' + sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }


        auctions.GetReqDepartments = function (userid, reqid, sessionid) {
            return $http({
                method: 'GET',
                url: domain + 'getreqdepartments?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        auctions.SaveReqDepartments = function (params) {
            var data = params;
            return $http({
                method: 'POST',
                url: domain + 'savereqdepartments',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: data
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        auctions.GetCompanyConfiguration = function (compid, configkey, sessionid) {
            return $http({
                method: 'GET',
                url: domain + 'getcompanyconfiguration?compid=' + compid + '&configkey=' + configkey + '&sessionid=' + sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        auctions.GetIsAuthorized = function (userid, reqid, sessionid) {
            return $http({
                method: 'GET',
                url: domain + 'getisauthorized?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        auctions.materialdispatch = function (params) {
            var data = {
                reqMat: params
            };
            return $http({
                method: 'POST',
                url: domain + 'materialdispatch',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: data
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        auctions.paymentdetails = function (params) {
            var data = {
                paymentDets: params
            };
            return $http({
                method: 'POST',
                url: domain + 'paymentdetails',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: data
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        auctions.generatePOinServer = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'generatepo',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        auctions.getDashboardStats = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getdashboardstats?userid=' + params.userid + '&sessionid=' + params.sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                if (response && response.data) {
                    if (response.data.errorMessage == '') {
                        return response.data;
                    } else {
                        return {};
                    }
                }
            });
        };

        auctions.getCategories = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getcategories?userid=' + (params ? params : -1),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
                //data: params
            }).then(function (response) {
                var list = [];
                if (response && response.data) {
                    if (response.data.length > 0) {
                        list = response.data;
                    }
                    return list;
                } else {
                    //console.log(response.data[0].errorMessage);
                }
            }, function (result) {
                //console.log("there is no categories");
            });
        };

        auctions.getKeyValuePairs = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getkeyvaluepairs?parameter=' + params,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
                //data: params
            }).then(function (response) {
                var list = [];
                if (response && response.data) {
                    if (response.data.length > 0) {
                        list = response.data;
                    }
                    return list;
                } else {
                    //console.log(response.data[0].errorMessage);
                }
            }, function (result) {
                //console.log("there is no categories");
            });
        };

        auctions.getmyAuctions = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getmyauctions?userid=' + params.userid + "&sessionid=" + params.sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no your Negotiations");
            });
        };






        auctions.getactiveleads = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getactiveleads?userid=' + params.userid + "&sessionid=" + params.sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no your Negotiations");
            });
        };




        auctions.getrequirementdata = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getrequirementdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };



        auctions.GetReportsRequirement = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getreportsrequirement?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no reports");
            });
        };


        auctions.GetVendorReminders = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getvendorreminders?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no Reminders");
            });
        };

        auctions.GetBidHistory = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getbidhistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };

        auctions.getpodetails = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getpodetails?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };

        auctions.getcomments = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getcomments?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };

        auctions.getpricecomparison = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getpricecomparison?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };

        auctions.GetPriceComparisonPreNegotiation = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getpricecomparisonprenegotiation?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };


        auctions.savecomment = function (params) {
            var comment = {
                "com": {
                    "requirementID": params.reqID,
                    "firstName": "",
                    "lastName": "",
                    "userID": params.userID,
                    "commentText": params.commentText,
                    "replyCommentID": -1,
                    "commentID": -1,
                    "errorMessage": "",
                    "sessionID": params.sessionID
                }
            }
            return $http({
                method: 'POST',
                url: domain + 'savecomment',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: comment
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };


        auctions.deleteAttachment = function (Attaachmentparams) {
            // params.action="updatebidtime";
            return $http({
                method: 'POST',
                url: domain + 'deleteattachment',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: Attaachmentparams
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                }

            }, function (result) {
                //console.log("service failed, please try later...!");
            });
        };


        auctions.postrequirementdata = function (params) {
            //params.action="requirementsave";
            var myDate = new Date(); // Your timezone!
            var myEpoch = parseInt(myDate.getTime() / 1000);
            //console.log(myEpoch);
            params.minBidAmount = 0;
            params.postedOn = "/Date(" + myEpoch + "000+0000)/";
            params.timeLeft = -1;
            params.price = -1;
            var requirement = {
                "requirement": {
                    "title": params.title,
                    "reqType": params.isRFP ? 2 : 1,
                    "description": params.description,
                    "category": params.category,
                    "subcategories": params.subcategories,
                    "urgency": params.urgency,
                    "budget": params.budget,
                    "attachmentName": params.attachmentName,
                    "deliveryLocation": params.deliveryLocation,
                    "taxes": params.taxes,
                    "paymentTerms": params.paymentTerms,
                    "requirementID": params.requirementID,
                    "lotId": params.lotId,
                    "customerID": params.customerID,
                    "isClosed": params.isClosed,
                    "endTime": null,
                    "sessionID": params.sessionID,
                    "errorMessage": "",
                    "timeLeft": -1,
                    "price": -1,
                    "auctionVendors": params.auctionVendors,
                    "startTime": null,
                    "status": "",
                    "postedOn": "/Date(" + myEpoch + "000+0000)/",
                    "custLastName": params.customerLastname,
                    "custFirstName": params.customerFirstname,
                    "deliveryTime": params.deliveryTime,
                    "includeFreight": params.includeFreight,
                    "inclusiveTax": params.inclusiveTax,
                    "minBidAmount": 0,
                    "checkBoxEmail": params.checkBoxEmail,
                    "checkBoxSms": params.checkBoxSms,
                    "quotationFreezTime": params.quotationFreezTime,
                    "currency": params.currency,
                    "timeZoneID": params.timeZoneID,
                    "listRequirementItems": params.listRequirementItems,
                    "isTabular": params.isTabular,
                    "reqComments": params.reqComments,
                    "itemsAttachment": params.itemsAttachment,
                    "isSubmit": params.isSubmit,
                    "isQuotationPriceLimit": params.isQuotationPriceLimit,
                    "quotationPriceLimit": params.quotationPriceLimit,
                    "noOfQuotationReminders": params.noOfQuotationReminders,
                    "remindersTimeInterval": params.remindersTimeInterval,
                    "custCompID": params.custCompID,
                    "customerCompanyName": params.customerCompanyName,
                    "deleteQuotations": params.deleteQuotations
                }, "attachment": params.attachment
            };
            return $http({
                method: 'POST',
                url: domain + 'requirementsave',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: requirement
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {
                    //console.log(response.data.errorMessage);
                }
            }, function (result) {
                //console.log(result);
                //console.log("there is no current Negotiations");
            });
        };

        auctions.updatebidtime = function (params) {
            // params.action="updatebidtime";
            return $http({
                method: 'POST',
                url: domain + 'updatebidtime',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                if (response && response.data) {
                    return "";
                } else {
                    return response.data.errorMessage;
                }
            }, function (result) {
                //console.log("service failed, please try later...!");
            });
        };

        auctions.makeabid = function (params) {
            //params.action="makeabid";
            var list = [];
            return $http({
                method: 'POST',
                url: domain + 'makeabid',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                if (response && response.data) {
                    //return "";
                    list = response.data;
                    return list;
                } else {
                    return response.data.errorMessage;
                }
            }, function (result) {
                //console.log("service failed, please try later...!");
            });
        };



        auctions.uploadQuotation = function (params) {
            //params.action="makeabid";
            var list = [];
            return $http({
                method: 'POST',
                url: domain + 'uploadquotation',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                if (response && response.data) {
                    //return "";
                    list = response.data;
                    return list;
                } else {
                    return response.data.errorMessage;
                }
            }, function (result) {
                //console.log("service failed, please try later...!");
            });
        };



        auctions.revquotationupload = function (params) {
            //params.action="makeabid";
            var list = [];
            return $http({
                method: 'POST',
                url: domain + 'revquotationupload',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                if (response && response.data) {
                    //return "";
                    list = response.data;
                    return list;
                } else {
                    return response.data.errorMessage;
                }
            }, function (result) {
                //console.log("service failed, please try later...!");
            });
        };


        auctions.QuatationAprovel = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'quatationaprovel',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        auctions.savepricecap = function (params) {
            return $http({
                method: 'POST',
                url: domain + 'savepricecap',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        auctions.GetCompanyLeads = function (params) {
            return $http({
                method: 'GET',
                url: domain + 'getcompanyleads?userid=' + params.userid + '&searchstring=' + params.searchstring + '&searchtype=' + params.searchtype + '&sessionid=' + params.sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        auctions.updateauctionstart = function (params) {
            //params.action="makeabid";
            return $http({
                method: 'POST',
                url: domain + 'updateauctionstart',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                if (response && response.data) {
                    return "";
                } else {
                    return response.data.errorMessage;
                }
            }, function (result) {
                //console.log("service failed, please try later...!");
            });
        }



        return auctions;
    })









    // =========================================================================
    // Header Messages and Notifications list Data
    // =========================================================================

    .service('messageService', ['$resource', function ($resource) {
        /* this.getMessage = function(img, user, text) {
             var gmList = $resource("data/messages-notifications.json");

             return gmList.get({
                 img: img,
                 user: user,
                 text: text
             });
         }*/
    }])




    //// =========================================================================
    //// Malihu Scroll - Custom Scroll bars
    //// =========================================================================
    //.service('scrollService', function() {
    //    var ss = {};
    //    ss.malihuScroll = function scrollBar(selector, theme, mousewheelaxis) {
    //        $(selector).mCustomScrollbar({
    //            theme: theme,
    //            scrollInertia: 100,
    //            axis:'yx',
    //            mouseWheel: {
    //                enable: true,
    //                axis: mousewheelaxis,
    //                preventDefault: true
    //            }
    //        });
    //    }

    //    return ss;
    //})

    //==============================================
    // BOOTSTRAP GROWL
    //==============================================

    .service('growlService', function () {
        var gs = {};
        gs.growl = function (message, type) {
            $.growl({
                message: message
            }, {
                    type: type,
                    allow_dismiss: false,
                    label: 'Cancel',
                    className: 'btn-xs btn-inverse',
                    placement: {
                        from: 'top',
                        align: 'right'
                    },
                    delay: 2500,
                    animate: {
                        enter: 'animated bounceIn',
                        exit: 'animated bounceOut'
                    },
                    offset: {
                        x: 20,
                        y: 85
                    }
                });
        }

        return gs;
    })

    .service('authService', ["$http", "store", "$state", "$rootScope", "domain", "version", function ($http, store, $state, $rootScope, domain, version) {
        var authService = this;
        var responseObj = {};
        responseObj.objectID = 0;
        authService.isValidSession = function (sessionid) {
            return $http({
                method: 'GET',
                url: domain + 'issessionvalid?sessionid=' + sessionid,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                if (response && response.data) {
                    responseObj = response.data;
                    return responseObj;
                } else {
                    return responseObj;
                }
            }, function (result) {
                //console.log("date error");
            });
        }

        return authService;
    }]);prmApp


    // =========================================================================
    // Auction Details Services
    // =========================================================================
    .service('storeService', function ($http, store, $state, $rootScope, storeDomain, version, userService) {
        //var domain = 'http://182.18.169.32/services/';
        var storeService = this;

        storeService.Stores = [];
        storeService.savestore = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestore',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.deletestore = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'deletestore',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.deleteStoreItemDetails = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'deletestoreitemdetails',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.getcompanystores = function (compID, parentStoreID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'companystores?compid=' + compID + '&parentstoreid=' + parentStoreID +'&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }


        storeService.getstores = function (storeID, compID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'stores?storeid=' + storeID + '&compid' + compID + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }





        storeService.savestoreitem = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestoreitem',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        storeService.deletestoreitem = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'deletestoreitem',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.getuniqueitems = function (compID, itemName) {
            return $http({
                method: 'GET',
                url: storeDomain + 'uniqueitems?compid=' + compID + '&itemname=' + itemName + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                console.log("date error");
            });
        }

        storeService.getcompanystoreitems = function (storeID, itemID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'storeitems?storeid=' + storeID + '&itemid=' + itemID + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }



        storeService.importentity = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'importentity',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }



        storeService.storesitempropvalues = function (itemID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'storesitempropvalues?itemid=' + itemID + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }


        storeService.savestoreitemprop = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestoreitemprop',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.savestoreitempropvalue = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestoreitempropvalue',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }


        storeService.storesitempropvalues = function (itemID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'storesitempropvalues?itemid=' + itemID + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }



        storeService.getStoreItemDetails = function (itemID, notDispatched) {
            return $http({
                method: 'GET',
                url: storeDomain + 'getstoreitemdetails?itemid=' + itemID + '&notdispatched=' + notDispatched + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        storeService.saveStoreItemDetails = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestoreitemdetails',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.saveStoreItemGIN = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestoreitemissue',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.saveStoreItemGRN = function (params) {
            return $http({
                method: 'POST',
                url: storeDomain + 'savestoreitemreceive',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        storeService.getStoreItemGIN = function (ginCode, storeID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'storeitemissuedetails?gincode=' + ginCode + '&storeid=' + storeID + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        storeService.getStoreItemGRN = function (grnCode, storeID) {
            return $http({
                method: 'GET',
                url: storeDomain + 'storeitemreceivedetails?grncode=' + grnCode + '&storeid=' + storeID + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }

        storeService.getIndentItemDetails = function (storeID, ginCode, grnCode) {
            return $http({
                method: 'GET',
                url: storeDomain + 'indentitemdetails?storeid=' + storeID + '&gincode=' + ginCode + '&grncode=' + grnCode + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        }


        return storeService;
    });






prmApp
    .service('techevalService', function ($http, store, $state, $rootScope, techevalDomain, version, userService) {
        //var domain = 'http://182.18.169.32/services/';
        var techevalService = this;

        techevalService.getquestionnairelist = function (evalID) {
            return $http({
                method: 'GET',
                url: techevalDomain + 'getquestionnairelist?userid=' + userService.getUserId() + '&evalid=' + evalID +'&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        };

        techevalService.savequestionnaire = function (params) {
            return $http({
                method: 'POST',
                url: techevalDomain + 'savequestionnaire',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        };

        techevalService.savequestion = function (params) {
            return $http({
                method: 'POST',
                url: techevalDomain + 'savequestion',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        };


        techevalService.deletequestion = function (params) {
            return $http({
                method: 'POST',
                url: techevalDomain + 'deletequestion',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        };

        techevalService.importentity = function (params) {
            return $http({
                method: 'POST',
                url: techevalDomain + 'importentity',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        };

        techevalService.getquestionnaire = function (evalid, loadquestions) {
            return $http({
                method: 'GET',
                url: techevalDomain + 'getquestionnaire?evalid=' + evalid + '&loadquestions=' + loadquestions + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        };

        techevalService.saveresponse = function (params) {
            return $http({
                method: 'POST',
                url: techevalDomain + 'saveresponse',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        };

        techevalService.saveTechApproval = function (params) {
            return $http({
                method: 'POST',
                url: techevalDomain + 'saveresponse',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        };


        techevalService.getresponses = function (reqid, evalid, userid) {
            return $http({
                method: 'GET',
                url: techevalDomain + 'getresponses?evalid=' + evalid + '&userid=' + userid + '&reqid=' + reqid + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        };


        techevalService.GetTechEvalution = function (evalid, reqid) {
            return $http({
                method: 'GET',
                url: techevalDomain + 'gettechevalution?evalid=' + evalid + '&reqid=' + reqid + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        };

        techevalService.GetVendorsForTechEval = function (evalid) {
            return $http({
                method: 'GET',
                url: techevalDomain + 'getvendorsfortecheval?evalid=' + evalid + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log("date error");
            });
        };


        techevalService.saveTechApproval = function (params) {
            return $http({
                method: 'POST',
                url: techevalDomain + 'savetechevaluation',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        };

        techevalService.saveVendorsForQuestionnaire = function (params) {
            return $http({
                method: 'POST',
                url: techevalDomain + 'savevendorforquestionnaire',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        return techevalService;
    })






angular.module("prm.user").service('userService', userService);


function userService($http, store, $state, $rootScope, version, domain, growlService) {
    //var domain = 'http://182.18.169.32/services/';
    var self = this;
    var successMessage = '';

    // currentUser must be stored in an object since it is nullable
    // If we two-way bind to currentUser directly, the bind would break
    // when currentUser is set to null.

    self.userData = { currentUser: null };

    self.getUserObj = function () {
        if (!self.userData.currentUser || !self.userData.currentUser.id) {
            self.userData.currentUser = store.get('currentUser');
        }
        return self.userData.currentUser || {};
    };
    self.getUserToken = function () {
        return store.get('sessionid');
    };

    self.getLocalEntitlement = function () {
        return store.get('localEntitlement');
    };

    self.removeLocalEntitlement = function () {
        return store.remove('localEntitlement');
    };

    self.getOtpVerified = function () {
        return self.getUserObj().isOTPVerified;
    };

    self.getEmailOtpVerified = function () {
        return self.getUserObj().isEmailOTPVerified;
    };

    self.reloadProfileObj = function(){

    }

    self.getDocsVerified = function () {
        return self.getUserObj().credentialsVerified;
    };
    self.removeUserToken = function () {
        store.remove('sessionid');
    };

    self.getRememberMeToken = function () {
        return store.get('rememberMeToken');
    };
    self.removeRememberMeToken = function () {
        store.remove('rememberMeToken');
    };

    self.removeUser = function () {
        store.remove('currentUser');
    };

    self.getUsername = function () {
        return self.getUserObj().username;
    };

    self.getFirstname = function () {
        return self.getUserObj().firstName;
    };

    self.getLastname = function () {
        return self.getUserObj().lastName;
    };

    self.getUserId = function () {
        return self.getUserObj().userID;
    };

    self.getUserCompanyId = function () {
        return self.getUserObj().companyId;
    };

    self.setMessage = function (msg) {
        successMessage = msg;
    };

    self.getMessage = function () {
        return successMessage;
    };

    self.getUserType = function () {
        return self.getUserObj().userType;
    };


    self.setCurrentUser = function () {
        self.userData.sessionid = self.getUserToken();
        self.userData.currentUser = self.getUserObj();
    };

    self.setCurrentUser();

    self.checkUniqueValue21 = function (type, currentvalue) {
        var data = {
            "type": type,
            "currentvalue": currentvalue
        };
        //console.log(data);
        return false;
        // return $http.post("", data).then( function(res) {
        //   return res.data.isUnique;
        // });
    };

    self.checkUniqueValue = function (type, currentvalue) {
        var data = {
            "phone": currentvalue,
            "idtype": type
        };
        //console.log(data);
        //return false;
        return $http({
            method: 'POST',
            url: domain + 'checkuserifexists',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            //console.log(response);
            return response.data.CheckUserIfExistsResult;
        });
    };

    self.forgotpassword = function (forgot) {
        var data = {
            "email": forgot.email,
            "sessionid":''
        };
        //data=forgot.email;
        return $http({
            method: 'POST',
            url: domain + 'forgotpassword',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            return response;
        });
        //  $http.post(domain+"forgotassword", data).then( function(res) {
        //   return res;
        // });
    };

    self.resetpassword = function (resetpass) {
        var data = {
            "email": resetpass.email,
            "sessionid":resetpass.sessionid,
            "NewPass": resetpass.NewPass,
            "ConfNewPass": resetpass.ConfNewPass
        };
        //data=forgot.email;
        return $http({
            method: 'POST',
            url: domain + 'resetpassword',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            $state.go('login');
            return response;
        });
        //  $http.post(domain+"forgotassword", data).then( function(res) {
        //   return res;
        // });
    };

    self.resendotp = function (userid) {
        var data = {
            "userID": userid
        };
        return $http({
            method: 'POST',
            url: domain + 'sendotp',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            swal("Done!", "OTP send to your registered Mobile No.", "success");
        });
    };


    self.isnegotiationrunning = function () {
        var data = {
            "userID": self.getUserId(),
            "sessionID": self.getUserToken()
        };
        return $http({
            method: 'POST',
            url: domain + 'isnegotiationrunning',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            //console.log('isnegotiationrunning');
            return response;
        });
    };


    self.resendemailotp = function (userid) {
        var data = {
            "userID": userid
        };
        return $http({
            method: 'POST',
            url: domain + 'sendotpforemail',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            swal("Done!", "OTP send to your registered Email.", "success");
        });
    };


    self.getUserDataNoCache = function(){
        var params = {
            userid: self.getUserId(),
            sessionid: self.getUserToken()
        }
        return $http({
            method: 'GET',
            url: domain + 'getuserinfo?userid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {

            store.set('currentUser', response.data);
            self.setCurrentUser();
            var list = {};
            if (response && response.data) {
                list = response.data;
                return list;
            } else {
                //console.log(response.data.errorMessage);
            }
        }, function (result) {
            //console.log(result);
        });
    }

    self.verifyOTP = function (otpobj) {
        var data = {
            "OTP": otpobj,
            "userID": self.getUserId(),
            "phone": otpobj.phone ? otpobj.phone : ""
        };
        return $http({
            method: 'POST',
            url: domain + 'verifyotp',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            if (response.data.sessionID != "" || response.data.userInfo.sessionID != "") {
                store.set('sessionid', response.data.sessionID != "" ? response.data.sessionID : response.data.userInfo.sessionID);
                if (response.data.userInfo.isOTPVerified == 1 && response.data.userInfo.credentialsVerified == 1) {
                    store.set('verified', 1);
                } else {
                    store.set('verified', 0);
                }
                if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                } else {
                    store.set('emailverified', 0);
                }
                if(response.data.userInfo.credentialsVerified == 1){
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                $state.go('pages.profile.profile-about');
            } else {
                swal('Warning', "The OTP is not valid. Please enter the valid OTP sent to your mobile number", "warning");
            }
            /*if(response.data.userInfo.isOTPVerified==1 && response.data.userInfo.credentialsVerified==1){
              store.set('verified',response.data.userInfo.credentialsVerified);
              $state.go('home');
              growlService.growl('Welcome to PRM360', 'inverse');
            }*/
            return response.data;
        });
    };





    self.verifyEmailOTP = function (otpobj) {
        var data = {
            "OTP": otpobj,
            "userID": self.getUserId(),
            "email": otpobj.email ? otpobj.email : ""
        };
        return $http({
            method: 'POST',
            url: domain + 'verifyemailotp',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            if (response.data.sessionID != "" || response.data.userInfo.sessionID != "") {
                store.set('sessionid', response.data.sessionID != "" ? response.data.sessionID : response.data.userInfo.sessionID);
                if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                } else {
                    store.set('emailverified', 0);
                }
                if (response.data.userInfo.isOTPVerified == 1) {
                    store.set('verified', 1);
                } else {
                    store.set('verified', 0);
                }
                if(response.data.userInfo.credentialsVerified == 1){
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                $state.go('pages.profile.profile-about');
            } else {
                swal('Warning', "The OTP is not valid. Please enter the valid OTP sent to your Email", "warning");
            }
            /*if(response.data.userInfo.isOTPVerified==1 && response.data.userInfo.credentialsVerified==1){
              store.set('verified',response.data.userInfo.credentialsVerified);
              $state.go('home');
              growlService.growl('Welcome to PRM360', 'inverse');
            }*/
            return response.data;
        });
    };









  		self.isLoggedIn = function () {
        return (self.getUserToken() && self.getOtpVerified() && self.getDocsVerified()) ? true : false;
  		};

    self.updateUser = function (params) {


        var params1 = {
            "user": {
                "userID": params.userID,
                "firstName": params.firstName,
                "lastName": params.lastName,
                "email": params.email,
                "phoneNum": params.phoneNum,
                "sessionID": params.sessionID,
                "isOTPVerified": params.isOTPVerified,
                "credentialsVerified": params.credentialsVerified,
                "errorMessage": params.errorMessage,
                "logoFile": params.logoFile ? params.logoFile : null,
                "logoURL": params.logoURL ? params.logoURL : "",
                "aboutUs": params.aboutUs ? params.aboutUs : "",
                "achievements": params.achievements ? params.achievements : "",
                "assocWithOEM": params.assocWithOEM ? params.assocWithOEM : false ,
                "assocWithOEMFile": params.assocWithOEMFile ? params.assocWithOEMFile : null ,
                "assocWithOEMFileName": params.assocWithOEMFileName ? params.assocWithOEMFileName : "" ,
                "clients": params.clients ? params.clients : "",
                "establishedDate": ("establishedDate" in params) ? params.establishedDate :'/Date(634334872000+0000)/',
                "products": params.products ? params.products : "",
                "strengths": params.strengths ? params.strengths : "",
                "responseTime": params.responseTime ? params.responseTime : "",
                "oemCompanyName": params.oemCompanyName ? params.oemCompanyName : "",
                "oemKnownSince": params.oemKnownSince? params.oemKnownSince : "",
                "files": [],
                "profileFile": params.profileFile ? params.profileFile : null ,
                "profileFileName": params.profileFileName ? params.profileFileName : "",
                "workingHours": params.workingHours ? params.workingHours : "",
                "directors": params.directors ? params.directors : "",
                "address": params.address ? params.address : "",
                "subcategories": params.subcategories
            }
        }
        // if (params.hasOwnProperty("establishedDate")) {
        //     var establishedDate = params.establishedDate.replace(/(\d+)\/(\d+)\/(\d+)/, "$3/$2/$1");
        //     var date = new Date(establishedDate);
        //     console.log(date);
        //     var milliseconds = parseInt(date.getTime() / 1000.0);
        //     console.log(milliseconds);
        //     params1.user.establishedDate = "/Date(" + milliseconds + "000+0530)/";
        // }
        //console.log(params1);
        return $http({
            method: 'POST',
            url: domain + 'updateuserinfo',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params1
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
                store.set('sessionid', response.data.sessionID);
                store.set('verified', 0);
                store.set('emailverified', 0);
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                if (response.data.userInfo.isOTPVerified == 1 && response.data.userInfo.credentialsVerified == 1) {
                    store.set('verified', 1);
                } if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                }
                if(response.data.userInfo.credentialsVerified == 1){
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                if (response.data.objectID > 0) {

                    growlService.growl('User data has updated Successfully!', 'inverse');
                    //self.getProfileDetails();
                    //$state.go('pages.profile.profile-about');
                    if(params.ischangePhoneNumber == 1){
                        //self.logout();
                        $state.go('login');
                        return responce.data;
                    }else{
                        $state.reload();
                    }
                    return "true";
                } else {
                    return "false";
                }
            } else if (response && response.data && response.data.errorMessage != "") {
                swal('Error', response.data.errorMessage, 'error');
                //store.set('emailverified', 1);
                return response.data.errorMessage;

            } else {
                return "Update failed";
            }
        }, function (result) {
            return "Update failed";
        });
    }

    self.getProfileDetails = function (params) {
        return $http({
            method: 'GET',
            url: domain + 'getuserdetails?userid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {
            var list = {};
            if (response && response.data) {
                list = response.data;
                return list;
            } else {
                //console.log(response.data.errorMessage);
            }
        }, function (result) {
            //console.log(result);
        });
        $state.go('pages.profile.profile-about');
    }

    self.updatePassword = function(params){
        return $http({
            method: 'POST',
            url: domain + 'changepassword',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params
        }).then(function (response) {
            return response.data;
        });
    }


    self.AddVendorsExcel = function (params) {
        return $http({
            method: 'POST',
            url: domain + 'importentity',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params
        }).then(function (response) {
            return response.data;
        });
    }

    self.getuseraccess = function (userid, sessionid) {
        return $http({
            method: 'GET',
            url: domain + 'getuseraccess?userid=' + userid + '&sessionid=' + sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        }, function (result) {
            console.log("date error");
        });
    }





    self.saveUserAccess = function (params) {
        return $http({
            method: 'POST',
            url: domain + 'saveuseraccess',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params
        }).then(function (response) {
            return response.data;
        });
    }




    self.getSubUsersData = function (params) {
        return $http({
            method: 'GET',
            url: domain + 'getsubuserdata?userid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {
            return response.data;
        });
    };

    self.getCompanyUsers = function (params) {
        return $http({
            method: 'GET',
            url: domain + 'getcompanyuserdata?compId=' + params.compId + '&sessionId=' + params.sessionId,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {
            return response.data;
        });
    };



	   self.updateVerified = function (verified) {
        store.set('credverified', verified);
    }
    self.checkUserUniqueResult = function (uniquevalue, idtype) {
        var data = {
            "phone": uniquevalue,
            "idtype": idtype
        };
        return $http({
            method: 'POST',
            url: domain + 'checkuserifexists',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            return response.data.CheckUserIfExistsResult;
        });
    }
    self.login = function (user) {
        return $http({
            method: 'POST',
            url: domain + 'loginuser',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: user
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
                store.set('sessionid', response.data.sessionID);
                store.set('verified', 0);
                store.set('emailverified', 0);
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                if (response.data.userInfo.isOTPVerified == 1) {
                    store.set('verified', 1);
                }
                else if (response.data.userInfo.isOTPVerified == 0) {
                    self.resendotp(response.data.objectID);
                }
                if(response.data.userInfo.credentialsVerified == 1){
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                }
                else if (response.data.userInfo.isEmailOTPVerified == 0) {
                    self.resendemailotp(response.data.objectID);
                }

                $rootScope.entitlements = [];
                self.getuseraccess(response.data.objectID, response.data.sessionID)
               .then(function (responseObj) {
                   if (responseObj && responseObj.length > 0) {
                       $rootScope.entitlements = responseObj;
                       store.set('localEntitlement', $rootScope.entitlements);
                   }

                   $state.go('home');
               })

                return response.data;
            } else if (response && response.data && response.data.errorMessage != "") {
                return response.data.errorMessage;
            } else {
                return "Login failed";
            }
        }, function (result) {
            return "Login failed";
        });
    };

    self.logout = function () {
        return $http({
            method: 'POST',
            data: { "userID": self.getUserId(), "sessionID": self.userData.sessionid },
            url: domain + 'logoutuser',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            self.removeUserToken();
            self.removeLocalEntitlement();
            self.removeUser();
            delete self.userData.sessionid;
            delete self.userData.currentUser;
            self.setMessage("Successfully Logged Out");
            $state.go('login');
        });
    };

    self.deleteUser = function(params){
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'deleteuser',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }

    self.activateUser = function(params){
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'activateuser',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }

    self.activatecompanyvendor = function (params) {
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'activatecompanyvendor',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }


    self.userregistration = function (register) {
        var params = {
            "userInfo": {
                "userID": "0",
                "firstName": register.firstName,
                "lastName": register.lastName,
                "email": register.email,
                "phoneNum": register.phoneNum,
                "username": register.username,
                "password": register.password,
                "birthday": '/Date(634334872000+0000)/',
                "userType": "CUSTOMER",
                "isOTPVerified": 0,
                "category": "",
                "institution": ("institution" in register) ? register.institution : "",
                "addressLine1": "",
                "addressLine2": "",
                "addressLine3": "",
                "city": "",
                "state": "",
                "country": "",
                "zipCode": "",
                "addressPhoneNum": "",
                "extension1": "",
                "extension2": "",
                "userData1": "",
                "registrationScore" : 0,
                "errorMessage": "",
                "sessionID": ""
            }
        };

        return $http({
            method: 'POST',
            url: domain + 'registeruser',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
                store.set('sessionid', response.data.sessionID);
                store.set('verified', 0);
                store.set('emailverified', 0);
                if(response.data.userInfo.credentialsVerified == 1){
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                return response.data;
                //$state.go('home');
                //growlService.growl('Welcome to PRM360, To add new requirement Please use "+" at the Bottom.', 'inverse');
            } else if (response && response.data && response.data.errorMessage != "") {
                return response.data.errorMessage;
            } else {
                return "Registeration failed";
            }
        }, function (result) {
            return "Registeration failed";
        });
    };

    self.vendorregistration = function (vendorregisterobj) {
        var vendorcat = [];
        vendorcat.push(vendorregisterobj.category);

        var params = {
            "vendorInfo": {
                "firstName": vendorregisterobj.firstName,
                "lastName": vendorregisterobj.lastName,
                "email": vendorregisterobj.email,
                "contactNum": vendorregisterobj.phoneNum,
                "username": vendorregisterobj.username,
                "password": vendorregisterobj.password,
                "institution": vendorregisterobj.institution ? vendorregisterobj.institution : "",
                "rating": 1,
                "isOTPVerified": 0,
                "category": vendorcat,
                "panNum": ("panno" in vendorregisterobj) ? vendorregisterobj.panno : "",
                "serviceTaxNum": ("taxno" in vendorregisterobj) ? vendorregisterobj.taxno : "",
                "vatNum": ("vatno" in vendorregisterobj) ? vendorregisterobj.vatno : "",
                "referringUserID": 0,
                "knownSince": "",
                "errorMessage": "",
                "sessionID": ""
            }
        };
        return $http({
            method: 'POST',
            url: domain + 'addnewvendor',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "") {
                store.set('sessionid', response.data.sessionID);
                store.set('currentUser', response.data);
                store.set('verified', 0);
                store.set('emailverified', 0);
                if(response.data.credentialsVerified == 1){
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                self.setCurrentUser();
                return response.data;
                //growlService.growl('Welcome to PRM360, Please go thourgh with your Auctions', 'inverse');
                //return "Vendor Registration Successfully Completed.";
            } else if (response && response.data && response.data.errorMessage != "") {
                return response.data.errorMessage;
            } else {
                return "Vendor Registration Failed.";
            }
        });
    };


    self.vendorregistration1 = function (vendorregisterobj) {
        var vendorcat = [];
        vendorcat.push(vendorregisterobj.category);
        //console.log(vendorregisterobj.role);
        var params = {
            "register": {
                "firstName": vendorregisterobj.firstName,
                "lastName": vendorregisterobj.lastName,
                "email": vendorregisterobj.email,
                "phoneNum": vendorregisterobj.phoneNum,
                "username": vendorregisterobj.username,
                "password": vendorregisterobj.password,
                "companyName": vendorregisterobj.institution ? vendorregisterobj.institution : "",
                "isOTPVerified": 0,
                "category": vendorregisterobj.category ? vendorregisterobj.category : "",
                "userType": vendorregisterobj.role,
                "panNumber": ("panno" in vendorregisterobj) ? vendorregisterobj.panno : "",
                "stnNumber": ("taxno" in vendorregisterobj) ? vendorregisterobj.taxno : "",
                "vatNumber": ("vatno" in vendorregisterobj) ? vendorregisterobj.vatno : "",
                "referringUserID": 0,
                "knownSince": "",
                "errorMessage": "",
                "sessionID": "",
                "userID": 0,
                "department": "",
                "currency": vendorregisterobj.currency,
                "timeZone": vendorregisterobj.timeZone,
                "subcategories": vendorregisterobj.subcategories ? vendorregisterobj.subcategories : []

            }
        };
        return $http({
            method: 'POST',
            url: domain + 'register',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "") {
                store.set('sessionid', response.data.sessionID);
                store.set('currentUser', response.data.userInfo);
                store.set('verified', 0);
                store.set('emailverified', 0);
                if(response.data.credentialsVerified == 1){
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                self.setCurrentUser();
                return response.data;
                //growlService.growl('Welcome to PRM360, Please go thourgh with your Auctions', 'inverse');
                //return "Vendor Registration Successfully Completed.";
            } else if (response && response.data && response.data.errorMessage != "") {
                return response.data.errorMessage;
            } else {
                return "Vendor Registration Failed.";
            }
        });
    };




    self.addnewuser = function (addnewuserobj) {
        var referringUserID = 0;
        referringUserID = self.getUserId();
        var params = {
            "register": {
                "firstName": addnewuserobj.firstName,
                "lastName": addnewuserobj.lastName,
                "email": addnewuserobj.email,
                "phoneNum": addnewuserobj.phoneNum,
                "username": addnewuserobj.email,
                "password": addnewuserobj.phoneNum,
                "companyName": addnewuserobj.companyName ? addnewuserobj.companyName : "",
                "isOTPVerified": 0,
                "category": addnewuserobj.category ? addnewuserobj.category : "",
                "userType": addnewuserobj.userType,
                "panNumber": "",
                "stnNumber": "",
                "vatNumber": "",
                "referringUserID": referringUserID,
                "knownSince": "",
                "errorMessage": "",
                "sessionID": "",
                "userID": 0,
                "department": "",
                "currency": addnewuserobj.currency,
                "isAdmin": addnewuserobj.isAdmin
            }
        };
        return $http({
            method: 'POST',
            url: domain + 'register',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            return response.data;
        });
    };


    self.GetCompanyVendors = function (params) {
        return $http({
            method: 'GET',
            url: domain + 'getcompanyvendors?userid=' + params.userID + '&sessionid=' + params.sessionID,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {
            var list = {};
            if (response && response.data) {
                list = response.data;
                return list;
            } else {
                //console.log(response.data.errorMessage);
            }
        }, function (result) {
            //console.log(result);
        });
    }



    //self.saveAlternateDetails = function (params) {
    //    return $http({
    //        method: 'POST',
    //        url: domain + 'savealternatedetails',
    //        headers: { 'Content-Type': 'application/json' },
    //        encodeURI: true,
    //        data: params
    //    }).then(function (response) {
    //        return response.data;
    //    });
    //}


    self.addnewdetails = function (addnewdetailsobj) {
        var referringUserID = 0;
        referringUserID = self.getUserId();
        var params = {
            "user": {
                "alternateID": 0,
                "companyId": userService.companyId,
                "userID": userService.getUserId() ,
                "firstName": addnewdetailsobj.firstName,
                "lastName": addnewdetailsobj.lastName,
                "email": addnewdetailsobj.email,
                "phoneNum": addnewdetailsobj.phoneNum,
                "CompanyName": addnewdetailsobj.companyName ? addnewdetailsobj.companyName : ""
                //"isPrimary": addnewdetailsobj ,
                //"isValid": addnewdetailsobj
            }
        };
        return $http({
            method: 'POST',
            url: domain + 'newDetails',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            return response.data;
        });
    };




};


prmApp
    .service('reportingService', ["reportingDomain", "userService", "httpServices", "$window", function (reportingDomain, userService, httpServices, $window) {
        //var domain = 'http://182.18.169.32/services/';
        var reportingService = this;

        reportingService.getLiveBiddingReport = function (reqID) {
            let url = reportingDomain + 'getlivebiddingreport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getItemWiseReport = function (reqID) {
            let url = reportingDomain + 'getitemwisereport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getDeliveryDateReport = function (reqID) {
            let url = reportingDomain + 'deliverytimelinereport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getPaymentTermsReport = function (reqID) {
            let url = reportingDomain + 'paymenttermsreport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetReqDetails = function (reqID) {
            let url = reportingDomain + 'getreqdetails?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetNewQuotations = function (params) {
            let url = reportingDomain + 'GetNewQuotations';
            return httpServices.post(url, params);
        };

        reportingService.DeleteQcs = function (params) {
            let url = reportingDomain + 'DeleteQcs';
            return httpServices.post(url, params);
        };

        reportingService.ActivateQcs = function (params) {
            let url = reportingDomain + 'ActivateQcs';
            return httpServices.post(url, params);
        };

        reportingService.downloadTemplate = function (template, userid, reqid, templateid) {
            if (!templateid) {
                templateid = 0;
            }

            let url = reportingDomain + 'gettemplates?template=' + template + '&userid=' + userid + '&reqid=' + reqid + '&compID=' + userService.getUserCompanyId() + '&templateid=' + templateid + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        reportingService.downloadConsolidatedTemplate = function (template, from, to, userid) {
            let url = reportingDomain + 'getconsolidatedtemplates?template=' + template + '&from=' + from + '&to=' + to + '&userid=' + userid + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        reportingService.GetUserLoginTemplates = function (template, from, to, userid) {
            let url = reportingDomain + 'GetUserLoginTemplates?template=' + template + '&from=' + from + '&to=' + to + '&userid=' + userid + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        reportingService.GetLogsTemplates = function (template, from, to, companyID) {
            let url = reportingDomain + 'GetLogsTemplates?template=' + template + '&from=' + from + '&to=' + to + '&companyID=' + companyID + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }

        reportingService.GetReqReportForExcel = function (reqid, sessionid) {
            let url = reportingDomain + 'getreqreportforexcel?reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        reportingService.GetReqItemWiseVendors = function (reqid, sessionid) {
            let url = reportingDomain + 'getReqItemWiseVendors?reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        reportingService.getConsolidatedReport = function (fromDate, toDate) {
            let url = reportingDomain + 'getconsolidatedreports?from=' + fromDate + '&to=' + toDate + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getLogisticConsalidatedReport = function (fromDate, toDate) {
            let url = reportingDomain + 'getLogisticConsolidatedreports?from=' + fromDate + '&to=' + toDate + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getconsolidatedbasepricereport = function (fromDate, toDate) {
            let url = reportingDomain + 'getconsolidatedbasepricereports?from=' + fromDate + '&to=' + toDate + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetAccountingConsolidatedReports = function (fromDate, toDate) {
            let url = reportingDomain + 'getAccountingConsolidatedreports?from=' + fromDate + '&to=' + toDate + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        //#region QCS

        reportingService.GetQCSDetails = function (params) {
            let url = reportingDomain + 'qcsdetails?qcsid=' + params.qcsid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetQcsPRDetails = function (params) {
            let url = reportingDomain + 'GetQcsPRDetails?reqID=' + params.reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetQCSList = function (params) {
            let url = reportingDomain + 'getqcslist?uid=' + params.uid + '&reqid=' + params.reqid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetQCSIDS = function (params) {
            let url = reportingDomain + 'GetQCSIDS?reqid=' + params.reqid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.SaveQCSDetails = function (params) {
            let url = reportingDomain + 'saveqcsdetails';
            return httpServices.post(url, params);
        };

        reportingService.GeneratePOProccess = function (params) {
            let url = reportingDomain + 'GeneratePOProccess';
            return httpServices.post(url, params);
        };

        //#endregion QCS

        //#region RM QCS

        reportingService.GetRMQCSReportForExcel = function (reqid, itemID, sessionid) {
            let url = reportingDomain + 'getrmqcsreportforexcel?reqid=' + reqid + '&itemID=' + itemID + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        reportingService.GetRMQCSDetails = function (params) {
            let url = reportingDomain + 'getrmqcsdetails?qcsid=' + params.qcsid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.SaveRMQCSDetails = function (params) {
            let url = reportingDomain + 'savermqcsdetails';
            return httpServices.post(url, params);
        };

        //#endregion RM QCS

        //# OPEN PO AND OPEN PR START 

        reportingService.GetOpenPR = function (compid, plant, purchase, exclusion) {
            let url = reportingDomain + 'openpr?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&sessionid=' + userService.getUserToken() + '&exclusion=' + exclusion;
            return httpServices.get(url);
        };

        reportingService.GetOpenPRPivot = function (compid, plant, purchase, exclusion) {
            let url = reportingDomain + 'openprpivot?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&sessionid=' + userService.getUserToken() + '&exclusion=' + exclusion;
            return httpServices.get(url);
        };

        reportingService.GetOpenPO = function (compid, pono, plant, purchase, exclude) {
            let url = reportingDomain + 'openpo?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&pono=' + pono + '&sessionid=' + userService.getUserToken() + '&exclude=' + exclude;
            return httpServices.get(url);
        };

        reportingService.GetOpenPOPivot = function (compid, plant, purchase) {
            let url = reportingDomain + 'openpopivot?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetPoComments = function (pono, item, type) {
            let url = reportingDomain + 'openpocomments?pono=' + pono + '&itemno=' + item + '&type=' + type + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.UpdatePoStatus = function (params) {
            var url = reportingDomain + 'updatepostatus';
            return httpServices.post(url, params);
        };

        reportingService.SavePoComments = function (params) {
            var url = reportingDomain + 'savepocomments';
            return httpServices.post(url, params);
        };

        reportingService.GetSapAccess = function (userid) {
            let url = reportingDomain + 'sapaccess?userid=' + userid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetAvdAccess = function (userid) {
            let url = reportingDomain + 'avdaccess?userid=' + userid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetLastUpdatedDate = function (table) {
            let url = reportingDomain + 'lastupdatedate?table=' + table + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetOpenPOReport = function (compid) {
            let url = reportingDomain + 'openporeport?compid=' + compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetOpenPRReport = function (compid) {
            let url = reportingDomain + 'openprreport?compid=' + compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };


        reportingService.GetOpenPOShortageReport = function (compid, pono, plant, purchase, exclude) {
            let url = reportingDomain + 'getopenposhortagereport?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&pono=' + pono + '&sessionid=' + userService.getUserToken() + '&exclude=' + exclude;
            return httpServices.get(url);
        };

        reportingService.GetOpenPRShortageReport = function (compid, plant, purchase, exclusion) {
            let url = reportingDomain + 'getopenprshortagereport?compid=' + compid + '&plant=' + plant + '&purchase=' + purchase + '&sessionid=' + userService.getUserToken() + '&exclusion=' + exclusion;
            return httpServices.get(url);
        };

        reportingService.GetCompanySavingStats = function (params) {
            let url = reportingDomain + 'getcompanysavingstats?compid=' + params.compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.UpdateQCSSavings = function (params) {
            let url = reportingDomain + 'UpdateQCSSavings?dbname=' + params.dbname + '&compid=' + params.compid + '&sessionid=' + params.sessionid + '&type=' + params.type;
            return httpServices.get(url);
        };

        reportingService.saveQCSSAVINGS = function (params) {
            var url = reportingDomain + 'saveQCSSAVINGS';
            return httpServices.post(url, params);
        };
        
        //# OPEN PO AND OPEN PR END


        return reportingService;
    }]);prmApp

    .service('rfqService', ["rfqDomain", "userService", "httpServices", function (rfqDomain, userService, httpServices) {
        //var domain = 'http://182.18.169.32/services/';
        var rfqService = this;
        
        //storeService.savestore = function (params) {
        //    let url = storeDomain + 'savestore';
        //    return httpServices.post(url, params);
        //};
        rfqService.GetRFQCIJList = function (userid, sessionid) {

            var url = rfqDomain + 'getrfqcijlist?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        rfqService.GetRFQCreators = function (indentid) {

            var url = rfqDomain + 'getrfqcreators?indentid=' + indentid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        rfqService.GetRFQIndentList = function (userid, sessionid) {

            var url = rfqDomain + 'getrfqindentlist?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

       
        return rfqService;
    }]);prmApp

    .service('fileUpload', ['$http', function ($http) {
        this.uploadFileToUrl = function (file, uploadUrl) {
            var fd = new FormData();
            fd.append('file', file);

            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            })

                .success(function () {
                })

                .error(function () {
                });
        }
    }])

    // =========================================================================
    // Auction Details Services
    // =========================================================================
    .service('auctionsService', ["$http", "store", "$state", "$rootScope", "domain", "version", "httpServices",
        function ($http, store, $state, $rootScope, domain, version, httpServices) {
        //var domain = 'http://182.18.169.32/services/';
        var auctions = this;
        auctions.getdate = function () {
            var date = new Date();
            var time = date.getTime();
            return $http({
                method: 'GET',
                url: domain + 'getdate?time=' + time,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
                //data: params
            }).then(function (response) {
                var list = [];
                if (response && response.data) {
                    if (response.data.length > 0) {
                        list = response.data;
                    }
                    return list;
                } else {
                    //console.log(response.data[0].errorMessage);
                }
            }, function (result) {
                //console.log("date error");
            });
            }

            auctions.GetDateLogistics = function () {
                var date = new Date();
                var time = date.getTime();
                return $http({
                    method: 'GET',
                    url: domain + 'getdatelogistics?time=' + time,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                    //data: params
                }).then(function (response) {
                    var list = [];
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            list = response.data;
                        }
                        return list;
                    } else {
                        //console.log(response.data[0].errorMessage);
                    }
                }, function (result) {
                    //console.log("date error");
                });
            }

        auctions.GetServerDateTime = function () {
            let url = domain + 'getserverdatetime';
            return httpServices.get(url);
        };


        auctions.CheckSystemDateTime = function () {
            auctions.GetServerDateTime()
                .then(function (response) {

                    var d = new Date();

                    var year = new Date().getUTCFullYear();
                    var month = new Date().getUTCMonth();
                    month = month + 1;
                    var day = new Date().getUTCDate();
                    var hour = new Date().getUTCHours();
                    var min = new Date().getUTCMinutes();
                    var sec = new Date().getUTCSeconds();


                    var diffSec = response.SEC - sec;
                    if (diffSec >= 0) { }
                    else if (diffSec < 0) {
                        diffSec = -(diffSec);
                    } else {
                        diffSec = 0;
                    }

                    console.log('*******************Local*******************');
                    console.log(' LY: ' + year + ' SY: ' + response.YEAR +
                        ' LM: ' + month + ' SM: ' + response.MONTH +
                        ' LD: ' + day + ' SD: ' + response.DAY +
                        ' LH: ' + hour + ' SH: ' + response.HOUR +
                        ' LM: ' + min + ' SM: ' + response.MIN +
                        ' LS: ' + sec + ' SS: ' + response.SEC);
                    console.log('*******************Server*******************');

                    if (year !== response.YEAR ||
                        month !== response.MONTH ||
                        day !== response.DAY ||
                        hour !== response.HOUR ||
                        min !== response.MIN) {
                        swal({
                            title: "Please update system time correctly",
                            text: "thanks.",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#F44336",
                            confirmButtonText: "OK",
                            closeOnConfirm: true
                        }, function () {
                            location.reload();
                        });
                    }
                });
        };

        auctions.getPreviousItemPrice = function (itemDetails) {
            let url = domain + 'itempreviousprice?companyid=' + itemDetails.compID + '&productname=' + itemDetails.productIDorName.trim() + '&productno=' + itemDetails.productNo.trim() + '&brand=' + itemDetails.productBrand.trim() + '&sessionid=' + itemDetails.sessionID;
            return httpServices.get(url);
        };

        auctions.getIncotermProductConfig = function (incoTerm, sessionid) {
            let url = domain + 'getIncotermProductConfig?incoTerm=' + incoTerm.trim() + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.getVendorSAPDetails = function (params) {
            let url = domain + 'getvendorsapdetails?vendorid=' + params.vendorid + '&reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        auctions.isnegotiationended = function (reqid, sessionid) {
            let url = domain + 'isnegotiationended?reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.selectVendor = function (params) {
            var url = domain + 'selectvendor';
            return httpServices.post(url, params);
        };

        auctions.getauctions = function (params) {
            let url = domain + 'getrunningauctions?section=' + params.section + '&userid=' + params.userid + '&sessionid=' + params.sessionid + '&limit=' + params.limit;
            return httpServices.get(url);
        };

        auctions.rateVendor = function (params) {
            var url = domain + 'userratings';
            return httpServices.post(url, params);
        };

        auctions.vendorstatistics = function (uid, sessionid) {
            let url = domain + 'getvendorstatistics?userid=' + uid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.getrevisedquotations = function (params) {
            var url = domain + 'getrevisedquotations';
            return httpServices.post(url, params);
        };

        auctions.updateStatus = function (params) {
            var url = domain + 'updatestatus';
            return httpServices.post(url, params);
        };

        auctions.uploadquotationsfromexcel = function (params) {
            var url = domain + 'uploadquotationsfromexcel';
            return httpServices.post(url, params);
        };

        auctions.itemwiseselectvendor = function (params) {
            var url = domain + 'itemwiseselectvendor';
            return httpServices.post(url, params);
        };


        auctions.updatedeliverdate = function (params) {
            var url = domain + 'updateexpdelandpaydate';
            return httpServices.post(url, params);
        };

        auctions.reqTechSupport = function (params) {
            var url = domain + 'reqtechsupport';
            return httpServices.post(url, params);
        };

        auctions.getrequirementsreport = function (params) {
            var url = domain + 'getrequirementsreport?userid=' + params.userid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        }

        auctions.SaveRunningItemPrice = function (params) {
            var url = domain + 'saverunningitemprice';
            return httpServices.post(url, params);
        };

        auctions.updatepaymentdate = function (params) {
            var url = domain + 'updateexpdelandpaydate';
            return httpServices.post(url, params);
        };
            auctions.GetRequirementsByProductIds = function (params) {
                let url = domain + 'getrequirementsbyproductids?productids=' + params.productids + '&compid=' + params.compid + '&sessionid=' + params.sessionid;
                return httpServices.get(url);
            };

        auctions.StartNegotiation = function (params) {
            var url = domain + 'startNegotiation';
            return httpServices.post(url, params);
        };

        auctions.checkForRestartNegotiation = function (params) {
            var url = domain + 'checkForRestartNegotiation';
            return httpServices.post(url, params);
        };

        auctions.RestartNegotiation = function (params) {
            var url = domain + 'restartnegotiation';
            return httpServices.post(url, params);
        };

        auctions.DeleteVendorFromAuction = function (params) {
            var url = domain + 'deletevendorfromauction';
            return httpServices.post(url, params);
        };

        auctions.generatepo = function (params) {
            var data = {
                reqPO: params
            };
            var url = domain + 'generatepoforuser';
            return httpServices.post(url, data);
        };

        auctions.vendorreminders = function (params) {
            var url = domain + 'vendorreminders';
            return httpServices.post(url, params);
        };


        auctions.GetLastPrice = function (itemDetails) {
            let url = domain + 'getlastprice?productIDorName=' + itemDetails.productIDorName.trim() + '&companyid=' + itemDetails.compID + '&sessionid=' + itemDetails.sessionID;
            return httpServices.get(url);
        };


        auctions.GetUserDepartmentDesignations = function (userid, sessionid) {
            var url = domain + 'getuserdepartmentdesignations?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };
        

        auctions.SaveCompanyDepartment = function (params) {
            var url = domain + 'savecompanydepartment';
            return httpServices.post(url, params);
        };

        auctions.SaveCompanyDesignations = function (params) {
            var url = domain + 'savecompanydesignations';
            return httpServices.post(url, params);
        };
            auctions.saveAttachment = function (params) {
                var url = domain + 'savefile';
                return httpServices.post(url, params);
            };

            auctions.GetCompanyDeptDesigTypes = function (userID, type, sessionid) {
                return $http({
                    method: 'GET',
                    url: domain + 'getcompanydeptdesigtypes?userid=' + userID + '&type=' + type + '&sessionid=' + sessionid,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    var list = {};
                    if (response && response.data) {
                        return response.data;
                    }
                }, function (result) {
                    //console.log("date error");
                });
            };



        auctions.DeleteDepartment = function (params) {
            var url = domain + 'deletedepartment';
            return httpServices.post(url, params);
        };

        auctions.DeleteDesignation = function (params) {
            var url = domain + 'deletedesignation';
            return httpServices.post(url, params);
        };

        auctions.GetCompanyDepartments = function (userid, sessionid) {
            var url = domain + 'getcompanydepartments?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.GetCurrencyFactors = function (userid, sessionid, compid) {
            var url = domain + 'getcurrencyfactors?userid=' + userid + '&sessionid=' + sessionid + '&compid=' + compid;
            return httpServices.get(url);
        };

        auctions.GetCurrencyFactorAudit = function (currencycode, sessionid, compid) {
            var url = domain + 'getcurrencyfactoraudit?currencycode=' + currencycode + '&sessionid=' + sessionid + '&compid=' + compid;
            return httpServices.get(url);
        };

        auctions.SaveCurrencyFactor = function (params) {
            var url = domain + 'savecurrencyfactor';
            return httpServices.post(url, params);
        };

        auctions.GetCompanyDesignations = function (userid, sessionid) {
            var url = domain + 'getcompanydesignations?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.GetUserDepartments = function (userid, sessionid) {
            var url = domain + 'getuserdepartments?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.GetUserDesignations = function (userid, sessionid) {
            var url = domain + 'getuserdesignations?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };


        auctions.GetUserDeptDesig = function (userid, sessionid) {
            var url = domain + 'getuserdeptdesig?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.GetCompDeptDesig = function (compid, sessionid) {
            var url = domain + 'getcompdeptdesig?compid=' + compid + '&sessionid=' + sessionid;
            return httpServices.get(url);
            };

            auctions.GetAssignetTickets = function (sessionid) {
                let url = domain + 'GetAssignetTickets?sessionID=' + sessionid;
                return httpServices.get(url);
            };

            auctions.SaveAssignTickets = function (params) {
                let url = domain + 'SaveAssignTickets';
                return httpServices.post(url, params);

            };

        //auctions.SaveUserDeptDesig = function (params) {
        //    var url = domain + 'saveuserdeptdesig';
        //    return httpServices.post(url, params);
        //};


        auctions.SaveUserDepartmentDesignation = function (params) {
            var data = params;
            return $http({
                method: 'POST',
                url: domain + 'saveuserdepartmentdesignation',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: data
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    list = response.data;
                    return list;
                } else {

                }
            }, function (result) {
                //console.log(result);
            });
        }

        auctions.GetReqDeptDesig = function (userid, reqid, sessionid) {
            var url = domain + 'getreqdeptdesig?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.SaveReqDeptDesig = function (params) {
            var url = domain + 'savereqdeptdesig';
            return httpServices.post(url, params);
        };

        auctions.SaveUserDepartments = function (params) {
            var url = domain + 'saveuserdepartments';
            return httpServices.post(url, params);
        };

        auctions.GetDepartmentUsers = function (deptid, userid, sessionid) {
            var url = domain + 'getdepartmentusers?deptid=' + deptid + '&userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.GetReqDepartments = function (userid, reqid, sessionid) {
            let url = domain + 'getreqdepartments?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.SaveReqDepartments = function (params) {
            let url = domain + 'savereqdepartments';
            return httpServices.post(url, params);
        };

        auctions.shareemaillinkstoVendors = function (params) {
            let url = domain + 'shareemaillinkstoVendors';
            return httpServices.post(url, params);
        };

        auctions.GetSharedLinkVendors = function (params) {
            let url = domain + 'getSharedLinkVendors?userid=' + params.userID + '&vendorids=' + params.vendorIDs + '&sessionid=' + params.sessionID;
            return httpServices.get(url);
        };

        auctions.GetUnBlockedVendors = function (params) {
            let url = domain + 'getUnBlockedVendors?userid=' + params.userID + '&vendorids=' + params.vendorIDs + '&sessionid=' + params.sessionID;
            return httpServices.get(url);
        };

        auctions.saveVendorOtherCharges = function (params) {
            let url = domain + 'saveVendorOtherCharges';
            return httpServices.post(url, params);
        };

        auctions.saveQCSVendorAssignments = function (params) {
                let url = domain + 'saveQCSVendorAssignments';
            return httpServices.post(url, params);
        };

            auctions.getQCSVendorItemAssignments = function (params) {
                let url = domain + 'getqcsvendoritemassignments?reqid=' + params.reqid + '&userid=' + params.userid + '&qcsid=' + params.qcsid + '&sessionid=' + params.sessionid;
            return httpServices.get(url, params);
        };

        auctions.AssignVendorToCompany = function (params) {
            let url = domain + 'assignvendortocompany';
            return httpServices.post(url, params);
        };

        auctions.saveVendorOtherCharges = function (params) {
            let url = domain + 'saveVendorOtherCharges';
            return httpServices.post(url, params);
        };

        auctions.GetCompanyConfiguration = function (compid, configkey, sessionid) {
            let url = domain + 'getcompanyconfiguration?compid=' + compid + '&configkey=' + configkey + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.getRequirementVendorCodes = function (reqid, sessionid) {
            let url = domain + 'getrequirementvendorcodes?reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.getRequirementVendorCodes1 = function (reqid, sessionid) {
            let url = domain + 'getrequirementvendorcodes1?reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.SearchRequirements = function (params) {
            params.excludeprlinked = params.excludeprlinked ? true : false;  
            let url = domain + 'searchrequirements?search=' + params.search + '&excludeprlinked=' + params.excludeprlinked + '&compid=' + params.compid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        auctions.GetCompanyConfigurationForLogin = function (compid, configkey, sessionid) {
            let url = domain + 'getcompanyconfigurationforlogin?compid=' + compid + '&configkey=' + configkey + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.GetCompanyCurrencies = function (compid, sessionid) {
            let url = domain + 'companycurrencies?compid=' + compid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.getCompanyPlants = function (compID, userID, sessionID) {
            let url = domain + 'getCompanyPlants?compID=' + compID + '&uID=' + userID + '&sessionID=' + sessionID;
            return httpServices.get(url);
        };

        auctions.GetPreferredLogin = function () {
            let url = domain + 'GetPreferredLogin';
            return httpServices.get(url);
        };

        auctions.GetIsAuthorized = function (userid, reqid, sessionid) {
            let url = domain + 'getisauthorized?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.materialdispatch = function (params) {
            var data = {
                reqMat: params
            };
            let url = domain + 'materialdispatch';
            return httpServices.post(url, data);
        };

        auctions.materialReceived = function (params) {
            var data = {
                materialreceived: params
            };
            let url = domain + 'materialreceived';
            return httpServices.post(url, data);
        };

        auctions.paymentdetails = function (params) {
            var data = {
                paymentDets: params
            };
            let url = domain + 'paymentdetails';
            return httpServices.post(url, data);
        };

        auctions.generatePOinServer = function (params) {
            let url = domain + 'generatepo';
            return httpServices.post(url, params);
        };
        auctions.getDashboardStatsNew = function (params) {
            let url = domain + 'getdashboardstats1?userid=' + params.userid + '&sessionid=' + params.sessionid + '&fromdate=' + params.fromDate + '&todate=' + params.toDate/* + '&depts='*/ /*+ params.filterDepts*/;
            return httpServices.get(url);
        };

        auctions.getDashboardStats = function (params) {
            let url = domain + 'getdashboardstats?userid=' + params.userid + '&sessionid=' + params.sessionid + '&fromdate=' + params.fromDate + '&todate=' + params.toDate/* + '&depts='*/ /*+ params.filterDepts*/;
            return httpServices.get(url);
        };
          
        auctions.getCustomerDashboard = function (params) {
            let url = domain + 'getcustomerdashboard?userid=' + params.userid + '&sessionid=' + params.sessionid + '&fromdate=' + params.fromDate + '&todate=' + params.toDate/* + '&depts='*/ /*+ params.filterDepts*/;
            return httpServices.get(url);
        };

        auctions.getDashboardIndicators = function (params) {
            let url = domain + 'getDashboardIndicators?userId=' + params.userId + '&compId=' + params.compId + '&deptId=' + params.deptId + '&sessionId=' + params.sessionId + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate;
            return httpServices.get(url);
        };

        auctions.getCategories = function (params) {
            let url = domain + 'getcategories?userid=' + (params ? params : -1);
            return httpServices.get(url);
        };

        auctions.getKeyValuePairs = function (params, compid) {
            if (!compid) {
                compid = 0;
            }

            let url = domain + 'getkeyvaluepairs?parameter=' + params + '&compid=' + compid;
            return httpServices.get(url);
        };

            //auctions.getmyAuctions = function (params) {
            //    if (!params.isRFP) {
            //        params.isRFP = false;
            //    }

            //    let url = domain + 'getmyauctions?userid=' + params.userid + '&page=' + params.page + '&limit=' + params.limit + '&reqstatus=' + params.reqstatus + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate + '&onlyrfq=' + params.isRFP + '&sessionid=' + params.sessionid;
            //return httpServices.get(url);
            //};

            auctions.getmyAuctions = function (params) {
                if (!params.onlyrfq) {
                    params.onlyrfq = 0;
                }

                if (!params.onlyrfp) {
                    params.onlyrfp = 0;
                }
                let url = domain + 'getmyauctions?userid=' + params.userid + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate + '&status=' + params.status + '&search=' + params.search + '&allBuyer=' + params.allBuyer + '&page=' + params.page + '&pagesize=' + params.pagesize + '&sessionid=' + params.sessionid + '&onlyrfq=' + params.onlyrfq + '&onlyrfp=' + params.onlyrfp;
                return httpServices.get(url);
            };


        auctions.downloadQCS = function (params) {
            let url = domain + 'downloadQCS?compId=' + params.compId + '&sessionId=' + params.sessionId;
            return httpServices.get(url);
        };

        auctions.getactiveleads = function (params) {
            let url = domain + 'getactiveleads?userid=' + params.userid + '&page=' + params.page + '&limit=' + params.limit + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        auctions.getrequirementdata = function (params) {
            let url = domain + 'getrequirementdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        auctions.getReportrequirementdata = function (params) {
            let url = domain + 'getreportrequirementdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid + "&excludePriceCap=" + params.excludePriceCap;
            return httpServices.get(url);
        };

        auctions.getLPPByProductIds = function (params) {
            let url = domain + 'getlppbyproductids?productids=' + params.productids + "&ignorereqid=" + params.ignorereqid + "&compid=" + params.compid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        auctions.GetReportsRequirement = function (params) {
            let url = domain + 'getreportsrequirement?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        auctions.GetVendorReminders = function (params) {
            let url = domain + 'getvendorreminders?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        auctions.GetBidHistory = function (params) {
            let url = domain + 'getbidhistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

            auctions.GetItemBidHistory = function (params) {
                let url = domain + 'GetItemBidHistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
                return httpServices.get(url);
            };

        auctions.getpodetails = function (params) {
            let url = domain + 'getpodetails?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        auctions.getcomments = function (params) {
            let url = domain + 'getcomments?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        auctions.getpricecomparison = function (params) {
            let url = domain + 'getpricecomparison?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        auctions.GetPriceComparisonPreNegotiation = function (params) {
            let url = domain + 'getpricecomparisonprenegotiation?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        auctions.savecomment = function (params) {
            var comment = {
                "com": {
                    "requirementID": params.reqID,
                    "firstName": "",
                    "lastName": "",
                    "userID": params.userID,
                    "commentText": params.commentText,
                    "replyCommentID": -1,
                    "commentID": -1,
                    "errorMessage": "",
                    "sessionID": params.sessionID
                }
            };

            let url = domain + 'savecomment';
            return httpServices.post(url, comment);
        };


        auctions.deleteAttachment = function (Attaachmentparams) {
            let url = domain + 'deleteattachment';
            return httpServices.post(url, Attaachmentparams);
        };


        auctions.postrequirementdata = function (params) {
            var myDate = new Date(); // Your timezone!
            var myEpoch = parseInt(myDate.getTime() / 1000);
            params.minBidAmount = 0;
            params.postedOn = "/Date(" + myEpoch + "000+0000)/";
            params.timeLeft = -1;
            params.price = -1;
            var requirement = {
                "requirement": {
                    "title": params.title,
                    "reqType": params.isRFP ? 2 : 1,
                    "description": params.description,
                    "category": params.category,
                    "subcategories": params.subcategories,
                    "urgency": params.urgency,
                    "budget": params.budget,
                    "attachmentName": params.attachmentName,
                    "deliveryLocation": params.deliveryLocation,
                    "taxes": params.taxes,
                    "paymentTerms": params.paymentTerms,
                    "lotId": params.lotId,
                    "requirementID": params.requirementID,
                    "customerID": params.customerID,
                    "isClosed": params.isClosed,
                    "endTime": null,
                    "sessionID": params.sessionID,
                    "errorMessage": "",
                    "timeLeft": -1,
                    "price": -1,
                    "auctionVendors": params.auctionVendors,
                    "startTime": null,
                    "status": "",
                    "postedOn": "/Date(" + myEpoch + "000+0000)/",
                    "custLastName": params.customerLastname,
                    "custFirstName": params.customerFirstname,
                    "deliveryTime": params.deliveryTime,
                    "includeFreight": params.includeFreight,
                    "inclusiveTax": params.inclusiveTax,
                    "minBidAmount": 0,
                    "checkBoxEmail": params.checkBoxEmail,
                    "checkBoxSms": params.checkBoxSms,
                    "quotationFreezTime": params.quotationFreezTime,
                    "currency": params.currency,
                    "timeZoneID": params.timeZoneID,
                    "listRequirementItems": params.listRequirementItems,
                    "isTabular": params.isTabular,
                    "reqComments": params.reqComments,
                    "itemsAttachment": params.itemsAttachment,
                    "isSubmit": params.isSubmit,
                    "isQuotationPriceLimit": params.isQuotationPriceLimit,
                    "quotationPriceLimit": params.quotationPriceLimit,
                    "noOfQuotationReminders": params.noOfQuotationReminders,
                    "remindersTimeInterval": params.remindersTimeInterval,
                    "custCompID": params.custCompID,
                    "customerEmails": params.customerEmails,
                    "customerCompanyName": params.customerCompanyName,
                    "deleteQuotations": params.deleteQuotations,
                    "indentID": params.indentID,
                    "expStartTime": params.expStartTime,
                    "cloneID": params.cloneID,
                    "isDiscountQuotation": params.isDiscountQuotation,
                    "contactDetails": params.contactDetails,
                    "generalTC": params.generalTC,
                    "isRevUnitDiscountEnable": params.isRevUnitDiscountEnable,
                    "multipleAttachments": params.multipleAttachments,
                    "contractStartTime": params.contractStartTime,
                    "contractEndTime": params.contractEndTime,
                    "isContract": params.isContract,
                    "prCode": params.prCode,
                    "PR_ID": params.PR_ID,
                    "PR_ITEM_IDS": params.selectedPRItemIds,
                    "isTechScoreReq": params.isTechScoreReq,
                    "auditComments": params.auditComments,
                    "biddingType": params.biddingType,
                    "templateId": params.templateId,
                    "projectName": params.projectName,
                    "projectDetails": params.projectDetails,
                    "prNumbers": params.PR_NUMBERS
                }, "attachment": params.attachment
            };

            let url = domain + 'requirementsave';
            return httpServices.post(url, requirement);
        };

        auctions.saveRequirementItemList = function (params) {
            let url = domain + 'saverequirementitemlist';
            return httpServices.post(url, params);
        };

        auctions.endNegotiation = function (params) {
            let url = domain + 'endnegotiation';
            return httpServices.post(url, params);
        };

        auctions.IsValidQuotationUpload = function (params) {
            let url = domain + 'isvalidquotationupload?reqid=' + params.reqId + '&sessionid=' + params.sessionid;
            return httpServices.get(url, params);
        };

        auctions.getCompanyGSTInfo = function (params) {
            let url = domain + 'getcompanygstinfo?compid=' + params.companyId + '&sessionid=' + params.sessionid;
            return httpServices.get(url, params);
        };

        auctions.saveCompanyGSTInfo = function (params) {
            let url = domain + 'savecompanygstinfo';
            return httpServices.post(url, params);
        };

        auctions.deleteGSTInfo = function (params) {
            let url = domain + 'deletecompanygstinfo';
            return httpServices.post(url, params);
        };

        auctions.updatebidtime = function (params) {
            let url = domain + 'updatebidtime';
            return httpServices.post(url, params);
        };

        auctions.makeabid = function (params) {
            let url = domain + 'makeabid';
            return httpServices.post(url, params);
        };

        auctions.uploadQuotation = function (params) {
            let url = domain + 'uploadquotation';
            return httpServices.post(url, params);
        };


        auctions.UploadQuotationForSelectVendorCeilingPrices = function (params) {
            let url = domain + 'uploadQuotationForSelectVendorCeilingPrices';
            return httpServices.post(url, params);
        };

        auctions.revquotationupload = function (params) {
            let url = domain + 'revquotationupload';
            return httpServices.post(url, params);
        };

        auctions.QuatationAprovel = function (params) {
            let url = domain + 'quatationaprovel';
            return httpServices.post(url, params);
        };

        auctions.ItemQuotationApproval = function (params) {
            let url = domain + 'itemquotationapproval';
            return httpServices.post(url, params);
        };

        auctions.ItemLevelQuotationApproval = function (params) {
            let url = domain + 'itemlevelquotationapproval';
            return httpServices.post(url, params);
        };

        auctions.SendQuotationApprovalStatusEmail = function (params) {
            let url = domain + 'sendquotationapprovalstatusemail';
            return httpServices.post(url, params);
        };

        auctions.getRequirementSettings = function (params) {
            let url = domain + 'requirementsettings?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
         };

        auctions.getRequirementVendorAnalysis = function (params) {
            let url = domain + 'requirementvendoranalysis?vendorids=' + params.vendorids + '&itemids=' + params.itemids + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        auctions.validateCurrencyRate = function (params) {
            let url = domain + 'validatecurrencyrate?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        auctions.refreshRequirementCurrency = function (params) {
            let url = domain + 'refreshrequirementcurrency?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        auctions.saveRequirementSetting = function (params) {
            let url = domain + 'saverequirementsetting';
            return httpServices.post(url, params);
        };

        auctions.toggleCounterBid = function (params) {
            let url = domain + 'togglecounterbid';
            return httpServices.post(url, params);
        };

        auctions.SaveDifferentialFactor = function (params) {
            let url = domain + 'savedifferentialfactor';
            return httpServices.post(url, params);
        };

        auctions.savepricecap = function (params) {
            let url = domain + 'savepricecap';
            return httpServices.post(url, params);
        };

        auctions.GetCompanyLeads = function (params) {
            let url = domain + 'getcompanyleads?userid=' + params.userid + '&searchstring=' + params.searchstring + '&searchtype=' + params.searchtype + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        auctions.updateauctionstart = function (params) {
            let url = domain + 'updateauctionstart';
            return httpServices.post(url, params);
        };

        auctions.SaveReqNegSettings = function (params) {
            let url = domain + 'SaveReqNegSettings';
            return httpServices.post(url, params);
        };

        auctions.GetMaterialReceivedData = function (params) {
            let url = domain + 'getmaterialreceiveddata?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        auctions.SaveCompanyCategories = function (params) {
            let url = domain + 'savecompanycategories';
            return httpServices.post(url, params);
        };

        auctions.GetCompanyCategories = function (compID, sessionID) {
            let url = domain + 'getcompanycategories?compid=' + compID + '&sessionid=' + sessionID;
            return httpServices.get(url);
        };

        auctions.newDetails = function (params) {
            let url = domain + 'savealternatecontacts';
            return httpServices.post(url, params);
        };

        auctions.getalternatecontacts = function (userid, compid, sessionID) {
            let url = domain + 'getalternatecontacts?userid=' + userid + '&compid=' + compid + '&sessionid=' + sessionID;
            return httpServices.get(url);
        };

        auctions.SaveRequirementTerms = function (params) {
            let url = domain + 'saverequirementterms';
            return httpServices.post(url, params);
        };

        auctions.GetRequirementTerms = function (userid, reqid, sessionID) {
            let url = domain + 'getrequirementterms?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionID;
            return httpServices.get(url);
        };

        auctions.DeleteRequirementTerms = function (params) {
            let url = domain + 'deleterequirementterms';
            return httpServices.post(url, params);
        };

        /*auctions.saveCIJ = function (params) {
            let url = domain + 'savecij';
            return httpServices.post(url, params);
        };

        auctions.GetCIJ = function (cijid, sessionid) {
            let url = domain + 'getcij?cijid=' + cijid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.GetIndentDetails = function (indentID, sessionid) {
            let url = domain + 'getindentdetails?indentid=' + indentID + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.SaveIndentDetails = function (params) {
            let url = domain + 'saveindentdetails';
            return httpServices.post(url, params);
        };

        auctions.GetCIJList = function (compid, sessionid) {
            let url = domain + 'getcijlist?compid=' + compid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.GetIndentList = function (compid, sessionid) {
            let url = domain + 'getindentlist?compid=' + compid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.saveFileUpload = function (params) {
            let url = domain + 'savefileupload';
            return httpServices.post(url, params);
        }; */

        auctions.GetUserDetails = function (userid, sessionid) {
            let url = domain + 'getuserinfo?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.UpdatePriceCap = function (params) {
            let url = domain + 'updatepricecap';
            return httpServices.post(url, params);
        };


        auctions.ResetPasswordByOTP = function (phone) {
            return $http({
                method: 'GET',
                url: domain + 'resetpasswordbyotp?phone=' + phone,
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                var list = {};
                if (response && response.data) {
                    return response.data;
                }
            }, function (result) {
            });
        };

        auctions.EnableMarginFields = function (params) {
            var url = domain + 'enablemarginfields';
            return httpServices.post(url, params);
        };

        auctions.GetAltUsers = function (clientid, vendorid, sessionid) {
            let url = domain + 'getaltusers?clientid=' + clientid + '&vendorid=' + vendorid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.uploadclientsidequotation = function (params) {
            var url = domain + 'uploadclientsidequotation';
            return httpServices.post(url, params);
        };

        auctions.uploadRequirementItemsSaveExcel = function (params) {
            var url = domain + 'uploadrequirementitemssaveexcel';
            return httpServices.post(url, params);
        };

        auctions.UploadRequirementItemsCeilingSave = function (params) {
            var url = domain + 'uploadrequirementitemsceilingsave';
            return httpServices.post(url, params);
        };

        auctions.getOTP = function (phone) {
            let url = domain + 'getotp?phone=' + phone;
            return httpServices.get(url);
        };

        auctions.UpdatePassword = function (params) {
            var url = domain + 'updatepassword';
            return httpServices.post(url, params);
        };


             auctions.GetItemsList = function (moduleid, module, sessionid) {
                    let url = domain + 'getitemslist?moduleid=' + moduleid + '&module=' + module + '&sessionid=' + sessionid;
                    return httpServices.get(url);
                };


        auctions.GetCompanyDeptDesigTypes = function (userID, type, sessionid) {
            let url =  domain + 'getcompanydeptdesigtypes?userid=' + userID + '&type=' + type + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.GetWorkCategories = function (userid, sessionid) {
            let url = domain + 'getworkcategories?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.SaveWorkCategory = function (params) {
            var url = domain + 'saveworkcategory';
            return httpServices.post(url, params);
        };

        auctions.GetBudgetCodes = function (userid, sessionid) {
           
                let url = domain + 'getbudgetcodes?userid=' + userid + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };


            auctions.SaveBudgetCode = function (params) {

                var url = domain + 'savebudgetcode';
                return httpServices.post(url, params);
        };

        auctions.GetUserDetailsByDeptDesigTypeID = function (compid, depttypeid, desigtypeid, sessionid) {
            let url = domain + 'getuserdetailsbydeptdesigtypeid?compid=' + compid + '&depttypeid=' + depttypeid + '&desigtypeid=' + desigtypeid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };





        auctions.SaveCBPrices = function (params) {
            var url = domain + 'savecbprices';
            return httpServices.post(url, params);
        };

        auctions.SaveCBPricesCustomer = function (params) {
            var url = domain + 'savecbpricescustomer';
            return httpServices.post(url, params);
        };

        auctions.FreezeCounterBid = function (params) {
            var url = domain + 'freezecounterbid';
            return httpServices.post(url, params);
        };

        auctions.GetCBPricesAudit = function (reqid, vendorid, sessionid) {
            let url = domain + 'getcbpricesaudit?reqid=' + reqid + '&vendorid=' + vendorid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        auctions.UpdateCBTime = function (params) {
            var url = domain + 'updatecbtime';
            return httpServices.post(url, params);
        };

        auctions.CBStopQuotations = function (params) {
            var url = domain + 'cbstopquotations';
            return httpServices.post(url, params);
        };

        auctions.CBMarkAsComplete = function (params) {
            var url = domain + 'cbmarkascomplete';
            return httpServices.post(url, params);
        };

        auctions.markAsCompleteREQ = function (params) {
            var url = domain + 'markAsCompleteREQ';
            return httpServices.post(url, params);
        };

            auctions.GetEmailLogs = function (fromDate, toDate,companyID, sessionId) {
                var url = domain + 'getemaillogs?from=' + fromDate + '&to=' + toDate + '&compid=' + companyID + '&sessionid=' + sessionId;
                return httpServices.get(url);
            };

            auctions.GetSMSLogs = function (fromDate, toDate,companyID, sessionId) {
                var url = domain + 'getsmslogs?from=' + fromDate + '&to=' + toDate + '&compid=' + companyID + '&sessionid=' + sessionId;
                return httpServices.get(url);
            };

            auctions.GetCompanyidByVendorid = function (ID) {
                var url = domain + 'getcompanyidbyvendorid?id=' + ID;
                return httpServices.get(url);
            };

            auctions.GetActiveUsers = function (companyID, sessionID) {
                var url = domain + 'getactiveusers?compid=' + companyID + '&sessionid=' + sessionID;
                return httpServices.get(url);
            };

            auctions.GetActiveBuyers = function (userID, sessionID) {
                var url = domain + 'getactivebuyers?userid=' + userID + '&sessionid=' + sessionID;
                return httpServices.get(url);
            };

            auctions.GetUsersLoginStatus = function (userID, fromDate, toDate, sessionID) {
                var url = domain + 'getusersloginstatus?userid=' + userID + '&from=' + fromDate + '&to=' + toDate + '&sessionid=' + sessionID;
                return httpServices.get(url);
            };

            auctions.getcatalogCategories = function (uid) {
                let url = domain + 'getCatalogCategories?uid=' + uid;
                return httpServices.get(url);
            };

            auctions.downloadActiveUserTemplate = function (template, userID, fromDate, toDate, sessionID) {
                let url = domain + 'getactiveuserstemplates?template=' + template + '&userid=' + userID + '&from=' + fromDate + '&to=' + toDate + '&sessionid=' + sessionID;
                let data = httpServices.get(url).then(function (response) {
                    if (response) {
                        response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                        var linkElement = document.createElement('a');
                        try {
                            //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                            var url = $window.URL.createObjectURL(response);

                            linkElement.setAttribute('href', url);
                            linkElement.setAttribute("download", template + ".xlsx");

                            var clickEvent = new MouseEvent("click", {
                                "view": window,
                                "bubbles": true,
                                "cancelable": false
                            });
                            linkElement.dispatchEvent(clickEvent);
                        }
                        catch (ex) { }
                        return response;
                    }
                }, function (result) {
                    //console.log("date error");
                });
            };

            function b64toBlob(b64Data, contentType, sliceSize) {
                contentType = contentType || '';
                sliceSize = sliceSize || 512;

                var byteCharacters = atob(b64Data);
                var byteArrays = [];

                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

                var blob = new Blob(byteArrays, { type: contentType });
                return blob;
            }

            auctions.saveReductionLevelSetting = function (params) {
                let url = domain + 'saveReductionLevelSetting';
                return httpServices.post(url, params);
            };

            auctions.getReductionSettings = function (params) {
                let url = domain + 'getReductionSettings?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
                return httpServices.get(url);
            };

            auctions.getVendorCodes = function (params) {
                let url = domain + 'getvendorcodes?uid=' + params.uid + "&compid=" + params.compid + "&siteCodes=" + params.siteCodes + "&sessionid=" + params.sessionid;
                return httpServices.get(url);
            };

        return auctions;



    }])


    // =========================================================================
    // Header Messages and Notifications list Data
    // =========================================================================

    .service('messageService', ['$resource', function ($resource) {

    }])

    //==============================================
    // BOOTSTRAP GROWL
    //==============================================

    .service('growlService', function () {
        var gs = {};
        gs.growl = function (message, type, position) {

            var _position = {
                from: 'top',
                align: 'right'
            }

            if (position !== undefined) {
                _position = position;
            }

            $.growl({
                message: message
            }, {
                type: type,
                allow_dismiss: false,
                label: 'Cancel',
                className: 'btn-xs btn-inverse',
                placement: _position,
                delay: 2500,
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                },
                offset: {
                    x: 20,
                    y: 85
                }
            });
        };

        return gs;
    })

    .service('authService', ["$http", "store", "$state", "$rootScope", "domain", "version", 'httpServices', function ($http, store, $state, $rootScope, domain, version, httpServices) {
        var authService = this;
        var responseObj = {};
        responseObj.objectID = 0;
        authService.isValidSession = function (sessionid) {
            let url = domain + 'issessionvalid?sessionid=' + sessionid;
            return httpServices.get(url);
        };

        

        return authService;
    }])

    .service("SettingService", ["$http", "store", "$state", "$rootScope", "settingDomian", "version", 'httpServices', function ($http, store, $state, $rootScope, settingDomian, version, httpServices) {


        var _settingService = this;
        _settingService.getRegistrationFields = function () {
            return httpServices.get(settingDomian + "settings/get_registration_setup")
        }


        _settingService.insertRegistrationField = function (data) {
            return httpServices.post(settingDomian + "settings/insert_reg_field", data)
        }

        _settingService.updateRegistrationField = function (data) {
            return httpServices.post(settingDomian + "settings/update_reg_field", data)
        }

        _settingService.deleteRegistrationField = function (id) {
            return httpServices.post(settingDomian + "settings/delete_reg_field/", { id: id })
        }


        _settingService.getRequirementFields = function () {
            return httpServices.get(settingDomian + "settings/get_requirement_setup")
        }


        _settingService.insertRequirementField = function (data) {
            return httpServices.post(settingDomian + "settings/insert_requirement_field", data)
        }

        _settingService.updateRequirementField = function (data) {
            return httpServices.post(settingDomian + "settings/update_requirement_field", data)
        }

        _settingService.deleteRequirementField = function (id) {
            return httpServices.post(settingDomian + "settings/delete_requirement_field/", { id: id })
        }

    }])
prmApp

    .service('storeService', ["store", "storeDomain", "userService", "httpServices", function (store, storeDomain, userService, httpServices) {
        //var domain = 'http://182.18.169.32/services/';
        var storeService = this;
        
        storeService.savestore = function (params) {
            let url = storeDomain + 'savestore';
            return httpServices.post(url, params);
        };

        storeService.deletestore = function (params) {
            let url = storeDomain + 'deletestore';
            return httpServices.post(url, params);
        };

        storeService.deleteStoreItemDetails = function (params) {
            let url = storeDomain + 'deletestoreitemdetails';
            return httpServices.post(url, params);
        };

        storeService.getcompanystores = function (compID, parentStoreID) {
            let url = storeDomain + 'companystores?compid=' + compID + '&parentstoreid=' + parentStoreID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.getstores = function (storeID, compID) {
            let url = storeDomain + 'stores?storeid=' + storeID + '&compid' + compID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.savestoreitem = function (params) {
            let url = storeDomain + 'savestoreitem';
            return httpServices.post(url, params);
        };

        storeService.deletestoreitem = function (params) {
            let url = storeDomain + 'deletestoreitem';
            return httpServices.post(url, params);
        };

        storeService.getuniqueitems = function (compID, itemName) {
            let url = storeDomain + 'uniqueitems?compid=' + compID + '&itemname=' + itemName + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.getcompanystoreitems = function (storeID, itemID) {
            let url = storeDomain + 'storeitems?storeid=' + storeID + '&itemid=' + itemID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.importentity = function (params) {
            let url = storeDomain + 'importentity';
            return httpServices.post(url, params);
        };
        
        storeService.storesitempropvalues = function (itemID) {
            let url = storeDomain + 'companypropvalues?itemid=' + itemID + '&module=STORE&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.savecompanyprop = function (params) {
            let url = storeDomain + 'savecompanyprop';
            return httpServices.post(url, params);
        };

        storeService.savecompanypropvalue = function (params) {
            let url = storeDomain + 'savecompanypropvalue';
            return httpServices.post(url, params);
        };

        storeService.companypropvalues = function (itemID) {
            let url = storeDomain + 'companypropvalues?entityid=' + itemID + '&module=STORE&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.getStoreItemDetails = function (itemID, notDispatched) {
            let url = storeDomain + 'getstoreitemdetails?itemid=' + itemID + '&notdispatched=' + notDispatched + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.saveStoreItemDetails = function (params) {
            let url = storeDomain + 'savestoreitemdetails';
            return httpServices.post(url, params);
        };

        storeService.saveStoreItemGIN = function (params) {
            let url = storeDomain + 'savestoreitemissue';
            return httpServices.post(url, params);
        };

        storeService.saveStoreItemGRN = function (params) {
            let url = storeDomain + 'savestoreitemreceive';
            return httpServices.post(url, params);
        };

        storeService.getStoreItemGIN = function (ginCode, storeID) {
            let url = storeDomain + 'storeitemissuedetails?gincode=' + ginCode + '&storeid=' + storeID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.getStoreItemGRN = function (grnCode, storeID) {
            let url = storeDomain + 'storeitemreceivedetails?grncode=' + grnCode + '&storeid=' + storeID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        storeService.getIndentItemDetails = function (storeID, ginCode, grnCode) {
            let url = storeDomain + 'indentitemdetails?storeid=' + storeID + '&gincode=' + ginCode + '&grncode=' + grnCode + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };
        
        return storeService;
    }]);prmApp
    .service('techevalService', ["techevalDomain", "userService", "httpServices",
        function (techevalDomain, userService, httpServices) {
            //var domain = 'http://182.18.169.32/services/';
            var techevalService = this;

            techevalService.getquestionnairelist = function (evalID) {
                var url = techevalDomain + 'getquestionnairelist?userid=' + userService.getUserId() + '&evalid=' + evalID + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.savequestionnaire = function (params) {
                let url = techevalDomain + 'savequestionnaire';
                return httpServices.post(url, params);
            };

            techevalService.assignquestionnaire = function (params) {
                let url = techevalDomain + 'assignquestionnaire';
                return httpServices.post(url, params);
            };

            techevalService.savequestion = function (params) {
                let url = techevalDomain + 'savequestion';
                return httpServices.post(url, params);
            };

            techevalService.deletequestion = function (params) {
                let url = techevalDomain + 'deletequestion';
                return httpServices.post(url, params);
            };

            techevalService.importentity = function (params) {
                let url = techevalDomain + 'importentity';
                return httpServices.post(url, params);
            };

            techevalService.getquestionnaire = function (evalid, loadquestions) {
                var url = techevalDomain + 'getquestionnaire?evalid=' + evalid + '&loadquestions=' + loadquestions + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.getreqquestionnaire = function (reqid, loadquestions) {
                var url = techevalDomain + 'gettechevalforrequirement?reqid=' + reqid + '&loadquestions=' + loadquestions + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.saveresponse = function (params) {
                let url = techevalDomain + 'saveresponse';
                return httpServices.post(url, params);
            };

            techevalService.saveTechApproval = function (params) {
                let url = techevalDomain + 'saveresponse';
                return httpServices.post(url, params);
            };

            techevalService.getresponses = function (reqid, evalid, userid) {
                var url = techevalDomain + 'getresponses?evalid=' + evalid + '&userid=' + userid + '&reqid=' + reqid + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.GetTechEvalution = function (evalid, reqid, userid) {
                let url = techevalDomain + 'gettechevalution?evalid=' + evalid + '&reqid=' + reqid + '&userid=' + userid + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.GetVendorsForTechEval = function (evalid) {
                let url = techevalDomain + 'getvendorsfortecheval?evalid=' + evalid + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            techevalService.saveTechApproval = function (params) {
                let url = techevalDomain + 'savetechevaluation';
                return httpServices.post(url, params);
            };

            techevalService.saveVendorsForQuestionnaire = function (params) {
                let url = techevalDomain + 'savevendorforquestionnaire';
                return httpServices.post(url, params);
            };

            techevalService.deletequestionnaire = function (params) {
                let url = techevalDomain + 'deletequestionnaire';
                return httpServices.post(url, params);
            };

            techevalService.sendquestionnairetovendors = function (params) {
                let url = techevalDomain + 'sendquestionnairetovendors';
                return httpServices.post(url, params);
            };

            return techevalService;
        }]);angular.module("prm.user").service('userService', userService);


function userService($http, store, $state, $rootScope, $q, version, domain, growlService, vendorDomian, httpServices, auctionsService, reportingDomain) {
    //var domain = 'http://182.18.169.32/services/';
    var self = this;
    self.myCatalog = [];
    var successMessage = '';
    self.companyFieldTemplates = [];
    $rootScope.featureCodeHide = true;

    self.userData = { currentUser: null };

    self.getUserObj = function () {
        if (!self.userData.currentUser || !self.userData.currentUser.id) {
            self.userData.currentUser = store.get('currentUser');
        }

        $rootScope.companyRoundingDecimalSetting = (self.userData.currentUser || {}).companyRoundingDecimalSetting;
        $rootScope.companyRoundingDecimalSetting = ($rootScope.companyRoundingDecimalSetting || 3);
        return self.userData.currentUser || {};
    };

    self.getUserToken = function () {
        return store.get('sessionid');
    };

    self.getUserHashToken = function () {
        return store.get('userhashtoken');
    };

    self.getJWTToken = function () {
        return store.get('authentication');
    };

    self.getLocalEntitlement = function () {
        return store.get('localEntitlement');
    };

    self.removeLocalEntitlement = function () {
        return store.remove('localEntitlement');
    };

    self.getLocalDeptDesigt = function () {
        return store.get('localDeptDesig');
    };

    self.removeLocalDeptDesig = function () {
        return store.remove('localDeptDesig');
    };

    self.getListUserDepartmentDesignations = function () {
        return store.get('ListUserDepartmentDesignations');
    };

    self.removeListUserDepartmentDesignations = function () {
        return store.remove('ListUserDepartmentDesignations');
    };

    self.getSelectedUserDepartmentDesignation = function () {
        return store.get('SelectedUserDepartmentDesignation');
    };

    self.removeSelectedUserDepartmentDesignation = function () {
        return store.remove('SelectedUserDepartmentDesignation');
    };

    self.getSelectedUserDeptID = function () {
        return store.get('SelectedUserDepartmentDesignation').deptID;
    };

    self.getSelectedUserDesigID = function () {

        var desigs = store.get('SelectedUserDepartmentDesignation').listDesignation;

        desigs = desigs.filter(function (d) {
            return (d.isAssignedToUser == true && d.isValid == true)
        });

        return desigs[0].desigID;

    };


    self.getOtpVerified = function () {
        return self.getUserObj().isOTPVerified;
    };

    self.getEmailOtpVerified = function () {
        return self.getUserObj().isEmailOTPVerified;
    };

    self.reloadProfileObj = function () {

    };

    self.getDecimalRoundingSetting = function () {
        return self.getUserObj().companyRoundingDecimalSetting;
    };

    self.getDocsVerified = function () {
        return self.getUserObj().credentialsVerified;
    };
    self.removeUserToken = function () {
        store.remove('sessionid');
    };
    self.removeUserHashToken = function () {
        store.remove('userhashtoken');
    };
    self.removeJWTToken = function () {
        store.remove('authentication');
    };    
    self.getRememberMeToken = function () {
        return store.get('rememberMeToken');
    };
    self.removeRememberMeToken = function () {
        store.remove('rememberMeToken');
    };

    self.removeUser = function () {
        store.remove('currentUser');
    };

    self.getUsername = function () {
        return self.getUserObj().username;
    };

    self.getFirstname = function () {
        console.log(self.getUserObj());
        return self.getUserObj().firstName;
    };

    self.getLastname = function () {
        return self.getUserObj().lastName;
    };

    self.getUserId = function () {
        return self.getUserObj().userID;
    };

    self.getSAPUserId = function () {
        return self.getUserObj().sapUserId;
    };

    self.getUserCompanyId = function () {
        return self.getUserObj().companyId;
    };

    self.getUserCatalogCompanyId = function () {
        return self.getUserObj().catalogueCompanyId;
    };

    self.setMessage = function (msg) {
        successMessage = msg;
    };

    self.getMessage = function () {
        return successMessage;
    };

    self.getUserType = function () {
        return self.getUserObj().userType;
    };


    self.setCurrentUser = function () {
        self.userData.sessionid = self.getUserToken();
        self.userData.currentUser = self.getUserObj();
    };

    self.setCurrentUser();

    self.checkUniqueValue21 = function (type, currentvalue) {
        var data = {
            "type": type,
            "currentvalue": currentvalue
        };
        //console.log(data);
        return false;
        // return $http.post("", data).then( function(res) {
        //   return res.data.isUnique;
        // });
    };

    self.checkUniqueValue = function (type, currentvalue) {
        var data = {
            "phone": currentvalue,
            "idtype": type
        };
        //console.log(data);
        //return false;
        return $http({
            method: 'POST',
            url: domain + 'checkuserifexists',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            //console.log(response);
            return response.data.CheckUserIfExistsResult;
        });
    };

    self.forgotpassword = function (forgot) {
        var data = {
            "email": forgot.email,
            "sessionid": ''
        };
        //data=forgot.email;
        return $http({
            method: 'POST',
            url: domain + 'forgotpassword',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            return response;
        });
        //  $http.post(domain+"forgotassword", data).then( function(res) {
        //   return res;
        // });
    };

    self.resetpassword = function (resetpass) {
        var data = {
            "email": resetpass.email,
            "sessionid": resetpass.sessionid,
            "NewPass": resetpass.NewPass,
            "ConfNewPass": resetpass.ConfNewPass
        };
        //data=forgot.email;
        return $http({
            method: 'POST',
            url: domain + 'resetpassword',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            $state.go('login');
            return response;
        });
        //  $http.post(domain+"forgotassword", data).then( function(res) {
        //   return res;
        // });
    };

    self.resendotp = function (userid) {
        var data = {
            "userID": userid
        };
        return $http({
            method: 'POST',
            url: domain + 'sendotp',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            swal("Done!", "OTP send to your registered Mobile No.", "success");
        });
    };


    self.isnegotiationrunning = function () {
        var data = {
            "userID": self.getUserId(),
            "sessionID": self.getUserToken()
        };
        return $http({
            method: 'POST',
            url: domain + 'isnegotiationrunning',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            //console.log('isnegotiationrunning');
            return response;
        });
    };
    
    self.resendemailotp = function (userid) {
        var data = {
            "userID": userid
        };
        return $http({
            method: 'POST',
            url: domain + 'sendotpforemail',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            swal("Done!", "OTP send to your registered Email.", "success");
        });
    };


    self.getUserDataNoCache = function () {
        var params = {
            userid: self.getUserId(),
            sessionid: self.getUserToken()
        }
        return $http({
            method: 'GET',
            url: domain + 'getuserinfo?userid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {

            store.set('currentUser', response.data);
            self.setCurrentUser();
            var list = {};
            if (response && response.data) {
                list = response.data;
                return list;
            } else {
                //console.log(response.data.errorMessage);
            }
        }, function (result) {
            //console.log(result);
        });
    }

    self.verifyOTP = function (otpobj) {
        var data = {
            "OTP": otpobj,
            "userID": self.getUserId(),
            "phone": otpobj.phone ? otpobj.phone : ""
        };
        return $http({
            method: 'POST',
            url: domain + 'verifyotp',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            if (response.data.sessionID != "" || response.data.userInfo.sessionID != "") {
                store.set('sessionid', response.data.sessionID != "" ? response.data.sessionID : response.data.userInfo.sessionID);
                if (response.data.userInfo.isOTPVerified == 1 && response.data.userInfo.credentialsVerified == 1) {
                    store.set('verified', 1);
                } else {
                    store.set('verified', 0);
                }
                if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                } else {
                    store.set('emailverified', 0);
                }
                if (response.data.userInfo.credentialsVerified == 1) {
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                $state.go('pages.profile.profile-about');
            } else {
                swal('Warning', "The OTP is not valid. Please enter the valid OTP sent to your mobile number", "warning");
            }
           
            return response.data;
        });
    };





    self.verifyEmailOTP = function (otpobj) {
        var data = {
            "OTP": otpobj,
            "userID": self.getUserId(),
            "email": otpobj.email ? otpobj.email : ""
        };
        return $http({
            method: 'POST',
            url: domain + 'verifyemailotp',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            if (response.data.sessionID != "" || response.data.userInfo.sessionID != "") {
                store.set('sessionid', response.data.sessionID != "" ? response.data.sessionID : response.data.userInfo.sessionID);
                if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                } else {
                    store.set('emailverified', 0);
                }
                if (response.data.userInfo.isOTPVerified == 1) {
                    store.set('verified', 1);
                } else {
                    store.set('verified', 0);
                }
                if (response.data.userInfo.credentialsVerified == 1) {
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                $state.go('pages.profile.profile-about');
            } else {
                swal('Warning', "The OTP is not valid. Please enter the valid OTP sent to your Email", "warning");
            }
           
            return response.data;
        });
    };


    self.GetVendorRatings = function (userid) {
        var params = {
            userid: userid,
            sessionid: self.getUserToken()
        }

        if (!self.getUserId()) {
            params.userid = store.get('userid');
        }

        return $http({
            method: 'GET',
            url: domain + 'getvendorratings?userid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        }, function (result) {
        });
    }

    self.SaveVendorRating = function (vendorRating) {
        var data = {
            "rId": vendorRating.rId,
            "reqId": vendorRating.reqId,
            "reviewerId": self.getUserId(),
            "revieweeId": vendorRating.revieweeId,
            "delivery": vendorRating.rating.deliveryValue,
            "quality": vendorRating.rating.qualityValue,
            "emergency": vendorRating.rating.emergencyValue,
            "service": vendorRating.rating.serviceValue,
            "response": vendorRating.rating.responseValue,
            "comments": vendorRating.comments,
            "seesionId": self.getUserToken()
        };

        return $http({
            method: 'POST',
            url: domain + 'savevendorrating',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            return response.data;
        });
    };



    self.getCompanyName = function () {

        let url = domain + 'getcompanyname';
        return httpServices.get(url);
    }


    self.isLoggedIn = function () {
        return (self.getUserToken() && self.getOtpVerified() && self.getDocsVerified()) ? true : false;
    };

    self.updateUser = function (params) {


        var params1 = {
            "user": {
                "userID": params.userID,
                "firstName": params.firstName,
                "lastName": params.lastName,
                "email": params.email,
                "phoneNum": params.phoneNum,
                "sessionID": params.sessionID,
                "isOTPVerified": params.isOTPVerified,
                "credentialsVerified": params.credentialsVerified,
                "errorMessage": params.errorMessage,
                "logoFile": params.logoFile ? params.logoFile : null,
                "logoURL": params.logoURL ? params.logoURL : "",
                "aboutUs": params.aboutUs ? params.aboutUs : "",
                "achievements": params.achievements ? params.achievements : "",
                "assocWithOEM": params.assocWithOEM ? params.assocWithOEM : false,
                "assocWithOEMFile": params.assocWithOEMFile ? params.assocWithOEMFile : null,
                "assocWithOEMFileName": params.assocWithOEMFileName ? params.assocWithOEMFileName : "",
                "clients": params.clients ? params.clients : "",
                "establishedDate": ("establishedDate" in params) ? params.establishedDate : '/Date(634334872000+0000)/',
                "products": params.products ? params.products : "",
                "strengths": params.strengths ? params.strengths : "",
                "responseTime": params.responseTime ? params.responseTime : "",
                "oemCompanyName": params.oemCompanyName ? params.oemCompanyName : "",
                "oemKnownSince": params.oemKnownSince ? params.oemKnownSince : "",
                "files": [],
                "profileFile": params.profileFile ? params.profileFile : null,
                "profileFileName": params.profileFileName ? params.profileFileName : "",
                "workingHours": params.workingHours ? params.workingHours : "",
                "directors": params.directors ? params.directors : "",
                "address": params.address ? params.address : "",
                "altEmail": params.altEmail,
                "altPhoneNum": params.altPhoneNum,
                "subcategories": params.subcategories,
                "Street1": params.Street1,
                "Street2": params.Street2,
                "Street3": params.Street3,
                "Country": params.Country,
                "State": params.State,
                "City": params.City,
                "PinCode": params.PinCode,
                "altMobile": params.altMobile,
                "landLine": params.landLine,
                "salesPerson": params.salesPerson,
                "salesContact": params.salesContact,
                "BankName": params.BankName,
                "BankAddress": params.BankAddress,
                "accntNumber": params.accntNumber,
                "Ifsc": params.Ifsc,
                "BussinessDet": params.BussinessDet,

                "gstAttachments": params.gstAttachments,
                "gstAttachmentName": params.gstAttachmentName,
                "panAttachments": params.panAttachments,
                "panAttachmentName": params.panAttachmentsName,
                "businessAttachments": params.businessAttachments,
                "businessAttachmentName": params.businessAttachmentName,
                "cancelledchequeattachments": params.cancelledchequeattachments,
                "cancelledchequeattachmentName": params.cancelledchequeattachmentName,
                "businessAttach": params.businessAttach,
                "panAttach": params.panAttach,
                "gstAttach": params.gstAttach,
                "cancelledCheque": params.cancelledCheque,
                "msmeAttach": params.msmeAttach,
                "msmeAttachments": params.msmeAttachments,
                "msmeAttachmentName": params.msmeAttachmentName,
                "additionalVendorInformation": params.additionalVendorInformation,
                "customerCopyLetterAttach": params.customerCopyLetterAttach,
                "customerCopyLetterAttachments": params.copyLetterAttachments,
                "customerCopyLetterAttachmentName": params.copyLetterAttachmentName,
                "PERSONAL_INFO_VALID": params.PERSONAL_INFO_VALID,
                "CONTACT_INFO_VALID": params.CONTACT_INFO_VALID,
                "BANK_INFO_VALID": params.BANK_INFO_VALID,
                "BUSINESS_INFO_VALID": params.BUSINESS_INFO_VALID,
                "CONFLICT_INFO_VALID": params.CONFLICT_INFO_VALID
            }
        }

        return $http({
            method: 'POST',
            url: domain + 'updateuserinfo',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params1
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
                store.set('sessionid', response.data.sessionID);
                store.set('verified', 0);
                store.set('emailverified', 0);
                store.set('credverified', 0);
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                if (response.data.userInfo.isOTPVerified == 1 && response.data.userInfo.credentialsVerified == 1) {
                    store.set('verified', 1);
                } if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                }
                if (response.data.userInfo.credentialsVerified == 1) {
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                if (response.data.objectID > 0) {

                    growlService.growl('User data has updated Successfully!', 'inverse');
                    //self.getProfileDetails();
                    //$state.go('pages.profile.profile-about');
                    if (params.ischangePhoneNumber == 1) {
                        //self.logout();
                        $state.go('login');
                        return responce.data;
                    } else {
                        $state.reload();
                    }
                    return "true";
                } else {
                    return "false";
                }
            } else if (response && response.data && response.data.errorMessage != "") {
                swal('Error', response.data.errorMessage, 'error');
                //store.set('emailverified', 1);
                return response.data.errorMessage;

            } else {
                return "Update failed";
            }
        }, function (result) {
            return "Update failed";
        });
    }







    self.UpdateUserProfileInfo = function (params) {
        return $http({
            method: 'POST',
            url: domain + 'updateuserprofileinfo',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params
        }).then(function (response) {
            return response.data;
        });
    }










    self.getProfileDetails = function (params) {
        return $http({
            method: 'GET',
            url: domain + 'getuserdetails?userid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {
            var list = {};
            if (response && response.data) {
                list = response.data;
                return list;
            } else {
                //console.log(response.data.errorMessage);
            }
        }, function (result) {
            //console.log(result);
        });
        $state.go('pages.profile.profile-about');
    };

    self.getGSTDetails = function (params) {
        let url = domain + 'searchgstin?gstin=' + params.gstin + "&year=" + params.year + "&sessionid=" + params.sessionid;
        return httpServices.get(url);
    };

    self.getUserDeptDesig = function (userid, sessionid) {
        return $http({
            method: 'GET',
            url: domain + 'getuserdeptdesig?userid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {
            var list = {};
            if (response && response.data) {
                list = response.data;
                return list;
            } else {
                //console.log(response.data.errorMessage);
            }
        }, function (result) {
            //console.log(result);
        });
    }

    self.updatePassword = function (params) {
        return $http({
            method: 'POST',
            url: domain + 'changepassword',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params
        }).then(function (response) {
            return response.data;
        });
    }


    self.AddVendorsExcel = function (params) {
        return $http({
            method: 'POST',
            url: domain + 'importentity',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params
        }).then(function (response) {
            return response.data;
        });
    }

    self.getuseraccess = function (userid, sessionid) {
        return $http({
            method: 'GET',
            url: domain + 'getuseraccess?userid=' + userid + '&sessionid=' + sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        }, function (result) {
            console.log("date error");
        });
    }





    self.saveUserAccess = function (params) {
        return $http({
            method: 'POST',
            url: domain + 'saveuseraccess',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: params
        }).then(function (response) {
            return response.data;
        });
    }




    self.getSubUsersData = function (params) {
        return $http({
            method: 'GET',
            url: domain + 'getsubuserdata?userid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {
            return response.data;
        });
    };

    self.getCompanyUsers = function (params) {
        return $http({
            method: 'GET',
            url: domain + 'getcompanyuserdata?compId=' + params.compId + '&sessionId=' + params.sessionId,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {
            return response.data;
        });
    };



    self.updateVerified = function (verified) {
        store.set('credverified', verified);
    }

    self.GetPRMFieldMappingDetails = function (template) {
        var objList = [];
        var def = $q.defer();
        if (self.companyFieldTemplates && self.companyFieldTemplates.length > 0) {
            objList = _.filter(self.companyFieldTemplates, function (o) {
                return o.TEMPLATE_NAME === template;
            });

            def.resolve(objList);
        } else {
            var tempParams = {
                compid: self.getUserCompanyId(),
                templatename: '',
                sessionId: self.getUserToken()
            };
            let url = 'custom-fields/svc/PRMCustomFieldService.svc/REST/getprmfieldmappingdetails?compid=' + self.getUserCompanyId() + '&templatename=' + tempParams.templatename
                + '&sessionid=' + self.getUserToken();
            httpServices.get(url)
                .then(function (response) {
                    if (response && response.length > 0) {
                        self.companyFieldTemplates = response;
                        objList = _.filter(self.companyFieldTemplates, function (o) {
                            return o.TEMPLATE_NAME === template;
                        });
                        def.resolve(objList);
                    }
                });
        }

        return def.promise;
    };

    self.checkUserUniqueResult = function (uniquevalue, idtype, onlyvendor, onlycustomer) {
        var data = {
            "phone": uniquevalue,
            "idtype": idtype,
            'onlyvendor': onlyvendor,
            'onlycustomer': onlycustomer
        };
        return $http({
            method: 'POST',
            url: domain + 'checkuserifexists',
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: data
        }).then(function (response) {
            return response.data.CheckUserIfExistsResult;
        });
    };

    self.login = function (user) {
        var loginMethod = "loginuser";
        if (user && user.userhashtoken && user.source == "DESKTOP_CONTAINER") {
            loginMethod = "loginuserbyhash";
        }

        if (user && user.ssotoken && user.ssoType === 'JWT') {
            loginMethod = "loginjwtsso";
        }
        if (user && user.ssotoken && user.ssoType === 'AD') {
            loginMethod = "loginadsso";
        }

        return $http({
            method: 'POST',
            url: domain + loginMethod,
            headers: { 'Content-Type': 'application/json' },
            encodeURI: true,
            data: user
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
                store.set('sessionid', response.data.sessionID);
                store.set('userhashtoken', response.data.userHashToken);
                store.set('authentication', response.data.jwtToken);
                store.set('verified', 0);
                store.set('emailverified', 0);
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                if (response.data.userInfo.isOTPVerified == 1) {
                    store.set('verified', 1);
                }
                else if (response.data.userInfo.isOTPVerified == 0) {
                    self.resendotp(response.data.objectID);
                }
                if (response.data.userInfo.credentialsVerified == 1) {
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                if (response.data.userInfo.isEmailOTPVerified == 1) {
                    store.set('emailverified', 1);
                }
                else if (response.data.userInfo.isEmailOTPVerified == 0) {
                    self.resendemailotp(response.data.objectID);
                }

                $rootScope.entitlements = [];
                self.getuseraccess(response.data.objectID, response.data.sessionID)
                    .then(function (responseObj) {
                        if (responseObj && responseObj.length > 0) {
                            $rootScope.entitlements = responseObj;
                            store.set('localEntitlement', $rootScope.entitlements);
                        }

                    });

                $rootScope.rootDeptDesig = [];

                $rootScope.rootDetails = {
                    userId: response.data.objectID,
                    sessionId: response.data.sessionID,
                    deptId: 0,
                    deptCode: '',
                    deptTypeID: 0,
                    desigId: 0,
                    desigCode: '',
                    desigTypeID: 0
                };

                if (self.getUserType() == "CUSTOMER") {
                    //Load company templates
                    var tempParams = {
                        compid: self.getUserCompanyId(),
                        templatename: '',
                        sessionId: self.getUserToken()
                    };
                    let url = 'custom-fields/svc/PRMCustomFieldService.svc/REST/getprmfieldmappingdetails?compid=' + self.getUserCompanyId() + '&templatename=' + tempParams.templatename
                        + '&sessionid=' + self.getUserToken();
                    httpServices.get(url)
                        .then(function (response) {
                            if (response && response.length > 0) {
                                self.companyFieldTemplates = response;
                            }
                        });
                    //^load company templates
                    auctionsService.GetUserDepartmentDesignations(response.data.objectID, response.data.sessionID)
                        .then(function (responseDeptDesig) {
                            if (responseDeptDesig && responseDeptDesig.length > 0) {
                                var ListUserDepartmentDesignations = responseDeptDesig;

                                ListUserDepartmentDesignations = ListUserDepartmentDesignations.filter(function (udd) {
                                    return (udd.isAssignedToUser == true);
                                });

                                store.set('localDeptDesig', ListUserDepartmentDesignations);
                                store.set('ListUserDepartmentDesignations', ListUserDepartmentDesignations);

                                if (ListUserDepartmentDesignations && ListUserDepartmentDesignations.length > 0) {

                                    ListUserDepartmentDesignations = ListUserDepartmentDesignations.filter(function (d) {
                                        return (d.isAssignedToUser == true && d.isValid == true)
                                    });

                                    store.set('SelectedUserDepartmentDesignation', ListUserDepartmentDesignations[0]);
                                }
                                else {
                                    growlService.growl('You are not assigned to any Departments and designation.', 'inverse');
                                }


                                console.log('ListUserDepartmentDesignations-------------------------->START');
                                console.log(ListUserDepartmentDesignations);
                                console.log('ListUserDepartmentDesignations-------------------------->END');

                            }

                            $state.go('home');
                        });
                }
                else {
                    $state.go('home');
                }
               

                return response.data;
            } else if (response && response.data && response.data.errorMessage != "") {
                return response.data;
            } else {
                return "Login failed";
            }
        }, function (result) {
            return "Login failed";
        });
    };

    self.logout = function () {
        return $http({
            method: 'POST',
            data: { "userID": self.getUserId(), "sessionID": self.userData.sessionid },
            url: domain + 'logoutuser',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            self.removeUserHashToken();
            self.removeJWTToken();
            self.removeUserToken();
            self.removeLocalEntitlement();
            self.removeUser();
            self.removeLocalDeptDesig();
            self.removeListUserDepartmentDesignations();
            self.removeSelectedUserDepartmentDesignation();
            delete self.userData.sessionid;
            delete self.userData.currentUser;
            self.setMessage("Successfully Logged Out");
            $state.go('login');
        });
    };

    self.deleteUser = function (params) {
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'deleteuser',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }

    self.activateUser = function (params) {
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'activateuser',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }

    self.activatecompanyvendor = function (params) {
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'activatecompanyvendor',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }


    self.approveVendorThroughWorkflow = function (params) {
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'approveVendorThroughWorkflow',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }

    self.blockUnBlockVendor = function (params) {
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'blockUnBlockVendor',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }


    self.acceptRegistration = function (params) {
        params.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: params,
            url: domain + 'acceptRegistration',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }

    self.updateCompanyStatus = function (data) {
        data.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: data,
            url: domain + 'VendorStatusUpdate',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }

    self.updateVendorScore = function (data) {
        data.sessionID = self.getUserToken();
        return $http({
            method: 'POST',
            data: data,
            url: vendorDomian + 'vendor/save_score',
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            return response.data;
        });
    }

    self.calculateScore = function (userId) {
        return httpServices.get(vendorDomian + "vendor/calculate_score/" + userId);
    }

    self.getVendorScore = function (userId) {
        return httpServices.get(vendorDomian + "vendor/get_score/" + userId);
    }

    self.getVendorSubCategories = function (userId) {
        return httpServices.get(vendorDomian + "vendor/get_vendor_subcategory/" + userId);
    }

    self.saveVendorStatus = function (data) {
        return httpServices.post(vendorDomian + "vendor/save_status/",data);
    }

    self.userregistration = function (register) {
        var params = {
            "userInfo": {
                "userID": "0",
                "firstName": register.firstName,
                "lastName": register.lastName,
                "email": register.email,
                "phoneNum": register.phoneNum,
                "username": register.username,
                "password": register.password,
                "birthday": '/Date(634334872000+0000)/',
                "userType": "CUSTOMER",
                "isOTPVerified": 0,
                "category": "",
                "institution": ("institution" in register) ? register.institution : "",
                "addressLine1": "",
                "addressLine2": "",
                "addressLine3": "",
                "city": "",
                "state": "",
                "country": "",
                "zipCode": "",
                "addressPhoneNum": "",
                "extension1": "",
                "extension2": "",
                "userData1": "",
                "registrationScore": 0,
                "errorMessage": "",
                "sessionID": "",
            }
        };

        return $http({
            method: 'POST',
            url: domain + 'registeruser',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "" && response.data.objectID != 0) {
                store.set('sessionid', response.data.sessionID);
                store.set('verified', 0);
                store.set('emailverified', 0);
                store.set('credverified', 0);
                if (response.data.userInfo.credentialsVerified == 1) {
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                store.set('currentUser', response.data.userInfo);
                self.setCurrentUser();
                return response.data;
                //growlService.growl('Welcome to PRM360, To add new requirement Please use "+" at the Bottom.', 'inverse');
            } else if (response && response.data && response.data.errorMessage != "") {
                return response.data.errorMessage;
            } else {
                return "Registeration failed";
            }
        }, function (result) {
            return "Registeration failed";
        });
    };

    self.vendorregistration = function (vendorregisterobj) {
        var vendorcat = [];
        vendorcat.push(vendorregisterobj.category);

        var params = {
            "vendorInfo": {
                "firstName": vendorregisterobj.firstName,
                "lastName": vendorregisterobj.lastName,
                "email": vendorregisterobj.email,
                "contactNum": vendorregisterobj.phoneNum,
                "username": vendorregisterobj.username,
                "password": vendorregisterobj.password,
                "institution": vendorregisterobj.institution ? vendorregisterobj.institution : "",
                "rating": 1,
                "isOTPVerified": 0,
                "category": vendorcat,
                "panNum": ("panno" in vendorregisterobj) ? vendorregisterobj.panno : "",
                "serviceTaxNum": ("taxno" in vendorregisterobj) ? vendorregisterobj.taxno : "",
                "vatNum": ("vatno" in vendorregisterobj) ? vendorregisterobj.vatno : "",
                "referringUserID": 0,
                "knownSince": "",
                "errorMessage": "",
                "sessionID": ""
            }
        };
        return $http({
            method: 'POST',
            url: domain + 'addnewvendor',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "") {
                store.set('sessionid', response.data.sessionID);
                store.set('currentUser', response.data);
                store.set('verified', 0);
                store.set('emailverified', 0);
                store.set('credverified', 0);
                if (response.data.credentialsVerified == 1) {
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                self.setCurrentUser();
                return response.data;
                //growlService.growl('Welcome to PRM360, Please go thourgh with your Auctions', 'inverse');
                //return "Vendor Registration Successfully Completed.";
            } else if (response && response.data && response.data.errorMessage != "") {
                return response.data.errorMessage;
            } else {
                return "Vendor Registration Failed.";
            }
        });
    };


    self.vendorregistration1 = function (vendorregisterobj) {
        var vendorcat = [];
        vendorcat.push(vendorregisterobj.category);
        //console.log(vendorregisterobj.role);



        var companyId = 0;
        if (vendorregisterobj.role == 'VENDOR') {
            if (store.get('currentUser') != undefined) {
                companyId = store.get('currentUser').companyId;
            } else {
                companyId = 1443;
            }
        }


        var params = {
            "register": {
                "firstName": vendorregisterobj.firstName,
                "lastName": vendorregisterobj.lastName,
                "email": vendorregisterobj.email,
                "phoneNum": vendorregisterobj.phoneNum,
                "username": vendorregisterobj.username,
                "password": vendorregisterobj.password,
                "companyName": vendorregisterobj.institution ? vendorregisterobj.institution : "",
                "isOTPVerified": 0,
                "category": vendorregisterobj.category ? vendorregisterobj.category : "",
                "userType": vendorregisterobj.role,
                "panNumber": ("panno" in vendorregisterobj) ? vendorregisterobj.panno : "",
                "stnNumber": ("taxno" in vendorregisterobj) ? vendorregisterobj.taxno : "",
                "vatNumber": ("vatno" in vendorregisterobj) ? vendorregisterobj.vatno : "",
                "referringUserID": companyId,
                "knownSince": "",
                "errorMessage": "",
                "sessionID": "",
                "userID": 0,
                "department": "",
                "currency": vendorregisterobj.currency,
                "timeZone": vendorregisterobj.timeZone,
                "subcategories": vendorregisterobj.subcategories ? vendorregisterobj.subcategories : [],
                "additionalData": JSON.stringify(vendorregisterobj.additionalData),
                "attachments": JSON.stringify(vendorregisterobj.attachments)

            }
        };
        return $http({
            method: 'POST',
            url: domain + 'register',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "") {
                store.set('sessionid', response.data.sessionID);
                store.set('currentUser', response.data.userInfo);
                store.set('verified', 0);
                store.set('emailverified', 0);
                store.set('credverified', 0);
                if (response.data.credentialsVerified == 1) {
                    store.set('credverified', 1);
                } else {
                    store.set('credverified', 0);
                }
                self.setCurrentUser();
                return response.data;
                //growlService.growl('Welcome to PRM360, Please go thourgh with your Auctions', 'inverse');
                //return "Vendor Registration Successfully Completed.";
            } else if (response && response.data && response.data.errorMessage != "") {
                return response.data.errorMessage;
            } else {
                return "Vendor Registration Failed.";
            }
        });
    };




    self.addnewuser = function (addnewuserobj) {
        var referringUserID = 0;
        referringUserID = self.getUserId();
        var params = {
            "register": {
                "firstName": addnewuserobj.firstName,
                "lastName": addnewuserobj.lastName,
                "email": addnewuserobj.email,
                "phoneNum": addnewuserobj.phoneNum,
                "username": addnewuserobj.email,
                "password": addnewuserobj.phoneNum,
                "companyName": addnewuserobj.companyName ? addnewuserobj.companyName : "",
                "isOTPVerified": 0,
                "category": addnewuserobj.category ? addnewuserobj.category : "",
                "userType": addnewuserobj.userType,
                "panNumber": "",
                "stnNumber": "",
                "vatNumber": "",
                "referringUserID": referringUserID,
                "knownSince": "",
                "errorMessage": "",
                "sessionID": addnewuserobj.isEditUser ? self.getUserToken() : "",
                "userID": addnewuserobj.isEditUser ? addnewuserobj.userID : 0,
                "department": "",
                "currency": addnewuserobj.currency,
                "userLoginID": addnewuserobj.loginid,
                "userPassword": addnewuserobj.password1,
                "isSubUser": addnewuserobj.isSubUser,
                "isEditUser": addnewuserobj.isEditUser,
                "sapUserId": addnewuserobj.sapUserId,
                "validityFrom": addnewuserobj.validityFrom,
                "validityTo": addnewuserobj.validityTo,
                "oldEmail": addnewuserobj.oldEmail,
                "oldPhone": addnewuserobj.oldPhone
            }
        };

        return $http({
            method: 'POST',
            url: domain + 'register',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            return response.data;
        });
    };


    self.GetCompanyVendors = function (params) {
        if (!params.PRODUCT_ID) {
            params.PRODUCT_ID = 0;
        }
        if (!params.IS_FROM) {
            params.IS_FROM = 'VENDORS';
        }
        if (!params.CHECKED_VENDORS) {
            params.CHECKED_VENDORS = '';
        }
        if (!params.DEACTIVE_PARAM)
        {
            params.DEACTIVE_PARAM = 1;
        }
        return $http({
            method: 'GET',
            url: domain + 'getcompanyvendors?userid=' + params.userID + '&sessionid=' + params.sessionID + '&PageSize=' + params.PageSize + '&NumberOfRecords=' + params.NumberOfRecords + '&searchString=' + params.searchString
                + '&PRODUCT_ID=' + params.PRODUCT_ID + '&IS_FROM=' + params.IS_FROM + '&CHECKED_VENDORS=' + params.CHECKED_VENDORS + '&VENDOR_CODE=' + params.VENDOR_CODE + '&DEACTIVE_PARAM=' + params.DEACTIVE_PARAM ,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
            //  + '&Categories=' + params.Categories
        }).then(function (response) {
            var list = {};
            if (response && response.data) {
                list = response.data;
                return list;
            } else {
                //console.log(response.data.errorMessage);
            }
        }, function (result) {
            //console.log(result);
        });
    }


    self.toUTCTicks = function (local) {
        var d = new Date();
        var n = d.getTimezoneOffset();
        n = n + 330;
        n = n * 60000;
        var ticks = moment(moment.utc(moment(local, "DD-MM-YYYY HH:mm")).format("MM-DD-YYYY HH:mm").valueOf(), "MM-DD-YYYY HH:mm").valueOf();
        ticks = parseInt(ticks) - n;
        return ticks;
    }

    self.toLocalDate = function (utc) {
        var utcTemp = moment.tz(utc, "UTC").add(330, 'minutes');
        return moment.utc(new moment(utcTemp).format("DD-MM-YYYY HH:mm"), "DD-MM-YYYY HH:mm").local().format('DD-MM-YYYY HH:mm');
    }
    self.toLocalDateFormat1 = function (utc) {
        var utcTemp = moment.tz(utc, "UTC").add(330, 'minutes');
        return moment.utc(new moment(utcTemp).format("DD-MMM-YYYY hh:mm A"), "DD-MMM-YYYY hh:mm A").local().format('DD-MMM-YYYY hh:mm A');
    }

    self.toLocalDateOnly = function (utc) {
        var utcTemp = moment.tz(utc, "UTC").add(330, 'minutes');
        return moment.utc(new moment(utcTemp).format("DD-MM-YYYY"), "DD-MM-YYYY").local().format('DD-MM-YYYY');
    }

    self.toOnlyDate = function (utc) {
        var utcTemp = moment.tz(utc, "UTC").add(330, 'minutes');
        return moment.utc(new moment(utcTemp).format("DD-MM-YYYY HH:mm"), "DD-MM-YYYY HH:mm").local().format('DD-MM-YYYY');
    }

    self.GetUserBranches = function (params) {
        return $http({
            method: 'GET',
            url: PRMCompanyDomain + 'userbranches?uid=' + params.userid + '&sessionid=' + params.sessionid,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
            //data: params
        }).then(function (response) {
            var list = {};
            if (response && response.data) {
                list = response.data;
                return list;
            } else {
                //console.log(response.data.errorMessage);
            }
        }, function (result) {
            //console.log(result);
        });
    }

    self.isCatalogueEnabled = function () {
        return self.getUserObj().isCatalogueEnabled;
    };

    self.getCatalogueCompanyId = function () {
        return self.getUserObj().catalogueCompanyId;
    };

    self.getUserSelectedBranch = function () {
        var currentBranchId = 0;
        if (store.get('selecteduserbranch')) {
            currentBranchId = store.get('selecteduserbranch');
        }

        return currentBranchId;
    };

    self.setUserSelectedBranch = function (branchId) {
        store.set('selecteduserbranch', branchId);
    };

    self.getToDoData = function (params) {
        let url = domain + 'getToDoData?myDate=' + params.myDate + '&userId=' + params.userId + '&userType=' + params.userType + '&sessionid=' + self.getUserToken();
        
        return httpServices.get(url);
    };

    self.goToQCS = function (reqId, sessionId, qcsId) {
        return $http({
            method: 'GET',
            url: reportingDomain + 'GetQCSIDS?reqid=0'+ '&sessionid=' + sessionId + '&qcsid=' + qcsId ,
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' }
        }).then(function (response) {
            var list = {};
            if (response && response.data) {
               list = response.data;
               var qcsObj = _.find(list, { QCS_ID: qcsId });
               if (qcsObj)
               {
                   if (qcsObj.QCS_TYPE === 'DOMESTIC')
                   {
                       var url = $state.href("cost-comparisions-qcs", { "reqID": qcsObj.REQ_ID, "qcsID": qcsId });
                       window.open(url, '_blank');
                   }

                   if (qcsObj.QCS_TYPE === 'IMPORT')
                   {
                       var url = $state.href("import-qcs", { "reqID": qcsObj.REQ_ID, "qcsID": qcsId });
                       window.open(url, '_blank');
                   }
               }
            } else {

            }
        }, function (result) {
            //console.log(result);
        });
    }

    self.NegotiationStatus = function (type, status) {

        if (type == 'CUSTOMER') {
            if (status == 'UNCONFIRMED') {
                status = 'Quotations pending';
            } else if (status == 'NOTSTARTED') {
                status = 'Negotiation scheduled';
            } else if (status == 'STARTED') {
                status = 'Negotiation started';
            } else if (status == 'Negotiation Ended') {
                status = 'Negotiation Closed';
            } else if (status == 'DELETED') {
                status = 'Negotiation Cancelled';
            } else if (status == 'Qcs Pending') {
                status = 'QCS Pending';
            } else if (status == 'Qcs Created') {
                status = 'QCS Created';
            } else if (status == 'Qcs Approved') {
                status = 'QCS Approved';
            } else if (status == 'Qcs Approval Pending') {
                status = 'QCS Approval Pending';
            } else if (status == 'Quotations Received') {
                status = 'Quotations Received';
            } else if (status == 'Saved As Draft') {
                status = 'Saved As Draft';
            } else if (status == 'Closed') {
                status = 'Closed';
            } else if (status == 'PO Generated') {
                status = 'PO Generated';
            }
        } else if (type == 'VENDOR') {
            if (status == 'NEW') {
                status = 'NEW'; // / WIP
            } else if (status == 'NOTSTARTED') {
                status = 'Negotiation scheduled';
            } else if (status == 'STARTED') {
                status = 'Negotiation started';
            } else if (status == 'Negotiation Ended') {
                status = 'Negotiation Closed';
            } else if (status == 'DELETED') {
                status = 'Negotiation Cancelled';
            } else if (status == 'WIP') {
                status = 'WIP';
            } else if (status == 'Closed') {
                status = 'Closed';
            }
        }

        return status;
    };


    self.getUserNotifications = function () {
        var params = {
            userid: self.getUserId(),
            sessionid: self.getUserToken()
        };

        let url = domain + 'getuseralerts?uid=' + params.userid + '&sessionid=' + self.getUserToken();
        return httpServices.get(url);
    };

    self.validateUserLogin = function (params) {
        var data = {
            'userLoginId': (params.userLoginId || ''),
            'email': (params.email || ''),
            'phone': (params.phone || '')
        };

        let url = domain + 'validateuserlogin';
        return httpServices.post(url, data);
    };

};prmApp
    .service('workflowService', ["workflowDomain", "userService", "httpServices",
        function (workflowDomain, userService, httpServices) {
            //var domain = 'http://182.18.169.32/services/';
            var workflowService = this;

            workflowService.getWorkflowList = function (evalID) {
                var url = workflowDomain + 'workflows?compid=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            workflowService.getItemWorkflow = function (wfid, moduleid, module) {
                var url = workflowDomain + 'itemworkflow?wfid=' + wfid + '&moduleid=' + moduleid + '&module=' + module + '&sessionid=' + userService.getUserToken() + '&userid=' + userService.getUserId();
                return httpServices.get(url);
            };

            workflowService.updateWorkflow = function (params) {
                var url = workflowDomain + 'saveworkflow';
                var params1 = {
                    workflow: params
                }
                return httpServices.post(url, params1);
            };


            workflowService.SaveWorkflowTrack = function (params) {
                var params1 = {
                    track: params
                }
                var url = workflowDomain + 'saveworkflowtrack';
                return httpServices.post(url, params1);
            };

            workflowService.SaveApprovalTrack = function (params) {
                var params1 = {
                    track: params
                }
                var url = workflowDomain + 'saveapprovaltrack';
                return httpServices.post(url, params1);
            };
            workflowService.updateWorkflowStage = function (params) {
                var url = workflowDomain + 'saveworkflowstage';
                return httpServices.post(url, params);
            };

            workflowService.deleteWorkflow = function (params) {
                var url = workflowDomain + 'deleteworkflow';
                return httpServices.post(url, params);
            };

            workflowService.deleteWorkflowStage = function (params) {
                var url = workflowDomain + 'deleteworkflowstage';
                return httpServices.post(url, params);
            };

            workflowService.assignWorkflow = function (params) {
                var url = workflowDomain + 'assignworkflow';
                return httpServices.post(url, params);
            };

            workflowService.IsUserApproverForStage = function (params) {
                var params1 = {
                    workflow: params
                }

                var url = workflowDomain + 'isuserapproverforstage';
                return httpServices.post(url, params1);
            };

            workflowService.IsUserApproverForStage = function (approverID, userID) {
                var url = workflowDomain + 'isuserapproverforstage?approverid=' + approverID + '&userid=' + userID + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            workflowService.GetMyWorkflows = function (userid, status) {
                var url = workflowDomain + 'getmyworkflows?userid=' + userid + '&status=' + status + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            workflowService.GetMyIndents = function (userid, type) {
                var url = workflowDomain + 'getmyindents?userid=' + userid + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            workflowService.DBGetMyPendingApprovals = function (userid, type) {
                var url = workflowDomain + 'dbgetmypendingapprovals?userid=' + userid + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            workflowService.GetMyPendingApprovals = function (deptID,desigID) {
                var url = workflowDomain + 'getmypendingapprovals?userid=' + userService.getUserId() + '&deptid=' + deptID + '&desigid=' + desigID + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            

            workflowService.GetSeries = function (series, seriestype, deptid) {

                var url = workflowDomain + 'getseries?series=' + series + '&seriestype=' + seriestype + '&compid=' + userService.getUserCompanyId() + '&deptid=' + deptid + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };


            workflowService.SaveUserStatus = function (params) {
                var url = workflowDomain + 'saveuserstatus';
                    return httpServices.post(url, params);
            };


            workflowService.GetApprovalList = function (params) {
                var url = workflowDomain + 'getapprovallist?userid=' + params.userid + '&deptid=' + params.deptid + '&desigid=' + params.desigid + '&type=' + params.type + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };


            return workflowService;
        }]);