﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Data;
using PRMServices.Models.Catalog;
using System.IO;
using OfficeOpenXml;
using PRMServices.Common;
using PRMServices.SQLHelper;
using PRMName = PRMServices.Models;
using CORE = PRM.Core.Common;
using System.Net.Mail;
using OfficeOpenXml.DataValidation;
using System.Drawing;
using OfficeOpenXml.Style;
using System.Configuration;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web;
using System.Dynamic;
using AutoMapper;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PRMRealTimePriceService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PRMRealTimePriceService.svc or PRMRealTimePriceService.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMCatalogService : IPRMCatalogService
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();

        private List<Category> Getcategories(int compId, int catId, string catCode, string catName, int PageSize = 0, int NumberOfRecords = 0)
        {
            DataSet ds = CatalogUtility.GetResultSet("cm_getcategories", new List<string>() { "P_COMP_ID", "P_CAT_ID", "P_CAT_CODE", "P_CAT_NAME", "P_PAGE", "P_PAGE_SIZE" }, new List<object>() { compId, catId, catCode, catName, PageSize, NumberOfRecords});
            List<Category> details = FillCategoryModel(ds);
            List<Category> data = new List<Category>();
            GetTreeData(details, data, 0);
            return data;
        }

        public List<Category> GetCategories(int compId, string sessionId, int PageSize = 0, int NumberOfRecords = 0)
        {
            int isValidSessin = Utilities.ValidateSession(sessionId);
            return Getcategories(compId, 0, "", "", PageSize, NumberOfRecords);
        }

        public Category GetCategoryById(int compId, int catId, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            List<Category> details = new List<Category>();
            details = Getcategories(compId, catId, string.Empty, string.Empty);
            if (details != null && details.Count > 0)
            {
                return details[0];
            }
            else
            {
                return new Category();
            }
        }

        public Category GetCategoryByName(int compId, string catName, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            List<Category> details = new List<Category>();
            details = Getcategories(compId, 0, string.Empty, catName);
            if (details != null && details.Count > 0)
            {
                return details[0];
            }
            else
            {
                return new Category();
            }
        }

        public Category GetCategoryByCode(int compId, string catCode, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            List<Category> details = new List<Category>();
            details = Getcategories(compId, 0, catCode, string.Empty);
            if (details != null && details.Count > 0)
            {
                return details[0];
            }
            else
            {
                return new Category();
            }
        }

        public List<Category> SearchCategories(string searchString, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            DataSet ds = CatalogUtility.GetResultSet("cm_SearchCategories", new List<string>() { "searchText" }, new List<object>() { searchString });
            return FillCategoryModel(ds);
        }

        public CatalogResponse AddCategory(Category reqCategory, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_CAT_ID", 0);
                sd.Add("P_COMP_ID", reqCategory.CompanyId);
                sd.Add("P_CAT_NAME", reqCategory.CategoryName);
                sd.Add("P_CAT_CODE", string.Empty);
                sd.Add("P_CAT_DESC", reqCategory.CategoryDesc == null ? string.Empty : reqCategory.CategoryDesc);
                sd.Add("P_CAT_ORDER", reqCategory.CategoryOrder);
                sd.Add("P_CAT_PARENTID", reqCategory.CatParentId);
                sd.Add("P_CAT_DEPTS", null == reqCategory.Departments? string.Empty: reqCategory.Departments);
                sd.Add("P_CAT_ISVALID", 1);
                sd.Add("P_USER", reqCategory.ModifiedBy);
                ds = sqlHelper.SelectList("cm_savecategory", sd);
            }
            catch(Exception ex)
            {
                catalogResponse.ErrorMessage = ex.Message;
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                if (catalogResponse.ResponseId == -99)
                {
                    catalogResponse.ErrorMessage = "\""+ reqCategory.CategoryName + "\" already exists";
                }
            }
            catalogResponse.SessionID = sessionid;

            return catalogResponse;
        }

        public CatalogResponse DeleteCategory(Category reqCategory, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_CAT_ID", reqCategory.CategoryId);
                sd.Add("P_COMP_ID", reqCategory.CompanyId);
                sd.Add("P_CAT_CODE", reqCategory.CategoryCode);
                sd.Add("P_USER", reqCategory.ModifiedBy);
                ds = sqlHelper.SelectList("cm_deletecategory", sd);
            }
            catch(Exception ex)
            {
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                if(ds.Tables[0].Columns.Count > 1)
                {
                    catalogResponse.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : "";
                }
            }

            catalogResponse.SessionID = sessionid;
            return catalogResponse;
        }

        public CatalogResponse UpdateCategory(Category reqCategory, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_CAT_ID", reqCategory.CategoryId);
                sd.Add("P_COMP_ID", reqCategory.CompanyId);
                sd.Add("P_CAT_NAME", reqCategory.CategoryName);
                sd.Add("P_CAT_CODE", reqCategory.CategoryCode == null ? string.Empty : reqCategory.CategoryCode);
                sd.Add("P_CAT_DESC", reqCategory.CategoryDesc == null ? string.Empty : reqCategory.CategoryDesc);
                sd.Add("P_CAT_ORDER", reqCategory.CategoryOrder);
                sd.Add("P_CAT_PARENTID", reqCategory.CatParentId);
                sd.Add("P_CAT_DEPTS", null == reqCategory.Departments ? string.Empty : reqCategory.Departments);
                sd.Add("P_CAT_ISVALID", 1);
                sd.Add("P_USER", reqCategory.ModifiedBy);
                ds = sqlHelper.SelectList("cm_savecategory", sd);
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                if (catalogResponse.ResponseId == -99)
                {
                    catalogResponse.ErrorMessage = "\"" + reqCategory.CategoryName + "\" alreadt exists";
                }
            }

            catalogResponse.SessionID = sessionid;
            return catalogResponse;
        }

        public List<Category> GetSubCategories(int parentCatId, int companyId, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            List<Category> details = new List<Category>();
            DataSet ds = CatalogUtility.GetResultSet("cm_getsubcategories", new List<string>() { "P_PARENT_ID", "P_COMP_ID" }, new List<object>() { parentCatId, companyId });
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    Category category = new Category();
                    try
                    {
                        CatalogUtility.SetItemFromRow(category, dr);
                    }
                    catch (Exception ex)
                    {
                        category = new Category();
                        category.ErrorMessage = ex.Message;
                    }

                    details.Add(category);
                }
            }
            return details;
        }

        public List<Category> GetProductSubCategories(int prodId, int parentCatId, int companyId, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            List<Category> details = new List<Category>();
            DataSet ds = CatalogUtility.GetResultSet("cm_GetProductCategories", new List<string>() { "P_COMP_ID", "P_CAT_PARENT_ID", "P_PROD_ID" }, new List<object>() { companyId, parentCatId, prodId });
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    Category category = new Category();
                    try
                    {
                        CatalogUtility.SetItemFromRow(category, dr);
                    }
                    catch (Exception ex)
                    {
                        category = new Category();
                        category.ErrorMessage = ex.Message;
                    }

                    details.Add(category);
                }
            }
            List<Category> data = new List<Category>();
            GetTreeData(details, data, parentCatId);
            return data;
        }

        public List<Category> GetVendorCategories(int vendorId, int parentCatId, int companyId, string sessionId, string type, int PageSize = 0, int NumberOfRecords = 0, string searchString = null)
        {
            Utilities.ValidateSession(sessionId);
            List<Category> details = new List<Category>();
            DataSet ds = CatalogUtility.GetResultSet("cm_GetVendorCategories", new List<string>() { "P_VENDOR_ID", "P_CAT_PARENT_ID", "P_COMP_ID", "P_TYPE", "P_PAGE", "P_PAGE_SIZE", "P_SEARCH_STRING" }, new List<object>() { vendorId, parentCatId, companyId, type, PageSize, NumberOfRecords, !string.IsNullOrEmpty(searchString) ? searchString : null });
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    Category category = new Category();
                    try
                    {
                        CatalogUtility.SetItemFromRow(category, dr);
                    }
                    catch (Exception ex)
                    {
                        category = new Category();
                        category.ErrorMessage = ex.Message;
                    }

                    details.Add(category);
                }
            }

            List<Category> data = new List<Category>();
            GetTreeData(details, data, 0);
            return data;
        }

        public List<Product> GetVendorProducts(int vendorId, int companyId, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            List<Product> details = new List<Product>();
            DataSet ds = CatalogUtility.GetResultSet("cm_GetVendorProducts", new List<string>() { "P_VENDOR_ID", "P_COMP_ID" }, new List<object>() { vendorId, companyId });
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    Product product = new Product();
                    try
                    {
                        CatalogUtility.SetItemFromRow(product, dr);
                    }
                    catch (Exception ex)
                    {
                        product = new Product();
                        product.ErrorMessage = ex.Message;
                    }

                    details.Add(product);
                }
            }
            //foreach(Product prod in details)
            //{
            //    if(prod.ProductSelected > 0)
            //    {
            //        prod.ProductChecked = true;
            //    }
            //    else
            //    {
            //        prod.ProductChecked = false;
            //    }
            //}
            return details;
        }


        public List<PRMName.ProductVendorDetails> GetProductVendors(int ProductID, string sessionID)
        {
            List<PRMName.ProductVendorDetails> getAssignedVendors = new List<PRMName.ProductVendorDetails>();

            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                string query = string.Format("SELECT * FROM cm_vendorproducts cvp "+
                               "inner join vendors v on v.U_ID = cvp.VendorId where cvp.ProductId = {0} and cvp.IsValid = 1;", ProductID);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        PRMName.ProductVendorDetails prmVendDet = new PRMName.ProductVendorDetails();
                        prmVendDet.VendorID = row["VendorId"] != DBNull.Value ? Convert.ToInt32(row["VendorId"]) : 0;
                        // prmVendDet.VendorID = row["VendorId"] != DBNull.Value ? Convert.ToInt32(row["VendorId"]) : 0;
                        prmVendDet.CompanyName = row["COMP_NAME"] != DBNull.Value ? Convert.ToString(row["COMP_NAME"]) : string.Empty;
                        prmVendDet.VendorCode = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                        getAssignedVendors.Add(prmVendDet);
                    }
                }

            }
            catch (Exception ex)
            {
                PRMName.ProductVendorDetails v = new PRMName.ProductVendorDetails();
                v.ErrorMessage = ex.Message;
                getAssignedVendors.Add(v);
            }

            return getAssignedVendors;
        }


        private void GetTreeData(List<Category> rawDetails, List<Category> parent, int id)
        {
            var items = rawDetails.Where(a => a.CatParentId == id && a.CategoryId > 0).Select(p => p).ToList();
            foreach (var item in items)
            {
                if(item.catSelected > 0)
                {
                    item.nodeChecked = true;
                }
                else
                {
                    item.nodeChecked = false;
                }
                parent.Add((Category)item);
            }
            foreach (var item in parent)
            {
                item.subCategories = new List<Category>();
                GetTreeData(rawDetails, item.subCategories, item.CategoryId);
            }
        }

        private List<Product> GetProducts(string sessionid, int compId, int prodId, string prodCode, string prodName, int PageSize = 0, int NumberOfRecords = 0, string searchString = null, int deactiveParams = 0)
        {
            Utilities.ValidateSession(sessionid);
            DataSet ds = CatalogUtility.GetResultSet("cm_getproducts", new List<string>() { "P_COMP_ID", "P_PROD_ID", "P_PROD_CODE", "P_PROD_NAME", "P_PAGE", "P_PAGE_SIZE", "P_SEARCH_STRING", "P_DEACTIVE_PARAMS" }, new List<object>() { compId, prodId, prodCode, prodName, PageSize, NumberOfRecords, !string.IsNullOrEmpty(searchString) ? searchString : null, deactiveParams });
            return FillProductModel(ds);
        }

        private List<Product> FillProductModel(DataSet ds)
        {
            List<Product> details = new List<Product>();
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    Product product = new Product();
                    try
                    {
                        product.ContractManagement = new List<ContractManagementDetails>();
                        CatalogUtility.SetItemFromRow(product, dr);
                        if (ds.Tables.Count > 1)
                        {
                            DataTable dt1 = ds.Tables[1];
                            foreach (DataRow dr1 in dt1.Rows)
                            {
                                ContractManagementDetails ContractManagement = new ContractManagementDetails();
                                CatalogUtility.SetItemFromRow(ContractManagement, dr1);
                                product.ContractManagement.Add(ContractManagement);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        product = new Product();
                        product.ErrorMessage = ex.Message;
                    }


                    if (string.IsNullOrEmpty(product.MfcdCode))
                    {
                        product.MfcdCode = product.ProductNo;
                    }

                    string[] text1 = { "^$~$^" };

                    if (product.ProductCode.Contains("^$~$^") && product.ProductCode != "^$~$^")
                    {
                        product.ProductCode = product.ProductCode.Split(text1, System.StringSplitOptions.RemoveEmptyEntries)[0];

                    }
                    //if (string.IsNullOrEmpty(product.CasNumber))
                    //{
                    //    product.CasNumber = product.ProductCode;
                    //}


                    details.Add(product);
                }
            }
            return details;
        }

        public List<Product> GetProducts(int compId, string sessionid, int PageSize = 0, int NumberOfRecords = 0, string searchString = null, int deactiveParams = 0)
        {
            return GetProducts(sessionid, compId, 0, "", "", PageSize, NumberOfRecords, !string.IsNullOrEmpty(searchString) ? searchString : searchString = null, deactiveParams);
        }

        public List<Product> GetUserProducts(int compId,int userId, string sessionid, int PageSize = 0, int NumberOfRecords = 0, string searchString = null, bool isCas = false, bool isMfcd = false, bool isnamesearch = false)
        {
            int SearchCas = 0;
            int SearchMfcd = 0;
            int nameSearch = 0;
            if (isCas || isMfcd) {
                SearchCas = 1;
                SearchMfcd = 1;
            }

            if (isnamesearch)
            {
                nameSearch = 1;
            }

            Utilities.ValidateSession(sessionid);
            DataSet ds = CatalogUtility.GetResultSet("cm_getuserproducts", new List<string>() { "P_COMP_ID", "P_USER", "P_PAGE", "P_PAGE_SIZE", "P_SEARCH_STRING", "P_SEARCH_CAS", "P_SEARCH_MFCD", "P_IS_NAME_SEARCH" }, 
                new List<object>() { compId, userId, PageSize, NumberOfRecords, !string.IsNullOrEmpty(searchString) ? searchString : null, SearchCas, SearchMfcd, nameSearch });
            return FillProductModel(ds);
        }

        public List<Product> GetNonCoreProducts(int compId, int userId, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            DataSet ds = CatalogUtility.GetResultSet("cm_GetNonCoreProducts", new List<string>() { "P_COMP_ID", "P_USER" }, new List<object>() { compId, userId });
            return FillProductModel(ds);
        }

        public List<Product> GetMaterialProducts(int compId, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            DataSet ds = CatalogUtility.GetResultSet("cm_getmaterialproducts", new List<string>() { "P_COMP_ID" }, new List<object>() { compId });
            return FillProductModel(ds);
        }

        public List<VendorProductData> GetProdDataReport(string reportType, int catitemid, string sessionid)
        {
            List<VendorProductData> details = new List<VendorProductData>();
            List<VendorProductDetails> details1 = new List<VendorProductDetails>();

            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                if (reportType.ToUpper().Contains("MATERIAL_REPORT"))
                {
                    sd.Add("P_C_I_ID", catitemid);
                    CORE.DataNamesMapper<VendorProductData> mapper = new CORE.DataNamesMapper<VendorProductData>();
                    var dataset = sqlHelper.SelectList("cp_GetProdDataReport", sd);
                    details = mapper.Map(dataset.Tables[0]).ToList();

                    CORE.DataNamesMapper<VendorProductDetails> mapper1 = new CORE.DataNamesMapper<VendorProductDetails>();
                    var dataset1 = sqlHelper.SelectList("cp_GetProdDataReport", sd);
                    details1 = mapper1.Map(dataset1.Tables[1]).ToList();

                    foreach (VendorProductData data in details)
                    {
                        data.VEND_PROD_DETAILS = details1.Where(d => d.REQ_ID == data.REQ_ID).ToList();

                    }

                }

                if (reportType.ToUpper().Contains("PPS_REPORT"))
                {
                    sd.Add("P_C_I_ID", catitemid);
                    CORE.DataNamesMapper<VendorProductData> mapper = new CORE.DataNamesMapper<VendorProductData>();
                    var dataset = sqlHelper.SelectList("cp_GetProdDataReport", sd);
                    details = mapper.Map(dataset.Tables[2]).ToList();
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<Product> GetProductsByFilters(int compId, string sessionId, List<ProductFilters> prodFilters)
        {
            var innerQuery = string.Empty;
            string attrQuery = string.Empty;
            string attrIsInQuery = string.Empty;
            string attrIsNotInQuery = string.Empty;
            string catQuery = string.Empty;
            string prodQuery = string.Empty;

            //var attrIsDefined = prodFilters.Where(p => (p.FilterType.Equals("attr") && (p.FilterCond.ToLower().Equals("is defined")))).Select(p => p.FilterField).ToList();
            //if(attrIsDefined != null)
            //{
            //    attrIsInQuery += " cp.propName in ( " + string.Join(",", attrIsDefined) + " ) ";
            //}

            //var attrIsNotDefined = prodFilters.Where(p => (p.FilterType.Equals("attr") && (p.FilterCond.ToLower().Equals("is defined")))).Select(p => p.FilterField).ToList();
            //if (attrIsNotDefined != null)
            //{
            //    attrIsNotInQuery += " cp.propName in ( " + string.Join(",", attrIsNotDefined) + " ) ";
            //}


            foreach (ProductFilters filter in prodFilters)
            {
                
                if (filter.FilterType.Equals("attr"))
                {
                    string condStr = string.Empty;
                    switch (filter.FilterCond.ToLower())
                    {
                        case "is defined":
                            attrIsInQuery = " cp.propName = '" + filter.FilterField + "' ";
                            break;
                        case "is not defined":
                            attrIsNotInQuery = " cp.propName = '" + filter.FilterField + "' ";
                            break;
                        case "contains":
                            attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue  like % '" + filter.FilterValue + "'";
                            break;

                        case "is equals to":
                            if (filter.filterFieldDataType.ToLower().Equals("date"))
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and Date(ep.propValue) = Date('" + filter.FilterValue + "') ";
                            }
                            else if (filter.filterFieldDataType.ToLower().Equals("int") || filter.filterFieldDataType.ToLower().Equals("decimal"))
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue = " + filter.FilterValue ;
                            }
                            else
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue = '" + filter.FilterValue + "' ";
                            }
                            
                            break;
                        case "is not equals to":
                            if (filter.filterFieldDataType.ToLower().Equals("date"))
                            {
                                attrIsNotInQuery = " cp.propName = '" + filter.FilterField + "' and Date(ep.propValue) = Date('" + filter.FilterValue + "') ";
                            }
                            else if (filter.filterFieldDataType.ToLower().Equals("int") || filter.filterFieldDataType.ToLower().Equals("decimal"))
                            {
                                attrIsNotInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue = " + filter.FilterValue;
                            }
                            else
                            {
                                attrIsNotInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue = '" + filter.FilterValue + "' ";
                            }
                            break;
                        
                        case "is greater than":
                            if(filter.filterFieldDataType.ToLower().Equals("date"))
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and Date(ep.propValue) > Date('" + filter.FilterValue + "')";
                            }
                            else 
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue > " + filter.FilterValue;
                            }
                            break;
                        case "is greater or equal to":
                            if (filter.filterFieldDataType.ToLower().Equals("date"))
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and Date(ep.propValue) >= Date('" + filter.FilterValue + "')";
                            }
                            else
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue >= " + filter.FilterValue;
                            }
                            break;
                        case "is less than":
                            if (filter.filterFieldDataType.ToLower().Equals("date"))
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and Date(ep.propValue) < Date('" + filter.FilterValue + "')";
                            }
                            else
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue < " + filter.FilterValue;
                            }
                            break;
                        case "is less or equal to":
                            if (filter.filterFieldDataType.ToLower().Equals("date"))
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and Date(ep.propValue) <= Date('" + filter.FilterValue + "')";
                            }
                            else
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue <= " + filter.FilterValue;
                            }
                            break;
                    }
                    if (!string.IsNullOrEmpty(attrIsInQuery))
                    {
                        if (string.IsNullOrEmpty(innerQuery))
                        {
                        innerQuery = " and ProductId in "
                        + "(select entityId from cm_catalogproperties cp, cm_entityproperties ep " +
                        " where cp.propId = ep.propertyId and cp.propType = 0 and cp.companyId = ep.CompanyId and cp.IsValid = 1 and ep.IsValid = 1 and " + attrIsInQuery + ")";
                        }
                        else
                        {
                            innerQuery += " and ProductId in "
                        + "(select entityId from cm_catalogproperties cp, cm_entityproperties ep " +
                        " where cp.propId = ep.propertyId and cp.propType = 0 and cp.companyId = ep.CompanyId and cp.IsValid = 1 and ep.IsValid = 1 and " + attrIsInQuery + ")";

                        }
                    }
                    if (!string.IsNullOrEmpty(attrIsNotInQuery))
                    {
                        if (string.IsNullOrEmpty(innerQuery))
                        {
                            innerQuery = " and ProductId not in "
                            + "(select entityId from cm_catalogproperties cp, cm_entityproperties ep " +
                            " where cp.propId = ep.propertyId and cp.propType = 0 and cp.companyId = ep.CompanyId and cp.IsValid = 1 and ep.IsValid = 1 and " + attrIsNotInQuery + ")";
                        }
                        else
                        {
                            innerQuery += " and ProductId not in "
                        + "(select entityId from cm_catalogproperties cp, cm_entityproperties ep " +
                        " where cp.propId = ep.propertyId and cp.propType = 0 and cp.companyId = ep.CompanyId and cp.IsValid = 1 and ep.IsValid = 1 and " + attrIsNotInQuery + ")";

                        }
                    }
                }
                else if (filter.FilterType.Equals("cat"))
                {
                    string condStr = string.Empty;
                    switch (filter.FilterCond.ToLower())
                    {
                        //case "is defined":
                        //    condStr = "is not null";
                        //    break;
                        //case "is not defined":
                        //    condStr = "is null";
                        //    break;
                        case "equals to":
                            condStr = "= " + filter.FilterValue;
                            break;
                        case "contains":
                            condStr = "like % '" + filter.FilterValue + "'";
                            break;
                    }
                    if (string.IsNullOrEmpty(catQuery))
                    {
                        catQuery = " cc.CategoryName = '" + condStr;
                    }
                    else
                    {
                        catQuery += " and cc.CategoryName = '" + condStr;
                    }
                }
                else if (filter.FilterType.Equals("prod"))
                {
                    string condStr = string.Empty;
                    if (filter.FilterField.ToLower().Equals("product name"))
                    {
                        condStr = " ProductName like '%" + filter.FilterValue + "%' ";
                    }
                    else if (filter.FilterField.ToLower().Equals("created") || filter.FilterField.ToLower().Equals("last modified"))
                    {
                        string columnName = "DateCreated";
                        if(filter.FilterField.ToLower().Equals("last modified"))
                        {
                            columnName = "DateModified";
                        }
                        switch (filter.FilterCond.ToLower()) {
                            case "is equals to":
                                condStr = " Date("+ columnName + ") = Date('" + filter.FilterValue + "') ";
                                break;
                            case "is not equals to":
                                condStr = " Date(" + columnName + ") <> Date('" + filter.FilterValue + "') ";
                                break;
                            case "is greater than":
                                condStr = " Date(" + columnName + ") > Date('" + filter.FilterValue + "') ";
                                break;
                            case "is greater or equal to":
                                condStr = " Date(" + columnName + ") >= Date('" + filter.FilterValue + "') ";
                                break;
                            case "is less than":
                                condStr = " Date(" + columnName + ") < Date('" + filter.FilterValue + "') ";
                                break;
                            case "is less or equal to":
                                condStr = " Date(" + columnName + ") <= Date('" + filter.FilterValue + "') ";
                                break;
                        }
                    }
                    if (!string.IsNullOrEmpty(condStr))
                    {
                        if (string.IsNullOrEmpty(prodQuery))
                        {
                            prodQuery = condStr;
                        }
                        else
                        {
                            prodQuery += " and " + condStr;
                        }
                    }
                }
            }
            

            string queryString = "select * from cm_product where IsValid = 1 and CompanyId = " + compId.ToString();

            if (!string.IsNullOrEmpty(innerQuery))
            {
                queryString += innerQuery;
            }
            if (!string.IsNullOrEmpty(prodQuery))
            {
                queryString += " and " + prodQuery;
            }

            queryString += " order by ProductName";


            DataSet ds = new DataSet();

            try
            {
                Utilities.ValidateSession(sessionId);
                ds = sqlHelper.ExecuteQuery(queryString);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return FillProductModel(ds);
        }

        public Product GetProductById(int compId, int prodId, string sessionId)
        {
            List<Product> details = new List<Product>();
            details = GetProducts(sessionId, compId, prodId, "", "");
            if (details != null && details.Count > 0)
            {
                return details[0];
            }
            else
            {
                return new Product();
            }
        }

        public List<Product> GetAllProductsByCategories(int compId, string catIds, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            List<Product> details = new List<Product>();
            DataSet ds = CatalogUtility.GetResultSet( "cm_getproductsByCategories", new List<string>() { "P_COMP_ID", "P_CAT_IDS", "P_FROM", "P_TO" }, new List<object>() { compId, catIds, 0, 100000 });
            details = FillProductModel(ds);
            return details;
        }


        public Product GetProductByName(int compId, string prodName, string sessionId)
        {
            List<Product> details = new List<Product>();
            details = GetProducts(sessionId, compId, 0, string.Empty, prodName);
            if (details != null && details.Count > 0)
            {
                return details[0];
            }
            else
            {
                return new Product();
            }
        }

        public List<Product> SearchProducts(int compId, string searchString, string sessionId)
        {
            DataSet ds = CatalogUtility.GetResultSet("cm_SearchProducts", new List<string>() { "P_COMP_ID", "P_SEARCH_TEXT" }, new List<object>() { compId, searchString });
            return FillProductModel(ds);
        }

        public List<Product> GetProductsByCategory(int compId, int catIds, string sessionId)
        {
            DataSet ds = CatalogUtility.GetResultSet("cm_CategoryProducts", new List<string>() { "P_COMP_ID", "P_CAT_IDS" }, new List<object>() { Convert.ToString(compId), catIds });
            return FillProductModel(ds);
        }

        public PRMName.Response GetUserMyCatalogFileId(int userid,string sessionid)
        {
            PRMName.Response response = new PRMName.Response();
            string query = string.Format("SELECT * FROM  UserData WHERE U_ID = {0};", userid);
            DataSet ds = sqlHelper.ExecuteQuery(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                response.Message = ds.Tables[0].Rows[0]["MY_CATALOG_FILE"] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0]["MY_CATALOG_FILE"]) : string.Empty;
            }

            return response;
        }

        public CatalogResponse AddProduct(Product reqProduct, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            CatalogResponse catalogResponse = new CatalogResponse();
            PRMServices prm = new PRMServices();
            //List<Product> data = new List<Product>();
            DataSet ds = new DataSet();


            string fileName = string.Empty;
            var filenameTemp = string.Empty;
            if (reqProduct.MultipleAttachments != null && reqProduct.MultipleAttachments.Count > 0)
            {
                foreach (PRMName.FileUpload fd in reqProduct.MultipleAttachments)
                {

                    if (fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
                    {

                        var attachName = string.Empty;

                        long tick = DateTime.UtcNow.Ticks;
                        attachName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + tick + "_user" + reqProduct.CreatedBy + "_" + fd.FileName);
                        prm.SaveFile(attachName, fd.FileStream);
                        //SaveFileAsync(fileName, attachment);

                        attachName = "req" + tick + "_user" + reqProduct.CreatedBy + "_" + fd.FileName;

                        PRMName.Response res = prm.SaveAttachment(attachName);
                        if (res.ErrorMessage != "")
                        {
                           
                        }

                        fd.FileID = res.ObjectID;


                        if (fd.FileID > 0)
                        {
                            Attachment singleAttachment = null;
                            var fileData = Utilities.DownloadFile(Convert.ToString(fd.FileID), sessionid, fd.FileStream, filenameTemp);
                            if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                            {
                                singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                            }
                        }

                    }
                    else if (fd.FileID > 0)
                    {
                        Attachment singleAttachment = null;
                        var fileData = Utilities.DownloadFile(Convert.ToString(fd.FileID), sessionid);
                        if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                        {
                            singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                        }
                    }

                    fileName += Convert.ToString(fd.FileID) + ",";

                }

                fileName = fileName.Substring(0, fileName.Length - 1);

            }


            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROD_ID", 0);
                sd.Add("P_COMP_ID", reqProduct.CompanyId);
                sd.Add("P_PROD_CODE", reqProduct.ProductCode == null ? string.Empty : reqProduct.ProductCode);
                sd.Add("P_PROD_NAME", reqProduct.ProductName);
                sd.Add("P_PROD_NO", reqProduct.ProductNo == null ? string.Empty : reqProduct.ProductNo);
                sd.Add("P_PROD_HSN", reqProduct.ProductHSNCode == null ? string.Empty : reqProduct.ProductHSNCode.Replace("'", ""));
                sd.Add("P_PROD_QTY", reqProduct.ProdQty);
                sd.Add("P_PROD_DESC", reqProduct.ProductDesc == null ? string.Empty : reqProduct.ProductDesc.Replace("'", ""));
                sd.Add("P_PROD_ISVALID", 1);
                sd.Add("P_PROD_ALTERNATIVE_UNITS", reqProduct.ProdAlternativeUnits == null ? string.Empty : reqProduct.ProdAlternativeUnits);
                sd.Add("P_UNIT_CONVERSION", reqProduct.UnitConversion == null ? string.Empty : reqProduct.UnitConversion);
                sd.Add("P_PRODUCT_VOLUME", reqProduct.ProductVolume == null ? string.Empty : reqProduct.ProductVolume);
                sd.Add("P_SHELF_LIFE", reqProduct.ShelfLife == null ? string.Empty : reqProduct.ShelfLife);
                
                sd.Add("P_PROD_GST", reqProduct.ProductGST);
                sd.Add("P_PREF_BRAND", reqProduct.PrefferedBrand == null ? string.Empty : reqProduct.PrefferedBrand.Replace("'", ""));
                sd.Add("P_ALTER_BRAND", reqProduct.AlternateBrand == null ? string.Empty : reqProduct.AlternateBrand.Replace("'", ""));
                sd.Add("P_TOT_PURCH_QTY", reqProduct.TotPurchaseQty);
                sd.Add("P_IN_TRANSIT", reqProduct.InTransit);
                sd.Add("P_LEAD_TIME", reqProduct.LeadTime == null ? string.Empty : reqProduct.LeadTime);
                sd.Add("P_DEPARTMENTS", reqProduct.Departments == null ? string.Empty : reqProduct.Departments);
                sd.Add("P_DELIVER_TREMS", reqProduct.DeliveryTerms);
                sd.Add("P_TERMS_CONDITIONS", reqProduct.TermsConditions);
                sd.Add("P_USER", reqProduct.ModifiedBy);
                sd.Add("P_ITEM_ATTACHMENTS", fileName);
                sd.Add("P_CAS_NUMBER", reqProduct.CasNumber == null ? string.Empty : reqProduct.CasNumber);
                sd.Add("P_MFCD_CODE", reqProduct.MfcdCode == null ? string.Empty : reqProduct.MfcdCode);
                //data.Add();
                ds = sqlHelper.SelectList("cm_saveproduct", sd);
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                catalogResponse.ObjectId = (ds.Tables[0].Rows[0][2] != DBNull.Value && !string.IsNullOrWhiteSpace(ds.Tables[0].Rows[0][2].ToString())) ? Convert.ToInt32(ds.Tables[0].Rows[0][2].ToString()) : -1;
                if (catalogResponse.ResponseId >=1 && reqProduct.ListVendorDetails != null && reqProduct.ListVendorDetails.Count > 0)
                {
                    //List<Product> ListVendorProducts = new List<Product>();
                    //foreach(ProductVendorDet)
                    //  SaveVendorCatalog(int vendorId, int compId, string catIds, string prodIds, int user, string sessionId);

                    DataSet dataset = new DataSet();
                    try
                    {
                        foreach (ContractManagementDetails contract in reqProduct.ContractManagement)
                        {
                            SortedDictionary<object, object> sd1 = new SortedDictionary<object, object>() { };
                            sd1.Add("P_PC_ID", catalogResponse.PcId);
                            sd1.Add("P_ProductId", catalogResponse.ResponseId);
                            sd1.Add("P_U_ID", contract.VendorId);
                            sd1.Add("P_number", contract.Number);
                            sd1.Add("P_value", contract.Value);
                            sd1.Add("P_quantity", contract.Quantity);
                            sd1.Add("P_availedQuantity", contract.AvailedQuantity);
                            sd1.Add("P_document", contract.Document);
                            sd1.Add("P_startTime", contract.StartTime);
                            sd1.Add("P_endTime", contract.EndTime);
                            sd1.Add("P_companyName", contract.CompanyName);
                            sd1.Add("P_isValid", 1);

                            dataset = sqlHelper.SelectList("cm_saveContractDetails", sd1);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message);
                    }

                    try
                    {
                        string deleteVendorProducts = string.Format("DELETE FROM cm_VendorProducts WHERE ProductId = {0};", catalogResponse.ResponseId);
                        sqlHelper.ExecuteNonQuery_IUD(deleteVendorProducts);
                        string vendorProduct = string.Empty;
                        foreach (PRMName.ProductVendorDetails pvd in reqProduct.ListVendorDetails)
                        {
                            vendorProduct += string.Format("INSERT INTO cm_VendorProducts(VendorId, ProductId,CompanyId, " +
                                "IsValid, ApprovalStatus, DateCreated, DateModified, CreatedBy, ModifiedBy) values({0}, {1}, {2}, {3}, {4}, {5}, " +
                                "{6}, {7}, {8})", pvd.VendorID, catalogResponse.ResponseId, reqProduct.CompanyId, 1, "'PENDING'", "NOW()", "NOW()",
                                reqProduct.ModifiedBy, reqProduct.ModifiedBy) + ";";
                        }

                        sqlHelper.ExecuteNonQuery_IUD(vendorProduct);

                    }
                    catch (Exception ex)
                    {
                        //response.ErrorMessage = ex.Message;
                    }
                }
                if(catalogResponse.ResponseId == -1 && ds.Tables[0].Columns.Count > 1)
                {
                    catalogResponse.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : "Invalid";
                }
            }

            catalogResponse.SessionID = sessionid;
            return catalogResponse;
        }

        public List<ContractManagementDetails> GetPendingContracts(int compid, string sessionid, string productName, string supplierName, string status, string startDate, string endDate,string search)
        {
            Utilities.ValidateSession(sessionid);
            List<ContractManagementDetails> details = new List<ContractManagementDetails>();

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_COMP_ID", compid);
            sd.Add("P_PROD_NAME", productName);
            sd.Add("P_SUPPLIER_NAME", supplierName);
            sd.Add("P_STATUS", status);
            sd.Add("P_START_DATE", startDate);
            sd.Add("P_END_DATE", endDate);
            sd.Add("P_SEARCH", search);
            DataSet ds = sqlHelper.SelectList("cm_GetContracts",sd); //ExecuteQuery(query);
            if(ds!=null & ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach(var row in ds.Tables[0].AsEnumerable())
                {
                    ContractManagementDetails detail = new ContractManagementDetails();

                    detail.AvailedQuantity = row["AvailedQuantity"] != DBNull.Value ? Convert.ToInt32(row["AvailedQuantity"]) : 0;
                    detail.Quantity = row["Quantity"] != DBNull.Value ? Convert.ToInt32(row["Quantity"]) : 0;
                    detail.StartTime = row["StartTime"] != DBNull.Value ? Convert.ToDateTime(row["StartTime"]) : DateTime.MaxValue;
                    detail.EndTime = row["EndTime"] != DBNull.Value ? Convert.ToDateTime(row["EndTime"]) : DateTime.MaxValue;
                    detail.ProductId = row["ProductId"] != DBNull.Value ? Convert.ToInt32(row["ProductId"]) : 0;
                    detail.PC_ID = row["PC_ID"] != DBNull.Value ? Convert.ToInt32(row["PC_ID"]) : 0;
                    detail.VendorId = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    detail.ProductName = row["ProductName"] != DBNull.Value ? Convert.ToString(row["ProductName"]) : string.Empty;
                    detail.CompanyName = row["CompanyName"] != DBNull.Value ? Convert.ToString(row["CompanyName"]) : string.Empty;
                    detail.ContractNumber =row["Number"] != DBNull.Value ? Convert.ToString(row["Number"]) : string.Empty;
                    detail.ContractValue = row["Value"] != DBNull.Value ? Convert.ToDecimal(row["Value"]) : 0;
                    detail.Categories = row["Categories"] != DBNull.Value ? Convert.ToString(row["Categories"]) : string.Empty;
                    detail.Document = row["document"] != DBNull.Value ? Convert.ToString(row["document"]) : string.Empty;
                    detail.PhoneNumber = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                    detail.SupplierName = row["SUPPLIER_NAME"] != DBNull.Value ? Convert.ToString(row["SUPPLIER_NAME"]) : string.Empty;
                    detail.PONumber = row["PURCHASE_ORDER_ID"] != DBNull.Value ? Convert.ToString(row["PURCHASE_ORDER_ID"]) : string.Empty;
                    detail.PriceType = row["PRICE_TYPE"] != DBNull.Value ? Convert.ToString(row["PRICE_TYPE"]) : string.Empty;
                    detail.ContractStatus = row["ContractStatus"] != DBNull.Value ? Convert.ToString(row["ContractStatus"]) : string.Empty;
                    detail.AllocationPercentage = row["ALLOC_PERC"] != DBNull.Value ? Convert.ToDecimal(row["ALLOC_PERC"]) : 0;
                    detail.REQ_ID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    detail.QCS_ID = row["QCS_ID"] != DBNull.Value ? Convert.ToInt32(row["QCS_ID"]) : 0;
                    detail.ITEM_ID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
                    detail.EXCHANGE_RATE = row["EXCH_RATE"] != DBNull.Value ? Convert.ToDecimal(row["EXCH_RATE"]) : 0;
                    detail.SITE_CODE = row["SITE_CODE"] != DBNull.Value ? Convert.ToString(row["SITE_CODE"]) : string.Empty;
                    detail.VENDOR_SITE_CODE = row["VENDOR_SITE_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_SITE_CODE"]) : string.Empty;
                    detail.PLANT_CODE = row["PLANT_CODE"] != DBNull.Value ? Convert.ToString(row["PLANT_CODE"]) : string.Empty;
                    detail.VENDOR_CODE = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                    detail.MODIFIED_DATE = row["DATE_MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_MODIFIED"]) : DateTime.MaxValue;
                    detail.VENDOR_COMPANY_ID = row["VENDOR_COMPANY_ID"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_COMPANY_ID"]) : 0;
                    detail.PR_LINE_ITEM_INFO = row["PR_LINE_ITEM_INFO"] != DBNull.Value ? Convert.ToString(row["PR_LINE_ITEM_INFO"]) : string.Empty;
                    details.Add(detail);
                }
            }
            
            
            return details;
        }

        public List<ContractManagementDetails> GetProductContracts(string productids, string sessionid, int includeCondition = 1)
        {
            Utilities.ValidateSession(sessionid);
            if (!string.IsNullOrWhiteSpace(productids))
            {
                productids = string.Join(",", productids.Split(',').ToList().Distinct());
            }
            List<ContractManagementDetails> details = new List<ContractManagementDetails>();


            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_PRODUCT_IDS", productids);
            sd.Add("P_VALID_CONTRACTS", includeCondition);
            //string query = $@"select PC.*, C.COMPANY_NAME, P.ProductCode, P.ProductName,
            //                (case when (PC.IsValid = 0 or PC.Quantity = PC.AvailedQuantity or convert(date,EndTime) < convert(date,NOW())) then 'Closed' else 'Active' end) as ContractStatus
            //                From productcontractdetails PC INNER JOIN cm_product P ON P.ProductId= PC.ProductID 
            //                INNER JOIN company C ON C.COMP_ID = dbo.GetCompanyId(PC.U_ID) WHERE P.productid IN ({productids})
            //                and (case when {includeCondition} = 1 then (case when PC.IsValid = 1 AND PC.Quantity <> PC.AvailedQuantity AND convert(date,EndTime) >= convert(date,NOW()) then 1 else 0 end) else 1 end) = 1 ";
            DataSet ds = sqlHelper.SelectList("cm_GetProductContracts", sd); //ExecuteQuery(query);
            //DataSet ds = sqlHelper.ExecuteQuery(query);
            if (ds != null & ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (var row in ds.Tables[0].AsEnumerable())
                {
                    ContractManagementDetails detail = new ContractManagementDetails();

                    detail.PC_ID = row["PC_ID"] != DBNull.Value ? Convert.ToInt32(row["PC_ID"]) : 0;
                    detail.AvailedQuantity = row["AvailedQuantity"] != DBNull.Value ? Convert.ToDecimal(row["AvailedQuantity"]) : 0;
                    detail.Price = row["Value"] != DBNull.Value ? Convert.ToDouble(row["Value"]) : 0;
                    detail.Value = row["Value"] != DBNull.Value ? Convert.ToDecimal(row["Value"]) : 0;
                    detail.IsValid = row["IsValid"] != DBNull.Value ? Convert.ToInt32(row["IsValid"]) : 0;
                    detail.Quantity = row["Quantity"] != DBNull.Value ? Convert.ToDecimal(row["Quantity"]) : 0;
                    detail.StartTime = row["StartTime"] != DBNull.Value ? Convert.ToDateTime(row["StartTime"]) : DateTime.MaxValue;
                    detail.EndTime = row["EndTime"] != DBNull.Value ? Convert.ToDateTime(row["EndTime"]) : DateTime.MaxValue;
                    detail.ProductId = row["ProductId"] != DBNull.Value ? Convert.ToInt32(row["ProductId"]) : 0;
                    detail.PRODUCT_CODE = row["ProductCode"] != DBNull.Value ? Convert.ToString(row["ProductCode"]) : string.Empty;
                    detail.ProductName = row["ProductName"] != DBNull.Value ? Convert.ToString(row["ProductName"]) : string.Empty;
                    //detail.VendorPartNumber = row["VendorPartNumber"] != DBNull.Value ? Convert.ToString(row["VendorPartNumber"]) : string.Empty;
                    detail.CompanyName = row["CompanyName"] != DBNull.Value ? Convert.ToString(row["CompanyName"]) : string.Empty;
                    detail.Document = row["document"] != DBNull.Value ? Convert.ToString(row["document"]) : string.Empty;
                    detail.Number = row["Number"] != DBNull.Value ? Convert.ToString(row["Number"]) : string.Empty;
                    detail.ContractNumber = row["Number"] != DBNull.Value ? Convert.ToString(row["Number"]) : string.Empty;
                    detail.VendorId = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    detail.VENDOR_CODE = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                    detail.U_ID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    detail.SelectedVendorCode = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                    detail.PAYMENT_TERMS = row["PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["PAYMENT_TERMS"]) : string.Empty;
                    detail.INCO_TERMS = row["INCO_TERMS"] != DBNull.Value ? Convert.ToString(row["INCO_TERMS"]) : string.Empty;
                    detail.DELIVERY_TERMS = row["DELIVERY_TERMS"] != DBNull.Value ? Convert.ToString(row["DELIVERY_TERMS"]) : string.Empty;
                    detail.GENERAL_TERMS = row["TERMS"] != DBNull.Value ? Convert.ToString(row["TERMS"]) : string.Empty;
                    detail.PLANT_CODE = row["PLANT_CODE"] != DBNull.Value ? Convert.ToString(row["PLANT_CODE"]) : string.Empty;

                    detail.DELIVERY_FROM = row["DELIVERY_FROM"] != DBNull.Value ? Convert.ToString(row["DELIVERY_FROM"]) : string.Empty;
                    detail.DELIVERY_AT = row["DELIVERY_AT"] != DBNull.Value ? Convert.ToString(row["DELIVERY_AT"]) : string.Empty;
                    detail.LEADTIME = row["LEAD_TIME"] != DBNull.Value ? Convert.ToString(row["LEAD_TIME"]) : string.Empty;
                    detail.WARRANTY = row["WARRANTY"] != DBNull.Value ? Convert.ToString(row["WARRANTY"]) : string.Empty;
                    detail.SPECIAL_INSTRUCTIONS = row["SPECIAL_INSTRUCTIONS"] != DBNull.Value ? Convert.ToString(row["SPECIAL_INSTRUCTIONS"]) : string.Empty;
                    detail.DISCOUNT_ELIGIBILITY = row["DISCOUNT_ELIGIBILITY"] != DBNull.Value ? Convert.ToString(row["DISCOUNT_ELIGIBILITY"]) : string.Empty;
                    detail.RECONCILIATION_TIMELINES = row["RECONCILIATION_TIMELINES"] != DBNull.Value ? Convert.ToString(row["RECONCILIATION_TIMELINES"]) : string.Empty;
                    detail.CURRENCY = row["CURRENCY"] != DBNull.Value ? Convert.ToString(row["CURRENCY"]) : string.Empty;
                    detail.EXCHANGE_RATE = row["EXCH_RATE"] != DBNull.Value ? Convert.ToDecimal(row["EXCH_RATE"]) : 0;
                    detail.PO_CURRENCY = row["PO_CURRENCY"] != DBNull.Value ? Convert.ToString(row["PO_CURRENCY"]) : string.Empty;
                    detail.GST = row["GST"] != DBNull.Value ? Convert.ToDecimal(row["GST"]) : 0;
                    detail.CUSTOM_DUTY = row["CUSTOM_DUTY"] != DBNull.Value ? Convert.ToDecimal(row["CUSTOM_DUTY"]) : 0;
                    detail.DISCOUNT = row["DISCOUNT"] != DBNull.Value ? Convert.ToDecimal(row["DISCOUNT"]) : 0;
                    detail.NET_PRICE = row["NET_PRICE"] != DBNull.Value ? Convert.ToDecimal(row["NET_PRICE"]) : 0;
                    detail.PriceType = row["PRICE_TYPE"] != DBNull.Value ? Convert.ToString(row["PRICE_TYPE"]) : string.Empty;
                    detail.ContractStatus = row["ContractStatus"] != DBNull.Value ? Convert.ToString(row["ContractStatus"]) : string.Empty;
                    detail.AllocationPercentage = row["ALLOC_PERC"] != DBNull.Value ? Convert.ToDecimal(row["ALLOC_PERC"]) : 0;
                    detail.REQ_ID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    detail.QCS_ID = row["QCS_ID"] != DBNull.Value ? Convert.ToInt32(row["QCS_ID"]) : 0;
                    detail.ITEM_ID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
                    detail.PLANT = row["DELIERY_DETAILS"] != DBNull.Value ? Convert.ToString(row["DELIERY_DETAILS"]) : string.Empty;
                    detail.SITE_CODE = row["SITE_CODE"] != DBNull.Value ? Convert.ToString(row["SITE_CODE"]) : string.Empty;
                    detail.VENDOR_SITE_CODE = row["VENDOR_SITE_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_SITE_CODE"]) : string.Empty;
                    details.Add(detail);
                }
            }

            return details;
        }

        //public CatalogResponse saveProductVendorDetails(int productId,List<PRMName.ProductVendorDetails> vendorDetails) {
        ////    Utilities.ValidateSession(sessionid, null);
        // //   CatalogResponse catalogResponse = new CatalogResponse();
        //      CatalogResponse catalogVendResp = new CatalogResponse();
        //      DataSet ds = new DataSet();

        //    int loopcount = 0;

        //    string queryValues = string.Empty;
        //    int count = 0;
        //    string delete = string.Format("DELETE FROM cm_VendorProducts WHERE ProductId = {0};", productId);
        //    string query = delete + "INSERT INTO cm_VendorProducts(VendorId, ProductId, CompanyId, IsValid, DateCreated, DateModified, CreatedBy," +
        //        " ModifiedBy, ApprovalStatus) Values";

        //    foreach (Product vendDet in vendorDetails)
        //    {
        //        string columns = "({0},{1},{2},~$^{3}~$^,~$^{4}~$^,~$^{5}~$^,~$^{6},~$^{7},~$^{8}),";

        //        queryValues = queryValues + string.Format(columns,vendDet.ListVendorDetails);


        //        count++;

        //    }




        //    return catalogVendResp;
        //}


        public CatalogResponse UpdateProduct(Product reqProduct, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            CatalogResponse catalogResponse = new CatalogResponse();
            PRMServices prm = new PRMServices();
            DataSet ds = new DataSet();

            string fileName = string.Empty;
            var filenameTemp = string.Empty;
            if (reqProduct.MultipleAttachments != null && reqProduct.MultipleAttachments.Count > 0)
            {
                foreach (PRMName.FileUpload fd in reqProduct.MultipleAttachments)
                {

                    if (fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
                    {

                        var attachName = string.Empty;

                        long tick = DateTime.UtcNow.Ticks;
                        attachName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + tick + "_user" + reqProduct.CreatedBy + "_" + fd.FileName);
                        prm.SaveFile(attachName, fd.FileStream);
                        //SaveFileAsync(fileName, attachment);

                        attachName = "req" + tick + "_user" + reqProduct.CreatedBy + "_" + fd.FileName;

                        PRMName.Response res = prm.SaveAttachment(attachName);
                        if (res.ErrorMessage != "")
                        {

                        }

                        fd.FileID = res.ObjectID;


                        if (fd.FileID > 0)
                        {
                            Attachment singleAttachment = null;
                            var fileData = Utilities.DownloadFile(Convert.ToString(fd.FileID), sessionid, fd.FileStream, filenameTemp);
                            if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                            {
                                singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                            }
                        }

                    }
                    else if (fd.FileID > 0)
                    {
                        Attachment singleAttachment = null;
                        var fileData = Utilities.DownloadFile(Convert.ToString(fd.FileID), sessionid);
                        if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                        {
                            singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                        }
                    }

                    fileName += Convert.ToString(fd.FileID) + ",";

                }

                fileName = fileName.Substring(0, fileName.Length - 1);

            }

            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROD_ID", reqProduct.ProductId);
                sd.Add("P_COMP_ID", reqProduct.CompanyId);
                sd.Add("P_PROD_CODE", reqProduct.ProductCode == null ? "" : reqProduct.ProductCode);
                sd.Add("P_PROD_NAME", reqProduct.ProductName);
                sd.Add("P_PROD_DESC", reqProduct.ProductDesc == null ? string.Empty : reqProduct.ProductDesc);
                sd.Add("P_PROD_NO", reqProduct.ProductNo);
                sd.Add("P_PROD_HSN", reqProduct.ProductHSNCode);
                sd.Add("P_PROD_QTY", reqProduct.ProdQty == null ? string.Empty : reqProduct.ProdQty);
                sd.Add("P_PROD_ISVALID", reqProduct.IsValid);
                sd.Add("P_USER", reqProduct.ModifiedBy);
                sd.Add("P_PROD_ALTERNATIVE_UNITS", reqProduct.ProdAlternativeUnits == null?string.Empty: reqProduct.ProdAlternativeUnits);
                sd.Add("P_UNIT_CONVERSION", reqProduct.UnitConversion == null ? string.Empty : reqProduct.UnitConversion);
                sd.Add("P_PRODUCT_VOLUME", reqProduct.ProductVolume == null ? string.Empty : reqProduct.ProductVolume);
                sd.Add("P_SHELF_LIFE", reqProduct.ShelfLife == null ? string.Empty : reqProduct.ShelfLife);

                sd.Add("P_PROD_GST", reqProduct.ProductGST);
                sd.Add("P_PREF_BRAND", reqProduct.PrefferedBrand == null ? string.Empty : reqProduct.PrefferedBrand);
                sd.Add("P_ALTER_BRAND", reqProduct.AlternateBrand == null ? string.Empty : reqProduct.AlternateBrand);
                sd.Add("P_TOT_PURCH_QTY", reqProduct.TotPurchaseQty);
                sd.Add("P_IN_TRANSIT", reqProduct.InTransit);
                sd.Add("P_LEAD_TIME", reqProduct.LeadTime == null ? string.Empty : reqProduct.LeadTime);
                sd.Add("P_DEPARTMENTS", reqProduct.Departments == null ? string.Empty : reqProduct.Departments);
                sd.Add("P_DELIVER_TREMS", reqProduct.DeliveryTerms == null ? string.Empty : reqProduct.DeliveryTerms);
                sd.Add("P_TERMS_CONDITIONS", reqProduct.TermsConditions == null ? string.Empty : reqProduct.TermsConditions);

                sd.Add("P_CAS_NUMBER", reqProduct.CasNumber == null ? string.Empty : reqProduct.CasNumber);
                sd.Add("P_MFCD_CODE", reqProduct.MfcdCode == null ? string.Empty : reqProduct.MfcdCode);
                sd.Add("P_ITEM_ATTACHMENTS", fileName);

                ds = sqlHelper.SelectList("cm_saveproduct", sd);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                //int delResp = DeleteVendorCatalog(catalogResponse.ResponseId, sessionid);
                //if (delResp == 0)
                //{
                //foreach (PRMName.ProductVendorDetails pvd in reqProduct.ListVendorDetails)
                //    {
                //    // saveProductVendorDetails(reqProduct.ProductId, pvd);
                //      SaveVendorCatalog(pvd.VendorID, reqProduct.CompanyId, "", catalogResponse.ResponseId.ToString(), Convert.ToUInt16(reqProduct.ModifiedBy), sessionid);//

                //    }
                //}

                DataSet dataset = new DataSet();
                try
                {
                    //   reqProduct.ContractManagement = new ContractManagement();

                    foreach (ContractManagementDetails contract in reqProduct.ContractManagement)
                    {
                        SortedDictionary<object, object> sd1 = new SortedDictionary<object, object>() { };
                        sd1.Add("P_PC_ID", contract.PC_ID);
                        sd1.Add("P_ProductId", contract.ProductId);
                        sd1.Add("P_U_ID", contract.VendorId);
                        sd1.Add("P_VENDOR_CODE", contract.SelectedVendorCode);
                        sd1.Add("P_companyName", contract.CompanyName);
                        sd1.Add("P_number", contract.Number);
                        sd1.Add("P_value", contract.Value);
                        sd1.Add("P_quantity", contract.Quantity);
                        sd1.Add("P_availedQuantity", contract.AvailedQuantity);
                        sd1.Add("P_document", contract.Document);
                        sd1.Add("P_startTime", contract.StartTime);
                        sd1.Add("P_endTime", contract.EndTime);
                        sd1.Add("P_PAYMENT_TERMS", contract.PAYMENT_TERMS);
                        sd1.Add("P_INCO_TERMS", contract.INCO_TERMS);
                        sd1.Add("P_DELIVERY_TERMS", contract.DELIVERY_TERMS);
                        sd1.Add("P_TERMS", contract.GENERAL_TERMS);
                        sd1.Add("P_isValid", 1);
                        sd1.Add("P_PLANT_CODE", contract.PLANT_CODE);
                        sd1.Add("P_DELIVERY_FROM", contract.DELIVERY_FROM);
                        sd1.Add("P_DELIVERY_AT", contract.DELIVERY_AT);
                        sd1.Add("P_LEAD_TIME", contract.LEADTIME);
                        sd1.Add("P_WARRANTY", contract.WARRANTY);
                        sd1.Add("P_SPECIAL_INSTRUCTIONS", contract.SPECIAL_INSTRUCTIONS);
                        sd1.Add("P_NET_PRICE", contract.NET_PRICE);
                        sd1.Add("P_DISCOUNT", contract.DISCOUNT);
                        sd1.Add("P_GST", contract.GST);
                        sd1.Add("P_CUSTOM_DUTY", contract.CUSTOM_DUTY);
                        sd1.Add("P_DISCOUNT_ELIGIBILITY", contract.DISCOUNT_ELIGIBILITY);
                        sd1.Add("P_RECONCILIATION_TIMELINES", contract.RECONCILIATION_TIMELINES);
                        sd1.Add("P_CURRENCY", contract.CURRENCY);
                        sd1.Add("P_EXCH_RATE", contract.EXCHANGE_RATE);
                        sd1.Add("P_PO_CURRENCY", contract.PO_CURRENCY);
                        sd1.Add("P_USER", contract.USER);
                        sd1.Add("P_PRICE_TYPE", contract.PriceType);

                        dataset = sqlHelper.SelectList("cm_saveContractDetails", sd1);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                }


                try
                {
                    string deleteVendorProducts = string.Format("DELETE FROM cm_VendorProducts WHERE ProductId = {0};", catalogResponse.ResponseId);
                    sqlHelper.ExecuteNonQuery_IUD(deleteVendorProducts);
                    string newVendorQuery = string.Empty;
                    foreach (PRMName.ProductVendorDetails pvd in reqProduct.ListVendorDetails)
                    {
                        newVendorQuery += string.Format("INSERT INTO cm_VendorProducts(VendorId, ProductId,CompanyId, " +
                            "IsValid, ApprovalStatus, DateCreated, DateModified, CreatedBy, ModifiedBy) values({0}, {1}, {2}, {3}, {4}, {5}, " +
                            "{6}, {7}, {8})", pvd.VendorID, catalogResponse.ResponseId, reqProduct.CompanyId, 1, "'PENDING'", "NOW()", "NOW()",
                            reqProduct.ModifiedBy, reqProduct.ModifiedBy)+ ";";
                    }

                    sqlHelper.ExecuteNonQuery_IUD(newVendorQuery);

                }
                catch (Exception ex)
                {
                    //response.ErrorMessage = ex.Message;
                }

                if (catalogResponse.ResponseId == -1 && ds.Tables[0].Columns.Count > 1)
                {
                    catalogResponse.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : "";
                }
            }

            catalogResponse.SessionID = sessionid;
            return catalogResponse;
        }


        public int DeleteVendorCatalog(int ProductID, string sessionID)
        {
            int a = -1;

            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                string query = string.Format("DELETE FROM cm_vendorproducts where ProductId = {0};", ProductID);
                DataSet ds = sqlHelper.ExecuteQuery(query);
               // ds.
                if (ds != null)
                {
                    a = 0;
                }

            }
            catch (Exception ex)
            {
                
            }

            return a;
        }

        public CatalogResponse updateproductcategories(int prodId, int compId, string catIds, int user, int statusCheck, string sessionId)
        {
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROD_ID", prodId);
                sd.Add("P_COMP_ID", compId);
                sd.Add("P_CAT_IDS", catIds);

                sd.Add("P_CHECKED", statusCheck);
                sd.Add("P_USER", user);
                ds = sqlHelper.SelectList("cm_saveproductcategories", sd);
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                catalogResponse.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
            }

            catalogResponse.SessionID = sessionId;
            return catalogResponse;
        }

        public CatalogResponse DeleteProduct(Product reqProduct, string sessionid)
        {
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROD_ID", reqProduct.ProductId);
                sd.Add("P_COMP_ID", reqProduct.CompanyId);
                sd.Add("P_PROD_CODE", reqProduct.ProductCode == null ? "" : reqProduct.ProductCode);
                sd.Add("P_ISVALID", reqProduct.IsValid);
                sd.Add("P_USER", reqProduct.CreatedBy);
                ds = sqlHelper.SelectList("cm_deleteproduct", sd);
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                if(ds.Tables[0].Columns.Count > 1 && Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) == -1)
                {
                    catalogResponse.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? ds.Tables[0].Rows[0][1].ToString() : "";
                }
            }

            catalogResponse.SessionID = sessionid;
            return catalogResponse;
        }

        public CatalogResponse IsProdudtEditAllowed(Product reqProduct, string sessionid)
        {
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROD_ID", reqProduct.ProductId);
                sd.Add("P_COMP_ID", reqProduct.CompanyId);
                sd.Add("P_USER", reqProduct.CreatedBy);
                ds = sqlHelper.SelectList("cm_isproducteditallowed", sd);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                if (ds.Tables[0].Columns.Count > 1 && Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) == -1)
                {
                    catalogResponse.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? ds.Tables[0].Rows[0][1].ToString() : "";
                }
            }

            catalogResponse.SessionID = sessionid;
            return catalogResponse;
        }

        public List<PropertyModel> GetProperties(int CompanyId, int entityId, int entityType, string sessionId)
        {
            List<PropertyModel> Properties = new List<PropertyModel>();
            DataSet ds = CatalogUtility.GetResultSet("cm_GetProperties", new List<string>() { "P_COMP_ID", "P_ENTITY_ID", "P_ENITY_TYPE" }, new List<object>() { CompanyId, entityId, entityType });
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    PropertyModel property = new PropertyModel();
                    try
                    {
                        CatalogUtility.SetItemFromRow(property, dr);
                    }
                    catch (Exception ex)
                    {
                        property = new PropertyModel();
                        property.ErrorMessage = ex.Message;
                    }

                    Properties.Add(property);
                }
            }

            return Properties;
        }

        public List<AttributeModel> GetAttribute(int CompanyId, string sessionId)
        {
            List<AttributeModel> Prop = new List<AttributeModel>();
            DataSet ds = CatalogUtility.GetResultSet("cm_getattributes", new List<string>() { "P_COMP_ID" }, new List<object>() { CompanyId });
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    AttributeModel property = new AttributeModel();
                    try
                    {
                        CatalogUtility.SetItemFromRow(property, dr);
                    }
                    catch (Exception ex)
                    {
                        property = new AttributeModel();
                        property.ErrorMessage = ex.Message;
                    }

                    Prop.Add(property);
                }
            }

            return Prop;
        }

        public CatalogResponse SaveCatalogProperty(PropertyModel property, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROP_ID", property.propId);
                sd.Add("P_COMP_ID", property.CompanyId);
                sd.Add("P_PROP_NAME", property.propName);
                sd.Add("P_PROP_DESC", property.propDesc == null ? "": property.propDesc);
                sd.Add("P_PROP_TYPE", property.propType);
                sd.Add("P_PROP_DATATYPE", property.propDataType);
                sd.Add("P_PROP_OPTIONS", property.propOptions == null ? "" : property.propOptions);
                sd.Add("P_PROP_DEFAULTVALUE", property.propDefaultValue == null ? "" : property.propDefaultValue);
                sd.Add("P_ISVALID", property.IsValid);
                sd.Add("P_USER", property.U_Id);
                ds = sqlHelper.SelectList("cm_savecatalogproperty", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (catalogResponse.ResponseId == -1 && ds.Tables[0].Columns.Count > 1)
                    {
                        catalogResponse.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1]) : "unknowwn error";
                    }
                }

                catalogResponse.SessionID = sessionId;
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
            }

            return catalogResponse;
        }

        public CatalogResponse SaveEntityProperties(List<PropertyEntityModel> propertyobj, string sessionId)
        {
            CatalogResponse catalogResponse = new CatalogResponse();

            string query = string.Empty;
            foreach (PropertyEntityModel pem in propertyobj)
            {
                query += $"CALL cm_saveentityproperty({pem.entityId}, {pem.propId}, {pem.companyId}, '{pem.propValue}', {pem.IsValid}, {pem.U_Id});";
            }

            sqlHelper.ExecuteNonQuery_IUD(query);
            catalogResponse.ResponseId = propertyobj.Count;
            catalogResponse.SessionID = sessionId;
            return catalogResponse;
        }


        public CatalogResponse ImportEntity(ImportEntity entity, int compId, int user, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            string moreInfo = string.Empty;
            CatalogResponse catalogResponse = new CatalogResponse();
            string sheetName = string.Empty;
            DataTable currentData = new DataTable();

            if (entity.Attachment != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    ms.Write(entity.Attachment, 0, entity.Attachment.Length);
                    using (ExcelPackage package = new ExcelPackage(ms))
                    {
                        currentData = StoreUtility.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                        sheetName = package.Workbook.Worksheets[1].Name;
                    }
                }
            }
            if (string.Compare(entity.EntityName, "products", StringComparison.InvariantCultureIgnoreCase) == 0 && currentData.Rows.Count > 0)
            {
                if (!sheetName.Equals("products", StringComparison.InvariantCultureIgnoreCase))
                {
                    return catalogResponse;
                }
            }

            DataSet ds = new DataSet();
            int count = 0;
            if (currentData.Rows.Count > 0)
            {
                foreach (DataRow row in currentData.Rows)
                {
                    string itemName = Convert.ToString(row["ProductName"]);
                    string itemNo = Convert.ToString(row["ProductNo"]);
                    //string itemCode = Convert.ToString(row["ProductCode"]);
                    string itemHSN = Convert.ToString(row["HSNCode"]);
                    string itemUnits = Convert.ToString(row["QuantityUnits"]);
                    string itemDesc = Convert.ToString(row["ProductDescription"]);
                    string itemCategories = Convert.ToString(row["ProductCategories"]);
                    string itemUnitConversion = "";
                    string itemProductVolume = "";
                    

                    // string itemProperties = Convert.ToString(row["ITEM_PROPERTIES"]);

                    if (!(string.IsNullOrEmpty(itemName)) && !(string.IsNullOrEmpty(itemUnits)))// || string.IsNullOrEmpty(itemNo) || string.IsNullOrEmpty(itemUnits)))
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        try
                        {
                            sd.Add("P_PROD_ID", 0);
                            sd.Add("P_COMP_ID", compId);
                            sd.Add("P_PROD_CODE", "");
                            sd.Add("P_PROD_NAME", itemName);
                            sd.Add("P_PROD_NO", null == itemNo ? "" : itemNo);
                            sd.Add("P_PROD_HSN", null == itemHSN ? "" : itemHSN);
                            sd.Add("P_PROD_QTY", null == itemUnits ? "" : itemUnits);
                            sd.Add("P_PROD_DESC", null == itemDesc ? "" : itemDesc);
                            sd.Add("P_PROD_ISVALID", 1);
                            sd.Add("P_PROD_ALTERNATIVE_UNITS", Convert.ToString(row["AlternativeQuantityUnits"]));
                            sd.Add("P_UNIT_CONVERSION", itemUnitConversion);
                            sd.Add("P_PRODUCT_VOLUME", itemProductVolume);
                            sd.Add("P_SHELF_LIFE", Convert.ToString(row["ShelfLife"]));
                            sd.Add("P_USER", user);

                            sd.Add("P_PROD_GST", 0.00);
                            sd.Add("P_PREF_BRAND", "");
                            sd.Add("P_ALTER_BRAND", "");
                            sd.Add("P_TOT_PURCH_QTY", 0);
                            sd.Add("P_IN_TRANSIT", 0);
                            sd.Add("P_LEAD_TIME", "");
                            sd.Add("P_DEPARTMENTS", "");
                            sd.Add("P_DELIVER_TREMS", "");
                            sd.Add("P_TERMS_CONDITIONS", "");
                            sd.Add("P_ITEM_ATTACHMENTS", "");
                            sd.Add("P_CAS_NUMBER", "" );
                            sd.Add("P_MFCD_CODE", "");

                            ds = sqlHelper.SelectList("cm_saveproduct", sd);
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                            {
                                int itemId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                                if (itemId > 0)
                                {
                                    count++;
                                    if (!string.IsNullOrWhiteSpace(itemCategories))
                                    {
                                        try
                                        {
                                            SortedDictionary<object, object> sdCat = new SortedDictionary<object, object>() { };
                                            sdCat.Add("P_COMP_ID", compId);
                                            sdCat.Add("P_CAT_NAMES", itemCategories);
                                            sdCat.Add("P_PROD_ID", itemId);
                                            sdCat.Add("P_USER", user);
                                            DataSet dsCat = sqlHelper.SelectList("cm_importItemCategory", sdCat);
                                        }
                                        catch (Exception ex)
                                        {
                                            logger.Error(ex.Message);
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.Message);
                        }
                    }
                    else
                    {
                        catalogResponse.ErrorMessage = "ProductName/ProductQuantityUnits should not be Empty";
                    }
                }
            }
            else
            {
                catalogResponse.ErrorMessage = "Please Fill All the Mandatory Fields";
            }
            catalogResponse.ResponseId = count;
            return catalogResponse;
        }

        public CatalogResponse ImportCatalogueCategories(ImportEntity entity, int compId, int user, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            string moreInfo = string.Empty;
            CatalogResponse catalogResponse = new CatalogResponse();
            string sheetName = string.Empty;
            DataTable currentData = new DataTable();

            if (entity.Attachment != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    ms.Write(entity.Attachment, 0, entity.Attachment.Length);
                    using (ExcelPackage package = new ExcelPackage(ms))
                    {
                        currentData = StoreUtility.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                        sheetName = package.Workbook.Worksheets[1].Name;
                    }
                }
            }
            if (string.Compare(entity.EntityName, "categories", StringComparison.InvariantCultureIgnoreCase) == 0 && currentData.Rows.Count > 0)
            {
                if (!sheetName.Equals("categories", StringComparison.InvariantCultureIgnoreCase))
                {
                    catalogResponse.ErrorMessage = "sheet name should be categories";
                    return catalogResponse;
                }
            }

            int count = 0;
            foreach (DataRow row in currentData.Rows)
            {
                string itemName = Convert.ToString(row["CategoryName"]);
                string itemDepts = Convert.ToString(row["CategoryDepartments"]);

                if (!string.IsNullOrEmpty(itemName))
                {
                    SortedDictionary<object, object> sdCat = new SortedDictionary<object, object>() { };
                   
                    try
                    {
                        sdCat.Add("P_COMP_ID", compId);
                        sdCat.Add("P_CAT_NAMES", itemName);
                        sdCat.Add("P_CAT_DEPTS", itemDepts);
                        sdCat.Add("P_USER", user);
                        DataSet dsCat = sqlHelper.SelectList("cm_importCatalogueCategory", sdCat);

                        if (dsCat != null && dsCat.Tables.Count > 0 && dsCat.Tables[0].Rows.Count > 0 && dsCat.Tables[0].Rows[0][0] != null)
                        {
                            int itemId = dsCat.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dsCat.Tables[0].Rows[0][0].ToString()) : -1;
                            if (itemId > 0)
                            {
                                count++;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message);
                    }
                }
            }
            catalogResponse.ResponseId = count;
            return catalogResponse;
        }

        public CatalogResponse ImportVendorItemCategories(ImportEntity entity, int compId, int vendorId, int user, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            string moreInfo = string.Empty;
            CatalogResponse catalogResponse = new CatalogResponse();
            string sheetName = string.Empty;
            DataTable currentData = new DataTable();

            if (entity.Attachment != null)
            {
                if(!string.Equals(entity.EntityName, "MyCategories", StringComparison.InvariantCultureIgnoreCase))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        ms.Write(entity.Attachment, 0, entity.Attachment.Length);
                        using (ExcelPackage package = new ExcelPackage(ms))
                        {
                            currentData = StoreUtility.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                            sheetName = package.Workbook.Worksheets[1].Name;
                        }
                    }
                }
            }

            if (string.Equals(entity.EntityName, "VendorItemCategories", StringComparison.InvariantCultureIgnoreCase) && currentData.Rows.Count > 0)
            {
                
                sqlHelper.ExecuteQuery("DELETE FROM cm_vendorcategory WHERE vendorid= 0;DELETE FROM cm_vendorproducts WHERE vendorid= 0;");

                int count = 0;
                foreach (DataRow row in currentData.Rows)
                {
                    string itemName = row["ProductName"] != DBNull.Value ? Convert.ToString(row["ProductName"]).Trim() : string.Empty;
                    string itemCategory = row["CategoryName"] != DBNull.Value ? Convert.ToString(row["CategoryName"]).Trim() : string.Empty;

                    if (!string.IsNullOrEmpty(itemName))
                    {
                        SortedDictionary<object, object> sdCat = new SortedDictionary<object, object>() { };

                        try
                        {
                            sdCat.Add("P_COMP_ID", compId);
                            sdCat.Add("P_VENDOR_ID", vendorId);
                            sdCat.Add("P_CAT_NAMES", itemCategory);
                            sdCat.Add("P_PROD_NAME", itemName);
                            sdCat.Add("P_USER", user);
                            DataSet dsCat = sqlHelper.SelectList("cm_importVendorItemCategory", sdCat);

                            if (dsCat != null && dsCat.Tables.Count > 0 && dsCat.Tables[0].Rows.Count > 0 && dsCat.Tables[0].Rows[0][0] != null)
                            {
                                int itemId = dsCat.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dsCat.Tables[0].Rows[0][0].ToString()) : -1;
                                if (itemId > 0)
                                {
                                    count++;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.Message);
                        }
                    }
                }
                catalogResponse.ResponseId = count;
                return catalogResponse;
            }
            else if (string.Equals(entity.EntityName, "MyCategories", StringComparison.InvariantCultureIgnoreCase))
            {
                if (entity.Attachment != null)
                {
                    long tick = DateTime.Now.Ticks;
                    string attachName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "mycatalog_" + tick + entity.UserID + "_" + Path.GetExtension(entity.AttachmentFileName));
                    string fileName = "mycatalog_" + tick + entity.UserID + "_" + Path.GetExtension(entity.AttachmentFileName);
                    Utilities.SaveFile(attachName, entity.Attachment);
                    PRMName.Response response = Utilities.SaveAttachment(fileName);
                    if(response !=null && response.ObjectID > 0)
                    {
                        string query = string.Format("UPDATE UserData SET MY_CATALOG_FILE = {0} WHERE U_ID = {1};", response.ObjectID , entity.UserID);
                        sqlHelper.ExecuteQuery(query);
                    }
                }
                catalogResponse.ResponseId = 1;
                return catalogResponse;
            }
            else
            {
                catalogResponse.ErrorMessage = "sheet name should be VendorItemCategories";
                return catalogResponse;
            }
        }

        private List<Category> FillCategoryModel(DataSet ds)
        {
            List<Category> details = new List<Category>();
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    Category category = new Category();
                    try
                    {
                        CatalogUtility.SetItemFromRow(category, dr);
                    }
                    catch (Exception ex)
                    {
                        category = new Category();
                        category.ErrorMessage = ex.Message;
                    }
                    details.Add(category);
                }
            }
            return details;
        }

        public CatalogResponse SaveVendorCatalog(int vendorId, int compId, string catIds, string prodIds, int user, string sessionId)
        {
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_VENDOR_ID", vendorId);
                sd.Add("P_COMP_ID", compId);
                sd.Add("P_CAT_IDS", catIds);
                sd.Add("P_PROD_IDS", string.IsNullOrEmpty(prodIds)?"0": prodIds);
                sd.Add("P_USER", user);
                ds = sqlHelper.SelectList("cm_savevendorcatalog", sd);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
            }

            catalogResponse.SessionID = sessionId;
            return catalogResponse;
        }



        public CatalogResponse SaveVendorCatalog1(int vendorId, int compId, string catIds, string prodIds, int user, string sessionId)
        {
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_VENDOR_ID", vendorId);
                sd.Add("P_COMP_ID", compId);
                sd.Add("P_CAT_IDS", catIds);
                sd.Add("P_PROD_IDS", string.IsNullOrEmpty(prodIds) ? "0" : prodIds);
                sd.Add("P_USER", user);
                ds = sqlHelper.SelectList("cm_savevendorcatalog1", sd);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
            }

            catalogResponse.SessionID = sessionId;
            return catalogResponse;
        }

        //public CatalogResponse SaveProductVendors(int vendorIds, int compId, string catIds, string prodId, int user, string sessionId)
        //{
        //    CatalogResponse catalogResponse = new CatalogResponse();
        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
        //        sd.Add("P_PROD_ID", prodId);
        //        sd.Add("P_COMP_ID", compId);
        //        sd.Add("P_CAT_IDS", catIds);
        //        sd.Add("P_VENDOR_IDS", string.IsNullOrEmpty(vendorIds) ? "0" : vendorIds);
        //        sd.Add("P_USER", user);
        //        ds = sqlHelper.SelectList("cm_saveproductvendors", sd);
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex.Message);
        //    }

        //    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
        //    {
        //        catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
        //    }

        //    catalogResponse.SessionID = sessionId;
        //    return catalogResponse;
        //}

        public List<CompanyConfiguration> GetCompanyConfiguration(int compID, string configKey, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<CompanyConfiguration> listCompanyConfiguration = new List<CompanyConfiguration>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_CONFIG_KEY", configKey);
                DataSet ds = sqlHelper.SelectList("cp_GetCompanyConfiguration", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        CompanyConfiguration companyconfiguration = new CompanyConfiguration();
                        companyconfiguration.CompConfigID = row["C_CONFIG_ID"] != DBNull.Value ? Convert.ToInt32(row["C_CONFIG_ID"]) : 0;
                        companyconfiguration.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                        companyconfiguration.ConfigKey = row["CONFIG_KEY"] != DBNull.Value ? Convert.ToString(row["CONFIG_KEY"]) : string.Empty;
                        companyconfiguration.ConfigValue = row["CONFIG_VALUE"] != DBNull.Value ? Convert.ToString(row["CONFIG_VALUE"]) : string.Empty;
                        companyconfiguration.ConfigText = row["CONFIG_TEXT"] != DBNull.Value ? Convert.ToString(row["CONFIG_TEXT"]) : string.Empty;
                        if (string.IsNullOrEmpty(companyconfiguration.ConfigText))
                        {
                            companyconfiguration.ConfigText = companyconfiguration.ConfigValue;
                        }

                        companyconfiguration.IsValid = row["IS_VALID"] != DBNull.Value ? (Convert.ToInt32(row["IS_VALID"]) == 1 ? true : false) : false;
                        companyconfiguration.UserID = row["MODIFIED_BY"] != DBNull.Value ? Convert.ToInt32(row["MODIFIED_BY"]) : 0;
                        listCompanyConfiguration.Add(companyconfiguration);
                    }
                }
            }
            catch (Exception ex)
            {
                CompanyConfiguration companyconfiguration = new CompanyConfiguration();
                companyconfiguration.ErrorMessage = ex.Message;
                listCompanyConfiguration.Add(companyconfiguration);
            }

            return listCompanyConfiguration;
        }



        public List<VendorProductData> GetProdVendorData(int vendorid, int catitemid, string sessionid)
        {
            List<VendorProductData> details = new List<VendorProductData>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_V_ID", vendorid);
                sd.Add("P_C_I_ID", catitemid);
                CORE.DataNamesMapper<VendorProductData> mapper = new CORE.DataNamesMapper<VendorProductData>();
                var dataset = sqlHelper.SelectList("cp_GetProdVendorData", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }


        public CatalogResponse SaveProductQuotationTemplate(ProductQuotationTemplate productquotationtemplate, string sessionid)
        {
            CatalogResponse details = new CatalogResponse();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_T_ID", productquotationtemplate.T_ID);
                sd.Add("P_PRODUCT_ID", productquotationtemplate.PRODUCT_ID);
                sd.Add("P_NAME", productquotationtemplate.NAME);
                sd.Add("P_DESCRIPTION", productquotationtemplate.DESCRIPTION);
                sd.Add("P_HAS_SPECIFICATION", productquotationtemplate.HAS_SPECIFICATION);
                sd.Add("P_HAS_PRICE", productquotationtemplate.HAS_PRICE);
                sd.Add("P_HAS_QUANTITY", productquotationtemplate.HAS_QUANTITY);
                sd.Add("P_CONSUMPTION", productquotationtemplate.CONSUMPTION);
                sd.Add("P_UOM", productquotationtemplate.UOM);
                sd.Add("P_HAS_TAX", productquotationtemplate.HAS_TAX);
                sd.Add("P_IS_VALID", productquotationtemplate.IS_VALID);
                sd.Add("P_U_ID", productquotationtemplate.U_ID);
                CORE.DataNamesMapper<CatalogResponse> mapper = new CORE.DataNamesMapper<CatalogResponse>();
                var dataset = sqlHelper.SelectList("cm_SaveProductQuotationTemplate", sd);
                details = mapper.Map(dataset.Tables[0]).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }


        public List<ProductQuotationTemplate> GetProductQuotationTemplate(int catitemid, string sessionid)
        {
            List<ProductQuotationTemplate> details = new List<ProductQuotationTemplate>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_C_I_ID", catitemid);
                CORE.DataNamesMapper<ProductQuotationTemplate> mapper = new CORE.DataNamesMapper<ProductQuotationTemplate>();
                var dataset = sqlHelper.SelectList("cm_GetProductQuotationTemplate", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }


        public PRMName.Response SaveProductContract(ContractManagementDetails contract, List<VendorWiseContractManagementDetails> contractMultiple = null)
        {
            PRMName.Response response = new PRMName.Response();
            PRMServices prm = new PRMServices();
            try
            {

                int maxSeries = 0;

                if (contractMultiple != null && contractMultiple.Count > 0) 
                {
                    contract.SessionId = contractMultiple[0].ContractItems[0].SessionId;
                }

                Utilities.ValidateSession(contract.SessionId);


                if (contract != null && contractMultiple.Count <= 0) 
                {
                    dynamic value = new ExpandoObject();
                    value.VendorID = 0;
                    value.ContractItems = contract;
                    contractMultiple.Add(value);
                }


                if (contractMultiple != null && contractMultiple.Count > 0) 
                {

                    foreach (var mulitContract in contractMultiple) 
                    {
                        if (mulitContract.VendorID > 0 && mulitContract.Type == "QCS")
                        {
                            DataSet ds = sqlHelper.ExecuteQuery($@"select (MAX(CONTRACT_SERIES) + 1) as SERIES_COUNT from productcontractdetails");

                            if (ds != null && ds.Tables[0].Rows.Count > 0) 
                            {
                                maxSeries = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                            }
                        }

                        if (mulitContract.ContractItems != null && mulitContract.ContractItems.Count > 0)
                        {
                            foreach (var obj in mulitContract.ContractItems)
                            {
                                contract = obj;
                                contract.StartTime = obj.StartTimeTemp;
                                contract.EndTime = obj.EndTimeTemp;

                                if (contract.PC_ID <= 0) 
                                {
                                    contract.Number = $@"CON-{maxSeries}";
                                }

                                if (contract != null && contract.IsValid > 0)
                                {
                                    if (string.IsNullOrEmpty(contract.PriceType))
                                    {
                                        contract.PriceType = "CONTRACT";
                                    }
                                    string fileName = string.Empty;
                                    List<PRMName.FileUpload> attachments = new List<PRMName.FileUpload>();
                                    if (contract.MultipleAttachments != null && contract.MultipleAttachments.Count > 0)
                                    {
                                        foreach (PRMName.FileUpload fd in contract.MultipleAttachments)
                                        {
                                            fd.FileName = Regex.Replace(fd.FileName, @"[^0-9a-zA-Z.]+", "PRM_");
                                            var newFileObj = new PRMName.FileUpload();
                                            if (fd.FileID <= 0)
                                            {
                                                var attachName = string.Empty;
                                                long tick = DateTime.UtcNow.Ticks;
                                                attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "contract" + tick + "_user" + contract.USER + "_" + fd.FileName);
                                                prm.SaveFile(attachName, fd.FileStream);
                                                attachName = "contract" + tick + "_user" + contract.USER + "_" + fd.FileName;
                                                PRMName.Response res = prm.SaveAttachment(attachName);
                                                if (res.ErrorMessage != "")
                                                {
                                                    response.ErrorMessage = res.ErrorMessage;
                                                }

                                                fd.FileID = res.ObjectID;
                                                newFileObj.FileID = fd.FileID;
                                                newFileObj.FileName = fd.FileName;
                                            }
                                            else if (fd.FileID > 0)
                                            {
                                                var fileData = prm.DownloadFile(Convert.ToString(fd.FileID), contract.SessionId);
                                                newFileObj.FileID = fd.FileID;
                                                newFileObj.FileName = fd.FileName;
                                            }
                                            attachments.Add(newFileObj);
                                        }
                                        var reduceAttachmentList = attachments.Select(e => new { e.FileID, e.FileName }).ToList();
                                        fileName = Newtonsoft.Json.JsonConvert.SerializeObject(reduceAttachmentList);
                                    }


                                    SortedDictionary<object, object> sd1 = new SortedDictionary<object, object>() { };
                                    sd1.Add("P_PC_ID", contract.PC_ID);
                                    sd1.Add("P_ProductId", contract.ProductId);
                                    sd1.Add("P_U_ID", contract.VendorId);
                                    sd1.Add("P_VENDOR_CODE", contract.SelectedVendorCode);
                                    sd1.Add("P_companyName", contract.CompanyName);
                                    sd1.Add("P_number", contract.Number);
                                    sd1.Add("P_value", contract.Value);
                                    sd1.Add("P_quantity", contract.Quantity);
                                    sd1.Add("P_availedQuantity", contract.AvailedQuantity);
                                    sd1.Add("P_document", fileName);
                                    sd1.Add("P_startTime", contract.StartTime);
                                    sd1.Add("P_endTime", contract.EndTime);
                                    sd1.Add("P_PAYMENT_TERMS", contract.PAYMENT_TERMS);
                                    sd1.Add("P_INCO_TERMS", contract.INCO_TERMS);
                                    sd1.Add("P_DELIVERY_TERMS", contract.DELIVERY_TERMS);
                                    sd1.Add("P_TERMS", contract.GENERAL_TERMS);
                                    sd1.Add("P_isValid", 1);
                                    sd1.Add("P_PLANT_CODE", contract.PLANT_CODE);
                                    sd1.Add("P_DELIVERY_FROM", contract.DELIVERY_FROM);
                                    sd1.Add("P_DELIVERY_AT", contract.DELIVERY_AT);
                                    sd1.Add("P_LEAD_TIME", contract.LEADTIME);
                                    sd1.Add("P_WARRANTY", contract.WARRANTY);
                                    sd1.Add("P_SPECIAL_INSTRUCTIONS", contract.SPECIAL_INSTRUCTIONS);
                                    sd1.Add("P_NET_PRICE", contract.NET_PRICE);
                                    sd1.Add("P_DISCOUNT", contract.DISCOUNT);
                                    sd1.Add("P_GST", contract.GST);
                                    sd1.Add("P_CUSTOM_DUTY", contract.CUSTOM_DUTY);
                                    sd1.Add("P_DISCOUNT_ELIGIBILITY", contract.DISCOUNT_ELIGIBILITY);
                                    sd1.Add("P_RECONCILIATION_TIMELINES", contract.RECONCILIATION_TIMELINES);
                                    sd1.Add("P_CURRENCY", contract.CURRENCY);
                                    sd1.Add("P_EXCH_RATE", contract.EXCHANGE_RATE);
                                    sd1.Add("P_PO_CURRENCY", contract.PO_CURRENCY);
                                    sd1.Add("P_USER", contract.USER);
                                    sd1.Add("P_PRICE_TYPE", contract.PriceType);
                                    sd1.Add("P_ALLOC_PERC", contract.AllocationPercentage);
                                    sd1.Add("P_CONTRACT_STATUS", contract.ContractStatus);
                                    sd1.Add("P_REQ_ID", contract.REQ_ID > 0 ? contract.REQ_ID : 0);
                                    sd1.Add("P_QCS_ID", contract.QCS_ID > 0 ? contract.QCS_ID : 0);
                                    sd1.Add("P_VENDOR_SITE_CODE", contract.VENDOR_SITE_CODE);
                                    sd1.Add("P_SERIES", maxSeries > 0 ? maxSeries : 0);

                                    var ds = sqlHelper.SelectList("cm_saveContractDetails", sd1);
                                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                    {
                                        var row = ds.Tables[0].Rows[0];
                                        string message = row[0] != DBNull.Value ? Convert.ToString(row[0]) : string.Empty;
                                        if (!string.IsNullOrEmpty(message) && message.Contains("ERROR:"))
                                        {
                                            throw new Exception(message);
                                        }
                                    }
                                }
                                else
                                {
                                    if (contract.PC_ID > 0)
                                    {
                                        sqlHelper.ExecuteNonQuery_IUD("UPDATE productcontractdetails SET IsValid = -2,contractStatus = 'Delete' WHERE PC_ID = " + contract.PC_ID);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }


        public string GetContractTemplate(int productid, int vendorid, int compid, bool includedata, string sessionid)
        {
            int maxRows = 500;
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;
            ExcelPackage ExcelPkg = new ExcelPackage();
            ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("PRODUCT_CONTRACT");
            wsSheet1.Cells["A1"].Value = "Material Code";
            wsSheet1.Cells["B1"].Value = "Vendor Part No";
            wsSheet1.Cells["C1"].Value = "Material Desc";
            wsSheet1.Cells["D1"].Value = "Contract Number";
            wsSheet1.Cells["E1"].Value = "Price";
            wsSheet1.Cells["F1"].Value = "Quantity";
            wsSheet1.Cells["G1"].Value = "AvailedQuantity";
            wsSheet1.Cells["H1"].Value = "StartDate";
            wsSheet1.Cells["I1"].Value = "EndDate";
            wsSheet1.Cells["J1"].Value = "VendorCodes";
            wsSheet1.Cells["K1"].Value = "PaymentTerms";
            wsSheet1.Cells["L1"].Value = "IncoTerms";
            wsSheet1.Cells["M1"].Value = "DeliveryTerms";
            wsSheet1.Cells["N1"].Value = "TermsAndConditions";
            wsSheet1.Cells["O1"].Value = "DeliveryFrom";
            wsSheet1.Cells["P1"].Value = "DeliveryAt";
            wsSheet1.Cells["Q1"].Value = "LeadTime";
            wsSheet1.Cells["R1"].Value = "Warranty";
            wsSheet1.Cells["S1"].Value = "SpecialInstructions";
            wsSheet1.Cells["T1"].Value = "Net Price";
            wsSheet1.Cells["U1"].Value = "Discount";
            wsSheet1.Cells["V1"].Value = "GST";
            wsSheet1.Cells["W1"].Value = "CustomDuty";
            wsSheet1.Cells["X1"].Value = "EligibilityToRedeemDiscount";
            wsSheet1.Cells["Y1"].Value = "ReconciliationTimelines";
            wsSheet1.Cells["Z1"].Value = "Currency";
            wsSheet1.Cells["AA1"].Value = "ExchRate";
            wsSheet1.Cells["AB1"].Value = "POCurrency";
            wsSheet1.Cells["AC1"].Value = "ProductId";
            wsSheet1.Cells["AD1"].Value = "PriceType";
            wsSheet1.Cells["AE1"].Value = "Allocation Percentage";
            wsSheet1.Cells["AF1"].Value = "Contract Status";

            wsSheet1.Cells[$"A:A"].Style.Numberformat.Format = "@";
            wsSheet1.Cells[$"B:B"].Style.Numberformat.Format = "@";
            wsSheet1.Cells[$"J:J"].Style.Numberformat.Format = "@";
            wsSheet1.Cells[$"H:H"].Style.Numberformat.Format = "@";
            wsSheet1.Cells[$"I:I"].Style.Numberformat.Format = "@";

            //var endDateValidation = wsSheet1.DataValidations.AddDateTimeValidation($"I2:I{maxRows}");
            //endDateValidation.ShowErrorMessage = true;
            //endDateValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
            //endDateValidation.ErrorTitle = "Invalid Value";
            //endDateValidation.Error = "Enter Valid Value.";
            //endDateValidation.Operator = ExcelDataValidationOperator.greaterThan;
            //endDateValidation.Formula.Value = DateTime.Now;

            var decimalValidation = wsSheet1.DataValidations.AddDecimalValidation($"V2:V{maxRows}");
            decimalValidation.ShowErrorMessage = true;
            decimalValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
            decimalValidation.ErrorTitle = "Invalid Value";
            decimalValidation.Error = "Enter Valid Value.";
            decimalValidation.Operator = ExcelDataValidationOperator.between;
            decimalValidation.Formula.Value = 0;
            decimalValidation.Formula2.Value = 100;

            var decimalValidation1 = wsSheet1.DataValidations.AddDecimalValidation($"W2:W{maxRows}");
            decimalValidation1.ShowErrorMessage = true;
            decimalValidation1.ErrorStyle = ExcelDataValidationWarningStyle.stop;
            decimalValidation1.ErrorTitle = "Invalid Value";
            decimalValidation1.Error = "Enter Valid Value.";
            decimalValidation1.Operator = ExcelDataValidationOperator.between;
            decimalValidation1.Formula.Value = 0;
            decimalValidation1.Formula2.Value = 100;

            var decimalValidation3 = wsSheet1.DataValidations.AddDecimalValidation($"F2:F{maxRows}");
            decimalValidation3.ShowErrorMessage = true;
            decimalValidation3.ErrorStyle = ExcelDataValidationWarningStyle.stop;
            decimalValidation3.ErrorTitle = "Invalid Value";
            decimalValidation3.Error = "Enter Valid Value.";
            decimalValidation3.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
            decimalValidation3.Formula.Value = 0;

            var decimalValidation4 = wsSheet1.DataValidations.AddDecimalValidation($"G2:G{maxRows}");
            decimalValidation4.ShowErrorMessage = true;
            decimalValidation4.ErrorStyle = ExcelDataValidationWarningStyle.stop;
            decimalValidation4.ErrorTitle = "Invalid Value";
            decimalValidation4.Error = "Enter Valid Value.";
            decimalValidation4.Operator = ExcelDataValidationOperator.greaterThanOrEqual;
            decimalValidation4.Formula.Value = 0;

            var vendorCodeValidation = wsSheet1.DataValidations.AddAnyValidation($"J2:J{maxRows}");
            vendorCodeValidation.ShowErrorMessage = true;
            vendorCodeValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
            vendorCodeValidation.ErrorTitle = "Enter Vendor Code";
            vendorCodeValidation.Error = "Enter Valid Value.";
            vendorCodeValidation.AllowBlank = false;


            var decimalValidation5 = wsSheet1.DataValidations.AddDecimalValidation($"U2:U{maxRows}");
            decimalValidation5.ShowErrorMessage = true;
            decimalValidation5.ErrorStyle = ExcelDataValidationWarningStyle.stop;
            decimalValidation5.ErrorTitle = "Invalid Value";
            decimalValidation5.Error = "Enter Valid Value.";
            decimalValidation5.Operator = ExcelDataValidationOperator.between;
            decimalValidation5.Formula.Value = 0;
            decimalValidation5.Formula2.Value = 100;


            var decimalValidation6 = wsSheet1.DataValidations.AddDecimalValidation($"AA2:AA{maxRows}");
            decimalValidation6.ShowErrorMessage = true;
            decimalValidation6.ErrorStyle = ExcelDataValidationWarningStyle.stop;
            decimalValidation6.ErrorTitle = "Invalid Value";
            decimalValidation6.Error = "Enter Valid Value.";
            decimalValidation6.Operator = ExcelDataValidationOperator.greaterThan;
            decimalValidation6.Formula.Value = 0;

            var decimalValidation7 = wsSheet1.Cells[$"AD2:AD1{maxRows}"].DataValidation.AddListDataValidation() as ExcelDataValidationList;
            decimalValidation7.ShowErrorMessage = true;
            decimalValidation7.AllowBlank = false;
            decimalValidation7.Formula.Values.Add("CONTRACT");
            decimalValidation7.Formula.Values.Add("PRICE_INFO");


            var decimalValidation8 = wsSheet1.DataValidations.AddDecimalValidation($"AE2:AE{maxRows}");
            decimalValidation8.ShowErrorMessage = true;
            decimalValidation8.ErrorStyle = ExcelDataValidationWarningStyle.stop;
            decimalValidation8.ErrorTitle = "Invalid Value";
            decimalValidation8.Error = "Enter Valid Value.";
            decimalValidation8.Operator = ExcelDataValidationOperator.between;
            decimalValidation8.Formula.Value = 0;
            decimalValidation8.Formula2.Value = 100;


            var decimalValidation9 = wsSheet1.Cells[$"AF2:AF1{maxRows}"].DataValidation.AddListDataValidation() as ExcelDataValidationList;
            decimalValidation9.ShowErrorMessage = true;
            decimalValidation9.AllowBlank = false;
            decimalValidation9.Formula.Values.Add("Active");
            decimalValidation9.Formula.Values.Add("On Hold");
            decimalValidation9.Formula.Values.Add("Delete");

            for (int i = 2; i <= maxRows; i++)
            {
                wsSheet1.Cells["T" + (i)].Formula = $"E{i}*(1-(U{i}/100))";
                wsSheet1.Cells["AF" + (i)].Value = "Active";
            }

            wsSheet1.Cells["A:AC"].AutoFitColumns();

            wsSheet1.Cells[$"A2:A{maxRows}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            wsSheet1.Cells[$"D2:D{maxRows}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            wsSheet1.Cells[$"J2:J{maxRows}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            wsSheet1.Cells[$"AC2:AC{maxRows}"].Style.Fill.PatternType = ExcelFillStyle.Solid;

            wsSheet1.Cells[$"A2:A{maxRows}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
            wsSheet1.Cells[$"D2:D{maxRows}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
            wsSheet1.Cells[$"J2:J{maxRows}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
            wsSheet1.Cells[$"AC2:AC{maxRows}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));

            wsSheet1.Cells[$"A2:A{maxRows}"].Style.Font.Color.SetColor(Color.Black);
            wsSheet1.Cells[$"D2:D{maxRows}"].Style.Font.Color.SetColor(Color.Black);
            wsSheet1.Cells[$"J2:J{maxRows}"].Style.Font.Color.SetColor(Color.Black);
            wsSheet1.Cells[$"AC2:AC{maxRows}"].Style.Font.Color.SetColor(Color.Black);

            wsSheet1.Cells[$"A2:A{maxRows}"].Style.Font.Bold = true;
            wsSheet1.Cells[$"D2:D{maxRows}"].Style.Font.Bold = true;
            wsSheet1.Cells[$"J2:J{maxRows}"].Style.Font.Bold = true;
            wsSheet1.Cells[$"AC2:AC{maxRows}"].Style.Font.Bold = true;

            wsSheet1.Cells[$"A2:A{maxRows}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            wsSheet1.Cells[$"D2:D{maxRows}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            wsSheet1.Cells[$"J2:J{maxRows}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            wsSheet1.Cells[$"AC2:AC{maxRows}"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            wsSheet1.Cells[$"A2:A{maxRows}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            wsSheet1.Cells[$"D2:D{maxRows}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            wsSheet1.Cells[$"J2:J{maxRows}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            wsSheet1.Cells[$"AC2:AC{maxRows}"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

            wsSheet1.Cells["A:AF"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            wsSheet1.Cells["A:AF"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
            wsSheet1.Cells["A:AF"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
            wsSheet1.Cells["A:AF"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

            //for (int i = 2; i< maxRows; i++)
            //{
            //    wsSheet1.Cells["E"+ i].Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            //    wsSheet1.Cells["F" + i].Value = DateTime.Now.AddYears(1).ToString("yyyy-MM-dd HH:mm:ss");
            //    wsSheet1.Cells["W" + i].Value = productid;
            //}

            wsSheet1.Cells["H2"].Value = DateTime.Now.ToString("dd/MM/yyyy");
            wsSheet1.Cells["I2"].Value = DateTime.Now.AddYears(1).ToString("dd/MM/yyyy");
            if (productid > 0)
            {
                var query = $"SELECT ProductCode, ProductName, ProductDesc, VendorPartNumber FROM cm_product WHERE ProductId = {productid}";
                DataSet ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    var row = ds.Tables[0].Rows[0];
                    wsSheet1.Cells["A2"].Value = row["ProductCode"] != DBNull.Value ? Convert.ToString(row["ProductCode"]) : string.Empty;
                    wsSheet1.Cells["B2"].Value = row["VendorPartNumber"] != DBNull.Value ? Convert.ToString(row["VendorPartNumber"]) : string.Empty;
                    wsSheet1.Cells["C2"].Value = row["ProductDesc"] != DBNull.Value ? Convert.ToString(row["ProductDesc"]) : string.Empty;
                }

                wsSheet1.Cells["AC2"].Value = productid;
            }

            if (productid == 0)
            {
                List<Product> products = this.GetProducts(compid, sessionid);
                if (products != null && products.Count > 0)
                {
                    products = products.Where(p => (p.IsValid > 0) && (!string.IsNullOrWhiteSpace(p.ProductCode) || !string.IsNullOrWhiteSpace(p.VendorPartNumber))).ToList();
                    ExcelWorksheet wsSheet2 = ExcelPkg.Workbook.Worksheets.Add("MATERIAL_CODE_CONFIG");
                    ExcelWorksheet wsSheet3 = ExcelPkg.Workbook.Worksheets.Add("VENDOR_PART_NO_CONFIG");
                    int count = 2;
                    int count1 = 2;

                    wsSheet2.Cells["A1"].Value = "Material Code";
                    wsSheet2.Cells["B1"].Value = "Vendor Part Number";
                    wsSheet2.Cells["C1"].Value = "Material Desc";
                    wsSheet2.Cells["D1"].Value = "MaterialId";

                    wsSheet3.Cells["A1"].Value = "Vendor Part Number";
                    wsSheet3.Cells["B1"].Value = "MaterialCode";
                    wsSheet3.Cells["C1"].Value = "Material Desc";
                    wsSheet3.Cells["D1"].Value = "Material Id";

                    wsSheet2.Cells[$"A:A"].Style.Numberformat.Format = "@";
                    wsSheet3.Cells[$"A:A"].Style.Numberformat.Format = "@";

                    var product1 = products.Where(p => (p.IsValid > 0) && !string.IsNullOrWhiteSpace(p.ProductCode)).ToList();
                    foreach (var product in product1)
                    {
                        wsSheet2.Cells["A" + count.ToString()].Value = product.ProductCode;
                        wsSheet2.Cells["B" + count.ToString()].Value = string.IsNullOrWhiteSpace(product.VendorPartNumber) ? "" : product.VendorPartNumber;
                        //wsSheet2.Cells["C" + count.ToString()].Value = product.ProductName;
                        wsSheet2.Cells["D" + count.ToString()].Value = product.ProductId;
                        count++;
                    }

                    var product2 = products.Where(p => (p.IsValid > 0) && !string.IsNullOrWhiteSpace(p.VendorPartNumber)).ToList();
                    foreach (var product in product2)
                    {
                        if (!string.IsNullOrWhiteSpace(product.VendorPartNumber))
                        {
                            wsSheet3.Cells["A" + count1.ToString()].Value = product.VendorPartNumber;
                            wsSheet3.Cells["B" + count1.ToString()].Value = product.ProductCode;
                            //wsSheet3.Cells["C" + count1.ToString()].Value = product.ProductName;
                            wsSheet3.Cells["D" + count1.ToString()].Value = product.ProductId;
                            count1++;
                        }
                    }

                    wsSheet2.Protection.IsProtected = false;
                    wsSheet2.Protection.AllowSelectLockedCells = false;
                    wsSheet3.Protection.IsProtected = false;
                    wsSheet3.Protection.AllowSelectLockedCells = false;
                }

                for (int i = 2; i <= maxRows; i++)
                {
                    wsSheet1.Cells["B" + (i)].Formula = $@"IF(A{i}<>"""",VLOOKUP(TEXT(A{i}, ""0""),MATERIAL_CODE_CONFIG!A:D,2,FALSE),"""")";
                    //wsSheet1.Cells["C" + (i)].Formula = $@"IF(A{i}<>"""",VLOOKUP(TEXT(A{i}, ""0""),MATERIAL_CODE_CONFIG!A:D,3,FALSE),IF($B{i}<>"""",VLOOKUP(TEXT($B{i}, ""0""),VENDOR_PART_NO_CONFIG!A:D,3,FALSE),""""))";
                    wsSheet1.Cells["AC" + (i)].Formula = $@"IF(A{i}<>"""",VLOOKUP(TEXT(A{i}, ""0""),MATERIAL_CODE_CONFIG!A:D,4,FALSE),IF($B{i}<>"""",VLOOKUP(TEXT($B{i}, ""0""),VENDOR_PART_NO_CONFIG!A:D,4,FALSE),""""))";
                }
            }


            if (includedata)
            {
                var productContracts = GetProductContracts(productid.ToString(), sessionid);
                if (productContracts != null && productContracts.Count > 0)
                {
                    if (vendorid > 0)
                    {
                        productContracts = productContracts.Where(p => p.VendorId == vendorid).ToList();
                    }

                    if (productContracts != null && productContracts.Count > 0)
                    {
                        int index = 2;
                        foreach (var contract in productContracts)
                        {
                            wsSheet1.Cells["A" + index].Value = contract.PRODUCT_CODE;
                            wsSheet1.Cells["B" + index].Value = contract.VendorPartNumber;
                            wsSheet1.Cells["C" + index].Value = contract.ProductName;
                            wsSheet1.Cells["D" + index].Value = contract.ContractNumber;
                            wsSheet1.Cells["E" + index].Value = contract.Price;
                            wsSheet1.Cells["F" + index].Value = contract.Quantity;
                            wsSheet1.Cells["G" + index].Value = contract.AvailedQuantity;
                            wsSheet1.Cells["H" + index].Value = contract.StartTime.Value.ToString("dd/MM/yyyy"); ;
                            wsSheet1.Cells["I" + index].Value = contract.EndTime.Value.ToString("dd/MM/yyyy"); ;
                            wsSheet1.Cells["J" + index].Value = contract.VENDOR_CODE;
                            wsSheet1.Cells["K" + index].Value = contract.PAYMENT_TERMS;
                            wsSheet1.Cells["L" + index].Value = contract.INCO_TERMS;
                            wsSheet1.Cells["M" + index].Value = contract.DELIVERY_TERMS;
                            wsSheet1.Cells["N" + index].Value = contract.GENERAL_TERMS;
                            wsSheet1.Cells["O" + index].Value = contract.DELIVERY_FROM;
                            wsSheet1.Cells["P" + index].Value = contract.DELIVERY_AT;
                            wsSheet1.Cells["Q" + index].Value = contract.LEADTIME;
                            wsSheet1.Cells["R" + index].Value = contract.WARRANTY;
                            wsSheet1.Cells["S" + index].Value = contract.SPECIAL_INSTRUCTIONS;
                            wsSheet1.Cells["T" + index].Value = contract.NET_PRICE;
                            wsSheet1.Cells["U" + index].Value = contract.DISCOUNT;
                            wsSheet1.Cells["V" + index].Value = contract.GST;
                            wsSheet1.Cells["W" + index].Value = contract.CUSTOM_DUTY;
                            wsSheet1.Cells["X" + index].Value = contract.DISCOUNT_ELIGIBILITY;
                            wsSheet1.Cells["Y" + index].Value = contract.RECONCILIATION_TIMELINES;
                            wsSheet1.Cells["Z" + index].Value = contract.CURRENCY;
                            wsSheet1.Cells["AA" + index].Value = contract.EXCHANGE_RATE;
                            wsSheet1.Cells["AB" + index].Value = contract.PO_CURRENCY;
                            wsSheet1.Cells["AC" + index].Value = contract.ProductId;
                            wsSheet1.Cells["AD" + index].Value = "";
                            wsSheet1.Cells["AE" + index].Value = contract.AllocationPercentage;
                            wsSheet1.Cells["AF" + index].Value = contract.ContractStatus;

                            index++;
                        }
                    }
                }
            }

            wsSheet1.Protection.IsProtected = false;
            wsSheet1.Protection.AllowSelectLockedCells = false;

            ExcelPkg.SaveAs(ms);
            if (ms != null)
            {
                contents = ms.ToArray();
            }

            return Convert.ToBase64String(ms.ToArray());
        }



        public CatalogResponse UploadContractsFromExcel(int user, byte[] attachment, int compid, int productid, string sessionid)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            CatalogResponse response = new CatalogResponse();
            var errorMessage = string.Empty;
            string summary = "";
            int totalProcessed = 0;
            int failedRows = 0;
            int rowNumber = 1;
            var guid1 = Guid.NewGuid();
            string guid = guid1.ToString();
            try
            {
                if (attachment != null)
                {
                    string sheetName = string.Empty;
                    DataTable currentData = new DataTable();
                    using (MemoryStream ms = new MemoryStream())
                    {
                        ms.Write(attachment, 0, attachment.Length);
                        using (ExcelPackage package = new ExcelPackage(ms))
                        {
                            currentData = Utilities.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                            sheetName = package.Workbook.Worksheets[1].Name;
                        }
                    }


                    if (!string.IsNullOrEmpty(sheetName) && sheetName.Equals("PRODUCT_CONTRACT", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (compid > 0 && productid <= 0)
                        {
                            var newPoducts = currentData.AsEnumerable().Where(row => !row.IsNull("Material Desc") && row["Material Desc"] != DBNull.Value && !string.IsNullOrWhiteSpace(row["Material Desc"].ToString().Trim())).ToList();

                            if (newPoducts != null && newPoducts.Count > 0)
                            {
                                DataSet dataSet = sqlHelper.ExecuteQuery($"select * From [cm_category] where CompanyId = getcompanyid({ConfigurationManager.AppSettings["COMPANY_ID"]}) AND CategoryCode='VENDOR_CATALOGUES'");
                                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                                {
                                    var catalogRow = dataSet.Tables[0].Rows[0];
                                    int catalogId = (catalogRow.IsNull("CategoryId") || catalogRow["CategoryId"] == DBNull.Value || string.IsNullOrEmpty(catalogRow["CategoryId"].ToString().Trim())) ? 0 : (Convert.ToInt32(catalogRow["CategoryId"]));

                                    if (catalogId > 0)
                                    {
                                        foreach (var row in newPoducts)
                                        {
                                            string materialName = (row.IsNull("Material Desc") || row["Material Desc"] == DBNull.Value || string.IsNullOrEmpty(row["Material Desc"].ToString().Trim())) ? string.Empty : (row["Material Desc"].ToString().Trim());
                                            try
                                            {
                                                bool isValidProductID = true;
                                                try
                                                {
                                                    isValidProductID = !row.IsNull("ProductId") && row["ProductId"] != DBNull.Value && !string.IsNullOrWhiteSpace(row["ProductId"].ToString().Trim()) && Convert.ToInt32(row["ProductId"]) <= 0 ? true : false;
                                                }
                                                catch (Exception ex)
                                                {
                                                    //throw new Exception("Product Id is mandatory, 0 for new products and valid Id for existing products.");
                                                }

                                                if (!isValidProductID)
                                                {
                                                    continue;
                                                }

                                                Product product = new Product();
                                                product.ProductCode = (row.IsNull("Material Code") || row["Material Code"] == DBNull.Value || string.IsNullOrEmpty(row["Material Code"].ToString().Trim())) ? string.Empty : (row["Material Code"].ToString().Trim());
                                                product.VendorPartNumber = (row.IsNull("Vendor Part No") || row["Vendor Part No"] == DBNull.Value || string.IsNullOrEmpty(row["Vendor Part No"].ToString().Trim())) ? string.Empty : (row["Vendor Part No"].ToString().Trim());
                                                product.ProductDesc = (row.IsNull("Material Desc") || row["Material Desc"] == DBNull.Value || string.IsNullOrEmpty(row["Material Desc"].ToString().Trim())) ? string.Empty : (row["Material Desc"].ToString().Trim());
                                                product.ProductName = (row.IsNull("Material Desc") || row["Material Desc"] == DBNull.Value || string.IsNullOrEmpty(row["Material Desc"].ToString().Trim())) ? string.Empty : (row["Material Desc"].ToString().Trim());
                                                if (string.IsNullOrEmpty(materialName))
                                                {
                                                    materialName = !string.IsNullOrEmpty(product.ProductCode) ? product.ProductCode : product.VendorPartNumber;
                                                }

                                                product.ProductNo = product.ProductCode;
                                                product.CompanyId = compid;
                                                var productResponse = this.AddProduct(product, sessionid);

                                                if (productResponse.ResponseId <= 0)
                                                {
                                                    if (!string.IsNullOrWhiteSpace(productResponse.ErrorMessage))
                                                    {
                                                        row["ProductId"] = productResponse.ObjectId;
                                                    }
                                                }
                                                else
                                                {
                                                    row["ProductId"] = productResponse.ResponseId;
                                                }

                                                if (productResponse.ResponseId > 0)
                                                {
                                                    sqlHelper.ExecuteNonQuery_IUD($@"INSERT INTO [cm_productcategory] ([ProductId] ,[CategoryId] ,[CompanyId] ,[IsValid] ,[DateCreated] ,[DateModified] ,[CreatedBy] ,[ModifiedBy])     
                                                    VALUES ({productResponse.ResponseId} , {catalogId},{compid} ,1 ,NOW() ,NOW() ,{user} ,{user})");
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                string error = ex.Message;
                                                //errorMessage = errorMessage + $"Material: {materialName} - ERROR: {error}" + Environment.NewLine + Environment.NewLine + "<br/>";
                                                logger.Error(ex, errorMessage);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        int count = 1;
                        //var rows = currentData.AsEnumerable().ToList();
                        var rows = currentData.AsEnumerable().Where(row =>
                                                                    (
                                                                        (!row.IsNull("ProductId") && row["ProductId"] != DBNull.Value && Convert.ToString(row["ProductId"]) != "") ||
                                                                        (!row.IsNull("Material Code") && row["Material Code"] != DBNull.Value && Convert.ToString(row["Material Code"]) != "") ||
                                                                        (!row.IsNull("Vendor Part No") && row["Vendor Part No"] != DBNull.Value && Convert.ToString(row["Vendor Part No"]) != "") ||
                                                                        (!row.IsNull("Contract Number") && row["Contract Number"] != DBNull.Value && Convert.ToString(row["Contract Number"]) != "") ||
                                                                        (!row.IsNull("Material Desc") && row["Material Desc"] != DBNull.Value && Convert.ToString(row["Material Desc"]) != "")
                                                                    )
                                                                    ).ToList();
                        summary = $"Total rows : {rows.Count} ";
                        foreach (DataRow row in rows)
                        {
                            rowNumber++;
                            string materialName = (row.IsNull("Material Desc") || row["Material Desc"] == DBNull.Value || string.IsNullOrEmpty(row["Material Desc"].ToString().Trim())) ? string.Empty : (row["Material Desc"].ToString().Trim());
                            try
                            {
                                ContractManagementDetails contract = new ContractManagementDetails();
                                contract.Number = (row.IsNull("Contract Number") || row["Contract Number"] == DBNull.Value || string.IsNullOrEmpty(row["Contract Number"].ToString().Trim())) ? string.Empty : (row["Contract Number"].ToString().Trim());
                                contract.Value = (row.IsNull("Price") || row["Price"] == DBNull.Value || string.IsNullOrEmpty(row["Price"].ToString().Trim())) ? 0 : Convert.ToDecimal(row["Price"].ToString().Trim());
                                contract.PriceType = (row.IsNull("PriceType") || row["PriceType"] == DBNull.Value || string.IsNullOrEmpty(row["PriceType"].ToString().Trim())) ? "CONTRACT" : Convert.ToString(row["PriceType"].ToString().Trim());
                                contract.Quantity = (row.IsNull("Quantity") || row["Quantity"] == DBNull.Value || string.IsNullOrEmpty(row["Quantity"].ToString().Trim())) ? 0 : Convert.ToDecimal(row["Quantity"].ToString().Trim());
                                contract.AvailedQuantity = (row.IsNull("AvailedQuantity") || row["AvailedQuantity"] == DBNull.Value || string.IsNullOrEmpty(row["AvailedQuantity"].ToString().Trim())) ? 0 : Convert.ToDecimal(row["AvailedQuantity"].ToString().Trim());
                                string startTimeTemp = (row.IsNull("StartDate") || row["StartDate"] == DBNull.Value || string.IsNullOrEmpty(row["StartDate"].ToString().Trim())) ? string.Empty : row["StartDate"].ToString().Trim();
                                string endTimeTemp = (row.IsNull("EndDate") || row["EndDate"] == DBNull.Value || string.IsNullOrEmpty(row["EndDate"].ToString().Trim())) ? string.Empty : row["EndDate"].ToString().Trim();

                                try
                                {
                                    string productCode = (row.IsNull("Material Code") || row["Material Code"] == DBNull.Value || string.IsNullOrEmpty(row["Material Code"].ToString().Trim())) ? string.Empty : (row["Material Code"].ToString().Trim());
                                    string vendorPartNumber = (row.IsNull("Vendor Part No") || row["Vendor Part No"] == DBNull.Value || string.IsNullOrEmpty(row["Vendor Part No"].ToString().Trim())) ? string.Empty : (row["Vendor Part No"].ToString().Trim());
                                    if (string.IsNullOrEmpty(materialName))
                                    {
                                        materialName = !string.IsNullOrEmpty(productCode) ? productCode : vendorPartNumber;
                                    }
                                }
                                catch
                                {

                                }

                                if (string.IsNullOrEmpty(startTimeTemp) || string.IsNullOrEmpty(endTimeTemp))
                                {
                                    throw new Exception("Start date & End date are mandatory.");
                                }

                                try
                                {
                                    if (!string.IsNullOrEmpty(startTimeTemp))
                                    {
                                        if (startTimeTemp.Contains("/"))
                                        {
                                            contract.StartTime = DateTime.ParseExact(startTimeTemp, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        }
                                        else if (startTimeTemp.Contains("."))
                                        {
                                            contract.StartTime = DateTime.ParseExact(startTimeTemp, "dd.MM.yyyy", CultureInfo.InvariantCulture);
                                        }
                                        else if (startTimeTemp.Contains("-"))
                                        {
                                            contract.StartTime = DateTime.ParseExact(startTimeTemp, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        }
                                        else
                                        {
                                            throw new Exception("Invalid start date format entered.");
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(endTimeTemp))
                                    {
                                        if (endTimeTemp.Contains("/"))
                                        {
                                            contract.EndTime = DateTime.ParseExact(endTimeTemp, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        }
                                        else if (endTimeTemp.Contains("."))
                                        {
                                            contract.EndTime = DateTime.ParseExact(endTimeTemp, "dd.MM.yyyy", CultureInfo.InvariantCulture);
                                        }
                                        else if (endTimeTemp.Contains("-"))
                                        {
                                            contract.EndTime = DateTime.ParseExact(endTimeTemp, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                                        }
                                        else
                                        {
                                            throw new Exception("Invalid start date format entered.");
                                        }
                                    }
                                }
                                catch
                                {
                                    throw new Exception("Invalid date format entered.");
                                }


                                if (contract.EndTime < contract.StartTime)
                                {
                                    throw new Exception("Contract end date cannot be less than start date.");
                                }

                                //contract.StartTime = (row.IsNull("StartDate") || row["StartDate"] == DBNull.Value || string.IsNullOrEmpty(row["StartDate"].ToString().Trim())) ? DateTime.Now : DateTime.ParseExact(row["StartDate"].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                //contract.EndTime = (row.IsNull("EndDate") || row["EndDate"] == DBNull.Value || string.IsNullOrEmpty(row["EndDate"].ToString().Trim())) ? DateTime.Now.AddYears(1) : DateTime.ParseExact(row["EndDate"].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                contract.SelectedVendorCode = (row.IsNull("VendorCodes") || row["VendorCodes"] == DBNull.Value || string.IsNullOrEmpty(row["VendorCodes"].ToString().Trim())) ? string.Empty : (row["VendorCodes"].ToString().Trim());
                                //contract.Document = (row.IsNull("Document") || row["Document"] == DBNull.Value || string.IsNullOrEmpty(row["Document"].ToString().Trim())) ? string.Empty : (row["Document"].ToString().Trim());
                                contract.PAYMENT_TERMS = (row.IsNull("PaymentTerms") || row["PaymentTerms"] == DBNull.Value || string.IsNullOrEmpty(row["PaymentTerms"].ToString().Trim())) ? string.Empty : (row["PaymentTerms"].ToString().Trim());
                                contract.INCO_TERMS = (row.IsNull("IncoTerms") || row["IncoTerms"] == DBNull.Value || string.IsNullOrEmpty(row["IncoTerms"].ToString().Trim())) ? string.Empty : (row["IncoTerms"].ToString().Trim());
                                contract.DELIVERY_TERMS = (row.IsNull("DeliveryTerms") || row["DeliveryTerms"] == DBNull.Value || string.IsNullOrEmpty(row["DeliveryTerms"].ToString().Trim())) ? string.Empty : (row["DeliveryTerms"].ToString().Trim());
                                contract.GENERAL_TERMS = (row.IsNull("TermsAndConditions") || row["TermsAndConditions"] == DBNull.Value || string.IsNullOrEmpty(row["TermsAndConditions"].ToString().Trim())) ? string.Empty : (row["TermsAndConditions"].ToString().Trim());
                                contract.DELIVERY_FROM = (row.IsNull("DeliveryFrom") || row["DeliveryFrom"] == DBNull.Value || string.IsNullOrEmpty(row["DeliveryFrom"].ToString().Trim())) ? string.Empty : (row["DeliveryFrom"].ToString().Trim());
                                contract.DELIVERY_AT = (row.IsNull("DeliveryAt") || row["DeliveryAt"] == DBNull.Value || string.IsNullOrEmpty(row["DeliveryAt"].ToString().Trim())) ? string.Empty : (row["DeliveryAt"].ToString().Trim());
                                contract.LEADTIME = (row.IsNull("LeadTime") || row["LeadTime"] == DBNull.Value || string.IsNullOrEmpty(row["LeadTime"].ToString().Trim())) ? string.Empty : (row["LeadTime"].ToString().Trim());
                                contract.WARRANTY = (row.IsNull("Warranty") || row["Warranty"] == DBNull.Value || string.IsNullOrEmpty(row["Warranty"].ToString().Trim())) ? string.Empty : (row["Warranty"].ToString().Trim());
                                contract.SPECIAL_INSTRUCTIONS = (row.IsNull("SpecialInstructions") || row["SpecialInstructions"] == DBNull.Value || string.IsNullOrEmpty(row["SpecialInstructions"].ToString().Trim())) ? string.Empty : (row["SpecialInstructions"].ToString().Trim());
                                contract.DISCOUNT = (row.IsNull("Discount") || row["Discount"] == DBNull.Value || string.IsNullOrEmpty(row["Discount"].ToString().Trim())) ? 0 : Convert.ToDecimal(row["Discount"].ToString().Trim().Replace("%", ""));

                                contract.GST = (row.IsNull("GST") || row["GST"] == DBNull.Value || string.IsNullOrEmpty(row["GST"].ToString().Trim())) ? 0 : Convert.ToDecimal(row["GST"].ToString().Trim().Replace("%", ""));
                                contract.CUSTOM_DUTY = (row.IsNull("CustomDuty") || row["CustomDuty"] == DBNull.Value || string.IsNullOrEmpty(row["CustomDuty"].ToString().Trim())) ? 0 : Convert.ToDecimal(row["CustomDuty"].ToString().Trim().Replace("%", ""));
                                contract.DISCOUNT_ELIGIBILITY = (row.IsNull("EligibilityToRedeemDiscount") || row["EligibilityToRedeemDiscount"] == DBNull.Value || string.IsNullOrEmpty(row["EligibilityToRedeemDiscount"].ToString().Trim())) ? string.Empty : (row["EligibilityToRedeemDiscount"].ToString().Trim());
                                contract.RECONCILIATION_TIMELINES = (row.IsNull("ReconciliationTimelines") || row["ReconciliationTimelines"] == DBNull.Value || string.IsNullOrEmpty(row["ReconciliationTimelines"].ToString().Trim())) ? string.Empty : (row["ReconciliationTimelines"].ToString().Trim());
                                contract.CURRENCY = (row.IsNull("Currency") || row["Currency"] == DBNull.Value || string.IsNullOrEmpty(row["Currency"].ToString().Trim())) ? string.Empty : (row["Currency"].ToString().Trim());
                                contract.PO_CURRENCY = (row.IsNull("POCurrency") || row["POCurrency"] == DBNull.Value || string.IsNullOrEmpty(row["POCurrency"].ToString().Trim())) ? string.Empty : (row["POCurrency"].ToString().Trim());
                                contract.EXCHANGE_RATE = (row.IsNull("ExchRate") || row["ExchRate"] == DBNull.Value || string.IsNullOrEmpty(row["ExchRate"].ToString().Trim())) ? 0 : Convert.ToDecimal(row["ExchRate"].ToString().Trim().Replace("%", ""));
                                contract.AllocationPercentage = (row.IsNull("Allocation Percentage") || row["Allocation Percentage"] == DBNull.Value || string.IsNullOrEmpty(row["Allocation Percentage"].ToString().Trim())) ? 0 : Convert.ToDecimal(row["Allocation Percentage"].ToString().Trim().Replace("%", ""));
                                contract.ContractStatus = (row.IsNull("Contract Status") || row["Contract Status"] == DBNull.Value || string.IsNullOrEmpty(row["Contract Status"].ToString().Trim())) ? string.Empty : (row["Contract Status"].ToString().Trim());

                                int productId = 0;
                                bool isValidProduct = true;
                                try
                                {
                                    productId = (row.IsNull("ProductId") || row["ProductId"] == DBNull.Value || string.IsNullOrEmpty(row["ProductId"].ToString().Trim())) ? 0 : Convert.ToInt32(row["ProductId"].ToString().Trim());
                                }
                                catch
                                {
                                    isValidProduct = false;
                                    //throw new Exception($"Invalid Productid.");
                                }

                                if (productId <= 0 || !isValidProduct)
                                {
                                    throw new Exception("Product Id is mandatory, 0 for new products and valid Id for existing products.");
                                }

                                contract.ProductId = productId;
                                contract.NET_PRICE = contract.Value * (1 - (contract.DISCOUNT)/100);
                                contract.DISCOUNT = contract.DISCOUNT * 100;
                                contract.GST = contract.GST * 100;
                                contract.CUSTOM_DUTY = contract.CUSTOM_DUTY * 100;

                                if (contract.Quantity < contract.AvailedQuantity)
                                {
                                    throw new Exception($"Availed quantity cannot be greater than quantity");
                                }

                                if (string.IsNullOrEmpty(contract.SelectedVendorCode))
                                {
                                    throw new Exception($"Vendor Code cannot be empty.");
                                }

                                if (string.IsNullOrEmpty(contract.Number))
                                {
                                    throw new Exception($"Contract Number cannot be empty.");
                                }

                                if (contract.Value <= 0)
                                {
                                    throw new Exception("Price cannot be 0");
                                }

                                if (contract.ProductId > 0 && !string.IsNullOrWhiteSpace(contract.Number) && !string.IsNullOrWhiteSpace(contract.SelectedVendorCode))
                                {
                                    contract.IsValid = 1;
                                    contract.VendorId = 0;
                                    contract.CompanyName = "";
                                    contract.SessionId = sessionid;
                                    var contractResponse = SaveProductContract(contract);
                                    if (contractResponse != null && !string.IsNullOrWhiteSpace(contractResponse.ErrorMessage))
                                    {
                                        throw new Exception(contractResponse.ErrorMessage);
                                    }
                                    totalProcessed++;
                                    count++;
                                }
                            }
                            catch (Exception ex)
                            {
                                string error = ex.Message;
                                if (error.ToLower().Contains("valid datetime") || error.ToLower().Contains("valid date time"))
                                {
                                    error = "Invalid date time";
                                }

                                errorMessage = $"<strong>Row-{rowNumber}:Material: {materialName} - {error}</strong>";
                                sqlHelper.ExecuteNonQuery_IUD($@"INSERT INTO UPLOAD_MODULE_ERROR_DETAILS(JOB_ID,REASON,TEMPLATE_NAME,COMP_ID,U_ID,DATE_CREATED)     
                                                 VALUES ('{guid}','{errorMessage}','CONTRACT_DETAILS',{compid},{user},NOW())");
                                failedRows++;
                                logger.Error(ex, errorMessage);
                                errorMessage = string.Empty;
                            }
                        }

                        response.ObjectId = count;
                    }
                }

                summary += $"{Environment.NewLine} Total rows processed: {totalProcessed}.";
                
                if (failedRows > 0)
                {
                    summary += $"{Environment.NewLine} Total rows failed: {failedRows}.";
                }

                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    throw new Exception(errorMessage);
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.ErrorMessage = response.ErrorMessage;
            }

            response.ErrorMessage = $"Summary: {summary} {Environment.NewLine} {Environment.NewLine}" + response.ErrorMessage;
            return response;
        }

        public List<ContractUtilisation> GetContractUtilisations(int compId, int userId, string sessionid, string contracts, string prs, 
            string products, string vendors, string pos, string status, string fromDate, string toDate,int pcID,
            int PageSize = 0, int NumberOfRecords = 0, string searchString = null) 
        {
            List<ContractUtilisation> response = new List<ContractUtilisation>();
            Utilities.ValidateSession(sessionid);
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                CORE.DataNamesMapper<ContractUtilisation> mapper = new CORE.DataNamesMapper<ContractUtilisation>();
                sd.Add("P_COMP_ID", compId);
                sd.Add("P_U_ID", userId);
                sd.Add("P_CONTRACTS", contracts);
                sd.Add("P_PRS", prs);
                sd.Add("P_PRODUCTS", products);
                sd.Add("P_VENDORS", vendors);
                sd.Add("P_POS", pos);
                sd.Add("P_STATUS", status);
                sd.Add("P_PAGE", PageSize);
                sd.Add("P_PAGE_SIZE", NumberOfRecords);
                sd.Add("P_SEARCH", searchString);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                sd.Add("P_PC_ID", pcID);
                DataSet ds = sqlHelper.SelectList("cm_GetContractUtilisation", sd);
                if (ds != null & ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (var row in ds.Tables[0].AsEnumerable())
                    {
                        ContractUtilisation detail = new ContractUtilisation();
                        detail.PC_ID = row["CONTRACT_ID"] != DBNull.Value ? Convert.ToInt32(row["CONTRACT_ID"]) : 0;
                        detail.COMP_ID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                        detail.U_ID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        detail.VENDOR_ID = row["VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_ID"]) : 0;
                        detail.PR_NUMBER = row["PR_NUMBER"] != DBNull.Value ? Convert.ToString(row["PR_NUMBER"]) : string.Empty;
                        detail.PR_LINE_ITEM = row["PR_LINE_ITEM"] != DBNull.Value ? Convert.ToString(row["PR_LINE_ITEM"]) : string.Empty;
                        detail.PO_NUMBER = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                        detail.PO_LINE_ITEM = row["PO_LINE_ITEM"] != DBNull.Value ? Convert.ToString(row["PO_LINE_ITEM"]) : string.Empty;
                        detail.QTY_UTILISED = row["QTY_UTILISED"] != DBNull.Value ? Convert.ToDecimal(row["QTY_UTILISED"]) : 0;
                        detail.PRODUCT_NAME = row["PRODUCT_NAME"] != DBNull.Value ? Convert.ToString(row["PRODUCT_NAME"]) : string.Empty;
                        detail.VENDOR_COMP_NAME = row["VENDOR_COMP_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_COMP_NAME"]) : string.Empty;
                        detail.CONTRACT_NUMBER = row["CONTRACT_NUMBER"] != DBNull.Value ? Convert.ToString(row["CONTRACT_NUMBER"]) : string.Empty;
                        detail.CREATED_BY_NAME = row["CREATED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["CREATED_BY_NAME"]) : string.Empty;
                        detail.DATE_CREATED = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.MaxValue;
                        detail.TOTAL_ROWS = row["TOTAL_ROWS"] != DBNull.Value ? Convert.ToInt32(row["TOTAL_ROWS"]) : 0;
                        detail.PLANT_CODE = row["PLANT_CODE"] != DBNull.Value ? Convert.ToString(row["PLANT_CODE"]) : string.Empty;
                        response.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
            return response;
        }

        public List<PRMName.PRFieldMapping> GetContractUtilisationsFilters(int compId, int userId, string sessionid)
        {
            List<PRMName.PRFieldMapping> details = new List<PRMName.PRFieldMapping>();
            DataSet ds = new DataSet();
            Utilities.ValidateSession(sessionid);
            try {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                CORE.DataNamesMapper<PRMName.PRFieldMapping> mapper = new CORE.DataNamesMapper<PRMName.PRFieldMapping>();
                sd.Add("P_COMP_ID", compId);
                sd.Add("P_U_ID", userId);
                var dataset = sqlHelper.SelectList("cm_GetContractUtilisationFilters", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex) {
                logger.Error(ex, ex.Message);
            }
            return details;
        }


        public CatalogResponse BulkUpdateContractStatus(string status, string sessionId, int uId, string contractIds)
        {
            CatalogResponse response = new CatalogResponse();
            Utilities.ValidateSession(sessionId);
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_STATUS", status);
                sd.Add("P_U_ID", uId);
                sd.Add("P_CONTRACT_IDS", contractIds);
                var dataset = sqlHelper.SelectList("cm_BulkUpdateContractStatus", sd);
                response.ErrorMessage = string.Empty;
            }
            catch (Exception ex) {
                response.ErrorMessage = ex.Message;
                logger.Error(ex, ex.Message);
            }
            return response;
        }

        public CatalogResponse UpdateContractsDailyBased(string user) 
        {
            CatalogResponse response = new CatalogResponse();
            if (user == "JOB_USER")
            {
                try
                {
                    response.ErrorMessage = $@"Contract Status Update Job Triggered on {DateTime.Now} Successfully. {Environment.NewLine}";
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    var dataset = sqlHelper.SelectList("cm_UpdateContractsDailyBased", sd);
                    response.ErrorMessage += $@"Contract status update job ran successfully on {DateTime.Now}. " +
                        Environment.NewLine + " and all the contracts for which availed & total quantity is equal (or) end time is completed " +
                        " their contract status has been updated to closed. ";
                }
                catch (Exception ex)
                {
                    response.ErrorMessage += $@"Error in Contract Status Update Job {Environment.NewLine} Error is >>> {ex.Message + ex.InnerException}";
                    logger.Error("Error in Contract Status Update Job status>>" + ex.Message);
                    response.ErrorMessage = ex.Message + ex.InnerException;
                }
            }
            else {
                response.ErrorMessage = "Invalid Access";
            }
            
            logger.Debug(response.ErrorMessage);
            return response;
        }


        public List<PRMName.QCSVendors> GetItemDetailsforContract(int REQ_ID, int QCS_ID, string sessionid)
        {
            List<PRMName.QCSVendors> details = new List<PRMName.QCSVendors>();
            List<PRMName.QCSVendorContractItems> details1 = new List<PRMName.QCSVendorContractItems>();
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<PRMName.QCSVendorContractItems> mapper = new CORE.DataNamesMapper<PRMName.QCSVendorContractItems>();
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", REQ_ID);
                sd.Add("P_QCS_ID", QCS_ID);
                DataSet dataSet = sqlHelper.SelectList("cm_getContractCreationItems", sd);
                details1 = mapper.Map(dataSet.Tables[0]).ToList();
                if (details1 != null && details1.Count > 0)
                {
                    var groupedItems = details1.GroupBy(x => new { x.VENDOR_ID }).Select(a => a.Key).ToList();

                    if (groupedItems != null && groupedItems.Count > 0) 
                    {
                        foreach (var vendorContractItem in groupedItems) 
                        {
                            int vendorID = 0;
                            var contractItems = details1.Where(item => item.VENDOR_ID == vendorContractItem.VENDOR_ID).ToList();
                            var vendorName = contractItems.Select(a => a.VENDOR_COMPANY_NAME).FirstOrDefault();
                            vendorID = contractItems.Select(a => a.VENDOR_ID).FirstOrDefault();

                            PRMName.QCSVendors newObj = new PRMName.QCSVendors();
                            newObj.VENDOR_NAME = vendorName;
                            newObj.VENDOR_ID = vendorID;

                            newObj.QCS_VENDOR_CONTRACT_ITEMS = contractItems;

                            details.Add(newObj);
                        }


                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data Get ItemDetails for Contract " + ex.Message);
            }
            return details;
        }
    }
}
