﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('save-pr-details', {
                    url: '/save-pr-details/:Id',
                    templateUrl: 'pr/views/save-pr-details.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('pr-actions', {
                    url: '/pr-actions/:Id',
                    templateUrl: 'pr/views/pr-actions.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('list-pr', {
                    url: '/list-pr',
                    templateUrl: 'pr/views/list-pr.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('list-archived-pr', {
                    url: '/list-archived-pr',
                    templateUrl: 'pr/views/list-archived-pr.html',
                    params: {
                        detailsObj: null
                    }
                })
				   .state('list-pr-v2', {
                    url: '/list-pr-v2',
                    templateUrl: 'pr/views/list-pr-v2.html',
                    params: {
                        detailsObj: null
                    }
                })
				;
        }]);﻿prmApp
    .controller('listprCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices",
        "PRMCustomFieldService", "catalogService", "$location", "PRMUploadServices", "$element", "fileReader",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices, PRMCustomFieldService, catalogService, $location, PRMUploadServices, $element, fileReader) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.sessionID = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.myAuctions1 = [];
            $scope.myAuctionsFiltred = [];
            $scope.selectedPRItems = [];
            $scope.prmTemplates = [];
            $scope.selectedTemplate = {};
            $scope.selectedRFP;
            $scope.selectedPR;
            $scope.chooseContractOption = false;
            $scope.isAchivedPRs = 0;
            if ($state.current.name === 'list-archived-pr') {
                $scope.isAchivedPRs = 1;
            }
            $scope.stateDetails = {
                poTemplate: 'po-contract-domestic-zsdm'
            };
            $scope.filteredRequirements = [];
            $scope.PRStats = {
                totalPRs: 0,
                newPRs: 0,
                partialPRs: 0,
                inProgressPRs: 0,
                totalPRItems: 0,
                totalRFQPosted: 0
            };
            $scope.prExcelReport = [];

            /********  CONSOLIDATE PR ********/
            $scope.prDet = {
                prLevel: true
            };
            /********  CONSOLIDATE PR ********/

            $scope.filtersList = {
                plantList: [],
                wbsCodeList: [],
                projectTypeList: [],
                projectNameList: [],
                sectionHeadList: [],
                purchaseGroupList1: [],
                statusList: [],
                profitCentreList: [],
                requisitionList: []
            };

            $scope.filters = {
                status: '',
                plant: {},
                projectType: {},
                sectionHead: {},
                wbsCode: {},
                projectName: {},
                purcahseGroup: {},
                material: '',
                docType: '',
                purchaseGroup: '',
                searchKeyword: '',
                profitCentre: {},
                requisitioners: {},
                newPrStatus: {},
                requisitionerName: {},
                searchRequirement: '',
                selectedRequirement: null,
                prToDate: '',
                prFromDate: ''
            };

            $scope.filters.prToDate = moment().format('YYYY-MM-DD');
            $scope.filters.prFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");
            $scope.PRItemStats = {
                totalPRs: 0,
                newPRs: 0,
                partialPRs: 0,
                inProgressPRs: 0,
                totalPRItems: 0,
                totalRFQPosted: 0
            };
            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.PlantsList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
           
            $scope.materialGroupList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.purchaseGroupList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.docTypeList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.prStatusList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getprlist(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            $scope.totalItems2 = 0;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage2 = 10;
            $scope.maxSize2 = 5;

            $scope.setPage2 = function (pageNo) {
                $scope.currentPage1 = pageNo;
            };

            $scope.pageChanged2 = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            /*PAGINATION CODE*/


            $scope.PRList = [];
            $scope.filteredPRList = [];
            $scope.PRItems = [];

            $('.selected-items-box').bind('click', function (e) {
                $('.multiple-selection-dropdown .list').slideToggle('fast');
            });
            var isFilter = false;
            $scope.setFilters = function (currentPage) {
                $scope.PRStats.totalPRs = 0;
                $scope.PRStats.newPRs =  0;
                $scope.PRStats.partialPRs = 0;
                $scope.PRStats.totalPRItems = 0;
                $scope.PRStats.totalRFQPosted = 0;
                $scope.PRStats.inProgressPRs = 0;

                if ($scope.prDet.prLevel) {
                    $scope.filteredPRList = $scope.PRList;
                    $scope.totalItems = $scope.filteredPRList.length;


                    if ($scope.filters.searchKeyword || !_.isEmpty($scope.filters.newPrStatus) || !_.isEmpty($scope.filters.profitCentre) || !_.isEmpty($scope.filters.plant) ||
                        !_.isEmpty($scope.filters.projectType) || !_.isEmpty($scope.filters.requisitionerName) ||
                        !_.isEmpty($scope.filters.sectionHead) || !_.isEmpty($scope.filters.wbsCode) || !_.isEmpty($scope.filters.purcahseGroup) ||
                        !_.isEmpty($scope.filters.projectName)) {
                       // $scope.getprlist(currentPage, 10, $scope.filters.searchKeyword);
                        $scope.getprlist(0, 10, $scope.filters.searchKeyword);
                    } else {
                        
                        if ($scope.initialPRPageArray && $scope.initialPRPageArray.length > 0) {
                            $scope.PRList = $scope.initialPRPageArray;
                            if ($scope.PRList && $scope.PRList.length > 0) {
                                $scope.totalItems = $scope.PRList[0].TOTAL_PR_COUNT;
                                $scope.PRStats.totalPRs = $scope.totalItems;
                                $scope.PRStats.newPRs = $scope.PRList[0].newPRs;
                                $scope.PRStats.partialPRs = $scope.PRList[0].partialPRs;
                                $scope.PRStats.totalPRItems = $scope.PRList[0].totalPRItems;
                                $scope.PRStats.totalRFQPosted = $scope.PRList[0].totalRFQPosted;
                                $scope.PRStats.inProgressPRs = $scope.PRList[0].inProgressPRs;
                                $scope.filteredPRList = $scope.PRList;
                            }

                        }
                    }
                } else {
                    $scope.getPRSBasedOnItem(0, 10);
                }
                
            };

            $scope.filterByDate = function () {
                $scope.PRStats.totalPRs = 0;
                $scope.PRStats.newPRs =  0;
                $scope.PRStats.partialPRs = 0;
                $scope.PRStats.totalPRItems = 0;
                $scope.PRStats.totalRFQPosted = 0;
                $scope.PRStats.inProgressPRs = 0;

                if ($scope.prDet.prLevel) {
                    $scope.getFilterValues();
                    $scope.filteredPRList = $scope.PRList;
                    $scope.totalItems = $scope.filteredPRList.length;
                    $scope.getprlist(0, 10, $scope.filters.searchKeyword);
                } else {
                    $scope.getPRSBasedOnItem(0,10);
                }
            };
            
            $scope.totalCount = 0;
            $scope.searchString = '';
            $scope.initialPRPageArray = [];

            $scope.getprlist = function (recordsFetchFrom,pageSize,searchString) {
                
                var plant, projectType, sectionHead, wbsCode, profitCentre, purchaseCode, creatorName, clientName, prStatus, fromDate, toDate;

                if (_.isEmpty($scope.filters.prFromDate)) {
                    fromDate = '';
                } else {
                    fromDate = $scope.filters.prFromDate;
                }

                if (_.isEmpty($scope.filters.prToDate)) {
                    toDate = '';
                } else {
                    toDate = $scope.filters.prToDate;
                }

                if (_.isEmpty($scope.filters.plant)) {
                    plant = '';
                } else if ($scope.filters.plant && $scope.filters.plant.length > 0) {
                    var plants = _($scope.filters.plant)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    plant = plants.join(',');
                }

                if (_.isEmpty($scope.filters.projectType)) {
                    projectType = '';
                } else if ($scope.filters.projectType && $scope.filters.projectType.length > 0) {
                    var projectTypes = _($scope.filters.projectType)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    projectType = projectTypes.join(',');
                }

                if (_.isEmpty($scope.filters.sectionHead)) {
                    sectionHead = '';
                } else if ($scope.filters.sectionHead && $scope.filters.sectionHead.length > 0) {
                    var sectionHeads = _($scope.filters.sectionHead)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    sectionHead = sectionHeads.join(',');
                }

                if (_.isEmpty($scope.filters.wbsCode)) {
                    wbsCode = '';
                } else if ($scope.filters.wbsCode && $scope.filters.wbsCode.length > 0) {
                    var wbsCodes = _($scope.filters.wbsCode)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    wbsCode = wbsCodes.join(',');
                }

                if (_.isEmpty($scope.filters.profitCentre)) {
                    profitCentre = '';
                } else if ($scope.filters.profitCentre && $scope.filters.profitCentre.length > 0) {
                    var profitCentres = _($scope.filters.profitCentre)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    profitCentre = profitCentres.join(',');
                }

                if (_.isEmpty($scope.filters.purcahseGroup)) {
                    purchaseCode = '';
                } else if ($scope.filters.purcahseGroup && $scope.filters.purcahseGroup.length > 0) {
                    var purchaseCodes = _($scope.filters.purcahseGroup)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    purchaseCode = purchaseCodes.join(',');
                }

                if (_.isEmpty($scope.filters.requisitionerName)) {
                    creatorName = '';
                } else if ($scope.filters.requisitionerName && $scope.filters.requisitionerName.length > 0) {
                    var creators = _($scope.filters.requisitionerName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    creatorName = creators.join(',');
                }

                if (_.isEmpty($scope.filters.projectName)) {
                    clientName = '';
                } else if ($scope.filters.projectName && $scope.filters.projectName.length > 0) {
                    var clientNames = _($scope.filters.projectName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    clientName = clientNames.join(',');
                }

                if (_.isEmpty($scope.filters.newPrStatus)) {
                    prStatus = '';
                } else if ($scope.filters.newPrStatus && $scope.filters.newPrStatus.length > 0) {
                    var statusFilters = _($scope.filters.newPrStatus)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    prStatus = statusFilters.join(',');
                }
                
                var params = {
                    "userid": userService.getUserId(),
                    "sessionid": userService.getUserToken(),
                    "deptid": userService.getSelectedUserDepartmentDesignation().deptID,
                    "desigid": userService.getSelectedUserDesigID(),
                    "depttypeid": userService.getSelectedUserDepartmentDesignation().deptTypeID,
                    "plant": plant,
                    "projectType": projectType,
                    "sectionHead": sectionHead,
                    "wbsCode": wbsCode,
                    "profitCentre": profitCentre,
                    "purchaseCode": purchaseCode,
                    "creatorName": creatorName,
                    "clientName": clientName,
                    "prStatus": prStatus,
                    "searchString": searchString ? searchString : "",
                    "PageSize": recordsFetchFrom * pageSize,
                    'fromDate': fromDate,
                    'toDate': toDate,
                    "NumberOfRecords": pageSize,
                    "prType": $scope.filters.prType ? $scope.filters.prType : '',
                    'isArchived': $scope.isAchivedPRs
                };

               // $scope.currentPage1 = (params.PageSize + 1);
                //$scope.NumberOfRecords = recordsFetchFrom > 0 ? ((recordsFetchFrom + 1) * pageSize) : $scope.PRStats.totalPRs;
                $scope.pageSizeTemp = (params.PageSize + 1);
                $scope.NumberOfRecords = ((recordsFetchFrom + 1) * pageSize);
               
                PRMPRServices.getprlist(params)
                    .then(function (response) {
                        //$scope.consolidatedReport = response;
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                var releaseDateTemp = item.RELEASE_DATE ? moment(item.RELEASE_DATE).format("DD-MM-YYYY") : '-';
                                item.RELEASE_DATE = releaseDateTemp != '-' ? releaseDateTemp.contains("1970") ? '-' : releaseDateTemp : '-';
                                item.MODIFIED_DATE = userService.toLocalDate(item.MODIFIED_DATE).split(' ')[0];
                                item.ARCHIVED_DATE = userService.toLocalDate(item.ARCHIVED_DATE).split(' ')[0];
                            });
                        }
                        if (!$scope.downloadExcel) {
                            $scope.PRList = [];
                            $scope.filteredPRList = [];
                            if (response && response.length > 0) {
                                response.forEach(function (item, index) {
                                    item.selectPRSForRFQ = false;
                                    $scope.PRList.push(item);
                                    if ($scope.initialPRPageArray.length <= 9) { // Push Initial 10 Records When Page is Loaded because needed in SetFilters function it's getting called every time (need to modify directive code)  #Crap Code need to remove(should think of another solution)
                                        $scope.initialPRPageArray.push(item);
                                    }
                                });

                            }

                            if ($scope.PRList && $scope.PRList.length > 0) {
                                $scope.totalItems = $scope.PRList[0].TOTAL_PR_COUNT;
                                $scope.PRStats.totalPRs = $scope.totalItems;
                                $scope.PRStats.newPRs = $scope.PRList[0].newPRs;
                                $scope.PRStats.partialPRs = $scope.PRList[0].partialPRs;
                                $scope.PRStats.totalPRItems = $scope.PRList[0].totalPRItems;
                                $scope.PRStats.totalRFQPosted = $scope.PRList[0].totalRFQPosted;
                                $scope.PRStats.inProgressPRs = $scope.PRList[0].inProgressPRs;
                                $scope.filteredPRList = $scope.PRList;
                            }
                        } else {
                            if (response && response.length > 0) {
                                $scope.prExcelReport = response;
                                downloadPRExcel()
                            } else {
                                swal("Error!", "No records.", "error");
                                $scope.downloadExcel = false;
                            }
                        }

                    });
            };

            $scope.getprlist(0,10,$scope.searchString);
            $scope.filterValues = [];

            $scope.getFilterValues = function () {
                var params =
                {
                    "compid": $scope.compId,
                    'fromDate': $scope.filters.prFromDate,
                    'toDate': $scope.filters.prToDate
                };

                let plantListTemp = [];
                let wbsCodeTemp = [];
                let projectTypeTemp = [];
                let projectNameTemp = [];
                let sectionHeadTemp = [];
                let purchaseGroupTemp = [];
                let statusTemp = [];
                let profitCentreTemp = [];
                let requisitionListTemp = [];

                PRMPRServices.getFilterValues(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.filterValues = response;
                            if ($scope.filterValues && $scope.filterValues.length > 0 ) {
                                $scope.filterValues.forEach(function (item, index) {
                                    if (item.TYPE === 'WBS_CODE') {
                                        wbsCodeTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PROFIT_CENTER') {
                                        profitCentreTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PROJECT_TYPE') {
                                        projectTypeTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'SECTION_HEAD') {
                                        sectionHeadTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PROJECT_DESCRIPTION') {
                                        projectNameTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PLANTS') {
                                        plantListTemp.push({ id: item.ID, name: item.ID + ' - ' + item.NAME });
                                    } else if (item.TYPE === 'PURCHASE_GROUP') {
                                       // purchaseGroupTemp.push({ id: +item.NAME, name: item.ID });
                                        purchaseGroupTemp.push({ id: item.NAME, name: item.NAME });
                                    } else if (item.TYPE === 'PR_CREATOR_NAME') {
                                        requisitionListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PR_STATUS') {
                                        statusTemp.push({ id: item.ID, name: item.NAME });
                                    }

                                });

                                $scope.filtersList.requisitionList = requisitionListTemp;
                                $scope.filtersList.plantList = plantListTemp;
                                $scope.filtersList.wbsCodeList = wbsCodeTemp;
                                $scope.filtersList.projectTypeList = projectTypeTemp;
                                $scope.filtersList.sectionHeadList = sectionHeadTemp;
                                $scope.filtersList.projectNameList = projectNameTemp;
                                $scope.filtersList.purchaseGroupList1 = purchaseGroupTemp;
                                $scope.filtersList.statusList = statusTemp;
                                $scope.filtersList.profitCentreList = profitCentreTemp;
                            }
                        }
                    });

            };
            $scope.getFilterValues();


            $scope.selectedPRItems = [];

            $scope.getPrItems = function (prDetails, createRequirement) {
                if (prDetails) {
                    var params = {
                        "prid": prDetails.PR_ID
                    };
                    PRMPRServices.getpritemslist(params)
                        .then(function (response) {
                            console.log(response);
                            $scope.PRItems = response;
                            $scope.PRItems.forEach(function (item, index) {
                                //item.CREATED_DATE = userService.toLocalDate(item.CREATED_DATE).split(' ')[0];
                                //item.DELIVERY_DATE = userService.toLocalDate(item.DELIVERY_DATE).split(' ')[0];
                            });

                            prDetails.PRItems = $scope.PRItems;

                            if (createRequirement) {
                                if (prDetails) {
                                    prDetails.isSelected = true;
                                    //$scope.selectedPRItems = [];
                                    if (prDetails.PRItems) {
                                        prDetails.PRItems.forEach(function (prItem) {
                                            prItem.isSelected = true;
                                        });
                                    }
                                    $scope.selectedPRItems.push(prDetails);
                                    //$scope.createRequirementMultiplePR();
                                }
                                $scope.selectedPRItems = _.uniqBy($scope.selectedPRItems, 'PR_ID');
                            }
                            
                            if (prDetails.selectPRSForRFQ) {
                                prDetails.PRItems.forEach(function (item, index) {
                                    item.isCheckedPrItem = true;
                                });
                            }
                        });
                }
            };


            $scope.goToPrEdit = function (id) {
                var url = $state.href("save-pr-details", { "Id": id });
                window.open(url, '_self');
            };

            $scope.goToPrAction = function (id) {
                var url = $state.href("pr-actions", { "Id": id });
                window.open(url, '_blank');
            };

            $scope.createRequirement = function (prDetails) {
                $scope.getPrItems(prDetails, true);
            };

            $scope.createRequirementMultiplePR = function () {
                $scope.ProductContractsLoaded = false;
                $scope.ProductContracts = [];
                $scope.ProductRequirements = [];
                $scope.getProductContracts();
                $scope.getProductRelatedRequirements();
            };

            $scope.addToRequirementList = function (prDetails, item, action) {
                if (action === 'ADD') {
                    item.isSelected = true;
                    prDetails.isSelected = true;
                    let index = _.findIndex($scope.selectedPRItems, function (pr) {
                        return pr.PR_ID === prDetails.PR_ID;
                    });

                    if (index < 0) {
                        $scope.selectedPRItems.push(prDetails);
                    }
                    
                } else {
                    item.isSelected = false;
                    let selectedPRItems = _.filter(prDetails.PRItems, function (prItem) {
                        return prItem.isSelected;
                    });

                    if (selectedPRItems && selectedPRItems.length > 0) {
                        prDetails.isSelected = true;
                    } else {
                        prDetails.isSelected = false;
                    }
                }


                console.log($scope.selectedPRItems);
            };

            $scope.showRequirementButton = function () {
                let isvisible = false;
                $scope.selectedPRNumbers = '';

                if ($scope.selectedPRItems && $scope.selectedPRItems.length > 0) {
                    let validSelectedPRs = _.filter($scope.selectedPRItems, function (prObj) {
                        return prObj.isSelected;
                    });

                    if (validSelectedPRs && validSelectedPRs.length > 0) {
                        var prNumbers = _(validSelectedPRs)
                            .map('PR_NUMBER')
                            .value();
                        $scope.selectedPRNumbers = prNumbers.join(',');
                        isvisible = true;

                    }
                }
                
                return isvisible;
            };
            $scope.requirementMappingClick = function () {
                $scope.ProductContractsLoaded = false;
                $scope.ProductContracts = [];
                $scope.ProductRequirements = [];
                $scope.getProductContracts();
                $scope.getProductRelatedRequirements();
            };
            $scope.getProductRelatedRequirements = function () {
                let productArray = [];
                $scope.contractPODetails = {
                    contractTable: [],
                    plantsArray: []
                }; //Keep local

                if ($scope.prDet.prLevel) {
                    let validSelectedPRs = _.filter($scope.selectedPRItems, function (prItemObj) {
                        return prItemObj.isSelected;
                    });

                    validSelectedPRs.forEach(function (prObj, index) {
                        prObj.PRItems.forEach(function (item, index1) {
                            if (!item.CONTRACT_NUMBER && !item.REQ_ID) {
                                productArray.push(item.PRODUCT_ID);
                                let contactTableExistngItem = _.filter($scope.contractPODetails.contractTable, function (tableItem) {
                                    return tableItem.productId === item.PRODUCT_ID;
                                });

                                if (!_.some($scope.contractPODetails.plantsArray, { plantCode: prObj.PLANT })) {
                                    $scope.contractPODetails.plantsArray.push({
                                        plantCode: prObj.PLANT,
                                        plantName: prObj.PLANT_NAME
                                    });
                                }
                                if (contactTableExistngItem && contactTableExistngItem.length > 0) {
                                    contactTableExistngItem[0].plantName = contactTableExistngItem[0].plantName + ',' + prObj.PLANT_NAME;
                                    contactTableExistngItem[0].plantCode = contactTableExistngItem[0].plantCode + ',' + prObj.PLANT;
                                    contactTableExistngItem[0].prNumber = contactTableExistngItem[0].prNumber + ',' + prObj.PR_NUMBER;
                                    contactTableExistngItem[0].prQuantity = contactTableExistngItem[0].prQuantity + prObj.REQUIRED_QUANTITY;
                                } else {
                                    $scope.contractPODetails.contractTable.push({
                                        productName: item.ITEM_NAME,
                                        productId: item.PRODUCT_ID,
                                        plantName: prObj.PLANT_NAME,
                                        plantCode: prObj.PLANT,
                                        plantCodeArr: [],
                                        prNumber: prObj.PR_NUMBER,
                                        prQuantity: item.REQUIRED_QUANTITY,
                                        prItemID: item.ITEM_ID
                                    });
                                }
                            }
                        });
                    });
                }

                if (!$scope.prDet.prLevel && $scope.selectedItemsForRFQ && $scope.selectedItemsForRFQ.length > 0) {
                    //$scope.selectedItemsForRFQ.forEach(function (item, index) {
                    //    productArray.push(item.PRODUCT_ID);
                    //});

                    $scope.selectedItemsForRFQ.forEach(function (prItem, index) {
                        prItem.PlantCodeArr = [];
                        prItem.PRNumbers = '';
                        prItem.ItemPRS.forEach(function (prObj, index1) {
                            productArray.push(prItem.PRODUCT_ID);
                            prItem.PRNumbers = prItem.PRNumbers + ',' + prObj.PR_NUMBER;
                            let contactTableExistngItem = _.filter($scope.contractPODetails.contractTable, function (tableItem) {
                                return tableItem.productId === prItem.PRODUCT_ID;
                            });

                            if (!prItem.PlantCodeArr.includes(prObj.PLANT)) {
                                prItem.PlantCodeArr.push(prObj.PLANT);
                            }

                            if (!_.some($scope.contractPODetails.plantsArray, { plantCode: prObj.PLANT })) {
                                $scope.contractPODetails.plantsArray.push({
                                    plantCode: prObj.PLANT,
                                    plantName: prObj.PLANT_NAME
                                });
                            }

                            if (contactTableExistngItem && contactTableExistngItem.length > 0) {
                                contactTableExistngItem[0].plantName = contactTableExistngItem[0].plantName + ',' + prObj.PLANT_NAME;
                                contactTableExistngItem[0].plantCode = contactTableExistngItem[0].plantCode + ',' + prObj.PLANT;
                                contactTableExistngItem[0].prNumber = contactTableExistngItem[0].prNumber + ',' + prObj.PR_NUMBER;
                                contactTableExistngItem[0].prQuantity = contactTableExistngItem[0].prQuantity + prItem.RFQ_QUANTITY;
                            } else {
                                $scope.contractPODetails.contractTable.push({
                                    productName: prItem.ITEM_NAME,
                                    productId: prItem.PRODUCT_ID,
                                    plantName: prObj.PLANT_NAME,
                                    plantCode: prObj.PLANT,
                                    plantCodeArr: [],
                                    prNumber: prObj.PR_NUMBER,
                                    prQuantity: prItem.RFQ_QUANTITY,
                                    prItemID: item.ITEM_ID
                                });
                            }
                        });
                    });
                }

                if (productArray && productArray.length > 0) {
                    let productIds = productArray.join();
                    if (productIds) {
                        auctionsService.GetRequirementsByProductIds({ "productids": productIds, "compid": $scope.compId, "sessionid": userService.getUserToken() })
                            .then(function (response) {
                                if (response) {
                                    $scope.ProductContractsLoaded = true;
                                    $scope.ProductRequirements = response;
                                    if ($scope.ProductRequirements && $scope.ProductRequirements.length > 0) {
                                        $scope.ProductRequirements[0].isSelected = true;
                                        $scope.ProductRequirements.forEach(function (item, index) {
                                            item.endTime = item.endTime ? userService.toLocalDate(item.endTime).split(' ')[0] : '-';
                                            item.isSelected = false;
                                        });
                                    }
                                }
                            });
                    }
                }
            };
           
            $scope.getProductContracts = function () {
                let productArray = [];
                $scope.contractPODetails = {
                    contractTable: [],
                    plantsArray: []
                }; //Keep local

                if ($scope.prDet.prLevel) {
                    let validSelectedPRs = _.filter($scope.selectedPRItems, function (prItemObj) {
                        return prItemObj.isSelected;
                    });

                    validSelectedPRs.forEach(function (prObj, index) {
                        prObj.PRItems.forEach(function (item, index1) {
                            if (!item.CONTRACT_NUMBER && !item.REQ_ID && item.isSelected) {
                                productArray.push(item.PRODUCT_ID);
                                let contactTableExistngItem = _.filter($scope.contractPODetails.contractTable, function (tableItem) {
                                    return tableItem.productId === item.PRODUCT_ID;
                                });

                                if (!_.some($scope.contractPODetails.plantsArray, { plantCode: prObj.PLANT })) {
                                    $scope.contractPODetails.plantsArray.push({
                                        plantCode: prObj.PLANT,
                                        plantName: prObj.PLANT_NAME
                                    });
                                }
                                if (contactTableExistngItem && contactTableExistngItem.length > 0) {
                                    contactTableExistngItem[0].plantName = contactTableExistngItem[0].plantName + ',' + prObj.PLANT_NAME;
                                    contactTableExistngItem[0].plantCode = contactTableExistngItem[0].plantCode + ',' + prObj.PLANT;
                                    contactTableExistngItem[0].prNumber = contactTableExistngItem[0].prNumber + ',' + prObj.PR_NUMBER;
                                    contactTableExistngItem[0].prQuantity = contactTableExistngItem[0].prQuantity + prObj.REQUIRED_QUANTITY;
                                } else {
                                    $scope.contractPODetails.contractTable.push({
                                        productName: item.ITEM_NAME,
                                        productId: item.PRODUCT_ID,
                                        plantName: prObj.PLANT_NAME,
                                        plantCode: prObj.PLANT,
                                        plantCodeArr: [],
                                        prNumber: prObj.PR_NUMBER,
                                        prQuantity: item.REQUIRED_QUANTITY,
                                        prItemID: item.ITEM_ID
                                    });
                                }
                            }
                        });
                    });
                }

                if (!$scope.prDet.prLevel && $scope.selectedItemsForRFQ && $scope.selectedItemsForRFQ.length > 0) {
                    //$scope.selectedItemsForRFQ.forEach(function (item, index) {
                    //    productArray.push(item.PRODUCT_ID);
                    //});

                    $scope.selectedItemsForRFQ.forEach(function (prItem, index) {
                        prItem.PlantCodeArr = [];
                        prItem.PRNumbers = '';
                        prItem.ItemPRS.forEach(function (prObj, index1) {
                            productArray.push(prItem.PRODUCT_ID);
                            prItem.PRNumbers = prItem.PRNumbers + ',' + prObj.PR_NUMBER;
                            let contactTableExistngItem = _.filter($scope.contractPODetails.contractTable, function (tableItem) {
                                return tableItem.productId === prItem.PRODUCT_ID;
                            });

                            if (!prItem.PlantCodeArr.includes(prObj.PLANT)) {
                                prItem.PlantCodeArr.push(prObj.PLANT);
                            }

                            if (!_.some($scope.contractPODetails.plantsArray, { plantCode: prObj.PLANT })) {
                                $scope.contractPODetails.plantsArray.push({
                                    plantCode: prObj.PLANT,
                                    plantName: prObj.PLANT_NAME
                                });
                            }

                            if (contactTableExistngItem && contactTableExistngItem.length > 0) {
                                contactTableExistngItem[0].plantName = contactTableExistngItem[0].plantName + ',' + prObj.PLANT_NAME;
                                contactTableExistngItem[0].plantCode = contactTableExistngItem[0].plantCode + ',' + prObj.PLANT;
                                contactTableExistngItem[0].prNumber = contactTableExistngItem[0].prNumber + ',' + prObj.PR_NUMBER;
                                contactTableExistngItem[0].prQuantity = contactTableExistngItem[0].prQuantity + prItem.RFQ_QUANTITY;
                            } else {
                                $scope.contractPODetails.contractTable.push({
                                    productName: prItem.ITEM_NAME,
                                    productId: prItem.PRODUCT_ID,
                                    plantName: prObj.PLANT_NAME,
                                    plantCode: prObj.PLANT,
                                    plantCodeArr: [],
                                    prNumber: prObj.PR_NUMBER,
                                    prQuantity: prItem.RFQ_QUANTITY,
                                    prItemID: item.ITEM_ID
                                });
                            }
                        });
                    });
                }

                if (productArray && productArray.length > 0) {
                    let productIds = productArray.join();
                    if (productIds) {
                        catalogService.GetProductContracts(productIds)
                            .then(function (response) {
                                if (response) {
                                    $scope.ProductContractsLoaded = true;
                                    console.log(response);
                                    $scope.ProductContracts = _.filter(response, function (contractitem) {
                                        return contractitem.contractStatus === 'Active';
                                    });
                                    if ($scope.ProductContracts && $scope.ProductContracts.length > 0) {
                                        $scope.ProductContracts.forEach(function (contract) {
                                            contract.startTime = userService.toLocalDate(contract.startTime);
                                            contract.endTime = userService.toLocalDate(contract.endTime);
                                        });

                                        if ($scope.contractPODetails && $scope.contractPODetails.contractTable && $scope.contractPODetails.contractTable.length > 0) {
                                            $scope.contractPODetails.contractTable.forEach(function (contract) {
                                                contract.contactsArray = _.filter($scope.ProductContracts, function (contractitem) {
                                                    return contractitem.ProductId === contract.productId;
                                                });
                                            });
                                        }
                                    }
                                }
                            });
                    }
                }
            };
            $scope.chooseContract = function () {
                $scope.chooseContractOption = false;
                if ($scope.contractPODetails.contractTable && $scope.contractPODetails.contractTable.length > 0) {
                    $scope.contractPODetails.contractTable.forEach(function (contactItem) {
                        if (contactItem && contactItem.contactsArray && contactItem.contactsArray.length > 0) {
                            contactItem.contactsArray.forEach(function (contract) {
                                if (contract.isSelected) {
                                    $scope.chooseContractOption = true;
                                }
                            });
                        }
                    });
                }
            };
            $scope.navigateToPOForm = function () {
                if ($scope.contractPODetails && $scope.contractPODetails.contractTable && $scope.contractPODetails.contractTable.length > 0) {
                    angular.element('#templateSelection').modal('hide');
                    let selectedTemplate = '';
                    if ($scope.stateDetails.poTemplate === 'po-contract-domestic-zsdm') {
                        selectedTemplate = 'ZSDM';
                    } else if ($scope.stateDetails.poTemplate === 'po-contract-import-zsim') {
                        selectedTemplate = 'ZSIM';
                    } else if ($scope.stateDetails.poTemplate === 'po-contract-bonded-wh') {
                        selectedTemplate = 'ZSBW';
                    } else if ($scope.stateDetails.poTemplate === 'po-contract-service-zssr') {
                        selectedTemplate = 'ZSSR';
                    }

                    let prDetails = [];
                    let isGMP = false;
                    if ($scope.prDet.prLevel) {
                        prDetails = _.filter($scope.selectedPRItems, function (prItemObj) {
                            return prItemObj.isSelected;
                        });

                        let temp = _.filter(prDetails, function (prItemObj) {
                            return prItemObj.GMP === 'GMP';
                        });

                        if (temp && temp.length > 0) {
                            isGMP = true;
                        }

                        prDetails[0].isGMP = isGMP;
                    }
                    else {
                        var rfqPRSList = $scope.selectedItemsForRFQ.filter(function (item) {
                            return item.selectForRFQ && item.RFQ_QUANTITY !== item.OVERALL_ITEM_QUANTITY;
                        });

                        rfqPRSList.forEach(function (prItem, index) {
                            if (!isGMP && prItem && prItem.ItemPRS && prItem.ItemPRS.length > 0) {
                                prItem.ItemPRS.forEach(function (prObj, index) {
                                    if (!isGMP && prObj && prObj.GMP === 'GMP') {
                                        isGMP = true;
                                    }
                                });
                            }
                        });

                        prDetails.push({
                            PRItems: rfqPRSList
                        });

                        prDetails[0].isGMP = isGMP;
                    }

                    let vendorAssignments = [];
                    $scope.contractPODetails.contractTable.forEach(function (contactItem, index) {
                        if (contactItem) {
                            contactItem.contactsArray.forEach(function (item, index) {
                                if (item.isSelected) {
                                    vendorAssignments.push({
                                        vendorID: item.vendorId,
                                        vendorCode: item.selectedVendorCode,
                                        vendorName: item.companyName,
                                        itemID: item.ProductId,
                                        item: item,
                                        assignedQty: contactItem.prQuantity,
                                        assignedPrice: item.netPrice, //item.price,
                                        totalPrice: item.netPrice, // item.price, //contactItem.prQuantity * item.price,
                                        currency: '',
                                        contractNumber: item.number
                                    });
                                }
                            });
                        }
                    });

                    $state.go(
                        $scope.stateDetails.poTemplate,
                        { 'contractDetails': $scope.contractPODetails, 'prDetails': prDetails, 'detailsObj': vendorAssignments, 'templateName': selectedTemplate, 'quoteLink': $location.absUrl() }
                    );
                }
            };

            $scope.showRequirementItemsButton = function () {
                let isvisible = false;

                if ($scope.selectedItemsForRFQ && $scope.selectedItemsForRFQ.length > 0) {

                    let selectedPRItems = _.filter($scope.selectedItemsForRFQ, function (prItem) {
                        return prItem.selectForRFQ && prItem.OVERALL_ITEM_QUANTITY != prItem.RFQ_QUANTITY;
                    });

                    if (selectedPRItems && selectedPRItems.length > 0) {
                        isvisible = true;
                    }
                }
                return isvisible;
            };

            $scope.showLinkToRFP = function (prDetails) {
                $scope.selectedPR = prDetails;
                $scope.getAuctions();
            };

            $scope.getAuctions = function () {
                auctionsService.SearchRequirements({ "search": '', "excludeprlinked": true, "compid": $scope.compId, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        if (response) {
                            $scope.myAuctions1 = [];
                            $scope.myAuctionsFiltred = [];
                            $scope.myAuctions1 = response;
                            $scope.myAuctionsFiltred = $scope.myAuctions1;
                        }
                    });
            };

            $scope.LinkToRFP = function () {
                if ($scope.selectedPR && $scope.myAuctionsFiltred && $scope.myAuctionsFiltred.length > 0) {
                    let selectedRFPs = $scope.myAuctionsFiltred.filter(function (rfq) {
                        return rfq.isSelected;
                    });

                    if (selectedRFPs && selectedRFPs.length > 0) {
                        selectedRFPs.forEach(function (rfpDetails, itemIndexs) {
                            var params = {
                                "userid": userService.getUserId(),
                                "sessionid": userService.getUserToken(),
                                "reqid": rfpDetails.requirementId,
                                "prid": $scope.selectedPR.PR_ID
                            };
                            PRMPRServices.linkRFPToPR(params)
                                .then(function (response) {
                                    if (response && response.errorMessage === '') {
                                        growlService.growl("Successfully link to RFP", "success");
                                        angular.element('#linkRFP').modal('hide');
                                        $scope.myAuctionsFiltred = [];
                                        console.log(response);
                                        $scope.myAuctions1.forEach(function (rfpTemp, itemIndexs) {
                                            rfpTemp.isSelected = false;
                                        });
                                        $scope.myAuctionsFiltred.forEach(function (rfpTemp, itemIndexs) {
                                            rfpTemp.isSelected = false;
                                        });
                                    } else {
                                        growlService.growl("Error linking PR to RFP, please contact support team.", "inverse");
                                    }
                                });
                        });
                    }
                }
            };

            $scope.isLast = function (last) {
                var a = '';
                if (last) {
                    a = '';
                } else {
                    a = ',';
                }
                return a;
            };

            $scope.searchRFPs = function (str) {
                var filterText = str ? str.toUpperCase() : '';
                if (!filterText) {
                    $scope.myAuctionsFiltred = $scope.myAuctions1;
                }
                else {
                    $scope.myAuctionsFiltred = $scope.myAuctions1.filter(function (req) {
                        return req.REQ_TITLE.toUpperCase().includes(filterText) || String(req.requirementId) === filterText || String(req.REQ_NUMBER) === filterText;
                    });
                }
            };



            /********  CONSOLIDATE PR ********/
            $scope.prDet = {
                prLevel: true
            };

            $scope.totalItems1 = 0;
            $scope.currentPage1 = 1;
            $scope.itemsPerPage1 = 10;
            $scope.maxSize1 = 5;

            $scope.setPage1 = function (pageNo) {
                $scope.currentPage1 = pageNo;
                $scope.getPRSBasedOnItem(($scope.currentPage1 - 1), 10);
            };

            $scope.pageChanged1 = function (pageNo) {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };


            $scope.itemsList = [];
            $scope.getPRSBasedOnItem = function (recordsFetchFrom, pageSize) {
                $scope.selectedPRNumbers = '';
                var plant, projectType, sectionHead, wbsCode, profitCentre = '',purchaseCode = '';

                if (_.isEmpty($scope.filters.plant)) {
                    plant = '';
                } else if ($scope.filters.plant && $scope.filters.plant.length > 0) {
                    var plants = _($scope.filters.plant)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    plant = plants.join(',');
                }

                if (_.isEmpty($scope.filters.projectType)) {
                    projectType = '';
                } else if ($scope.filters.projectType && $scope.filters.projectType.length > 0) {
                    var projectTypes = _($scope.filters.projectType)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    projectType = projectTypes.join(',');
                }

                if (_.isEmpty($scope.filters.sectionHead)) {
                    sectionHead = '';
                } else if ($scope.filters.sectionHead && $scope.filters.sectionHead.length > 0) {
                    var sectionHeads = _($scope.filters.sectionHead)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    sectionHead = sectionHeads.join(',');
                }

                if (_.isEmpty($scope.filters.wbsCode)) {
                    wbsCode = '';
                } else if ($scope.filters.wbsCode && $scope.filters.wbsCode.length > 0) {
                    var wbsCodes = _($scope.filters.wbsCode)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    wbsCode = wbsCodes.join(',');
                }

                if (_.isEmpty($scope.filters.profitCentre)) {
                    profitCentre = '';
                } else if ($scope.filters.profitCentre && $scope.filters.profitCentre.length > 0) {
                    var profitCentres = _($scope.filters.profitCentre)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    profitCentre = profitCentres.join(',');
                }

                if (_.isEmpty($scope.filters.purcahseGroup)) {
                    purchaseCode = '';
                } else if ($scope.filters.purcahseGroup && $scope.filters.purcahseGroup.length > 0) {
                    var purchaseCodes = _($scope.filters.purcahseGroup)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    purchaseCode = purchaseCodes.join(',');
                }
                if (_.isEmpty($scope.filters.requisitionerName)) {
                    creatorName = '';
                } else if ($scope.filters.requisitionerName && $scope.filters.requisitionerName.length > 0) {
                    var creators = _($scope.filters.requisitionerName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    creatorName = creators.join(',');
                }
                if (_.isEmpty($scope.filters.projectName)) {
                    clientName = '';
                } else if ($scope.filters.projectName && $scope.filters.projectName.length > 0) {
                    var clientNames = _($scope.filters.projectName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    clientName = clientNames.join(',');
                }
                
                var params = {
                    "plant": plant,
                    "projectType": projectType,
                    "sectionHead": sectionHead,
                    "wbsCode": wbsCode,
                    "profitCentre": profitCentre,
                    "purchaseCode": purchaseCode,
                    "creatorName": creatorName,
                    "clientName": clientName,
                    "search": $scope.filters.searchKeyword ? $scope.filters.searchKeyword : '',
                    "PageSize": recordsFetchFrom * pageSize,
                    "NumberOfRecords": pageSize
                };
                $scope.pageSizeTemp1 = (params.PageSize + 1);
                $scope.NumberOfRecords = ((recordsFetchFrom+1) * pageSize);
                
                PRMPRServices.GetIndividualItems(params)
                    .then(function (response) {
                        $scope.itemsList = response;
                        $scope.itemsList.forEach(function (item, index) {
                            item.selectForRFQ = false;
                            item.IsDisabled = false;
                            item.SELECTED_QUANTITY = 0;
                            item.RFQ_QUANTITY = 0;
                            item.FILTERED_PR_COUNT = 0;
                        });
                        $scope.totalItems1 = $scope.itemsList[0].TOTAL_PR_ITEM_COUNT;
                        $scope.PRItemStats.totalPRs = $scope.totalItems1;
                        ////addCategoryToFilters(response);
                        //if ($scope.itemsList.length > 0) {
                        //    $scope.totalItems1 = $scope.itemsList.length;
                        //} else {
                        //    $scope.totalItems1 = 0;
                        //}

                    });
            };

            $scope.getPRLevelView = function () {
                $scope.selectedPRNumbers = '';
            };

            $scope.ItemPRS = [];
            $scope.ItemPRSTemp = [];
            $scope.ItemPRSPopUp = [];
            $scope.ItemPRSPopUpTemp = [];

            $scope.GetPRSbyItem = function (prItem,type,productid) {

                var plant, projectType, sectionHead, wbsCode, profitCentre = '';

                if (_.isEmpty($scope.filters.plant)) {
                    plant = '';
                } else if ($scope.filters.plant && $scope.filters.plant.length > 0) {
                    var plants = _($scope.filters.plant)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    plant = plants.join(',');
                }

                if (_.isEmpty($scope.filters.projectType)) {
                    projectType = '';
                } else if ($scope.filters.projectType && $scope.filters.projectType.length > 0) {
                    var projectTypes = _($scope.filters.projectType)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    projectType = projectTypes.join(',');
                }

                if (_.isEmpty($scope.filters.sectionHead)) {
                    sectionHead = '';
                } else if ($scope.filters.sectionHead && $scope.filters.sectionHead.length > 0) {
                    var sectionHeads = _($scope.filters.sectionHead)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    sectionHead = sectionHeads.join(',');
                }

                if (_.isEmpty($scope.filters.wbsCode)) {
                    wbsCode = '';
                } else if ($scope.filters.wbsCode && $scope.filters.wbsCode.length > 0) {
                    var wbsCodes = _($scope.filters.wbsCode)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    wbsCode = wbsCodes.join(',');
                }

                if (_.isEmpty($scope.filters.profitCentre)) {
                    profitCentre = '';
                } else if ($scope.filters.profitCentre && $scope.filters.profitCentre.length > 0) {
                    var profitCentres = _($scope.filters.profitCentre)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    profitCentre = profitCentres.join(',');
                }



                var params = {
                    "PRODUCT_ID": prItem.PRODUCT_ID,
                    "plant": plant,
                    "projectType": projectType,
                    "sectionHead": sectionHead,
                    "wbsCode": wbsCode,
                    "profitCentre": profitCentre,
                    "search": $scope.filters.searchKeyword ? $scope.filters.searchKeyword : ''
                };

                if (!prItem.ItemPRS) {
                    PRMPRServices.GetPRSbyItem(params)
                        .then(function (response) {
                            $scope.ItemPRS = response;
                            $scope.ItemPRSPopUp = response;
                            $scope.ItemPRSPopUpTemp = response;
                            $scope.ItemPRSTemp = response;
                            //prItem.FILTERED_PR_COUNT = $scope.ItemPRS.length;
                            var prids = _.uniqBy($scope.ItemPRS, 'PR_NUMBER');
                            prItem.FILTERED_PR_COUNT = prids.length;
                            /**** Disabling For All Items ****/
                            $scope.itemsList.forEach(function (item, index) {
                                item.IsDisabled = false;
                            });
                            /**** Disabling For All Items ****/
                           
                            $scope.ItemPRS.forEach(function (item, index) {
                                //item.CREATED_DATE = moment(item.CREATED_DATE).format("YYYY-MM-DD");
                               
                                var releaseDateTemp = item.RELEASE_DATE ? moment(item.RELEASE_DATE).format("DD-MM-YYYY") : '-';
                                item.RELEASE_DATE = releaseDateTemp != '-' ? releaseDateTemp.contains("1970") ? '-' : releaseDateTemp : '-';

                                //if (item.COMPLETED_ITEMS === item.TOTAL_ITEMS) {
                                //    
                                //    item.NEW_PR_STATUS = "In Progess"
                                //} else if (item.COMPLETED_ITEMS > 0 && item.COMPLETED_ITEMS < item.TOTAL_ITEMS) {
                                //    item.NEW_PR_STATUS = "Partial"
                                //} else {
                                //    item.NEW_PR_STATUS = "New"
                                //}
                                

                                prItem.isExpanded1 = true;
                                
                                item.isChecked = prItem.isExpanded1;
                               

                            });

                            

                            $scope.ItemPRSPopUp = $scope.ItemPRS;
                            $scope.ItemPRSPopUpTemp = $scope.ItemPRS;
                            $scope.ItemPRSTemp = $scope.ItemPRS;

                            //$scope.ItemPRSPopUp.forEach(function (itemPopup, indexPopup) {
                            //    itemPopup.CREATED_DATE = userService.toLocalDate(itemPopup.CREATED_DATE).split(' ')[0];
                            //    itemPopup.RELEASE_DATE = userService.toLocalDate(itemPopup.RELEASE_DATE).split(' ')[0];
                            //    prItem.isExpanded1 = true;
                            //    itemPopup.isChecked = prItem.isExpanded1;
                            //});

                            //$scope.ItemPRSPopUpTemp.forEach(function (itemPopupTemp, indexPopup) {
                            //    itemPopupTemp.CREATED_DATE = userService.toLocalDate(itemPopupTemp.CREATED_DATE).split(' ')[0];
                            //    itemPopupTemp.RELEASE_DATE = userService.toLocalDate(itemPopupTemp.RELEASE_DATE).split(' ')[0];
                            //    prItem.isExpanded1 = true;
                            //    itemPopupTemp.isChecked = prItem.isExpanded1;
                            //});

                            $scope.totalItems2 = $scope.ItemPRSPopUp.length;


                            calculateQuantity($scope.ItemPRS, prItem);
                            $scope.ItemPRSTemp.forEach(function (item, index) {
                                //item.CREATED_DATE = userService.toLocalDate(item.CREATED_DATE);
                                prItem.isExpanded1 = true;
                                item.isChecked = prItem.isExpanded1;
                            });
                            prItem.ItemPRS = $scope.ItemPRS;

                            prItem.ItemPRSTemp = prItem.ItemPRS;

                            /********** RFQ Posting With Selected Items ***********/
                            if (prItem.selectForRFQ) {
                                $scope.AddItemToRequirement(prItem, prItem.selectForRFQ);
                            }
                            /********** RFQ Posting With Selected Items ***********/
                            
                            if (type && type === 'ITEM_DISPLAY')
                            {
                                $scope.displayItems(productid);
                            }


                        });
                } else {
                    /**** Disabling For All Items ****/
                    //calculateQuantity($scope.ItemPRS, prItem);
                    $scope.itemsList.forEach(function (item, index) {
                        item.IsDisabled = false;
                    });
                    /**** Disabling For All Items ****/


                    /********** RFQ Posting With Selected Items ***********/
                    if (prItem.selectForRFQ) {
                        $scope.AddItemToRequirement(prItem, prItem.selectForRFQ);
                    }
                    /********** RFQ Posting With Selected Items ***********/

                    if (type && type === 'ITEM_DISPLAY') {
                        $scope.displayItems(productid);
                    }
                }

            };

            $scope.searchPR = function (searchText, prItem) {
                var filterText = angular.lowercase(searchText);
                if (!filterText || filterText == '' || filterText == undefined || filterText == null) {
                    prItem.ItemPRS = prItem.ItemPRSTemp;
                    $scope.consolidate(prItem, filterText);
                }
                else {
                    prItem.ItemPRS = prItem.ItemPRSTemp.filter(function (pr) {
                        return (String(angular.lowercase(pr.PR_NUMBER)).includes(filterText) == true);
                    });
                }

                $scope.totalItems1 = prItem.ItemPRS.length;

            };


            $scope.unSelectAll = function (prItem, searchedPR) {

                if (!prItem.isExpanded1) {
                    prItem.ItemPRS.forEach(function (item, index) {
                        if (item.REQ_ID <= 0) {
                            item.isChecked = false;
                        }
                    });
                    $scope.consolidate(prItem, searchedPR, prItem.isExpanded1, 'OVERALL');
                } else {
                    prItem.ItemPRS.forEach(function (item, index) {
                        item.isChecked = true;
                    });
                    $scope.consolidate(prItem, searchedPR, prItem.isExpanded1, 'OVERALL');
                }

            };

            $scope.consolidate = function (pr, searchedPR, isChecked, type) {
                if (searchedPR == null || searchedPR == "undefined" || searchedPR == "") {
                    if (pr && pr.ItemPRS.length > 0) {
                        var consolidateQuantity = pr.ItemPRS.filter(function (items) { return items.isChecked && items.REQ_ID <= 0; });
                        var getRFQQuantity = pr.ItemPRS.filter(function (items) { return items.isChecked && items.REQ_ID > 0; });
                        if (consolidateQuantity && consolidateQuantity.length > 0) {
                            pr.SELECTED_QUANTITY = _.sumBy(consolidateQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                            pr.RFQ_QUANTITY = _.sumBy(getRFQQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                        } else {
                            pr.SELECTED_QUANTITY = 0;
                        }
                    }
                }
                else {
                    searchedPR = angular.lowercase(searchedPR);
                    var searchedFilteredPRS = [];
                    if (pr.isExpanded1) {
                        if (type === 'OVERALL') {
                            searchedFilteredPRS = $scope.ItemPRSTemp;
                            if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                calculateQuantity(searchedFilteredPRS, pr);
                            }
                        } else {
                            if (!isChecked) {
                                var unCheckedPRID = _.result(_.find(pr.ItemPRS, function (items) {
                                    return !items.isChecked;
                                }), 'PR_ID'); // get Unchecked PR IR's

                                searchedFilteredPRS = $scope.ItemPRSTemp.filter(function (item) { return item.PR_ID != unCheckedPRID; }); // remove the unchecked PR and calculate

                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }

                            } else {
                                searchedFilteredPRS = $scope.ItemPRSTemp; // Assign All The PR's from DB
                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }
                            }
                        }
                    } else {
                        if (type === 'OVERALL') {

                            var prOverallIds = _(pr.ItemPRS)
                                .filter(item => !item.isChecked)
                                .map('PR_ID')
                                .value();

                            searchedFilteredPRS = $scope.ItemPRSTemp.filter(function (item) { return !prOverallIds.includes(item.PR_ID); }); // remove the unchecked PR and calculate

                            if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                calculateQuantity(searchedFilteredPRS, pr);
                            }

                        } else {
                            if (!isChecked) {
                                var itemUnCheckedPRID = _.result(_.find(pr.ItemPRS, function (items) {
                                    return !items.isChecked;
                                }), 'PR_ID'); // get Unchecked PR IR's

                                searchedFilteredPRS = $scope.ItemPRSTemp.filter(function (item) { return item.PR_ID != itemUnCheckedPRID; }); // remove the unchecked PR and calculate

                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }

                            } else {
                                searchedFilteredPRS = $scope.ItemPRSTemp; // Assign All The PR's from DB
                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }
                            }
                        }
                    }

                }
            };


            function calculateQuantity(searchedFilteredPRS, pr) {
                var searchConsolidatedQuantity = searchedFilteredPRS.filter(function (items) { return items.isChecked && items.REQ_ID <= 0; }); // calculate all the checked PR's Quantity
                var getRFQQuantity = searchedFilteredPRS.filter(function (items) { return items.isChecked && items.REQ_ID > 0; });
                if (searchConsolidatedQuantity && searchConsolidatedQuantity.length > 0) {
                    pr.SELECTED_QUANTITY = _.sumBy(searchConsolidatedQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                    pr.RFQ_QUANTITY = _.sumBy(getRFQQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                } else if (getRFQQuantity && getRFQQuantity.length > 0) {
                    pr.RFQ_QUANTITY = _.sumBy(getRFQQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                } else {
                    pr.SELECTED_QUANTITY = 0;
                }
            }

            function addCategoryToFilters(catArray) {
                var categoryListresp = catArray;
                categoryListresp = _.uniqBy(categoryListresp, 'CATEGORY_ID');
                categoryListresp.forEach(function (item, index) {
                    var catObj = {
                        FIELD_NAME: '',
                        FIELD_VALUE: ''
                    };
                    catObj.FIELD_NAME = item.CategoryName;
                    catObj.FIELD_VALUE = item.CATEGORY_ID;
                    $scope.categoryList.push(catObj);
                });

                $scope.categoryList = _.uniqBy($scope.categoryList, 'FIELD_VALUE');

            }


            $scope.PostRequirement = function (prItem) {
                $scope.itemsList.forEach(function (item, index) {
                    item.IsDisabled = true;
                });
                if (prItem.selectForRFQ) {
                    $scope.GetPRSbyItem(prItem);
                } else {
                    $scope.itemsList.forEach(function (item, index) {
                        item.IsDisabled = false;
                    });

                    $scope.AddItemToRequirement(prItem, prItem.selectForRFQ);
                }
            };

            $scope.selectedItemsForRFQ = [];
            $scope.AddItemToRequirement = function (prItem, type) {

                if (type) {
                    var checkedPRIDs = _(prItem.ItemPRS)
                        .filter(item => item.isChecked)
                        .map('PR_ID')
                        .value();

                    prItem.PR_ID = checkedPRIDs.join(',');
                    $scope.selectedItemsForRFQ.push(prItem);
                } else {
                    if ($scope.selectedItemsForRFQ && $scope.selectedItemsForRFQ.length > 0) {
                        var itemIndex = _.findIndex($scope.selectedItemsForRFQ, function (item) {
                            return item.PRODUCT_ID === prItem.PRODUCT_ID;
                        });

                        if (itemIndex >= 0) {
                            $scope.selectedItemsForRFQ.splice(itemIndex, 1);
                        }
                    }
                }

                $scope.selectedItemsForRFQ = _.uniqBy($scope.selectedItemsForRFQ, 'PRODUCT_ID');

            };

            $scope.createRFQWithConsolidatedItems = function () {
                let prmFieldMappingDetails = {};
                let isServiceRelatedITems = false;
                let isEmptyItemCode = false;
                var template = $scope.selectedTemplate && $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : 'PRM-DEFAULT';
                var params = {
                    "templateid": 0,
                    "templatename": template,
                    "sessionid": userService.getUserToken()
                };

                var rfqPRSList = $scope.selectedItemsForRFQ.filter(function (item) {
                    return item.selectForRFQ && item.RFQ_QUANTITY != item.OVERALL_ITEM_QUANTITY;
                });                

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });

                    if (prmFieldMappingDetails) {
                        prmFieldMappingDetails.IS_SEARCH_FOR_CAS = prmFieldMappingDetails.PRODUCT_CODE.FIELD_LABEL.toLowerCase().contains("cas number");
                        prmFieldMappingDetails.IS_SEARCH_FOR_MFCD = prmFieldMappingDetails.PRODUCT_NUMBER.FIELD_LABEL.toLowerCase().contains("mfcd code");
                        if (!isServiceRelatedITems && prmFieldMappingDetails.IS_SEARCH_FOR_CAS) {
                            isServiceRelatedITems = true;
                        }
                    }

                    rfqPRSList.forEach(function (item, index) {
                        if (!isEmptyItemCode && !(item.ITEM_CODE_CAS || item.CASNR) && !item.REQ_ID) {
                            isEmptyItemCode = true;
                        }
                    });

                    if (isServiceRelatedITems && isEmptyItemCode && false) {
                        swal("Error!", 'Selected template is not Valid as empty CAS number');
                    }
                    else if (rfqPRSList && rfqPRSList.length > 0) {
                        if ($scope.filters.selectedRequirement && $scope.filters.selectedRequirement.requirementId) {
                            $state.go('save-requirementAdv', { 'Id': $scope.filters.selectedRequirement.requirementId, 'prItemsList': rfqPRSList, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        } else {
                            $state.go('save-requirementAdv', { 'prItemsList': rfqPRSList, 'selectedTemplate': $scope.selectedTemplate, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        }
                    }
                });
            };

        /********  CONSOLIDATE PR ********/

            $scope.navigateToRequirement = function () {
                angular.element('#templateSelection').modal('hide');
                if ($scope.prDet.prLevel) {
                    $scope.GetPRMTemplateFields();
                } else {
                    $scope.createRFQWithConsolidatedItems();
                }
            };

            $scope.GetPRMTemplates = function () {
                PRMCustomFieldService.GetTemplates().then(function (response) {
                    $scope.prmTemplates = response;
                    if ($scope.prmTemplates && $scope.prmTemplates.length > 0) {
                        $scope.selectedTemplate = $scope.prmTemplates[0];
                    }
                });
            };

            $scope.GetPRMTemplates();


            $scope.GetPRMTemplateFields = function () {
                let prmFieldMappingDetails = {};
                let isServiceRelatedITems = false;
                let isEmptyItemCode = false;
                var template = $scope.selectedTemplate && $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : 'PRM-DEFAULT';
                var params = {
                    "templateid": 0,
                    "templatename": template,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });

                    if (prmFieldMappingDetails) {
                        prmFieldMappingDetails.IS_SEARCH_FOR_CAS = prmFieldMappingDetails.PRODUCT_CODE.FIELD_LABEL.toLowerCase().contains("cas number");
                        prmFieldMappingDetails.IS_SEARCH_FOR_MFCD = prmFieldMappingDetails.PRODUCT_NUMBER.FIELD_LABEL.toLowerCase().contains("mfcd code");
                        if (!isServiceRelatedITems && prmFieldMappingDetails.IS_SEARCH_FOR_CAS) {
                            isServiceRelatedITems = true;
                        }
                    }

                    let validSelectedPRs = _.filter($scope.selectedPRItems, function (prItemObj) {
                        return prItemObj.isSelected;
                    });

                    validSelectedPRs.forEach(function (prObj, index) {
                        prObj.PRItems.forEach(function (item, index1) {
                            if (prmFieldMappingDetails.IS_SEARCH_FOR_CAS && (item.ITEM_CODE_CAS || item.CASNR)) {
                                item.ITEM_CODE = item.ITEM_CODE_CAS || item.CASNR;
                            }

                            if (prmFieldMappingDetails.IS_SEARCH_FOR_MFCD && (item.ITEM_CODE_MFCD || item.MFCD_NUMBER)) {
                                item.ITEM_NUM = item.ITEM_CODE_MFCD || item.MFCD_NUMBER;
                            }

                            if (!isEmptyItemCode && !(item.ITEM_CODE_CAS || item.CASNR) && !item.REQ_ID && item.isCheckedPrItem) {
                                isEmptyItemCode = true;
                            }
                        });
                    });

                    if (isServiceRelatedITems && isEmptyItemCode && false) {
                        swal("Error!", 'Selected template is not Valid as empty CAS number');
                    }
                    else if (validSelectedPRs && validSelectedPRs.length > 0) {
                        if ($scope.filters.selectedRequirement && $scope.filters.selectedRequirement.requirementId) {
                            $state.go('save-requirementAdv', { 'Id': $scope.filters.selectedRequirement.requirementId, 'prDetailsList': validSelectedPRs, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        } else {
                            $state.go('save-requirementAdv', { 'prDetailsList': validSelectedPRs, 'selectedTemplate': $scope.selectedTemplate, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        }
                    }                    
                });
            };


            $scope.convertDate = function (date) {
                if (date) {
                    var convertedDateTemp = moment(date).format("DD-MM-YYYY");
                    return convertedDateTemp.contains("1970") ? '-' : convertedDateTemp;
                } else {
                    return '-';
                }
            };


            $scope.ItemsListPopup = [];

            $scope.showPRItemlevelDetails = function (prItem) {
                $scope.GetPRSbyItem(prItem, 'ITEM_DISPLAY', prItem.PRODUCT_ID);
            };

            $scope.displayItems = function (productId) {

                var PR_IDS = '';

                var pridspop = _($scope.ItemPRSPopUp)
                    .filter(item => item.PR_ID)
                    .map('PR_ID')
                    .value();
                PR_IDS = pridspop.join(',');

                var params = {
                    "prIds": PR_IDS
                };

                PRMPRServices.GetItemDetails(params)
                    .then(function (response) {
                        $scope.ItemsListPopup = response;

                        $scope.ItemsListPopup = $scope.ItemsListPopup.filter(function (item) {
                            return item.PRODUCT_ID === productId;
                        });

                    });
            };
            
            $scope.filterPRsPopup = function (searchText) {

                var filterText = angular.lowercase(searchText);

                if (filterText) {
                    $scope.ItemPRSPopUp = $scope.ItemPRSPopUpTemp.filter(function (pr) {
                        return (String(angular.lowercase(pr.PR_NUMBER)).includes(filterText) == true);
                    });
                } else {
                    $scope.ItemPRSPopUp = $scope.ItemPRSPopUpTemp;
                }

                $scope.totalItems2 = $scope.ItemPRSPopUp.length;
            };

            $scope.selectMultiplePRS = [];

            $scope.createRFQWithMultiplePR = function (prDet)
            {
                if (prDet.selectPRSForRFQ) {
                    var getCheckedPRIDs = _($scope.filteredPRList)
                        .filter(pr => pr.selectPRSForRFQ)
                        .map('PR_ID')
                        .value(); // GET checked PR_ID's

                    if (getCheckedPRIDs && getCheckedPRIDs.length > 0) {

                        var checkedPRObjects = $scope.filteredPRList.filter(function (prItem, prIndex) { return getCheckedPRIDs[0] === prItem.PR_ID; });

                        if (checkedPRObjects && checkedPRObjects.length > 0) {
                            $scope.createRequirement(prDet, true);
                        }
                    }
                } else {
                    
                    var getUnCheckedPRIDs = _($scope.selectedPRItems)
                        .filter(pr => !pr.selectPRSForRFQ)
                        .map('PR_ID')
                        .value(); // GET un checked PR_ID's 

                    prDet.PRItems.forEach(function (prItem, prIndex) {
                        prItem.isCheckedPrItem = false;
                    });
                    
                    var prDetIndex = _.findIndex($scope.selectedPRItems, function (item) {
                        return item.PR_ID === getUnCheckedPRIDs[0];
                    }); // filter the PR in the RFQ posting List and get the Index
                    
                    if (prDetIndex >= 0) { // if PR Found
                        $scope.selectedPRItems.splice(prDetIndex, 1); //remove the PR  and PR Items
                    }
                }
            };

            $scope.downloadExcel = false;
            $scope.GetReport = function () {
                $scope.PRList = [];
                $scope.downloadExcel = true;
                $scope.getprlist(0, 0, $scope.searchString);
            };

            $scope.searchRequirement = function () {
                $scope.filteredRequirements = [];
                auctionsService.SearchRequirements({ "search": $scope.filters.searchRequirement, "compid": $scope.compId, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        if (response) {
                            $scope.filteredRequirements = response;
                        }
                    });
            };

            $scope.selectRequirement = function () {
                //console.log($scope.filters.selectedRequirement);
            };

            function downloadPRExcel() {
                alasql('SELECT PR_NUMBER as [PR Number],PLANT as [Plant],PLANT_NAME as [Plant Name], ' +
                    'NEW_PR_STATUS as [Status],RELEASE_DATE as [Release Date], ' +
                    'PURCHASE_GROUP_CODE as [Purchase Group Code], ' +
                    'PR_TYPE1 as [PR Type]' +

                    'INTO XLSX(?, { headers: true, sheetid: "PR_DETAILS", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                    ["PR Details.xlsx", $scope.prExcelReport]);
                $scope.downloadExcel = false;
            }

            $scope.exportPOItemsToExcel = function () {
                //alasql('SELECT WERKS  as [PLANT_CODE], MATNR  as [MATERIAL_CODE], TXZ01  as [MATERIAL_DESCRIPTION], MATKL  as [MAT_GROUP], LIFNR  as [VENDOR_CODE], NAME1  as [VENDOR_NAME], TELF1  as [VENDOR_PRIMARY_PHONE_NUMBER], SMTP_ADDR  as [VENDOR_PRIMARY_EMAIL], EBELN  as [PO_NUMBER], EBELP  as [PO_LINE_ITEM], AEDAT  as [PO_DATE], ERNAM  as [PO_CREATOR], ZTERM  as [PAYMENT_TERMS], MWSKZ  as [TAX_CODE], TEXT1  as [TAX_CODE_DESC], UDATE  as [PO_RELEASE_DATE], EINDT  as [DELV_DATE], MTART  as [MAT_TYPE], ORT01  as [CITY], REGIO  as [REGION_DESC], J_1BNBM  as [HSN_CODE], ORD_QTY  as [ORD_QTY], ORD_QTYSpecified  as [ORD_QTYSPECIFIED], MEINS  as [UOM], MEINS1  as [ALTERNATIVE_UOM], MENGE1  as [QTY_IN_ALTERNATE_UOM], MENGE1Specified  as [MENGE1SPECIFIED], EFFWR  as [NET_PRICE], EFFWRSpecified  as [EFFWRSPECIFIED], NETWR1  as [VALUE_INR], NETWR1Specified  as [NETWR1SPECIFIED], WAERS  as [CURRENCY], KNUMV  as [FREIGHT], KNUMVSpecified  as [KNUMVSPECIFIED], MENGE2  as [PEND_QTY], MENGE2Specified  as [MENGE2SPECIFIED], BANFN  as [PR_LINE], BNFPO  as [PR_NUM], MENGE3  as [PR_QTY], MENGE3Specified  as [MENGE3SPECIFIED], UDATE1  as [PR_RELEASE_DATE], LTEXT1  as [PR_LINE_TEXT], LFDAT  as [PR_DELV_DATE], LTEXT2  as [ITEM_TEXT_PO], FRGKZ  as [REL_IND], AFNAM  as [PR_REQUISITIONER], BSART  as [DOC_TYPE], SRVPOS  as [SERVICE_CODE], ASKTX  as [SERVICE_DESCRIPTION], KNUMV1  as [MISC_CHARGES], KNUMV1Specified  as [KNUMV1SPECIFIED], KNUMV2  as [PACKING_CHARGES], KNUMV2Specified  as [KNUMV2SPECIFIED], BSTYP  as [PO_CONTRACT], KDATB  as [VALID_FROM], KDATE  as [VALID_TO], AEDAT1  as [AEDAT1], ELIKZ  as [DELIVERY_COMPLETION_INDICATOR], LOEKZ  as [DELETED INDICATOR], CGST  as [CGST], SGST  as [SGST], IGST  as [IGST], CESS  as [CESS], TCS  as [TCS], PO_ITEM_CHANGE_DATE  as [PO_ITEM_CHANGE_DATE], PO_MATERIAL_DESC  as [PO_MATERIAL_DESC], ITEM_MAT_TEXT  as [ITEM_MAT_TEXT], ITEM_GROSS_PRICE  as [ITEM_GROSS_PRICE], ITEM_DISCOUNT_VALUE  as [ITEM_DISCOUNT_VALUE], ITEM_DISCOUNT_PERCENTAGE  as [ITEM_DISCOUNT_PERCENTAGE], INCO_TERMS  as [INCO_TERMS], VENDOR_ADDRESS  as [VENDOR_ADDRESS], HEADER_TEXT  as [HEADER_TEXT] INTO XLSX(?, { headers: true, sheetid: "PO_DETAILS", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                //    ["PO Details.xlsx", $scope.poFields]);
                //alasql('SELECT * INTO XLSX(?, { headers: true, sheetid: "PO_DETAILS", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                //    ["PO Details.xlsx", $scope.poFields]);
                PRMUploadServices.GetExcelTemplate("PR_DETAILS");
            };

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);
                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];

                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            $scope.attachment = $.makeArray(bytearray);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;

                            if (id == "prclientupload") {
                                $scope.uploadPendingPO('PR_DETAILS', 0, fileUpload.fileName);
                                $scope.file = [];
                                $scope.file.name = '';
                            } else {
                                //if (!$scope.filteredPendingPOsList[id].multipleAttachments) {
                                //    $scope.filteredPendingPOsList[id].multipleAttachments = [];
                                //}

                                //var ifExists = _.findIndex($scope.filteredPendingPOsList[id].multipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                                //if (ifExists <= -1) {
                                //    $scope.filteredPendingPOsList[id].multipleAttachments.push(fileUpload);
                                //}
                            }


                        });
                })
            }

            $scope.showErrorPopup = false;
            $scope.rowErrors = [];

            $scope.uploadPendingPO = function (type, proceedFurther, fileName) {
                $scope.showErrorPopup = false;
                $scope.FILE_NAME = fileName ? fileName : $scope.FILE_NAME;
                var params = {
                    name: type,
                    compID: $scope.compId,
                    sessionID: $scope.sessionID,
                    tableName: 'SAP_PR_DETAILS',
                    attachment: $scope.attachment,
                    proceedFurther: proceedFurther,
                    fileName: $scope.FILE_NAME
                };
                PRMUploadServices.uploadTemplate(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            if (response[0].IS_SUCCESS) {
                                angular.element('#errorPopUp').modal('hide');
                                swal("Status!", "Total Count : " + response[0].TOTAL_COUNT + " \n " + " Successfull Count : " + response[0].SUCCESS_COUNT + " \n " + " Failed Count : " + response[0].FAILED_COUNT, "warning");
                                $("#prclientupload").val(null);
                                $scope.getprlist(0, 10, $scope.filters.searchKeyword);
                               /* $scope.getpendingPOlist(0, 10, $scope.filters.searchKeyword);*/
                            } else {
                                if (!response[0].IS_SUCCESS && proceedFurther === 1) {
                                    $scope.showErrorPopup = true;
                                    $scope.rowErrors = response;
                                    angular.element('#errorPopUp').modal('show');
                                    if (response[0].IS_NO_RECORDS) {
                                        swal("Status!", "Sheet is empty.. So cannot continue further ! Please validate the sheet again and upload.", "error");
                                    } else {
                                        swal("Status!", "All the records in this sheet contains errors.. So cannot continue further ! Please validate the sheet again and upload.", "error");
                                    }
                                    $("#prclientupload").val(null);
                                    angular.element('#errorPopUp').modal('hide');
                                } else {
                                    $scope.showErrorPopup = true;
                                    $scope.rowErrors = response;
                                    angular.element('#errorPopUp').modal('show');
                                    $("#prclientupload").val(null);
                                }
                            }
                        } else {
                            $("#prclientupload").val(null);
                            $scope.uploadPendingPO('PR_DETAILS', 1);
                        }
                    });

            };

            $scope.cancel = function () {
                angular.element('#errorPopUp').modal('hide');
            };

            $scope.exportErrorDetails = function (type) {
                $state.go('moduleErrorList', { 'moduleName': type });
            };

            $scope.archivePR = function (prDetails, isAchived) {
                let params = {
                    prid: prDetails.PR_ID,
                    isAchived: isAchived,
                    user: $scope.userID,
                    sessionid: $scope.sessionID
                };

                var text = isAchived ? "Archived" :"Un-Archived";

                PRMPRServices.archivePR(params)
                    .then(function (response) {
                        console.log(response);
                        if (response && response.objectID) {
                            swal("Success", "Successfully " + text + " ", "success");
                            $scope.setPage($scope.currentPage)
                        } else {
                            swal("Error!", "No records.", "error");
                        }                        
                    });
            };


            $scope.archivePRConfirm = function (prDetails, isAchived) {
                swal({
                    title: "Are you sure?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Confirm",
                    cancelButtonText: "Discard!",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function () {
                    $scope.archivePR(prDetails, isAchived);
                });
            };



            $scope.linkContract = function () {
                $scope.linkContractArray = [];
                if ($scope.contractPODetails.contractTable && $scope.contractPODetails.contractTable.length > 0) {
                    $scope.contractPODetails.contractTable.forEach(function (contactItem) {
                        if (contactItem && contactItem.contactsArray && contactItem.contactsArray.length > 0) {
                            let selectedContractItem = _.filter(contactItem.contactsArray, function (contract) {
                                return contract.isSelected;
                            });
                            if (selectedContractItem && selectedContractItem.length > 0) {
                                selectedContractItem.forEach(function (cItem, cIndex) {
                                    var obj = {
                                        PR_ITEM_ID: 0,
                                        ContractNumber: '',
                                    };
                                    obj.PR_ITEM_ID = contactItem.prItemID;
                                    obj.contractNumber = cItem.contractNumber;
                                    $scope.linkContractArray.push(obj);
                                })
                            }
                        }
                    });
                };

                var params = {
                    "contracts": $scope.linkContractArray,
                    "sessionID": $scope.sessionID,
                };
                PRMPRServices.linkContractToPRItems(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            growlService.growl("Succesfully Linked With Contracts", 'success');
                            location.reload();
                        } else {
                            growlService.growl(response.errorMessage, 'inverse');
                        }
                    })

            };


        }]);﻿prmApp
    .controller('listprv2Ctrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices", 
        "PRMCustomFieldService", "catalogService", "$location", '$timeout',
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices, PRMCustomFieldService, catalogService, $location, $timeout) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.sessionID = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.myAuctions1 = [];
            $scope.myAuctionsFiltred = [];
            $scope.selectedPRItems = [];
            $scope.prmTemplates = [];
            $scope.selectedTemplate = {};
            $scope.selectedRFP;
            $scope.selectedPR;
            $scope.filteredRequirements = [];
            $scope.ProductContractsLoaded = false;
            $scope.chooseContractOption = false;
            $scope.prRequirementContractToggle = true;
            $scope.ProductContracts = [];
            $scope.ProductRequirements = [];
            $scope.sapPRHistoryList = [];
            $scope.sapPRSearch = '';
            $scope.stateDetails = {
                poTemplate: 'po-contract-domestic-zsdm'
            };
            $scope.objectID = 0;

            $scope.PRStats = {
                totalPRs: 0,
                newPRs: 0,
                partialPRs: 0,
                inProgressPRs: 0,
                totalPRItems: 0,
                totalRFQPosted: 0
            };

            /********  CONSOLIDATE PR ********/
            $scope.prDet = {
                prLevel: true,
                selectedPRtype: 'ALL',
                contractprs: 0,
                nocontractreqprs: 0,
                reqprs: 0
            };
            /********  CONSOLIDATE PR ********/

            $scope.filtersList = {
                plantList: [],
                wbsCodeList: [],
                projectTypeList: [],
                projectNameList: [],
                sectionHeadList: [],
                purchaseGroupList1: [],
                statusList: [],
                profitCentreList: [],
                requisitionList: []
            };

            $scope.filters = {
                status: '',
                plant: {},
                projectType: {},
                sectionHead: {},
                wbsCode: {},
                projectName: {},
                purcahseGroup: {},
                material: '',
                docType: '',
                purchaseGroup: '',
                searchKeyword: '',
                profitCentre: {},
                requisitioners: {},
                newPrStatus: {},
                requisitionerName: {},
                searchRequirement: '',
                selectedRequirement: null,
                prToDate: '',
                prFromDate: '',
                prType: '',
                prType_old: ''
            };

            $scope.filters.prToDate = moment().format('YYYY-MM-DD');
            $scope.filters.prFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");
            $scope.selectedPRItems = [];

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.PlantsList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.materialGroupList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.purchaseGroupList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.docTypeList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.prStatusList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];

            $scope.setPageV2 = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getPRListV2(false, false);
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            $scope.totalItems2 = 0;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage2 = 10;
            $scope.maxSize2 = 5;

            $scope.setPage2 = function (pageNo) {
                $scope.currentPage1 = pageNo;
            };

            $scope.pageChanged2 = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            /*PAGINATION CODE*/


            $scope.filterByDate = function () {
                $scope.PRStats.totalPRs = 0;
                $scope.PRStats.newPRs = 0;
                $scope.PRStats.partialPRs = 0;
                $scope.PRStats.totalPRItems = 0;
                $scope.PRStats.totalRFQPosted = 0;
                $scope.PRStats.inProgressPRs = 0;

                if ($scope.prDet.prLevel) {
                    $scope.filteredPRList = $scope.PRList;
                    $scope.totalItems = $scope.filteredPRList.length;
                    $scope.getPRListV2(false, true);
                } else {
                    $scope.getPRSBasedOnItem(0, 10);
                }
            };

            $scope.PRList = [];
            $scope.filteredPRList = [];
            $scope.PRItems = [];

            $('.selected-items-box').bind('click', function (e) {
                $('.multiple-selection-dropdown .list').slideToggle('fast');
            });

            $scope.setFilters = function (currentPage) {
                $scope.filteredPRList = $scope.PRList;
                $scope.totalItems = $scope.filteredPRList.length;

                if ($scope.filters.searchKeyword || !_.isEmpty($scope.filters.newPrStatus) || !_.isEmpty($scope.filters.profitCentre) || !_.isEmpty($scope.filters.plant) ||
                    !_.isEmpty($scope.filters.projectType) || !_.isEmpty($scope.filters.requisitionerName) ||
                    !_.isEmpty($scope.filters.sectionHead) || !_.isEmpty($scope.filters.wbsCode) || !_.isEmpty($scope.filters.purcahseGroup) ||
                    !_.isEmpty($scope.filters.projectName)) {
                    $scope.getPRListV2(false, false);
                }
            };

            $scope.totalCount = 0;
            $scope.searchString = '';
            $scope.initialPRPageArray = [];

            $scope.filterValues = [];

            $scope.selectedPRItems = [];
            $scope.goToPrEdit = function (id) {
                var url = $state.href("save-pr-details", { "Id": id });
                window.open(url, '_self');
            };

            $scope.goToPrAction = function (id) {
                var url = $state.href("pr-actions", { "Id": id });
                window.open(url, '_blank');
            };

            $scope.requirementMappingClick = function () {
                $scope.ProductContractsLoaded = false;
                $scope.ProductContracts = [];
                $scope.ProductRequirements = [];
                $scope.getProductContracts();
                $scope.getProductRelatedRequirements();
            };

            $scope.addToRequirementList = function (prDetails, item, action) {
                if (action === 'ADD') {
                    item.isSelected = true;
                    prDetails.isSelected = true;
                    let index = _.findIndex($scope.selectedPRItems, function (pr) {
                        return pr.PR_ID === prDetails.PR_ID;
                    });

                    if (index < 0) {
                        $scope.selectedPRItems.push(prDetails);
                    }

                } else {
                    item.isSelected = false;
                    let selectedPRItems = _.filter(prDetails.PRItems, function (prItem) {
                        return prItem.isSelected;
                    });

                    if (selectedPRItems && selectedPRItems.length > 0) {
                        prDetails.isSelected = true;
                    } else {
                        prDetails.isSelected = false;
                    }
                }


                console.log($scope.selectedPRItems);
            };

            $scope.showRequirementButton = function () {
                let isvisible = false;
                $scope.selectedPRNumbers = '';
                if ($scope.selectedPRItems && $scope.selectedPRItems.length > 0) {
                    let validSelectedPRs = _.filter($scope.selectedPRItems, function (prObj) {
                        return prObj.isSelected;
                    });

                    if (validSelectedPRs && validSelectedPRs.length > 0) {
                        var prNumbers = _(validSelectedPRs)
                            .map('PR_NUMBER')
                            .value();

                        prNumbers = _.uniqBy(prNumbers);
                        $scope.selectedPRNumbers = prNumbers.join(',');
                        isvisible = true;
                    }
                }

                return isvisible;
            };

            $scope.showLinkToRFP = function (prDetails) {
                $scope.selectedPR = prDetails;
                $scope.getAuctions();
            };

            $scope.getAuctions = function () {
                auctionsService.SearchRequirements({ "search": '', "excludeprlinked": true, "compid": $scope.compId, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        if (response) {
                            $scope.myAuctions1 = [];
                            $scope.myAuctionsFiltred = [];
                            $scope.myAuctions1 = response;
                            $scope.myAuctionsFiltred = $scope.myAuctions1;
                        }
                    });
            };

            $scope.LinkToRFP = function () {
                if ($scope.selectedPR && $scope.myAuctionsFiltred && $scope.myAuctionsFiltred.length > 0) {
                    let selectedRFPs = $scope.myAuctionsFiltred.filter(function (rfq) {
                        return rfq.isSelected;
                    });

                    if (selectedRFPs && selectedRFPs.length > 0) {
                        selectedRFPs.forEach(function (rfpDetails, itemIndexs) {
                            var params = {
                                "userid": userService.getUserId(),
                                "sessionid": userService.getUserToken(),
                                "reqid": rfpDetails.requirementId,
                                "prid": $scope.selectedPR.PR_ID
                            };

                            PRMPRServices.linkRFPToPR(params)
                                .then(function (response) {
                                    if (response && response.errorMessage === '') {
                                        growlService.growl("Successfully link to RFP", "success");
                                        angular.element('#linkRFP').modal('hide');
                                        $scope.myAuctionsFiltred = [];
                                        console.log(response);
                                        $scope.myAuctions1.forEach(function (rfpTemp, itemIndexs) {
                                            rfpTemp.isSelected = false;
                                        });
                                        $scope.myAuctionsFiltred.forEach(function (rfpTemp, itemIndexs) {
                                            rfpTemp.isSelected = false;
                                        });
                                    } else {
                                        growlService.growl("Error linking PR to RFP, please contact support team.", "inverse");
                                    }
                                });
                        });
                    }
                }
            };

            $scope.isLast = function (last) {
                var a = '';
                if (last) {
                    a = '';
                } else {
                    a = ',';
                }
                return a;
            };

            $scope.searchRFPs = function (str) {
                var filterText = str ? str.toUpperCase() : '';
                if (!filterText) {
                    $scope.myAuctionsFiltred = $scope.myAuctions1;
                }
                else {
                    $scope.myAuctionsFiltred = $scope.myAuctions1.filter(function (req) {
                        return req.REQ_TITLE.toUpperCase().includes(filterText) || String(req.requirementId) === filterText || String(req.REQ_NUMBER) === filterText;
                    });
                }
            };

            /********  CONSOLIDATE PR ********/
            $scope.prDet = {
                prLevel: true
            };

            $scope.totalItems1 = 0;
            $scope.currentPage1 = 1;
            $scope.itemsPerPage1 = 10;
            $scope.maxSize1 = 5;

            $scope.setPage1 = function (pageNo) {
                $scope.currentPage1 = pageNo;
            };

            $scope.pageChanged1 = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };


            $scope.itemsList = [];
            $scope.ItemPRS = [];
            $scope.ItemPRSTemp = [];
            $scope.ItemPRSPopUp = [];
            $scope.ItemPRSPopUpTemp = [];

            $scope.GetPRSbyItem = function (prItem, type, productid) {

                var plant, projectType, sectionHead, wbsCode, profitCentre = '';

                if (_.isEmpty($scope.filters.plant)) {
                    plant = '';
                } else if ($scope.filters.plant && $scope.filters.plant.length > 0) {
                    var plants = _($scope.filters.plant)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    plant = plants.join(',');
                }

                if (_.isEmpty($scope.filters.projectType)) {
                    projectType = '';
                } else if ($scope.filters.projectType && $scope.filters.projectType.length > 0) {
                    var projectTypes = _($scope.filters.projectType)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    projectType = projectTypes.join(',');
                }

                if (_.isEmpty($scope.filters.sectionHead)) {
                    sectionHead = '';
                } else if ($scope.filters.sectionHead && $scope.filters.sectionHead.length > 0) {
                    var sectionHeads = _($scope.filters.sectionHead)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    sectionHead = sectionHeads.join(',');
                }

                if (_.isEmpty($scope.filters.wbsCode)) {
                    wbsCode = '';
                } else if ($scope.filters.wbsCode && $scope.filters.wbsCode.length > 0) {
                    var wbsCodes = _($scope.filters.wbsCode)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    wbsCode = wbsCodes.join(',');
                }

                if (_.isEmpty($scope.filters.profitCentre)) {
                    profitCentre = '';
                } else if ($scope.filters.profitCentre && $scope.filters.profitCentre.length > 0) {
                    var profitCentres = _($scope.filters.profitCentre)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    profitCentre = profitCentres.join(',');
                }

                var params = {
                    "PRODUCT_ID": prItem.PRODUCT_ID,
                    "plant": plant,
                    "projectType": projectType,
                    "sectionHead": sectionHead,
                    "wbsCode": wbsCode,
                    "profitCentre": profitCentre,
                    "search": $scope.filters.searchKeyword ? $scope.filters.searchKeyword : ''
                };

                if (!prItem.ItemPRS) {
                    PRMPRServices.GetPRSbyItem(params)
                        .then(function (response) {
                            $scope.ItemPRS = response;
                            $scope.ItemPRSPopUp = response;
                            $scope.ItemPRSPopUpTemp = response;
                            $scope.ItemPRSTemp = response;
                            //prItem.FILTERED_PR_COUNT = $scope.ItemPRS.length;
                            var prids = _.uniqBy($scope.ItemPRS, 'PR_NUMBER');
                            prItem.FILTERED_PR_COUNT = prids.length;
                            /**** Disabling For All Items ****/
                            $scope.itemsList.forEach(function (item, index) {
                                item.IsDisabled = false;
                            });
                            /**** Disabling For All Items ****/

                            $scope.ItemPRS.forEach(function (item, index) {
                                //item.CREATED_DATE = moment(item.CREATED_DATE).format("YYYY-MM-DD");

                                var releaseDateTemp = item.RELEASE_DATE ? moment(item.RELEASE_DATE).format("DD-MM-YYYY") : '-';
                                item.RELEASE_DATE = releaseDateTemp != '-' ? releaseDateTemp.contains("1970") ? '-' : releaseDateTemp : '-';
                                prItem.isExpanded1 = true;
                                item.isChecked = prItem.isExpanded1;
                            });

                            $scope.ItemPRSPopUp = $scope.ItemPRS;
                            $scope.ItemPRSPopUpTemp = $scope.ItemPRS;
                            $scope.ItemPRSTemp = $scope.ItemPRS;
                            $scope.totalItems2 = $scope.ItemPRSPopUp.length;

                            calculateQuantity($scope.ItemPRS, prItem);
                            $scope.ItemPRSTemp.forEach(function (item, index) {
                                //item.CREATED_DATE = userService.toLocalDate(item.CREATED_DATE);
                                prItem.isExpanded1 = true;
                                item.isChecked = prItem.isExpanded1;
                            });
                            prItem.ItemPRS = $scope.ItemPRS;

                            prItem.ItemPRSTemp = prItem.ItemPRS;

                            /********** RFQ Posting With Selected Items ***********/
                            if (prItem.selectForRFQ) {
                                $scope.AddItemToRequirement(prItem, prItem.selectForRFQ);
                            }
                            /********** RFQ Posting With Selected Items ***********/

                            if (type && type === 'ITEM_DISPLAY') {
                                $scope.displayItems(productid);
                            }


                        });
                } else {
                    /**** Disabling For All Items ****/
                    //calculateQuantity($scope.ItemPRS, prItem);
                    $scope.itemsList.forEach(function (item, index) {
                        item.IsDisabled = false;
                    });
                    /**** Disabling For All Items ****/


                    /********** RFQ Posting With Selected Items ***********/
                    if (prItem.selectForRFQ) {
                        $scope.AddItemToRequirement(prItem, prItem.selectForRFQ);
                    }
                    /********** RFQ Posting With Selected Items ***********/

                    if (type && type === 'ITEM_DISPLAY') {
                        $scope.displayItems(productid);
                    }
                }
            };

            $scope.searchPR = function (searchText, prItem) {
                var filterText = angular.lowercase(searchText);
                if (!filterText || filterText == '' || filterText == undefined || filterText == null) {
                    prItem.ItemPRS = prItem.ItemPRSTemp;
                    $scope.consolidate(prItem, filterText);
                }
                else {
                    prItem.ItemPRS = prItem.ItemPRSTemp.filter(function (pr) {
                        return (String(angular.lowercase(pr.PR_NUMBER)).includes(filterText) == true);
                    });
                }

                $scope.totalItems1 = prItem.ItemPRS.length;
            };


            $scope.unSelectAll = function (prItem, searchedPR) {

                if (!prItem.isExpanded1) {
                    prItem.ItemPRS.forEach(function (item, index) {
                        if (item.REQ_ID <= 0) {
                            item.isChecked = false;
                        }
                    });
                    $scope.consolidate(prItem, searchedPR, prItem.isExpanded1, 'OVERALL');
                } else {
                    prItem.ItemPRS.forEach(function (item, index) {
                        item.isChecked = true;
                    });
                    $scope.consolidate(prItem, searchedPR, prItem.isExpanded1, 'OVERALL');
                }

            };

            $scope.consolidate = function (pr, searchedPR, isChecked, type) {
                if (!searchedPR) {
                    if (pr && pr.ItemPRS.length > 0) {
                        var consolidateQuantity = pr.ItemPRS.filter(function (items) { return items.isChecked && items.REQ_ID <= 0; });
                        var getRFQQuantity = pr.ItemPRS.filter(function (items) { return items.isChecked && items.REQ_ID > 0; });
                        if (consolidateQuantity && consolidateQuantity.length > 0) {
                            pr.SELECTED_QUANTITY = _.sumBy(consolidateQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                            pr.RFQ_QUANTITY = _.sumBy(getRFQQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                        } else {
                            pr.SELECTED_QUANTITY = 0;
                        }
                    }
                }
                else {
                    searchedPR = angular.lowercase(searchedPR);
                    var searchedFilteredPRS = [];
                    if (pr.isExpanded1) {
                        if (type === 'OVERALL') {
                            searchedFilteredPRS = $scope.ItemPRSTemp;
                            if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                calculateQuantity(searchedFilteredPRS, pr);
                            }
                        } else {
                            if (!isChecked) {
                                var unCheckedPRID = _.result(_.find(pr.ItemPRS, function (items) {
                                    return !items.isChecked;
                                }), 'PR_ID'); // get Unchecked PR IR's

                                searchedFilteredPRS = $scope.ItemPRSTemp.filter(function (item) { return item.PR_ID != unCheckedPRID; }); // remove the unchecked PR and calculate

                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }

                            } else {
                                searchedFilteredPRS = $scope.ItemPRSTemp; // Assign All The PR's from DB
                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }
                            }
                        }
                    } else {
                        if (type === 'OVERALL') {

                            var prOverallIds = _(pr.ItemPRS)
                                .filter(item => !item.isChecked)
                                .map('PR_ID')
                                .value();

                            searchedFilteredPRS = $scope.ItemPRSTemp.filter(function (item) { return !prOverallIds.includes(item.PR_ID); }); // remove the unchecked PR and calculate

                            if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                calculateQuantity(searchedFilteredPRS, pr);
                            }

                        } else {
                            if (!isChecked) {
                                var itemUnCheckedPRID = _.result(_.find(pr.ItemPRS, function (items) {
                                    return !items.isChecked;
                                }), 'PR_ID'); // get Unchecked PR IR's

                                searchedFilteredPRS = $scope.ItemPRSTemp.filter(function (item) { return item.PR_ID != itemUnCheckedPRID; }); // remove the unchecked PR and calculate

                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }

                            } else {
                                searchedFilteredPRS = $scope.ItemPRSTemp; // Assign All The PR's from DB
                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }
                            }
                        }
                    }

                }
            };


            function calculateQuantity(searchedFilteredPRS, pr) {
                var searchConsolidatedQuantity = searchedFilteredPRS.filter(function (items) { return items.isChecked && items.REQ_ID <= 0; }); // calculate all the checked PR's Quantity
                var getRFQQuantity = searchedFilteredPRS.filter(function (items) { return items.isChecked && items.REQ_ID > 0; });
                if (searchConsolidatedQuantity && searchConsolidatedQuantity.length > 0) {
                    pr.SELECTED_QUANTITY = _.sumBy(searchConsolidatedQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                    pr.RFQ_QUANTITY = _.sumBy(getRFQQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                } else if (getRFQQuantity && getRFQQuantity.length > 0) {
                    pr.RFQ_QUANTITY = _.sumBy(getRFQQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                } else {
                    pr.SELECTED_QUANTITY = 0;
                }
            }

            function addCategoryToFilters(catArray) {
                var categoryListresp = catArray;
                categoryListresp = _.uniqBy(categoryListresp, 'CATEGORY_ID');
                categoryListresp.forEach(function (item, index) {
                    var catObj = {
                        FIELD_NAME: '',
                        FIELD_VALUE: ''
                    };
                    catObj.FIELD_NAME = item.CategoryName;
                    catObj.FIELD_VALUE = item.CATEGORY_ID;
                    $scope.categoryList.push(catObj);
                });

                $scope.categoryList = _.uniqBy($scope.categoryList, 'FIELD_VALUE');

            }


            $scope.PostRequirement = function (prItem) {
                $scope.itemsList.forEach(function (item, index) {
                    item.IsDisabled = true;
                });
                if (prItem.selectForRFQ) {
                    $scope.GetPRSbyItem(prItem);
                } else {
                    $scope.itemsList.forEach(function (item, index) {
                        item.IsDisabled = false;
                    });

                    $scope.AddItemToRequirement(prItem, prItem.selectForRFQ);
                }
            };

            $scope.selectedItemsForRFQ = [];
            $scope.AddItemToRequirement = function (prItem, type) {

                if (type) {
                    var checkedPRIDs = _(prItem.ItemPRS)
                        .filter(item => item.isChecked)
                        .map('PR_ID')
                        .value();

                    prItem.PR_ID = checkedPRIDs.join(',');
                    $scope.selectedItemsForRFQ.push(prItem);
                } else {
                    if ($scope.selectedItemsForRFQ && $scope.selectedItemsForRFQ.length > 0) {
                        var itemIndex = _.findIndex($scope.selectedItemsForRFQ, function (item) {
                            return item.PRODUCT_ID === prItem.PRODUCT_ID;
                        });

                        if (itemIndex >= 0) {
                            $scope.selectedItemsForRFQ.splice(itemIndex, 1);
                        }
                    }
                }

                $scope.selectedItemsForRFQ = _.uniqBy($scope.selectedItemsForRFQ, 'PRODUCT_ID');

            };

            /********  CONSOLIDATE PR ********/

            $scope.navigateToRequirement = function () {
                angular.element('#templateSelection').modal('hide');
                $scope.GetPRMTemplateFields();
            };

            $scope.navigateToRateContract = function () {
                angular.element('#templateSelection').modal('hide');
                let validSelectedPRs = _.filter($scope.selectedPRItems, function (prItemObj) {
                    return prItemObj.isSelected;
                });

                $state.go('pr-punchout', { 'prDetailsList': validSelectedPRs, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
            };

            $scope.GetPRMTemplates = function () {
                PRMCustomFieldService.GetTemplates().then(function (response) {
                    $scope.prmTemplates = response;
                    if ($scope.prmTemplates && $scope.prmTemplates.length > 0) {
                        $scope.selectedTemplate = $scope.prmTemplates[0];
                    }
                });
            };

            $scope.GetPRMTemplates();


            $scope.GetPRMTemplateFields = function () {
                let prmFieldMappingDetails = {};
                let isServiceRelatedITems = false;
                let isEmptyItemCode = false;
                var template = $scope.selectedTemplate && $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : 'PRM-DEFAULT';
                var params = {
                    "templateid": 0,
                    "templatename": template,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });

                    if (prmFieldMappingDetails) {
                        prmFieldMappingDetails.IS_SEARCH_FOR_CAS = prmFieldMappingDetails.PRODUCT_CODE.FIELD_LABEL.toLowerCase().contains("cas number");
                        prmFieldMappingDetails.IS_SEARCH_FOR_MFCD = prmFieldMappingDetails.PRODUCT_NUMBER.FIELD_LABEL.toLowerCase().contains("mfcd code");
                        if (!isServiceRelatedITems && prmFieldMappingDetails.IS_SEARCH_FOR_CAS) {
                            isServiceRelatedITems = true;
                        }
                    }

                    let validSelectedPRs = _.filter($scope.selectedPRItems, function (prItemObj) {
                        return prItemObj.isSelected;
                    });

                    validSelectedPRs.forEach(function (prObj, index) {
                        if (prmFieldMappingDetails.IS_SEARCH_FOR_CAS && (prObj.ITEM_CODE_CAS || prObj.CASNR)) {
                            prObj.ITEM_CODE = prObj.ITEM_CODE_CAS || prObj.CASNR;
                        }

                        if (prmFieldMappingDetails.IS_SEARCH_FOR_MFCD && (prObj.ITEM_CODE_MFCD || prObj.MFCD_NUMBER)) {
                            prObj.ITEM_NUM = prObj.ITEM_CODE_MFCD || prObj.MFCD_NUMBER;
                        }

                        if (!isEmptyItemCode && !(prObj.ITEM_CODE_CAS || prObj.CASNR) && !prObj.REQ_ID && prObj.isCheckedPrItem) {
                            isEmptyItemCode = true;
                        }
                    });

                    if (isServiceRelatedITems && isEmptyItemCode && false) {
                        swal("Error!", 'Selected template is not Valid as empty CAS number');
                    }
                    else if (validSelectedPRs && validSelectedPRs.length > 0) {
                        if ($scope.filters.selectedRequirement && $scope.filters.selectedRequirement.requirementId) {
                            $state.go('save-requirementAdv', { 'Id': $scope.filters.selectedRequirement.requirementId, 'prDetailsListV2': validSelectedPRs, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        } else {
                            $state.go('save-requirementAdv', { 'prDetailsListV2': validSelectedPRs, 'selectedTemplate': $scope.selectedTemplate, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        }
                    }
                });
            };


            $scope.convertDate = function (date) {
                if (date) {
                    var convertedDateTemp = moment(date).format("DD-MM-YYYY");
                    return convertedDateTemp.contains("1970") ? '-' : convertedDateTemp;
                } else {
                    return '-';
                }
            };

            $scope.ItemsListPopup = [];
            $scope.displayItems = function (productId) {
                var PR_IDS = '';
                var pridspop = _($scope.ItemPRSPopUp)
                    .filter(item => item.PR_ID)
                    .map('PR_ID')
                    .value();
                PR_IDS = pridspop.join(',');

                var params = {
                    "prIds": PR_IDS
                };

                PRMPRServices.GetItemDetails(params)
                    .then(function (response) {
                        $scope.ItemsListPopup = response;

                        $scope.ItemsListPopup = $scope.ItemsListPopup.filter(function (item) {
                            return item.PRODUCT_ID === productId;
                        });

                    });
            };

            $scope.filterPRsPopup = function (searchText) {

                var filterText = angular.lowercase(searchText);

                if (filterText) {
                    $scope.ItemPRSPopUp = $scope.ItemPRSPopUpTemp.filter(function (pr) {
                        return (String(angular.lowercase(pr.PR_NUMBER)).includes(filterText) == true);
                    });
                } else {
                    $scope.ItemPRSPopUp = $scope.ItemPRSPopUpTemp;
                }

                $scope.totalItems2 = $scope.ItemPRSPopUp.length;
            };

            $scope.selectMultiplePRS = [];

            $scope.createRFQWithMultiplePRV2 = function (prDet) {
                $scope.filteredPRList.forEach(function (PrObj, prIndex) {
                    if (prDet.PR_NUMBER == PrObj.PR_NUMBER) {
                        if (prDet.selectPRSForRFQ && +PrObj.REQ_ID <= 0 && !PrObj.CONTRACT_NUMBER) {
                            PrObj.isCheckedPrItem = true;
                            PrObj.selectPRSForRFQ = true;
                            PrObj.isSelected = true;
                            $scope.selectedPRItems.push(PrObj);
                        } else {
                            PrObj.isCheckedPrItem = false;
                            PrObj.selectPRSForRFQ = false;
                            PrObj.isSelected = false;
                            let index = _.findIndex($scope.selectedPRItems, function (pr) { return !pr.isSelected; });
                            if (index >= 0) {
                                $scope.selectedPRItems.splice(index, 1);
                            }
                        }
                    }
                });
            };

            $scope.makeCheckedPR = function () {
                if ($scope.selectedPRItems && $scope.selectedPRItems.length > 0) {
                    $scope.selectedPRItems.forEach(function (item) {
                        $scope.filteredPRList.forEach(function (PrObj) {
                            if (item.ITEM_ID == PrObj.ITEM_ID) {
                                PrObj.isCheckedPrItem = true;
                                PrObj.selectPRSForRFQ = true;
                            }
                        });
                    })
                }
            };

            $scope.handleItemLevelSelectV2 = function (prDet) {
                if (prDet.isCheckedPrItem) {
                    prDet.selectPRSForRFQ = true;
                    prDet.isSelected = true;
                } else {
                    prDet.selectPRSForRFQ = false;
                    prDet.isSelected = false;
                    let index = _.findIndex($scope.selectedPRItems, function (pr) { return !pr.isSelected; });
                    if (index >= 0) {
                        $scope.selectedPRItems.splice(index, 1);
                    }
                }

                $scope.filteredPRList.forEach(function (PrObj, prIndex) {
                    if (PrObj.selectPRSForRFQ) {
                        PrObj.isSelected = true;
                        $scope.selectedPRItems.push(PrObj);
                    }
                });
            };

            $scope.GetReport = function () {
                $scope.PRList = [];
                $scope.getprlist(0, 0, $scope.searchString);
            };

            $scope.searchRequirement = function () {
                $scope.filteredRequirements = [];
                auctionsService.SearchRequirements({ "search": $scope.filters.searchRequirement, "compid": $scope.compId, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        if (response) {
                            $scope.filteredRequirements = response;
                        }
                    });
            };

            $scope.selectRequirement = function () {
                //console.log($scope.filters.selectedRequirement);
            };

            $scope.getProductContracts = function () {
                let productArray = [];
                $scope.contractPODetails = {
                    contractTable: [],
                    plantsArray: []
                }; //Keep local

                let validSelectedPRs = _.filter($scope.selectedPRItems, function (prItemObj) {
                    return prItemObj.isSelected;
                });
                validSelectedPRs = _.uniqBy(validSelectedPRs, 'ITEM_ID');

                validSelectedPRs.forEach(function (prObj, index) {
                    if (prObj.HAS_CONTRACT > 0) {
                        if (!prObj.REQ_ID && prObj.REQ_ID <= 0) {
                            productArray.push(prObj.PRODUCT_ID);
                            let contactTableExistngItem = _.filter($scope.contractPODetails.contractTable, function (tableItem) {
                                return tableItem.productId === prObj.PRODUCT_ID;
                            });

                            if (!_.some($scope.contractPODetails.plantsArray, { plantCode: prObj.PLANT })) {
                                $scope.contractPODetails.plantsArray.push({
                                    plantCode: prObj.PLANT,
                                    plantName: prObj.PLANT_NAME
                                });
                            }
                            if (contactTableExistngItem && contactTableExistngItem.length > 0) {
                                contactTableExistngItem[0].plantName = contactTableExistngItem[0].plantName + ',' + prObj.PLANT_NAME;
                                contactTableExistngItem[0].plantCode = contactTableExistngItem[0].plantCode + ',' + prObj.PLANT;
                                contactTableExistngItem[0].prNumber = contactTableExistngItem[0].prNumber + ',' + prObj.PR_NUMBER;
                                contactTableExistngItem[0].prQuantity = contactTableExistngItem[0].prQuantity + prObj.REQUIRED_QUANTITY;
                                contactTableExistngItem[0].prLineItem = contactTableExistngItem[0].prLineItem + ',' + (prObj.PR_NUMBER + '$^^$' + prObj.ITEM_NUM + '$^^$' + prObj.REQUIRED_QUANTITY + '$^^$' + prObj.MODIFIED_DATE + '$^^$' + prObj.ITEM_ID + '$^^$' + prObj.PRODUCT_ID);
                                contactTableExistngItem[0].prItemID = contactTableExistngItem[0].prItemID + ',' + prObj.ITEM_ID;
                                contactTableExistngItem[0].CREATED_DATE = contactTableExistngItem[0].CREATED_DATE;
                                //contactTableExistngItem[0].MODIFIED_DATE = contactTableExistngItem[0].MODIFIED_DATE;
                            } else {
                                $scope.contractPODetails.contractTable.push({
                                    productName: prObj.ITEM_NAME,
                                    productId: prObj.PRODUCT_ID,
                                    plantName: prObj.PLANT_NAME,
                                    plantCode: prObj.PLANT,
                                    plantCodeArr: [],
                                    prNumber: prObj.PR_NUMBER,
                                    prQuantity: prObj.REQUIRED_QUANTITY,
                                    prItemID: prObj.ITEM_ID,
                                    prLineItem: prObj.PR_NUMBER + '$^^$' + prObj.ITEM_NUM + '$^^$' + prObj.REQUIRED_QUANTITY + '$^^$' + prObj.MODIFIED_DATE + '$^^$' + prObj.ITEM_ID + '$^^$' + prObj.PRODUCT_ID,
                                    CREATED_DATE: prObj.CREATED_DATE,
                                    //MODIFIED_DATE: prObj.MODIFIED_DATE
                                });
                            }
                        }
                    }
                });
                $scope.contractPODetails.contractTable.forEach(function (item, index) {
                    if (item.prNumber) {
                        uniqPRNumber = _.uniq(item.prNumber.split(','));
                        item.prNumber = uniqPRNumber.join(',');
                    }
                    if (item.plantCode) {
                        uniqPlant = _.uniq(item.plantCode.split(','));
                        item.plantCode = uniqPlant.join(',');
                    }
                })

                if (productArray && productArray.length > 0) {
                    let productIds = productArray.join();
                    if (productIds) {
                        catalogService.GetProductContracts(productIds)
                            .then(function (response) {
                                if (response) {
                                    $scope.ProductContractsLoaded = true;
                                    console.log(response);
                                    $scope.ProductContracts = _.filter(response, function (contractitem) {
                                        return contractitem.contractStatus === 'Active';
                                    });
                                    if ($scope.ProductContracts && $scope.ProductContracts.length > 0) {
                                        $scope.ProductContracts.forEach(function (contract) {
                                            contract.startTime = userService.toLocalDate(contract.startTime);
                                            contract.endTime = userService.toLocalDate(contract.endTime);
                                            
                                        });

                                        if ($scope.contractPODetails && $scope.contractPODetails.contractTable && $scope.contractPODetails.contractTable.length > 0) {
                                            $scope.contractPODetails.contractTable.forEach(function (contract) {
                                                contract.contactsArrayTemp = [];

                                                contract.contactsArray = _.filter($scope.ProductContracts, function (contractitem) {
                                                    if (contract.plantCode != 912) {
                                                        return contractitem.ProductId === contract.productId && contract.plantCode == (contractitem.SITE_CODE ? contractitem.SITE_CODE.split('_')[0] :  contractitem.VENDOR_SITE_CODE.split('_')[0]);
                                                    } else {
                                                        return contractitem.ProductId === contract.productId
                                                    }
                                                });

                                                contract.contactsArrayTemp = _.filter($scope.ProductContracts, function (contractitem) {
                                                    return contractitem.ProductId === contract.productId;
                                                });

                                                if (contract.contactsArray && contract.contactsArray.length > 0) {
                                                    contract.contactsArray.forEach(function (conItem, conIndex) {
                                                        conItem.isValid = false;
                                                        $scope.assignEditQuantity(conItem, contract);
                                                    });
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                    }
                }
            };

            $scope.chooseContract = function () {
                $scope.chooseContractOption = false;
                if ($scope.contractPODetails.contractTable && $scope.contractPODetails.contractTable.length > 0) {
                    $scope.contractPODetails.contractTable.forEach(function (contactItem) {
                        if (contactItem && contactItem.contactsArray && contactItem.contactsArray.length > 0) {
                            contactItem.contactsArray.forEach(function (contract) {
                                if (contract.isSelected) {
                                    $scope.chooseContractOption = true;
                                }
                            });
                        }
                    });
                }
            };

            $scope.navigateToPOForm = function () {
                if ($scope.contractPODetails && $scope.contractPODetails.contractTable && $scope.contractPODetails.contractTable.length > 0) {
                    angular.element('#templateSelection').modal('hide');
                    let selectedTemplate = '';
                    if ($scope.stateDetails.poTemplate === 'po-contract-domestic-zsdm') {
                        selectedTemplate = 'ZSDM';
                    } else if ($scope.stateDetails.poTemplate === 'po-contract-import-zsim') {
                        selectedTemplate = 'ZSIM';
                    } else if ($scope.stateDetails.poTemplate === 'po-contract-bonded-wh') {
                        selectedTemplate = 'ZSBW';
                    } else if ($scope.stateDetails.poTemplate === 'po-contract-service-zssr') {
                        selectedTemplate = 'ZSSR';
                    }

                    let prDetails = [];
                    let isGMP = false;
                    if ($scope.prDet.prLevel) {
                        prDetails = _.filter($scope.selectedPRItems, function (prItemObj) {
                            return prItemObj.isSelected;
                        });

                        let temp = _.filter(prDetails, function (prItemObj) {
                            return prItemObj.GMP === 'GMP';
                        });

                        if (temp && temp.length > 0) {
                            isGMP = true;
                        }

                        prDetails[0].isGMP = isGMP;
                    } else {
                        var rfqPRSList = $scope.selectedItemsForRFQ.filter(function (item) {
                            return item.selectForRFQ && item.RFQ_QUANTITY !== item.OVERALL_ITEM_QUANTITY;
                        });

                        rfqPRSList.forEach(function (prItem, index) {
                            if (!isGMP && prItem && prItem.ItemPRS && prItem.ItemPRS.length > 0) {
                                prItem.ItemPRS.forEach(function (prObj, index) {
                                    if (!isGMP && prObj && prObj.GMP === 'GMP') {
                                        isGMP = true;
                                    }
                                });
                            }
                        });

                        prDetails.push({
                            PRItems: rfqPRSList
                        });

                        prDetails[0].isGMP = isGMP;
                    }

                    let vendorAssignments = [];
                    $scope.contractPODetails.contractTable.forEach(function (contactItem, index) {
                        if (contactItem) {
                            contactItem.contactsArray.forEach(function (item, index) {
                                if (item.isSelected) {
                                    vendorAssignments.push({
                                        vendorID: item.vendorId,
                                        vendorCode: item.selectedVendorCode,
                                        vendorName: item.companyName,
                                        itemID: item.ProductId,
                                        item: item,
                                        assignedQty: contactItem.prQuantity,
                                        assignedPrice: item.netPrice, //item.price,
                                        totalPrice: item.netPrice, // item.price, //contactItem.prQuantity * item.price,
                                        currency: '',
                                        contractNumber: item.number
                                    });
                                }
                            });
                        }
                    });

                    $state.go(
                        $scope.stateDetails.poTemplate,
                        { 'contractDetails': $scope.contractPODetails, 'prDetails': prDetails, 'detailsObj': vendorAssignments, 'templateName': selectedTemplate, 'quoteLink': '' }
                    );
                }
            };

            $scope.getSAPPRHistory = function () {
                var params = {
                    "prnumber": $scope.sapPRSearch
                };

                if ($scope.sapPRSearch) {
                    PRMPRServices.GetSAPPRHistory(params)
                        .then(function (response) {
                            $scope.sapPRHistoryList = response;
                            if ($scope.sapPRHistoryList && $scope.sapPRHistoryList.length > 0) {
                                $scope.sapPRHistoryList.forEach(function (item, index) {
                                    item.CREATED_DATE = item.CREATED_DATE ? userService.toLocalDate(item.CREATED_DATE).split(' ')[0] : '-';
                                    item.RELEASE_DATE = item.RELEASE_DATE ? userService.toLocalDate(item.RELEASE_DATE).split(' ')[0] : '-';
                                });
                            }
                            if ($scope.sapPRHistoryList.length > 0 && $scope.filteredPRList.length == 0) {
                                var a = _.find($scope.sapPRHistoryList, ['LOEKZ', 'X']);
                                if (a) {
                                    $scope.sapDeleteIndictaorErrorShow = true;
                                }
                                var b = _.find($scope.sapPRHistoryList, ['EBAKZ', 'X' || null]);
                                if (b) {
                                    $scope.sapClosedIndicatorErrorShow = true;
                                }
                                var c = _.find($scope.sapPRHistoryList, ['RELEASE_DATE', '01-01-1970']);
                                if (c) {
                                    $scope.sapReleasedDateErrorShow = true;
                                }
                            }
                        });
                }
            };

            $scope.getProductRelatedRequirements = function (prDetails) {
                let productArray = [];               
                let validSelectedPRs = _.filter($scope.selectedPRItems, function (prItemObj) {
                    return prItemObj.isSelected;
                });

                validSelectedPRs.forEach(function (prObj, index) {
                    productArray.push(prObj.PRODUCT_ID);
                });

                if (prDetails && prDetails.PRODUCT_ID && (!productArray || productArray.length <= 0)) {
                    productArray.push(prDetails.PRODUCT_ID);
                }

                if (productArray && productArray.length > 0) {
                    let productIds = productArray.join();
                    if (productIds) {
                        auctionsService.GetRequirementsByProductIds({ "productids": productIds, "compid": $scope.compId, "sessionid": userService.getUserToken() })
                            .then(function (response) {
                                if (response) {
                                    $scope.ProductContractsLoaded = true;
                                    $scope.ProductRequirements = response;
                                    if ($scope.ProductRequirements && $scope.ProductRequirements.length > 0) {
                                        $scope.ProductRequirements[0].isSelected = true;
                                        $scope.ProductRequirements.forEach(function (item, index) {
                                            item.endTime = item.endTime ? userService.toLocalDate(item.endTime).split(' ')[0] : '-';
                                            item.isSelected = false;
                                        });
                                    }
                                }
                            });
                    }
                }
            };

            $scope.isRequirementSelected = function () {
                let doDisable = true;
                var selectedRequirement = $scope.ProductRequirements.filter(function (req) {
                    return req.isSelected;
                });

                if (selectedRequirement && selectedRequirement.length > 0) {
                    doDisable = false;
                }

                return doDisable;
            };

            $scope.navigateToQCSForm = function () {
                let contractsToQCSList = [];
                var selectedRequirement = $scope.ProductRequirements.filter(function (req) {
                    return req.isSelected;
                });

                let validSelectedPRs = _.filter($scope.selectedPRItems, function (prItemObj) {
                    return prItemObj.isSelected;
                });

                let contactPlantInfo = { plantList: [], plantNameList: [] };

                validSelectedPRs.forEach(function (prObj, index) {
                    if (prObj.PLANT && !contactPlantInfo.plantList.includes(prObj.PLANT)) {
                        contactPlantInfo.plantList.push(prObj.PLANT);
                    }

                    if (prObj.PLANT_NAME && !contactPlantInfo.plantNameList.includes(prObj.PLANT_NAME)) {
                        contactPlantInfo.plantNameList.push(prObj.PLANT_NAME);
                    }

                    if (prObj.HAS_CONTRACT <= 0 && prObj.REQ_ID <= 0 && prObj.isSelected) {

                        let currentItem = _.filter($scope.contractsToQCSList, function (item1) {
                            return item1.PR_ID = prObj.PR_ID;
                        });

                        if (currentItem && currentItem.length > 0) {
                            currentItem[0].PR_QUANTITY = (+currentItem[0].PR_QUANTITY) + prObj.REQUIRED_QUANTITY;
                            currentItem[0].CONSOLIDATED_PRS = currentItem[0].CONSOLIDATED_PRS + ',' + prObj.PR_NUMBER;
                            currentItem[0].CONSOLIDATED_ITEM_IDS = currentItem[0].CONSOLIDATED_ITEM_IDS + ',' + prObj.ITEM_ID;
                        } else {
                            let contractsToQCS = {
                                REQ_ID: selectedRequirement[0].requirementId,
                                PRODUCT_ID: prObj.PRODUCT_ID,
                                PR_QUANTITY: prObj.REQUIRED_QUANTITY,
                                CONSOLIDATED_PRS: '' + prObj.PR_NUMBER,
                                PR_NUMBER: prObj.PR_NUMBER,
                                PR_ID: prObj.PR_ID,
                                ITEM_ID: prObj.ITEM_ID,
                                CONSOLIDATED_ITEM_IDS: '' + prObj.ITEM_ID
                            };

                            contractsToQCSList.push(contractsToQCS);
                        }
                    }
                });

                if (contractsToQCSList && contractsToQCSList.length > 0) {
                    if (selectedRequirement[0].QCS_TYPE && selectedRequirement[0].QCS_TYPE === 'DOMESTIC') {
                        $state.go(
                            'cost-comparisions-qcs', { 'contractsToQCSList': contractsToQCSList, "contractsToQCSPlantInfo": contactPlantInfo, "reqID": selectedRequirement[0].requirementId, "qcsID": 0 }
                        );
                    } else {
                        $state.go(
                            'import-qcs', { 'contractsToQCSList': contractsToQCSList, "contractsToQCSPlantInfo": contactPlantInfo, "reqID": selectedRequirement[0].requirementId, "qcsID": 0 }
                        );
                    }
                }
            };

            $scope.getItemLevelPrice = function (item) {
                if (!$scope.itemLastPriceArr) {
                    $scope.itemLastPriceArr = [];
                }
                item.productIDorName = item.ITEM_NAME;
                item.compID = $scope.compId;
                item.sessionID = $scope.sessionID;
                item.productNo = item.ITEM_CODE;
                item.productBrand = '';
                $scope.itemPreviousPrice = {};
                $scope.itemPreviousPrice.lastPrice = -1;
                auctionsService.getPreviousItemPrice(item)
                    .then(function (response) {
                        if (response && response.errorMessage === '') {
                            $scope.lastPrice = Number(response.initialPrice);
                            $scope.lastPriceDate = userService.toLocalDate(response.currentTime);
                            $scope.lastPriceVendor = response.companyName;

                            if ($scope.lastPrice) {
                                $scope.itemLastPriceArr = _.filter($scope.itemLastPriceArr, function (item1) {
                                    return !item1.isBestPrice;
                                });

                                $scope.itemLastPriceArr.unshift({
                                    isBestPrice: true,
                                    requirementID: '',
                                    unitPrice: $scope.lastPrice,
                                    quantity: '',
                                    companyName: $scope.lastPriceVendor,
                                    currentTime: $scope.lastPriceDate
                                });
                            }
                        }
                    });

                if (!$scope.itemLastPriceArr) {
                    $scope.itemLastPriceArr = [];
                }
                let bestPriceArray = _.filter($scope.itemLastPriceArr, function (item) {
                    return item.isBestPrice;
                });
                $scope.itemLastPriceArr = [];

                auctionsService.GetLastPrice(item)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.itemLastPriceArr = response;
                            $scope.itemLastPriceArr.forEach(function (item, index) {
                                item.currentTime = userService.toLocalDate(item.currentTime);
                                item.isBestPrice = false;
                            });
                        }
                        if (bestPriceArray && bestPriceArray.length > 0) {
                            $scope.itemLastPriceArr.unshift(bestPriceArray[0]);
                        }
                    });
            };

            $scope.handlePRTypeFilter = function (val) {
                $scope.currentPage = 1;
                $scope.totalItems = 0;
                if ($scope.filters.prType_old && $scope.filters.prType_old === $scope.filters.prType) {
                    $scope.filters.prType = null;
                    $scope.filters.prType_old = null;
                } else {
                    $scope.filters.prType_old = $scope.filters.prType
                }

                $scope.getPRListV2(false, false);
            };

            $scope.getPRListV2 = function (isExport, getFilters) {
                let plant, projectType, sectionHead, wbsCode, profitCentre, purchaseCode, creatorName, clientName, prStatus, fromDate, toDate;
                if (_.isEmpty($scope.filters.prFromDate)) {
                    fromDate = '';
                } else {
                    fromDate = $scope.filters.prFromDate;
                }

                if (_.isEmpty($scope.filters.prToDate)) {
                    toDate = '';
                } else {
                    toDate = $scope.filters.prToDate;
                }

                if (_.isEmpty($scope.filters.plant)) {
                    plant = '';
                } else if ($scope.filters.plant && $scope.filters.plant.length > 0) {
                    var plants = _($scope.filters.plant)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    plant = plants.join(',');
                }

                if (_.isEmpty($scope.filters.projectType)) {
                    projectType = '';
                } else if ($scope.filters.projectType && $scope.filters.projectType.length > 0) {
                    var projectTypes = _($scope.filters.projectType)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    projectType = projectTypes.join(',');
                }

                if (_.isEmpty($scope.filters.sectionHead)) {
                    sectionHead = '';
                } else if ($scope.filters.sectionHead && $scope.filters.sectionHead.length > 0) {
                    var sectionHeads = _($scope.filters.sectionHead)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    sectionHead = sectionHeads.join(',');
                }

                if (_.isEmpty($scope.filters.wbsCode)) {
                    wbsCode = '';
                } else if ($scope.filters.wbsCode && $scope.filters.wbsCode.length > 0) {
                    var wbsCodes = _($scope.filters.wbsCode)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    wbsCode = wbsCodes.join(',');
                }

                if (_.isEmpty($scope.filters.profitCentre)) {
                    profitCentre = '';
                } else if ($scope.filters.profitCentre && $scope.filters.profitCentre.length > 0) {
                    var profitCentres = _($scope.filters.profitCentre)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    profitCentre = profitCentres.join(',');
                }

                if (_.isEmpty($scope.filters.purcahseGroup)) {
                    purchaseCode = '';
                } else if ($scope.filters.purcahseGroup && $scope.filters.purcahseGroup.length > 0) {
                    var purchaseCodes = _($scope.filters.purcahseGroup)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    purchaseCode = purchaseCodes.join(',');
                }

                if (_.isEmpty($scope.filters.requisitionerName)) {
                    creatorName = '';
                } else if ($scope.filters.requisitionerName && $scope.filters.requisitionerName.length > 0) {
                    var creators = _($scope.filters.requisitionerName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    creatorName = creators.join(',');
                }

                if (_.isEmpty($scope.filters.projectName)) {
                    clientName = '';
                } else if ($scope.filters.projectName && $scope.filters.projectName.length > 0) {
                    var clientNames = _($scope.filters.projectName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    clientName = clientNames.join(',');
                }

                if (_.isEmpty($scope.filters.newPrStatus)) {
                    prStatus = '';
                } else if ($scope.filters.newPrStatus && $scope.filters.newPrStatus.length > 0) {
                    var statusFilters = _($scope.filters.newPrStatus)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    prStatus = statusFilters.join(',');
                }

                console.log($scope.filters);
                var params = {
                    filters: {
                        "U_ID": $scope.userID,
                        "COMP_ID": $scope.compId,
                        "sessionID": $scope.sessionID,
                        "PLANT": plant,
                        "PROJECT_TYPE": projectType,
                        "SECTION_HEAD": sectionHead,
                        "WBS_CODE": wbsCode,
                        "PROFIT_CENTRE": profitCentre,
                        "PURCHASE_GROUP": purchaseCode,
                        "CREATOR_NAME": creatorName,
                        "CLIENT_NAME": clientName,
                        "PR_STATUS": prStatus,
                        "SEARCH": $scope.filters.searchKeyword ? $scope.filters.searchKeyword : '',
                        'FROM_DATE': fromDate,
                        'TO_DATE': toDate,
                        "PAGE_SIZE": $scope.itemsPerPage,
                        "PAGE": ($scope.currentPage - 1) * $scope.itemsPerPage,
                        "TYPE": $scope.filters.prType ? $scope.filters.prType : '',
                        "EXPORT_DATA": isExport ? true : false
                    }
                };

                if (getFilters) {
                    $scope.getFilterValuesV2(params);
                }

                PRMPRServices.getprlistV2(params)
                    .then(function (response) {
                        if (isExport) {
                            $scope.PRList = [];
                            if (response && response.length > 0) {
                                response.forEach(function (item, index) {
                                    item.DELIVERY_DATE = userService.toLocalDate(item.DELIVERY_DATE);
                                    item.REQUESITION_DATE = userService.toLocalDate(item.REQUESITION_DATE);
                                    item.MODIFIED_DATE = userService.toLocalDate(item.MODIFIED_DATE);
                                    item.PR_CHANGE_DATE = userService.toLocalDate(item.PR_CHANGE_DATE);
                                    item.RELEASE_DATE = userService.toLocalDate(item.RELEASE_DATE).split(' ')[0];
                                    
                                    $scope.PRList.push(item);
                                });

                                alasql('SELECT PR_NUMBER AS [PR NUMBER], ITEM_NUM AS [ITEM NUMBER], REQ_NUMBER AS [Req Number],CONTRACT_NUMBER AS [Contract Number], ITEM_CODE as [Material], ITEM_NAME as [Name],PLANT as [Plant], ITEM_NAME as [Name],' +
                                    'REQUIRED_QUANTITY as QTY, UOM as [Qty Units],' +
                                    'RELEASE_DATE as [Release Date]' +
                                    'INTO XLSX(?, { headers: true, sheetid: "PR_DETAILS", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                                    ["PR Details.xlsx", $scope.PRList]);
                            }
                        } else {
                            $scope.PRList = [];
                            $scope.filteredPRList = [];
                            if (response && response.length > 0) {
                                response.forEach(function (item, index) {
                                    item.selectPRSForRFQ = false;
                                    item.RELEASE_DATE = userService.toLocalDate(item.RELEASE_DATE).split(' ')[0];
                                    $scope.PRList.push(item);
                                    if ($scope.initialPRPageArray.length <= 9) {
                                        $scope.initialPRPageArray.push(item);
                                    }
                                });

                                $scope.filteredPRList = $scope.PRList;
                                $scope.makeCheckedPR();
                                $scope.totalItems = $scope.filteredPRList[0].TOTAL_PR_COUNT;
                                $scope.PRStats.totalPRs = $scope.totalItems;

                                $scope.PRStats.newPRs = $scope.filteredPRList[0].TOTAL_OPEN_PR;
                                $scope.PRStats.partialPRs = $scope.filteredPRList[0].TOTAL_PARTIAL_PR;
                                $scope.PRStats.totalPRItems = $scope.filteredPRList[0].TOTAL_PR_PRODUCTS;
                                $scope.PRStats.totalRFQPosted = $scope.filteredPRList[0].TOTAL_RFQS_POSTED;
                                $scope.PRStats.inProgressPRs = $scope.filteredPRList[0].TOTAL_IN_PROGRESS_PR;
                            }
                        }                       
                    });
            };

            $scope.getFilterValuesV2 = function (params) {
                let plantListTemp = [];
                let wbsCodeTemp = [];
                let projectTypeTemp = [];
                let projectNameTemp = [];
                let sectionHeadTemp = [];
                let purchaseGroupTemp = [];
                let statusTemp = [];
                let profitCentreTemp = [];
                let requisitionListTemp = [];

                PRMPRServices.getFilterValuesV2(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.filterValues = response;
                            if ($scope.filterValues && $scope.filterValues.length > 0) {
                                $scope.filterValues.forEach(function (item, index) {
                                    if (item.TYPE === 'WBS_CODE') {
                                        wbsCodeTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PROFIT_CENTER') {
                                        profitCentreTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PROJECT_TYPE') {
                                        projectTypeTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'SECTION_HEAD') {
                                        sectionHeadTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PROJECT_DESCRIPTION') {
                                        projectNameTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PLANTS') {
                                        plantListTemp.push({ id: +item.NAME, name: item.ID + ' - ' + item.NAME });
                                    } else if (item.TYPE === 'PURCHASE_GROUP') {
                                        purchaseGroupTemp.push({ id: +item.NAME, name: item.ID });
                                    } else if (item.TYPE === 'PR_CREATOR_NAME') {
                                        requisitionListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PR_STATUS') {
                                        statusTemp.push({ id: item.ID, name: item.NAME });
                                    }

                                });

                                $scope.filtersList.requisitionList = requisitionListTemp;
                                $scope.filtersList.plantList = plantListTemp;
                                $scope.filtersList.wbsCodeList = wbsCodeTemp;
                                $scope.filtersList.projectTypeList = projectTypeTemp;
                                $scope.filtersList.sectionHeadList = sectionHeadTemp;
                                $scope.filtersList.projectNameList = projectNameTemp;
                                $scope.filtersList.purchaseGroupList1 = purchaseGroupTemp;
                                $scope.filtersList.statusList = statusTemp;
                                $scope.filtersList.profitCentreList = profitCentreTemp;
                            }
                        }
                    });
            };

            $scope.navigateToProduct = function (prObj) {
                $state.go("productEdit", { "productId": prObj.PRODUCT_ID, "viewId": "edit" });
            };

            $scope.getPRListV2(false, true);


            $scope.linkContract = function () {
                $scope.linkContractArray = [];
                if ($scope.contractPODetails.contractTable && $scope.contractPODetails.contractTable.length > 0) {
                    $scope.contractPODetails.contractTable.forEach(function (contactItem) {
                        if (contactItem && contactItem.contactsArray && contactItem.contactsArray.length > 0) {
                            let selectedContractItem = _.filter(contactItem.contactsArray, function (contract) {
                                return contract.isSelected;
                            });
                            if (selectedContractItem && selectedContractItem.length > 0) {
                                var contractNumber;
                                var contractNumbers = _(selectedContractItem)
                                    .filter(item => item.contractNumber)
                                    .map('contractNumber')
                                    .value();
                                contractNumber = contractNumbers.join(',');

                                selectedContractItem.forEach(function (cItem, cIndex) {
                                    var obj = {
                                        PR_ITEM_ID: 0,
                                        ContractNumber: '',
                                        VENDOR_ID: 0,
                                        REQUIRED_QUANTITY: 0,
                                        REQ_ID : 0,
                                        listUtilisationDetails: [],
                                        QCS_ID: 0,
                                        PR_NUMBER : ''
                                    };
                                    obj.PR_NUMBER = contactItem.prNumber;
                                    obj.PR_ITEM_ID = contactItem.prItemID;
                                    obj.contractNumber = contractNumber;
                                    obj.PC_ID = cItem.PC_ID;
                                    obj.ProductId = cItem.ProductId;
                                    obj.ProductName = cItem.ProductName;
                                    obj.REQUIRED_QUANTITY = cItem.quantity;
                                    obj.REQ_ID = cItem.REQ_ID > 0 ? cItem.REQ_ID : 0;
                                    obj.QCS_ID = cItem.QCS_ID > 0 ? cItem.QCS_ID : 0;
                                    obj.PLANT_CODE = cItem.PLANT_CODE

                                    //var ids = _.uniq(obj.PR_ITEM_ID.split(','));
                                    //obj.PR_ITEM_ID = ids.join(',');
                                    
                                    var prs = _.uniq(contactItem.prLineItem.split(','))
                                    if (prs && prs.length > 0)
                                    {
                                        prs.forEach(function (prItem,prIndex) {
                                            var qtyAllocation =
                                            {
                                                PC_ID: cItem.PC_ID,
                                                COMP_ID: +$scope.compId,
                                                U_ID: +$scope.userID,
                                                VENDOR_ID: cItem.U_ID,
                                                REQUIRED_QUANTITY: 0,
                                                PR_ITEM_ID: 0,
                                                PRODUCT_ID: 0
                                            }
                                            qtyAllocation.PR_NUMBER = prItem.split('$^^$')[0];
                                            qtyAllocation.PR_LINE_ITEM = prItem.split('$^^$')[1];
                                            qtyAllocation.PO_NUMBER = null;
                                            qtyAllocation.PO_LINE_ITEM = null;
                                            qtyAllocation.QTY_UTILISED = +cItem.editQuantity;
                                            qtyAllocation.REQUIRED_QUANTITY = prItem.split('$^^$')[2];
                                            qtyAllocation.CREATED_DATE = contactItem.CREATED_DATE ? contactItem.CREATED_DATE : null;
                                            qtyAllocation.MODIFIED_DATE = prItem.split('$^^$')[3];
                                            qtyAllocation.PR_ITEM_ID = prItem.split('$^^$')[4];
                                            qtyAllocation.PRODUCT_ID = prItem.split('$^^$')[5];
                                              //$scope.precisionRound((+cItem.editQuantity / prs.length), $rootScope.companyRoundingDecimalSetting);
                                            obj.listUtilisationDetails.push(qtyAllocation);
                                        });
                                    }
                                    
                                    $scope.linkContractArray.push(obj);
                                })
                            }
                        }
                    });
                };

                var params = {
                    "contracts": $scope.linkContractArray,
                    "sessionID": $scope.sessionID,
                };

                PRMPRServices.linkContractToPRItems(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.objectID = response.objectID;
                            growlService.growl("Succesfully Linked With Contracts", 'success');
                            $scope.saveVendorPOItems();
                            //location.reload();
                        } else {
                            growlService.growl(response.errorMessage, 'inverse');
                        }
                    })

            };

            $scope.validateQuantity = function (contract)
            {
                contract.showQtyErrorMessage = '';
                contract.isValid = false;
                if ((+contract.editQuantity + contract.availedQuantity) > contract.quantity)
                {
                    contract.showQtyErrorMessage = 'Cannot Enter Quantity more than Total Quantity.';
                    contract.isValid = true;
                }

                if (+contract.editQuantity <= 0)
                {
                    contract.showQtyErrorMessage = 'Please enter quantity greater than zero.';
                    contract.isValid = true;
                }
            };

            $scope.assignEditQuantity = function (conItem,prContract)
            {
                var value = (prContract.prQuantity * (conItem.allocationPercentage / 100));
                conItem.editQuantity = $scope.precisionRound(value, $rootScope.companyRoundingDecimalSetting);
                $scope.validateQuantity(conItem);
            };

            $scope.precisionRound = function (number, precision) {
                var factor = Math.pow(10, precision);
                return Math.round(number * factor) / factor;
            };


            //$scope.savePOForm = function () {
            //    if ($scope.contractPODetails.contractTable && $scope.contractPODetails.contractTable.length > 0) {
            //        swal({
            //            title: "Are you Sure!",
            //            text: 'Do You want to Proceed to Create PO',
            //            type: "warning",
            //            showCancelButton: true,
            //            confirmButtonColor: "#DD6B55",
            //            confirmButtonText: "Yes",
            //            closeOnConfirm: true
            //        }, function (isConfirm) {
            //            if (isConfirm) {
            //                $timeout(function () {
            //                    $scope.$apply(function () {
            //                        angular.element('#templateSelection').modal('hide');
            //                        $scope.linkContract()
            //                        $scope.saveVendorPOItems();
            //                        growlService.growl("PO Saved Succesfully", 'success');

            //                    })
            //                }, 100);
            //                // angular.element('showContractForm').modal(true);
            //            } else {
            //                angular.element('#templateSelection').modal('show');
            //            }
            //        })
            //    }

            //}


            $scope.savePOForm = function () {
                angular.element('#templateSelection').modal('hide');
                $scope.ContractRequirements = [];
                if ($scope.contractPODetails.contractTable && $scope.contractPODetails.contractTable.length > 0) {
                    $scope.contractPODetails.contractTable.forEach(function (contactItem) {
                        if (contactItem && contactItem.contactsArray && contactItem.contactsArray.length > 0) {
                            let selectedContractItem = _.filter(contactItem.contactsArray, function (contract) {
                                return contract.isSelected;
                            });
                            if (selectedContractItem && selectedContractItem.length > 0) {
                                var contractNumber;
                                var contractNumbers = _(selectedContractItem)
                                    .filter(item => item.contractNumber)
                                    .map('contractNumber')
                                    .value();
                                contractNumber = contractNumbers.join(',');

                                selectedContractItem.forEach(function (cItem, cIndex) {
                                    var obj = {};
                                        obj.vendorId = cItem.vendorId;
                                        obj.contractNumber = cItem.contractNumber;
                                        obj.PC_ID = cItem.PC_ID;
                                        obj.ProductId = cItem.ProductId;
                                        obj.ProductName = cItem.ProductName;
                                        obj.REQ_ID = cItem.REQ_ID > 0 ? cItem.REQ_ID : 0;
                                        obj.QCS_ID = cItem.QCS_ID > 0 ? cItem.QCS_ID : 0;
                                        obj.ITEM_ID = cItem.ITEM_ID > 0 ? cItem.ITEM_ID : 0;
                                        obj.quantity = cItem.quantity;
                                        obj.price = cItem.price;
                                        obj.EXCHANGE_RATE = cItem.EXCHANGE_RATE;
                                        obj.companyName = cItem.companyName,
                                        obj.VENDOR_CODE = cItem.VENDOR_CODE,
                                        obj.availedQuantity = cItem.availedQuantity,
                                        obj.editQuantity = cItem.editQuantity,
                                        obj.startTime = cItem.startTime,
                                        obj.endTime = cItem.endTime,
                                        obj.TOTAL_PRICE = cItem.editQuantity * cItem.price,
                                        obj.VENDOR_SITE_CODE = cItem.SITE_CODE ? cItem.SITE_CODE : cItem.VENDOR_SITE_CODE;
                                        obj.PLANT_CODE = cItem.SITE_CODE ? cItem.SITE_CODE.split('_')[0] : cItem.VENDOR_SITE_CODE.split('_')[0];
                                        $scope.ContractRequirements.push(obj);
                                })
                            }
                        }
                    });
                }

            }



            $scope.saveVendorPOItems = function () {
                //$scope.linkContract()
                if ($scope.objectID > 0) {
                    $scope.linkContractArray = [];
                    if ($scope.contractPODetails.contractTable && $scope.contractPODetails.contractTable.length > 0) {
                        $scope.contractPODetails.contractTable.forEach(function (contactItem) {
                            if (contactItem && contactItem.contactsArray && contactItem.contactsArray.length > 0) {
                                let selectedContractItem = _.filter(contactItem.contactsArray, function (contract) {
                                    return contract.isSelected;
                                });
                                if (selectedContractItem && selectedContractItem.length > 0) {
                                    var contractNumber;
                                    var contractNumbers = _(selectedContractItem)
                                        .filter(item => item.contractNumber)
                                        .map('contractNumber')
                                        .value();
                                    contractNumber = contractNumbers.join(',');

                                    selectedContractItem.forEach(function (cItem, cIndex) {
                                        var obj = {};
                                        obj.listUtilisationDetails = [],
                                        obj.vendorId = cItem.vendorId;
                                        obj.contractNumber = contractNumber;
                                        obj.PC_ID = cItem.PC_ID;
                                        obj.ProductId = cItem.ProductId;
                                        obj.ProductName = cItem.ProductName;
                                        obj.PR_NUMBER = contactItem.prNumber;
                                        obj.REQ_ID = cItem.REQ_ID;
                                        obj.QCS_ID = cItem.QCS_ID;
                                        obj.ITEM_ID = cItem.ITEM_ID;
                                        obj.quantity = cItem.quantity;
                                        obj.price = cItem.price;
                                        obj.availedQuantity = cItem.availedQuantity;
                                        obj.editQuantity = cItem.editQuantity;
                                        obj.EXCHANGE_RATE = cItem.EXCHANGE_RATE;
                                        obj.VENDOR_SITE_CODE = cItem.SITE_CODE ? cItem.SITE_CODE : cItem.VENDOR_SITE_CODE;
                                        obj.PLANT_CODE = cItem.SITE_CODE ? cItem.SITE_CODE.split('_')[0] : cItem.VENDOR_SITE_CODE.split('_')[0];
                                        obj.VENDOR_CODE = cItem.VENDOR_CODE;


                                       var prs = _.uniq(contactItem.prLineItem.split(','))
                                        if (prs && prs.length > 0) {
                                            prs.forEach(function (prItem, prIndex) {
                                                var qtyAllocation =
                                                {
                                                    PC_ID: cItem.PC_ID,
                                                    COMP_ID: +$scope.compId,
                                                    U_ID: +$scope.userID,
                                                    VENDOR_ID: cItem.U_ID,
                                                    REQUIRED_QUANTITY: 0,
                                                    PR_ITEM_ID: 0,
                                                    PRODUCT_ID : 0
                                                }
                                                qtyAllocation.PR_NUMBER = prItem.split('$^^$')[0];
                                                qtyAllocation.QTY_UTILISED = +cItem.editQuantity;
                                                qtyAllocation.REQUIRED_QUANTITY = prItem.split('$^^$')[2];
                                                qtyAllocation.CREATED_DATE = contactItem.CREATED_DATE ? prItem.CREATED_DATE : null;
                                                qtyAllocation.MODIFIED_DATE = prItem.split('$^^$')[3];
                                                qtyAllocation.PR_ITEM_ID = prItem.split('$^^$')[4];
                                                qtyAllocation.PRODUCT_ID = prItem.split('$^^$')[5];
                                                obj.listUtilisationDetails.push(qtyAllocation);
                                            });
                                        }


                                        $scope.linkContractArray.push(obj);
                                    })
                                }
                            }
                        });
                    };

                    var params = {
                        "contracts": $scope.linkContractArray,
                        "sessionID": $scope.sessionID,
                    };

                    PRMPRServices.saveVendorPOItems(params)
                        .then(function (response) {
                            if (response.errorMessage == '') {

                                swal({
                                    title: "Done!",
                                    text: "This PO Information will be Pushed to ERP",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: true
                                },

                                    function () {
                                        location.reload();
                                    });

                                //growlService.growl("PO Saved Succesfully", 'success');
                                //location.reload();
                            } else {
                                swal({
                                    title: "Error!",
                                    text: response.errorMessage,
                                    type: "warning",
                                    showCancelButton: false,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: true
                                },

                                    function () {
                                        location.reload();
                                    });

                                //growlService.growl(response.errorMessage, 'inverse');
                            }
                        })
                }

            };

            $scope.cancelContractRequirements = function () {
                angular.element('#templateSelection').modal('show');
                angular.element('#potemplateSelection').modal('hide');
            }

            

        }]);﻿prmApp
    .controller('prActionsCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService",
        "storeService", "growlService", "PRMPRServices", "poService", "$rootScope", "catalogService",
        "fileReader",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService,
            storeService, growlService, PRMPRServices, poService, $rootScope, catalogService,
            fileReader) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compID = userService.getUserCompanyId();
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.prID = $stateParams.Id;
            $scope.sendcommunication = true;

            $scope.prStatus = [
                {
                    display: 'PENDING',
                    value: 'PENDING'
                },
                {
                    display: 'COMPLETED',
                    value: 'COMPLETED'
                },
                {
                    display: 'CANCELLED',
                    value: 'CANCELLED'
                },
                {
                    display: 'REJECTED',
                    value: 'REJECTED'
                }
            ];
          
            $scope.savePr = function () {

                var params = {
                    "prdetails": $scope.PRDetails,
                    "sendcommunication": $scope.sendcommunication,
                    "sessionid": userService.getUserToken()
                };

                var ts = userService.toUTCTicks($scope.PRDetails.REQUEST_DATE);
                var m = moment(ts);
                var reqdate = new Date(m);
                var milliseconds = parseInt(reqdate.getTime() / 1000.0);
                params.prdetails.REQUEST_DATE = "/Date(" + milliseconds + "000++530)/";

                ts = userService.toUTCTicks($scope.PRDetails.REQUIRED_DATE);
                m = moment(ts);
                var reqrddate = new Date(m);
                milliseconds = parseInt(reqrddate.getTime() / 1000.0);
                params.prdetails.REQUIRED_DATE = "/Date(" + milliseconds + "000++530)/";
                
                PRMPRServices.savePRActions(params)
                    .then(function (response) {

                        if (response.errorMessage !== '') {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Saved Successfully.", "success");
                        }
                    });
            };


            $scope.getprdetails = function () {
                var params = {
                    "prid": $stateParams.Id,
                    "sessionid": userService.getUserToken()
                };
                PRMPRServices.getprdetails(params)
                    .then(function (response) {

                        $scope.PRDetails = response;                        
                        $scope.PRDetails.REQUEST_DATE = userService.toLocalDate($scope.PRDetails.REQUEST_DATE);
                        $scope.PRDetails.REQUIRED_DATE = userService.toLocalDate($scope.PRDetails.REQUIRED_DATE);                       
                    });
            };


            if ($stateParams.Id > 0) {
                $scope.getprdetails();
            }

            $scope.goToPrList = function (id) {
                var url = $state.href("list-pr");
                window.open(url, '_self');
            };
        }]);
﻿prmApp
    .controller('prCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService",
        "storeService", "growlService", "PRMPRServices", "poService", "$rootScope", "catalogService",
        "fileReader",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService,
            storeService, growlService, PRMPRServices, poService, $rootScope, catalogService,
            fileReader) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compID = userService.getUserCompanyId();
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.prID = $stateParams.Id;

            $scope.prStatus = [
                {
                    display: 'PENDING',
                    value: 'PENDING'
                },
                {
                    display: 'COMPLETED',
                    value: 'COMPLETED'
                },
                {
                    display: 'CANCELLED',
                    value: 'CANCELLED'
                },
                {
                    display: 'REJECTED',
                    value: 'REJECTED'
                }
            ];


            $scope.companyItemUnits = [];


            $scope.companyDepartments = [];

            $scope.PRDetails = {
                isTabular: true,
                PR_ID: 0,
                U_ID: userService.getUserId(),
                COMP_ID: 0,
                PR_NUMBER: '',
                PR_TYPE: 0,
                ASSET_TYPE: '',
                PRIORITY: '',
                PRIORITY_COMMENTS: '',
                DEPARTMENT: 0,
                REQUEST_DATE: '',
                REQUIRED_DATE: '',
                ATTACHMENTS: '',
                TOTAL_BASE_PRICE: 0,
                TOTAL_GST_PRICE: 0,
                TOTAL_PRICE: 0,
                SUB_TOTAL: 0,
                GST_PRICE: 0,
                WF_ID:0,
                STATUS: 'PENDING',
                CLASSIFICATION: '',
                COMPANY: '',
                WORK_ORDER_DURATION: '',
                PURPOSE_IN_BRIEF:'',

                PRItemsList: [],

                PURCHASE:true,
                PRSHOW :true
            };

            $scope.PRItem = {

                ITEM_ID: 0,
                PR_ID: 0,
                ITEM_NAME: '',
                HSN_CODE: '',
                ITEM_CODE: '',
                ITEM_DESCRIPTION: '',
                BRAND: '',
                UNITS: '',
                EXIST_QUANTITY: 0,
                REQUIRED_QUANTITY: 0,
                UNIT_PRICE: 0,
                C_GST_PERCENTAGE: 0,
                S_GST_PERCENTAGE: 0,
                I_GST_PERCENTAGE: 0,
                TOTAL_PRICE: 0,
                COMMENTS: '',
                ATTACHMENTS: '',
                CREATED_BY: 0,
                CREATED_DATE: '',
                MODIFIED_BY: 0,
                MODIFIED_DATE: '',
                CATALOGUE_ID: 0,
                U_ID: $scope.userID,
                sessionID: $scope.sessionID,
                itemAttachment: [],
                attachmentName: '',
                ITEM_NUM:''
            };

            auctionsService.GetCompanyDepartments($scope.userID, $scope.sessionID)
                .then(function (response) {
                    $scope.companyDepartments = response;
                });

            $scope.compID = userService.getUserCompanyId();
            auctionsService.GetCompanyConfiguration($scope.compID, "ITEM_UNITS", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyItemUnits = unitResponse;
                });

            $scope.PRDetails.PRItemsList.push($scope.PRItem);

            $scope.addPrItem = function () {
                var PRItem = {
                    ITEM_ID: 0,
                    PR_ID: 0,
                    ITEM_NAME: '',
                    HSN_CODE: '',
                    ITEM_CODE: '',
                    ITEM_DESCRIPTION: '',
                    BRAND: '',
                    UNITS: '',
                    EXIST_QUANTITY: 0,
                    REQUIRED_QUANTITY: 0,
                    UNIT_PRICE: 0,
                    C_GST_PERCENTAGE: 0,
                    S_GST_PERCENTAGE: 0,
                    I_GST_PERCENTAGE: 0,
                    TOTAL_PRICE: 0,
                    COMMENTS: '',
                    ATTACHMENTS: '',
                    CREATED_BY: 0,
                    CREATED_DATE: '',
                    MODIFIED_BY: 0,
                    MODIFIED_DATE: '',
                    CATALOGUE_ID: 0,
                    U_ID: $scope.userID,
                    sessionID: $scope.sessionID,
                    ITEM_NUM :''
                };

                $scope.PRDetails.PRItemsList.push(PRItem);

            };

            $scope.checkIsFormDisable = function () {
                $scope.isFormdisabled = true;
                if ($scope.PRDetails.CREATED_BY == $scope.userID) {
                    $scope.isFormdisabled = false;
                }
            };
           
            $scope.savePr = function () {
                $scope.PRDetails.U_ID = userService.getUserId();
                var params = {
                    "prdetails": $scope.PRDetails,
                    "sessionid": userService.getUserToken()
                };
                PRMPRServices.savePrDetails(params)
                    .then(function (response) {

                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Saved Successfully.", "success");
                            $state.go('list-pr');

                        }

                    });
            };


            $scope.getprdetails = function () {
                var params = {
                    "prid": $stateParams.Id,
                    "sessionid": userService.getUserToken()
                };
                PRMPRServices.getprdetails(params)
                    .then(function (response) {

                        $scope.PRDetails = response;
                        if ($scope.PRDetails.TYPE == "PR") {
                            $scope.PRDetails.PURCHASE = true;
                            $scope.PRDetails.PRSHOW = true;
                        } else {
                            $scope.PRDetails.PURCHASE = false;
                            $scope.PRDetails.PRSHOW = false;
                        }
                        $scope.PRDetails.REQUEST_DATE = userService.toLocalDate($scope.PRDetails.REQUEST_DATE);//new moment($scope.PRDetails.REQUEST_DATE).format("YYYY-MM-DD HH:mm");
                        $scope.PRDetails.REQUIRED_DATE = userService.toLocalDate($scope.PRDetails.REQUIRED_DATE);//new moment($scope.PRDetails.REQUIRED_DATE).format("YYYY-MM-DD HH:mm");
                        if ($stateParams.Id > 0) {
                            $scope.checkIsFormDisable();
                        }
                    });
            };


            if ($stateParams.Id > 0) {
                $scope.getprdetails();
            }

            $scope.deletePrItem = function (index) {
                if ($scope.PRDetails.PRItemsList.length > 1) {
                    $scope.PRDetails.PRItemsList.splice(index, 1);
                    $scope.prUnitPriceCalculation();
                } else {
                    growlService.growl("Can't Delete this Item", "inverse");
                }
            };
            
            $scope.prUnitPriceCalculation = function () {
                $scope.PRDetails.TOTAL_PRICE = 0;
                $scope.PRDetails.TOTAL_BASE_PRICE = 0;
                $scope.PRDetails.TOTAL_GST_PRICE = 0;

                $scope.PRDetails.PRItemsList.forEach(function (item, index) {
                    if (item.UNIT_PRICE == undefined || item.UNIT_PRICE <= 0) {
                        item.UNIT_PRICE = 0;
                    };


                    item.TOTAL_BASE_PRICE = item.UNIT_PRICE * item.REQUIRED_QUANTITY;
                    item.TOTAL_PRICE = item.TOTAL_BASE_PRICE + ((item.TOTAL_BASE_PRICE / 100) * (item.C_GST_PERCENTAGE));

                    $scope.PRDetails.TOTAL_GST_PRICE += (item.TOTAL_BASE_PRICE / 100) * (item.C_GST_PERCENTAGE);
                    $scope.PRDetails.TOTAL_BASE_PRICE += item.TOTAL_BASE_PRICE;
                    $scope.PRDetails.TOTAL_PRICE += item.TOTAL_PRICE;

                });
            };

            $scope.userDepartments = [];
            $scope.userDepartments.push(userService.getSelectedUserDepartmentDesignation());
            if ($scope.PRDetails.PR_ID == 0) {
                $scope.PRDetails.DEPARTMENT = $scope.userDepartments[0].deptID;
                $scope.PRDetails.DEPT_CODE = $scope.userDepartments[0].deptCode;
            }
            $scope.goToPrList = function (id) {
                var url = $state.href("list-pr");
                window.open(url, '_self');
            };

            $scope.deptChanged = function () {
                $scope.userDepartments.filter(function (udd) {
                    if (udd.deptID == $scope.PRDetails.DEPARTMENT) {
                        $scope.PRDetails.DEPT_CODE = udd.deptCode;
                    }
                });
            };



            //#region Catalog
            $scope.productsList = [];
            $scope.getProducts = function () {
                catalogService.getProducts($scope.compID)
                    .then(function (response) {
                        $scope.productsList = response;
                    });
            }
            $scope.getProducts();
            $scope.autofillProduct = function (prodName, index) {
                $scope['ItemSelected_' + index] = false;
                var output = [];
                if (prodName && prodName.trim() != '') {
                    angular.forEach($scope.productsList, function (prod) {
                        if (prod.prodName.toLowerCase().indexOf(prodName.trim().toLowerCase()) >= 0) {
                            output.push(prod);
                        }
                    });
                }
                $scope["filterProducts_" + index] = output;
            }
            $scope.fillTextbox = function (selProd, index) {
                $scope['ItemSelected_' + index] = true;
                $scope.PRDetails.PRItemsList[index].ITEM_NAME = selProd.prodName;
                $scope.PRDetails.PRItemsList[index].ITEM_CODE = selProd.prodCode;
                $scope.PRDetails.PRItemsList[index].ITEM_NUM = selProd.prodNo;
                //ITEM_NUM
                $scope.PRDetails.PRItemsList[index].CATALOGUE_ID = selProd.prodId;
                $scope.PRDetails.PRItemsList[index].ITEM_DESCRIPTION = selProd.prodDesc;
                $scope.PRDetails.PRItemsList[index].UNITS = selProd.prodQty;
                $scope.PRDetails.PRItemsList[index].HSN_CODE = selProd.prodHSNCode;
                $scope['filterProducts_' + index] = null;

                $scope.loadUserDepartments(selProd.prodId, index);

            }

            $scope.onBlurProduct = function (index) {
                if ($scope['ItemSelected_' + index] == false) {
                    $scope.PRDetails.PRItemsList[index].ITEM_NAME = "";
                }
            }
            //#endregion Catalog

            $scope.storeDetailsEdit = [];

            $scope.loadUserDepartments = function (productid, index) {

                var params = {
                    storeid: 0,
                    productid: productid
                }

                var inStock = 0, storeCode = '';
                $scope.inStock = 0;
                $scope.storeName = '';
                storeService.getcompanystoreitems(params.storeid, params.productid)
                    .then(function (response) {
                        $scope.storeDetails = response;
                        $scope.storeDetails.forEach(function (item, index) {
                            $scope.inStock += item.inStock;

                        });

                        //inStock = response[0].inStock;
                        $scope.PRDetails.PRItemsList[index].EXIST_QUANTITY = $scope.inStock;
                    })
            };

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {

                        if (id != "itemsAttachment") {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var index = id;
                            var obj = $scope.PRDetails.PRItemsList[index];
                            obj.itemAttachment = arrayByte;
                            obj.attachmentName = ItemFileName;
                            $scope.PRDetails.PRItemsList.splice(index, 1, obj);
                            console.log(index);
                            console.log($scope.PRDetails.PRItemsList);
                        }
                    });
            };

            $scope.GetCompanyRFQCreators = function () {
                var params = {
                    pr_id: $stateParams.Id//,
                   // dept_id: userService.getSelectedUserDepartmentDesignation().deptID
                }
                PRMPRServices.GetCompanyRFQCreators(params)
                    .then(function (response) {
                        $scope.CompanyRFQCreators = response;

                        $scope.CompanyRFQCreators = $scope.CompanyRFQCreators.filter(function (item, index) {
                            if (item.DEPT_ID == userService.getSelectedUserDepartmentDesignation().deptID) {
                                return item;
                            } else {

                            }
                        })

                        if ($scope.CompanyRFQCreators && $scope.CompanyRFQCreators.length > 0)
                        {
                            $scope.CompanyRFQCreators.forEach(function (item, index) {
                                if (item.IS_ASSIGNED > 0) {
                                    item.IS_ASSIGNED = true;
                                }
                                else {
                                    item.IS_ASSIGNED = false;
                                }
                            })
                        }

                    })
            };

            $scope.GetCompanyRFQCreators();

            //if ($rootScope.isUserEntitled(591168))
            //{
            //    $scope.GetCompanyRFQCreators();
            //}

            
            $scope.SaveCompanyRFQCreators = function () {
                var params = {
                    listPRRFQCreator: $scope.CompanyRFQCreators,
                    sessionid: userService.getUserToken()
                }

                if (params.listPRRFQCreator && params.listPRRFQCreator.length > 0) {
                    params.listPRRFQCreator.forEach(function (item, index) {
                        item.PR_ID = $stateParams.Id;
                        if (item.IS_ASSIGNED) {
                            item.IS_ASSIGNED = 1;
                        }
                        else {
                            item.IS_ASSIGNED = 0;
                        }
                    })
                }

                PRMPRServices.SaveCompanyRFQCreators(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.GetCompanyRFQCreators();
                        } else {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    })
            };



            $scope.display = function (type) {
                if (type == 'PR') {
                    $scope.PRDetails.PRSHOW = true;
                    $scope.PRDetails.PRIORITY = '';
                    $scope.PRDetails.COMPANY = '';
                  //  $scope.PRDetails.REQUEST_DATE = today;
                } else {
                    $scope.PRDetails.PRSHOW = false;
                    $scope.PRDetails.PR_NUMBER = '';
                    $scope.GetSeries('', userService.getSelectedUserDeptID());
                    $scope.PRDetails.PRItemsList = [];
                    //$scope.PRDetails = {
                    $scope.PRDetailsisTabular = true;
                    $scope.PRDetails.PR_ID = 0,
                        $scope.PRDetails.U_ID = userService.getUserId();
                    $scope.PRDetails.COMP_ID = 0;
                    $scope.PRDetails.PR_NUMBER = '';
                    $scope.PRDetails.PR_TYPE = 0;
                    $scope.PRDetails.ASSET_TYPE = '';
                    $scope.PRDetails.PRIORITY = 'Anantapur';
                    $scope.PRDetails.PRIORITY_COMMENTS = '';
                    $scope.PRDetails.DEPARTMENT = userService.getSelectedUserDeptID();
                 //   $scope.PRDetails.REQUEST_DATE = today;
                    $scope.PRDetails.REQUIRED_DATE = '';
                    $scope.PRDetails.ATTACHMENTS = '';
                    $scope.PRDetails.TOTAL_BASE_PRICE = 0;
                    $scope.PRDetails.TOTAL_GST_PRICE = 0;
                    $scope.PRDetails.TOTAL_PRICE = 0;
                    $scope.PRDetails.SUB_TOTAL = 0;
                    $scope.PRDetails.GST_PRICE = 0;
                    $scope.PRDetails.WF_ID = 0;
                    //CREATED_BY: 0,
                    //CREATED_DATE: '',
                    //MODIFIED_BY: 0,
                    //MODIFIED_DATE: '',
                    $scope.PRDetails.CLASSIFICATION = '';
                    $scope.PRDetails.COMPANY = 'Greenko';
                    $scope.PRDetails.WORK_ORDER_DURATION = '';
                    $scope.PRDetails.PURPOSE_IN_BRIEF = '';

                    $scope.PRDetails.PRItemsList = [];

                    //};


                    var PRItem = {

                        ITEM_ID: 0,
                        PR_ID: 0,
                        ITEM_NAME: '',
                        HSN_CODE: '',
                        ITEM_CODE: '',
                        ITEM_DESCRIPTION: '',
                        BRAND: '',
                        UNITS: '',
                        EXIST_QUANTITY: 0,
                        REQUIRED_QUANTITY: 0,
                        UNIT_PRICE: 0,
                        C_GST_PERCENTAGE: 0,
                        S_GST_PERCENTAGE: 0,
                        I_GST_PERCENTAGE: 0,
                        TOTAL_PRICE: 0,
                        COMMENTS: '',
                        ATTACHMENTS: '',
                        CREATED_BY: 0,
                        CREATED_DATE: '',
                        MODIFIED_BY: 0,
                        MODIFIED_DATE: '',
                        CATALOGUE_ID: 0,
                        U_ID: $scope.userID,
                        sessionID: $scope.sessionID,
                        itemAttachment: [],
                        attachmentName: '',
                        ITEM_NUM: ''

                    };
                    $scope.PRDetails.PRItemsList.push(PRItem);
                }
            }

          //  console.log("purchase value is>>>>>>" + $scope.PRDetails.PURCHASE);
        }]);
prmApp.constant('PRMPRServicesDomain', 'pr/svc/PRMPRService.svc/REST/');
prmApp.constant('signalRFwdHubName', 'fwdRequirementHub');
prmApp.service('PRMPRServices', ["PRMPRServicesDomain", "userService", "httpServices",
    function (PRMPRServicesDomain, userService, httpServices) {


        var PRMPRServices = this;


        PRMPRServices.GetSeries = function (series, seriestype, deptid) {
            var url = PRMPRServicesDomain + 'getseries?series=' + series + '&seriestype=' + seriestype + '&compid=' + userService.getUserCompanyId() + '&deptid=' + deptid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.getprdetails = function (params) {
            let url = PRMPRServicesDomain + 'getprdetails?prid=' + params.prid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.GetItemDetails = function (params) {
            let url = PRMPRServicesDomain + 'GetItemDetails?prIDS=' + params.prIds + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.GetPRItemsByReqId = function (params) {
            let url = PRMPRServicesDomain + 'getpritemsbyreq?reqid=' + params.reqid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.DeLinkInValidPRItems = function (params) {
            let url = PRMPRServicesDomain + 'delinkinvalidpritems?reqid=' + params.reqid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.getFilterValues = function (params) {
           // let url = PRMPRServicesDomain + 'getFilterValues?compid=' + params.compid + '&sessionid=' + userService.getUserToken();
            let url = PRMPRServicesDomain + 'getFilterValues?compid=' + params.compid + '&sessionid=' + userService.getUserToken() + '&fromdate=' + params.fromDate + '&todate=' + params.toDate;
            return httpServices.get(url);
        };
        
        PRMPRServices.getprlist = function (params) {
            let url = PRMPRServicesDomain + 'getprlist?userid=' + params.userid + '&sessionid=' + params.sessionid + '&deptid=' + params.deptid
                + '&desigid=' + params.desigid + '&depttypeid=' + params.depttypeid + '&onlyopen=' + 0
                + '&plant=' + params.plant + '&projectType=' + params.projectType + '&sectionHead=' + params.sectionHead + '&wbsCode=' + params.wbsCode + '&profitCentre=' + params.profitCentre
                + '&purchaseCode=' + params.purchaseCode + '&creatorName=' + params.creatorName
                + '&clientName=' + params.clientName + '&prStatus=' + params.prStatus + '&search=' + params.searchString
                + '&PageSize=' + params.PageSize + '&NumberOfRecords=' + params.NumberOfRecords + '&fromdate=' + params.fromDate + '&todate=' + params.toDate + '&prType=' + params.prType + '&isArchived=' + params.isArchived;
            return httpServices.get(url);
        };

        PRMPRServices.GetFullPRDetails = function (params) {
            let url = PRMPRServicesDomain + 'getFullPRDetails?prid=' + params.userid + '&sessionid=' + params.sessionid + '&deptid=' + params.deptid + '&desigid=' + params.desigid + '&depttypeid=' + params.depttypeid;
            return httpServices.get(url);
        };

        PRMPRServices.savePrDetails = function (params) {
            let url = PRMPRServicesDomain + 'saveprdetails';
            return httpServices.post(url, params);
        };

        PRMPRServices.savePRActions = function (params) {
            let url = PRMPRServicesDomain + 'savepractions';
            return httpServices.post(url, params);
        };

        PRMPRServices.getreqprlist = function (params) {
            //let url = PRMPRServicesDomain + 'getreqprlist?userid=' + params.userid + '&sessionid=' + userService.getUserToken();
            //return httpServices.get(url);
            if (params.onlyopen) {
                params.onlyopen = 1;
            } else {
                params.onlyopen = 0;
            }

            let url = PRMPRServicesDomain + 'getprlist?userid=' + params.userid + '&sessionid=' + params.sessionid + '&deptid=' + params.deptid + '&desigid=' + params.desigid + '&depttypeid=' + params.depttypeid + '&onlyopen=' + params.onlyopen;
            return httpServices.get(url);
        };

        PRMPRServices.GetCompanyRFQCreators = function (params) {
            let url = PRMPRServicesDomain + 'getcompanyrfqcreators?u_id=' + userService.getUserId() + '&pr_id=' + params.pr_id + '&sessionid=' + userService.getUserToken();// + '&dept_id=' + params.dept_id
            return httpServices.get(url);
        };

        PRMPRServices.SaveCompanyRFQCreators = function (params) {
            let url = PRMPRServicesDomain + 'savecompanyrfqcreators';
            return httpServices.post(url, params);
        };

        PRMPRServices.getpritemslist = function (params) {
            let url = PRMPRServicesDomain + 'getpritemslist?prid=' + params.prid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.getrequirementprstatus = function (params) {
            let url = PRMPRServicesDomain + 'getrequirementprstatus?reqid=' + params.reqid + '&prid=' + params.prid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.getprfieldmapping = function (params) {
            let url = PRMPRServicesDomain + 'getprfieldmapping?type=' + params.type + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPRServices.linkRFPToPR = function (params) {
            let url = PRMPRServicesDomain + 'linktorfp?reqid=' + params.reqid + '&prid=' + params.prid + '&user=' + params.userid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        /********  CONSOLIDATE PR ********/

        PRMPRServices.GetIndividualItems = function (params) {
            let url = PRMPRServicesDomain + 'GetIndividualItems?userId=' + userService.getUserId() + '&compId=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken()
                + '&plant=' + params.plant + '&projectType=' + params.projectType + '&sectionHead=' + params.sectionHead + '&wbsCode=' + params.wbsCode + '&profitCentre=' + params.profitCentre + '&purchaseCode=' + params.purchaseCode + '&creatorName=' + params.creatorName + '&clientName=' + params.clientName + '&search=' + params.search + '&PageSize=' + params.PageSize + '&NumberOfRecords=' + params.NumberOfRecords;
            return httpServices.get(url);
        };

        PRMPRServices.GetPRSbyItem = function (params) {
            let url = PRMPRServicesDomain + 'GetPRSbyItem?productId=' + params.PRODUCT_ID + '&userId=' + userService.getUserId() +
                '&compId=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken()
                + '&plant=' + params.plant + '&projectType=' + params.projectType + '&sectionHead=' + params.sectionHead + '&wbsCode=' + params.wbsCode + '&profitCentre=' + params.profitCentre + '&search=' + params.search;
            return httpServices.get(url);
        };

        PRMPRServices.archivePR = function (params) {
            let url = PRMPRServicesDomain + 'archivepr';
            return httpServices.post(url, params);
        };

        /********  CONSOLIDATE PR ********/


        PRMPRServices.getFilterValuesV2 = function (params) {
            let url = PRMPRServicesDomain + 'getFilterValuesV2';
            return httpServices.post(url, params);
        };

        PRMPRServices.getprlistV2 = function (params) {
            let url = PRMPRServicesDomain + 'getPRListV2';
            return httpServices.post(url, params);
        };

        PRMPRServices.linkContractToPRItems = function (params) {
            let url = PRMPRServicesDomain + 'linkContractToPRItems';
            return httpServices.post(url, params);
        };

        PRMPRServices.saveVendorPOItems = function (params) {
            let url = PRMPRServicesDomain + 'saveVendorPOItems';
            return httpServices.post(url, params);
        };

        PRMPRServices.autoAllocate = function (params) {
            let url = PRMPRServicesDomain + 'autoAllocate';
            return httpServices.post(url, params);
        };

        return PRMPRServices;

}]);