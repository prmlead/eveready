﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class json_entity
    {

        [DataMember] [DataNames("ENTITY_ID")] public int ENTITY_ID { get; set; }
        [DataMember] [DataNames("MESSAGE")] public string MESSAGE { get; set; }

        [DataMember] [DataNames("ERROR_MESSAGE")] public string ERROR_MESSAGE { get; set; }
        [DataMember] [DataNames("SESSION_ID")] public string SESSION_ID { get; set; }

        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime? DATE_MODIFIED { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }

        private DateTime? currentTime = DateTime.Now;
        [DataMember(Name = "currentTime")]
        public DateTime? CurrentTime
        {
            get { return currentTime; }
            set { currentTime = value; }
        }

    }
}