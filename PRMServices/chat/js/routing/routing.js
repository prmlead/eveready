﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('req-chat', {
                    url: '/req-chat/:reqId',
                    templateUrl: 'chat/views/chat-details.html',
                    params: {
                        detailsObj: null
                    }
                });
        }]);