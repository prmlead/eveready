﻿
prmApp
    .controller('pendingContractsCtrl', function ($scope, $rootScope, $state, $stateParams, userService, catalogService, PRMPRServices, growlService, auctionsService) {

        $scope.sessionid = userService.getUserToken();  

        $scope.MyPendingContracts = [];
        $scope.MyPendingContracts1 = [];
        $scope.MyPendingContracts2 = [];
        $scope.openContracts = 0;
        $scope.closedContracts = 0;
        $scope.totalContracts = 0;
        $scope.contractValue = 0;
        $scope.quantity = 0;
        $scope.availedQuantity = 0;
        $scope.utility = 0;
        $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
        $scope.isSuperUser = userService.getUserObj().isSuperUser;
        $scope.objectID = 0;
        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
        let requirementPRContacts = $rootScope.PRPendingContracts;
        console.log(requirementPRContacts);
        $scope.isModalWindow = true;

        $scope.filters = {
            categoryStatus: '',
            departmentStatus: '',
            productName: '',
            supplierName: '',
            status: 'Active',
            startDate: moment().add('days', -30).format('YYYY-MM-DD'),
            endDate: moment().format('YYYY-MM-DD'),
            bulkStatus: 'Delete',
            searchKeyword : ''

        };
        $scope.userID = userService.getUserId();
        $scope.compID = userService.getUserCompanyId();
        /*PAGINATION CODE*/
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5; //Number of pager buttons to show
        
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };
    /*PAGINATION CODE*/

        $scope.supplierArr = [];
        $scope.itemArr = [];
        $scope.catArr = [];

       
        $scope.GetMyPendingContracts = function (productName, supplierName, status, startDate, endDate, search) {
            //if (productName == '') {
            //    $scope.itemArr = [];
            //}
            $scope.MyPendingContracts = [];
            $scope.openContracts = 0;
            $scope.totalContracts = 0;
            $scope.contractValue = 0;
            $scope.closedContracts = 0;
            $scope.utility = 0;
            $scope.totalItems = 0;
            productName = productName ? productName : '';
            supplierName = supplierName ? supplierName : '';
            status = status ? status : '';
            catalogService.GetMyPendingContracts(productName, supplierName, status, startDate, endDate, search)
                .then(function (response) {
                    if (response && response.length > 0) {
                        response.forEach(function (item, index) {

                            $scope.contractValue += item.contractValue;
                            $scope.quantity += item.quantity;
                            $scope.availedQuantity += item.availedQuantity;
                            

                            if ($scope.supplierArr.indexOf(item.supplierName) == -1) {
                                $scope.supplierArr.push(item.supplierName);

                            }
                            
                            if ($scope.catArr.indexOf(item.categories) == -1) {
                                $scope.catArr.push(item.categories);

                            }
                            if ($scope.itemArr.indexOf(item.ProductName) == -1) {
                                $scope.itemArr.push(item.ProductName);

                            }

                            let isContractItem = false;
                            if (requirementPRContacts && requirementPRContacts.length > 0) {
                                let anyContractItem = _.filter(requirementPRContacts, function (o) {
                                    return o.PRODUCT_ID === item.ProductId;
                                });

                                if (anyContractItem && anyContractItem.length > 0) {
                                    isContractItem = true;
                                }
                            } else {
                                isContractItem = true;
                            }


                            item.startTime = userService.toLocalDate(item.startTime);
                            item.endTime = userService.toLocalDate(item.endTime);

                            if (item.contractStatus === 'Active') {
                                $scope.openContracts += 1;
                            }
                            if (item.contractStatus === 'Closed') {
                                $scope.closedContracts += 1;
                            }

                            item.multipleAttachments = [];
                            if (item.document) {
                                item.multipleAttachments = JSON.parse(item.document);
                            }

                            $scope.MyPendingContracts.push(item);

                            isContractItem = true;
                            
                        });
                        $scope.utility = (($scope.availedQuantity / $scope.quantity) * 100).toFixed(2);
                        

                        $scope.totalContracts = $scope.MyPendingContracts.length
                        $scope.MyPendingContracts1 = $scope.MyPendingContracts;
                        $scope.MyPendingContracts2 = $scope.MyPendingContracts;
                        /*PAGINATION CODE START*/
                        $scope.totalItems = $scope.MyPendingContracts.length;
                        /*PAGINATION CODE END*/
                        //$scope.filters.status = 'Active';
                        //$scope.setFilters();

                    }
                });
        };

        $scope.GetMyPendingContracts('', '', $scope.filters.status, $scope.filters.startDate, $scope.filters.endDate, $scope.filters.searchKeyword);

        $scope.setFilters = function ()
        {
            $scope.MyPendingContracts = [];
            $scope.GetMyPendingContracts($scope.filters.productName, $scope.filters.supplierName, $scope.filters.status, $scope.filters.startDate, $scope.filters.endDate,$scope.filters.searchKeyword);
        }



        $scope.editContract = function (contract) {
            //$state.go("productEdit", { "viewId": "edit", "productId": contract.ProductId });
            $state.go("viewProductContract", { "PC_ID": contract.PC_ID, "productId": contract.ProductId});
            /*console.log(contract);*/
        };

        $scope.showDelete = false;

        $scope.checkAll = function (wf) {
            $scope.MyPendingContracts.forEach(function (item, index) {
                item.isChecked = wf.checkAll;
            });
            $scope.showDelete = wf.checkAll;
        };

        $scope.checkEachRow = function () {
            var filteredRows = $scope.MyPendingContracts.filter(function (item) {
                return item.isChecked;
            })

            if (filteredRows && filteredRows.length > 0) {
                $scope.showDelete = true;
            } else {
                $scope.showDelete = false;
            }
        };

        $scope.bulkUpdateStatus = function (status)
        {
            if (!$scope.filters.bulkStatus)
            {
                swal("Error!", "Please select the status to update", 'error');
                return;
            }

            var filteredRows = $scope.MyPendingContracts.filter(function (item) {
                return item.isChecked;
            })
            var contract;
            if (filteredRows && filteredRows.length > 0) {
                var contracts = _(filteredRows)
                    .filter(item => item.isChecked)
                    .map('PC_ID')
                    .value();
                contract = contracts.join(',');
            } else {
                swal("Error!", "Please select atleast one contract to update the status", 'error');
                return;
            }

            var params =
            {
                "status": $scope.filters.bulkStatus,
                "sessionId": $scope.sessionid,
                "uId": userService.getUserId(),
                "contractIds": contract
            };
            catalogService.bulkUpdateContractStatus(params)
                .then(function (response) {
                    if (!response.errorMessage) {
                        swal("Success!", "Status Updated Successfully.", 'success');
                        location.reload();
                    } else {
                        swal("Error!", response.errorMessage, 'error');
                    }
                });
        };

        $scope.utiliseContract = function (contract)
        {
            var contracts = [{ id: '', name: '' }];
            contracts[0].id = contract.PC_ID;
            contracts[0].name = contract.PC_ID;

            var products = [{ id: '', name: '' }];
            products[0].id = contract.ProductId;
            products[0].name = contract.ProductId;

            var vendors = [{ id: '', name: '' }];
            vendors[0].id = contract.vendorId;
            vendors[0].name = contract.vendorId;

            var routeObj = { "contracts": contracts, "products": products, "vendors": vendors };
            $state.go('contractUtilisation', { "PC_ID": contract.PC_ID, 'contractProducts': JSON.stringify(routeObj) });
        };




        $scope.linkContract = function (obj,qty,siteCode) {
            $scope.linkContractArray = [];

            $scope.MyPendingContracts.forEach(function (item) {
                if (obj == item.PC_ID) {
                    item.editQuantity = qty;
                    item.startTime = null;
                    item.endTime = null;
                    item.listUtilisationDetails = [];
                    item.VENDOR_SITE_CODE = item.SITE_CODE ? item.SITE_CODE : item.VENDOR_SITE_CODE;
                    item.PLANT_CODE = item.SITE_CODE ? item.SITE_CODE.split('_')[0] : item.VENDOR_SITE_CODE.split('_')[0];


                    var prs = _.uniq(item.PR_LINE_ITEM_INFO.split(','))
                    if (prs && prs.length > 0) {
                        prs.forEach(function (prItem, prIndex) {
                            var qtyAllocation =
                            {
                                PC_ID: item.PC_ID,
                                COMP_ID: +$scope.compID,
                                U_ID: +$scope.userID,
                                VENDOR_ID: item.vendorId,
                                REQUIRED_QUANTITY: 0,
                                PR_ITEM_ID: 0,
                                PRODUCT_ID : 0
                            }
                            qtyAllocation.PR_NUMBER = prItem.split('$^^$')[0];
                            qtyAllocation.PR_LINE_ITEM = prItem.split('$^^$')[1];
                            qtyAllocation.PO_NUMBER = null;
                            qtyAllocation.PO_LINE_ITEM = null;
                            qtyAllocation.QTY_UTILISED = item.editQuantity;
                            qtyAllocation.PR_ITEM_ID = prItem.split('$^^$')[2];
                            qtyAllocation.REQUIRED_QUANTITY = prItem.split('$^^$')[3];
                            qtyAllocation.CREATED_DATE =  null;
                            qtyAllocation.MODIFIED_DATE = prItem.split('$^^$')[4];
                            qtyAllocation.PRODUCT_ID = prItem.split('$^^$')[5];
                            if (qtyAllocation.MODIFIED_DATE) {
                                var ts = moment(qtyAllocation.MODIFIED_DATE);
                                var m = moment(ts);
                                var date = new Date(m);
                                var milliseconds = parseInt(date.getTime() / 1000.0);
                                qtyAllocation.MODIFIED_DATE = "/Date(" + milliseconds + "000+0530)/";
                            } else {
                                qtyAllocation.MODIFIED_DATE = null;
                            }
                            //$scope.precisionRound((+cItem.editQuantity / prs.length), $rootScope.companyRoundingDecimalSetting);
                            item.listUtilisationDetails.push(qtyAllocation);
                        });
                    }
                    //var UtiliseDetails =
                    //{
                    //    PC_ID: item.PC_ID,
                    //    COMP_ID: $scope.compID,
                    //    PR_NUMBER: '',
                    //    PR_LINE_ITEM: '',
                    //    PO_NUMBER: '',
                    //    PO_LINE_ITEM: '',
                    //    QTY_UTILISED: item.editQuantity,
                    //    VENDOR_ID: item.vendorId,
                    //    ProductId: item.ProductId,
                    //    U_ID: $scope.userID,
                    //    ProductName: item.ProductName,
                    //    REQUIRED_QTY: item.quantity,
                    //    MODIFIED_DATE : item.DATE_MODIFIED
                    //}
                    //item.listUtilisationDetails.push(UtiliseDetails);
                    $scope.linkContractArray.push(item);
                }

            })


            $scope.linkContractArray.forEach(function (item) {
                $scope.companyGSTInfo.forEach(function (gst) {
                    if (item.vendorId == gst.vendorId && siteCode == gst.vendorSiteCode) {
                        item.VENDOR_CODE = gst.vendorCode;
                        item.VENDOR_SITE_CODE = siteCode;
                        item.PLANT_CODE = item.VENDOR_SITE_CODE ? item.VENDOR_SITE_CODE.split('_')[0] : item.VENDOR_SITE_CODE.split('_')[0];
                    }
                })
            })

            
            var params = {
                "contracts": $scope.linkContractArray,
                "sessionID": userService.getUserToken(),
            };

            PRMPRServices.linkContractToPRItems(params)
                .then(function (response) {
                    if (response.errorMessage == '') {
                        $scope.objectID = response.objectID;
                        growlService.growl("Succesfully Linked With Contracts", 'success');
                        $scope.GeneratePO();
                        //location.reload();
                    } else {
                        swal({
                            title: "Error!",
                            text: response.errorMessage,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },

                            function () {
                                location.reload();
                            });
                        //growlService.growl(response.errorMessage, 'inverse');
                        return;
                    }
                })

        };




        $scope.GeneratePO = function (cItem, qty) {
            angular.element('#potemplateSelection').modal('hide');
            //$scope.linkContract(cItem,qty);
            if ($scope.objectID > 0) {
                $scope.linkContractArray.forEach(function (item) {
                    item.price = item.contractValue
                    item.startTime = null;
                    item.endTime = null;
                    item.isValid = 1;
                })

                var params = {
                    "contracts": $scope.linkContractArray,
                    "sessionID": userService.getUserToken(),
                };

                PRMPRServices.saveVendorPOItems(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            swal({
                                title: "Done!",
                                text: "This Contract Information will be Pushed to ERP",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },

                                function () {
                                    location.reload();
                                });

                            //growlService.growl("PO Saved Succesfully", 'success');
                            //location.reload();
                        } else {
                            swal({
                                title: "Error!",
                                text: response.errorMessage,
                                type: "warning",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },

                                function () {
                                    location.reload();
                                });

                            //growlService.growl(response.errorMessage, 'inverse');
                        }
                    })
            }

        };


        $scope.validateQuantity = function (contract) {
            contract.showQtyErrorMessage = '';
            contract.isValid = false;
            if ((+contract.editQuantity + contract.availedQuantity) > contract.quantity) {
                contract.showQtyErrorMessage = 'Cannot Enter Quantity more than Total Quantity.';
                contract.isValid = true;
            }

            if (+contract.editQuantity <= 0) {
                contract.showQtyErrorMessage = 'Please enter quantity greater than zero.';
                contract.isValid = true;
            }
        };


        $scope.cancelContractRequirements = function () {
            angular.element('#potemplateSelection').modal('hide');
        }


        $scope.getvendorsbySiteCode = function () {
            $scope.vedorCompanyID = [];
            $scope.companyGSTInfo = [];
          

            let params = {
                companyId: $scope.linkContractArray[0].VENDOR_COMPANY_ID, //userService.getUserCompanyId()
                sessionid: userService.getUserToken() //userService.getUserCompanyId()
            };
            auctionsService.getCompanyGSTInfo(params)
                .then(function (response) {
                    if (response) {
                        response.forEach(function (item) {
                            item.vendorSiteStatus = item.vendorSiteStatus == 1 ? 'Active' : 'Inactive';
                        })
                        $scope.companyGSTInfo = response;
                        $scope.linkContractArray.forEach(function (item) {
                            $scope.companyGSTInfo.forEach(function (gst) {
                                if (item.vendorId == gst.vendorId && gst.vendorSiteStatus == 'Active') {
                                    var obj = {};
                                    obj.VENDOR_SITE_CODE = gst.vendorSiteCode;
                                    obj.VENDOR_CODE = gst.vendorCode
                                    item.vendorSitCodeList.push(obj)
                                }
                            })
                        });
                    }
                });
        };


        $scope.populateItems = function (cItem) {
            $scope.linkContractArray = [];

            angular.element('#potemplateSelection').modal('show');
            var obj = {};
            obj.showQtyErrorMessage = 'Please Enter the Quantity'
            obj.vendorId = cItem.vendorId;
            obj.editQuantity = 0;
            obj.contractNumber = cItem.contractNumber;
            obj.PC_ID = cItem.PC_ID;
            obj.ProductId = cItem.ProductId;
            obj.ProductName = cItem.ProductName;
            obj.REQ_ID = cItem.REQ_ID;
            obj.QCS_ID = cItem.QCS_ID;
            obj.quantity = cItem.quantity;
            obj.price = cItem.contractValue;
            obj.EXCHANGE_RATE = cItem.EXCHANGE_RATE;
            obj.companyName = cItem.companyName,
            obj.VENDOR_CODE = cItem.VENDOR_CODE,
            obj.startTime = cItem.startTime,
            obj.endTime = cItem.endTime,
            obj.availedQuantity = cItem.availedQuantity;
            obj.PLANT_CODE = cItem.PLANT_CODE;
            obj.VENDOR_SITE_CODE = cItem.SITE_CODE ? cItem.SITE_CODE : cItem.VENDOR_SITE_CODE;
            obj.vendorSitCodeList = [];
            obj.VENDOR_COMPANY_ID = cItem.VENDOR_COMPANY_ID;
            obj.ITEM_PR_NUMBER = [];
            var prs = _.uniq(cItem.PR_LINE_ITEM_INFO.split(','))
            if (prs && prs.length > 0) {
                prs.forEach(function (prItem, prIndex) {
                    var qtyAllocation =
                    {
                        PR_NUMBER : ''
                    }
                    qtyAllocation.PR_NUMBER = prItem.split('$^^$')[0];
                                        
                    obj.ITEM_PR_NUMBER.push(qtyAllocation.PR_NUMBER)
                });
            }
            if (obj.ITEM_PR_NUMBER.length > 0 && obj.ITEM_PR_NUMBER){
                obj.PR_NUMBER = (obj.ITEM_PR_NUMBER.join(', '))
            }else {
                obj.PR_NUMBER = '';
            }
            $scope.linkContractArray.push(obj);
            $scope.getvendorsbySiteCode();

        }


        $scope.isPlantModified = function (siteCodeList) {
            $scope.linkContractArray = [];
            siteCodeList.forEach(function (item) {
                $scope.companyGSTInfo.forEach(function (gst) {
                    if (item.vendorId == gst.vendorId && item.VENDOR_SITE_CODE == gst.vendorSiteCode) {
                        item.VENDOR_CODE = gst.vendorCode;
                        $scope.linkContractArray.push(item)
                    }
                })
            });
           
        
        }


        
    });