prmApp
    .service('workflowService', ["workflowDomain", "userService", "httpServices",
        function (workflowDomain, userService, httpServices) {
            //var domain = 'http://182.18.169.32/services/';
            var workflowService = this;

            workflowService.getWorkflowList = function (evalID) {
                var url = workflowDomain + 'workflows?compid=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            workflowService.getItemWorkflow = function (wfid, moduleid, module) {
                var url = workflowDomain + 'itemworkflow?wfid=' + wfid + '&moduleid=' + moduleid + '&module=' + module + '&sessionid=' + userService.getUserToken() + '&userid=' + userService.getUserId();
                return httpServices.get(url);
            };

            workflowService.updateWorkflow = function (params) {
                var url = workflowDomain + 'saveworkflow';
                var params1 = {
                    workflow: params
                }
                return httpServices.post(url, params1);
            };


            workflowService.SaveWorkflowTrack = function (params) {
                var params1 = {
                    track: params
                }
                var url = workflowDomain + 'saveworkflowtrack';
                return httpServices.post(url, params1);
            };

            workflowService.SaveApprovalTrack = function (params) {
                var params1 = {
                    track: params
                }
                var url = workflowDomain + 'saveapprovaltrack';
                return httpServices.post(url, params1);
            };
            workflowService.updateWorkflowStage = function (params) {
                var url = workflowDomain + 'saveworkflowstage';
                return httpServices.post(url, params);
            };

            workflowService.deleteWorkflow = function (params) {
                var url = workflowDomain + 'deleteworkflow';
                return httpServices.post(url, params);
            };

            workflowService.deleteWorkflowStage = function (params) {
                var url = workflowDomain + 'deleteworkflowstage';
                return httpServices.post(url, params);
            };

            workflowService.assignWorkflow = function (params) {
                var url = workflowDomain + 'assignworkflow';
                return httpServices.post(url, params);
            };

            workflowService.IsUserApproverForStage = function (params) {
                var params1 = {
                    workflow: params
                }

                var url = workflowDomain + 'isuserapproverforstage';
                return httpServices.post(url, params1);
            };

            workflowService.IsUserApproverForStage = function (approverID, userID) {
                var url = workflowDomain + 'isuserapproverforstage?approverid=' + approverID + '&userid=' + userID + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            workflowService.GetMyWorkflows = function (userid, status) {
                var url = workflowDomain + 'getmyworkflows?userid=' + userid + '&status=' + status + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            workflowService.GetMyIndents = function (userid, type) {
                var url = workflowDomain + 'getmyindents?userid=' + userid + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            workflowService.DBGetMyPendingApprovals = function (userid, type) {
                var url = workflowDomain + 'dbgetmypendingapprovals?userid=' + userid + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            workflowService.GetMyPendingApprovals = function (deptID,desigID) {
                var url = workflowDomain + 'getmypendingapprovals?userid=' + userService.getUserId() + '&deptid=' + deptID + '&desigid=' + desigID + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };

            

            workflowService.GetSeries = function (series, seriestype, deptid) {

                var url = workflowDomain + 'getseries?series=' + series + '&seriestype=' + seriestype + '&compid=' + userService.getUserCompanyId() + '&deptid=' + deptid + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };


            workflowService.SaveUserStatus = function (params) {
                var url = workflowDomain + 'saveuserstatus';
                    return httpServices.post(url, params);
            };


            workflowService.GetApprovalList = function (params) {
                var url = workflowDomain + 'getapprovallist?userid=' + params.userid + '&deptid=' + params.deptid + '&desigid=' + params.desigid + '&type=' + params.type + '&sessionid=' + userService.getUserToken();
                return httpServices.get(url);
            };


            return workflowService;
        }]);