﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class VendorCodeDetails : Entity
    {
        [DataMember(Name = "VENDOR_ID")]
        [DataNames("VENDOR_ID")]
        public int VendorId { get; set; }

        [DataMember(Name = "VENDOR_CODE")]
        [DataNames("VENDOR_CODE")]
        public string VendorCode
        {
            get; set;
        }

        [DataMember(Name = "GST_NUMBER")]
        [DataNames("GST_NUMBER")]
        public string GSTNumber
        {
            get; set;
        }

        [DataMember(Name = "GST_ADDR")]
        [DataNames("GST_ADDR")]
        public string GSTAddress
        {
            get; set;
        }

        [DataMember(Name = "STR_SUPPL1")]
        [DataNames("STR_SUPPL1")]
        public string StrSuppl1
        {
            get; set;
        }

        [DataMember(Name = "STR_SUPPL2")]
        [DataNames("STR_SUPPL2")]
        public string StrSuppl2
        {
            get; set;
        }

        [DataMember(Name = "POST_CODE1")]
        [DataNames("POST_CODE1")]
        public string PostCode1
        {
            get; set;
        }

        [DataMember(Name = "VENDOR_SITE_CODE")]
        [DataNames("VENDOR_SITE_CODE")]
        public string VENDOR_SITE_CODE
        {
            get; set;
        }

        [DataMember(Name = "VENDOR_SITE_ID")]
        [DataNames("VENDOR_SITE_ID")]
        public string VENDOR_SITE_ID
        {
            get; set;
        }

        [DataMember(Name = "U_EMAIL")]
        [DataNames("U_EMAIL")]
        public string U_EMAIL
        {
            get; set;
        }
    }
}