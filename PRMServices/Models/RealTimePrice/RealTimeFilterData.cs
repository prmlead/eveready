﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Models.RealTimePrice
{
    public class RealTimeFilterData
    {
        public string[] Categories { get; set; }

        public string[] Markets { get; set; }

        public string[] Products { get; set; }


    }
}