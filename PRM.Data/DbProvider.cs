﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace PRM.Data
{
    public class DbProvider  : IDbProvider, IDisposable
    {

        private string _connectionString ;
        private DbConnection _connection;
        //private DbProviderFactory _provider;
        private string _database = null;
        private string _providerName;
        public DbProvider()
        {

        }

        public IDbConnection Connection(string connectionString = "",string providerName = "")
        {

            try
            {
                _connectionString = string.IsNullOrEmpty(connectionString) ? ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString : connectionString;
                _providerName = string.IsNullOrEmpty(providerName) ? ConfigurationManager.ConnectionStrings["SQLConnectionString"].ProviderName : providerName;
                _database = ConfigurationManager.AppSettings["DatabaseProvider"].ToString();
                if (_database.Equals("MSSQL", StringComparison.InvariantCultureIgnoreCase))
                {
                    _connection = new SqlConnection(_connectionString);
                }
                else {
                    _connection = new MySqlConnection(_connectionString);
                }
                //_provider = DbProviderFactories.GetFactory(_providerName);
                _connection.Open();
                return _connection;
            }
            catch (Exception ex)
            {
                _connection.Close();

                return _connection;
            }

        }

        public void Dispose()
        {
            if (_connection != null)
                _connection.Close();
        }
    }
}
