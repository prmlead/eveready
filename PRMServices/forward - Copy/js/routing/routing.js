﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
            .state('view-fwd-req', {
                url: '/view-fwd-req/:Id',
                templateUrl: 'forward/views/viewFwdReq.html'
            })
            .state('save-fwd-req', {
                url: '/save-fwd-req/:Id',
                templateUrl: 'forward/views/saveFwdReq.html'
            })

        }]);