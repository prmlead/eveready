﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using PRMServices.Models;
using PRM.Core.Models.Reports;
using CORE = PRM.Core.Common;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMFwdReportService
    {
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getlivebiddingreport?reqid={reqID}&count={count}&sessionid={sessionID}")]
        LiveBidding[] GetLiveBiddingReport(int reqID, int count, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getitemwisereport?reqid={reqID}&sessionid={sessionID}")]
        ItemWiseReport[] GetItemWiseReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "deliverytimelinereport?reqid={reqID}&sessionid={sessionID}")]
        DeliveryTimeLine[] GetDeliveryTimeLineReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "paymenttermsreport?reqid={reqID}&sessionid={sessionID}")]
        DeliveryTimeLine[] GetPaymentTermsReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqdetails?reqid={reqID}&sessionid={sessionID}")]
        ReportsRequirement GetReqDetails(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "gettemplates?template={template}&compid={compID}&userid={userID}&reqid={reqID}&sessionid={sessionID}")]
        string GetTemplates(string template, int compID, int userID, int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getfwdreqreportforexcel?reqid={reqID}&sessionid={sessionID}")]
        ExcelRequirement Getfwdreqreportforexcel(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getconsolidatedreports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<ConsolidatedReport> GetConsolidatedReports(string sessionID, string from, string to, int userID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "openpr?compid={compid}&plant={plant}&purchase={purchase}&sessionid={sessionid}&exclusion={exclusion}")]
        List<OpenPR> GetOpenPR(int compid, int plant, int purchase, string sessionid, int exclusion);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "openprpivot?compid={compid}&plant={plant}&purchase={purchase}&sessionid={sessionid}&exclusion={exclusion}")]
        List<KeyValuePair> GetOpenPRPivot(int compid, int plant, int purchase, string sessionid, int exclusion);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "openpo?compid={compid}&pono={pono}&plant={plant}&purchase={purchase}&sessionid={sessionid}&exclude={exclude}")]
        List<OpenPO> GetOpenPO(int compid, string pono, int plant, int purchase, string sessionid, int exclude);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "openpopivot?compid={compid}&plant={plant}&purchase={purchase}&sessionid={sessionid}")]
        List<KeyValuePair> GetOpenPOPivot(int compid, int plant, int purchase, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "openpocomments?pono={pono}&itemno={itemno}&type={type}&sessionid={sessionid}")]
        List<Response> GetOpenPOComments(string pono, string itemno, string type, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "sapaccess?userid={userid}&sessionid={sessionid}")]
        List<Response> GetSapAccess(int userid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "lastupdatedate?table={table}&sessionid={sessionid}")]
        Response GetLastUpdatedDate(string table, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "openporeport?compid={compid}&sessionid={sessionid}")]
        List<OpenPO> GetOpenPOReport(int compid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "openprreport?compid={compid}&sessionid={sessionid}")]
        List<OpenPR> GetOpenPRReport(int compid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "senddailyreport?compid={compid}&type={type}&sessionid={sessionid}")]
        void SendDailyReport(int compid, string type, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getopenposhortagereport?compid={compid}&pono={pono}&plant={plant}&purchase={purchase}&sessionid={sessionid}&exclude={exclude}")]
        List<OpenPO> GetOpenPOShortageReport(int compid, string pono, int plant, int purchase, string sessionid, int exclude);

        [WebGet(RequestFormat = WebMessageFormat.Json, 
            ResponseFormat = WebMessageFormat.Json, 
            UriTemplate = "getopenprshortagereport?compid={compid}&plant={plant}&purchase={purchase}&sessionid={sessionid}&exclusion={exclusion}")]
        List<OpenPR> GetOpenPRShortageReport(int compid, int plant, int purchase, string sessionid, int exclusion);

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savepocomments")]
        Response SavePOComments(string pono, string itemno, double quantity, string comments, 
            DateTime newdeliverdate, string type, int userid, string materialdescription, string apiname, string sitename, 
            int compid, int plant, int purchase, string sessionid, OpenPR openpr, OpenPO openpo,string category, string status);


        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatepostatus")]
        Response UpdatePoStatus(string pono, string itemno, string sessionid, string status, int userid);


        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "importentity")]
        Response ImportEntity(ImportEntity entity);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getconsolidatedtemplates?template={template}&from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        string GetConsolidatedTemplates(string template, string from, string to, int userID, string sessionID);
    }
}
