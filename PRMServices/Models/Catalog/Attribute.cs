﻿using System.Runtime.Serialization;
using PRM.Core.Models;

namespace PRMServices.Models.Catalog
{
    [DataContract]
    public class AttributeModel : EntityExt
    {
        [DataMember(Name = "attrId")]
        public int AttrId { get; set; }

        [DataMember(Name = "attrName")]
        public string AttrName { get; set; }

        [DataMember(Name = "attrDesc")]
        public string AttrDesc { get; set; }

        [DataMember(Name = "attrDataType")]
        public string AttrDataType { get; set; }

        [DataMember(Name = "attrOptions")]
        public string AttrOptions { get; set; }

        [DataMember(Name = "u_id")]
        public int U_Id { get; set; }

        [DataMember(Name = "companyId")]
        public int CompanyId { get; set; }

        [DataMember(Name = "attrOrder")]
        public int AttrOrder { get; set; }

        [DataMember(Name = "attrDefaultValue")]
        public string AttrDefaultValue { get; set; }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }


    }

    [DataContract]
    public class PropertyModel : EntityExt
    {
        [DataMember(Name = "propId")]
        public int propId { get; set; }

        [DataMember(Name = "propName")]
        public string propName { get; set; }

        [DataMember(Name = "propDesc")]
        public string propDesc { get; set; }

        /// <summary>
        /// 0 - product
        /// 1- Category
        /// </summary>
        [DataMember(Name = "propType")]
        public int propType { get; set; }

        [DataMember(Name = "propDataType")]
        public string propDataType { get; set; }

        [DataMember(Name = "propValue")]
        public string propValue { get; set; }

        [DataMember(Name = "propOptions")]
        public string propOptions { get; set; }

        [DataMember(Name = "user")]
        public int U_Id { get; set; }

        [DataMember(Name = "companyId")]
        public int CompanyId { get; set; }

        [DataMember(Name = "propDefaultValue")]
        public string propDefaultValue { get; set; }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }


    }

    [DataContract]
    public class PropertyEntityModel : EntityExt
    {
        [DataMember(Name = "entityId")]
        public int entityId { get; set; }

        [DataMember(Name = "companyId")]
        public int companyId { get; set; }

        [DataMember(Name = "propId")]
        public int propId { get; set; }

        [DataMember(Name = "propValue")]
        public string propValue { get; set; }

        [DataMember(Name = "user")]
        public int U_Id { get; set; }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }
    }
}


