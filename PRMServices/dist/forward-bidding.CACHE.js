﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('fwd-save-requirement', {
                    url: '/fwd-save-requirement/:Id',
                    templateUrl: 'forward-bidding/views/save-fwd-requirement.html',
                    params: {
                        reqObj: null,
                        prDetail: null,
                        prDetailsList: null,
                        prItemsList: null,
                        selectedTemplate: null,
                        selectedPRNumbers: null
                    }
                })
                .state('pages.profile.fwd-requirements', {
                    url: '/fwd-requirements',
                    templateUrl: 'forward-bidding/views/fwd-requirements.html',
                    params: { filters: null },
                    onEnter: function (store, $state) {
                        if (!(store.get('sessionid') && store.get('verified') && store.get('emailverified'))) { 
                            $state.go('pages.profile.profile-about');
                            swal("Warning", "Please verify your Credential documents/OTP", "warning");
                        }
                    },
                    onExit: function () {
                        //write code when you change state
                        ////console.log('exit');
                    },
                    resolve: {
                        loadPlugin: function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: 'css',
                                    insertBefore: '#app-level',
                                    files: [
                                        'vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css'
                                    ]
                                },
                                {
                                    name: 'vendors',
                                    files: [
                                        'vendors/bower_components/ofline/offline.js',
                                        'vendors/bower_components/lightgallery/light-gallery/js/lightGallery.min.js'
                                    ]
                                }
                            ]);
                        }
                    }
                }).state('pages.profile.fwd-vendor-leads', {
                    url: '/fwd-vendor-leads',
                    templateUrl: 'forward-bidding/views/fwd-vendor-leads.html',
                    onEnter: function (store, $state) {
                        ////console.log('entry');
                        if (store.get('sessionid') && store.get('verified') && store.get('emailverified')) {
                            ////console.log('verified user');
                        } else {
                            $state.go('pages.profile.profile-about');
                            swal("Warning", "Please verify your Credential documents/OTP", "warning");
                        }
                    },
                    onExit: function () {
                        //write code when you change state
                        ////console.log('exit');
                    },
                    resolve: {
                        loadPlugin: function ($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: 'css',
                                    insertBefore: '#app-level',
                                    files: [
                                        'vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css'
                                    ]
                                },
                                {
                                    name: 'vendors',
                                    files: [
                                        'vendors/bower_components/ofline/offline.js',
                                        'vendors/bower_components/lightgallery/light-gallery/js/lightGallery.min.js'
                                    ]
                                }
                            ]);
                        }
                    }
                }).state('fwd-consalidatedReport', {
                    url: '/fwd-consalidatedReport',
                    templateUrl: 'forward-bidding/views/fwd-consalidated-report.html'
                }).state('fwd-comparatives', {
                    url: '/fwd-comparatives/:reqID',
                    templateUrl: 'forward-bidding/views/fwd-comparatives.html'
                }).state('fwd-req-savingsPreNegotiation', {
                    url: '/fwd-req-savingsPreNegotiation/:Id',
                    templateUrl: 'forward-bidding/views/fwd-req-savingsPreNegotiation.html'
                }).state('fwd-req-savings', {
                    url: '/fwd-req-savings/:Id',
                    templateUrl: 'forward-bidding/views/fwd-req-savings.html'
                }).state('fwd-reqTechSupport', {
                    url: '/fwd-reqTechSupport/:reqId',
                    templateUrl: 'forward-bidding/views/fwd-reqTechSupport.html'
                }).state('fwd-reminders', {
                    url: '/fwd-reminders/:Id',
                    templateUrl: 'forward-bidding/views/fwd-reminders.html'
                }).state('fwd-reports', {
                    url: '/fwd-reports/:reqID',
                    templateUrl: 'forward-bidding/views/fwd-reports.html'
                }).state('fwd-bidhistory', {
                    url: '/fwd-bidhistory/:Id/:reqID/:isfwd',
                    templateUrl: 'forward-bidding/views/fwd-bidHistory.html'
                }).state('fwd-audit', {
                    url: '/fwd-audit/:reqID',
                    templateUrl: 'forward-bidding/views/fwd-audit.html'
                });
        }]);prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('fwdAuditCtrl', ["$state", "$stateParams", "$scope", "PRMForwardBidService", "userService",
        "$http", "$window", "domain", "fileReader", "growlService", "$log", "$filter", "ngDialog",
        function ($state, $stateParams, $scope, PRMForwardBidService, userService,
            $http, $window, domain, fileReader, growlService, $log, $filter, ngDialog) {
            $scope.auditLogs = [];
            $scope.reqId = $stateParams.reqID;
            $scope.requirementDetails = {};
            $scope.auditData = [];
            $scope.auditDetails = [];
            $scope.filteredDetails = [];
            $scope.getAuditData = function () {
                if ($scope.reqId) {
                    PRMForwardBidService.getrequirementaudit($scope.reqId)
                        .then(function (response) {
                            console.log(response);
                            if (response.errorMessage) {
                                console.log(response.errorMessage);
                            } else {
                                //$scope.createAuditTable(response);
                                $scope.auditData = response[0].Value;
                                $scope.auditDetails = response[1].Value;
                            }

                        });
                }
            };

            $scope.getAuditData();

            $scope.getAuditDetails = function (audit) {
                $scope.auditData.forEach(function (audit1, vI) {
                    audit1.expanded = false;
                });

                audit.expanded = true;
                $scope.filteredDetails = _.filter($scope.auditDetails, function (detail) {
                    return detail.auditVersion === audit.auditVersion;
                });

                if ($scope.filteredDetails) {
                    $scope.filteredDetails.forEach(function (val, vI) {
                        val.dateCreated1 = userService.toLocalDate(val.dateCreated);
                    });
                } else {
                    $scope.filteredDetails = [];
                }
            };

            

            $scope.getRequirementDetails = function () {
                PRMForwardBidService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        if (response) {
                            $scope.requirementDetails = response;
                        }
                    });
            };
            
            $scope.getRequirementDetails();

        }]);﻿prmApp

    .controller('fwdBidHistoryCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$log", "$stateParams", "$timeout", "userService", "SignalRFactory", "fileReader", "growlService", "PRMForwardBidService",
        function ($scope, $http, $state, domain, $filter, $log, $stateParams, $timeout, userService, SignalRFactory, fileReader, growlService, PRMForwardBidService) {
            $scope.bidhistory = {};
            $scope.bidhistory.uID = $stateParams.Id;
            $scope.bidhistory.reqID = $stateParams.reqID;
            $log.info($stateParams.isfwd);
            $scope.isForwardBidding = $stateParams.isfwd == 'true' ? true : false;

            $scope.userID = userService.getUserId();
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

            $scope.bidHistory = {};
            $scope.CovertedDate = '';
            $scope.Name = 'No previous bids';

            $scope.show = false;
            $scope.show1 = false;

            $scope.data = [];
            $scope.categories = [];
            $scope.auctionItem = [];

            $scope.renderChart = function (response) {
                $scope.bidHistory = response;

                if ($scope.bidHistory.length > 0) {
                    $scope.Name = $scope.bidHistory[0].firstName + ' ' + $scope.bidHistory[0].lastName;
                }

                $scope.bidHistory.forEach(function (item, index) {
                    $scope.data.push(item.bidAmount);
                    item.createdTime = userService.toLocalDate(item.createdTime);
                    //var time = new moment(item.createdTime).format("HH:mm");
                    $scope.categories.push(item.createdTime.toString());
                });

                $scope.startTime = $scope.bidHistory[0].createdTime;
                $scope.endTime = $scope.bidHistory[$scope.bidHistory.length - 1].createdTime;

                $scope.chartOptions = {
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: 'Requirement Bid History Graph'
                    },
                    xAxis: {
                        categories: $scope.categories,
                        title: {
                            text: 'Time ( Start Time: ' + $scope.startTime + ' - End Time: ' + $scope.endTime + ')'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Bid Price'
                        }
                    },

                    series: [{
                        name: $scope.Name,
                        data: $scope.data


                    }]
                };

                $scope.show = true;
            };

            $scope.GetBidHistory = function () {
                PRMForwardBidService.GetBidHistory({ "reqid": $scope.bidhistory.reqID, 'userid': $scope.bidhistory.uID, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.renderChart(response);
                    });
            };

            $scope.GetBidHistory();

            $scope.GetDateconverted = function (dateBefore) {

                var date = dateBefore.split('+')[0].split('(')[1];
                var newDate = new Date(parseInt(parseInt(date)));
                $scope.CovertedDate = newDate.toString().replace('Z', '');
                return $scope.CovertedDate;

            };

            $scope.getData = function () {
                PRMForwardBidService.getrequirementdata({ "reqid": $scope.bidhistory.reqID, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        if (response) {
                            $scope.auctionItem = response;
                        }
                    });
            };

            $scope.getData();

            $scope.CBPricesAudit = {};

            $scope.GetCBPricesAudit = function () {
                PRMForwardBidService.GetCBPricesAudit($scope.bidhistory.reqID, $scope.bidhistory.uID, userService.getUserToken())
                    .then(function (response) {
                        $scope.CBPricesAudit = response;

                        $scope.CBPricesAudit.forEach(function (history, historyIndex) {

                            if ($scope.isCustomer) {
                                if ($scope.bidhistory.uID == history.CREATED_BY) {
                                    history.isTargetBid = true;
                                } else {
                                    history.isTargetBid = false;
                                }
                            }
                            else {
                                if ($scope.userID != history.CREATED_BY) {
                                    history.isTargetBid = true;
                                } else {
                                    history.isTargetBid = false;
                                }
                            }
                        });
                    });
            };

            $scope.GetCBPricesAudit();

            $scope.ProductFilter = [];
            $scope.ItemrenderChart = function (response, itemID, SelectedItem) {
                $scope.categories1 = [];
                if (SelectedItem) {
                    if (SelectedItem.isChecked) {
                        $scope.ProductFilter.push(SelectedItem);
                        $scope.categories1 = [];
                        $scope.itemLevelOptions.series = [];
                        $scope.show1 = false;
                    } else {
                        var tempIdx = $scope.ProductFilter.indexOf(SelectedItem);
                        if (tempIdx > -1) {
                            $scope.ProductFilter.splice(tempIdx, 1);
                        }
                    }
                }

                $scope.displaybidHistory = [];
                $scope.ItembidHistory = response;
                $scope.ItembidHistoryTemp = angular.copy(response);
                $scope.ItembidHistoryTemp.forEach(function (item, itemIdx) {
                    if ($scope.ProductFilter.length > 0) {
                        $scope.ProductFilter.forEach(function (product) {
                            if (product.itemID == item.itemID) {
                                item.bidHistory.forEach(function (bid) {
                                    // bid.createdTime = new moment(bid.createdTime).format("DD-MM-YYYY HH:mm");
                                    bid.createdTime = userService.toLocalDate(bid.createdTime);
                                    $scope.displaybidHistory.push(bid);
                                    $scope.categories1.push(bid.createdTime);
                                });
                            }
                        });
                    } else {
                        if (item && item.bidHistory.length > 0) {
                            item.bidHistory.forEach(function (bid) {
                                // bid.createdTime = new moment(bid.createdTime).format("DD-MM-YYYY HH:mm");
                                bid.createdTime = userService.toLocalDate(bid.createdTime);
                                $scope.displaybidHistory.push(bid);
                                $scope.categories1.push(bid.createdTime);
                            });
                        }
                    }
                });

                $scope.displaybidHistory = $scope.displaybidHistory.sort(function (a, b) {
                    a = new Date(a.createdTime);
                    b = new Date(b.createdTime);
                    return a - b;
                });

                $scope.categories1 = $scope.categories1 ? $scope.categories1.sort() : '';
                $scope.startTime = $scope.categories1[0] ? $scope.categories1[0] : '';
                $scope.endTime = $scope.categories1 ? $scope.categories1[$scope.categories1.length - 1] : '';
                $scope.itemLevelOptions = {
                    credits: {
                        enabled: false
                    },
                    options: {
                        chart: {
                            type: 'line'
                        }
                    },
                    series: [],
                    title: {
                        text: 'Item Bid History Graph'
                    },
                    xAxis: {
                        categories: $scope.categories1,
                        title: {
                            text: 'Time ( Start Time: ' + $scope.startTime + ' - End Time: ' + $scope.endTime + ')'
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Item Bid Price'
                        }
                    },
                    plotOptions: {
                        line: {
                            dataLabels: {
                                enabled: false
                            },
                            enableMouseTracking: true
                        }
                    }
                }

                $scope.ItembidHistory.forEach(function (item, index) {

                    if ($scope.ProductFilter.length > 0) {
                        $scope.ProductFilter.forEach(function (product) {
                            if (product.itemID == item.itemID) {
                                var data = [];

                                item.bidHistory.forEach(function (bid, bidIdx) {
                                    data.push(bid.bidAmount);
                                });


                                $scope.itemLevelOptions.series.push({

                                    name: item.productIDorName,
                                    data: data
                                });
                            }
                        });
                    } else {
                        var data = [];

                        item.bidHistory.forEach(function (bid, bidIdx) {
                            data.push(bid.bidAmount);
                        });


                        $scope.itemLevelOptions.series.push({

                            name: item.productIDorName,
                            data: data
                        });
                    }


                });

                if ($scope.ItembidHistory.length > 0) {
                    $scope.Name1 = $scope.ItembidHistory[0].bidHistory[0] ? $scope.ItembidHistory[0].bidHistory[0].companyName : '';
                }

                $scope.show1 = true;
            };



            $scope.bidresponse = [];
            $scope.bidresponsetemp = [];
            $scope.GetItemBidHistory = function () {
                PRMForwardBidService.GetItemBidHistory({ "reqid": $scope.bidhistory.reqID, 'userid': $scope.bidhistory.uID, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.bidresponse = response;
                        $scope.bidresponsetemp = angular.copy(response);
                        $scope.ItemrenderChart(response, false, '');
                    });
            };

            $scope.GetItemBidHistory();
            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };

        }]);﻿prmApp
    .controller('fwdComparativesCtrl', ["$scope", "$state", "$log", "$stateParams", "userService", "$window",
        "$timeout", "PRMCustomFieldService", "PRMForwardBidService",
        function ($scope, $state, $log, $stateParams, userService, $window, $timeout, PRMCustomFieldService, PRMForwardBidService) {
            $scope.reqId = $stateParams.reqID;
            $scope.isUOMDifferent = false;
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
            if (!$scope.isCustomer) {
                $state.go('home');
            }

            $scope.userId = userService.getUserId();
            $scope.sessionid = userService.getUserToken();
            $scope.listRequirementTaxes = [];
            $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
            $scope.uomDetails = [];
            $scope.requirementSettings = [];
            $scope.selectedTemplate = {};
            $scope.prmFieldMappingDetails = {};
            $scope.subIemsColumnsVisibility = {
                hideSPECIFICATION: true,
                hideQUANTITY: true,
                hideTAX: true,
                hidePRICE: true
            };



            $scope.handleUOMItems = function (item) {
                if (item) {
                    item.reqVendors.forEach(function (vendor, index) {
                        var uomDetailsObj = {
                            itemId: item.itemID,
                            vendorId: vendor.vendorID,
                            containsUOMItem: false
                        };

                        if (vendor.quotationPrices) {
                            uomDetailsObj.containsUOMItem = item.productQuantityIn.toUpperCase() != vendor.quotationPrices.vendorUnits.toUpperCase();
                        }

                        $scope.uomDetails.push(uomDetailsObj);
                    });
                }
            };

            $scope.containsUOMITems = function (vendorId) {
                var filteredUOMitems = _.filter($scope.uomDetails, function (uomItem) { return uomItem.vendorId == vendorId && uomItem.containsUOMItem == true; });
                if (filteredUOMitems && filteredUOMitems.length > 0) {
                    return true;
                }

                return false;
            };

            $scope.ReqReportForExcel = [];
            $scope.GetReqReportForExcel = function () {
                PRMForwardBidService.GetReqReportForExcel($scope.reqId, userService.getUserToken())
                    .then(function (response) {
                        $scope.ReqReportForExcel = response;
                        $scope.ReqReportForExcel.currentDate = userService.toLocalDate($scope.ReqReportForExcel.currentDate);
                        $scope.ReqReportForExcel.postedOn = userService.toLocalDate($scope.ReqReportForExcel.postedOn);
                        $scope.ReqReportForExcel.startTime = userService.toLocalDate($scope.ReqReportForExcel.startTime);
                        $scope.ReqReportForExcel.reqItems.forEach(function (item, index) {
                            $scope.handleUOMItems(item);
                            if (item.reqVendors && item.reqVendors.length > 0) {
                                item.reqVendors.forEach(function (vendor, index) {
                                    if (vendor.quotationPrices.productQuotationTemplateJson !== '') {
                                        vendor.quotationPrices.productQuotationTemplateArray1 = JSON.parse(vendor.quotationPrices.productQuotationTemplateJson);
                                    }
                                });
                            }
                        });
                    });
            };

            $scope.GetReqReportForExcel();
            $scope.doPrint = false;
            $scope.printReport = function () {
                $scope.doPrint = true;
                $timeout(function () {
                    $window.print();
                    $scope.doPrint = false
                }, 1000);
            };


            $scope.htmlToCanvasSaveLoading = false;

            $scope.htmlToCanvasSave = function (format) {
                $scope.htmlToCanvasSaveLoading = true;
                setTimeout(function () {
                    try {
                        var name = "Comparisions-ReqID-" + $scope.reqId + "." + format;
                        var canvas = document.createElement("canvas");
                        if (format == 'pdf') {
                            document.getElementById("widget").style["display"] = "";
                        }
                        else {
                            document.getElementById("widget").style["display"] = "inline-block";
                        }

                        html2canvas($("#widget"), {
                            onrendered: function (canvas) {
                                theCanvas = canvas;

                                const a = document.createElement("a");
                                a.style = "display: none";
                                a.href = canvas.toDataURL();

                                // Add Image to HTML
                                //document.body.appendChild(canvas);

                                /* Save As PDF */
                                if (format == 'pdf') {
                                    var imgData = canvas.toDataURL();
                                    var pdf = new jsPDF();
                                    pdf.addImage(imgData, 'JPEG', 0, 0);
                                    pdf.save(name);
                                }
                                else {
                                    a.download = name;
                                    a.click();
                                }

                                // Clean up 
                                //document.body.removeChild(canvas);
                                document.getElementById("widget").style["display"] = "";
                                $scope.htmlToCanvasSaveLoading = false;
                            }
                        });
                    }
                    catch (err) {
                        document.getElementById("widget").style["display"] = "";
                        $scope.htmlToCanvasSaveLoading = false;
                    }
                    finally {

                    }

                }, 500);

            };

            $scope.getTotalTax = function (quotation) {
                if (quotation) {
                    if (!quotation.cGst || quotation.cGst == undefined) {
                        quotation.cGst = 0;
                    }

                    if (!quotation.sGst || quotation.sGst == undefined) {
                        quotation.sGst = 0;
                    }

                    if (!quotation.iGst || quotation.iGst == undefined) {
                        quotation.iGst = 0;
                    }

                    return quotation.cGst + quotation.sGst + quotation.iGst;

                } else {
                    return 0;
                }
            };

            $scope.isOnlySpecificationTemplate = function (productQuotationTemplateArray) {
                var isOnlySpecificatoin = true;
                if (productQuotationTemplateArray.length > 0) {
                    var items = _.filter(productQuotationTemplateArray, function (item) { return item.HAS_PRICE; });
                    if (items && items.length > 0) {
                        isOnlySpecificatoin = false;
                    }
                }

                return isOnlySpecificatoin;
            };

            $scope.handleSubItemsVisibility = function (productQuotationTemplateArray) {
                if (productQuotationTemplateArray && productQuotationTemplateArray.length > 0) {
                    var items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_SPECIFICATION === 1);
                    });

                    if (items && items.length > 0) {
                        $scope.subIemsColumnsVisibility.hideSPECIFICATION = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_QUANTITY === 1);
                    });

                    if (items && items.length > 0) {
                        $scope.subIemsColumnsVisibility.hideQUANTITY = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_TAX === 1);
                    });

                    if (items && items.length > 0) {
                        $scope.subIemsColumnsVisibility.hideTAX = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_PRICE === 1);
                    });

                    if (items && items.length > 0) {
                        $scope.subIemsColumnsVisibility.hidePRICE = false;
                    }
                }
            };



            $scope.detectLinks = function urlify(text) {
                var urlRegex = /(https?:\/\/[^\s]+)/g;
                return text.replace(urlRegex, function (url) {
                    return '<a target="_blank" href="' + url + '">' + url + '</a>';
                });
            };


            $scope.getRequirementSettings = function () {
                $scope.requirementSettings = [];
                $scope.selectedTemplate.TEMPLATE_NAME = 'PRM_DEFAULT';
                PRMForwardBidService.getRequirementSettings({ "reqid": $scope.reqId, "sessionid": $scope.sessionid })
                    .then(function (response) {
                        $scope.requirementSettings = response;
                        if ($scope.requirementSettings && $scope.requirementSettings.length > 0) {
                            var template = $scope.requirementSettings.filter(function (setting) {
                                return setting.REQ_SETTING === 'TEMPLATE_ID';
                            });

                            if (template && template.length > 0) {
                                $scope.selectedTemplate.TEMPLATE_ID = template[0].REQ_SETTING_VALUE;
                            }
                        }

                        if ($scope.selectedTemplate.TEMPLATE_ID || $scope.selectedTemplate.TEMPLATE_NAME) {
                            $scope.GetPRMTemplateFields();
                        }
                    });
            };

            $scope.getRequirementSettings();

            $scope.GetPRMTemplateFields = function () {
                $scope.prmFieldMappingDetails = {};
                var params = {
                    "templateid": $scope.selectedTemplate.TEMPLATE_ID ? $scope.selectedTemplate.TEMPLATE_ID : 0,
                    "templatename": $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : '',
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        $scope.prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });
                });
            };
        }]);﻿
prmApp
    .controller('fwdConsalidatedReportCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "PRMForwardBidService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, $http, domain, $rootScope, fileReader, $filter, $log, PRMForwardBidService) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;
            $scope.consalidatedReport = [];

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 8;
            $scope.revisedUserL1 = [];


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
            };

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            //$scope.reportFromDate = '';
            //$scope.reportToDate = '';

            $scope.reportToDate = moment().format('YYYY-MM-DD');
            $scope.reportFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            $scope.getConsalidatedReport = function () {
                $scope.errMessage = '';

                PRMForwardBidService.getConsolidatedReport($scope.reportFromDate, $scope.reportToDate)
                    .then(function (response) {
                        $scope.consalidatedReport = response;

                        $scope.totalItems = $scope.consalidatedReport.length;
                        $scope.consalidatedReport.forEach(function (item, index) {
                            item.closed = $scope.prmStatus($scope.isCustomer, item.closed);
                            item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                            item.reqPostedOn = $scope.GetDateconverted(item.reqPostedOn);
                            item.startTime = $scope.GetDateconverted(item.startTime);

                            if (String(item.startTime).includes('9999')) {
                                item.startTime = '';
                            }
                        });
                    });
            };

            $scope.getConsalidatedReport();

            $scope.GetReport = function () {

                alasql('SELECT requirementNumber as [Requirement Number],title as [Requirement Title],prNumbers as [PR Numbers],reqCategory as [Category], ' +
                    'closed as Status, ' +
                    'reqPostedOn as [Posted On], quotationFreezTime as [Closed bid deadline], startTime as [Scheduled],IL1_vendTotalPrice as [Initial Least Price], ' +
                    'RL1_companyName as [L1 Company Name],  RL1_revVendTotalPrice as [L1 Rev Price], ' +
                    'RL2_companyName as [L2 Company Name],  RL2_revVendTotalPrice as [L2 Rev Price],  ' +
                    'basePriceSavings as [Earnings],savingsPercentage as [Earnings %] ' +
                    'INTO XLSX(?, { headers: true, sheetid: "ConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                    ["ConsolidatedReport.xlsx", $scope.consalidatedReport]);
            };

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };
        }]);﻿prmApp

    // =========================================================================
    // AUCTION ITEM
    // =========================================================================

    .controller('fwdItemCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRFwdHubName", "ngDialog",
        "$window", "PRMLotReqService", "PRMCustomFieldService", "workflowService", "PRMSurrogateBidService",
        "PRMForwardBidService",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRFwdHubName, ngDialog, $window,
            PRMLotReqService, PRMCustomFieldService, workflowService, PRMSurrogateBidService, PRMForwardBidService) {
            var liveId = "";
            $scope.tempScopeVariables = {};
            $scope.selectedTemplate = {};
            $scope.prmFieldMappingDetails = {};
            $scope.companyItemUnits = [];
            $scope.requirementSettings = [];
            $scope.selectedcustomFieldList = [];
            var id = $stateParams.Id;
            $scope.reqId = $stateParams.Id;
            $scope.IS_CB_ENABLED = false;
            $scope.IS_CB_NO_REGRET = false;
            $scope.selectedVendorForItemLevelPrices = {};
            $scope.currentUserId = +userService.getUserId();
            $scope.subUsers = [];
            $scope.disableSubmitAllItemApproval = true;
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.vendorIncoTerms = {};
            $scope.requirementPRStatus = {
                MESSAGE: '',
                showStatus: false,
                validPRs: ''
            };
            $scope.companyINCOTerms = [];
            $scope.localGSTNumberCode = '36';
            $scope.disableLocalGSTFeature = false;
            $scope.overallPercentage = 0;
            $scope.itemCeilingVendor = 0;
            $scope.isAuctionScheduled = false;
            $scope.rankLevelChanged = false;
            $scope.reductionLevelChanged = false;
            $scope.isTaxesMandatory = true;

            if ($scope.reqId > 0) {
            }
            else {
                $scope.reqId = $stateParams.reqID;
                $stateParams.Id = $stateParams.reqID;
            }

            $scope.overAllValue = {
                overallPercentage: 0,
                overallDiscountPercentage: 0
            };

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.customerType = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            $scope.regretDetails = [];
            $scope.isCustomer = $scope.customerType == "CUSTOMER" ? true : false;
            $scope.listRequirementItemsTemp = [];
            //Pagination
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage = 5;
            $scope.itemsPerPage2 = 5;
            $scope.maxSize = 8;
            //^Pagination

            $scope.currentSessionId = userService.getUserToken();
            $scope.currentUserCompID = userService.getUserCompanyId();
            //#CB-0-2018-12-05
            $scope.goToCbVendor = function (vendorID) {
                $state.go('cb-vendor', { 'reqID': $stateParams.Id, 'vendorID': vendorID });
            };


            $scope.goToAudit = function () {
                var url = $state.href("fwd-audit", { "reqID": $scope.reqId });
                window.open(url, '_blank');
            };
            $scope.specificationData = [];
            $scope.numberOfItemDetails = 1;
            $scope.signalRCustomerAccess = false;
            $scope.Loding = false;
            $scope.makeaBidLoding = false;
            $scope.showTimer = false;
            $scope.userIsOwner = false;
            $scope.sessionid = $scope.currentSessionId;
            $scope.allItemsSelected = true;
            $scope.RevQuotationfirstvendor = "";
            $scope.disableBidButton = false;
            $scope.nonParticipatedMsg = '';
            $scope.quotationRejecteddMsg = '';
            $scope.quotationNotviewedMsg = '';
            $scope.revQuotationRejecteddMsg = '';
            $scope.revQuotationNotviewedMsg = '';
            $scope.quotationApprovedMsg = '';
            $scope.revQuotationApprovedMsg = '';
            $scope.reduceBidAmountNote = '';
            $scope.incTaxBidAmountNote = '';
            $scope.noteForBidValue = '';
            $scope.CSGSTFields = false;
            $scope.IGSTFilelds = false;
            $scope.disableDecreaseButtons = true;
            $scope.disableAddButton = true;
            $scope.NegotiationEnded = false;
            $scope.uploadQuotationButtonMsg = 'The Submit Quotation option would be available only if prices are entered above.';
            $scope.uploadQuotationTaxValidationMsg = 'Please check all Tax Fields';
            $scope.currentID = -1;
            $scope.timerStyle = { 'color': '#000' };
            $scope.savingsStyle = { 'color': '#228B22' };
            $scope.restartStyle = { 'color': '#f00' };
            $scope.bidAttachement = [];
            $scope.bidAttachementName = "";
            $scope.bidAttachementValidation = false;
            $scope.bidPriceEmpty = false;
            $scope.bidPriceValidation = false;
            $scope.showStatusDropDown = false;
            $scope.vendorInitialPrice = 0;
            $scope.showGeneratePOButton = false;
            $scope.isDeleted = false;
            $scope.ratingForVendor = 0;
            $scope.ratingForCustomer = 0;
            $scope.participatedVendors = [];
            $scope.vendorApprovals = [];
            $scope.companyGSTInfo = [];
            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.workflowObj = {};
            $scope.workflowObj.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'VENDOR';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/
            $scope.surrogateObj = {};
            $scope.surrogateUsers = [];
            $scope.filteredSubUsers = [];
            $scope.selectedSurrogateVendor = {};
            $scope.showCurrencyRefresh = false;
            $scope.paymentPopUp = false;
            var requirementHub = SignalRFactory('', signalRFwdHubName);
            $scope.auctionItem = {
                auctionVendors: [],
                listRequirementItems: [],
                listRequirementTaxes: []
            };

            $scope.auctionItemTemporary = {
                emailLinkVendors: []
            };

            $scope.checkConnection = function () {
                if (requirementHub) {
                    return requirementHub.getStatus();
                } else {
                    return 0;
                }
            };
            $scope.auctionItemVendor = [];
            $scope.reconnectHub = function () {
                if (requirementHub) {
                    if (requirementHub.getStatus() == 0) {
                        requirementHub.reconnect();
                        return true;
                    }
                } else {
                    requirementHub = SignalRFactory('', signalRFwdHubName);
                }
            };

            $scope.invokeSignalR = function (methodName, params, callback) {
                if ($scope.checkConnection() == 1) {
                    requirementHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    });
                } else {
                    $scope.reconnectHub();
                    requirementHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    });
                }
            };

            /*Chat hub*/
            requirementHub.connection.start().done(function () {
                let signalRLotGroupName = '';
                let signalRGroupName = 'requirementGroup' + $stateParams.Id + '_' + $scope.currentUserId;
                signalRLotGroupName = 'requirementGroup_LOT_' + $scope.currentUserId;

                if ($scope.isCustomer) {
                    signalRGroupName = 'requirementGroup' + $stateParams.Id + '_CUSTOMER';
                    signalRLotGroupName = 'requirementGroup_LOT_' + 'CUSTOMER';
                }

                $scope.invokeSignalR('joinGroup', signalRGroupName);
                $scope.invokeSignalR('joinGroup', 'chatGroup' + $scope.reqId);

                if (signalRLotGroupName) {
                    $scope.invokeSignalR('joinGroup', signalRLotGroupName);
                }
            });

            requirementHub.on('chatUpdate', function (payLoad) {
                if (payLoad && !payLoad.IsCustomer) {
                    toastr.info(payLoad.FromName + ' left a message: ' + payLoad.Message);
                }
                if (payLoad && payLoad.IsCustomer && payLoad.ToId === $scope.currentUserId) {
                    toastr.info('Customer left a message: ' + payLoad.Message);
                }
            });
            /*^Chat Hub*/

            $scope.setOptions = function () {
                toastr.options.positionClass = "toast-top-right";
                toastr.options.closeButton = true;
                toastr.options.showMethod = 'slideDown';
                toastr.options.hideMethod = 'slideUp';
                //toastr.options.newestOnTop = false;
                toastr.options.progressBar = true;
            };

            $scope.setOptions();



            var requirementData = {};
            $scope.quotationAttachment = null;
            $scope.days = 0;
            $scope.hours = 0;
            $scope.mins = 0;
            $scope.NegotiationSettings = {
                negotiationDuration: ''
            };
            $scope.divfix = {};
            $scope.divfix3 = {};
            $scope.divfix1 = {};
            $scope.divfix2 = {};
            $scope.divfixMakeabid = {};
            $scope.divfixMakeabidError = {};
            $scope.boxfix = {};
            $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
            $scope.auctionItem.reqType = 'REGULAR';
            $scope.auctionItem.priceCapValue = 0;
            $scope.auctionItem.isUnitPriceBidding = 1;
            $scope.$on("$destroy", function () {
                $log.info('disconecting signalR');
                requirementHub.stop();
                $log.info('disconected signalR');
            });
            $scope.clickToOpen = function () {
                ngDialog.open({ template: 'login/termsUpdateStartTime.html', width: 1000, height: 500 });
            };

            $scope.updateTimeLeftSignalR = function () {
                var parties = id + "$" + $scope.currentUserId + "$" + "10000" + "$" + $scope.currentSessionId;
                $scope.invokeSignalR('UpdateTime', parties);
            };

            $scope.subIemsColumnsVisibility = {
                hideSPECIFICATION: true,
                hideQUANTITY: true,
                hideTAX: true,
                hidePRICE: true
            };

            $scope.newPriceToBeQuoted = 0;

            $scope.reduceBidAmount = function () {
                //if (!isNaN($scope.reduceBidValue) && $scope.reduceBidValue != "" && $scope.reduceBidValue >= 0 && $scope.reduceBidValue > $scope.auctionItem.minPrice) {
                if (!isNaN($scope.reduceBidValue) && $scope.reduceBidValue != "" && $scope.reduceBidValue >= 0) {                
                    $("#reduceBidValue").val($scope.reduceBidValue);
                    $("#makebidvalue").val($scope.precisionRound(parseFloat($scope.auctionItem.minPrice + $scope.reduceBidValue), $rootScope.companyRoundingDecimalSetting));
                    $("#reduceBidValue1").val($scope.precisionRound(parseFloat($scope.auctionItem.minPrice + $scope.reduceBidValue), $rootScope.companyRoundingDecimalSetting));
                }
                else {
                    $("#makebidvalue").val($scope.auctionItem.minPrice);
                    $("#reduceBidValue1").val($scope.auctionItem.minPrice);
                    $scope.reduceBidValue = 0;
                    $("#reduceBidValue").val($scope.reduceBidValue);
                }
            };

            $scope.bidAmount = function () {
                if ($scope.vendorBidPrice != "" && $scope.vendorBidPrice >= 0 && $scope.auctionItem.minPrice > $scope.vendorBidPrice) {
                    $("#reduceBidValue").val($scope.auctionItem.minPrice - $scope.vendorBidPrice);
                    /*angular.element($event.target).parent().addClass('fg-line fg-toggled');*/
                }
                else {
                    $("#reduceBidValue").val(0);
                    swal("Error!", "Invalid bid value.", "error");
                    $scope.vendorBidPrice = 0;
                }
            };

            $scope.formRequest = {
                overallItemLevelComments: '',
                rankLevel: 'BOTH',
                reductionLevel: 'OVERALL',
                selectedVendor: {},
                priceCapValue: 0,
                priceCapValueMsg: ''
            };

            $scope.selectVendor = function () {
                var selVendID = $scope.formRequest.selectedVendor.vendorID;
                var winVendID = $scope.auctionItem.auctionVendors[0].vendorID;
                if (!$scope.formRequest.selectedVendor.reason && selVendID != winVendID) {
                    growlService.growl("Please enter the reason for choosing the particular vendor", "inverse");
                    return false;
                }
                var params = {
                    userID: $scope.currentUserId,
                    vendorID: $scope.formRequest.selectedVendor.vendorID,
                    reqID: $scope.auctionItem.requirementID,
                    reason: $scope.formRequest.selectedVendor.reason ? $scope.formRequest.selectedVendor.reason : (selVendID != winVendID ? $scope.formRequest.selectedVendor.reason : ""),
                    sessionID: $scope.currentSessionId
                };
                auctionsService.selectVendor(params)
                    .then(function (response) {
                        if (!response.errorMessage) {
                            growlService.growl("Vendor " + response.userInfo.firstName + " " + response.userInfo.lastName + " has been selected for the final  ", "inverse");
                            $scope.getData();
                        }
                    });
            };

            $scope.exportItemsToExcel = function () {
                var mystyle = {
                    sheetid: 'QuotationDetails',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                if ($scope.auctionItem.isDiscountQuotation == 0 || $scope.auctionItem.isDiscountQuotation == 1) {
                    var name = '';
                    if ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED') {
                        name = 'UNIT_QUOTATION_' + $scope.reqId;
                    } else {
                        name = 'UNIT_BIDDING_' + $scope.reqId;
                    }

                    PRMForwardBidService.downloadTemplate(name, $scope.currentUserId, $scope.reqId);
                }
            };

            $scope.exportItemsToExcelForCeiling = function () {
                var name = 'CEILING_DETAILS_' + $scope.reqId;
                let userId = $scope.currentUserId;
                if ($scope.itemCeilingVendor && $scope.itemCeilingVendor.vendorID) {
                    userId = $scope.itemCeilingVendor.vendorID;
                }

                PRMForwardBidService.downloadTemplate(name, userId, $scope.reqId);
            };

            $scope.rateVendor = function (vendorID) {
                var params = {
                    uID: $scope.currentUserId,
                    userID: vendorID,
                    rating: $scope.ratingForVendor,
                    sessionID: $scope.currentSessionId
                };
                auctionsService.rateVendor(params)
                    .then(function (response) {
                        if (!response.errorMessage) {
                            growlService.growl("Rating saved successfully. You can edit your rating as well.", "inverse");
                        }
                    });
            };

            $scope.rateCustomer = function () {
                var params = {
                    uID: $scope.currentUserId,
                    userID: $scope.auctionItem.customerID,
                    rating: $scope.ratingForCustomer,
                    sessionID: $scope.currentSessionId
                };

                auctionsService.rateVendor(params)
                    .then(function (response) {
                        if (!response.errorMessage) {
                            growlService.growl("Rating saved successfully. You can edit your rating as well.", "inverse");
                        }
                    });
            };

            $scope.makeABidDisable = false;

            $scope.makeaBid1 = function () {
                let currentVendorFactor = $scope.auctionItem.auctionVendors[0].vendorCurrencyFactor;
                let isValidItemPrice = true;
                let isValidItemPriceReduction = true;
                $scope.makeABidDisable = true;
                var bidPrice = $("#makebidvalue").val();
                let containsItemsPriceMoreThanCeilingPrice = false;
                $scope.auctionItemVendor.listRequirementItems.forEach(function (vendorItem, itemIndexs) {
                    if (vendorItem.revUnitPrice) {
                        var requirementItem = $scope.auctionItem.listRequirementItems.filter(function (reqItem) {
                            return reqItem.itemID === vendorItem.itemID;
                        });

                        requirementItem[0].ceilingPriceFactor = requirementItem[0].ceilingPrice / currentVendorFactor;
                        if (requirementItem && requirementItem.length > 0 && requirementItem[0].ceilingPrice && +vendorItem.revUnitPriceOld && +vendorItem.revUnitPrice < requirementItem[0].ceilingPriceFactor) {
                            vendorItem.revUnitPrice = vendorItem.revUnitPriceOld ? vendorItem.revUnitPriceOld : vendorItem.revUnitPrice;
                            isValidItemPrice = false;
                            $scope.makeABidDisable = false;
                            return false;
                        } else if (requirementItem && requirementItem.length > 0 && requirementItem[0].ceilingPrice && +vendorItem.revUnitPriceOld && +vendorItem.revUnitPriceOld < requirementItem[0].ceilingPrice && +vendorItem.revUnitPrice >= requirementItem[0].ceilingPriceFactor) {
                            if (vendorItem.revUnitPrice > (1.25 * requirementItem[0].ceilingPriceFactor)) {
                                vendorItem.revUnitPrice = vendorItem.revUnitPriceOld ? vendorItem.revUnitPriceOld : vendorItem.revUnitPrice;
                                isValidItemPriceReduction = false;
                                $scope.makeABidDisable = false;
                                return false;
                            }
                        }
                    }

                    if (requirementItem && requirementItem.length > 0 && requirementItem[0].ceilingPrice && +vendorItem.revUnitPriceOld < requirementItem[0].ceilingPriceFactor) {
                        containsItemsPriceMoreThanCeilingPrice = true;
                    }
                });

                if (!isValidItemPrice) {
                    swal("Error!", "Your item revised amount should be greater than Item Ceiling amount", 'error');
                    $scope.makeABidDisable = false;
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    return false;
                }

                if (!isValidItemPriceReduction) {
                    swal("Error!", "You are increasing more than 25% of current item amount. The Maximum increment amount per item should not exceed more than 25% from item ceiling amount", 'error');
                    $scope.makeABidDisable = false;
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    return false;
                }

                if (bidPrice == "" || Number(bidPrice) <= 0) {
                    $scope.bidPriceEmpty = true;
                    $scope.bidPriceValidation = false;
                    $("#makebidvalue").val("");
                    $scope.makeABidDisable = false;
                    return false;
                } else if (!isNaN($scope.auctionItem.minPrice) && $scope.auctionItem.minPrice > 0 && +bidPrice <= $scope.auctionItem.minPrice) {
                    $scope.bidPriceValidation = true;
                    $scope.bidPriceEmpty = false;
                    $scope.makeABidDisable = false;
                    $scope.makeABidDisable = false;
                    return false;
                } else {
                    $scope.bidPriceValidation = false;
                    $scope.bidPriceEmpty = false;
                }
                if (($scope.bidAttachementName == "" || $scope.bidAttachement.length == 0) && $scope.quotationStatus == false) {
                    $scope.bidAttachementValidation = true;
                    $scope.makeABidDisable = false;
                    return false;
                } else {
                    $scope.bidAttachementValidation = false;
                }
                if (+bidPrice < (+$scope.auctionItem.auctionVendors[0].runningPrice) + ($scope.auctionItem.minBidAmount / currentVendorFactor)) {
                    $scope.reduceBidValue = "";
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    swal("Error!", "Your amount must be at least " + $scope.precisionRound(($scope.auctionItem.minBidAmount / currentVendorFactor), $rootScope.companyRoundingDecimalSetting) + " greater than your previous bid.", 'error');
                    $scope.makeABidDisable = false;
                    return false;
                }
                if (+bidPrice > (1.25 * (+$scope.auctionItem.auctionVendors[0].runningPrice)) && !containsItemsPriceMoreThanCeilingPrice) {
                    $scope.reduceBidValue = "";
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    swal("Maximum Increment Error!", " You are increasing more than 25% of current bid amount. The Maximum increment amount per bid should not exceed more than 25% from current bid amount  " + $scope.auctionItem.minPrice + ". Incase if You want to increment more Please Do it in Multiple Bids", "error");
                    $scope.makeABidDisable = false;
                    return false;
                }
                else {
                    var params = {};
                    params.reqID = parseInt(id);
                    params.sessionID = $scope.currentSessionId;
                    params.userID = $scope.currentUserId;
                    params.price = $scope.precisionRound(parseFloat(bidPrice), $rootScope.companyRoundingDecimalSetting);
                    params.quotationName = $scope.bidAttachementName;
                    params.ignorevalidations = containsItemsPriceMoreThanCeilingPrice;
                    params.payment = validateStringWithoutSpecialCharacters($scope.payment);

                    if (!params.payment) {
                        params.payment = '';
                        swal({
                            title: "Error",
                            text: "Please enter the Payment Terms",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        });
                        $scope.makeABidDisable = false;
                        return false;
                    }

                    params.quotationObject = $scope.auctionItemVendor.listRequirementItems;
                    params.vendorBidPrice = $scope.revvendorBidPrice;
                    params.itemrevtotalprice = $scope.revtotalprice;

                    if ($scope.auctionItem.isUnitPriceBidding === 1) {
                        $scope.items = {
                            itemsList: $scope.auctionItemVendor.listRequirementItems,
                            userID: $scope.currentUserId,
                            reqID: params.reqID,
                            price: $scope.revtotalprice,
                            vendorBidPrice: $scope.revvendorBidPrice
                        };
                    }

                    requirementHub.invoke('MakeBid', params, function (req) {

                        if (req.errorMessage === '') {
                            $scope.makeABidDisable = false;
                            $scope.overAllValue.overallPercentage = 0;
                            $scope.overAllValue.overallDiscountPercentage = 0;
                            swal("Thanks !", "Your bidding process has been successfully updated", "success");
                            $scope.overAllValue.overallPercentage = 0;
                            $scope.overAllValue.overallDiscountPercentage = 0;

                            if ($scope.auctionItemVendor.listRequirementItems.length > 20) {
                                if ($scope.auctionItem.isUnitPriceBidding === 1) {
                                    if ($scope.items.revinstallationCharges == null || $scope.items.revinstallationCharges == '' || $scope.items.revinstallationCharges == undefined || isNaN($scope.items.revinstallationCharges)) {
                                        $scope.items.revinstallationCharges = 0;
                                    }
                                    $scope.items.itemsList.forEach(function (item, itemIndexs) {
                                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                                subItem.dateCreated = "/Date(1561000200000+0530)/";
                                                subItem.dateModified = "/Date(1561000200000+0530)/";
                                            });
                                        }
                                        item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplateArray);
                                    });

                                    PRMForwardBidService.SaveRunningItemPrice($scope.items)
                                        .then(function (response) {
                                            if (response.errorMessage) {
                                                growlService.growl(response.errorMessage, "inverse");
                                            } else {
                                                $scope.recalculate('', 0, false);
                                            }
                                        });
                                }
                            }

                            $scope.reduceBidValue = "";
                            $("#makebidvalue").val("");
                            $("#reduceBidValue").val("");

                        } else {
                            if (req.errorMessage != '') {

                                $scope.getData();
                                $scope.makeABidDisable = false;
                                var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId;
                                requirementHub.invoke('CheckRequirement', parties, function () {

                                    $scope.reduceBidValue = "";
                                    $("#makebidvalue").val("");
                                    $("#reduceBidValue").val("");

                                    var htmlContent = 'We have some one in this range <h3>' + req.errorMessage + ' </h3> Kindly Quote some other value.'

                                    swal("Cancelled", "", "error");
                                    $(".sweet-alert h2").html("oops...! Your Price is Too close to another Bid <br> We have some one in this range <h3 style='color:red'>" + req.errorMessage + " </h3> Kindly Quote some other value.");

                                });

                            } else {
                                $scope.makeABidDisable = false;
                                swal("Error!", req.errorMessage, "error");
                            }
                        }
                    });
                }
            };

            $scope.recalculate = function (subMethodName, receiverId, showswal) {

                if (subMethodName == null || subMethodName == '' || subMethodName == undefined) {
                    subMethodName = '';
                }

                if (receiverId < 0) {
                    receiverId = -1;
                }

                var params = {};
                params.reqID = id;
                params.sessionID = $scope.currentSessionId;
                params.userID = $scope.currentUserId;
                var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId + "$" + subMethodName + "$" + receiverId;
                $scope.invokeSignalR('CheckRequirement', parties);
                if (showswal) {
                    swal("done!", "a refresh command has been sent to everyone.", "success");
                }
            };

            $scope.setFields = function () {
                if ($scope.auctionItem.status == "CLOSED") {
                    $scope.mactrl.skinSwitch('green');
                    if (($scope.auctionItem.customerID == $scope.currentUserId || $scope.auctionItem.superUserID == $scope.currentUserId || $scope.auctionItem.customerReqAccess)) {
                        $scope.errMsg = "Negotiation has been completed. You can generate the Purchase Order by pressing the button below.";
                    }
                    else {
                        $scope.errMsg = "Negotiation has completed.";
                    }
                    $scope.showStatusDropDown = false;
                    $scope.showGeneratePOButton = true;
                } else if ($scope.auctionItem.status == "UNCONFIRMED" || $scope.auctionItem.status == "NOTSTARTED") {
                    $scope.mactrl.skinSwitch('teal');
                    $scope.errMsg = "Negotiation has not started yet.";
                    $scope.showStatusDropDown = false;
                    $scope.auctionStarted = false;
                    $scope.isAuctionScheduled = true;
                    $scope.timeLeftMessage = "Negotiation Starts in: ";
                    $scope.startBtns = true;
                    $scope.customerBtns = false;
                } else if ($scope.auctionItem.status == "STARTED") {
                    $scope.mactrl.skinSwitch('orange');
                    $scope.errMsg = "Negotiation has started.";
                    $scope.showStatusDropDown = false;
                    $scope.auctionStarted = true;
                    $scope.isAuctionScheduled = false;
                    // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION
                    $scope.timeLeftMessage = "Negotiation Ends in: ";
                    // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION

                    $scope.startBtns = false;
                    $scope.customerBtns = true;
                } else if ($scope.auctionItem.status == "DELETED") {
                    $scope.mactrl.skinSwitch('bluegray');
                    $scope.errMsg = "This requirement has been cancelled.";
                    $scope.showStatusDropDown = false;
                    $scope.isDeleted = true;
                } else if ($scope.auctionItem.status == "Negotiation Ended") {
                    $scope.mactrl.skinSwitch('bluegray');
                    $scope.errMsg = "Negotiation has been completed.";
                    $scope.showStatusDropDown = false;
                } else if ($scope.auctionItem.status == "Vendor Selected") {
                    $scope.mactrl.skinSwitch('bluegray');
                    if ($scope.auctionItem.isTabular) {
                        $scope.errMsg = "Please select vendors for all items in order to provide Purchase Order Information.";
                    } else {
                        $scope.errMsg = "Please click the button below to provide the Purchase Order information.";
                    }
                    $scope.showStatusDropDown = false;
                } else if ($scope.auctionItem.status == "PO Processing") {
                    $scope.mactrl.skinSwitch('lightblue');
                    $scope.errMsg = "The PO has been generated. Please find the PO here: ";
                    $scope.showStatusDropDown = false;
                } else {
                    $scope.mactrl.skinSwitch('lightblue');
                    $scope.showStatusDropDown = true;
                }
                if (($scope.auctionItem.customerID == $scope.currentUserId || $scope.auctionItem.superUserID == $scope.currentUserId || $scope.auctionItem.customerReqAccess)) {
                    $scope.userIsOwner = true;
                    if ($scope.userIsOwner && !$rootScope.isUserEntitled(546654) && $scope.auctionItem.status == "STARTED") {
                        swal("Error!", "Live negotiation Access Denined", "error");
                        $state.go('home');
                    }
                    if ($scope.userIsOwner && !$rootScope.isUserEntitled(591159)) {
                        swal("Error!", "View Requirement Access Denied", "error");
                        $state.go('home');
                    }
                    $scope.options = ['PO Sent', 'Material Received', 'Payment Processing', 'Payment Released'];
                    $scope.options.push($scope.auctionItem.status);
                }
                var start = $scope.auctionItem.startTime.split('+')[0].split('(')[1];
                auctionsService.getdate()
                    .then(function (responseFromServer) {
                        var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                        $log.debug(dateFromServer);
                        var curDate = dateFromServer;

                        var myEpoch = curDate.getTime();
                        $scope.timeLeftMessage = "";
                        if (start > myEpoch) {
                            $scope.auctionStarted = false;
                            $scope.isAuctionScheduled = true;
                            $scope.timeLeftMessage = "Negotiation Starts in: ";
                            $scope.startBtns = true;
                            $scope.customerBtns = false;
                        } else {
                            $scope.auctionStarted = true;
                            $scope.isAuctionScheduled = false;
                            // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION
                            $scope.timeLeftMessage = "Negotiation Ends in: ";
                            // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION

                            $scope.startBtns = false;
                            $scope.customerBtns = true;
                        }
                        if ($scope.auctionItem.customerID != $scope.currentUserId && $scope.auctionItem.superUserID != $scope.currentUserId && !$scope.auctionItem.customerReqAccess) {
                            $scope.startBtns = false;
                            $scope.customerBtns = false;
                        }
                        if ($scope.auctionItem.timeLeft == null || $scope.auctionItem.timeLeft < 1) {
                            $scope.showTimer = false;
                            $scope.disableButtons();
                        } else {
                            $scope.showTimer = true;
                        }
                        //$scope.auctionItem.postedOn = new Date(parseFloat($scope.auctionItem.postedOn.substr(6)));
                        //var date = $scope.auctionItem.postedOn.split('+')[0].split('(')[1];
                        //var newDate = new Date(parseInt(parseInt(date)));
                        //$scope.auctionItem.postedOn = newDate.toString().replace('Z', '');

                        //var date1 = $scope.auctionItem.deliveryTime.split('+')[0].split('(')[1];
                        //var newDate1 = new Date(parseInt(parseInt(date1)));
                        //$scope.auctionItem.deliveryTime = newDate1.toString().replace('Z', '');

                        //var date2 = $scope.auctionItem.quotationFreezTime.split('+')[0].split('(')[1];
                        //var newDate2 = new Date(parseInt(parseInt(date2)));
                        //$scope.auctionItem.quotationFreezTime = newDate2.toString().replace('Z', '');

                        // // #INTERNATIONALIZATION-0-2019-02-12
                        $scope.auctionItem.postedOn = userService.toLocalDate($scope.auctionItem.postedOn);

                        var minPrice = 0;
                        if ($scope.auctionItem.status == "NOTSTARTED") {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].initialPrice : 0;
                        } else {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].runningPrice : 0;
                        }
                        //$scope.auctionItem.deliveryTime = $scope.auctionItem.deliveryTime.split("GMT")[0];
                        for (var i in $scope.auctionItem.auctionVendors) {

                            $scope.nonParticipatedMsg = '';
                            $scope.quotationRejecteddMsg = '';
                            $scope.quotationNotviewedMsg = '';
                            $scope.revQuotationRejecteddMsg = '';
                            $scope.revQuotationNotviewedMsg = '';
                            $scope.quotationApprovedMsg = '';
                            $scope.revQuotationApprovedMsg = '';

                            var vendor = $scope.auctionItem.auctionVendors[i];
                            if (vendor.vendorID === $scope.currentUserId && vendor.quotationUrl === "") {
                                $scope.quotationStatus = false;
                                $scope.quotationUploaded = false;
                            } else {
                                $scope.quotationStatus = true;
                                if ($scope.auctionItem.customerID != $scope.currentUserId && $scope.auctionItem.superUserID != $scope.currentUserId && !$scope.auctionItem.customerReqAccess) {
                                    $scope.quotationUploaded = true;
                                }

                                $scope.quotationUrl = vendor.quotationUrl;
                                $scope.revquotationUrl = vendor.revquotationUrl;
                                if (!$scope.vendorQuotedPrice) {
                                    $scope.vendorQuotedPrice = vendor.runningPrice;
                                }
                            }

                            if (i == 0 && vendor.initialPrice != 0) {
                                minPrice = vendor.initialPrice;
                            } else {
                                if (vendor.initialPrice < minPrice && vendor.initialPrice != 0) {
                                    minPrice = vendor.initialPrice;
                                }
                            }
                            $scope.vendorInitialPrice = minPrice;
                            var runningMinPrice = 0;
                            if ($scope.auctionItem.auctionVendors[i].runningPrice > 0 && $scope.auctionItem.auctionVendors[i].runningPrice < $scope.vendorInitialPrice) {
                                runningMinPrice = $scope.auctionItem.auctionVendors[i].runningPrice;
                            }
                            //$scope.auctionItem.minPrice = runningMinPrice;
                            if ($scope.auctionItem.auctionVendors[i].runningPrice == 0) {
                                $scope.auctionItem.auctionVendors[i].runningPrice = 0.00;
                                $scope.auctionItem.auctionVendors[i].totalPriceIncl = 0.00;
                                $scope.auctionItem.auctionVendors[i].rank = 'NA';
                            } else {
                                $scope.vendorRank = vendor.rank;
                                if (vendor.rank == 1) {
                                    $scope.toprankerName = vendor.vendorName;
                                    if ($scope.currentUserId == vendor.vendorID) {
                                        $scope.options = ['PO Accepted', 'Material Dispatched', 'Payment Acknowledged'];
                                        $scope.options.push($scope.auctionItem.status);
                                        if ($scope.auctionItem.status == "STARTED") {
                                            $scope.enableMakeBids = true;
                                        }
                                    }
                                }
                                //$scope.auctionItem.auctionVendors[i].totalPriceIncl = $scope.auctionItem.auctionVendors[i].runningPrice + ($scope.auctionItem.auctionVendors[i].runningPrice * $scope.auctionItem.auctionVendors[i].taxes) / 100;
                                $scope.auctionItem.auctionVendors[i].totalPriceIncl = $scope.auctionItem.auctionVendors[i].runningPrice;
                            }
                            if ($scope.auctionItem.auctionVendors[i].initialPrice == 0) {
                                $scope.auctionItem.auctionVendors[i].initialPrice = 0.00;
                                $scope.nonParticipatedMsg = $scope.showMessageBasedOnBiddingType('You have missed an opportunity to participate in this Negotiation.', 'NEG_STARTED');
                            }

                            if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 1) {
                                $scope.quotationRejecteddMsg = 'Your quotation rejected by the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == -1) {
                                $scope.quotationNotviewedMsg = 'Your quotation submitted to the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == 1 && $scope.auctionItem.status == "Negotiation Ended") {
                                $scope.revQuotationRejecteddMsg = 'Your Rev.Quotation rejected by the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == -1 && $scope.auctionItem.status == 'Negotiation Ended') {
                                $scope.revQuotationNotviewedMsg = 'Your Rev.Quotation submitted to the customer.';
                            }

                            if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 0) {
                                $scope.quotationApprovedMsg = $scope.showMessageBasedOnBiddingType('Your quotation has been shortlisted for further process.', 'INITIAL_QUOTE');
                            }
                            if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 0 && $scope.itemQuotationsMessage()) {
                                $scope.quotationApprovedMsg = $scope.itemQuotationsMessage();
                            }
                            if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == 0) {
                                $scope.revQuotationApprovedMsg = 'Your Rev.Quotation Approved by the customer.';
                            }

                        }
                        $scope.$broadcast('timer-set-countdown-seconds', $scope.auctionItem.timeLeft);
                        $('.datetimepicker').datetimepicker({
                            useCurrent: false,
                            icons: {
                                time: 'glyphicon glyphicon-time',
                                date: 'glyphicon glyphicon-calendar',
                                up: 'glyphicon glyphicon-chevron-up',
                                down: 'glyphicon glyphicon-chevron-down',
                                previous: 'glyphicon glyphicon-chevron-left',
                                next: 'glyphicon glyphicon-chevron-right',
                                today: 'glyphicon glyphicon-screenshot',
                                clear: 'glyphicon glyphicon-trash',
                                close: 'glyphicon glyphicon-remove'

                            },

                            minDate: curDate
                        });
                    })


                $scope.reduceBidAmountNote = 'Specify the value to be increased from bid Amount.';
                //$scope.incTaxBidAmountNote = 'Please Enter the consolidate bid value including tax amount.';
                $scope.incTaxBidAmountNote = '';
                //$scope.noteForBidValue = 'NOTE : If you fill one field, other will be autocalculated.';
                $scope.noteForBidValue = '';

                if ($scope.auctionItem.status == "STARTED") {
                    liveId = "live";
                }
                else {
                    liveId = "";
                }

            };

            $scope.vendorBidPrice = 0;
            $scope.revvendorBidPrice = 0;
            $scope.auctionStarted = true;
            $scope.customerBtns = true;
            $scope.showVendorTable = true;
            $scope.quotationStatus = true;
            $scope.toprankerName = "";
            $scope.vendorRank = 0;
            $scope.quotationUrl = "";
            $scope.revquotationUrl = "";
            $scope.vendorBtns = false;
            $scope.vendorQuotedPrice = 0;
            $scope.startBtns = false;
            $scope.commentsvalidation = false;
            $scope.enableMakeBids = false;
            $scope.price = "";
            $scope.startTime = '';
            $scope.customerID = $scope.currentUserId;

            $scope.priceSwitch = 0;

            $scope.poDetails = {};

            $scope.bidHistory = {};

            $scope.GetBidHistory = function () {
                PRMForwardBidService.GetBidHistory({ "reqid": $stateParams.Id, "sessionid": $scope.currentSessionId, 'userid': $scope.currentUserId })
                    .then(function (response) {
                        $scope.bidHistory = response;
                    });
            };

            $scope.vendorID = 0;

            $scope.validity = '';


            if ($scope.auctionItem.urgency == 'Critical (Will be Closed Today)') {
                $scope.validity = '1 Day';
            }
            if ($scope.auctionItem.urgency == 'High (Will be Closed in 2 Days)') {
                $scope.validity = '2 Days';
            }
            if ($scope.auctionItem.urgency == 'Medium (Will be Closed in 3 Days)') {
                $scope.validity = '3 Days';
            }
            if ($scope.auctionItem.urgency == 'Low (Will Take 7 Days Time)') {
                $scope.validity = '7 Days';
            }

            $scope.warranty = 'As Per OEM';
            $scope.duration = $scope.auctionItem.deliveryTime;
            $scope.payment = $scope.auctionItem.paymentTerms;
            $scope.gstNumber = $scope.auctionItem.gstNumber;
            $scope.isQuotationRejected = -1;
            $scope.starttimecondition1 = 0;
            $scope.starttimecondition2 = 0;
            $scope.revQuotationUrl1 = 0;
            $scope.L1QuotationUrl = 0;
            $scope.notviewedcompanynames = '';
            $scope.starreturn = false;
            $scope.customerListRequirementTerms = [];
            $scope.customerDeliveryList = [];
            $scope.customerPaymentlist = [];
            $scope.listTerms = [];
            $scope.listRequirementTerms = [];
            $scope.deliveryRadio = false;
            $scope.deliveryList = [];
            $scope.paymentRadio = false;
            $scope.paymentlist = [];
            $scope.vendorsFromPRM = 1;
            $scope.vendorsFromSelf = 2;
            $scope.totalprice = 0;
            $scope.taxs = 0;
            $scope.vendorTaxes = 0;
            $scope.discountAmount = 0;
            $scope.totalpriceinctaxfreight = 0;
            $scope.vendorBidPriceWithoutDiscount = 0;
            $scope.revtotalprice = 0;
            $scope.revtaxs = 0;
            $scope.revvendorTaxes = $scope.vendorTaxes;
            $scope.revtotalpriceinctaxfreight = 0;
            $scope.priceValidationsVendor = '';
            $scope.calculatedSumOfAllTaxes = 0;
            $scope.taxValidation = false;
            $scope.ItemPriceValidation = false;
            $scope.discountfreightValidation = false;
            $scope.gstValidation = false;
            $scope.freightCharges = 0;
            $scope.revfreightCharges = 0;
            $scope.packingCharges = 0;
            $scope.revpackingCharges = 0;
            $scope.installationCharges = 0;
            $scope.revinstallationCharges = 0;

            $scope.additionalSavings = 0;

            $scope.getpricecomparison = function () {
                PRMForwardBidService.getpricecomparison({ "reqid": $scope.reqId, "sessionid": $scope.currentSessionId, 'userid': $scope.currentUserId })
                    .then(function (response) {
                        $scope.priceCompObj = response;

                        $scope.totalItemsL1Price = 0;
                        $scope.totalItemsMinimunPrice = 0;
                        $scope.negotiationSavings = 0;
                        $scope.savingsByLeastBidder = 0;
                        $scope.savingsByItemMinPrice = 0;

                        if ($scope.priceCompObj && $scope.priceCompObj.priceCompareObject && $scope.priceCompObj.priceCompareObject.length > 0) {
                            for (var i = 0; i < $scope.priceCompObj.priceCompareObject.length; i++) {
                                $scope.totalItemsL1Price += $scope.priceCompObj.priceCompareObject[i].leastBidderPrice;
                                $scope.totalItemsMinimunPrice += $scope.priceCompObj.priceCompareObject[i].minPrice;
                            }
                        }

                        $scope.negotiationSavings = $scope.priceCompObj.requirement.savings;
                        $scope.savingsByLeastBidder = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsL1Price;
                        $scope.savingsByItemMinPrice = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsMinimunPrice;

                        $scope.additionalSavings = 0;

                        $scope.additionalSavings = $scope.totalItemsL1Price - $scope.totalItemsMinimunPrice;

                    });
            };

            $scope.GetRequirementTerms = function () {
                PRMForwardBidService.GetRequirementTerms($scope.currentUserId, $scope.reqId, $scope.currentSessionId)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.listRequirementTerms = response;
                            $scope.listRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }

                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });
                        }
                        else {

                            $scope.customerListRequirementTerms.forEach(function (item, index) {

                                var obj = {
                                    currentTime: item.currentTime,
                                    errorMessage: item.errorMessage,
                                    isRevised: item.isRevised,
                                    refReqTermID: item.reqTermsID,
                                    reqID: item.reqID,
                                    reqTermsDays: item.reqTermsDays,
                                    reqTermsID: 0,
                                    reqTermsPercent: item.reqTermsPercent,
                                    reqTermsType: item.reqTermsType,
                                    sessionID: item.sessionID,
                                    userID: item.userID
                                };

                                $scope.listRequirementTerms.push(obj);

                            });

                            $scope.listRequirementTerms.forEach(function (item, index) {

                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }

                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });
                        }

                    });
            };

            $scope.GetRequirementTermsCustomer = function () {
                PRMForwardBidService.GetRequirementTerms($scope.auctionItem.customerID, $scope.reqId, $scope.currentSessionId)
                    .then(function (response) {
                        if (response && response.length > 0) {

                            $scope.customerListRequirementTerms = [];
                            $scope.customerDeliveryList = [];
                            $scope.customerPaymentlist = [];

                            $scope.customerListRequirementTerms = response;

                            $scope.customerListRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.customerDeliveryList.push(item);
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }
                                    else if (item.reqTermsDays == 0) {
                                        item.paymentType = '';
                                    }

                                    $scope.customerPaymentlist.push(item);
                                }
                            });
                        }


                        //$scope.GetRequirementTerms();


                    });
            };

            $scope.getUserCredentials = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getusercredentials?sessionid=' + $scope.currentSessionId + "&userid=" + $scope.currentUserId,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    $scope.CredentialsResponce = response.data;
                    $scope.CredentialsResponce.forEach(function (cred, credIndex) {
                        if (cred.fileType === "STN") {
                            $scope.gstNumber = cred.credentialID;
                        }
                    });

                    if (!$scope.gstNumber && $scope.companyGSTInfo && $scope.companyGSTInfo.length > 0) {
                        $scope.gstNumber = $scope.companyGSTInfo[0].gstNumber;
                    }

                }, function (result) {

                });
            };

            $scope.colspanDynamic = 14;
            $scope.getData = function (methodName, callerID) {
                PRMForwardBidService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": $scope.currentSessionId, 'userid': $scope.currentUserId })
                    .then(function (response) {
                        if (response) {
                            $scope.setAuctionInitializer(response, methodName, callerID);
                            $scope.GetReqDataPriceCap();
                            //$scope.getCurrencyRateStatus();
                            $scope.isTaxAvailable();
                            auctionsService.GetCompanyConfiguration(response.custCompID, "ITEM_UNITS,GST_LOCAL_CODE", $scope.currentSessionId)
                                .then(function (configResponse) {
                                    if (configResponse && configResponse.length > 0) {
                                        $scope.companyItemUnits = configResponse.filter(function (config) {
                                            return config.configKey === 'ITEM_UNITS';
                                        });

                                        let gstLocalCodeArr = configResponse.filter(function (config) {
                                            return config.configKey === 'GST_LOCAL_CODE';
                                        });

                                        if (gstLocalCodeArr && gstLocalCodeArr.length > 0) {
                                            $scope.localGSTNumberCode = gstLocalCodeArr[0].configValue;
                                        }
                                    }
                                });
                        }
                    });
                //$scope.$broadcast('timer-start');
            };

            //Get Min reduction amount based on min quotation amount of 1%
            $scope.getMinReductionAmount = function () {
                if ($scope.auctionItem.minBidAmount > 0) {
                    $scope.NegotiationSettings.minReductionAmount = $scope.auctionItem.minBidAmount;
                    $scope.NegotiationSettings.rankComparision = $scope.auctionItem.minVendorComparision;
                }
                else {
                    var amount = $scope.NegotiationSettings.minReductionAmount;
                    if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                        var tempAuctionVendors = $scope.auctionItem.auctionVendors.filter(function (vendor) {
                            return (vendor.runningPrice > 0 && vendor.initialPrice > 0 && vendor.companyName != 'PRICE_CAP' && vendor.isQuotationRejected == 0);
                        });

                        var temp = _.maxBy(tempAuctionVendors, 'initialPrice'); //doubt is this required for reverse we go by minby
                        if (temp && temp.initialPrice && temp.initialPrice > 0) {
                            amount = temp.initialPrice * 0.005;
                        }
                    }

                    if ($scope.formRequest.reductionLevel && $scope.formRequest.reductionLevel === 'OVERALL') {
                        $scope.NegotiationSettings.minReductionAmount = 0.5;
                    } else {
                        $scope.NegotiationSettings.minReductionAmount = amount;
                    }

                    $scope.NegotiationSettings.rankComparision = amount;
                }
            };


            $scope.CheckDisable = false;
            $scope.existingVendors = [];

            $scope.setAuctionInitializer = function (response, methodName, callerID) {
                let requirementIncoTermsSettings = {
                    INCO_TERMS: ''
                };

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                    requirementIncoTermsSettings.INCO_TERMS = $scope.auctionItem.auctionVendors[0].INCO_TERMS;
                }

                $scope.auctionItem = response;
                $scope.auctionItem.paymentTerms = ($scope.auctionItem.paymentTerms || '');
                $scope.auctionItemTemporary = response;
                $scope.auctionItem.listRequirementItems = _.orderBy($scope.auctionItem.listRequirementItems, ['isCoreProductCategory'], ['desc']);

                if ($scope.auctionItem.auctionVendors.length > 0 && $scope.auctionItem.auctionVendors[0].isVendAckChecked) {
                    $scope.auctionItem.auctionVendors[0].isVendAckApprove = true;
                }
                if (!$scope.isCustomer && $scope.companyINCOTerms.length <= 0) {
                    auctionsService.GetCompanyConfiguration($scope.currentUserCompID, "INCO", $scope.currentSessionId)
                        .then(function (unitResponse) {
                            $scope.companyINCOTerms = unitResponse;
                        });
                }

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                    $scope.GetCompanyGSTInfo();
                    $scope.auctionItem.auctionVendors[0].INCO_TERMS = requirementIncoTermsSettings.INCO_TERMS ? requirementIncoTermsSettings.INCO_TERMS : $scope.auctionItem.auctionVendors[0].INCO_TERMS;
                }

                if ($scope.auctionItemTemporary && $scope.auctionItemTemporary && $scope.auctionItemTemporary.auctionVendors) {
                    $scope.auctionItemTemporary.emailLinkVendors = $scope.auctionItemTemporary.auctionVendors.filter(function (item) {
                        item.listWorkflows = [];
                        if (item.isEmailSent == undefined || item.isEmailSent == '') {
                            item.isEmailSent = false;
                        } else {
                            item.isEmailSent = true;
                        }

                        if (item.companyName != 'PRICE_CAP') {
                            $scope.existingVendors.push(item.vendorID);
                        }

                        return item.companyName != 'PRICE_CAP';
                    });

                    //$scope.getWorkflows();

                }


                $scope.CB_END_TIME = $scope.auctionItem.CB_END_TIME;
                //#CB-0-2018-12-05
                if (!$scope.isCustomer && $scope.auctionItem && $scope.auctionItem.auctionVendors &&
                    $scope.auctionItem.auctionVendors.length > 0 && $scope.auctionItem.auctionVendors[0].revVendorTotalPriceCB > 0 &&
                    //$scope.auctionItem.auctionVendors[0].FREEZE_CB == false &&
                    $scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 &&
                    $scope.auctionItem.IS_CB_ENABLED == 1 &&
                    $scope.auctionItem.IS_CB_COMPLETED == false) {
                    $scope.goToCbVendor($scope.auctionItem.auctionVendors[0].vendorID);
                }

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0
                    && $scope.auctionItem.auctionVendors[0].isQuotationRejected == 0) {
                    $scope.colspanDynamic = 21;
                } else {
                    $scope.colspanDynamic = 14;
                }
                //ack code start
                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                    if ($scope.auctionItem.auctionVendors[0].quotationUrl == '' || $scope.auctionItem.auctionVendors[0].quotationUrl == undefined) {
                        $scope.auctionItem.auctionVendors[0].isVendAckChecked = false;
                    } else {
                        $scope.auctionItem.auctionVendors[0].isVendAckChecked = true;
                        $scope.CheckDisable = true;
                    }
                }

                $scope.IS_CB_ENABLED = $scope.auctionItem.IS_CB_ENABLED;
                $scope.IS_CB_NO_REGRET = $scope.auctionItem.IS_CB_NO_REGRET;
                $scope.auctionItem.isUOMDifferent = false;
                if (callerID == $scope.currentUserId || callerID == undefined) {
                    $scope.auctionItemVendor = $scope.auctionItem;
                    if ($scope.auctionItemVendor.listRequirementItems != null) {
                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                            item.isEdit = true;
                            if (item.cGst > 0 || item.sGst > 0) {
                                $scope.IGSTFields = true;
                                $scope.CSGSTFields = false;
                            } else if (item.iGst > 0) {
                                $scope.IGSTFields = false;
                                $scope.CSGSTFields = true;
                            }
                            item.oldRevUnitPrice = item.revUnitPrice;
                            item.TemperoryRevUnitPrice = 0;
                            item.TemperoryRevUnitPrice = item.revUnitPrice;

                            item.TemperoryUnitDiscount = 0;
                            item.TemperoryRevUnitDiscount = 0;
                            item.TemperoryUnitDiscount = item.unitDiscount;
                            item.TemperoryRevUnitDiscount = item.revUnitDiscount;

                            item.vendorUnits = (item.vendorUnits == '' || item.vendorUnits == undefined) ? item.productQuantityIn : item.vendorUnits;
                            if (item.productQuantityIn.toUpperCase() != item.vendorUnits.toUpperCase()) {
                                //$scope.auctionItem.isUOMDifferent = true;
                            }
                        });
                    }
                }

                $scope.isCurrency = false;
                if (!$scope.isCustomer) {
                    $scope.fieldValidation();
                }

                $scope.notviewedcompanynames = '';
                $scope.auctionItem.listRequirementItems.forEach(function (item, itemIndex) {
                    if (item.productQuotationTemplateJson && item.productQuotationTemplateJson !== '' && item.productQuotationTemplateJson) {
                        item.productQuotationTemplateArray = JSON.parse(item.productQuotationTemplateJson);
                        item.productQuotationTemplateArray = item.productQuotationTemplateArray.filter(function (prodObj) {
                            if (prodObj) {
                                return prodObj;
                            }
                        });
                        $scope.handleSubItemsVisibility(item);
                    } else {
                        item.productQuotationTemplateArray = [];
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                    if ($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                        $scope.auctionItem.auctionVendors[0].listRequirementItems.forEach(function (i, indx) {
                            if (item.itemID && i.itemID && item.itemID == i.itemID) {
                                //console.log('ItemId:' + item.itemID + '--Rank:' + i.itemRank);
                                item.itemRank = i.itemRank;
                            }
                        });
                    }
                });

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors) {
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        if (!item.bestPrice) {
                            item.bestPrice = 0;
                        }

                        item.quotationRejectedComment = item.quotationRejectedComment.replace("Approved-", "");
                        item.quotationRejectedComment = item.quotationRejectedComment.replace("Reject Comments-", "");
                        if (item.quotationRejectedComment) {
                            item.quotationRejectedComment = item.quotationRejectedComment.trim();
                        }

                        item.revQuotationRejectedComment = item.revQuotationRejectedComment.replace("Approved-", "");
                        item.revQuotationRejectedComment = item.revQuotationRejectedComment.replace("Reject Comments-", "");
                        if (item.revQuotationRejectedComment) {
                            item.revQuotationRejectedComment = item.revQuotationRejectedComment.trim();
                        }


                        if ($scope.auctionItem.LAST_BID_ID == item.vendorID &&
                            ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED' || $scope.auctionItem.status == 'UNCONFIRMED')) {
                            item.vendorColorStyle = { 'background-color': 'lightgreen' };
                        }
                        else if (item.FREEZE_CB == true) {
                            item.vendorColorStyle = { 'background-color': 'rgb(255, 228, 225)' };
                        }
                        else if (item.VEND_FREEZE_CB == true) {
                            item.vendorColorStyle = { 'background-color': 'rgb(176, 224, 230)' };
                            //rgb(176, 224, 230)
                            //#B0E0E6
                        }
                        else {
                            item.vendorColorStyle = {};
                        }

                        item.isQuotationRejectedDB = item.isQuotationRejected;
                        item.isRevQuotationRejectedDB = item.isRevQuotationRejected;

                        item.bestPrice = 0;
                        item.hasRegretItems = false;
                        item.listRequirementItems.forEach(function (item2, index) {
                            item2.itemQuotationRejectedComment = item2.itemQuotationRejectedComment.replace("Approved-", "").replace("Reject Comments-", "");
                            //Core products only
                            if (item2.isCoreProductCategory) {
                                item.bestPrice += (item2.revUnitPrice * item2.productQuantity);
                            }

                            if (item2.isRegret) {
                                item.hasRegretItems = true;
                            }

                            if (item2.productQuantityIn.toUpperCase() != item2.vendorUnits.toUpperCase()) {
                                //item.isUOMDifferent = true;
                            }

                            if (item2.productQuotationTemplateJson && item2.productQuotationTemplateJson !== '' && item2.productQuotationTemplateJson) {
                                item2.productQuotationTemplateArray = JSON.parse(item2.productQuotationTemplateJson);
                            }
                            else {
                                item2.productQuotationTemplateArray = [];
                            }

                        });

                        if (!$scope.multipleAttachmentsList) {
                            $scope.multipleAttachmentsList = [];
                        }
                        if (!item.multipleAttachmentsList) {
                            item.multipleAttachmentsList = [];
                        }

                        if (item.multipleAttachments != '' && item.multipleAttachments != null && item.multipleAttachments != undefined) {
                            var multipleAttachmentsList = item.multipleAttachments.split(',');
                            item.multipleAttachmentsList = [];
                            $scope.multipleAttachmentsList = [];
                            multipleAttachmentsList.forEach(function (att, index) {
                                var fileUpload = {
                                    fileStream: [],
                                    fileName: '',
                                    fileID: att
                                };

                                item.multipleAttachmentsList.push(fileUpload);
                                $scope.multipleAttachmentsList.push(fileUpload);
                            });
                        }

                        if (item.quotationUrl != '' && item.isQuotationRejected == -1) {
                            $scope.Loding = false;
                            $scope.notviewedcompanynames += item.companyName + ', ';
                            $scope.starreturn = true;
                        }

                        //if (!$scope.isCustomer && (item.quotationUrl == '' || item.quotationUrl == null || item.quotationUrl == undefined)) {
                        //    $scope.getUserCredentials();
                        //}
                    });
                }

                $scope.selectedcurr = '';
                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                    //$scope.auctionItem.auctionVendors[0].selectedVendorCurrency = $scope.auctionItem.currency; //Force Requirement Currency
                    $scope.selectedcurr = $scope.auctionItem.auctionVendors[0].selectedVendorCurrency;
                }

                if (!$scope.auctionItem.multipleAttachments) {
                    $scope.auctionItem.multipleAttachments = [];
                }

                $scope.auctionItem.attFile = response.attachmentName;
                if ($scope.auctionItem.attFile != '' && $scope.auctionItem.attFile != null && $scope.auctionItem.attFile != undefined) {
                    var attchArray = $scope.auctionItem.attFile.split(',');
                    attchArray.forEach(function (att, index) {
                        var fileUpload = {
                            fileStream: [],
                            fileName: '',
                            fileID: att
                        };

                        $scope.auctionItem.multipleAttachments.push(fileUpload);
                    });
                }

                $scope.notviewedcompanynames = $scope.notviewedcompanynames.substring(0, $scope.notviewedcompanynames.length - 2);

                if ($scope.auctionItem && $scope.auctionItem.listRequirementTaxes && $scope.auctionItem.listRequirementTaxes.length == 0) {
                    $scope.auctionItem.listRequirementTaxes = $scope.listRequirementTaxes;
                }
                else {
                    $scope.listRequirementTaxes = $scope.auctionItem.listRequirementTaxes || [];
                }

                $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
                $scope.reqTaxSNo = $scope.auctionItem.taxSNoCount;
                $scope.NegotiationSettings = ($scope.auctionItem.NegotiationSettings || {});
                $scope.disableLocalGSTFeature = $scope.NegotiationSettings.disableLocalGSTFeature ? true : false;
                if ($scope.NegotiationSettings && $scope.NegotiationSettings.negotiationDuration) {
                    var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
                    $scope.days = parseInt(duration[0]);
                    duration = duration[1];
                    duration = duration.split(":", 4);
                    $scope.hours = parseInt(duration[0]);
                    $scope.mins = parseInt(duration[1]);
                    $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                }

                if (!response.auctionVendors || response.auctionVendors.length == 0) {
                    $scope.auctionItem.auctionVendors = [];
                    $scope.auctionItem.description = "";
                }

                // // #INTERNATIONALIZATION-0-2019-02-12
                $scope.auctionItem.quotationFreezTime = userService.toLocalDate($scope.auctionItem.quotationFreezTime);
                $scope.auctionItem.expStartTime = userService.toLocalDate($scope.auctionItem.expStartTime);




                if (($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length != 0) && (callerID == $scope.currentUserId || callerID == undefined)) {
                    $scope.totalprice = $scope.auctionItem.auctionVendors[0].initialPriceWithOutTaxFreight;
                    $scope.vendorTaxes = $scope.auctionItem.auctionVendors[0].taxes;
                    $scope.vendorBidPrice = $scope.auctionItem.auctionVendors[0].totalInitialPrice;

                    $scope.discountAmount = $scope.auctionItem.auctionVendors[0].discount;
                    $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice + $scope.discountAmount;
                    $scope.vendorID = $scope.auctionItem.auctionVendors[0].vendorID;
                    $scope.warranty = $scope.auctionItem.auctionVendors[0].warranty;
                    $scope.duration = $scope.auctionItem.auctionVendors[0].duration;
                    $scope.payment = $scope.auctionItem.auctionVendors[0].payment;
                    $scope.gstNumber = $scope.auctionItem.auctionVendors[0].gstNumber;
                    $scope.validity = $scope.auctionItem.auctionVendors[0].validity;
                    $scope.otherProperties = $scope.auctionItem.auctionVendors[0].otherProperties;

                    $scope.revvendorTaxes = $scope.auctionItem.auctionVendors[0].taxes;
                    $scope.isQuotationRejected = $scope.auctionItem.auctionVendors[0].isQuotationRejected;
                    $scope.quotationRejectedComment = $scope.auctionItem.auctionVendors[0].quotationRejectedComment;
                    $scope.revQuotationRejectedComment = $scope.auctionItem.auctionVendors[0].revQuotationRejectedComment;

                    $scope.selectedVendorCurrency = $scope.auctionItem.auctionVendors[0].selectedVendorCurrency;

                    if ($scope.auctionItem.urgency == 'Critical (Will be Closed Today)' && $scope.validity == '') {
                        $scope.validity = '1 Day';
                    }
                    else if ($scope.auctionItem.urgency == 'High (Will be Closed in 2 Days)' && $scope.validity == '') {
                        $scope.validity = '2 Days';
                    }
                    else if ($scope.auctionItem.urgency == 'Medium (Will be Closed in 3 Days)' && $scope.validity == '') {
                        $scope.validity = '3 Days';
                    }
                    else if ($scope.auctionItem.urgency == 'Low (Will Take 7 Days Time)' && $scope.validity == '') {
                        $scope.validity = '7 Days';
                    }
                    if ($scope.warranty == '') {
                        $scope.warranty = 'As Per OEM';
                    }
                    if ($scope.duration == '') {
                        //$scope.duration = new moment($scope.auctionItem.deliveryTime).format("DD-MM-YYYY");
                        $scope.duration = $scope.auctionItem.deliveryTime;
                    }
                    if ($scope.payment == '') {
                        $scope.payment = $scope.auctionItem.paymentTerms;
                    }

                    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                        if (item.productQuantityIn.toUpperCase() != item.vendorUnits.toUpperCase()) {
                            //$scope.auctionItem.isUOMDifferent = true;
                        }

                        item.Gst = item.cGst + item.sGst + item.iGst;

                        if ($scope.auctionItem.isDiscountQuotation == 1) {
                            if ($scope.auctionItem.status == 'STARTED') {
                                $scope.overAllValue.overallDiscountPercentage = item.revUnitDiscount;
                                $scope.revDiscountPrice = item.revUnitDiscount;
                            } else {
                                $scope.overAllValue.overallDiscountPercentage = item.unitDiscount;
                                $scope.revDiscountPrice = item.revUnitDiscount;
                            }

                        }

                        if ($scope.auctionItem.isDiscountQuotation == 2) {
                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                item.revCostPrice = item.costPrice;
                                item.revmarginAmount = item.marginAmount;
                            }

                            item = $scope.handlePrecision(item, 2);
                            item.Gst = item.cGst + item.sGst;
                            item = $scope.handlePrecision(item, 2);
                            item.costPrice = (item.unitMRP * 100 * 100) / ((item.unitDiscount * 100) + 10000 + (item.unitDiscount * item.Gst) + (item.Gst * 100));
                            item.revCostPrice = (item.unitMRP * 100 * 100) / ((item.revUnitDiscount * 100) + 10000 + (item.revUnitDiscount * item.Gst) + (item.Gst * 100));
                            item.netPrice = item.costPrice * (1 + item.Gst / 100);
                            item.revnetPrice = item.revCostPrice * (1 + item.Gst / 100);
                            item.marginAmount = item.unitMRP - item.netPrice;
                            item.revmarginAmount = item.unitMRP - item.revnetPrice;
                            item = $scope.handlePrecision(item, 2);
                        }
                        if (!item.selectedVendorID || item.selectedVendorID == 0) {
                            $scope.allItemsSelected = false;
                        }
                        if ($scope.auctionItem.status == "Negotiation Ended" || $scope.auctionItem.status == "Vendor Selected" || $scope.auctionItem.status == "PO Processing") {
                            if (item.selectedVendorID > 0) {
                                var vendorArray = $filter('filter')($scope.auctionItem.auctionVendors, { vendorID: item.selectedVendorID });
                                if (vendorArray.length > 0) {
                                    item.selectedVendor = vendorArray[0];
                                }
                            } else {
                                item.selectedVendor = $scope.auctionItem.auctionVendors[0];
                            }
                        }

                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                if (subItem.IS_VALID === 1 && (subItem.HAS_PRICE === 1 || subItem.HAS_SPECIFICATION === 1 || subItem.HAS_QUANTITY || subItem.HAS_TAX || subItem.DESCRIPTION !== null)) {
                                    item.expanded = true;
                                }
                            });
                        }

                    });

                    if (!$scope.auctionItem.isStopped && $scope.auctionItem.timeLeft > 0) {
                        $scope.disableAddButton = false;
                    }

                    if (callerID == $scope.currentUserId || callerID == undefined) {
                        $scope.revtotalprice = $scope.auctionItem.auctionVendors[0].revPrice;

                        $scope.revtotalprice = $scope.precisionRound(parseFloat($scope.revtotalprice), $rootScope.companyRoundingDecimalSetting);

                        $scope.revvendorBidPrice = $scope.auctionItem.auctionVendors[0].revVendorTotalPrice;
                    }

                    $scope.starttimecondition1 = $scope.auctionItem.auctionVendors[0].isQuotationRejected;


                    $scope.revQuotationUrl1 = $scope.auctionItem.auctionVendors[0].revquotationUrl;

                    if ($scope.auctionItem.auctionVendors[0].companyName == 'PRICE_CAP') {
                        $scope.revQuotationUrl1 = $scope.auctionItem.auctionVendors[1].revquotationUrl;
                    }

                    $scope.L1QuotationUrl = $scope.auctionItem.auctionVendors[0].quotationUrl;


                    //= $filter('filter')($scope.auctionItem.auctionVendors, {revquotationUrl > ''});
                    //$scope.RevQuotationfirstvendor 
                    // $scope.isRejectedPOEnable = true;
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        if (item.revquotationUrl != '') {
                            $scope.RevQuotationfirstvendor = item.revquotationUrl;
                        }
                        //if (item.isRevQuotationRejected == 1) {
                        //    $scope.isRejectedPOEnable = false;
                        //} else {
                        //    $scope.isRejectedPOEnable = true;
                        //}

                    });
                }

                if (!$scope.isCustomer) {
                    if ($scope.auctionItem.biddingType != 'SPOT') {
                        $scope.auctionItem.auctionVendors[0].showIncoTerm = $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0;
                    } else {
                        $scope.auctionItem.auctionVendors[0].showIncoTerm = $scope.auctionItem.auctionVendors[0].initialPrice > 0 ? ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 ? false : true) : true;
                    }

                    $scope.auctionItem.auctionVendors[0].displayTax = $scope.evaluateBiddingTypeBasedOnStatus();
                }

                if ($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length >= 2) {
                    $scope.starttimecondition2 = $scope.auctionItem.auctionVendors[1].isQuotationRejected;
                }
                //$scope.vendorID = $scope.auctionItem.auctionVendors[0].vendorID;

                $scope.participatedVendors = $scope.auctionItem.auctionVendors.filter(function (vendor) {
                    return (vendor.runningPrice > 0 && vendor.companyName != 'PRICE_CAP');
                });

                $scope.revisedParticipatedVendors = $scope.participatedVendors.filter(function (vendor) {
                    return (vendor.revquotationUrl != null && vendor.revquotationUrl != '' && vendor.companyName != 'PRICE_CAP');
                });

                $scope.revisedApprovedVendors = $scope.participatedVendors.filter(function (vendor) {
                    return (vendor.revquotationUrl != null && vendor.revquotationUrl != '' && vendor.companyName != 'PRICE_CAP' && vendor.isRevQuotationRejected == 0);
                });


                if ($scope.auctionItem.status != 'DELETED' && $scope.auctionItem.status != 'UNCONFIRMED' && $scope.auctionItem.status != 'STARTED' && $scope.auctionItem.status != 'NOTSTARTED' && $scope.auctionItem.status != 'Negotiation Ended') {
                    auctionsService.getpodetails({ "reqid": $stateParams.Id, "sessionid": $scope.currentSessionId, 'userid': $scope.currentUserId })
                        .then(function (response) {
                            $scope.poDetails = response;
                            //$scope.updatedeliverydateparams.date = $scope.poDetails.expectedDelivery;
                            if (response != undefined) {
                                //var date = $scope.poDetails.expectedDelivery.split('+')[0].split('(')[1];
                                var date1 = moment($scope.poDetails.expectedDelivery).format('DD/MM/YYYY  HH:mm');
                                $scope.updatedeliverydateparams.date = date1;

                                //var date1 = $scope.poDetails.paymentScheduleDate.split('+')[0].split('(')[1];
                                //var newDate1 = new Date(parseInt(parseInt(date1)));
                                var date2 = moment($scope.poDetails.expPaymentDate).format('DD/MM/YYYY  HH:mm');
                                $scope.updatepaymentdateparams.date = date2;
                                if ($scope.updatepaymentdateparams.date == '31/12/9999') {
                                    $scope.updatepaymentdateparams.date = 'Payment Date';
                                }
                            }
                        });
                }

                //  $scope.auctionItem.description = $scope.auctionItem.description.replace("\u000a", "\n")
                $scope.description = $scope.auctionItem.description.replace(/\u000a/g, "</br>");
                $scope.deliveryLocation = $scope.auctionItem.deliveryLocation.replace(/\u000a/g, "</br>");
                $scope.paymentTerms = $scope.auctionItem.paymentTerms.replace(/\u000a/g, "</br>");
                $scope.deliveryTime = $scope.auctionItem.deliveryTime.replace(/\u000a/g, "</br>");


                //var id = $scope.currentUserId;
                var result = $scope.auctionItem.auctionVendors.filter(function (obj) {
                    return obj.vendorID == $scope.currentUserId;
                });

                if ($scope.currentUserId != $scope.auctionItem.customerID && $scope.currentUserId != $scope.auctionItem.superUserID && !$scope.auctionItem.customerReqAccess && result.length == 0) {
                    swal("Access denied", "You do not have access to this requirement because you are not part of this requirements process.", 'error');
                    $state.go('home');
                } else {
                    $scope.setFields();

                }


                $scope.quotationApprovedColor = {};
                $scope.quotationNotApprovedColor = {};

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors[0] && $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 && $scope.auctionItem.isUnitPriceBidding == 1) {
                    $scope.quotationApprovedColor = {
                        'background-color': '#f5b2b2'
                    };
                    $scope.quotationNotApprovedColor = {};
                } else if ($scope.auctionItem.isUnitPriceBidding == 1) {
                    $scope.quotationNotApprovedColor = {
                        'background-color': '#f5b2b2'
                    };
                    $scope.quotationApprovedColor = {};
                }

                $scope.getMinReductionAmount();
                if ($scope.isCustomer) {
                    // $scope.getpricecomparison(); // disable as of now, as UI is commented out.
                }

                if ($scope.isCustomer) {
                    $scope.listRequirementItemsTemp = $scope.auctionItem.listRequirementItems;
                } else {
                    $scope.listRequirementItemsTemp = $scope.auctionItemVendor.listRequirementItems;
                }

                $scope.totalItems = ($scope.listRequirementItemsTemp) ? $scope.listRequirementItemsTemp.length : 0;
            };



            if ($scope.isCustomer) {
                PRMForwardBidService.GetIsAuthorized($scope.currentUserId, $scope.reqId, $scope.currentSessionId)
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                            $state.go('home');
                            return false;
                        }

                        $scope.getData();
                    });
            } else {
                $scope.getData();
            }

            //$scope.GetBidHistory();

            $scope.AmountSaved = 0;

            $scope.selectItemVendor = function (itemID, vendorID) {
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: $scope.currentUserId,
                    itemID: itemID,
                    vendorID: vendorID,
                    sessionID: $scope.currentSessionId
                };
                PRMForwardBidService.itemwiseselectvendor(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            var index = -1;
                            for (var i = 0, len = $scope.auctionItemVendor.listRequirementItems.length; i < len; i++) {
                                if ($scope.auctionItemVendor.listRequirementItems[i].itemID === itemID) {
                                    index = i;
                                    $scope.auctionItemVendor.listRequirementItems[i].selectedVendorID = vendorID;
                                    break;
                                }
                                if ($scope.auctionItemVendor.listRequirementItems[i].selectedVendor == 0) {
                                    $scope.allItemsSelected = false;
                                } else {
                                    $scope.allItemsSelected = true;
                                }
                            }
                            swal("Success!", "This item has been assigned to Vendor", "success");
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })
            }

            $scope.handlePrecision = function (item, precision) {
                item.unitDiscount = $scope.precisionRound(parseFloat(item.unitDiscount), precision + 6);
                item.revUnitDiscount = $scope.precisionRound(parseFloat(item.revUnitDiscount), precision + 6);
                return item;
            };

            $scope.$watch('auctionItemVendor.listRequirementItems', function (oldVal, newVal) {
                $log.debug("items list has changed");
                if (!newVal) { return; }
                newVal.forEach(function (item, index) {
                    if (!item.selectedVendorID || item.selectedVendorID == 0) {
                        $scope.allItemsSelected = false;
                    }
                });
            });

            $scope.generatePDFonHTML = function () {
                auctionsService.getdate()
                    .then(function (response) {
                        var date = new Date(parseInt(response.substr(6)));
                        var obj = $scope.auctionItem;
                        $scope.POTemplate = "<div id='POTemplate' style='display:none;'><html><head><title>PRM360</title><style>.date{margin-left: 850px;}.to{margin-left: 250px;}.name{margin-left: 300px;}.sub{margin-left: 450px;}img{position: absolute; left: 750px; top:75px; z-index: -1;}</style></head><body><header><br><br><br><img src='acads360.jpg' width='50' height='50'><h1 align='center'>PRM360<img </h1></header><br><div class='date'><p><b>Date:</b> " + date + "</p><p><b>PO No:</b> " + obj.requirementID + "</p></div><div class='to'><p>To,</p><p><b>" + obj.CompanyName + ",</b></p><p><b>" + obj.deliveryLocation + ".</b></p></div><p class='name'><b>Hello </b> " + obj.auctionVendors[0].vendorName + "</p><p class='sub'><b>Sub:</b> " + obj.title + "</p><p align='center'><b>Bill of Material</b></p><table border='1' cellpadding='2' style='width:60%' align='center'><tr><th>Product Name</th><th>Description</th><th>Price</th></tr><tr><td>" + obj.title + "</td><td>" + obj.description + "</td><td>" + obj.price + "</td></tr></table><p class='to'><b>Terms & Conditions</b></p><div class='name'> <p>1. Payment : " + obj.paymentTerms + ".</p><p>2. Delivery : " + obj.deliveryLocation + ".</p><p>3. Tax : " + obj.taxes + ".</p></div><p class='to'><b>Billing and Shipping Address:</b></p><p class='to'>Savvy Associates, # 3-4-174/21/2, Radha Krishna Nagar, Attapur, Hyderguda, Hyderabad - 500048</p><p align=center>This is a system generated PO, henceforth sign and seal is not required.</p><br><footer class='to'>M/s. Savvy Associates, H.No: 3-4-174/21/2, Radha Krishna Nagar, Attapur, Hyderguda, Hyderabad – 48Contact,M: 91-9949245791.,<br>E: savvyassociates@gmail.com.<br><b>URL:</b> www.savvyassociates.com. </footer></body></html></div>";
                        var content = document.getElementById('content');
                        content.insertAdjacentHTML('beforeend', $scope.POTemplate);
                    });
            };

            $scope.generatePOasPDF = function (divName) {
                $scope.generatePDFonHTML();
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;

                if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
                    var popupWin = window.open('', '_blank', 'width=600,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                    popupWin.window.focus();
                    popupWin.document.write('<!DOCTYPE html><html><head>' +
                        '<link rel="stylesheet" type="text/css" href="style.css" />' +
                        '</head><body onload="window.print()"><div class="reward-body">' + printContents + '</div></html>');
                    popupWin.onbeforeunload = function (event) {
                        popupWin.close();
                        return '.\n';
                    };
                    popupWin.onabort = function (event) {
                        popupWin.document.close();
                        popupWin.close();
                    }
                } else {
                    var popupWin = window.open('', '_blank', 'width=800,height=600');
                    popupWin.document.open();
                    popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</html>');
                    popupWin.document.close();
                }
                popupWin.document.close();
                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: $scope.currentUserId,
                    status: 'PO Processing',
                    type: "WINVENDOR",
                    sessionID: $scope.currentSessionId
                };
                PRMForwardBidService.updateStatus(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                            //doc.save("DOC.PDF");
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })

                return true;
            }

            $scope.generatePO = function () {
                var doc = new jsPDF();
                doc.setFontSize(40);
                //doc.text(40, 30, "Octocat loves jsPDF", 4);
                /*doc.fromHTML($("#POTemplate")[0], 15, 15, {
                    "width": 170,
                    function() {
                        $scope.POFile = $.makeArray(new Uint8Array(doc.output('arraybuffer')));
                    }
                })*/

                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: $scope.currentUserId,
                    status: 'PO Processing',
                    type: "WINVENDOR",
                    sessionID: $scope.currentSessionId
                };
                PRMForwardBidService.updateStatus(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                            doc.save("DOC.PDF");
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })
            }

            $scope.generatePOinServer = function () {
                if ($scope.POTemplate == "") {
                    $scope.generatePDFonHTML();

                }
                var doc = new jsPDF('p', 'in', 'letter');
                var specialElementHandlers = {};
                var doc = new jsPDF();
                //doc.setFontSize(40);
                doc.fromHTML($scope.POTemplate, 0.5, 0.5, {
                    'width': 7.5, // max width of content on PDF
                });
                //doc.save("DOC.PDF");
                doc.output("dataurl");
                $scope.POFile = $.makeArray(new Uint8Array(doc.output('arraybuffer')));
                var params = {
                    POfile: $scope.POFile,
                    reqid: $scope.auctionItem.requirementID,
                    userid: $scope.currentUserId,
                    POfileName: 'PO_req_' + $scope.auctionItem.requirementID + '.pdf',
                    sessionID: $scope.currentSessionId
                };

                auctionsService.generatePOinServer(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.showStatusDropDown = true;
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    });
            };

            $scope.updateStatus = function (status) {
                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: $scope.currentUserId,
                    status: status,
                    type: "ALLVENDORS",
                    sessionID: $scope.currentSessionId
                };
                PRMForwardBidService.updateStatus(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    });
            };

            $scope.uploadquotationsfromexcel = function (status) {
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: $scope.currentUserId,
                    sessionID: $scope.currentSessionId,
                    quotationAttachment: $scope.quotationAttachment
                };
                PRMForwardBidService.uploadquotationsfromexcel(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            location.reload();
                        } else {
                            swal("Error", response.errorMessage, 'error');
                        }
                    });
            };


            $scope.uploadclientsidequotation = function (type) {
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: $scope.currentUserId,
                    sessionID: $scope.currentSessionId,
                    quotationAttachment: $scope.quotationAttachment
                };
                PRMForwardBidService.uploadclientsidequotation(params)
                    .then(function (response) {

                        if (response && response.length > 0 && !response[0].errorMessage) {
                            var requirementItem = [];

                            requirementItem = response.filter(function (I) {
                                if (I.unitPrice < 0 || I.unitDiscount < 0 || I.unitMRP < 0 || I.revUnitDiscount < 0) {
                                    return I;
                                }
                            });

                            requirementItem;
                            if (requirementItem.length > 0) {
                                swal("Error", "Prices/Discounts should be greater than 0", "error");
                                return;
                            }
                        }


                        if (response && response.length > 0 && !response[0].errorMessage) {
                            var validation = true;
                            var itemID = 0;
                            for (item in $scope.auctionItem.listRequirementItems) {
                                item = (+item);
                                var newItem = $filter('filter')(response, { itemID: $scope.auctionItem.listRequirementItems[item].itemID })[0];
                                if (newItem) {
                                    if ($scope.auctionItem.status == "STARTED" && $scope.auctionItem.listRequirementItems[item].revUnitPrice > newItem.revUnitPrice && $scope.auctionItem.isDiscountQuotation == 0) {
                                        validation = false;
                                        itemID = newItem.itemID;
                                    } else if ($scope.auctionItem.status == "STARTED" && $scope.auctionItem.listRequirementItems[item].unitDiscount < newItem.revUnitDiscount && $scope.auctionItem.isDiscountQuotation == 1) {
                                        validation = false;
                                        itemID = newItem.itemID;
                                    }
                                }
                            }
                            if (validation) {
                                for (item in $scope.auctionItem.listRequirementItems) {
                                    item = (+item);
                                    newItem = $filter('filter')(response, { itemID: $scope.auctionItem.listRequirementItems[item].itemID })[0];
                                    if (newItem) {

                                        $scope.auctionItem.listRequirementItems[item].unitPrice = newItem.unitPrice;
                                        $scope.auctionItem.listRequirementItems[item].revUnitPrice = newItem.revUnitPrice;
                                        $scope.auctionItem.listRequirementItems[item].unitMRP = newItem.unitMRP;
                                        $scope.auctionItem.listRequirementItems[item].unitDiscount = newItem.unitDiscount;
                                        $scope.auctionItem.listRequirementItems[item].revUnitDiscount = newItem.revUnitDiscount;
                                        $scope.auctionItem.listRequirementItems[item].cGst = newItem.cGst;
                                        $scope.auctionItem.listRequirementItems[item].sGst = newItem.sGst;
                                        $scope.auctionItem.listRequirementItems[item].iGst = newItem.iGst;
                                        $scope.auctionItem.listRequirementItems[item].Gst = newItem.iGst + newItem.sGst + newItem.cGst;
                                        $scope.auctionItem.listRequirementItems[item].itemLevelInitialComments = newItem.itemLevelInitialComments;
                                        $scope.auctionItem.listRequirementItems[item].isRegret = newItem.isRegret;
                                        $scope.auctionItem.listRequirementItems[item].regretComments = newItem.regretComments;

                                        if ($scope.auctionItem.auctionVendors[0].selectedVendorCurrency != 'INR') {
                                            $scope.auctionItem.listRequirementItems[item].cGst = 0;
                                            $scope.auctionItem.listRequirementItems[item].sGst = 0;
                                            $scope.auctionItem.listRequirementItems[item].iGst = 0;
                                        }

                                        if ($scope.gstNumber && $scope.gstNumber.startsWith($scope.localGSTNumberCode)) {
                                            $scope.auctionItem.listRequirementItems[item].cGst = newItem.iGst / 2;
                                            $scope.auctionItem.listRequirementItems[item].sGst = newItem.iGst / 2;
                                            $scope.auctionItem.listRequirementItems[item].iGst = 0;
                                        }
                                        if (newItem.isRegret) {
                                            $scope.regretHandle($scope.auctionItem.listRequirementItems[item]);
                                        }

                                        if (newItem.productQuotationTemplateJson && !newItem.isRegret) {
                                            $scope.auctionItem.listRequirementItems[item].productQuotationTemplateArray = JSON.parse(newItem.productQuotationTemplateJson);

                                            if ($scope.auctionItem.listRequirementItems[item].productQuotationTemplateArray) {

                                                $scope.GetItemUnitPrice(false, -1);
                                                $scope.mrpDiscountCalculation();
                                                $scope.unitPriceCalculation('PRC');
                                                $scope.getTotalPrice($scope.discountAmount, $scope.freightCharges, $scope.totalprice,
                                                    $scope.packingCharges, $scope.packingChargesTaxPercentage, $scope.packingChargesWithTax,
                                                    $scope.installationCharges, $scope.installationChargesTaxPercentage, $scope.installationChargesWithTax,
                                                    $scope.freightChargesTaxPercentage, $scope.freightChargesWithTax);
                                                $scope.getRevTotalPrice($scope.revfreightCharges, $scope.revtotalprice,
                                                    $scope.revpackingCharges, $scope.packingChargesTaxPercentage, $scope.revpackingChargesWithTax,
                                                    $scope.revinstallationCharges, $scope.installationChargesTaxPercentage, $scope.revinstallationChargesWithTax,
                                                    $scope.freightChargesTaxPercentage, $scope.revfreightChargesWithTax);
                                            }
                                        }
                                    }
                                }
                            } else {
                                swal("Error", 'New bid for item cannot be lower than the old bid. Please check the values in the excel sheet for item: ' + itemID, 'error');
                            }

                            $scope.auctionItemVendor.listRequirementItems = $scope.auctionItem.listRequirementItems;
                            $scope.listRequirementItemsTemp = $scope.auctionItemVendor.listRequirementItems;
                            $scope.totalItems = $scope.listRequirementItemsTemp.length;
                            $scope.unitPriceCalculation('IGST');
                            $scope.getTotalPrice($scope.discountAmount, 0, $scope.totalprice);
                            $scope.getRevTotalPrice(0, $scope.revtotalprice);
                            swal("Verify", "Please verify and Submit your prices!", "success");
                            $(".fileinput").fileinput("clear");
                            $scope.fieldValidation();
                            return;
                        } else if (response && response.length > 0) {
                            swal("Error!", response[0].errorMessage, "error");
                        }

                        $("#clientsideupload").val(null);
                        $("#quotationBulkUpload").val(null);
                    });
            };

            $scope.getrevisedquotations = function () {
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: $scope.currentUserId,
                    sessionID: $scope.currentSessionId
                };
                PRMForwardBidService.getrevisedquotations(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            location.reload();
                        } else {
                            swal("Error", response.errorMessage, 'error');
                            console.log(response.errorMessage);
                        }
                    });
            };

            $scope.updatedeliverydateparams = {
                date: ''
            };

            $scope.updatedeliverdate = function () {
                var ts = moment($scope.updatedeliverydateparams.date, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                $scope.updatedeliverydateparams.date = "/Date(" + milliseconds + "000+0530)/";

                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: $scope.currentUserId,
                    date: $scope.updatedeliverydateparams.date,
                    type: "DELIVERY",
                    sessionID: $scope.currentSessionId
                };
                PRMForwardBidService.updatedeliverdate(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }
                    });
            };

            $scope.updatepaymentdateparams = {
                date: ''
            };

            $scope.updatepaymentdate = function () {
                var ts = moment($scope.updatepaymentdateparams.date, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                $scope.updatepaymentdateparams.date = "/Date(" + milliseconds + "000+0530)/";

                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: $scope.currentUserId,
                    date: $scope.updatepaymentdateparams.date,
                    type: "PAYMENT",
                    sessionID: $scope.currentSessionId
                };
                PRMForwardBidService.updatepaymentdate(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    });
            };

            $scope.RestartNegotiation = function () {
                var params = {};
                params.reqID = id;
                params.sessionID = $scope.currentSessionId;
                params.userID = $scope.currentUserId;
                var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId;
                $scope.isnegotiationended = false;
                $scope.NegotiationEnded = false;
                // $scope.invokeSignalR('RestartNegotiation', parties);


                PRMForwardBidService.checkForRestartNegotiation(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.invokeSignalR('RestartNegotiation', parties);
                        } else {
                            swal("Error", response.errorMessage, 'error');
                        }
                    });
            };

            $scope.saveComment = function () {
                var commentText = "";
                if ($scope.newComment && $scope.newComment != "") {
                    commentText = $socpe.newComment;
                } else if ($("#comment")[0].value != '') {
                    commentText = $("#comment")[0].value;
                } else {
                    $scope.commentsvalidation = true;
                }
                if (!$scope.commentsvalidation) {
                    var params = {};
                    auctionsService.getdate()
                        .then(function (response) {
                            var date = new Date(parseInt(response.substr(6)));
                            var myEpoch = date.getTime();
                            params.requirementID = id;
                            params.firstName = "";
                            params.lastName = "";
                            params.replyCommentID = -1;
                            params.commentID = -1;
                            params.errorMessage = "";
                            params.createdTime = "/Date(" + myEpoch + "+0000)/";
                            params.sessionID = $scope.currentSessionId;
                            params.userID = $scope.currentUserId;
                            params.commentText = commentText;
                            $scope.invokeSignalR('SaveComment', params);
                        });
                }
            };

            $scope.multipleAttachments = [];
            //$scope.multipleAttachmentsList = [];

            if (!$scope.multipleAttachmentsList) {
                $scope.multipleAttachmentsList = [];
            }

            $scope.getFile = function () {
                $scope.progress = 0;
                //var quotation = $("#quotation")[0].files[0];
                //if (quotation != undefined && quotation != '') {
                //    $scope.file = $("#quotation")[0].files[0];
                //    $log.info($("#quotation")[0].files[0]);
                //}

                //fileReader.readAsDataUrl($scope.file, $scope)
                //    .then(function (result) {
                //        var bytearray = new Uint8Array(result);
                //        if (quotation != undefined && quotation != '') {
                //            $scope.bidAttachement = $.makeArray(bytearray);
                //            $scope.bidAttachementName = $scope.file.name;
                //            $scope.enableMakeBids = true;
                //        }
                //    });

                $scope.multipleAttachments = $("#attachement")[0].files;
                $scope.multipleAttachments = Object.values($scope.multipleAttachments)

                $scope.multipleAttachments.forEach(function (item, index) {
                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;
                            if (!$scope.multipleAttachmentsList) {
                                $scope.multipleAttachmentsList = [];
                            }
                            $scope.multipleAttachmentsList.push(fileUpload);
                        });
                });

            };

            $scope.updateTime = function (time) {
                var isDone = false;
                $scope.disableDecreaseButtons = true;
                $scope.disableAddButton = true;
                $scope.$on('timer-tick', function (event, args) {
                    var temp = event.targetScope.countdown;

                    if (!isDone) {
                        if (time < 0 && temp + time < 0) {
                            growlService.growl("You cannot reduce the time when it is already below 60 seconds", "inverse");
                            isDone = true;
                            return false;
                        }

                        isDone = true;
                        var params = {};
                        params.reqID = id;
                        params.sessionID = $scope.currentSessionId;
                        params.userID = $scope.currentUserId;
                        params.newTicks = ($scope.countdownVal + time);
                        params.lotID = $scope.auctionItem.lotId;
                        var parties = id + "$" + $scope.currentUserId + "$" + params.newTicks + "$" + $scope.currentSessionId + "$" + params.lotID;
                        $scope.invokeSignalR('UpdateTime', parties, function () { console.log('time:' + time); });
                    }
                });
            };

            $scope.Loding = false;

            $scope.priceCapError = false;

            $scope.validateCurrencyRate = function () {
                let pendingQuotationsVendors = $scope.vendorsQuotationStatus();
                if (pendingQuotationsVendors && pendingQuotationsVendors.length > 0) {
                    swal("Warning!", "Please approve or reject vendors Quotation. Pending vendors: " + pendingQuotationsVendors, "error");
                } else {
                    var startValue = $("#startTimeDate").val(); //$scope.startTime; //Need fix on this.
                    if (startValue && startValue != null && startValue != "") {
                        PRMForwardBidService.validateCurrencyRate({ reqid: $scope.reqId, sessionid: $scope.currentSessionId })
                            .then(function (currencyValidation) {
                                //$scope.updateAuctionStart(currencyValidation.currentTime);
                                if (currencyValidation.objectID) {
                                    swal({
                                        title: "Warning Currency Factor...",
                                        text: "Currency factor has been changed since Requirement creation, please click Ok to continue or click Cancel to re-calculate.",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#F44336",
                                        confirmButtonText: "OK",
                                        closeOnConfirm: false
                                    }, function (isConfirm) {
                                        if (!isConfirm) {
                                            $scope.showCurrencyRefresh = true;
                                        } else {
                                            $scope.updateAuctionStart(currencyValidation.currentTime);
                                        }
                                    });
                                } else {
                                    $scope.updateAuctionStart(currencyValidation.currentTime);
                                }
                            });
                    } else {
                        alert("Please enter the date and time to update Start Time to.");
                    }
                }
            };


            $scope.saveReqNegSettings = function () {
                $scope.NegotiationSettingsValidationMessage = '';
                $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);
                if ($scope.NegotiationSettingsValidationMessage) {
                    $scope.Loding = false;
                    return;
                }

                if ($scope.formRequest.reductionLevel && $scope.formRequest.reductionLevel === 'OVERALL' &&
                    $scope.NegotiationSettings.minReductionAmount && +$scope.NegotiationSettings.minReductionAmount > 25) {
                    $scope.Loding = false;
                    swal("Warning!", "Overall incremental % cannot be more than 25%.", "error");
                    return;
                }

                var params = {};
                params.reqID = $scope.auctionItem.requirementID;
                params.sessionID = $scope.currentSessionId;
                $scope.NegotiationSettings.negotiationDuration = $scope.days + ':' + $scope.hours + ':' + $scope.mins + ':00.0';
                params.NegotiationSettings = $scope.NegotiationSettings;

                PRMForwardBidService.SaveReqNegSettings(params)
                    .then(function (response) {
                        if (response.SaveReqNegSettingsResult) {
                            if (response.SaveReqNegSettingsResult.errorMessage == '') {
                                swal("Done!", "Saved Successfully.", "success");
                            } else {
                                swal("Error", response.SaveReqNegSettingsResult.errorMessage, "inverse");
                            }
                        } else {
                            swal("Error", response.SaveReqNegSettingsResult.errorMessage, "inverse");
                        }

                    });

            };

            $scope.updateAuctionStart = function (timeToValidate) {
                $scope.priceCapError = false;
                $scope.showCurrencyRefresh = false;
                $scope.Loding = true;
                $scope.NegotiationSettingsValidationMessage = '';
                $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);
                if ($scope.NegotiationSettingsValidationMessage) {
                    $scope.Loding = false;
                    return;
                }

                if ($scope.formRequest.reductionLevel && $scope.formRequest.reductionLevel === 'OVERALL' &&
                    $scope.NegotiationSettings.minReductionAmount && +$scope.NegotiationSettings.minReductionAmount > 25) {
                    $scope.Loding = false;
                    swal("Warning!", "Overall incremental % cannot be more than 25%.", "error");
                    return;
                }

                var params = {};
                params.auctionID = id;
                var startValue = $("#startTimeDate").val(); //$scope.startTime; //Need fix on this.
                if (startValue && startValue != null && startValue != "") {
                    //var ts = moment($scope.startTime, "DD-MM-YYYY HH:mm").valueOf();
                    var ts = moment(startValue, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var auctionStartDate = new Date(m);

                    var CurrentDateToLocal = userService.toLocalDate(timeToValidate);
                    var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";
                    var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));
                    $log.debug(CurrentDate < auctionStartDate);
                    $log.debug('div' + auctionStartDate);
                    if (CurrentDate >= auctionStartDate) {
                        $scope.Loding = false;
                        swal("Done!", "Your Negotiation Start Time should be greater than current time.", "error");
                        return;
                    }

                    milliseconds = parseInt(auctionStartDate.getTime() / 1000.0);
                    // // #INTERNATIONALIZATION-0-2019-02-12
                    params.postedOn = "/Date(" + userService.toUTCTicks(startValue) + "+0530)/";
                    params.auctionEnds = "/Date(" + userService.toUTCTicks(startValue) + "+0530)/";
                    params.customerID = $scope.currentUserId;
                    params.sessionID = $scope.currentSessionId;

                    $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                    params.NegotiationSettings = $scope.NegotiationSettings;

                    if ($scope.starreturn) {
                        $scope.Loding = false;
                        swal("Not Allowed", "You are not allowed to schedule the Negotiation time until all the Vendor Quotations are validated. (They should be either approved/rejected). The following vendors quotations are yet to be validated. (" + $scope.notviewedcompanynames + ")", "error");
                        return;
                    }

                    if ($scope.auctionItem.auctionVendors.length == 0 || !$scope.auctionItem.auctionVendors[0] || !$scope.auctionItem.auctionVendors[1] || $scope.auctionItem.auctionVendors[0].quotationUrl == "" || $scope.auctionItem.auctionVendors[1].quotationUrl == "" || $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 || $scope.auctionItem.auctionVendors[1].isQuotationRejected != 0 || $scope.starttimecondition1 != 0 || $scope.starttimecondition2 != 0) {
                        $scope.Loding = false;
                        swal("Not Allowed", "You are not allowed to create a start time until at least 2 vendors Approved.", "error");
                        return;
                    }
                    if ($scope.auctionItem.auctionVendors[0].companyName == "") {
                        $scope.Loding = false;
                        swal("Not Allowed", "You are not allowed to create a start time until at least 2 vendors Approved.", "error");
                        return;
                    }
                    if ($scope.priceCapError == true) {
                        $scope.Loding = false;
                        swal("Not Allowed", "Price CAP Value should be less then all the vendors price.", "error");
                        return;
                    }
                    else {
                        $scope.Loding = true;
                        requirementHub.invoke('UpdateAuctionStartSignalR', params, function (req) {
                            $scope.Loding = false;
                            swal("Done!", "Your Negotiation Start Time Updated Successfully!", "success");
                            $scope.Loding = false;
                            var start = $scope.auctionItem.startTime.split('+')[0].split('(')[1];
                            auctionsService.getdate()
                                .then(function (response) {
                                    //var curDate = new Date(parseInt(response.substr(6)));

                                    var CurrentDateToLocal = userService.toLocalDate(response);

                                    var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                                    var m = moment(ts);
                                    var deliveryDate = new Date(m);
                                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                                    var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                                    //var curDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));
                                    var curDate = new Date(parseInt(CurrentDateToTicks.substr(6)));
                                    var myEpoch = curDate.getTime();
                                    if (($scope.auctionItem.customerID == $scope.currentUserId || $scope.auctionItem.superUserID == $scope.currentUserId || $scope.auctionItem.customerReqAccess) && +start > myEpoch) {
                                        $scope.startBtns = true;
                                        $scope.customerBtns = false;
                                    } else {

                                        $scope.startBtns = false;
                                        $scope.disableButtons();
                                        $scope.customerBtns = true;
                                    }

                                    if ($scope.auctionItem.timeLeft == null || $scope.auctionItem.timeLeft < 0) {
                                        $scope.showTimer = false;
                                    } else {

                                        $scope.showTimer = true;
                                    }
                                });
                        });
                    }

                    $scope.Loding = false;
                } else {
                    $scope.Loding = false;
                    alert("Please enter the date and time to update Start Time to.");
                }

                $scope.Loding = false;
            };



            $scope.makeaBidLoding = false;
            $scope.makeaBid = function (call, isReviced) {
                $scope.makeaBidLoding = true;

                var bidPrice = 0;

                if (isReviced == 0) {
                    bidPrice = $("#quotationamount").val();
                }
                if (isReviced == 1) {

                    bidPrice = $("#revquotationamount").val();
                }

                var params = {};
                params.reqID = parseInt(id);
                params.sessionID = $scope.currentSessionId;
                params.userID = $scope.currentUserId;

                params.price = $scope.precisionRound(parseFloat(bidPrice), $rootScope.companyRoundingDecimalSetting);
                params.quotation = $scope.bidAttachement;
                params.quotationName = $scope.bidAttachementName;
                params.discountAmount = $scope.discountAmount;

                params.tax = $scope.vendorTaxes;

                params.warranty = $scope.warranty;
                params.duration = $scope.duration;
                params.payment = $scope.payment;
                params.gstNumber = $scope.gstNumber;
                params.validity = $scope.validity;

                params.otherProperties = $scope.otherProperties;

                params.quotationType = 'USER';
                params.type = 'quotation';

                params.listRequirementTaxes = $scope.listRequirementTaxes;

                params.quotationObject = $scope.auctionItemVendor.listRequirementItems;

                if (isReviced == 0) {
                    params.revised = isReviced;
                    params.priceWithoutTax = $scope.totalprice;
                    params.price = $scope.vendorBidPrice;
                    params.discountAmount = $scope.discountAmount;
                }
                if (isReviced == 1) {
                    params.revised = isReviced;
                    params.priceWithoutTax = $scope.revtotalprice;
                    params.price = $scope.revvendorBidPrice;
                    params.discountAmount = $scope.discountAmount;
                }

                if (($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isQuotationPriceLimit && $scope.vendorBidPrice < $scope.auctionItem.quotationPriceLimit) {
                    swal("Error!", 'Your Amount Should be greater than or equal to Quotation min. Price limit :' + $scope.auctionItem.quotationPriceLimit, "error");
                    return;
                }

                auctionsService.getdate()
                    .then(function (responseFromServer) {
                        var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                        var auctionStart = new Date(parseInt($scope.auctionItem.startTime.substr(6)));
                        var timeofauctionstart = auctionStart.getTime();
                        var currentServerTime = dateFromServer.getTime();

                        if ($scope.auctionItem.status == "Negotiation Ended") {
                            PRMForwardBidService.makeabid(params).then(function (req) {
                                if (req.errorMessage == '') {

                                    swal({
                                        title: "Thanks!",
                                        text: "Your Quotation has been Uploaded Successfully",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            location.reload();
                                        });

                                    $("#quotationamount").val("");
                                    $scope.quotationStatus = true;
                                    $scope.auctionStarted = false;
                                    $scope.getData();
                                    $scope.makeaBidLoding = false;
                                } else {
                                    $scope.makeaBidLoding = false;
                                    swal({
                                        title: "Error",
                                        text: req.errorMessage,
                                        type: "error",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    });
                                }
                            });

                        } else {
                            if (timeofauctionstart - currentServerTime >= 3600000 && $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                                // $scope.DeleteRequirementTerms();


                                if ($scope.auctionItem.status == 'Negotiation Ended') {
                                } else {
                                    // $scope.SaveRequirementTerms($scope.reqId);
                                }

                                PRMForwardBidService.makeabid(params).then(function (req) {
                                    if (req.errorMessage == '') {

                                        swal({
                                            title: "Thanks!",
                                            text: "Your Quotation has been Uploaded Successfully",
                                            type: "success",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        },
                                            function () {
                                                location.reload();
                                            });

                                        $("#quotationamount").val("");
                                        $scope.quotationStatus = true;
                                        $scope.auctionStarted = false;
                                        $scope.getData();
                                        $scope.makeaBidLoding = false;
                                    } else {
                                        $scope.makeaBidLoding = false;
                                        swal({
                                            title: "Error",
                                            text: req.errorMessage,
                                            type: "error",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        });
                                    }
                                });
                            }

                            else {

                                var message = "You have missed the deadline for Quotation Submission";
                                if ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 && $scope.auctionItem.status != "Negotiation Ended") { message = "Your Quotation Already Approved You cant Update Quotation Please Contact PRM360" }

                                $scope.makeaBidLoding = false;
                                swal({
                                    title: "Error",
                                    text: message,
                                    type: "error",
                                    showCancelButton: false,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: true
                                });
                                $scope.getData();
                            }



                        }
                    });
            };

            $scope.revquotationupload = function () {
                $scope.makeaBidLoding = true;
                if (($scope.bidAttachementName == "" || $scope.bidAttachement.length == 0) && $scope.quotationStatus == false) {
                    $scope.bidAttachementValidation = true;
                    $scope.makeaBidLoding = false;
                    return false;
                } else {
                    $scope.bidAttachementValidation = false;
                }
                var params = {};
                params.reqID = parseInt(id);
                params.sessionID = $scope.currentSessionId;
                params.userID = $scope.currentUserId;
                params.price = 0;
                params.quotation = $scope.bidAttachement;
                params.quotationName = $scope.bidAttachementName;
                params.tax = 0;
                PRMForwardBidService.revquotationupload(params).then(function (req) {
                    if (req.errorMessage == '') {
                        swal({
                            title: "Thanks!",
                            text: "Your Revised Quotation Uploaded Successfully",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                location.reload();
                            });
                        $("#quotationamount1").val("");
                        $scope.quotationStatus = true;
                        $scope.auctionStarted = false;
                        $scope.getData();
                        $scope.makeaBidLoding = false;
                    } else {
                        $scope.makeaBidLoding = false;
                        swal("Error!", req.errorMessage, "error");
                    }
                });
            };

            $scope.$on('timer-tick', function (event, args) {
                if (event.targetScope.seconds == 5 ||
                    event.targetScope.seconds == 15 ||
                    event.targetScope.seconds == 25 ||
                    event.targetScope.seconds == 35 ||
                    event.targetScope.seconds == 45 ||
                    event.targetScope.seconds == 55) {

                }
                if ($scope.auctionItem.IS_CB_ENABLED == false || $scope.auctionItem.IS_CB_ENABLED == 0) {
                    $scope.countdownVal = event.targetScope.countdown;
                    if (event.targetScope.countdown < 61 && !$scope.disableStopBids && !$scope.disablereduceTime) {
                        $timeout($scope.disableButtons(), 1000);
                    }
                    if (event.targetScope.countdown > 60) {
                        $timeout($scope.enableButtons(), 1000);
                    }
                    if (event.targetScope.countdown == 60 || event.targetScope.countdown == 59 || event.targetScope.countdown == 30 || event.targetScope.countdown == 29) {
                        //var msie = $(document) [0].documentMode;
                        var ua = window.navigator.userAgent;
                        var msieIndex = ua.indexOf("MSIE ");
                        var msieIndex2 = ua.indexOf("Trident");
                        // if is IE (documentMode contains IE version)
                        var params = {};
                        params.reqID = id;
                        params.sessionID = $scope.currentSessionId;
                        params.userID = $scope.currentUserId;
                        //var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId;
                        //requirementHub.invoke('CheckRequirement', parties, function () {
                        //    $scope.getData();
                        //    //$scope.$broadcast('timer-set-countdown-seconds', 0);
                        //    //swal("Negotiation Completed!", "Congratulations! you procurement process is now completed. " + $scope.toprankerName + " is the least bider with the value " + $scope.auctionItem.minPrice + " \n Your savings through PRM :" + ($scope.vendorInitialPrice - $scope.auctionItem.minPrice), "success");
                        //});
                        if (msieIndex > 0 || msieIndex2 > 0) {
                            $scope.getData();
                        }
                    }
                    if (event.targetScope.countdown <= 0) {
                        if ($scope.auctionStarted && ($scope.auctionItem.status == "CLOSED" || $scope.auctionItem.status == "STARTED")) {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0].runningPrice;
                        } else if ($scope.auctionItem.status == "NOTSTARTED") {
                            var params = {};
                            params.reqID = id;
                            params.sessionID = $scope.currentSessionId;
                            params.userID = $scope.currentUserId;

                            PRMForwardBidService.StartNegotiation(params)
                                .then(function (response) {
                                    if (response.errorMessage == '') {
                                        $scope.getData();
                                    }
                                })

                            var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId;
                            $scope.invokeSignalR('CheckRequirement', parties);
                        }
                    }

                    if (event.targetScope.countdown <= 120) {
                        $scope.timerStyle = {
                            'color': '#f00',
                            '-webkit - animation': 'flash linear 1s infinite',
                            'animation': 'flash linear 1s infinite'
                        };
                    }
                    //$log.info($('#reqTitle').visible());
                    //console.log($('#reqTitle').visible());
                    $scope.timerFloat = $('#reqTitle').show();
                    if ($scope.auctionItem.status == 'STARTED' && !$scope.timerFloat) {
                        $scope.divfix = {
                            'bottom': '2%',
                            'left': '3%',
                            'position': 'fixed',
                            'z-index': '3000'
                        };
                        $scope.divfix3 = {
                            'bottom': '9%',
                            'left': '3%',
                            'position': 'fixed',
                            'z-index': '3000'
                        };
                        $scope.divfix1 = {
                            'bottom': '8%',
                            'left': '3%',
                            'position': 'fixed',
                            'z-index': '3000'
                        };
                        $scope.divfix2 = {
                            'bottom': '13%',
                            'left': '3%',
                            'position': 'fixed',
                            'z-index': '3000'
                        };
                        $scope.divfixMakeabid = {
                            'bottom': '14%',
                            'left': '10%',
                            'position': 'fixed',
                            'z-index': '3000'
                        };
                        $scope.divfixMakeabidError = {
                            'bottom': '13%',
                            'left': '18%',
                            'position': 'fixed',
                            'z-index': '3000',
                            'background-color': 'lightgrey'
                        };

                        if (!$scope.isCustomer) {
                            $scope.boxfix = {
                                'background-color': 'lightgrey',
                                'width': '280px',
                                'height': '120px',
                                //'border': '25px solid green',
                                'padding': '25px',
                                'margin': '25px',


                                'bottom': '-2%',
                                'left': '0%',
                                'position': 'fixed',
                                'z-index': '3000'
                            }
                        }
                        else {
                            $scope.boxfix = {
                                'background-color': 'lightgrey',
                                'width': '280px',
                                'height': '90px',
                                //'border': '25px solid green',
                                'padding': '25px',
                                'margin': '25px',


                                'bottom': '-2%',
                                'left': '0%',
                                'position': 'fixed',
                                'z-index': '3000'
                            }
                        }

                    }
                    else {
                        $scope.divfix = {};
                        $scope.divfix3 = {};
                        $scope.divfix1 = {};
                        $scope.divfix2 = {};
                        $scope.divfixMakeabid = {};
                        $scope.divfixMakeabidError = {};
                        $scope.boxfix = {};
                    };
                    if ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') {

                        if ($scope.auctionItem.isUnitPriceBidding == 1 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isTabular == true) {
                            $scope.divBorder = {
                                'background-color': '#f5b2b2'
                            };
                        }

                        if ($scope.auctionItem.isUnitPriceBidding == 0 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isTabular == true) {
                            $scope.divBorder1 = {
                                'background-color': '#f5b2b2'
                            };
                        }
                    }
                    if (event.targetScope.countdown > 120 && $scope.auctionItem.status == 'NOTSTARTED') {
                        $scope.timerStyle = { 'color': '#000' };
                    }
                    if (event.targetScope.countdown > 120 && $scope.auctionItem.status != 'NOTSTARTED') {
                        $scope.timerStyle = { 'color': '#228B22' };
                    }
                    if (event.targetScope.countdown <= 60 && $scope.auctionItem.status == 'STARTED') {
                        $scope.disableDecreaseButtons = true;
                    }
                    if (event.targetScope.countdown > 60 && $scope.auctionItem.status == 'STARTED') {
                        $scope.disableDecreaseButtons = false;
                    }
                    if (event.targetScope.countdown <= 0 && $scope.auctionItem.status == 'NOTSTARTED') {
                        $scope.showTimer = false;

                        //$scope.getData();
                        window.location.reload();
                    }
                    if (event.targetScope.countdown < 1 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'Negotiation Ended')) {
                        //var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId;
                        //$scope.invokeSignalR('CheckRequirement', parties);  
                        $scope.showTimer = false;
                        $scope.auctionItem.status = 'Negotiation Ended';

                        swal({
                            title: "Thanks!",
                            text: "Negotiation complete! Thank you for being a part of this.",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                location.reload();
                                $log.info("Negotiation Ended.");
                            });
                    }

                    if (event.targetScope.countdown <= 3) {
                        $scope.disableAddButton = true;
                    }
                    if (event.targetScope.countdown < 6) {
                        $scope.disableBidButton = true;
                    }
                    if (event.targetScope.countdown > 3) {
                        //$scope.getData();
                        $scope.disableAddButton = false;
                    }
                    if (event.targetScope.countdown >= 6) {
                        $scope.disableBidButton = false;
                    }
                }
                else if ($scope.auctionItem.IS_CB_ENABLED == true || $scope.auctionItem.IS_CB_ENABLED == 1) {
                    //var temp = event.targetScope.countdown;
                    //console.log(temp);
                    //$scope.auctionItem.CB_TIME_LEFT = $scope.auctionItem.CB_TIME_LEFT - 1;

                    if (event.targetScope.minutes < 2) {
                        $scope.timerStyle = {
                            'color': '#f00',
                            '-webkit - animation': 'flash linear 1s infinite',
                            'animation': 'flash linear 1s infinite'
                        };
                    }

                    if (event.targetScope.minutes >= 2 || event.targetScope.days > 0 || event.targetScope.hours > 0) {
                        $scope.timerStyle = { 'color': '#228B22' };
                    }


                    if (event.targetScope.days == 0 &&
                        event.targetScope.hours == 0 &&
                        event.targetScope.minutes == 0 &&
                        (event.targetScope.seconds == 1)) {
                        location.reload();
                    }
                }

            });


            requirementHub.on('timerUpdateSignalR', function (payloadObj) {
                if (payloadObj && payloadObj.payLoad && payloadObj.payLoad.length > 0) {
                    $scope.auctionItem.timeLeft = +payloadObj.payLoad[0];
                    $scope.$broadcast('timer-set-countdown-seconds', +payloadObj.payLoad[0]);
                }
            });

            requirementHub.on('timerUpdateLotSignalR', function (payloadObj) {
                if (payloadObj && payloadObj.payLoad && payloadObj.payLoad.length > 0 && $scope.auctionItem && $scope.auctionItem.status !== 'STARTED') {
                    window.location.reload();
                }
            });

            requirementHub.on('checkRequirement', function (items) {
                var req = items.inv;
                var iteminvcallerID = 0;
                if ($scope.auctionItem.IS_CB_ENABLED == false || $scope.auctionItem.IS_CB_ENABLED == 0) {
                    if (items.inv) {
                        iteminvcallerID = items.inv.callerID;
                    }
                    if ($scope.isCustomer) {
                        var itemTemp = _.filter(items.payLoad, function (userItem) {
                            if (userItem.superUserID === $scope.auctionItem.superUserID) {
                                $scope.signalRCustomerAccess = true;
                            }

                            return userItem.superUserID === $scope.auctionItem.superUserID;
                        });
                    }
                    else {
                        var itemTemp = _.filter(items.payLoad, function (userItem) {
                            return userItem.entityID === +$scope.currentUserId;
                        });
                    }
                    let item = itemTemp[0];
                    $scope.setAuctionInitializer(item, '', iteminvcallerID);
                    $scope.$broadcast('timer-set-countdown-seconds', $scope.auctionItem.timeLeft);
                    $('.datetimepicker').datetimepicker({
                        useCurrent: false,
                        icons: {
                            time: 'glyphicon glyphicon-time',
                            date: 'glyphicon glyphicon-calendar',
                            up: 'glyphicon glyphicon-chevron-up',
                            down: 'glyphicon glyphicon-chevron-down',
                            previous: 'glyphicon glyphicon-chevron-left',
                            next: 'glyphicon glyphicon-chevron-right',
                            today: 'glyphicon glyphicon-screenshot',
                            clear: 'glyphicon glyphicon-trash',
                            close: 'glyphicon glyphicon-remove'

                        },

                        minDate: item.currentTime
                    });
                    if (!$scope.NegotiationEnded) {
                        $scope.$broadcast('timer-start');
                    }
                    if (id == req.requirementID && ($scope.signalRCustomerAccess || $scope.currentUserId == req.customerID || $scope.currentUserId == req.superUserID || req.userIDList.indexOf($scope.currentUserId) > -1 || req.custCompID == $scope.currentUserCompID)) {
                        if (req.methodName == "UpdateTime" && req.userIDList.indexOf($scope.currentUserId) > -1) {
                            growlService.growl("Negotiation time has been updated.", 'inverse');
                        } else if (req.methodName == "MakeBid" && ($scope.signalRCustomerAccess || $scope.currentUserId == req.customerID || req.superUserID == $scope.currentUserId || $scope.auctionItem.customerReqAccess || req.custCompID == $scope.currentUserCompID)) {
                            growlService.growl("A vendor has made a bid.", "success");
                        } else if (req.methodName == "RestartNegotiation") {
                            window.location.reload();
                        } else if (req.methodName == "RevUploadQuotation") {

                        } else if (req.methodName == "StopBids") {
                            if (req.userIDList.indexOf($scope.currentUserId) > -1) {
                                growlService.growl("Negotiation Time reduced to 1 minute by the customer. New bids will not extend time.");
                            }
                        } else if (req.methodName == "SAVE_DIFFERENTIAL_FACTOR") {
                            if ($scope.auctionItem.status == 'STARTED') {
                                growlService.growl("A vendor has made a bid.", "success");
                            } else {
                                //growlService.growl(req.status, "success");
                            }
                        } else if (!$scope.NegotiationEnded) {
                            PRMForwardBidService.isnegotiationended(id, $scope.currentSessionId)
                                .then(function (response) {
                                    if (response.errorMessage == '') {
                                        if (response.objectID == 1) {
                                            $scope.NegotiationEnded = true;
                                            if (($scope.signalRCustomerAccess || $scope.currentUserId == req.customerID || req.superUserID == $scope.currentUserId || $scope.auctionItem.customerReqAccess)) {
                                                // swal("Negotiation Completed!", "Congratulations! your procurement process is now completed. " + $scope.toprankerName + " is the least bidder with the value " + $scope.auctionItem.minPrice + " " + $scope.auctionItem.currency + " \n Your savings through PRM 360 :" + ($scope.auctionItem.savings) + " " + $scope.auctionItem.currency, "success");
                                                swal("Negotiation Completed!", "Congratulations! your procurement process is now completed.", "success");
                                            } else if (req.userIDList.indexOf($scope.currentUserId) > -1 && $scope.timeLeftMessage == "Negotiation Ends in: ") {
                                                if ($scope.vendorRank == 1) {
                                                    swal("Negotiation Completed!", "Congratulations! you are the least bidder for this requirement. Your price is : " + $scope.auctionItem.minPrice + " " + $scope.auctionItem.currency + "\n Customer would be reaching out you shortly. All the best!", "success");
                                                } else {
                                                    swal("Negotiation Completed!", "Bidding Completed.\n Thank You for your interest on this requirement. You ranked " + $scope.vendorRank + " in this requirement. Thank you for your participation.", "success");
                                                }
                                            }
                                        }
                                    }
                                });
                        }
                    } else {
                        if (req.methodName == "SAVE_DIFFERENTIAL_FACTOR" && iteminvcallerID != $scope.currentUserId) {
                            growlService.growl("A vendor has made a bid.", "success");
                        }
                    }
                }
                else if ($scope.auctionItem.IS_CB_ENABLED) {

                    if ($scope.userIsOwner) {


                        if (items.inv.methodName == 'CheckRequirement') {
                            var vendorCompanyName = '';
                            $scope.auctionItem.requirementVendorsList.forEach(function (vendor, vendorIndex) {
                                if (items.inv.callerID == vendor.vendorID) {
                                    vendorCompanyName = vendor.companyName;
                                    growlService.growl(vendorCompanyName + ' has made a bid.', "success");
                                }
                            });
                        }

                        $scope.getData();
                    } else if ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0) {
                        $scope.getData();
                    }
                }
            });

            $scope.stopBids = function () {
                swal({
                    title: "Are you sure?",
                    text: "The Negotiation will be stopped after one minute.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Yes, Stop Bids!",
                    closeOnConfirm: true
                }, function () {
                    var params = {};
                    params.reqID = id;
                    params.sessionID = $scope.currentSessionId;
                    params.userID = $scope.currentUserId;
                    var parties = params.reqID + "$" + params.userID + "$" + params.sessionID;
                    $scope.invokeSignalR('StopBids', parties, function () {
                        $scope.$broadcast('timer-set-countdown-seconds', 60);
                        $scope.disableButtons();
                        swal("Done!", "Negotiation time reduced to one minute.", "success");
                    });

                    //requirementHub.invoke('StopBids', parties, function (req) {
                    //    $scope.$broadcast('timer-set-countdown-seconds', 60);
                    //    $scope.disableButtons();
                    //    swal("Done!", "Negotiation time reduced to one minute.", "success");
                    //});
                });
            };

            $scope.disableButtons = function () {
                $scope.buttonsDisabled = true;
            };

            $scope.enableButtons = function () {
                $scope.buttonsDisabled = false;
            }

            $scope.editRequirement = function () {
                $log.info('in edit' + $stateParams.Id);
                $state.go('fwd-save-requirement', { 'Id': $stateParams.Id });
            };

            $scope.goToReqReport = function (reqID) {
                var url = $state.href('fwd-reports', { "reqID": reqID });
                $window.open(url, '_blank');
            };

            $scope.generatePOforUser = function () {
                $state.go('po', { 'reqID': $stateParams.Id });
            };

            $scope.metrialDispatchmentForm = function () {
                $state.go('material-dispatchment', { 'Id': $stateParams.Id });
            };

            $scope.paymentdetailsForm = function () {
                $state.go('payment-details', { 'Id': $stateParams.Id });
            };

            $scope.deleteRequirement = function () {
                swal({
                    title: "Are you sure?",
                    text: "The Negotiation will be deleted and an email will be sent out to all vendors involved.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Yes, I am sure",
                    closeOnConfirm: true
                }, function () {
                    var params = {};
                    params.reqID = id;
                    params.sessionID = $scope.currentSessionId;
                    params.userID = $scope.currentUserId;
                    var parties = params.reqID + "$" + params.userID + "$" + params.sessionID + "$" + $scope.reason;
                    $scope.invokeSignalR('DeleteRequirement', parties);
                });
            };

            $scope.DeleteVendorFromAuction = function (VendoID, quotationUrl) {
                if (($scope.auctionItem.auctionVendors.length > 2 || quotationUrl == "") && (quotationUrl == "" || (quotationUrl != "" && $scope.auctionItem.auctionVendors[2].quotationUrl != ""))) {
                    swal({
                        title: "Are you sure?",
                        text: "The Vendor will be deleted and an email will be sent out to The vendor.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "Yes, I am sure",
                        closeOnConfirm: true
                    }, function () {
                        var params = {};
                        params.reqID = id;
                        params.sessionID = $scope.currentSessionId;
                        params.userID = VendoID;

                        PRMForwardBidService.DeleteVendorFromAuction(params)
                            .then(function (response) {
                                if (response.errorMessage == '') {

                                    $scope.getData();
                                    swal("Done!", "Done! Vendor Deleted Successfully!", "success");
                                } else {
                                    swal("Error", "You cannot Delete Vendor", "inverse");
                                }
                            });
                    });
                }
                else {
                    swal("Not Allowed", "You are not allowed to Delete the Vendors.", "error");
                }
            };



            $scope.makeBidUnitPriceValidation = function (revUnitPrice, TemperoryRevUnitPrice, productIDorName) {
                if (TemperoryRevUnitPrice > revUnitPrice && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'Negotiation Ended') && $scope.auctionItem.isDiscountQuotation == 0) {
                    $scope.getData();
                    swal("Error!", 'Please enter price greater than ' + TemperoryRevUnitPrice + ' of ' + productIDorName);
                }
            };

            $scope.changeTotalItemPrice = function (product) {
                if (product && product.itemPrice && product.productQuantity) {
                    product.itemPrice = +(product.itemPrice);
                    if (+product.sGst || +product.cGst || +product.iGst) {
                        let tax = 0;
                        if (+product.iGst) {
                            tax = +product.iGst;
                        } else if (+product.sGst || +product.cGst) {
                            tax = (+product.sGst) + (+product.cGst);
                        }

                        if (tax) {
                            product.unitPrice = (product.itemPrice / (1 + (tax / 100))) / product.productQuantity;
                        }

                    } else {
                        product.unitPrice = product.itemPrice / product.productQuantity;
                    }
                    $scope.totalprice = product.itemPrice;
                    $scope.unitPriceCalculation('PRC');
                    $scope.getTotalPrice($scope.discountAmount, $scope.freightCharges, $scope.totalprice,
                        $scope.packingCharges, $scope.packingChargesTaxPercentage, $scope.packingChargesWithTax,
                        $scope.installationCharges, $scope.installationChargesTaxPercentage, $scope.installationChargesWithTax,
                        $scope.freightChargesTaxPercentage, $scope.freightChargesWithTax);
                    $scope.getRevTotalPrice($scope.revfreightCharges, $scope.revtotalprice,
                        $scope.revpackingCharges, $scope.packingChargesTaxPercentage, $scope.revpackingChargesWithTax,
                        $scope.revinstallationCharges, $scope.installationChargesTaxPercentage, $scope.revinstallationChargesWithTax,
                        $scope.freightChargesTaxPercentage, $scope.revfreightChargesWithTax);
                }
            };

            $scope.unitPriceCalculation = function (val) {
                $scope.gstValidation = false;
                var myInit = 1;
                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                    if ((val === 'CGST' || val === 'SGST') && (item.sGst != 0 || item.cGst != 0) && myInit === 1) {
                        $scope.IGSTFields = true;
                        $scope.CSGSTFields = false;
                        myInit++;
                    } else if (val === 'IGST' && item.iGst != 0 && myInit === 1) {
                        $scope.CSGSTFields = true;
                        $scope.IGSTFields = false;
                        myInit++;
                    } else if (myInit === 1 && val != "PRC") {
                        $scope.CSGSTFields = false;
                        $scope.IGSTFields = false;
                    }

                    if (val === 'GST') {

                        if ($scope.auctionItem.isDiscountQuotation === 2) {

                            item = $scope.handlePrecision(item, 2);
                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status === 'UNCONFIRMED' || $scope.auctionItem.status === 'NOTSTARTED')) {
                                item.revCostPrice = item.costPrice;
                                item.revmarginAmount = item.marginAmount;
                            }

                            item.cGst = item.Gst / 2;
                            item.sGst = item.Gst / 2;
                            item.netPrice = 1 + item.Gst / 100;
                            item = $scope.handlePrecision(item, 2);
                            item.netPrice = item.costPrice * item.netPrice;
                            item = $scope.handlePrecision(item, 2);
                            item.marginAmount = item.unitMRP - item.netPrice;

                            item.revnetPrice = 1 + item.Gst / 100;
                            item = $scope.handlePrecision(item, 2);
                            item.revnetPrice = item.revCostPrice * item.revnetPrice;
                            item = $scope.handlePrecision(item, 2);
                            item.revmarginAmount = item.unitMRP - item.revnetPrice;

                            item = $scope.handlePrecision(item, 2);
                            //item.unitDiscount = (item.marginAmount / item.costPrice);
                            item.unitDiscount = (item.marginAmount / item.netPrice);
                            item.unitDiscount = item.unitDiscount * 100;
                            item = $scope.handlePrecision(item, 2);
                        }
                    }
                    if ($scope.auctionItem.biddingType === 'SPOT' ? $scope.auctionItem.isTabular && $scope.evaluateBiddingTypeBasedOnStatus() : $scope.auctionItem.isTabular && ($scope.auctionItem.status === 'UNCONFIRMED' || $scope.auctionItem.status === 'NOTSTARTED')) {
                        if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                            item.revUnitPrice = item.unitPrice;
                        }

                        //item.sGst = item.cGst;
                        var tempUnitPrice = item.unitPrice;
                        var tempCGst = item.cGst;
                        var tempSGst = item.sGst;
                        var tempIGst = item.iGst;
                        if (!item.unitPrice) {
                            item.unitPrice = 0;
                        };

                        item.itemPrice = item.unitPrice * item.productQuantity;
                        if (!item.cGst) {
                            item.cGst = 0;
                        } else if (val === 'cGST') {
                            // item.sGst = item.cGst;
                            item.iGst = 0;
                            tempCGst = item.cGst;
                            tempSGst = item.sGst;
                            tempIGst = item.iGst;
                        }

                        if (!item.sGst) {
                            item.sGst = 0;
                        } else if (val === 'sGST') {
                            // item.cGst = item.sGst;
                            item.iGst = 0;

                            tempCGst = item.cGst;
                            tempSGst = item.sGst;
                            tempIGst = item.iGst;
                        }
                        if (!item.iGst) {
                            item.iGst = 0;
                        } else if (val === 'iGST') {
                            item.cGst = item.sGst = 0;

                            tempCGst = item.cGst;
                            tempSGst = item.sGst;
                            tempIGst = item.iGst;
                        }

                        if ((parseFloat(item.cGst) + parseFloat(item.sGst)) > 100) {
                            item.cGst = item.sGst = tempCGst = tempSGst = 0;
                            swal("Error!", 'Please enter valid TAX %');
                        } else if (item.iGst > 100) {
                            item.iGst = tempIGst = 0;
                            swal("Error!", 'Please enter valid TAX %');
                        }

                        item.itemPrice = (item.itemPrice + ((item.itemPrice / 100) * (parseFloat(item.cGst) + parseFloat(item.sGst) + parseFloat(item.iGst))) + 0);

                        item.unitPrice = tempUnitPrice;
                        item.cGst = tempCGst;
                        item.sGst = tempSGst;
                        item.iGst = tempIGst;
                    }


                    if ($scope.auctionItem.isTabular && ($scope.auctionItem.status === 'Negotiation Ended' || $scope.auctionItem.status === 'STARTED' || $scope.auctionItem.status === 'UNCONFIRMED' || $scope.auctionItem.status === 'NOTSTARTED')) {

                        var tempRevUnitPrice = item.revUnitPrice;
                        tempCGst = item.cGst;
                        tempSGst = item.sGst;
                        tempIGst = item.iGst;

                        if (!item.revUnitPrice) {
                            item.revUnitPrice = 0;
                        }

                        item.revitemPrice = item.revUnitPrice * item.productQuantity;
                        if (!item.cGst) {
                            item.cGst = 0;
                        }

                        if (!item.sGst) {
                            item.sGst = 0;
                        }

                        if (!item.iGst) {
                            item.iGst = 0;
                        }

                        item.revitemPrice = (item.revitemPrice + ((item.revitemPrice / 100) * (parseFloat(item.cGst) + parseFloat(item.sGst) + parseFloat(item.iGst))) + 0);
                        item.revUnitPrice = tempRevUnitPrice;
                        item.cGst = tempCGst;
                        item.sGst = tempSGst;
                        item.iGst = tempIGst;
                    }
                });

                // For disablig non core item prices when total price is not gretaer than 0
                if ($scope.totalprice > 0) {
                    $scope.fieldValidation();
                }
                // For disablig non core item prices when total price is not gretaer than 0
            };

            $scope.regretError = false;



            $scope.getTotalPrice = function (discountAmount, freightCharges, totalprice,
                packingCharges, packingChargesTaxPercentage, packingChargesWithTax,
                installationCharges, installationChargesTaxPercentage, installationChargesWithTax,
                freightChargesTaxPercentage, freightChargesWithTax) {
                if (!$scope.auctionItem) {
                    return;
                }


                $scope.regretError = false;
                $scope.priceValidationsVendor = '';
                $scope.taxValidation = false;
                $scope.ItemPriceValidation = false;
                $scope.discountfreightValidation = false;

                $scope.discountAmount = $scope.precisionRound(parseFloat(discountAmount), $rootScope.companyRoundingDecimalSetting);

                if (!$scope.auctionItem.isTabular) {
                    $scope.totalprice = totalprice;
                    if (isNaN($scope.totalprice) || !$scope.totalprice) {
                        //$("#totalprice").val(0);
                        $scope.vendorBidPrice = 0;
                        $scope.vendorBidPriceWithoutDiscount = 0;
                    }
                }
                else {
                    $scope.auctionItem.isUOMDifferent = false;
                    if ($scope.auctionItemVendor && $scope.auctionItemVendor.listRequirementItems.length > 0) {

                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                            if (item.unitPrice > 0) {
                                item.isRegret = false;
                                item.regretComments = '';
                            }

                            if (item.isRegret && !item.regretComments) {
                                $scope.regretError = true;
                            }

                            //if (item.productQuantityIn.toUpperCase() != item.vendorUnits.toUpperCase()) {
                            //    //$scope.auctionItem.isUOMDifferent = true;
                            //}

                            if (isNaN(item.itemPrice) || !item.itemPrice) {
                                $scope.ItemPriceValidation = true;
                            }

                            $scope.regretHandle(item);

                        });
                    }
                }

                $scope.discountAmountlocal = $scope.discountAmount;
                if (isNaN($scope.discountAmount) || !$scope.discountAmount) {
                    $scope.discountAmountlocal = 0;
                    $scope.discountfreightValidation = true;
                }

                $scope.discountAmountlocal = $scope.precisionRound(parseFloat($scope.discountAmountlocal), $rootScope.companyRoundingDecimalSetting);

                if ($scope.auctionItem.isTabular) {
                    $scope.totalprice = _.sumBy($scope.auctionItemVendor.listRequirementItems, 'itemPrice');
                    $scope.totalprice = $scope.precisionRound(parseFloat($scope.totalprice), $rootScope.companyRoundingDecimalSetting);
                    if ($scope.auctionItem.isDiscountQuotation === 2) {
                        $scope.totalprice = 0;
                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                            item = $scope.handlePrecision(item, 2);
                            $scope.totalprice += item.netPrice * item.productQuantity;
                            item = $scope.handlePrecision(item, 2);
                        });
                    }

                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.totalprice), $rootScope.companyRoundingDecimalSetting);

                    $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice;
                    $scope.vendorBidPrice = $scope.vendorBidPrice - $scope.discountAmountlocal;
                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.vendorBidPrice), $rootScope.companyRoundingDecimalSetting);
                    if (!$scope.vendorBidPrice || isNaN($scope.vendorBidPrice)) {
                        $scope.vendorBidPrice = 0;
                    }

                    $scope.taxValidation = false;
                    if ($scope.isTaxesMandatory && $scope.auctionItem.auctionVendors.length > 0 && $scope.auctionItem.auctionVendors[0].selectedVendorCurrency === "INR" && $scope.auctionItemVendor.listRequirementItems && $scope.auctionItemVendor.listRequirementItems.length > 0) {
                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                            if (!$scope.taxValidation && item.isCoreProductCategory && !item.isRegret) {
                                if (item.isCoreProductCategory && (+item.iGst || +item.cGst || +item.sGst)) {
                                    $scope.taxValidation = false;
                                } else {
                                    $scope.taxValidation = true;
                                }
                            }
                        });
                    }

                }
                if (!$scope.auctionItem.isTabular) {
                    $scope.totalprice = $scope.precisionRound(parseFloat($scope.totalprice), $rootScope.companyRoundingDecimalSetting);

                    $scope.calculatedSumOfAllTaxes = 0;
                    if ($scope.listRequirementTaxes) {
                        $scope.listRequirementTaxes.forEach(function (tax, index) {
                            if (tax.taxPercentage !== '' && tax.taxPercentage != undefined) {
                                $scope.calculatedSumOfAllTaxes += $scope.precisionRound(parseFloat($scope.totalprice), $rootScope.companyRoundingDecimalSetting)
                                    * $scope.precisionRound(parseFloat(tax.taxPercentage), $rootScope.companyRoundingDecimalSetting) / 100;
                            }
                        });

                        $scope.vendorTaxes = $scope.calculatedSumOfAllTaxes;

                        $scope.listRequirementTaxes.forEach(function (item, index) {
                            if (!item.taxName) {
                                $scope.taxValidation = true;
                            }
                            if (!item.taxPercentage) {
                                $scope.taxValidation = true;
                            }
                            if (item.taxPercentage <= 0) {
                                item.taxPercentage = 0;
                            }
                        });
                    }

                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.totalprice), $rootScope.companyRoundingDecimalSetting) +
                        $scope.precisionRound(parseFloat($scope.vendorTaxes), $rootScope.companyRoundingDecimalSetting);

                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.vendorBidPrice), $rootScope.companyRoundingDecimalSetting);

                    $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice;
                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.vendorBidPrice), $rootScope.companyRoundingDecimalSetting) -
                        $scope.precisionRound(parseFloat($scope.discountAmountlocal), $rootScope.companyRoundingDecimalSetting);
                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.vendorBidPrice), $rootScope.companyRoundingDecimalSetting);
                    if (!$scope.vendorBidPrice || isNaN($scope.vendorBidPrice)) {
                        $scope.vendorBidPrice = 0;
                    }
                }
            };

            $scope.getRevTotalPrice = function (revfreightCharges, revtotalprice,
                revpackingCharges, packingChargesTaxPercentage, revpackingChargesWithTax,
                revinstallationCharges, installationChargesTaxPercentage, revinstallationChargesWithTax,
                freightChargesTaxPercentage, revfreightChargesWithTax) {

                $scope.ItemPriceValidation = false;
                $scope.discountfreightValidation = false;

                if (!$scope.auctionItem.isTabular) {
                    $scope.revtotalprice = revtotalprice;
                    if (isNaN($scope.revtotalprice) || !$scope.revtotalprice) {
                        $("#revtotalprice").val(0);
                        $scope.revvendorBidPrice = 0;
                    }
                }
                else {
                    if ($scope.auctionItemVendor.length > 0) {
                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                            if (item.oldRevUnitPrice > 0 && item.revUnitPrice > 0 && item.oldRevUnitPrice > item.revUnitPrice && $scope.auctionItem.status === 'STARTED') {
                                if ($scope.auctionItem.isDiscountQuotation === 0) {
                                    var reductionval = parseFloat(item.oldRevUnitPrice).toFixed(2) - parseFloat(item.revUnitPrice).toFixed(2);
                                    //reductionval = parseFloat(reductionval).toFixed(2);                            
                                    if (reductionval >= item.itemMinReduction && reductionval > 0 && item.itemMinReduction > 0) {
                                        $scope.ItemPriceValidation = false;

                                    } else if (reductionval < item.itemMinReduction && reductionval > 0 && item.itemMinReduction > 0) {
                                        $scope.ItemPriceValidation = true;
                                        item.revUnitPrice = item.oldRevUnitPrice;
                                        $scope.getData();
                                        swal("Error!", 'Your Unit price reduction should be greater than Min.unit Price limit :' + item.itemMinReduction, "error");
                                        return;
                                    }
                                }

                            }

                            if (isNaN(item.revitemPrice) || !item.revitemPrice) {
                                $scope.ItemPriceValidation = true;
                            }
                        });
                    }
                }


                if ($scope.auctionItem.isTabular) {
                    $scope.revtotalprice = _.sumBy($scope.auctionItemVendor.listRequirementItems, 'revitemPrice');
                    if ($scope.auctionItem.isDiscountQuotation === 2) {

                        $scope.revtotalprice = 0;

                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                            item = $scope.handlePrecision(item, 2);
                            $scope.revtotalprice += item.revnetPrice * item.productQuantity;
                            item = $scope.handlePrecision(item, 2);
                        });
                    }



                    $scope.calculatedSumOfAllTaxes = 0;
                    if ($scope.listRequirementTaxes) {
                        $scope.listRequirementTaxes.forEach(function (tax, index) {
                            if (tax.taxPercentage != '' && tax.taxPercentage != undefined) {
                                $scope.calculatedSumOfAllTaxes += $scope.revtotalprice * tax.taxPercentage / 100;
                            }
                        });
                    }


                    $scope.vendorTaxes = $scope.calculatedSumOfAllTaxes;

                    $scope.revvendorBidPrice = $scope.precisionRound(parseFloat($scope.revtotalprice), $rootScope.companyRoundingDecimalSetting) +
                        $scope.precisionRound(parseFloat($scope.vendorTaxes), $rootScope.companyRoundingDecimalSetting);

                    $scope.revvendorBidPrice = $scope.precisionRound(parseFloat($scope.revvendorBidPrice), $rootScope.companyRoundingDecimalSetting);
                    if (!$scope.revvendorBidPrice || isNaN($scope.revvendorBidPrice)) {
                        $scope.revvendorBidPrice = 0;
                    }

                    $scope.reduceBidValue = $scope.precisionRound(parseFloat($scope.revvendorBidPrice - $scope.auctionItem.minPrice), $rootScope.companyRoundingDecimalSetting);
                    $scope.reduceBidAmount();
                }

                if (!$scope.auctionItem.isTabular) {
                    $scope.revtotalprice = $scope.precisionRound(parseFloat(revtotalprice), $rootScope.companyRoundingDecimalSetting);


                    $scope.calculatedSumOfAllTaxes = 0;
                    $scope.listRequirementTaxes.forEach(function (tax, index) {
                        if (tax.taxPercentage != '' && tax.taxPercentage != undefined) {
                            $scope.calculatedSumOfAllTaxes += $scope.revtotalprice * tax.taxPercentage / 100;
                        }
                    })
                    $scope.vendorTaxes = $scope.calculatedSumOfAllTaxes;

                    $scope.revvendorBidPrice = $scope.precisionRound(parseFloat($scope.revtotalprice), $rootScope.companyRoundingDecimalSetting) +
                        $scope.precisionRound(parseFloat($scope.vendorTaxes), 2);

                    $scope.revvendorBidPrice = $scope.precisionRound(parseFloat($scope.revvendorBidPrice), $rootScope.companyRoundingDecimalSetting);
                    if (!$scope.revvendorBidPrice || isNaN($scope.revvendorBidPrice)) {
                        $scope.revvendorBidPrice = 0;
                    }
                }

            };





            $scope.currencyValidationStyle = {};

            $scope.uploadQuotation = function (quotationType) {
                let hasItemQuotationRejected = false;
                $scope.currencyValidationStyle = {};

                if (!$scope.auctionItem.auctionVendors[0].selectedVendorCurrency) {
                    document.getElementById('priceDiv').scrollIntoView();
                    $scope.currencyValidationStyle = {
                        'border-color': 'red'
                    };
                    growlService.growl('Please Select Currency', "inverse");
                    return;
                };

                var uploadQuotationError = false;

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                    if (!hasItemQuotationRejected && item.isItemQuotationRejected === 1) {
                        hasItemQuotationRejected = true;
                    }

                    if (uploadQuotationError) {
                        return false;
                    }
                    var slno = index + 1;
                    if (item.isRegret) {
                        if (!item.regretComments) {
                            growlService.growl("Please enter regret comments.", "inverse");
                            uploadQuotationError = true;
                            return false;
                        }
                    } else {
                        var isTaxDisabled = $scope.isTaxFieldEditableForSubItems(item.productQuotationTemplateArray);
                        if (item.unitPrice <= 0 && item.isCoreProductCategory > 0) {
                            growlService.growl("Please enter Unit price for Product " + slno + ".", "inverse");
                            uploadQuotationError = true;
                            return false;
                        }
                    }
                });

                var TaxFiledValidation = 0;

                if (!$scope.CSGSTFields) {
                    TaxFiledValidation = 1;
                }

                if (uploadQuotationError) {
                    return;
                }

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    item.productIDorName = validateStringWithoutSpecialCharacters(item.productIDorName);
                    item.productNo = validateStringWithoutSpecialCharacters(item.productNo);
                    item.productDescription = validateStringWithoutSpecialCharacters(item.productDescription);
                    item.productBrand = validateStringWithoutSpecialCharacters(item.productBrand);
                    item.productDeliveryDetails = validateStringWithoutSpecialCharacters(item.productDeliveryDetails);
                    item.itemLevelInitialComments = validateStringWithoutSpecialCharacters(item.itemLevelInitialComments);
                    item.itemLevelRevComments = validateStringWithoutSpecialCharacters(item.itemLevelRevComments);
                    item.regretComments = validateStringWithoutSpecialCharacters(item.regretComments);
                    item.itemQuotationRejectedComment = validateStringWithoutSpecialCharacters(item.itemQuotationRejectedComment);

                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            subItem.DESCRIPTION = validateStringWithoutSpecialCharacters(subItem.DESCRIPTION);
                            subItem.dateCreated = "/Date(1561000200000+0530)/";
                            subItem.dateModified = "/Date(1561000200000+0530)/";
                        });
                    }
                    item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplateArray);
                    if ($scope.auctionItem.biddingType === 'SPOT') {
                        item.revUnitPrice = item.unitPrice;
                    }

                    //Remove Special Chars
                    //item.productQuotationTemplateJson = item.productQuotationTemplateJson.replace(/\'/gi, "\\'");
                    //item.productQuotationTemplateJson = item.productQuotationTemplateJson.replace(/(\r\n|\n|\r)/gm, "");
                    //let tempTemplateArray = JSON.parse(item.productQuotationTemplateJson);
                    //subItem.SPECIFICATION = item.productQuotationTemplateJson.SPECIFICATION.replace(/\'/gi, "\\'");
                    //if (tempTemplateArray && tempTemplateArray.length > 0) {
                    //    tempTemplateArray.forEach(function (subItem, subItemIndex) {
                    //        if (subItem.SPECIFICATION) {
                    //            subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/"/g, '\\"');
                    //        }
                    //    });

                    //    item.productQuotationTemplateJson = JSON.stringify(tempTemplateArray);
                    //}

                });
                $scope.warranty = validateStringWithoutSpecialCharacters($scope.warranty);
                $scope.validity = validateStringWithoutSpecialCharacters($scope.validity);
                $scope.duration = validateStringWithoutSpecialCharacters($scope.duration);
                $scope.payment = validateStringWithoutSpecialCharacters($scope.payment);
                $scope.otherProperties = validateStringWithoutSpecialCharacters($scope.otherProperties);

                var params = {
                    'quotationObject': $scope.auctionItemVendor.listRequirementItems, // 1
                    'userID': $scope.currentUserId,
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': $scope.currentSessionId,
                    'price': $scope.totalprice, // 5
                    'tax': $scope.vendorTaxes,
                    'vendorBidPrice': $scope.vendorBidPrice ? $scope.vendorBidPrice : 0,
                    'warranty': $scope.warranty,
                    'duration': $scope.duration,
                    'payment': $scope.payment, // 10
                    'gstNumber': $scope.gstNumber,
                    'validity': $scope.validity,
                    'otherProperties': $scope.otherProperties,
                    'quotationType': quotationType,
                    'revised': 0, // 15
                    'discountAmount': $scope.discountAmount ? $scope.discountAmount : 0,
                    'listRequirementTaxes': $scope.listRequirementTaxes,
                    'quotationAttachment': $scope.quotationAttachment,
                    'multipleAttachments': $scope.multipleAttachmentsList,
                    'TaxFiledValidation': TaxFiledValidation,
                    'selectedVendorCurrency': $scope.auctionItem.auctionVendors[0].selectedVendorCurrency, //35
                    'isVendAckChecked': $scope.auctionItem.auctionVendors[0].isVendAckChecked,
                    'quotFreezeTime': $scope.auctionItem.quotationFreezTime
                };

                params.unitPrice = $scope.auctionItem.unitPrice;
                params.productQuantity = $scope.auctionItem.productQuantity;
                params.cGst = $scope.auctionItem.cGst;
                params.sGst = $scope.auctionItem.sGst;
                params.iGst = $scope.auctionItem.iGst;
                params.INCO_TERMS = $scope.auctionItem.auctionVendors[0].INCO_TERMS;

                //auctionItem.auctionVendors[0].INCO_TERMS

                if (!params.warranty) {
                    params.warranty = '';
                    growlService.growl('Please enter Warranty.', "inverse");
                    return false;
                }

                if (!params.validity) {
                    params.validity = '';
                    growlService.growl('Please enter Price Validity.', "inverse");
                    return false;
                }

                if (!params.otherProperties) {
                    params.otherProperties = '';
                }

                if (!params.payment) {
                    params.payment = '';
                    swal({
                        title: "Error",
                        text: "Please enter the Payment Terms",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    });
                    return false;
                }
                if ((params.payment == validateStringWithoutSpecialCharacters($scope.auctionItem.paymentTerms)) && !$scope.paymentPopUp) {
                    swal({
                        title: "",
                        text: "Please check the payment terms",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                $scope.paymentPopUp = true;
                            } else {
                                $scope.paymentPopUp = false;
                            }
                        });
                    if (!$scope.paymentPopUp) {
                        return;
                    }

                }

                if (!params.gstNumber) {
                    params.gstNumber = '';
                }

                $scope.specificationData = [];

                params.quotationObject.forEach(function (item) {
                    item.productQuotationTemplateArray.forEach(function (spec) {
                        if ((!spec.SPECIFICATION || spec.SPECIFICATION === null) && spec.T_ID > 0 && spec.IS_MANDATE_TO_VENDOR === 1 && spec.IS_VALID === 1 && !item.isRegret) {
                            $scope.specificationData.push(spec.NAME)
                        }
                    })
                })
                if ($scope.specificationData.length > 0) {
                    growlService.growl('Please enter ' + $scope.specificationData.toString() + ' Specification.', "inverse");
                    return false;
                }

                if ($scope.auctionItem.status === 'Negotiation Ended') {
                    params.price = $scope.revtotalprice,
                        params.tax = $scope.vendorTaxes,
                        params.vendorBidPrice = $scope.revvendorBidPrice,
                        params.revised = 1
                }


                if (($scope.auctionItem.status === 'UNCONFIRMED' || $scope.auctionItem.status === 'NOTSTARTED') && $scope.auctionItem.isQuotationPriceLimit && $scope.vendorBidPrice < $scope.auctionItem.quotationPriceLimit) {
                    swal("Error!", 'Your Amount Should be greater than or equal to Quotation min. Price limit :' + $scope.auctionItem.quotationPriceLimit, "error");
                    return;
                }

                $scope.taxEmpty = false;

                if ($scope.taxEmpty) {

                    swal({
                        title: "warning!",
                        text: "Please fill TAXES slab",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    });

                }
                else {
                    auctionsService.getdate()
                        .then(function (responseFromServer) {
                            var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                            var auctionStart = new Date(parseInt($scope.auctionItem.startTime.substr(6)));
                            var timeofauctionstart = auctionStart.getTime();
                            var currentServerTime = dateFromServer.getTime();

                            if ($scope.auctionItem.status === "Negotiation Ended") {


                                // $scope.DeleteRequirementTerms();

                                if ($scope.auctionItem.status === 'Negotiation Ended') {
                                } else {
                                    // $scope.SaveRequirementTerms($scope.reqId);
                                }

                                PRMForwardBidService.uploadQuotation(params)
                                    .then(function (req) {
                                        if (req.errorMessage === '' || req.errorMessage === 'reviced') {
                                            if (req.errorMessage === '') {
                                                var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId;
                                                //$scope.invokeSignalR('CheckRequirement', parties);
                                                //    requirementHub.invoke('CheckRequirement', parties, function () {                    
                                                //});
                                            }

                                            let successText = 'Your Quotation uploaded successfully';
                                            let type = "success";
                                            var quotationFreeTime = moment($scope.auctionItem.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                                            var quotationFreeTimeMoment = moment(quotationFreeTime);
                                            var today = moment();

                                            swal({
                                                title: "Thanks!",
                                                text: successText,
                                                type: type,
                                                showCancelButton: false,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "Ok",
                                                closeOnConfirm: true
                                            },
                                                function () {
                                                    location.reload();
                                                });

                                        } else {
                                            swal({
                                                title: "Error",
                                                text: req.errorMessage,
                                                type: "error",
                                                showCancelButton: false,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "Ok",
                                                closeOnConfirm: true
                                            });
                                        }
                                    });

                            }
                            else {
                                if ($scope.auctionItem.biddingType === 'SPOT' && ($scope.auctionItem.minPrice <= 0 ? true : $scope.auctionItem.minPrice > 0 && $scope.auctionItem.auctionVendors[0].isQuotationRejected === 1) ? true : timeofauctionstart - currentServerTime >= 3600000 &&
                                    ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 || hasItemQuotationRejected)) {
                                    PRMForwardBidService.uploadQuotation(params)
                                        .then(function (req) {
                                            if (req.errorMessage === '' || req.errorMessage === 'reviced') {
                                                if (req.errorMessage === '') {
                                                    var parties = id + "$" + $scope.currentUserId + "$" + $scope.currentSessionId;
                                                }

                                                let successText = 'Your Quotation uploaded successfully';
                                                let type = "success";
                                                var quotationFreeTime = moment($scope.auctionItem.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                                                var quotationFreeTimeMoment = moment(quotationFreeTime);
                                                var today = moment();

                                                swal({
                                                    title: "Thanks!",
                                                    text: successText,
                                                    type: type,
                                                    showCancelButton: false,
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "Ok",
                                                    closeOnConfirm: true
                                                },
                                                    function () {
                                                        location.reload();
                                                    });

                                            } else {
                                                swal({
                                                    title: "Error",
                                                    text: req.errorMessage,
                                                    type: "error",
                                                    showCancelButton: false,
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "Ok",
                                                    closeOnConfirm: true
                                                });
                                            }
                                        });
                                } else {

                                    var message = "You have missed the deadline for Quotation Submission";
                                    if ($scope.auctionItem.auctionVendors[0].isQuotationRejected === 0 && $scope.auctionItem.status !== "Negotiation Ended") {
                                        message = $scope.showMessageBasedOnBiddingType('Your Quotation Already Approved You cant Update Quotation Please Contact PRM360', 'QUOTATION_UPLOAD');
                                    }

                                    swal({
                                        title: "Error",
                                        text: message,
                                        type: "error",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    });
                                    $scope.getData();
                                }
                            }
                        });
                }
            };

            $scope.UploadRequirementItemsCeilingSave = function (fileContent) {
                var params = {
                    reqID: $scope.reqId,
                    sessionID: userService.getUserToken(),
                    requirementItemsAttachment: fileContent
                };
                PRMForwardBidService.UploadRequirementItemsCeilingSave(params)
                    .then(function (response) {
                        $("#requirementItemsCeilingSaveAttachment").val(null);
                        if (response) {
                            location.reload();
                        }
                    });
            };

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {

                        if (id === "excelquotation") {
                            var bytearray = new Uint8Array(result);
                            $scope.quotationAttachment = $.makeArray(bytearray);
                            $scope.uploadquotationsfromexcel();
                        } else if (id === "clientsideupload") {
                            var bytearray = new Uint8Array(result);
                            $scope.quotationAttachment = $.makeArray(bytearray);
                            $scope.uploadclientsidequotation('clientsideupload');
                            $scope.quotationAttachment = null;
                            $scope.file = [];
                            $scope.file.name = '';
                        }
                        else if (id === "quotationBulkUpload") {
                            var bytearray = new Uint8Array(result);
                            $scope.quotationAttachment = $.makeArray(bytearray);
                            $scope.uploadclientsidequotation('quotationBulkUpload');
                            $scope.quotationAttachment = null;
                            $scope.file = [];
                            $scope.file.name = '';
                        }
                        else if (id === "requirementItemsCeilingSaveAttachment") {
                            if (ext !== "xlsx") {
                                swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
                                return;
                            }

                            var bytearray = new Uint8Array(result);
                            $scope.UploadRequirementItemsCeilingSave($.makeArray(bytearray));
                            $scope.file = [];
                            $scope.file.name = '';
                            //return;
                        }
                        else {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var index = _.indexOf($scope.auctionItemVendor.listRequirementItems, _.find($scope.auctionItemVendor.listRequirementItems, function (o) {
                                return o.productSNo === id;
                            }));
                            var obj = $scope.auctionItemVendor.listRequirementItems[index];
                            obj.itemAttachment = arrayByte;
                            obj.attachmentName = ItemFileName;
                            $scope.auctionItemVendor.listRequirementItems.splice(index, 1, obj);
                        }
                    });
            };

            $scope.QuotationRatioButton = {
                upload: 'generate'
            };

            $scope.rejectreson = '';
            $scope.rejectresonValidation = false;

            $scope.QuatationAprovelvalue = true;

            $scope.QuatationAprovel = function (userID, value, comment, action, auctionVendor) {

                var operationResult = 'Quotation Approved Successfully';
                if (value) {
                    operationResult = "Quotation Rejected Successfully";
                    //auctionVendor.technicalScore = 0;
                }
                if (auctionVendor && $scope.auctionItem.isTechScoreReq && auctionVendor.technicalScore <= 0 && !value) {
                    swal("Error!", 'Vendor can be approved only after Technical Score.', "error");
                    return;
                }

                if (comment) {
                    comment = comment.trim();
                }

                if (userID === auctionVendor.vendorID && value && !comment) {
                    auctionVendor.rejectresonValidation = true;
                    growlService.growl('Please enter reject Reason', "inverse");
                    return false;
                } else {
                    auctionVendor.rejectresonValidation = false;
                }

                var params = {
                    'vendorID': userID,
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': $scope.currentSessionId,
                    'customerID': $scope.currentUserId,
                    'value': value,
                    'reason': comment,
                    'technicalScore': auctionVendor.technicalScore,
                    'action': action
                };


                PRMForwardBidService.QuatationAprovel(params).then(function (req) {
                    if (req.errorMessage === '') {
                        swal({
                            title: "Done!",
                            text: operationResult,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                //$scope.recalculate('QUOTATION_APPROVAL_CUSTOMER', userID);
                                location.reload();
                            });

                    } else {
                        swal("Error!", req.errorMessage, "error");
                    }
                });
            };

            $scope.submitButtonShow = 0;

            $scope.submitButtonShowfunction = function (value, auctionVendor) {

                auctionVendor.quotationRejectedComment = '';
                $scope.submitButtonShow = value;
                let tempArr = _.filter($scope.vendorApprovals, function (vendor) {
                    return Number(vendor.vendorID) !== Number(auctionVendor.vendorID)
                });

                tempArr.push(auctionVendor);
                $scope.vendorApprovals = tempArr;

                $scope.auctionItem.auctionVendors.forEach(function (v, vI) {
                    if (v.vendorID === auctionVendor.vendorID) {
                        v.isApproveOrRejectClicked = true;
                    }
                })

            }

            $scope.saveApprovalStatus = function () {
                $scope.vendorApprovals.forEach(function (auctionVendor, index) {
                    if ($scope.auctionItem.status === 'Negotiation Ended') {
                        $scope.QuatationAprovel(auctionVendor.vendorID, auctionVendor.isRevQuotationRejected, auctionVendor.revQuotationRejectedComment, 'revquotation', auctionVendor);
                    } else {
                        $scope.QuatationAprovel(auctionVendor.vendorID, auctionVendor.isQuotationRejected, auctionVendor.quotationRejectedComment, 'quotation', auctionVendor);
                    }
                });
            };

            $scope.NegotiationSettingsValidationMessage = '';

            $scope.NegotiationTimeValidation = function (days, hours, mins, minReduction, rankComparision) {

                $scope.days = days;
                $scope.hours = hours;
                $scope.mins = mins;


                $log.info($scope.days);
                $log.info($scope.hours);
                $log.info($scope.mins);

                $scope.NegotiationSettingsValidationMessage = '';

                if ($scope.auctionItem.isDiscountQuotation != 2) {
                    if (parseFloat(minReduction) <= 0 || minReduction == undefined) {
                        $scope.NegotiationSettingsValidationMessage = 'Set Min 0.01 value for Min.Amount incremental field';
                        return;
                    }

                    if (parseFloat(rankComparision) <= 0 || rankComparision == undefined) {
                        $scope.NegotiationSettingsValidationMessage = 'Set Min 0.01 for Rank Comparision price field';
                        return;
                    }

                } else if ($scope.auctionItem.isDiscountQuotation === 2) {
                    $scope.NegotiationSettings.minReductionAmount = 0;
                    $scope.NegotiationSettings.rankComparision = 0;
                }

                if (days == undefined || days < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Days';
                    return;
                }
                if (hours < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                    return;
                }
                if (mins < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minunts';
                    return;
                }
                if (mins >= 60 || mins == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minunts';
                    return;
                }
                if (hours > 24 || hours == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                    return;
                }
                if (hours === 24 && mins > 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours & Minuts';
                    return;
                }
                if (mins < 5 && hours === 0 && days === 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Min 5 Minutes';
                    return;
                }
            }

            $scope.listRequirementTaxes = [];
            $scope.reqTaxSNo = 1;

            $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;

            $scope.requirementTaxes =
            {
                taxSNo: $scope.reqTaxSNo++,
                taxName: '',
                taxPercentage: 0
            }

            $scope.AddTax = function () {

                if ($scope.listRequirementTaxes.length > 4) {
                    return;
                }
                $scope.requirementTaxes =
                {
                    taxSNo: $scope.reqTaxSNo++,
                    taxName: '',
                    taxPercentage: 0
                };
                $scope.listRequirementTaxes.push($scope.requirementTaxes);
                $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
                $scope.getTotalPrice($scope.discountAmount, 0, $scope.totalprice);
            };

            $scope.deleteTax = function (SNo) {
                $scope.listRequirementTaxes = _.filter($scope.listRequirementTaxes, function (x) { return x.taxSNo !== SNo; });
                $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
                $scope.getTotalPrice($scope.discountAmount, 0, $scope.totalprice);
            };

            $scope.reports = {};

            $scope.isReportGenerated = 0;

            $scope.GetReportsRequirement = function () {
                PRMForwardBidService.GetReportsRequirement({ "reqid": $stateParams.Id, "sessionid": $scope.currentSessionId, 'userid': $scope.currentUserId })
                    .then(function (response) {
                        $scope.reports = response;
                        $scope.getData();
                        $scope.isReportGenerated = 1;
                    });
            }

            $scope.generateReports = function () {
                $scope.isReportGenerated = 0;
            }

            $scope.updatePriceCap = function (auctionVendor) {
                let levelPrice = 1;
                $scope.formRequest.priceCapValueMsg = '';
                let l1Vendor = $scope.precisionRound(parseFloat($scope.auctionItem.auctionVendors[0].totalPriceIncl), $rootScope.companyRoundingDecimalSetting);

                if ($scope.auctionItem.auctionVendors[0].companyName === 'PRICE_CAP') {
                    levelPrice = 2;
                    l1Vendor = $scope.precisionRound(parseFloat($scope.auctionItem.auctionVendors[1].totalPriceIncl), $rootScope.companyRoundingDecimalSetting);
                }

                if ($scope.precisionRound(parseFloat($scope.formRequest.priceCapValue), $rootScope.companyRoundingDecimalSetting) > 0 && $scope.precisionRound(parseFloat($scope.formRequest.priceCapValue), $rootScope.companyRoundingDecimalSetting) < l1Vendor) {
                    let priceDetails = {
                        uID: auctionVendor.vendorID,
                        reqID: $scope.reqId,
                        price: $scope.formRequest.priceCapValue,
                        sessionID: $scope.currentSessionId
                    };

                    PRMForwardBidService.UpdatePriceCap(priceDetails)
                        .then(function (response) {
                            if (response.errorMessage === '') {
                                growlService.growl('Successfully updated price cap.', "success");
                                $scope.formRequest.priceCapValueMsg = '';
                                auctionVendor.totalPriceIncl = $scope.formRequest.priceCapValue;
                            }
                            else {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                        });
                }
                else {
                    $scope.formRequest.priceCapValueMsg = 'Price cap amount should be less L' + levelPrice + ' price ' + l1Vendor;
                    // growlService.growl('Price cap amount should be more than 0  and less than L1 price ' + l1Vendor, "inverse");
                }
            };

            $scope.savepricecap = function () {

                $scope.l1Price = $scope.auctionItem.auctionVendors[0].totalPriceIncl;

                $scope.l1CompnyName = $scope.auctionItem.auctionVendors[0].companyName;

                if ($scope.l1CompnyName === 'PRICE_CAP') {
                    $scope.l1Price = $scope.auctionItem.auctionVendors[1].totalPriceIncl;
                }


                if (!$scope.IS_CB_ENABLED) {
                    $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);
                    if ($scope.NegotiationSettingsValidationMessage !== '') {
                        $scope.Loding = false;
                        return;
                    }

                    $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                }

                if ($scope.auctionItem.reqType === 'PRICE_CAP' && ($scope.auctionItem.priceCapValue <= 0 || $scope.auctionItem.priceCapValue == undefined || $scope.auctionItem.priceCapValue === '')) {
                    swal("Error!", 'Please Enter Valid Price cap', "error");
                    return;
                }

                var params = {
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': $scope.currentSessionId,
                    'userID': $scope.currentUserId,
                    'reqType': $scope.auctionItem.reqType,
                    'priceCapValue': $scope.auctionItem.priceCapValue,
                    'isUnitPriceBidding': $scope.auctionItem.isUnitPriceBidding,
                    //#CB-0-2018-12-05
                    'IS_CB_ENABLED': $scope.IS_CB_ENABLED,
                    'IS_CB_NO_REGRET': $scope.IS_CB_NO_REGRET
                };

                if ($scope.IS_CB_ENABLED) {

                    swal({
                        title: "Type of bidding cannot be changed once selected. The only way to change it is by creating a new requirement.",
                        text: "Kindly confirm the action.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function () {
                        PRMForwardBidService.savepricecap(params).then(function (req) {
                            if (req.errorMessage === '') {
                                //code before the pause
                                setTimeout(function () {
                                    //do what you need here
                                    swal({
                                        title: "Done!",
                                        text: 'Requirement Type and Bidding Type Saved Successfully',
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            if ($scope.auctionItem.status == 'STARTED') {
                                                $scope.recalculate('', 0, true);
                                            }
                                            location.reload();
                                        });
                                }, 1000);
                            } else {
                                swal("Error!", req.errorMessage, "error");
                            }
                        });
                    })

                } else {
                    PRMForwardBidService.savepricecap(params).then(function (req) {
                        if (req.errorMessage == '') {
                            swal({
                                title: "Done!",
                                text: 'Requirement Type and Bidding Type Saved Successfully',
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    if ($scope.auctionItem.status == 'STARTED') {
                                        $scope.recalculate('', 0, true);
                                    }
                                    location.reload();
                                });
                        } else {
                            swal("Error!", req.errorMessage, "error");
                        }
                    });
                }

            };

            $scope.goToReqTechSupport = function () {
                var url = $state.href("fwd-reqTechSupport", { "reqId": $scope.reqId });
                window.open(url, '_blank');
            };

            $scope.goToMaterialReceived = function () {
                //$state.go("reqTechSupport", { "reqId": $scope.reqId }, { reload: true, newtab: true });

                var url = $state.href("Material-Received", { "Id": $scope.reqId });
                window.open(url, '_blank');
            };

            $scope.goToVendorPo = function (vendorID) {
                var url = $state.href("po-list", { "reqID": $scope.reqId, "vendorID": vendorID, "poID": 0 });
                window.open(url, '_blank');
            };

            $scope.goToDescPo = function () {
                var url = $state.href("desc-po", { "reqID": $scope.reqId });
                window.open(url, '_blank');
            };

            $scope.goToPreNegotiation = function (Id) {
                var url = $state.href("fwd-req-savingsPreNegotiation", { "Id": $scope.reqId });
                window.open(url, '_blank');
            };

            $scope.goToComparatives = function (reqID) {
                if ($scope.auctionItem.isDiscountQuotation != 2) {
                    let url = $state.href("fwd-comparatives", { "reqID": $scope.reqId });
                    window.open(url, '_blank');
                }
                else if ($scope.auctionItem.isDiscountQuotation == 2) {
                    let url = $state.href("marginComparatives", { "reqID": $scope.reqId });
                    window.open(url, '_blank');
                }
            };

            $scope.goToQCS = function (reqID, qcsID) {
                var url = $state.href("qcs", { "reqID": $scope.reqId, "qcsID": qcsID });
                window.open(url, '_self');
            };

            $scope.goToQCSList = function (reqID) {
                var url = $state.href("list-qcs", { "reqID": $scope.reqId });
                window.open(url, '_blank');
            };

            $scope.goToCostQCS = function (reqID) {
                var url = $state.href("cost-comparisions-qcs", { "reqID": $scope.reqId });
                window.open(url, '_blank');
            };

            $scope.goToRMQCS = function (reqID, itemID) {
                var url = $state.href("qcsRM", { "reqID": $scope.reqId, "itemID": itemID });
                window.open(url, '_blank');
            };

            $scope.isItemChanged = function (product) {
                var isSame = 0;
                var filterCustItem = _.filter($scope.auctionItem.listRequirementItems, function (custItem) {
                    return custItem.itemID === product.itemID;
                });

                if (filterCustItem.length > 0) {
                    if (filterCustItem[0].productIDorNameCustomer.toUpperCase() !== product.productIDorName.toUpperCase()
                        || filterCustItem[0].productNoCustomer.toUpperCase() !== product.productNo.toUpperCase()) {
                        isSame = 1;
                    }
                }

                if (filterCustItem.length <= 0) {
                    isSame = 2;
                }

                return isSame;
            };

            $scope.paymentRadio = false;

            $scope.paymentlist = [];

            $scope.addpaymentvalue = function () {
                var listpaymet =
                {
                    reqTermsID: 0,
                    reqID: $scope.stateParamsReqID,
                    userID: $scope.currentUserId,
                    reqTermsDays: 0,
                    reqTermsPercent: 0,
                    reqTermsType: 'PAYMENT',
                    paymentType: '+'
                };

                if ($scope.auctionItem.status == 'Negotiation Ended') {
                    listpaymet.isRevised = 1
                } else {
                    listpaymet.isRevised = 0
                }

                $scope.paymentlist.push(listpaymet);
            };

            $scope.delpaymentvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }

                $scope.paymentlist.splice(val, 1);
            };

            $scope.resetpayment = function () {
                $scope.paymentlist = [];
            };

            $scope.deliveryRadio = false;

            $scope.deliveryList = [];

            $scope.adddeliveryvalue = function (val) {
                var deliveryObj =
                {
                    reqTermsID: 0,
                    reqID: $scope.stateParamsReqID,
                    userID: $scope.currentUserId,
                    reqTermsDays: 0,
                    reqTermsPercent: 0,
                    reqTermsType: 'DELIVERY',
                    refReqTermID: val
                };


                if ($scope.auctionItem.status == 'Negotiation Ended') {
                    deliveryObj.isRevised = 1
                } else {
                    deliveryObj.isRevised = 0
                }

                $scope.deliveryList.push(deliveryObj);
            };

            $scope.deldeliveryvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }


                $scope.deliveryList.splice(val, 1);
            };

            $scope.resetdelivery = function () {
                $scope.deliveryList = [];
            };

            $scope.listRequirementTerms = [];

            $scope.SaveRequirementTerms = function (reqID) {

                $scope.listRequirementTerms = [];

                $scope.deliveryList.forEach(function (item, index) {
                    item.reqID = reqID;
                    $scope.listRequirementTerms.push(item);
                });

                $scope.paymentlist.forEach(function (item, index) {

                    item.reqID = reqID;

                    if (item.paymentType == '-') {
                        item.reqTermsDays = -(item.reqTermsDays);
                    }

                    $scope.listRequirementTerms.push(item);
                });

                var params = {
                    "listRequirementTerms": $scope.listRequirementTerms,
                    sessionID: $scope.currentSessionId
                };

                PRMForwardBidService.SaveRequirementTerms(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    });
            };

            $scope.listTerms = [];

            $scope.DeleteRequirementTerms = function () {

                var params = {
                    "listTerms": $scope.listTerms,
                    sessionID: $scope.currentSessionId
                };

                PRMForwardBidService.DeleteRequirementTerms(params)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    });
            };

            $scope.mrpDiscountCalculation = function () {

                if ($scope.auctionItem.isDiscountQuotation == 1 || $scope.auctionItem.isDiscountQuotation == 2) {

                    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                        if ($scope.auctionItem.isDiscountQuotation == 2) {


                            if ($scope.auctionItem.biddingType === 'SPOT' ? $scope.auctionItem.isTabular && $scope.evaluateBiddingTypeBasedOnStatus() : $scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                item.revCostPrice = item.costPrice;
                                item.revmarginAmount = item.marginAmount;
                            }

                            item = $scope.handlePrecision(item, 2);
                            item.cGst = item.Gst / 2;
                            item.sGst = item.Gst / 2;
                            item = $scope.handlePrecision(item, 2);
                            item.netPrice = 1 + (item.Gst / 100);
                            item = $scope.handlePrecision(item, 2);
                            item.netPrice = item.costPrice * item.netPrice;
                            item = $scope.handlePrecision(item, 2);
                            item.marginAmount = item.unitMRP - item.netPrice;

                            item.revnetPrice = 1 + (item.Gst / 100);
                            item = $scope.handlePrecision(item, 2);
                            item.revnetPrice = item.revCostPrice * item.revnetPrice;
                            item = $scope.handlePrecision(item, 2);
                            item.revmarginAmount = item.unitMRP - item.revnetPrice;


                            item = $scope.handlePrecision(item, 2);
                            //item.unitDiscount = item.marginAmount / item.costPrice;
                            item.unitDiscount = item.marginAmount / item.netPrice;
                            item.unitDiscount = item.unitDiscount * 100;
                            item = $scope.handlePrecision(item, 2);
                            item.revitemPrice = item.revCostPrice * (1 + item.Gst / 100);
                            item = $scope.handlePrecision(item, 2);
                            item.revmarginAmount = item.unitMRP - item.revitemPrice;
                            item = $scope.handlePrecision(item, 2);
                            //item.revUnitDiscount = item.revmarginAmount / item.revCostPrice;
                            item.revUnitDiscount = item.revmarginAmount / item.revnetPrice;

                            item.revUnitDiscount = item.revUnitDiscount * 100;
                            item = $scope.handlePrecision(item, 2);
                        }

                        if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                            item.revUnitDiscount = item.unitDiscount;

                        }

                        if (item.unitDiscount > 100) {
                            swal("Error!", 'Please enter valid discount percentage ');
                            item.unitDiscount = item.TemperoryUnitDiscount;
                        }
                        if (item.revUnitDiscount > 100) {
                            swal("Error!", 'Please enter valid discount percentage ');
                            item.revUnitDiscount = item.TemperoryRevUnitDiscount;
                        }

                        if ($scope.auctionItem.biddingType === 'SPOT' ? $scope.auctionItem.isTabular && $scope.evaluateBiddingTypeBasedOnStatus() : $scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                            item.tempUitMRP = 0;
                            item.tempUnitDiscount = 0;

                            item.tempUitMRP = item.unitMRP;
                            item.tempUnitDiscount = item.unitDiscount;

                            if (item.unitMRP == undefined || item.unitMRP <= 0) {
                                item.unitMRP = 0;
                            }
                            if (item.unitDiscount == undefined || item.unitDiscount <= 0) {
                                item.unitDiscount = 0;
                            }

                            item.unitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.unitDiscount));




                            item.unitMRP = item.tempUitMRP;
                            item.unitDiscount = item.tempUnitDiscount;

                        }

                        item.reductionPrice = (item.unitMRP - item.unitPrice) * item.productQuantity;
                        item.reductionPrice = $scope.precisionRound(parseFloat(item.reductionPrice), $rootScope.companyRoundingDecimalSetting);

                        if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'Negotiation Ended' || $scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {

                            item.tempUitMRP = 0;
                            item.tempRevUnitDiscount = 0;

                            item.tempUitMRP = item.unitMRP;
                            item.tempRevUnitDiscount = item.revUnitDiscount;

                            if (item.unitMRP == undefined || item.unitMRP <= 0) {
                                item.unitMRP = 0;
                            }
                            if (item.revUnitDiscount == undefined || item.revUnitDiscount <= 0) {
                                item.revUnitDiscount = 0;
                            }

                            item.revUnitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.revUnitDiscount));

                            item.revReductionPrice = (item.unitMRP - item.revUnitPrice) * item.productQuantity;

                            item.revReductionPrice = $scope.precisionRound(parseFloat(item.revReductionPrice), $rootScope.companyRoundingDecimalSetting);


                            item.unitMRP = item.tempUitMRP;
                            item.revUnitDiscount = item.tempRevUnitDiscount;

                        }

                    });
                }
            };

            $scope.precisionRound = function (number, precision) {
                var factor = Math.pow(10, precision);
                return Math.round(number * factor) / factor;
            };

            $scope.downloadTemplate = function () {

                var name = '';
                if ($scope.auctionItem.isDiscountQuotation == 2) {
                    if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                        name = 'MARGIN_QUOTATION_' + $scope.reqId;
                    }
                    else {
                        name = 'MARGIN_REV_QUOTATION_' + $scope.reqId;
                    }
                } else {
                    name = 'UNIT_PRICE_QUOTATION_' + $scope.reqId;
                }


                PRMForwardBidService.downloadTemplate(name, $scope.currentUserId, $scope.reqId);
            };

            $scope.getReport = function (name) {
                PRMForwardBidService.downloadTemplate(name + "_" + $scope.reqId, $scope.currentUserId, $scope.reqId);
            };

            $scope.EnableMarginFields = function (val) {
                var params = {
                    isEnabled: val,
                    reqID: $scope.reqId,
                    sessionID: $scope.currentSessionId
                };
                PRMForwardBidService.EnableMarginFields(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Saved Successfully.", "success");
                        }
                        else {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    });
            };

            $scope.numToDecimal = function (val, decimalRound) {

                val = parseFloat(val);
                return val.toFixed(decimalRound);
            };

            $scope.GetReqDataPriceCap = function () {

                var params = {
                    'reqid': $scope.reqId,
                    'sessionid': $scope.currentSessionId
                };

                if ($scope.isCustomer && $scope.auctionItem.reqType && $scope.auctionItem.reqType === 'PRICE_CAP') {
                    PRMForwardBidService.GetReqData(params)
                        .then(function (response) {
                            $scope.priceCapData = response;

                            if ($scope.priceCapData && $scope.priceCapData.listRequirementItems && $scope.priceCapData.listRequirementItems.length > 0) {
                                $scope.priceCapData.listRequirementItems.forEach(function (item, index) {
                                    item.Gst = item.cGst + item.sGst + item.iGst;
                                });
                            }

                            $scope.priceCapCalculations();
                        });
                }
            };

            $scope.SavePriceCapItemLevel = function () {
                var params = {
                    reqID: $scope.reqId,
                    listReqItems: $scope.priceCapData.listRequirementItems,
                    price: $scope.formRequest.priceCapValue,
                    isDiscountQuotation: $scope.auctionItem.isDiscountQuotation,
                    sessionID: $scope.currentSessionId
                };
                PRMForwardBidService.SavePriceCap(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Saved Successfully.", "success");
                            if ($scope.auctionItem.status == 'STARTED') {
                                $scope.recalculate('', 0, true);
                            }
                            location.reload();
                        }
                        else {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    });
            };

            $scope.GetDifferentialFactorPrice = function (vendorID) {



                $scope.auctionItem.auctionVendors.forEach(function (vendor, index) {

                    if (vendorID == vendor.vendorID) {
                        vendor.changedDF = {
                            'background-color': 'yellow'
                        }
                    }
                    var QP = vendor.QP;
                    var BP = vendor.BP;
                    var RP = vendor.RP;
                    var DIFFERENTIAL_FACTOR = vendor.DIFFERENTIAL_FACTOR;
                    vendor.DF_REV_VENDOR_TOTAL_PRICE = parseFloat(vendor.revVendorTotalPrice) + parseFloat(DIFFERENTIAL_FACTOR);

                    if ($scope.auctionItem.status == 'Negotiation Ended' || $scope.auctionItem.status == 'STARTED') {
                        vendor.DF_REVISED_VENDOR_TOTAL_PRICE = parseFloat(vendor.revVendorTotalPrice) + parseFloat(DIFFERENTIAL_FACTOR);


                        vendor.QP = parseFloat(vendor.initialPrice) + parseFloat(vendor.DIFFERENTIAL_FACTOR);
                        vendor.BP = parseFloat(vendor.totalPriceIncl) + parseFloat(vendor.DIFFERENTIAL_FACTOR);
                        vendor.RP = parseFloat(vendor.revVendorTotalPrice) + parseFloat(vendor.DIFFERENTIAL_FACTOR);
                    }
                })

            };


            $scope.SaveDifferentialFactor = function (auctionVendor) {


                var params = {
                    'vendorID': auctionVendor.vendorID,
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': $scope.currentSessionId,
                    'customerID': $scope.currentUserId,
                    'value': auctionVendor.DIFFERENTIAL_FACTOR,
                    'reason': ''
                };

                PRMForwardBidService.SaveDifferentialFactor(params).then(function (req) {
                    if (req.errorMessage == '') {
                        growlService.growl('Saved Successfully.', "success");
                        var parties = params.reqID + "$" + $scope.currentUserId + "$" + $scope.currentSessionId + "$" + "SAVE_DIFFERENTIAL_FACTOR";
                        $scope.invokeSignalR('CheckRequirement', parties);
                    } else {
                        swal("Error!", req.errorMessage, "error");
                    }
                });
            };

            $scope.priceCapCalculations = function () {
                if ($scope.auctionItem.isDiscountQuotation == 1 || $scope.auctionItem.isDiscountQuotation == 0) {
                    $scope.formRequest.priceCapValue = 0;
                    if ($scope.priceCapData && $scope.priceCapData.listRequirementItems) {
                        $scope.priceCapData.listRequirementItems.forEach(function (item, index) {

                            //if ($scope.priceCapData.auctionVendors[0].isQuotationRejected != 0) {
                            //    item.revUnitDiscount = item.unitDiscount;
                            //}

                            item.cGst = item.Gst / 2;
                            item.sGst = item.Gst / 2;

                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                if ($scope.auctionItem.isDiscountQuotation == 1) {
                                    item.revUnitDiscount = item.unitDiscount;
                                    item.tempUitMRP = 0;
                                    item.tempUnitDiscount = 0;

                                    item.tempUitMRP = item.unitMRP;
                                    item.tempUnitDiscount = item.unitDiscount;

                                    if (item.unitMRP == undefined || item.unitMRP <= 0) {
                                        item.unitMRP = 0;
                                    }
                                    if (item.unitDiscount == undefined || item.unitDiscount <= 0) {
                                        item.unitDiscount = 0;
                                    }

                                    item.unitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.unitDiscount));

                                    item.unitMRP = item.tempUitMRP;
                                    item.unitDiscount = item.tempUnitDiscount;


                                    if ($scope.auctionItem.isDiscountQuotation == 1) {
                                        item.reductionPrice = (item.unitMRP - item.unitPrice) * item.productQuantity;
                                        item.reductionPrice = $scope.precisionRound(parseFloat(item.reductionPrice), $rootScope.companyRoundingDecimalSetting);
                                    }

                                }


                                ////////////////////
                                item.sGst = item.cGst;
                                var tempUnitPrice = item.unitPrice;
                                var tempCGst = item.cGst;
                                var tempSGst = item.sGst;
                                var tempIGst = item.iGst;
                                if (item.unitPrice == undefined || item.unitPrice <= 0) {
                                    item.unitPrice = 0;
                                };

                                if ($scope.auctionItem.isDiscountQuotation == 0) {
                                    item.revUnitPrice = item.unitPrice;
                                }

                                item.itemPrice = item.unitPrice * item.productQuantity;


                                item.itemPrice = item.itemPrice + ((item.itemPrice / 100) * (item.cGst + item.sGst + item.iGst));

                                item.unitPrice = tempUnitPrice;
                                item.cGst = tempCGst
                                item.sGst = tempSGst;
                                item.iGst = tempIGst;

                                if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                                    // $scope.gstValidation = true;
                                    // not mandatory
                                };
                                /////////////////////

                            }

                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'Negotiation Ended' || $scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {

                                if ($scope.auctionItem.isDiscountQuotation == 1) {
                                    item.tempUitMRP = 0;
                                    item.tempRevUnitDiscount = 0;

                                    item.tempUitMRP = item.unitMRP;
                                    item.tempRevUnitDiscount = item.revUnitDiscount;

                                    if (item.unitMRP == undefined || item.unitMRP <= 0) {
                                        item.unitMRP = 0;
                                    }
                                    if (item.revUnitDiscount == undefined || item.revUnitDiscount <= 0) {
                                        item.revUnitDiscount = 0;
                                    }

                                    item.revUnitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.revUnitDiscount));

                                    item.revReductionPrice = (item.unitMRP - item.revUnitPrice) * item.productQuantity;

                                    item.revReductionPrice = $scope.precisionRound(parseFloat(item.revReductionPrice), $rootScope.companyRoundingDecimalSetting);


                                    item.unitMRP = item.tempUitMRP;
                                    item.revUnitDiscount = item.tempRevUnitDiscount;
                                }

                                ////////////////////////
                                var tempRevUnitPrice = item.revUnitPrice;
                                tempCGst = item.cGst;
                                tempSGst = item.sGst;
                                tempIGst = item.iGst;

                                if (item.revUnitPrice == undefined || item.revUnitPrice <= 0) {
                                    item.revUnitPrice = 0;
                                };

                                item.revitemPrice = item.revUnitPrice * item.productQuantity;

                                if (item.cGst == undefined || item.cGst <= 0) {
                                    item.cGst = 0;
                                };
                                if (item.sGst == undefined || item.sGst <= 0) {
                                    item.sGst = 0;
                                };
                                if (item.iGst == undefined || item.iGst <= 0) {
                                    item.iGst = 0;
                                };

                                item.revitemPrice = item.revitemPrice + ((item.revitemPrice / 100) * (item.cGst + item.sGst + item.iGst));

                                item.revUnitPrice = tempRevUnitPrice;
                                item.cGst = tempCGst
                                item.sGst = tempSGst;
                                item.iGst = tempIGst;

                                if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                                    // $scope.gstValidation = true;
                                    // not mandatory
                                }
                                //////////////////////

                            }

                            if (item.revitemPrice > 0) {
                                $scope.formRequest.priceCapValue = parseFloat($scope.formRequest.priceCapValue);
                                $scope.formRequest.priceCapValue += item.revitemPrice;
                            }


                            //item.unitPrice = $scope.numToDecimal(item.unitPrice, $rootScope.companyRoundingDecimalSetting);
                            item.itemPrice = $scope.numToDecimal(item.itemPrice, $rootScope.companyRoundingDecimalSetting);

                            //item.revUnitPrice = $scope.numToDecimal(item.revUnitPrice, $rootScope.companyRoundingDecimalSetting);
                            item.revitemPrice = $scope.numToDecimal(item.revitemPrice, $rootScope.companyRoundingDecimalSetting);


                            $scope.formRequest.priceCapValue = $scope.numToDecimal($scope.formRequest.priceCapValue, $rootScope.companyRoundingDecimalSetting);
                        });
                    }
                }
            };

            $scope.removeAttach = function (index) {
                $scope.multipleAttachmentsList.splice(index, 1);
                $scope.auctionItem.auctionVendors[0].multipleAttachmentsList.splice(index, 1);
            };

            $scope.regretHandle = function (item) {
                if (item.isRegret) {
                    item.unitPrice = 0;
                    item.unitMRP = 0;
                    item.unitDiscount = 0;
                    item.Gst = 0;
                    item.sGst = 0;
                    item.cGst = 0;
                    item.iGst = 0;
                } else {
                    item.regretComments = '';
                }


                if ($scope.totalprice == 0) {

                    $("#totalprice").val(0);
                    $("#revtotalprice").val(0);
                    $("#vendorBidPrice").val(0);
                    $("#revvendorBidPrice").val(0);

                }
            };

            $scope.showVendorRegretDetails = function (vendor) {
                $scope.regretDetails = vendor.listRequirementItems;
            };

            //#CB-0-2018-12-05
            $scope.goToCbCustomer = function (vendorID) {
                var url = $state.href("cb-customer", { "reqID": $scope.reqId, "vendorID": vendorID });
                window.open(url, '_blank');
            };

            $scope.UpdateCBTime = function () {

                if ($scope.auctionItem.auctionVendors.length == 0 || !$scope.auctionItem.auctionVendors[0] || $scope.auctionItem.auctionVendors[0].quotationUrl == "" || $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 || $scope.starttimecondition1 != 0) {
                    $scope.Loding = false;
                    swal("Not Allowed", "You are not allowed to create a end time until at least one vendor Approved.", "error");
                    return;
                }


                var CB_END_TIME = $("#CB_END_TIME").val(); //$scope.startTime; //Need fix on this.

                if (CB_END_TIME && CB_END_TIME != null && CB_END_TIME != "") {

                    //var ts = moment($scope.startTime, "DD-MM-YYYY HH:mm").valueOf();
                    var ts = moment(CB_END_TIME, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var auctionStartDate = new Date(m);
                    auctionsService.getdate()
                        .then(function (getdateResponse) {
                            var CurrentDateToLocal = userService.toLocalDate(getdateResponse);
                            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                            var m = moment(ts);
                            var deliveryDate = new Date(m);
                            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                            var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));

                            if (CurrentDate >= auctionStartDate) {
                                $scope.Loding = false;
                                swal("Done!", "End time should be greater than current time.", "error");
                                return;
                            }

                            swal({
                                title: "Are you sure?",
                                text: "Your negotiation process will be automatically closed at " + $scope.formRequest.CB_END_TIME + ".",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#F44336",
                                confirmButtonText: "OK",
                                closeOnConfirm: true
                            }, function () {

                                var ts = moment($scope.formRequest.CB_END_TIME, "DD-MM-YYYY HH:mm").valueOf();
                                var m = moment(ts);
                                var date = new Date(m);
                                var milliseconds = parseInt(date.getTime() / 1000.0);
                                // // #INTERNATIONALIZATION-0-2019-02-12
                                var cbEndTime = "/Date(" + userService.toUTCTicks(CB_END_TIME) + "+0530)/";
                                //000

                                var params = {
                                    cbEndTime: cbEndTime,
                                    userID: $scope.currentUserId,
                                    reqID: $scope.reqId,
                                    sessionID: $scope.currentSessionId
                                };

                                PRMForwardBidService.UpdateCBTime(params)
                                    .then(function (response) {
                                        if (response.errorMessage != "") {
                                            growlService.growl(response.errorMessage, "inverse");
                                        } else {
                                            $scope.recalculate('UPDATE_CB_TIME_CUSTOMER', 0, false);
                                            growlService.growl('Saved Successfully', "inverse");
                                        }
                                    });
                            });
                        });
                }
                else {
                    swal("Done!", "Please enter the date and time to update End Time.", "error");
                    //alert("Please enter the date and time to update Start Time to.");
                }


            };

            $scope.CBStopQuotations = function (value) {

                var params = {
                    stopCBQuotations: value,
                    userID: $scope.currentUserId,
                    reqID: $scope.reqId,
                    sessionID: $scope.currentSessionId
                };

                swal({
                    title: "Are you sure?",
                    text: "You will not receive any further quotations.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    PRMForwardBidService.CBStopQuotations(params)
                        .then(function (response) {
                            if (response.errorMessage != "") {
                                growlService.growl(response.errorMessage, "inverse");
                            } else {
                                $scope.recalculate('CB_STOP_QUOTATIONS_CUSTOMER', 0, false);
                                growlService.growl('Saved Successfully', "inverse");
                            }
                        });
                });
            };

            $scope.CBMarkAsComplete = function (value) {

                var params = {
                    isCBCompleted: value,
                    userID: $scope.currentUserId,
                    reqID: $scope.reqId,
                    sessionID: $scope.currentSessionId
                };


                PRMForwardBidService.CBMarkAsComplete(params)
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            $scope.recalculate('CB_MARK_AS_COMPLETED_CUSTOMER', 0, false);
                            $scope.getrevisedquotations();
                            growlService.growl('Saved Successfully', "inverse");
                        }
                    });
            };

            $scope.changeBiddingType = function (value) {
                if (value == '1' || value == '2') {
                    $scope.IS_CB_ENABLED = true;
                    if (value == '2') {
                        $scope.IS_CB_NO_REGRET = true;
                    } else {
                        $scope.IS_CB_NO_REGRET = false;
                    }
                }
                if (value == '0') {
                    $scope.IS_CB_ENABLED = false;
                }
            };

            $scope.shouldShow = function (permissionLevel) {
                // put your authorization logic here
                if (permissionLevel) {
                    return true;
                } else {
                    return false;
                }
            };


            $scope.markAsCompleteREQ = function (reqId, value, isMACForCBOrNot) {
                var params = {
                    isREQCompleted: value,
                    userID: $scope.currentUserId,
                    reqID: reqId,
                    sessionID: $scope.currentSessionId
                };

                swal({
                    title: "Once requirement is marked as completed user will not have privileges to edit this requirement.",
                    text: "Kindly confirm.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    PRMForwardBidService.markAsCompleteREQ(params)
                        .then(function (response) {
                            if (response.errorMessage != "") {
                                growlService.growl(response.errorMessage, "inverse");
                            } else {
                                growlService.growl('Saved Successfully', "success");
                                location.reload();
                                if (isMACForCBOrNot == true) {
                                    $scope.CBMarkAsComplete(1);
                                }
                            }
                        });
                });
            };

            $scope.GetItemUnitPrice = function (isPerUnit, templateIndex) {
                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (isPerUnit) {
                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                            item.unitPrice = 0;
                            item.revUnitPrice = 0;
                            item.UNIT_PRICE = 0;
                            item.REV_UNIT_PRICE = 0;
                            item.margin15 = 0;
                            item.revmargin15 = 0;
                            item.margin3 = 0;
                            item.revmargin3 = 0;

                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                if (templateIndex == subItemIndex || templateIndex == -1) {
                                    if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                                        subItem.REV_BULK_PRICE = subItem.BULK_PRICE;
                                        subItem.REV_CONSUMPTION = subItem.CONSUMPTION;
                                        subItem.REV_TAX = subItem.TAX;
                                        subItem.REV_UNIT_PRICE = subItem.UNIT_PRICE;
                                    }

                                    var BULK_PRICE = 0;
                                    if (parseFloat(subItem.BULK_PRICE) > 0) {
                                        BULK_PRICE = parseFloat(subItem.BULK_PRICE);
                                    }
                                    var REV_BULK_PRICE = 0;
                                    if (parseFloat(subItem.REV_BULK_PRICE) > 0) {
                                        REV_BULK_PRICE = parseFloat(subItem.REV_BULK_PRICE);
                                    }

                                    var CONSUMPTION = 0;
                                    if (parseFloat(subItem.CONSUMPTION) > 0) {
                                        CONSUMPTION = parseFloat(subItem.CONSUMPTION);
                                    }
                                    var REV_CONSUMPTION = 0;
                                    if (parseFloat(subItem.REV_CONSUMPTION) > 0) {
                                        REV_CONSUMPTION = parseFloat(subItem.REV_CONSUMPTION);
                                    }

                                    var TAX = 0;
                                    if (parseFloat(subItem.TAX) > 0) {
                                        TAX = parseFloat(subItem.TAX);
                                    }
                                    var REV_TAX = 0;
                                    if (parseFloat(subItem.REV_TAX) > 0) {
                                        REV_TAX = parseFloat(subItem.REV_TAX);
                                    }

                                    var UNIT_PRICE = 0;
                                    if (parseFloat(subItem.UNIT_PRICE) > 0) {
                                        UNIT_PRICE = parseFloat(subItem.UNIT_PRICE);
                                    }
                                    var REV_UNIT_PRICE = 0;
                                    if (parseFloat(subItem.REV_UNIT_PRICE) > 0) {
                                        REV_UNIT_PRICE = parseFloat(subItem.REV_UNIT_PRICE);
                                    }

                                    //UNIT_PRICE = BULK_PRICE * CONSUMPTION;
                                    //REV_UNIT_PRICE = REV_BULK_PRICE * REV_CONSUMPTION;
                                    if (CONSUMPTION > 0) {
                                        subItem.BULK_PRICE = UNIT_PRICE / CONSUMPTION;
                                    }
                                    else {
                                        subItem.BULK_PRICE = 0;
                                    }
                                    if (REV_CONSUMPTION > 0) {
                                        subItem.REV_BULK_PRICE = REV_UNIT_PRICE / REV_CONSUMPTION;
                                    }
                                    else {
                                        subItem.REV_BULK_PRICE = 0;
                                    }
                                    //subItem.UNIT_PRICE = UNIT_PRICE;
                                    //subItem.REV_UNIT_PRICE = REV_UNIT_PRICE;

                                    //if (subItem.IS_CALCULATED == 0) {
                                    //    //item.unitPrice += UNIT_PRICE;
                                    //    //item.revUnitPrice += REV_UNIT_PRICE;
                                    //    item.UNIT_PRICE += UNIT_PRICE;
                                    //    item.REV_UNIT_PRICE += REV_UNIT_PRICE;

                                    //}
                                }
                            });
                        }
                    }
                    else {
                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0 && $scope.isUnitPriceEditableForSubItems(item.productQuotationTemplateArray)) {
                            item.unitPrice = 0;
                            item.revUnitPrice = 0;
                            item.UNIT_PRICE = 0;
                            item.REV_UNIT_PRICE = 0;
                            item.margin15 = 0;
                            item.revmargin15 = 0;
                            item.margin3 = 0;
                            item.revmargin3 = 0;

                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                if (templateIndex == subItemIndex || templateIndex == -1) {
                                    if ($scope.auctionItem.biddingType === 'SPOT' ? $scope.evaluateBiddingTypeBasedOnStatus() : $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                                        subItem.REV_BULK_PRICE = subItem.BULK_PRICE;
                                        subItem.REV_CONSUMPTION = subItem.CONSUMPTION;
                                        subItem.REV_TAX = subItem.TAX;
                                        subItem.REV_UNIT_PRICE = subItem.UNIT_PRICE;
                                    }

                                    var BULK_PRICE = 0;
                                    if (parseFloat(subItem.BULK_PRICE) > 0) {
                                        BULK_PRICE = parseFloat(subItem.BULK_PRICE);
                                    }
                                    var REV_BULK_PRICE = 0;
                                    if (parseFloat(subItem.REV_BULK_PRICE) > 0) {
                                        REV_BULK_PRICE = parseFloat(subItem.REV_BULK_PRICE);
                                    }

                                    var CONSUMPTION = 0;
                                    if (parseFloat(subItem.CONSUMPTION) > 0) {
                                        CONSUMPTION = parseFloat(subItem.CONSUMPTION);
                                    }
                                    var REV_CONSUMPTION = 0;
                                    if (parseFloat(subItem.REV_CONSUMPTION) > 0) {
                                        REV_CONSUMPTION = parseFloat(subItem.REV_CONSUMPTION);
                                    }

                                    var TAX = 0;
                                    if (parseFloat(subItem.TAX) > 0) {
                                        TAX = parseFloat(subItem.TAX);
                                    }
                                    var REV_TAX = 0;
                                    if (parseFloat(subItem.REV_TAX) > 0) {
                                        REV_TAX = parseFloat(subItem.REV_TAX);
                                    }

                                    var UNIT_PRICE = 0;
                                    if (parseFloat(subItem.UNIT_PRICE) > 0) {
                                        UNIT_PRICE = parseFloat(subItem.UNIT_PRICE);
                                    }
                                    var REV_UNIT_PRICE = 0;
                                    if (parseFloat(subItem.REV_UNIT_PRICE) > 0) {
                                        REV_UNIT_PRICE = parseFloat(subItem.REV_UNIT_PRICE);
                                    }

                                    UNIT_PRICE = BULK_PRICE * CONSUMPTION + ((BULK_PRICE * CONSUMPTION) * (TAX / 100));
                                    REV_UNIT_PRICE = REV_BULK_PRICE * REV_CONSUMPTION + ((REV_BULK_PRICE * REV_CONSUMPTION) * (TAX / 100));

                                    subItem.UNIT_PRICE = UNIT_PRICE;
                                    subItem.REV_UNIT_PRICE = REV_UNIT_PRICE;

                                    //subItem.CEILING_PRICE = +subItem.CEILING_PRICE;
                                    //if (subItem.HAS_PRICE && subItem.CEILING_PRICE > 0) {
                                    //    item.ceilingPrice += parseFloat(subItem.CEILING_PRICE);
                                    //}
                                    //if (subItem.IS_CALCULATED == 0) {
                                    //    //item.unitPrice += UNIT_PRICE;
                                    //    //item.revUnitPrice += REV_UNIT_PRICE;
                                    //    item.UNIT_PRICE += UNIT_PRICE;
                                    //    item.REV_UNIT_PRICE += REV_UNIT_PRICE;
                                    //}
                                }
                            });
                        }
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    //item.ceilingPrice = 0;
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.IS_CALCULATED == 0) {
                                if (subItem.UNIT_PRICE > 0) {
                                    item.UNIT_PRICE += parseFloat(subItem.UNIT_PRICE);
                                }
                                if (subItem.REV_UNIT_PRICE > 0) {
                                    item.REV_UNIT_PRICE += parseFloat(subItem.REV_UNIT_PRICE);
                                }
                                if (subItem.HAS_PRICE && +subItem.CEILING_PRICE > 0) {
                                    subItem.CEILING_PRICE1 = +subItem.CEILING_PRICE;
                                    item.ceilingPrice = _.sumBy(item.productQuotationTemplateArray, 'CEILING_PRICE1');
                                }
                            }
                        });
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        //item.margin15 = (item.UNIT_PRICE / 100) * 15;
                        //item.revmargin15 = (item.REV_UNIT_PRICE / 100) * 15;

                        //item.margin3 = (item.UNIT_PRICE / 100) * 3;
                        //item.revmargin3 = (item.REV_UNIT_PRICE / 100) * 3;
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.IS_CALCULATED == 1) {
                                subItem.UNIT_PRICE = item.UNIT_PRICE;
                                subItem.REV_UNIT_PRICE = item.REV_UNIT_PRICE;
                            }
                            if (subItem.IS_CALCULATED == 2) {
                                subItem.UNIT_PRICE = item.margin15;
                                subItem.REV_UNIT_PRICE = item.revmargin15;
                            }
                            if (subItem.IS_CALCULATED == 3) {
                                subItem.UNIT_PRICE = item.margin3;
                                subItem.REV_UNIT_PRICE = item.revmargin3;
                            }
                            if (subItem.IS_CALCULATED == 4) {
                                item.UNIT_PRICE = parseFloat(item.UNIT_PRICE) + parseFloat(subItem.UNIT_PRICE);
                                item.REV_UNIT_PRICE = parseFloat(item.REV_UNIT_PRICE) + parseFloat(subItem.REV_UNIT_PRICE);
                            }
                        });
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0 && $scope.isUnitPriceEditableForSubItems(item.productQuotationTemplateArray)) {
                        item.unitPrice = parseFloat(item.UNIT_PRICE) + parseFloat(item.margin15) + parseFloat(item.margin3);
                        item.revUnitPrice = parseFloat(item.REV_UNIT_PRICE) + parseFloat(item.revmargin15) + parseFloat(item.revmargin3);
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.IS_CALCULATED == 5) {
                                subItem.UNIT_PRICE = item.unitPrice;
                                subItem.REV_UNIT_PRICE = item.revUnitPrice;
                            }
                        });
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.SPECIFICATION) {
                                subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/\'/gi, "");
                                subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/\"/gi, "");
                                subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                                subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/(\r\n|\n|\r)/gm, "");
                                subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/\t/g, '');
                            }
                        });

                        item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplateArray);

                    } else {
                        item.productQuotationTemplateJson = '';
                    }
                });

            };


            $scope.VendorItemlevelPrices = [];
            $scope.showVendorItemlevelPrices = function (vendor) {
                $scope.disableSubmitAllItemApproval = true;
                $scope.selectedVendorForItemLevelPrices = vendor;
                $scope.VendorItemlevelPrices = [];
                $scope.VendorItemlevelPrices = vendor.listRequirementItems;

                $scope.VendorItemlevelPrices.sort(function (a, b) {
                    return b.isCoreProductCategory - a.isCoreProductCategory;
                });

                $scope.VendorItemlevelPrices.map((element) => {
                    return element.otherProperties = vendor.otherProperties;
                });

                $scope.VendorItemlevelPrices.map((element1) => {
                    return element1.surrogateComments = vendor.surrogateComments;
                });

                console.log($scope.VendorItemlevelPrices);
            };



            $scope.createLot = function (requirementId) {

                swal({
                    title: "Once Lot Requirement is Created Vendors cannot submit Quotations for this Requirement.",
                    text: "Kindly confirm.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    PRMLotReqService.lotdetails(0, requirementId)
                        .then(function (response) {
                            if (response) {
                                if (response) {
                                    $state.go("save-lot-details", { "Id": response.LotId, "reqId": requirementId });
                                }
                            }
                        });
                });


            };

            $scope.isValidLotBidding = function () {
                if ($scope.auctionItem.auctionVendors.length == 0 || $scope.auctionItem.auctionVendors[0].quotationUrl == "" || $scope.auctionItem.auctionVendors[1].quotationUrl == "" || $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 || $scope.auctionItem.auctionVendors[1].isQuotationRejected != 0 || $scope.starttimecondition1 != 0 || $scope.starttimecondition2 != 0) {
                    return false;
                }
                else {
                    return true;
                }
            };

            $scope.handleSubItemsVisibility = function (product) {
                product.subIemsColumnsVisibility = {
                    hideSPECIFICATION: true,
                    hideQUANTITY: true,
                    hideTAX: true,
                    hidePRICE: true
                };
                var productQuotationTemplateArray = product.productQuotationTemplateArray;
                if (productQuotationTemplateArray && productQuotationTemplateArray.length > 0) {
                    var items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_SPECIFICATION === 1);
                    });

                    if (items && items.length > 0) {
                        product.subIemsColumnsVisibility.hideSPECIFICATION = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_QUANTITY === 1);
                    });

                    if (items && items.length > 0) {
                        product.subIemsColumnsVisibility.hideQUANTITY = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_TAX === 1);
                    });

                    if (items && items.length > 0) {
                        product.subIemsColumnsVisibility.hideTAX = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_PRICE === 1);
                    });

                    if (items && items.length > 0) {
                        product.subIemsColumnsVisibility.hidePRICE = false;
                    }

                    //product.subIemsColumnsVisibility = _.clone($scope.subIemsColumnsVisibility);
                }
            };

            $scope.isUnitPriceEditableForSubItems = function (productQuotationTemplateArray) {
                var isDisabled = false;
                if (productQuotationTemplateArray && productQuotationTemplateArray.length > 0) {
                    var hasOnlySpecificationItems = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_PRICE === 1);
                    });

                    if (hasOnlySpecificationItems && hasOnlySpecificationItems.length > 0) {
                        isDisabled = true;
                    }
                }

                return isDisabled;
            };

            $scope.isTaxFieldEditableForSubItems = function (productQuotationTemplateArray) {
                var isDisabled = false;
                if (productQuotationTemplateArray && productQuotationTemplateArray.length > 0) {
                    var hasOnlySpecificationItems = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_TAX === 1);
                    });

                    if (hasOnlySpecificationItems && hasOnlySpecificationItems.length > 0) {
                        isDisabled = true;
                    }
                }

                return isDisabled;
            };

            $scope.getCustomFieldsByModuleId = function () {
                params = {
                    "compid": $scope.currentUserCompID,
                    "fieldmodule": 'REQUIREMENT',
                    "moduleid": $stateParams.Id,
                    "sessionid": $scope.currentSessionId
                };

                PRMCustomFieldService.GetCustomFieldsByModuleId(params)
                    .then(function (response) {
                        $scope.selectedcustomFieldList = response;
                    });
            };

            //$scope.getCustomFieldsByModuleId(); //Temporarly disabled

            $scope.IsAllChecked = false;
            $scope.isSaveEmailDisabled = true;
            // $scope.disableToOtherUsers = [];



            $scope.SendEmailLinksToVendors = function (emailToVendors) {

                $scope.vendors = [];

                emailToVendors.forEach(function (item, index) {
                    if (item.isEmailSent == true) {
                        if (item.WF_ID > 0) {
                            item.VendorObj =
                            {
                                vendorID: item.vendorID,
                                WF_ID: item.WF_ID
                            }
                            item = item.VendorObj;
                            $scope.vendors.push(item);
                        } else {
                            $scope.auctionItemTemporary.emailLinkVendors.forEach(function (mand, mandIndex) {
                                if (mand.vendorID == item.vendorID) {
                                    mand.showingWorkflowErrorMessage = 'Please Select Workflow';
                                }
                            })
                            return;
                        }
                    }

                });

                if ($scope.vendors.length <= 0) {
                    growlService.growl("Mandatory fields are required.", "inverse");
                    return;
                }


                var params =
                {
                    userID: $scope.currentUserId,
                    vendorIDs: $scope.vendors,
                    sessionID: $scope.currentSessionId
                };

                auctionsService.shareemaillinkstoVendors(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            angular.element('#showVendors').modal('hide');
                            growlService.growl("Email Has Been Send To The Selected Vendors ", "success");
                            $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                                item.isEmailSent = false;
                                item.WF_ID = 0;
                                item.showingWorkflowErrorMessage = '';
                            });

                            $scope.IsAllChecked = false;
                        }
                    });
            };

            $scope.showErrorMessagesForVendors = function () {
                $scope.disableVendors = [];
                var vendorsString = '';
                vendorsString = $scope.existingVendors.join(',');

                var params =
                {
                    userID: $scope.currentUserId,
                    vendorIDs: vendorsString,
                    sessionID: $scope.currentSessionId
                };

                auctionsService.GetSharedLinkVendors(params)
                    .then(function (response) {
                        //$scope.auctionItemTemporary.emailLinkVendors
                        $scope.disableVendors = response;

                        $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                            item.isDisable = false;
                            item.message = '';
                            item.errorMessage = '';
                            $scope.disableVendors.forEach(function (item1, index1) {
                                if (item.vendorID == item1.vendorID) {
                                    item.isDisable = true;
                                    item.message = 'Vendor has already received the registration link by';
                                    item.errorMessage = item1.errorMessage;
                                }
                            });
                        });
                    });
            };

            $scope.selectAll = function (value) {
                if (value) {
                    $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                        item.isEmailSent = true;
                        $scope.disableVendors.forEach(function (item1, index1) {
                            if (item.vendorID == item1.vendorID) {
                                item.isEmailSent = false;
                            }
                            return;
                        })
                    });
                } else {
                    $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                        item.isEmailSent = false;
                    });
                    //$scope.isSaveEmailDisabled = true;
                }

            };


            $scope.selectedVendor = function () {
                //console.log($scope.vendors.length);
            }


            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                if ($scope.isCustomer && $scope.auctionItem.status === 'Negotiation Ended') {
                    workflowService.getWorkflowList()
                        .then(function (response) {
                            $scope.workflowList = [];
                            $scope.workflowListTemp = response;
                            $scope.workflowListTemp.forEach(function (item, index) {
                                if (item.WorkflowModule == $scope.WorkflowModule) {
                                    $scope.workflowList.push(item);

                                }
                            });

                            if ($scope.auctionItemTemporary) {
                                $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                                    item.listWorkflows = $scope.workflowList;
                                });
                            }

                            if ($scope.isSuperUser) {
                                $scope.workflowList = $scope.workflowList;
                            }
                            else {
                                $scope.workflowList = $scope.workflowList.filter(function (item) {
                                    return item.deptID == userService.getSelectedUserDepartmentDesignation().deptID;

                                });
                            }
                        });
                }
            };


            /*region end WORKFLOW*/

            $scope.clearNonCoreItemsValues = function () {
                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                    if (!item.isCoreProductCategory) {
                        item.sGst = 0;
                        item.cGst = 0;
                        item.iGst = 0;
                        item.unitPrice = 0;
                    }
                });
            };

            $scope.fieldValidation = function () {
                if ($scope.auctionItem.auctionVendors.length > 0) {
                    $scope.auctionItem.auctionVendors.forEach(function (vendor, index) {
                        if (vendor.INCO_TERMS && !$scope.vendorIncoTerms[vendor.INCO_TERMS]) {
                            $scope.vendorIncoTerms[vendor.INCO_TERMS] = [];
                            auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, $scope.currentSessionId)
                                .then(function (response) {
                                    $scope.vendorIncoTerms[vendor.INCO_TERMS] = response;
                                    $scope.auctionItem.auctionVendors.forEach(function (vendor1, index) {
                                        if (vendor1.INCO_TERMS === vendor.INCO_TERMS) {
                                            validateIncoTerms(response);
                                        }
                                    });
                                });
                        } else if (vendor.INCO_TERMS && $scope.vendorIncoTerms[vendor.INCO_TERMS]) {
                            validateIncoTerms($scope.vendorIncoTerms[vendor.INCO_TERMS]);
                        }
                    });
                }
            };

            function validateIncoTerms(incoTerms) {
                incoTerms.forEach(function (incoItem, itemIndexs) {
                    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                        if (item.isCoreProductCategory == 0 && item.catalogueItemID == incoItem.ProductId) {
                            if ($scope.userIsOwner && incoItem.IS_CUSTOMER_EDIT) {
                                item.isEdit = false;
                            } else if (!$scope.userIsOwner && incoItem.IS_VENDOR_EDIT && $scope.totalprice > 0) {
                                item.isEdit = false;
                            } else {
                                item.isEdit = true;
                            }
                        } else if (item.isCoreProductCategory) {
                            item.isEdit = false;
                        }

                    });
                });
            }


            $scope.getDFTotalPrice = function (qp, oc) {
                qp = parseFloat(qp);
                oc = parseFloat(oc);
                var result = parseFloat(qp + oc).toFixed(2);
                return result;
            };

            $scope.detectLinks = function urlify(text) {
                if (text) {
                    var urlRegex = /(https?:\/\/[^\s]+)/g;
                    return text.replace(urlRegex, function (url) {
                        return '<a target="_blank" href="' + url + '">' + url + '</a>';
                    });
                }

            };

            $scope.saveItemLevelCeilingPrice = function () {
                var updatedItems = [];
                $scope.auctionItem.listRequirementItems.forEach(function (item, itemIndexs) {
                    let itemCeilingPrice = item.ceilingPrice;
                    let productQuotationJSON = JSON.stringify(item.productQuotationTemplateArray);
                    if ($scope.itemCeilingVendor) {
                        var vendorItem = $scope.itemCeilingVendor.listRequirementItems.filter(function (vendItem) {
                            return vendItem.itemID === item.itemID;
                        });

                        if (vendorItem && vendorItem.length > 0 && vendorItem[0].ceilingPrice) {
                            itemCeilingPrice = vendorItem[0].ceilingPrice;
                            productQuotationJSON = JSON.stringify(vendorItem[0].productQuotationTemplateArray)
                        }
                    }

                    updatedItems.push({
                        ceilingPrice: itemCeilingPrice,//item.ceilingPrice,
                        itemID: item.itemID,
                        requirementID: item.requirementID,
                        productQuotationTemplateJson: productQuotationJSON
                    });
                });

                var params = {
                    user: $scope.currentUserId,
                    vendorid: ($scope.itemCeilingVendor ? $scope.itemCeilingVendor.vendorID : 0),
                    reqid: $scope.reqId,
                    requirementitemlist: updatedItems,
                    sessionid: $scope.currentSessionId
                };

                PRMForwardBidService.saveRequirementItemList(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Details have been saved successfully", "inverse");
                            location.reload();
                        }
                    });
            };

            $scope.getItemCeilingPrie = function (itemId) {
                let ceilingPrice = 0;
                var requirementItem = $scope.auctionItem.listRequirementItems.filter(function (reqItem) {
                    return reqItem.itemID === itemId;
                });

                if (requirementItem && requirementItem.length > 0) {
                    ceilingPrice = requirementItem[0].ceilingPrice;
                }

                return ceilingPrice;
            };

            $scope.validateCeilingPrice = function (product, type) {
                if (type === 'ITEM') {
                    if (product && product.ceilingPrice > 0 && +product.unitPrice && +product.unitPrice < product.ceilingPrice) {
                        $timeout(function () {
                            swal("Error!", "Please enter Unit Price greater than ceiling price given.", "error");
                        }, 0);
                        product.unitPrice = product.ceilingPrice;
                        $scope.unitPriceCalculation('PRC');
                        $scope.getTotalPrice(0, 0, 0);
                        return;
                    }
                } else if (type === 'SUB_ITEM') {
                    if (product && product.ceilingPrice > 0) {
                        if (product.productQuotationTemplateArray && product.productQuotationTemplateArray.length > 0) {
                            product.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                if (subItem.HAS_PRICE && subItem.IS_VALID && subItem.CEILING_PRICE > 0) {
                                    if (+subItem.BULK_PRICE && +subItem.BULK_PRICE < subItem.CEILING_PRICE) {
                                        $timeout(function () {
                                            swal("Error!", "Please enter Unit Price greater than ceiling price given.", "error");
                                        }, 0);
                                        subItem.BULK_PRICE = subItem.CEILING_PRICE + 1;
                                        $scope.GetItemUnitPrice(false, subItemIndex);
                                        $scope.unitPriceCalculation('PRC');
                                        $scope.getTotalPrice(0, 0, 0);
                                        return;
                                    }
                                }
                            });
                        }
                    }
                }
            };

            $scope.hasOtherCharges = function () {
                let hasOtherCharges = false;
                let vendors = $scope.auctionItem.requirementVendorsList ? $scope.auctionItem.requirementVendorsList : $scope.auctionItem.auctionVendors;
                if ($scope.auctionItem && vendors && vendors.length > 0) {
                    let otherChargesVendor = vendors.filter(function (vendor) {
                        return vendor.DIFFERENTIAL_FACTOR > 0;
                    });

                    if (otherChargesVendor && otherChargesVendor.length > 0) {
                        hasOtherCharges = true;
                    }
                }

                return hasOtherCharges;
            };


            $scope.isValidCeilingAccept = function () {
                //auctionItem.auctionVendors[0].vendorCurrencyFactor
                let isValid = false;
                if ($scope.auctionItem) {
                    let vendors = $scope.auctionItem.requirementVendorsList ? $scope.auctionItem.requirementVendorsList : $scope.auctionItem.auctionVendors;
                    if (vendors && vendors.length > 0) {
                        let currentVendor = vendors.filter(function (vendor) {
                            return +vendor.vendorID === $scope.currentUserId;
                        });

                        if (currentVendor && currentVendor.length > 0 && currentVendor[0].listRequirementItems && currentVendor[0].listRequirementItems.length > 0) {
                            currentVendor[0].listRequirementItems.forEach(function (vendorItem, itemIndexs) {
                                var requirementItem = $scope.auctionItem.listRequirementItems.filter(function (reqItem) {
                                    return reqItem.itemID === vendorItem.itemID;
                                });

                                if (requirementItem && requirementItem.length > 0 && requirementItem[0].ceilingPrice && +vendorItem.revUnitPrice < (requirementItem[0].ceilingPrice / currentVendor[0].vendorCurrencyFactor).toFixed(2)) {
                                    isValid = true;
                                }
                            });
                        }
                    }
                }

                return isValid;
            };

            $scope.acceptCeilingPrices = function () {
                var currencyFactor = $scope.auctionItemVendor.auctionVendors[0].vendorCurrencyFactor;
                swal({
                    title: "Accept Ceiling Price ?",
                    text: "Please click to accept Ceiling Prices for all items in requirement.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    $scope.auctionItemVendor.listRequirementItems.forEach(function (vendorItem, itemIndexs) {
                        var requirementItem = $scope.auctionItem.listRequirementItems.filter(function (reqItem) {
                            return reqItem.itemID === vendorItem.itemID;
                        });

                        if (requirementItem && requirementItem.length > 0 && requirementItem[0].ceilingPrice && +vendorItem.revUnitPrice < (requirementItem[0].ceilingPrice / currencyFactor)) {
                            vendorItem.revUnitPriceOld = vendorItem.revUnitPrice;
                            vendorItem.revUnitPrice = (requirementItem[0].ceilingPrice / currencyFactor);
                            if (vendorItem.productQuotationTemplateArray && vendorItem.productQuotationTemplateArray.length > 0) {
                                vendorItem.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                    if (subItem.HAS_PRICE === 1 && subItem.IS_VALID === 1) {
                                        subItem.REV_BULK_PRICE = subItem.CEILING_PRICE;
                                        $scope.GetItemUnitPrice(false, subItemIndex);
                                    }
                                });
                            }
                            $scope.unitPriceCalculation('PRC');
                            $scope.makeBidUnitPriceValidation(vendorItem.revUnitPrice, vendorItem.TemperoryRevUnitPrice, vendorItem.productIDorName);
                            $scope.getRevTotalPrice(0, $scope.revtotalprice, $scope.revpackingChargesWithTax, $scope.revpackingCharges, $scope.revinstallationCharges);
                        }
                    });

                    $scope.makeaBid1();
                });
            };

            $scope.GetCompanyGSTInfo = function () {
                let params = {
                    companyId: $scope.currentUserCompID,
                    sessionid: $scope.sessionid
                };
                auctionsService.getCompanyGSTInfo(params)
                    .then(function (response) {
                        $scope.companyGSTInfo = response;

                        let currentVendor = $scope.auctionItem.auctionVendors[0];
                        if (currentVendor.quotationUrl == '' || currentVendor.quotationUrl == null || currentVendor.quotationUrl == undefined) {
                            $scope.getUserCredentials();
                        }
                    });
            };

            $scope.itemQuotationApproval = function (item, vendor) {
                var params = {
                    'vendorId': vendor.vendorID,
                    'reqId': $scope.auctionItem.requirementID,
                    'sessionId': $scope.currentSessionId,
                    'customerId': $scope.currentUserId,
                    'itemId': item.itemID,
                    'value': item.isItemQuotationRejected,
                    'comment': item.itemQuotationRejectedComment
                };

                PRMForwardBidService.ItemQuotationApproval(params).then(function (req) {
                    if (!req.errorMessage) {
                        toastr.info('Successfully saved.');

                    } else {
                        swal("Error!", req.errorMessage, "error");
                    }
                });
            };

            $scope.isItemFullyApproved = function (vendor, item, checkAuctionStatus) {
                let isApproved = false;
                item.quotationApprovedColor = {
                    'background-color': '#f5b2b2'
                };

                //The Below Code is because when the RFQ is selected as SPOT then vendor QUOTATION is Approved By default 
                //as this is done Unit price fields are getting disabled so to upload the QUOTE in NOTSTARTED state
                //we have to enable when the reduction setting type is OVERALL.
                if ($scope.auctionItem.biddingType === "SPOT") {
                    return $scope.enableWhenQuotationApproved();
                }

                if (vendor && vendor.isQuotationRejected === 0 && item.isItemQuotationRejected === 0) {
                    isApproved = true;
                    item.quotationApprovedColor = {};
                }

                if (checkAuctionStatus && !isApproved && $scope.auctionItem.status === 'STARTED') {
                    isApproved = true;
                    item.quotationApprovedColor = {};
                }

                //if (!item.isCoreProductCategory) {
                //    isApproved = true;
                //}

                return isApproved;
            };

            $scope.handleAllItemRejection = function (value) {
                $scope.disableSubmitAllItemApproval = false;
                if ($scope.VendorItemlevelPrices && $scope.VendorItemlevelPrices.length > 0) {
                    $scope.VendorItemlevelPrices.forEach(function (item, index) {
                        item.isItemQuotationRejected = value;
                        item.itemQuotationRejectedComment = $scope.formRequest.overallItemLevelComments;
                    });
                }
            };

            $scope.submitAllItemRejectionStatus = function (vendor) {
                if ($scope.VendorItemlevelPrices && $scope.VendorItemlevelPrices.length > 0) {
                    var params = {
                        'items': [],
                        'vendorId': vendor.vendorID,
                        'reqId': $scope.auctionItem.requirementID,
                        'customerId': $scope.currentUserId,
                        'sessionId': $scope.currentSessionId
                    };

                    $scope.VendorItemlevelPrices.forEach(function (currentItem, index) {
                        var item = {
                            itemID: currentItem.itemID,
                            isItemQuotationRejected: currentItem.isItemQuotationRejected,
                            itemQuotationRejectedComment: currentItem.itemQuotationRejectedComment
                        };

                        params.items.push(item);
                    });

                    PRMForwardBidService.ItemLevelQuotationApproval(params).then(function (req) {
                        if (!req.errorMessage) {
                            toastr.info('Successfully saved.');
                            location.reload();
                        } else {
                            swal("Error!", req.errorMessage, "error");
                        }
                    });
                }
            };

            $scope.anyItemQuotationsRejected = function () {
                let contains = false;
                if (!$scope.isCustomer && $scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0 &&
                    $scope.auctionItem.auctionVendors[0].listRequirementItems) {
                    let vendorQuotationItems = $scope.auctionItem.auctionVendors[0].listRequirementItems;
                    if (vendorQuotationItems && vendorQuotationItems.length > 0) {
                        var quotationRejetedItems = vendorQuotationItems.filter(function (reqItem) {
                            return reqItem.isItemQuotationRejected === 1;
                        });

                        if (quotationRejetedItems && quotationRejetedItems.length > 0) {
                            contains = true;
                        }
                    }
                }

                return contains;
            };

            $scope.itemQuotationsMessage = function () {
                let message = '';
                if (!$scope.isCustomer && $scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0 &&
                    $scope.auctionItem.auctionVendors[0].listRequirementItems) {
                    let vendorQuotationItems = $scope.auctionItem.auctionVendors[0].listRequirementItems;
                    if (vendorQuotationItems && vendorQuotationItems.length > 0) {
                        var quotationOtherItems = vendorQuotationItems.filter(function (reqItem) {
                            return reqItem.isItemQuotationRejected === -1 && reqItem.isCoreProductCategory;
                        });

                        if (quotationOtherItems && quotationOtherItems.length > 0) {
                            message = 'Your quotation partially reviewed. Please view below table for more details with status and comments.';
                        } else {
                            var quotationRejectedItems = vendorQuotationItems.filter(function (reqItem) {
                                return reqItem.isItemQuotationRejected === 1 && reqItem.isCoreProductCategory;
                            });

                            var quotationApprovedItems = vendorQuotationItems.filter(function (reqItem) {
                                return reqItem.isItemQuotationRejected === 0 && reqItem.isCoreProductCategory;
                            });

                            if (quotationRejectedItems && quotationApprovedItems && quotationRejectedItems.length > 0 && quotationApprovedItems.length > 0) {
                                message = 'Your quotation partially approved. Please view below table for more details with status and comments.';
                            }
                        }
                    }
                }

                return message;
            };

            $scope.getSubUserData = function () {
                if ($scope.isCustomer) {
                    userService.getCompanyUsers({ "compId": $scope.currentUserCompID, "sessionId": $scope.currentSessionId })
                        .then(function (response) {
                            response.forEach(function (cust, index) {

                            });

                            $scope.subUsers = $filter('filter')(response, { isValid: true });
                        });
                }
            };

            $scope.getSubUserData();


            function resetSurrogateObj() {
                $scope.surrogateObj = {
                    surrogates: [],
                    reqid: $scope.reqId,
                    comments: '',
                    user: $scope.currentUserId,
                    sessionid: $scope.currentSessionId
                };
            }

            resetSurrogateObj();
            $scope.searchSurrogate = function (value) {
                $scope.filteredSubUsers = [];
                if (value) {
                    $scope.filteredSubUsers = _.filter($scope.subUsers, function (item) {
                        return (item.email.toUpperCase().indexOf(value.toUpperCase()) > -1
                            || item.firstName.toUpperCase().indexOf(value.toUpperCase()) > -1
                            || item.lastName.toUpperCase().indexOf(value.toUpperCase()) > -1
                            || item.firstName.concat(" ").concat(item.lastName).toUpperCase().indexOf(value.toUpperCase()) > -1
                        );
                    });

                } else {
                    $scope.filteredSubUsers = $scope.subUsers;
                }
            };

            $scope.addSurrogateUser = function (customerObj) {
                customerObj.isSurrogateUser = !customerObj.isSurrogateUser;
                let filteredUers = _.filter($scope.surrogateUsers, function (user) {
                    return (+user.CUSTOMER_ID === +customerObj.userID);
                });

                if (filteredUers && filteredUers.length > 0) {
                    filteredUers[0].IS_VALID = (customerObj.isSurrogateUser ? 1 : 0);
                    filteredUers[0].comments = '';
                } else {
                    $scope.surrogateUsers.push({
                        VENDOR_ID: $scope.selectedSurrogateVendor.vendorID,
                        CUSTOMER_ID: customerObj.userID,
                        COMMENTS: $scope.surrogateObj.comments,
                        CUSTOMER_NAME: customerObj.firstName + ' ' + customerObj.lastName,
                        IS_VALID: 1
                    });
                }

                $scope.searchSurrogateString = '';
            };

            $scope.removeSurrogateUser = function (customerObj) {
                let filteredUers = _.filter($scope.surrogateUsers, function (user) {
                    return (user.CUSTOMER_ID === customerObj.CUSTOMER_ID);
                });

                if (filteredUers && filteredUers.length > 0) {
                    filteredUers[0].IS_VALID = 0;
                    filteredUers[0].comment = '';
                }

                var filteredSubUser = _.filter($scope.subUsers, function (subuser) {
                    return (+subuser.userID === +customerObj.CUSTOMER_ID);
                });

                if (filteredSubUser && filteredSubUser.length > 0) {
                    filteredSubUser[0].isSurrogateUser = false;
                }
            };

            $scope.getSurrogateUsers = function (vendor) {
                $scope.surrogateObj.comments = '';
                $scope.searchSurrogateString = '';

                $scope.surrogateUsers = [];
                if ($scope.subUsers && $scope.subUsers.length > 0) {
                    $scope.subUsers.forEach(function (user) {
                        user.isSurrogateUser = false;
                    });
                }

                resetSurrogateObj();
                $scope.surrogateObj.comments1 = '';
                $scope.selectedSurrogateVendor = vendor;
                let params = {
                    reqid: $scope.reqId,
                    vendorid: vendor.vendorID,
                    deptid: 0
                };

                PRMSurrogateBidService.GetSurrogates(params)
                    .then(function (response) {
                        if (response && response.length > 0 && $scope.subUsers && $scope.subUsers.length > 0) {
                            $scope.surrogateUsers = response;
                            $scope.surrogateUsers.forEach(function (surrogateuser) {
                                if (surrogateuser.COMMENTS) {
                                    $scope.surrogateObj.comments1 += surrogateuser.CUSTOMER_NAME + ': ' + surrogateuser.COMMENTS + '<br/>';
                                }

                                var filterResult = _.filter($scope.subUsers, function (subuser) {
                                    return (+subuser.userID === +surrogateuser.CUSTOMER_ID);
                                });

                                if (filterResult && filterResult.length > 0) {
                                    filterResult[0].isSurrogateUser = true;
                                }
                            });


                            $scope.searchSurrogate($scope.searchSurrogateString);
                        }
                    });
            };

            $scope.saveSurrogateUsers = function () {
                $scope.surrogateUsers.forEach(function (user, index) {
                    user.COMMENTS = user.COMMENTS ? user.COMMENTS : $scope.surrogateObj.comments;
                });
                $scope.surrogateObj.surrogates = $scope.surrogateUsers;
                PRMSurrogateBidService.SaveSurrogateUsers($scope.surrogateObj)
                    .then(function (response) {
                        if (response && !response.errorMessage) {
                            resetSurrogateObj();
                            //angular.element('#SurrogateUsersPopUp').modal('hide');
                            swal("Thanks !", "Successfully updated", "success");
                            $scope.getSurrogateUsers($scope.selectedSurrogateVendor);
                        } else {
                            swal("Error!", "Error saving data.", "error");
                        }
                    });
            };

            $scope.surrogateVendor = function (vendor) {

                //let surrogateURL = $state.href("surrogate-bid", { "Id": $scope.reqId, 'VendorId': vendor.vendorID, compId: $scope.currentUserCompID });
                //if ($scope.IS_CB_ENABLED) {
                //    surrogateURL = $state.href("surrogate-bid-cb", { "Id": $scope.reqId, 'VendorId': vendor.vendorID, compId: $scope.currentUserCompID });
                //}

                //window.open(surrogateURL, '_blank');

                let params = {
                    reqid: $scope.reqId,
                    vendorid: vendor.vendorID,
                    deptid: 0
                };

                PRMSurrogateBidService.GetSurrogates(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            var filterResult = _.filter(response, function (surrogateUser) {
                                return (surrogateUser.CUSTOMER_ID === $scope.currentUserId);
                            });

                            if (filterResult && filterResult.length > 0) {
                                let surrogateURL = $state.href("surrogate-bid", { "Id": $scope.reqId, 'VendorId': vendor.vendorID, compId: $scope.currentUserCompID });
                                if ($scope.IS_CB_ENABLED) {
                                    surrogateURL = $state.href("surrogate-bid-cb", { "Id": $scope.reqId, 'VendorId': vendor.vendorID, compId: $scope.currentUserCompID });
                                }

                                window.open(surrogateURL, '_blank');
                            } else {
                                swal("Error!", "Unauthorized to Surrogate current Vendor.", "error");
                            }
                        } else {
                            swal("Error!", "No Surrogate users assigned.", "error");
                        }
                    });
            };

            $scope.searchTable = function (str) {
                if ($scope.isCustomer) {
                    $scope.listRequirementItemsTemp = $scope.auctionItem.listRequirementItems.filter(function (item) {
                        return item.productIDorName.toUpperCase().indexOf(str.toUpperCase()) >= 0;
                    });
                } else {
                    $scope.listRequirementItemsTemp = $scope.auctionItemVendor.listRequirementItems.filter(function (item) {
                        return item.productIDorName.toUpperCase().indexOf(str.toUpperCase()) >= 0;
                    });
                }

                $scope.totalItems = $scope.listRequirementItemsTemp.length;
            };

            $scope.arrayLength = function (arr, count) {
                return arr.slice(0, count);
            };

            $scope.getRequirementSettings = function () {
                $scope.requirementSettings = [];
                $scope.formRequest.rankLevel = 'BOTH';
                $scope.selectedTemplate.TEMPLATE_NAME = 'PRM_DEFAULT';
                PRMForwardBidService.getRequirementSettings({ "reqid": $scope.reqId, "sessionid": $scope.currentSessionId })
                    .then(function (response) {
                        $scope.requirementSettings = response;
                        if ($scope.requirementSettings && $scope.requirementSettings.length > 0) {
                            var rankLevel = $scope.requirementSettings.filter(function (setting) {
                                return setting.REQ_SETTING === 'RANK_LEVEL';
                            });

                            if (rankLevel && rankLevel.length > 0) {
                                $scope.formRequest.rankLevel = rankLevel[0].REQ_SETTING_VALUE;
                            }


                            var template = $scope.requirementSettings.filter(function (setting) {
                                return setting.REQ_SETTING === 'TEMPLATE_ID';
                            });

                            if (template && template.length > 0) {
                                $scope.selectedTemplate.TEMPLATE_ID = template[0].REQ_SETTING_VALUE;
                            }
                        }

                        if ($scope.selectedTemplate.TEMPLATE_ID || $scope.selectedTemplate.TEMPLATE_NAME) {
                            $scope.GetPRMTemplateFields();
                        }
                    });
            };

            $scope.getRequirementSettings();

            $scope.saveRankLevelSetting = function () {
                $scope.tempScopeVariables.RankTypeSaving = true;
                $scope.rankLevelChanged = false;
                let param = {
                    requirementSetting: {
                        REQ_ID: $scope.reqId,
                        REQ_SETTING: 'RANK_LEVEL',
                        REQ_SETTING_VALUE: $scope.formRequest.rankLevel ? $scope.formRequest.rankLevel : 'BOTH',
                        USER: $scope.currentUserId,
                        sessionID: $scope.currentSessionId
                    }
                };

                PRMForwardBidService.saveRequirementSetting(param)
                    .then(function (response) {
                        $scope.tempScopeVariables.RankTypeSaving = false;
                        if (response && !response.errorMessage) {
                            swal("Thanks !", "Successfully updated", "success");
                        } else {
                            swal("Error!", "Error saving data.", "error");
                        }
                    });
            };

            $scope.displayItemLevelRank = function () {
                let itemLevel = $scope.formRequest.rankLevel && ($scope.formRequest.rankLevel === 'BOTH' || $scope.formRequest.rankLevel === 'ITEM');
                if (!itemLevel && $scope.anyItemQuotationsRejected()) {
                    itemLevel = true;
                }

                return itemLevel;
            };

            $scope.displayRequirementLevelRank = function () {
                let display = true;
                if ($scope.formRequest.rankLevel && $scope.formRequest.rankLevel === 'ITEM') {
                    display = false;
                }

                return display;
            };

            $scope.conertToCounterBid = function () {
                let param = {
                    reqId: $scope.reqId,
                    isCounterBid: $scope.IS_CB_ENABLED ? 0 : 1,
                    user: $scope.currentUserId,
                    sessionId: $scope.currentSessionId
                };

                PRMForwardBidService.toggleCounterBid(param)
                    .then(function (response) {
                        if (response && !response.errorMessage) {
                            swal("Thanks !", "Successfully updated", "success");
                            location.reload();
                        } else {
                            swal("Error!", response.errorMessage, "error");
                        }
                    });
            };

            $scope.GetPRMTemplateFields = function () {
                $scope.prmFieldMappingDetails = {};
                var params = {
                    "templateid": $scope.selectedTemplate.TEMPLATE_ID ? $scope.selectedTemplate.TEMPLATE_ID : 0,
                    "templatename": $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : '',
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        $scope.prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });
                });
            };

            $scope.isTaxAvailable = function (taxType) {
                var isVisible = true;

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0 &&
                    $scope.auctionItem.auctionVendors[0].selectedVendorCurrency !== 'INR') {
                    return false;
                }

                if ($scope.gstNumber) {
                    if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0 &&
                        $scope.auctionItem.auctionVendors[0].selectedVendorCurrency !== 'INR') {
                        isVisible = false;
                    } else if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0 &&
                        $scope.auctionItem.auctionVendors[0].selectedVendorCurrency === 'INR') {
                        if (!$scope.disableLocalGSTFeature) {
                            if ($scope.gstNumber && $scope.gstNumber.startsWith($scope.localGSTNumberCode) && taxType && taxType === 'IGST') {
                                isVisible = false;
                            } else if ($scope.gstNumber && $scope.gstNumber.startsWith($scope.localGSTNumberCode) && taxType && (taxType === 'CGST' || taxType === 'SGST')) {
                                isVisible = true;
                            } else if (taxType && (taxType === 'CGST' || taxType === 'SGST')) {
                                isVisible = false;
                            }
                        } else {
                            return true;
                        }
                    }
                }

                return isVisible;
            };

            $scope.refreshRequirementCurrency = function () {
                PRMForwardBidService.refreshRequirementCurrency({ reqid: $scope.reqId, sessionid: $scope.currentSessionId })
                    .then(function () {
                        swal("Thanks !", "Successfully Refreshed", "success");
                        location.reload();
                    });
            };

            $scope.getCurrencyRateStatus = function () {
                $scope.showCurrencyRefresh = false;
                PRMForwardBidService.validateCurrencyRate({ reqid: $scope.reqId, sessionid: $scope.currentSessionId })
                    .then(function (currencyValidation) {
                        if (currencyValidation.objectID) {
                            $scope.showCurrencyRefresh = true;
                        }
                    });
            };

            $scope.vendorsQuotationStatus = function () {
                let pendingQuotationVendors = [];
                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {

                    $scope.auctionItem.auctionVendors.forEach(function (vendor, index) {
                        let pendingQuotationItems = vendor.listRequirementItems.filter(function (reqItem) {
                            return reqItem.isItemQuotationRejected === -1 && reqItem.isCoreProductCategory;
                        });

                        if (pendingQuotationItems && pendingQuotationItems.length > 0) {
                            pendingQuotationVendors.push(vendor.vendorName);
                        }
                    });
                }

                return pendingQuotationVendors.join();
            };

            $scope.hasAnyPartiallyApprovedVendors = function () {
                let anyPartiallyApprovedVendors = false;
                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                    $scope.auctionItem.auctionVendors.forEach(function (vendor, index) {
                        if (vendor.isQuotationRejected === 0 && !anyPartiallyApprovedVendors) {
                            let rejectedQuotationItems = vendor.listRequirementItems.filter(function (reqItem) {
                                return reqItem.isItemQuotationRejected === 1 && reqItem.isCoreProductCategory;
                            });
                            if (rejectedQuotationItems && rejectedQuotationItems.length > 0) {
                                anyPartiallyApprovedVendors = true;
                            }
                        }
                    });
                }

                if (anyPartiallyApprovedVendors) {
                    $scope.formRequest.rankLevel = 'ITEM';
                }

                return anyPartiallyApprovedVendors;
            };

            $scope.hasAnyItemsApprovedVendors = function (vendor) {
                let anyPartiallyApprovedVendors = false;
                if (vendor && vendor.listRequirementItems.length > 0) {
                    if (vendor.isQuotationRejected === 0 && !anyPartiallyApprovedVendors) {
                        let notApprovedQuotationItems = vendor.listRequirementItems.filter(function (reqItem) {
                            return reqItem.isItemQuotationRejected === -1 && reqItem.isCoreProductCategory;
                        });

                        if (notApprovedQuotationItems && notApprovedQuotationItems.length > 0) {
                            anyPartiallyApprovedVendors = true;
                        }
                    }
                }

                return anyPartiallyApprovedVendors;
            };

            $scope.currencyChangeEvent = function () {
                //listRequirementItemsTemp
                if ($scope.auctionItem.auctionVendors[0].selectedVendorCurrency && $scope.auctionItem.auctionVendors[0].selectedVendorCurrency !== 'INR') {
                    console.log($scope.auctionItem.auctionVendors[0].selectedVendorCurrency);
                    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                        item.sGst = 0;
                        item.iGst = 0;
                        item.cGst = 0;
                        $scope.unitPriceCalculation('CGST');
                        $scope.unitPriceCalculation('SGST');
                        $scope.getTotalPrice($scope.discountAmount, $scope.freightCharges, $scope.totalprice,
                            $scope.packingCharges, $scope.packingChargesTaxPercentage, $scope.packingChargesWithTax,
                            $scope.installationCharges, $scope.installationChargesTaxPercentage, $scope.installationChargesWithTax,
                            $scope.freightChargesTaxPercentage, $scope.freightChargesWithTax);
                        $scope.getRevTotalPrice($scope.revfreightCharges, $scope.revtotalprice,
                            $scope.revpackingCharges, $scope.packingChargesTaxPercentage, $scope.revpackingChargesWithTax,
                            $scope.revinstallationCharges, $scope.installationChargesTaxPercentage, $scope.revinstallationChargesWithTax,
                            $scope.freightChargesTaxPercentage, $scope.revfreightChargesWithTax);
                    });
                }
            };

            $scope.gstSelect = function () {
                if ($scope.gstNumber) {
                    $scope.auctionItemVendor.listRequirementItems.forEach(function (vendorItem, itemIndexs) {
                        if (vendorItem.iGst) {
                            vendorItem.sGst = vendorItem.iGst / 2;
                            vendorItem.cGst = vendorItem.iGst / 2;
                            vendorItem.iGst = 0;
                        } else if (vendorItem.sGst && vendorItem.cGst) {
                            vendorItem.iGst = (+vendorItem.sGst + +vendorItem.cGst);
                            vendorItem.sGst = 0;
                            vendorItem.cGst = 0;
                        }



                        $scope.unitPriceCalculation('CGST');
                        $scope.unitPriceCalculation('SGST');
                        $scope.unitPriceCalculation('IGST');
                        $scope.getTotalPrice($scope.discountAmount, $scope.freightCharges, $scope.totalprice,
                            $scope.packingCharges, $scope.packingChargesTaxPercentage, $scope.packingChargesWithTax,
                            $scope.installationCharges, $scope.installationChargesTaxPercentage, $scope.installationChargesWithTax,
                            $scope.freightChargesTaxPercentage, $scope.freightChargesWithTax);
                        $scope.getRevTotalPrice($scope.revfreightCharges, $scope.revtotalprice,
                            $scope.revpackingCharges, $scope.packingChargesTaxPercentage, $scope.revpackingChargesWithTax,
                            $scope.revinstallationCharges, $scope.installationChargesTaxPercentage, $scope.revinstallationChargesWithTax,
                            $scope.freightChargesTaxPercentage, $scope.revfreightChargesWithTax);
                    });
                }
            };

            $scope.changeItemPercentage = function (perc) {
                $scope.overAllValue.overallPercentage = perc;
                $scope.EnableMakeaBid();
                if (perc > 0 && perc < $scope.NegotiationSettings.minReductionAmount) {
                    swal("Error!", "Overall increment should greater or equal to: " + $scope.NegotiationSettings.minReductionAmount + '%', "error");
                    return;
                }

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    item.revUnitPrice = item.revUnitPriceOld ? item.revUnitPriceOld : item.revUnitPrice;
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.HAS_PRICE === 1 && subItem.IS_VALID === 1) {
                                subItem.REV_BULK_PRICE = subItem.REV_BULK_PRICE_OLD ? subItem.REV_BULK_PRICE_OLD : subItem.REV_BULK_PRICE;
                            }
                        });
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.revUnitPrice && perc) {
                        item.revUnitPriceOld = item.revUnitPrice;
                        item.revUnitPrice = item.revUnitPrice + (item.revUnitPrice * perc / 100);
                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                if (subItem.HAS_PRICE === 1 && subItem.IS_VALID === 1) {
                                    subItem.REV_BULK_PRICE_OLD = subItem.REV_BULK_PRICE;
                                    subItem.REV_BULK_PRICE = +subItem.REV_BULK_PRICE + (+subItem.REV_BULK_PRICE * +perc / 100);
                                    //subItem.REV_UNIT_PRICE = subItem.REV_BULK_PRICE * subItem.REV_CONSUMPTION + ((subItem.REV_BULK_PRICE * subItem.REV_CONSUMPTION) * (subItem.TAX / 100));
                                    $scope.GetItemUnitPrice(false, subItemIndex);
                                }
                            });
                        }
                    }

                    $scope.unitPriceCalculation('PRC');
                    $scope.makeBidUnitPriceValidation(item.revUnitPrice, item.TemperoryRevUnitPrice, item.productIDorName);
                });
            };

            $scope.sendQuotationApprovalStatusEmail = function (vendor) {
                if ($scope.VendorItemlevelPrices && $scope.VendorItemlevelPrices.length > 0) {
                    var params = {
                        'items': [],
                        'vendorId': vendor.vendorID,
                        'reqId': $scope.auctionItem.requirementID,
                        'customerId': $scope.currentUserId,
                        'sessionId': $scope.currentSessionId
                    };

                    $scope.VendorItemlevelPrices.forEach(function (currentItem, index) {
                        if (currentItem.isItemQuotationRejected == false) {
                            currentItem.isItemQuotationRejected = 0;
                        } else if (currentItem.isItemQuotationRejected == true) {
                            currentItem.isItemQuotationRejected = 1;
                        }
                        var item = {
                            itemID: currentItem.itemID,
                            isItemQuotationRejected: currentItem.isItemQuotationRejected,
                            itemQuotationRejectedComment: currentItem.itemQuotationRejectedComment,
                            isCoreProductCategory: currentItem.isCoreProductCategory
                        };

                        params.items.push(item);
                    });

                    PRMForwardBidService.SendQuotationApprovalStatusEmail(params).then(function (req) {
                        if (!req.errorMessage) {
                            toastr.info('Successfully Sent.');
                        } else {
                            swal("Error!", req.errorMessage, "error");
                        }
                    });
                }
            };


            $scope.getReductionSettings = function () {
                $scope.reductionSettings = [];
                $scope.formRequest.reductionLevel = 'OVERALL';
                PRMForwardBidService.getReductionSettings({ "reqid": $scope.reqId, "sessionid": $scope.currentSessionId })
                    .then(function (response) {
                        $scope.reductionSettings = response;
                        if ($scope.reductionSettings && $scope.reductionSettings.length > 0) {
                            var reductionLevel = $scope.reductionSettings.filter(function (setting) {
                                return setting.REDUCTION_SETTING === 'REDUCTION_LEVEL';
                            });

                            if (reductionLevel && reductionLevel.length > 0) {
                                $scope.formRequest.reductionLevel = reductionLevel[0].REDUCTION_SETTING_VALUE;
                            }
                        }
                        $scope.displayOverAllReduction();
                    });
            };

            $scope.getReductionSettings();
            // $scope.displayOverAllReduction();

            $scope.saveReductionLevelSetting = function () {
                $scope.NegotiationSettingsValidationMessage = '';
                $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);
                if ($scope.NegotiationSettingsValidationMessage) {
                    $scope.Loding = false;
                    return;
                }

                if ($scope.formRequest.reductionLevel && $scope.formRequest.reductionLevel === 'OVERALL' &&
                    $scope.NegotiationSettings.minReductionAmount && +$scope.NegotiationSettings.minReductionAmount > 25) {
                    $scope.Loding = false;
                    swal("Warning!", "Overall incremental % cannot be more than 25%.", "error");
                    return;
                }

                var params = {};
                params.reqID = $scope.auctionItem.requirementID;
                params.sessionID = $scope.currentSessionId;
                $scope.NegotiationSettings.negotiationDuration = $scope.days + ':' + $scope.hours + ':' + $scope.mins + ':00.0';
                params.NegotiationSettings = $scope.NegotiationSettings;

                PRMForwardBidService.SaveReqNegSettings(params)
                    .then(function (response) {
                        if (response.SaveReqNegSettingsResult) {
                            if (response.SaveReqNegSettingsResult.errorMessage == '') {
                                swal("Done!", "Saved Successfully.", "success");
                            } else {
                                swal("Error", response.SaveReqNegSettingsResult.errorMessage, "inverse");
                            }
                        } else {
                            swal("Error", response.SaveReqNegSettingsResult.errorMessage, "inverse");
                        }

                    });

                $scope.tempScopeVariables.ReductionTypeSaving = true;
                $scope.reductionLevelChanged = false;
                let param = {
                    reductionSetting: {
                        REQ_ID: $scope.reqId,
                        REDUCTION_SETTING: 'REDUCTION_LEVEL',
                        REDUCTION_SETTING_VALUE: $scope.formRequest.reductionLevel ? $scope.formRequest.reductionLevel : 'OVERALL',
                        USER: $scope.currentUserId,
                        sessionID: $scope.currentSessionId
                    }
                };

                PRMForwardBidService.saveReductionLevelSetting(param)
                    .then(function (response) {
                        $scope.tempScopeVariables.ReductionTypeSaving = false;
                        if (response && !response.errorMessage) {
                            swal("Thanks !", "Successfully updated", "success");
                        } else {
                            swal("Error!", "Error saving data.", "error");
                        }
                    });
            };


            $scope.displayItemLevelReduction = function () {
                let reductionLevel = $scope.formRequest.reductionLevel && ($scope.formRequest.reductionLevel === 'ITEM');
                if (!reductionLevel) {
                    reductionLevel = true;
                }

                return reductionLevel;
            };

            $scope.displayOverAllReduction = function () {
                $scope.formRequest.display = true;
                if ($scope.formRequest.reductionLevel && $scope.formRequest.reductionLevel === 'ITEM') {
                    $scope.formRequest.display = false;
                }
                $scope.EnableMakeaBid();
                return $scope.formRequest.display;
            };

            $scope.revDiscountPrice = 0;
            $scope.updateRevisedUnitDiscount = function (perc) {
                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.revUnitDiscount > perc) {
                        swal("Error!", "Overall reduction should greater or equal to: " + item.revUnitDiscount, "error");
                        //item.revUnitDiscount = 0;
                        $scope.overAllValue.overallDiscountPercentage = item.revUnitDiscount;
                        return;
                    } else {
                        item.revUnitDiscount = perc;
                        $scope.revDiscountPrice = item.revUnitDiscount;
                        $scope.overAllValue.overallDiscountPercentage = perc;
                        $scope.mrpDiscountCalculation();
                        $scope.unitPriceCalculation();
                        $scope.getRevTotalPrice($scope.revfreightcharges, $scope.revtotalprice);
                        $scope.makeBidUnitPriceValidation(item.revUnitPrice, item.TemperoryRevUnitPrice, item.productIDorName);
                    }

                    $scope.EnableMakeaBid();
                });
            };

            $scope.EnableMakeaBid = function () {
                $scope.formRequest.MakeBiddisplay = false;
                if ($scope.formRequest.display == true && $scope.auctionItem.isDiscountQuotation == 0) {
                    if (+$scope.overAllValue.overallPercentage < $scope.NegotiationSettings.minReductionAmount) {
                        $scope.formRequest.MakeBiddisplay = true;
                    }
                }
                if ($scope.formRequest.display == true && $scope.auctionItem.isDiscountQuotation == 1) {
                    $scope.formRequest.MakeBiddisplay = false;
                }

                return $scope.formRequest.MakeBiddisplay;
            };

            $scope.EnableMakeaBid();

            $scope.enableWhenQuotationApproved = function () {

                var showField = true;

                if ($scope.auctionItem.biddingType === 'SPOT') {
                    if ($scope.auctionItem.minPrice > 0) {
                        if ($scope.auctionItem.status === 'STARTED' || $scope.auctionItem.status === 'Negotiation Ended') {
                            showField = true;
                        } else {
                            showField = false;
                        }
                    } else {
                        showField = false;
                    }

                } else {
                    if (!$scope.auctionItem.auctionVendors[0].isQuotationRejected == 0) {
                        showField = false;
                    }
                }
                return showField;
            };

            // The Below Function is for Tax fields when the RFQ is SPOT (This will work When the Negotiation is STARTED and when vendor doesn't 
            //submit the Quotation then Unit price calculation function isn't working because of STATUS so if the RFQ is SPOT then to calculate the Prices with QTY this will work)
            $scope.evaluateBiddingTypeBasedOnStatus = function () {
                var enableTax = false;
                if ($scope.auctionItem.biddingType === 'SPOT') {
                    if ($scope.auctionItem.status !== 'STARTED' || ($scope.auctionItem.status === 'STARTED' && $scope.auctionItem.auctionVendors[0].initialPrice <= 0)) {
                        enableTax = true;
                    }
                } else {
                    return $scope.auctionItem.status !== 'STARTED';
                }
                return enableTax;
            };


            $scope.showMessageBasedOnBiddingType = function (defaultMessage, type) {

                if (type === 'QUOTATION_UPLOAD') {
                    defaultMessage = $scope.auctionItem.biddingType === 'SPOT' ? "Your first bid is already submitted, you can revise the bid only during the auction time" : defaultMessage;
                } else if (type === 'INITIAL_QUOTE') {
                    defaultMessage = $scope.auctionItem.biddingType === 'SPOT' ? "" : defaultMessage;
                } else if (type === 'NEG_STARTED') {
                    defaultMessage = $scope.auctionItem.biddingType === 'SPOT' ? "" : defaultMessage;
                }

                return defaultMessage;
            };


            $scope.enableBasedOnBiddingType = function () {
                var display = true;
                if ($scope.auctionItem.biddingType === 'SPOT') {
                    display = false;
                } else {
                    display = true;
                }
                return display;//$scope.auctionItem.biddingType === 'SPOT' ? display = false : display
            };

            $scope.enableBasedOnBiddingTypeForSubItems = function (type, isRevFields) {

                var isDisabled = true;

                if (type && type.IS_CALCULATED == 0) {
                    if (isRevFields === 'INITIAL' && ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 && $scope.auctionItem.minPrice <= 0 || $scope.auctionItem.auctionVendors[0].isQuotationRejected == 1)) {
                        isDisabled = false;
                    } else if (isRevFields === 'REV' && ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 && $scope.auctionItem.minPrice > 0 || $scope.auctionItem.auctionVendors[0].isQuotationRejected == 1)) {
                        isDisabled = false;
                    }
                }

                return isDisabled;
            };

            $scope.getSubItemCeilingPrice = function (itemId) {
                let subItemCeilingPrice = 0;
                var requirementItem = $scope.auctionItem.listRequirementItems.filter(function (reqItem) {
                    return reqItem.itemID === itemId;
                });

                if (requirementItem && requirementItem.length > 0) {
                    if (requirementItem[0].productQuotationTemplateArray && requirementItem[0].productQuotationTemplateArray.length > 0) {
                        requirementItem[0].productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.CEILING_PRICE && +subItem.CEILING_PRICE > 0) {
                                subItemCeilingPrice = subItem.CEILING_PRICE;
                            }
                        });
                    }
                }

                return subItemCeilingPrice;


            };


            $scope.UploadQuotationForSelectVendorCeilingPrices = function (vendorID) {

                $scope.itemCeilingVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            subItem.dateCreated = "/Date(1561000200000+0530)/";
                            subItem.dateModified = "/Date(1561000200000+0530)/";
                        });
                        item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplateArray);
                    }
                    item.unitPrice = 0;
                });

                $scope.itemCeilingVendor.listRequirementItems = $scope.itemCeilingVendor.listRequirementItems = $scope.itemCeilingVendor.listRequirementItems.filter(function (item, index) {
                    return +item.ceilingPrice > 0 || (item.isCoreProductCategory === 0 && +item.ceilingPrice === 0);//PLEASE DONOT REMOVE THIS FILTER.  This Filter is because if the ceiling price is not given for any other items then also we are inserting that item into quotations so next time if we are saving the ceiling price for that item then it's not saving
                });

                var params = {
                    'quotationObject': $scope.itemCeilingVendor.listRequirementItems, // 1
                    'userID': vendorID,
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': $scope.currentSessionId,
                    'price': 0, // 5
                    'tax': 0,
                    'vendorBidPrice': 0,
                    'warranty': '',
                    'duration': '',
                    'payment': '', // 10
                    'gstNumber': '',
                    'validity': '',
                    'otherProperties': '',
                    'quotationType': '',
                    'revised': 0, // 15
                    'discountAmount': 0,
                    'listRequirementTaxes': [],
                    'quotationAttachment': null,
                    'multipleAttachments': [],
                    'TaxFiledValidation': 0,
                    'selectedVendorCurrency': '', //35
                    'isVendAckChecked': false,
                    'quotFreezeTime': ''
                };

                PRMForwardBidService.UploadQuotationForSelectVendorCeilingPrices(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Details have been saved successfully", "inverse");
                            location.reload();
                        }
                    });
            };


            $scope.sumCeilingPriceForSubItems = function () {

                $scope.itemCeilingVendor.listRequirementItems.forEach(function (reqItem, reqIndex) {
                    if (reqItem.productQuotationTemplateArray && reqItem.productQuotationTemplateArray.length > 0) {
                        reqItem.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.IS_CALCULATED == 0 && subItem.HAS_PRICE == 1) {
                                subItem.CEILING_PRICE1 = +subItem.CEILING_PRICE;
                            }
                        });
                        reqItem.ceilingPrice = _.sumBy(reqItem.productQuotationTemplateArray, 'CEILING_PRICE1');
                    }
                });

            };

            $scope.rankLevelChange = function () {
                $scope.rankLevelChanged = true;
            };

            $scope.reductionLevelChange = function () {
                $scope.reductionLevelChanged = true;
                $scope.getMinReductionAmount();
            };

            $scope.closeDeleteRequirementPopup = function () {
                angular.element('#deleteReqPopup').modal('hide');
            };


            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            }


            $scope.SaveVendorConfirmation = function (auctionItem) {

                if ($scope.auctionItem.auctionVendors[0].isVendAckChecked) {
                    $('#termsAndConditionsModal').modal('hide');
                    swal({
                        title: "Warning",
                        text: "You've acknowledged Successfully. Kindly click on Generate and Submit Button to Submit Quotation.",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    });
                } else {

                }

            }

            $scope.RemoveVendorConformation = function (auctionItem) {
                if (!$scope.auctionItem.auctionVendors[0].isVendAckApprove) {
                    $scope.auctionItem.auctionVendors[0].isVendAckChecked = false;
                } else {

                }

            }

        }]);prmApp
    .controller('fwdRemindersCtrl', ["$scope", "$state", "$stateParams", "userService", "auctionsService", "fileReader", "PRMForwardBidService",
        function ($scope, $state, $stateParams, userService, auctionsService, fileReader, PRMForwardBidService) {
            $scope.id = $stateParams.Id;
            $scope.formRequest = {};

            $scope.sendMessageTo = '';

            $scope.listSelectedVendors = [
            ];

            $scope.selectedVendors = {
                vendorName: '',
                vendorIDs: 0
            };

            $scope.vendorIDs = [];
            $scope.reminderMessage = '';

            $scope.reminderMessageValidation = false;
            $scope.reminderVendorsValidation = true;

            $scope.requestType = 'REMINDER';



            $scope.getData = function () {
                PRMForwardBidService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.auctionItem = response;
                        $scope.auctionItem.auctionVendors = $scope.auctionItem.auctionVendors.filter(function (el) { return el.companyName != "PRICE_CAP"; });
                    });
            };

            $scope.getData();

            $scope.getVendors = function () {
                $scope.reminderVendorsValidation = false;
                $scope.listSelectedVendors = [];
                $scope.vendorIDs = [];
                $scope.vendorCompanyNames = [];

                if ($scope.sendMessageTo == 'approved') {
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        $scope.selectedVendors = {};
                        if (item.quotationUrl != '' && item.isQuotationRejected == 0) {
                            $scope.vendorIDs.push(item.vendorID);
                            $scope.vendorCompanyNames.push(item.companyName);
                            $scope.selectedVendors.vendorID = item.vendorID;
                            $scope.selectedVendors.vendorName = item.companyName;
                            $scope.listSelectedVendors.push($scope.selectedVendors);
                        }
                    });
                }

                if ($scope.sendMessageTo == 'rejected') {
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        $scope.selectedVendors = {};
                        if (item.quotationUrl != '' && item.isQuotationRejected == 1) {
                            $scope.vendorIDs.push(item.vendorID);
                            $scope.vendorCompanyNames.push(item.companyName);
                            $scope.selectedVendors.vendorID = item.vendorID;
                            $scope.selectedVendors.vendorName = item.companyName;
                            $scope.listSelectedVendors.push($scope.selectedVendors);
                        }
                    });
                }

                if ($scope.sendMessageTo == 'specific') {
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        $scope.selectedVendors = {};
                        $scope.selectedVendors.vendorID = item.vendorID;
                        $scope.selectedVendors.vendorName = item.companyName;
                        $scope.listSelectedVendors.push($scope.selectedVendors);
                    });

                    $scope.vendorIDs = [];
                    $scope.vendorCompanyNames = [];
                }

                if ($scope.sendMessageTo == 'notuploaded') {
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        $scope.selectedVendors = {};
                        if (item.quotationUrl == '' || item.quotationUrl == 0 || item.quotationUrl == undefined) {
                            $scope.vendorIDs.push(item.vendorID);
                            $scope.vendorCompanyNames.push(item.companyName);
                            $scope.selectedVendors.vendorID = item.vendorID;
                            $scope.selectedVendors.vendorName = item.companyName;
                            $scope.listSelectedVendors.push($scope.selectedVendors);
                        }
                    });
                }

                if ($scope.sendMessageTo == 'all') {
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        $scope.selectedVendors = {};
                        $scope.vendorIDs.push(item.vendorID);
                        $scope.vendorCompanyNames.push(item.companyName);
                        $scope.selectedVendors.vendorID = item.vendorID;
                        $scope.selectedVendors.vendorName = item.companyName;
                        $scope.listSelectedVendors.push($scope.selectedVendors);
                    });
                }

                if ($scope.vendorIDs.length == 0) {
                    $scope.reminderVendorsValidation = true;
                }
            };


            $scope.selectVendor = function (vendorID, companyName) {
                $scope.reminderVendorsValidation = false;

                var index = $scope.vendorIDs.indexOf(vendorID);
                index = $scope.vendorCompanyNames.indexOf(companyName);

                if (index > -1) {
                    $scope.vendorIDs.splice(index, 1);
                    $scope.vendorCompanyNames.splice(index, 1);
                }
                else {
                    $scope.vendorIDs.push(vendorID);
                    $scope.vendorCompanyNames.push(companyName);
                }

                if ($scope.vendorIDs.length == 0) {
                    $scope.reminderVendorsValidation = true;
                }
            };


            $scope.vendorreminders = function () {
                $scope.reminderMessageValidation = false;
                $scope.reminderVendorsValidation = false;
                if ($scope.reminderMessage == '') {
                    $scope.reminderMessageValidation = true;
                }
                if ($scope.vendorIDs.length == 0) {
                    $scope.reminderVendorsValidation = true;
                }
                if ($scope.reminderMessageValidation || $scope.reminderVendorsValidation) {
                    return;
                }
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: userService.getUserId(),
                    sessionID: userService.getUserToken(),
                    message: $scope.reminderMessage,
                    vendorIDs: $scope.vendorIDs,
                    vendorCompanyNames: $scope.vendorCompanyNames,
                    requestType: $scope.requestType
                };

                PRMForwardBidService.vendorreminders(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            swal({
                                title: "Done!",
                                text: "Reminders sent Successfully.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    //$state.go('reminders', { 'Id': response.objectID });
                                    location.reload();
                                });
                        }
                        else {
                            swal("Error!", response.errorMessage, "error");
                        }
                    });
            };

            $scope.getHistory = function () {
                PRMForwardBidService.GetVendorReminders({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.ListReminders = response;
                        $scope.ListReminders.forEach(function (item, index) {
                            item.sentOn = userService.toLocalDate(item.sentOn);
                        });
                    });
            };

            $scope.getHistory();

            $scope.reminderMessage = '';
            //$scope.reminderMessage = "We are using an online tool -PRM360 for our procurement process (Enquiry, Receiving Quotes, Negotiations and releasing POs. Just now we have floated an enquiry request you to please use the link you received in your mailbox and on Mobile for sending your offers." + "\n" + "Initially, Ms. Megha Mehta and Ms. Hannah  will help and train you in using the tool." + "\n" + "Request you to please coordinate with them. You may reach them at" + "\n" + "Email: megha@prm360.com ; hannah@prm360.com" + "\n" + "Megha   :  9989389385" + "\n" + "Hannah :  9160258358"

            $scope.bindHtml = function (val) {
                $scope.bindreminderMessage = val.replace(/\u000a/g, "</br>");
            };


        }]);﻿prmApp
    .controller('fwdReportCtrl', ["$scope", "$http", "$state", "$window", "$timeout", "domain", "$filter", "$stateParams", "$log", "PRMForwardBidService", "userService",
        function ($scope, $http, $state, $window, $timeout, domain, $filter, $stateParams, $log, PRMForwardBidService, userService) {

            //two calls are going wrong Service (getreqreportforexcel, requirementsettings) not sure where they are called

            $scope.deliveryDateData = [];
            $scope.reqDetails = [];
            $scope.reqId = $stateParams.reqID;
            $scope.userId = userService.getUserId();

            $scope.liveBiddingChart = {
                credits: {
                    enabled: false
                },
                options: {
                    chart: {
                        type: 'line'
                    }
                },
                series: [],
                title: {
                    text: 'Bidding Report'
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                }
            };

            $scope.categories = [];
            $scope.shouldAdd = true;
            $scope.getLiveBiddingReport = function () {
                PRMForwardBidService.getLiveBiddingReport($stateParams.reqID)
                    .then(function (response) {
                        $scope.liveBiddingData = response;
                        $scope.liveBiddingData.forEach(function (item, index) {
                            var data = [];
                            item.arrayKeyValue.forEach(function (val, ind) {
                                if ($scope.shouldAdd) {
                                    val.keyDateTime = userService.toLocalDate(val.keyDateTime);
                                    $scope.categories.push(val.keyDateTime);
                                }
                                data.push(val.decimalVal);
                            });

                            $scope.shouldAdd = false;
                            $scope.liveBiddingChart.series.push({
                                name: item.vendor.firstName + " " + item.vendor.lastName,
                                data: data
                            });
                        });
                        $scope.liveBiddingChart.xAxis = {
                            title: {
                                text: 'Time'
                            },
                            categories: $scope.categories
                        };

                        $scope.liveBiddingChart.yAxis = {
                            title: {
                                text: 'Negotiation Price'
                            }
                        };

                        $scope.show = true;
                    });
            };

            $scope.getLiveBiddingReport();

            $scope.GetReqDetails = function () {
                PRMForwardBidService.GetReqDetails($stateParams.reqID)
                    .then(function (response) {
                        $scope.reqDetails = response;

                        $scope.reqDetails.postedOn = userService.toLocalDate($scope.reqDetails.postedOn);
                        $scope.reqDetails.startTime = userService.toLocalDate($scope.reqDetails.startTime);
                        $scope.reqDetails.endTime = userService.toLocalDate($scope.reqDetails.endTime);

                        $log.info($scope.reqDetails);
                    });
            };

            $scope.GetReqDetails();

            $scope.getItemWiseReport = function () {
                PRMForwardBidService.getItemWiseReport($stateParams.reqID)
                    .then(function (response) {
                        $scope.itemWiseData = response;
                    });
            };

            $scope.getItemWiseReport();

            $scope.getDeliveryDateReport = function () {
                PRMForwardBidService.getDeliveryDateReport($stateParams.reqID)
                    .then(function (response) {
                        $scope.deliveryDateData = response;

                    });
            };

            $scope.getDeliveryDateReport();

            $scope.getPaymentTermsReport = function () {
                PRMForwardBidService.getPaymentTermsReport($stateParams.reqID)
                    .then(function (response) {
                        $scope.paymentTermsData = response;
                    });
            };

            $scope.getPaymentTermsReport();

            $scope.converToDate = function (date) {
                var fDate = userService.toLocalDate(date);

                if (fDate.indexOf('-9999') > -1) {
                    fDate = "-";
                }

                return fDate;
            };

            $scope.printReport = function () {
                $scope.doPrint = true;
                $timeout(function () {
                    $window.print();
                    $scope.doPrint = false;
                }, 1000);
            };

            $scope.getCustomerData = function (userId) {
                PRMForwardBidService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userId })
                    .then(function (response) {
                        $scope.auctionItem = response;
                        $scope.auctionItem.auctionVendors = _.filter($scope.auctionItem.auctionVendors, function (x) { return x.companyName !== 'PRICE_CAP'; });
                    });
            };

            $scope.getVendorData = function (item) {
                PRMForwardBidService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': item.vendorID })
                    .then(function (response) {
                        $scope.auctionItemVendor = response;
                        item.auctionItemVendor = $scope.auctionItemVendor;
                    });
            };

            $scope.getRequirementData = function () {
                $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                    if (item.vendorID > 0) {
                        item.auctionItemVendor = [];
                        $scope.getVendorData(item);
                    }
                });
            };

            $scope.getCustomerData($scope.userId);


        }]);
prmApp
    .controller('fwdReqTechSupportCtrl', ["$scope", "$state", "$stateParams", "$log", "userService", "auctionsService", "fileReader", "growlService", "PRMForwardBidService",
        function ($scope, $state, $stateParams, $log, userService, auctionsService, fileReader, growlService, PRMForwardBidService) {


            $scope.message = 'Team, We have posted the following requirement in the system. Please train the vendors and get the quotations on time.'

            PRMForwardBidService.getrequirementdata({ "reqid": $stateParams.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.auctionItem = response;
                    $scope.reqShare = {
                        requirement: $scope.auctionItem,
                        message: $scope.message + ' . This is related to forward bidding',
                        expectedBidding: '',
                        minReductionAmount: $scope.auctionItem.minBidAmount,
                        vendorRankComparision: $scope.auctionItem.minVendorComparision,
                        negotiationDuration: $scope.auctionItem.negotiationDuration,
                        quotationFreezTime: $scope.auctionItem.quotationFreezTime
                    };

                    $scope.quotationFreezTime = userService.toLocalDate($scope.auctionItem.quotationFreezTime);
                });



            $scope.reqTechSupport = function () {

                $scope.reqShare.sessionID = userService.getUserToken();

                var params = {
                    reqShare: $scope.reqShare
                };

                auctionsService.reqTechSupport(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Requirement Details have been sent Successfully to PRM360 Tech Support Team.", "success");
                        }
                    });
            };
        }]);﻿
prmApp
    .controller('fwdRequirementsCtrl', ["$state", "$window", "$scope", "userService", "auctionsService", "$http", "domain", "PRMForwardBidService", "$stateParams",
        function ($state, $window, $scope, userService, auctionsService, $http, domain, PRMForwardBidService, $stateParams) {
            /*pagination code*/
            $scope.totalItems = 0;
            $scope.totalVendors = 0;
            $scope.totalSubuser = 0;
            $scope.totalInactiveVendors = 0;
            $scope.totalLeads = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.PageSize = 10;
            $scope.itemsPerPage = 10;
            $scope.itemsPerPage2 = 8;
            $scope.maxSize = 8;
            $scope.myAuctions = [];
            $scope.categories = [];
            $scope.reqStatus = 'ALL';
            $scope.biddingType = 'ALL';
            $scope.category = 'ALL CATEGORIES';
            $scope.buyerFilter = 'ALL';
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.compId = userService.getUserCompanyId();
            $scope.searchKeyword = '';

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };

            /* CLIENT STATUS MAPPING TO PRM STATUS */

            // Clickable dashboard //
            $scope.NavigationFilters = $stateParams.filters;
            // Clickable dashboard //


            $scope.setPage = function (pageNo) {
                $scope.NavigationFilters = {};
                $scope.currentPage = pageNo;
                $scope.getAuctions(($scope.currentPage - 1), 10, $scope.searchKeyword);

            };

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };

            $scope.pageChanged = function () {
                //$scope.getAuctions();
            };

            $scope.filterDate = {
                reqToDate: '',
                reqFromDate: ''
            };

            $scope.filterDate.reqToDate = moment().format('YYYY-MM-DD');
            $scope.filterDate.reqFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            $scope.buyers = ['ALL'];
            $scope.getAuctions = function (recordsFetchFrom, pageSize, searchKeyword) {
                if (!$scope.NavigationFilters) {
                    $scope.NavigationFilters = {};
                }

                PRMForwardBidService.getmyAuctions({ "userid": userService.getUserId(), "fromDate": $scope.filterDate.reqFromDate, "toDate": $scope.filterDate.reqToDate, "status": $scope.reqStatus, "search": searchKeyword ? searchKeyword : '', "allBuyer": $scope.buyerFilter, "page": recordsFetchFrom * pageSize, "pagesize": pageSize, "sessionid": userService.getUserToken(), "onlyrfq": 0, "onlyrfp": 0 })
                    .then(function (response) {
                        $scope.myAuctions1 = [];
                        $scope.forReqFilter = [];
                        $scope.myAuctions1 = response;
                        $scope.forReqFilter = angular.copy(response);
                        $scope.myAuctions1 = $scope.myAuctions1.filter(function (auction) {
                            return (auction.biddingType !== 'TENDER');
                        });
                        $scope.myAuctions1.forEach(function (item, index) {
                            item.postedOn = $scope.GetDateconverted(item.postedOn);
                            item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                            item.expStartTime = $scope.GetDateconverted(item.expStartTime);
                            item.startTime = $scope.GetDateconverted(item.startTime);

                            if (String(item.startTime).includes('9999')) {
                                item.startTime = '';
                            }

                            if (item.status === 'UNCONFIRMED') {
                                item.statusColor = 'text-warning';
                            }
                            else if (item.status === 'NOT STARTED') {
                                item.statusColor = 'text-warning';
                            }
                            else if (item.status === 'STARTED') {
                                item.statusColor = 'text-danger';
                            }
                            else if (item.status === 'Negotiation Ended') {
                                item.statusColor = 'text-success';
                            }
                            else if (item.status === 'Qcs Pending') {
                                item.statusColor = 'text-danger';
                            }
                            else if (item.status === 'Qcs Created') {
                                item.statusColor = 'text-danger';
                            }
                            else if (item.status === 'Qcs Approval Pending') {
                                item.statusColor = 'text-danger';
                            }
                            else if (item.status === 'Qcs Approved') {
                                item.statusColor = 'text-success';
                            }
                            else if (item.status === 'Quotations Received') {
                                item.statusColor = 'text-success';
                            } else if (item.status === 'Closed') {
                                item.statusColor = 'text-success';
                            }
                            else {
                                item.statusColor = '';
                            }


                            if (item.isRFP) {
                                item.rfpStatusColor = 'orangered';
                            } else {
                                item.rfpStatusColor = 'darkgreen';
                            }
                        });

                        $scope.myAuctions = $scope.myAuctions1;
                        $scope.myAuctions2 = response;

                        if ($scope.myAuctions.length > 0) {
                            $scope.myAuctionsLoaded = true;
                            $scope.totalItems = $scope.myAuctions[0].totalCount;
                        } else {
                            $scope.myAuctionsLoaded = false;
                            $scope.totalItems = 0;
                            $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                        }



                        $scope.tempStatusFilter = angular.copy($scope.myAuctions);
                        $scope.tempCategoryFilter = angular.copy($scope.myAuctions);


                        // Clickable dashboard //

                        if ($scope.NavigationFilters && $scope.NavigationFilters.status) {
                            $scope.getStatusFilter($scope.NavigationFilters.status);
                            $scope.reqStatus = $scope.NavigationFilters.status;
                        }

                        // Clickable dashboard //

                    });
            };

            //$scope.getAuctions(0, 10, $scope.searchKeyword);

            $scope.GetRequirementsReport = function () {
                PRMForwardBidService.getrequirementsreport({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.reqReport = response;
                        alasql.fn.handleDate = function (date) {
                            return new moment(date).format("MM/DD/YYYY");
                        }
                        alasql('SELECT requirementID as RequirementID, title as RequirementTitle, handleDate(postedDate) as [PostedDate], status as Status, vendorID as VendorID, companyName as CompanyName, userPhone as Phone,userEmail as Email, itemID as [ItemID], productBrand as Brand, productQuantity as Quantity, productQuantityIn as [Units], costPrice as [InitialCostPrice], revCostPrice as [FinalCostPrice], netPrice as [InitialNetPrice], marginAmount as [InitialMarginAmount], unitDiscount as [MarginPercentage],revUnitDiscount as [RevisedMarginPercentage],unitMRP as [MRP],gst as [GST],savings as [Savings], handleDate(startTime) as [StartTime], status as [Status], othersBrands as [ManufacturerName] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xlsx", $scope.reqReport]);
                    });

                //alasql('SELECT requirementID as [RequirementID],title as [Title],vendorID as [VendorID],companyName as [Company],userPhone as [Phone],userEmail as [Email],itemID as [ItemID],productBrand as [Brand],productQuantity as [Quantity], productQuantityIn as [Units],costPrice as [InitialCostPrice],revCostPrice as [FinalCostPrice],netPrice as [NetPrice],marginAmount as [InitialMarginAmount],unitDiscount as [InitialMargin],revUnitDiscount as [FinalMargin],unitMRP as [MRP],gst as [GST],maxInitMargin as [MaximumInitialMargin],maxFinalMargin as [MaximumFinalMargin],savings as [Savings] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xslx", $scope.reqReport]);

            };

            $scope.goToReqReport = function (reqID) {
                //$state.go("reports", { "reqID": reqID });

                var url = $state.href('reports', { "reqID": reqID });
                $window.open(url, '_blank');

            };

            $scope.cloneRequirement = function (requirement) {
                let tempObj = {};
                let stateView = 'fwd-save-requirement';
                if (requirement.biddingType && requirement.biddingType === 'TENDER') {
                    stateView = 'save-tender';
                }

                tempObj.cloneId = requirement.requirementID;
                $state.go(stateView, { 'Id': requirement.requirementID, reqObj: tempObj });
            };

            $scope.chatRequirement = function (requirement) {
                var url = $state.href('req-chat', { "reqId": requirement.requirementID });
                $window.open(url, '_blank');
            };

            $scope.searchTable = function (str) {
                // RFQ Date Based Filter // 

                str = str.toLowerCase();
                $scope.category = 'ALL CATEGORIES';
                $scope.reqStatus = 'ALL';
                $scope.myAuctions = $scope.myAuctions1.filter(function (req) {
                    return (String(req.requirementID).includes(str)
                        || String(req.title.toLowerCase()).includes(str)
                        || String(req.userName.toLowerCase()).includes(str)
                        || String(req.projectName.toLowerCase()).includes(str)
                        || String(req.projectDetails.toLowerCase()).includes(str)
                        || String(req.postedOn).includes(str)
                        || req.category[0].toLowerCase().includes(str)
                        || (req.prNumbers ? req.prNumbers : '').includes(str)

                    );

                });

                // RFQ Date Based Filter //

                $scope.totalItems = $scope.myAuctions.length;
            };


            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));

                            $scope.categories.push('ALL CATEGORIES');

                            //categories.splice(categories.indexOf("ALL"), 1);
                            //categories.unshift('ALL');

                            $scope.categories = $scope.categories.filter(item => item !== "ALL CATEGORIES" && item !== "");
                            $scope.categories.unshift("ALL CATEGORIES");

                            //console.log($scope.categories);

                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    }
                }, function (result) {
                });

            };


            //  $scope.getCategories();

            $scope.getStatusFilter = function (filterVal, str) {
                $scope.myAuctions = $scope.myAuctions1;
                $scope.filterArray = [];
                if (filterVal !== 'ALL') {
                    $scope.filterArray = $scope.myAuctions.filter(function (item) {
                        return $scope.prmStatus($scope.isCustomer, item.status.toLowerCase()) === $scope.prmStatus($scope.isCustomer, filterVal.toLowerCase());
                    });

                    $scope.myAuctions = $scope.filterArray;
                }
                if ($scope.buyerFilter !== 'ALL') {
                    $scope.filterArray = $scope.myAuctions.filter(function (item) {
                        return item.userName === $scope.buyerFilter;
                    });

                    $scope.myAuctions = $scope.filterArray;
                }

                if (str) {

                    $scope.myAuctions = $scope.myAuctions.filter(function (req) {
                        return (String(req.requirementID).includes(str.toLowerCase())
                            || String(req.title.toLowerCase()).includes(str.toLowerCase())
                            || String(req.userName.toLowerCase()).includes(str.toLowerCase())
                            || String(req.projectName.toLowerCase()).includes(str.toLowerCase())
                            || String(req.projectDetails.toLowerCase()).includes(str.toLowerCase())
                            || String(req.postedOn).includes(str.toLowerCase())
                            || req.category[0].toLowerCase().includes(str.toLowerCase())
                            || (req.prNumbers ? req.prNumbers : '').includes(str.toLowerCase())
                            || (req.requirementNumber ? req.requirementNumber.toLowerCase() : '').includes(str.toLowerCase())

                        );

                    });
                    //$scope.myAuctions = $scope.filterArray;
                }

                $scope.totalItems = $scope.myAuctions.length;

            };

            $scope.getCategoryFilter = function (filterCategoryVal) {
                $scope.filterCatArray = [];
                if (filterCategoryVal === 'ALL CATEGORIES') {
                    $scope.myAuctions = $scope.myAuctions1;
                } else {
                    $scope.filterCatArray = $scope.tempCategoryFilter.filter(function (item) {
                        return item.category[0] === filterCategoryVal;
                    });
                    $scope.myAuctions = $scope.filterCatArray;

                }


            };


            $scope.GetFilteredReqs = function (fromDate, toDate, text) {
                $scope.filterReqArray = [];

                if (text === 'CLEAR') {
                    $scope.myAuctions = $scope.myAuctions1;
                } else {
                    $scope.forReqFilter.filter(function (req) {
                        req.postedOn = $scope.GetDateconverted(req.postedOn);
                        req.quotationFreezTime = $scope.GetDateconverted(req.quotationFreezTime);
                        req.expStartTime = $scope.GetDateconverted(req.expStartTime);
                        req.startTime = $scope.GetDateconverted(req.startTime);
                        var from = Date.parse(fromDate);
                        var to = Date.parse(toDate);
                        var check = Date.parse(req.postedOn);


                        if (fromDate <= req.postedOn && req.postedOn <= toDate) {
                            $scope.filterReqArray.push(req);
                        }

                    });
                    $scope.myAuctions = $scope.filterReqArray;
                }
                $scope.totalItems = $scope.myAuctions.length;
            };

            $scope.loadBuyers = function () {
                $scope.buyers = [];
                $scope.buyers.push('ALL');
                $http({
                    method: 'GET',
                    url: domain + 'getPostedRFQUsers?compid=' + +$scope.compId + '&sessionid=' + userService.getUserToken() + '&fromDate=' + $scope.filterDate.reqFromDate + '&toDate=' + $scope.filterDate.reqToDate,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        //if (response.data.length > 0) {
                        //    $scope.categories.push('ALL CATEGORIES');
                        //}
                        if (response.data && response.data.length > 0) {
                            response.data.forEach(function (item) {
                                $scope.buyers.push(item);
                            });
                        }

                        $scope.getAuctions(0, 10, $scope.searchKeyword);
                    }
                }, function (result) {

                });

            };

            $scope.loadBuyers();


        }]);﻿prmApp
    .controller('fwdSavingsCtrl', ["$scope", "$state", "$stateParams", "userService", "PRMCustomFieldService", "PRMForwardBidService",
        function ($scope, $state, $stateParams, userService, PRMCustomFieldService, PRMForwardBidService) {
            $scope.id = $stateParams.Id;
            $scope.totalItemsMinPrice = 0;
            $scope.totalLeastBidderPrice = 0;
            $scope.totalInitialPrice = 0;
            $scope.sessionid = userService.getUserToken();
            $scope.requirementSettings = [];
            $scope.selectedTemplate = {};
            $scope.prmFieldMappingDetails = {};

            $scope.getData = function () {
                PRMForwardBidService.getpricecomparison({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.auctionItem = response.requirement;
                        $scope.priceCompObj = response;

                        $scope.totalItemsL1Price = 0;
                        $scope.totalItemsMinimunPrice = 0;
                        $scope.negotiationSavings = 0;
                        $scope.savingsByLeastBidder = 0;
                        $scope.savingsByItemMinPrice = 0;

                        $scope.L1CompanyName = '';
                        $scope.L1CompanyName = $scope.auctionItem.auctionVendors[0].companyName;


                        for (var i = 0; i < $scope.priceCompObj.priceCompareObject.length; i++) {
                            $scope.totalItemsL1Price += $scope.priceCompObj.priceCompareObject[i].leastBidderPrice;
                            $scope.totalItemsMinimunPrice += $scope.priceCompObj.priceCompareObject[i].minPrice;
                        }

                        $scope.negotiationSavings = $scope.auctionItem.savings;
                        $scope.savingsByLeastBidder = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsL1Price;
                        $scope.savingsByItemMinPrice = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsMinimunPrice;

                        $scope.additionalSavings = 0;

                        $scope.additionalSavings = $scope.totalItemsL1Price - $scope.totalItemsMinimunPrice;

                    });
            };


            $scope.isReportGenerated = 0;

            $scope.GetReportsRequirement = function () {
                PRMForwardBidService.GetReportsRequirement({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.reports = response;
                        $scope.getData();
                        $scope.isReportGenerated = 1;
                    });
            };

            $scope.getData();

            $scope.getRequirementSettings = function () {
                $scope.requirementSettings = [];
                $scope.selectedTemplate.TEMPLATE_NAME = 'PRM_DEFAULT';
                PRMForwardBidService.getRequirementSettings({ "reqid": $scope.id, "sessionid": $scope.sessionid })
                    .then(function (response) {
                        $scope.requirementSettings = response;
                        if ($scope.requirementSettings && $scope.requirementSettings.length > 0) {
                            var template = $scope.requirementSettings.filter(function (setting) {
                                return setting.REQ_SETTING === 'TEMPLATE_ID';
                            });

                            if (template && template.length > 0) {
                                $scope.selectedTemplate.TEMPLATE_ID = template[0].REQ_SETTING_VALUE;
                            }
                        }

                        if ($scope.selectedTemplate.TEMPLATE_ID || $scope.selectedTemplate.TEMPLATE_NAME) {
                            $scope.GetPRMTemplateFields();
                        }
                    });
            };

            $scope.getRequirementSettings();

            $scope.GetPRMTemplateFields = function () {
                $scope.prmFieldMappingDetails = {};
                var params = {
                    "templateid": $scope.selectedTemplate.TEMPLATE_ID ? $scope.selectedTemplate.TEMPLATE_ID : 0,
                    "templatename": $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : '',
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        $scope.prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });
                });
            };

        }]);﻿prmApp
    .controller('fwdSavingsPreNegotiationCtrl', ["$scope", "$state", "$stateParams", "$window", "$timeout", "userService",
        "PRMCustomFieldService", "PRMForwardBidService",
        function ($scope, $state, $stateParams, $window, $timeout, userService, PRMCustomFieldService, PRMForwardBidService) {
            $scope.id = $stateParams.Id;
            $scope.totalItemsMinPrice = 0;
            $scope.totalLeastBidderPrice = 0;
            $scope.totalInitialPrice = 0;
            $scope.sessionid = userService.getUserToken();
            $scope.requirementSettings = [];
            $scope.selectedTemplate = {};
            $scope.prmFieldMappingDetails = {};

            $scope.getData = function () {
                PRMForwardBidService.GetPriceComparisonPreNegotiation({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.auctionItem = response.requirement;
                        $scope.priceCompObj = response;
                        $scope.totalItemsL1Price = 0;
                        $scope.totalItemsMinimunPrice = 0;
                        $scope.negotiationSavings = 0;
                        $scope.savingsByLeastBidder = 0;
                        $scope.savingsByItemMinPrice = 0;

                        $scope.L1CompanyName = '';
                        $scope.L1CompanyName = $scope.auctionItem.auctionVendors[0].companyName;

                        if ($scope.auctionItem.auctionVendors[0].companyName == "PRICE_CAP") {
                            $scope.L1CompanyName = $scope.auctionItem.auctionVendors[1].companyName;
                        }

                        $scope.priceCompObj.priceCompareObject.forEach(function (priceObj, vI) {
                            let factorTemp = _.filter($scope.auctionItem.auctionVendors, function (vendor) {
                                return Number(vendor.vendorID) === Number(priceObj.minPriceBidder);
                            });

                            priceObj.vendorCurrencyFactor = factorTemp[0].vendorCurrencyFactor;
                            $scope.totalItemsL1Price += priceObj.leastBidderPrice * factorTemp[0].vendorCurrencyFactor;
                            $scope.totalItemsMinimunPrice += priceObj.minPrice * factorTemp[0].vendorCurrencyFactor;
                        });

                        $scope.negotiationSavings = $scope.auctionItem.savings;
                        $scope.savingsByLeastBidder = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsL1Price;
                        $scope.savingsByItemMinPrice = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsMinimunPrice;

                        $scope.additionalSavings = 0;

                        $scope.additionalSavings = $scope.totalItemsL1Price - $scope.totalItemsMinimunPrice;

                    });
            };

            $scope.isReportGenerated = 0;

            $scope.GetReportsRequirement = function () {
                PRMForwardBidService.GetReportsRequirement({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.reports = response;
                        $scope.getData();
                        $scope.isReportGenerated = 1;
                    });
            };

            $scope.getData();

            $scope.doPrint = false;
            $scope.printReport = function () {
                $scope.priceCompObj.priceCompareObject.forEach(function (item, index) {
                    item.expanded = true;
                });

                $scope.doPrint = true;
                $timeout(function () {
                    $window.print();
                    $scope.doPrint = false;
                    $scope.priceCompObj.priceCompareObject.forEach(function (item, index) {
                        item.expanded = false;
                    });

                }, 1000);
            };

            $scope.getRequirementSettings = function () {
                $scope.requirementSettings = [];
                $scope.selectedTemplate.TEMPLATE_NAME = 'PRM_DEFAULT';
                PRMForwardBidService.getRequirementSettings({ "reqid": $scope.id, "sessionid": $scope.sessionid })
                    .then(function (response) {
                        $scope.requirementSettings = response;
                        if ($scope.requirementSettings && $scope.requirementSettings.length > 0) {
                            var template = $scope.requirementSettings.filter(function (setting) {
                                return setting.REQ_SETTING === 'TEMPLATE_ID';
                            });

                            if (template && template.length > 0) {
                                $scope.selectedTemplate.TEMPLATE_ID = template[0].REQ_SETTING_VALUE;
                            }
                        }

                        if ($scope.selectedTemplate.TEMPLATE_ID || $scope.selectedTemplate.TEMPLATE_NAME) {
                            $scope.GetPRMTemplateFields();
                        }
                    });
            };

            $scope.getRequirementSettings();

            $scope.GetPRMTemplateFields = function () {
                $scope.prmFieldMappingDetails = {};
                var params = {
                    "templateid": $scope.selectedTemplate.TEMPLATE_ID ? $scope.selectedTemplate.TEMPLATE_ID : 0,
                    "templatename": $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : '',
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        $scope.prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });
                });
            };
        }]);﻿
prmApp
    .controller('fwdVendorLeadsCtrl', function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, PRMForwardBidService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {

        var loginUserData = userService.getUserObj();

        $scope.isOTPVerified = loginUserData.isOTPVerified;
        $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
        $scope.formRequest = {};
        $scope.formRequest.isForwardBidding = false;
        $scope.myActiveLeads = [];
        $scope.currentPage = 1;
        $scope.currentPage2 = 1;
        $scope.itemsPerPage = 10;
        $scope.itemsPerPage2 = 10;
        $scope.maxSize = 10;
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        ///* CLIENT STATUS MAPPING TO PRM STATUS */
        $scope.isCustomer = userService.getUserType();
        $scope.prmStatus = function (type, status) {
            return userService.NegotiationStatus(type, status);
        };
        /* CLIENT STATUS MAPPING TO PRM STATUS */

        $scope.pageChanged = function () {
            //$scope.getLeads();
        };
        $scope.quotationAttachment = null;

        $scope.GetDateconverted = function (dateBefore) {
            if (dateBefore) {
                return userService.toLocalDate(dateBefore);
            }
        };

        $scope.maxSize = 10;
        $scope.totalLeads = 0;
        $scope.itemsPerPage = 10;
        $scope.myAuctionsLoaded = false;
        $scope.myAuctions = [];
        $scope.myActiveLeads = [];
        $scope.myAuctionsMessage = '';

        $scope.getAuctions = function () {
            $log.info($scope.formRequest.isForwardBidding);
            if (!$scope.formRequest.isForwardBidding) {
                PRMForwardBidService.getmyAuctions({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.myAuctions = response;
                        if ($scope.myAuctions.length > 0) {
                            $scope.myAuctionsLoaded = true;
                            $scope.totalItems = $scope.myAuctions.length;
                        } else {
                            $scope.myAuctionsLoaded = false;
                            $scope.totalItems = 0;
                            $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                        }
                    });
            }
        };

        $scope.GetRequirementsReport = function () {

            PRMForwardBidService.getrequirementsreport({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.reqReport = response;

                    //alasql('SELECT requirementID as [RequirementID],title as [Title],vendorID as [VendorID],companyName as [Company],userPhone as [Phone],userEmail as [SEmail],itemID as [ItemID],productBrand as [Brand],productQuantity as [Quantity], productQuantityIn as [Units],costPrice as [InitialCostPrice],revCostPrice as [FinalCostPrice],netPrice as [NetPrice],marginAmount as [InitialMarginAmount],unitDiscount as [InitialMargin],revUnitDiscount as [FinalMargin],unitMRP as [MRP],gst as [GST],maxInitMargin as [MaximumInitialMargin],maxFinalMargin as [MaximumFinalMargin],savings as [Savings] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xslx", $scope.reqReport]);
                    alasql.fn.handleDate = function (date) {
                        return new moment(date).format("MM/DD/YYYY");
                    };

                    alasql('SELECT requirementID as RequirementID, title as RequirementTitle, handleDate(postedDate) as [PostedDate], status as Status, vendorID as VendorID, companyName as CompanyName, itemID as [ItemID], productBrand as Brand, productQuantity as Quantity, productQuantityIn as [Units], costPrice as [InitialCostPrice], revCostPrice as [FinalCostPrice], netPrice as [InitialNetPrice], marginAmount as [InitialMarginAmount], unitDiscount as [MarginPercentage],revUnitDiscount as [RevisedMarginPercentage],unitMRP as [MRP],gst as [GST] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xlsx", $scope.reqReport]);

                });
        };
        

        
        //$scope.getAuctions();
        $scope.getLeads = function () {
            PRMForwardBidService.getactiveleads({ "userid": userService.getUserId(), "page": ($scope.currentPage - 1) * $scope.itemsPerPage, "limit": $scope.itemsPerPage, "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.myActiveLeads1 = response;
                    $scope.myActiveLeads = response;

                    $scope.myActiveLeads.forEach(function (item, index) {
                        if (String(item.startTime).includes('9999')) {
                            item.startTime = '';
                        }
                        if (String(item.expStartTime).includes('9999')) {
                            item.expStartTime = '';
                        }
                        item.postedOn = $scope.GetDateconverted(item.postedOn);
                        item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                        item.expStartTime = $scope.GetDateconverted(item.expStartTime);
                        item.startTime = $scope.GetDateconverted(item.startTime);
                    });

                    if ($scope.myActiveLeads.length > 0) {

                        $scope.myAuctionsLoaded = true;
                        $scope.totalLeads = $scope.myActiveLeads.length;
                    } else {
                        $scope.totalLeads = 0;
                        $scope.myAuctionsLoaded = false;
                        $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                    }
                });
        };
            
        if ($scope.isOTPVerified && $scope.isEmailOTPVerified) {
            $scope.getLeads();
        }

        $scope.downloadTemplate = function (name) {
            reportingService.downloadTemplate(name, userService.getUserId(), 0);
        };

        $scope.filters = {
            reqStatus: '',
            priority: ''
        };

        $scope.postedOnDate = '';
        $scope.getStatusFilter = function () {
            $scope.myActiveLeads = $scope.myActiveLeads1;
            $scope.filterArray = [];
            if ($scope.filters.reqStatus) {
                $scope.filterArray = $scope.myActiveLeads.filter(function (item) {
                    return $scope.prmStatus($scope.isCustomer, item.status.toLowerCase()) === $scope.prmStatus($scope.isCustomer, $scope.filters.reqStatus.toLowerCase());
                });

                $scope.myActiveLeads = $scope.filterArray;
            }
            if ($scope.filters.priority) {
                $scope.filterArray = $scope.myActiveLeads.filter(function (item) {
                    return item.urgency.split(' ')[0].toLowerCase() === $scope.filters.priority.toLowerCase()
                });
                $scope.myActiveLeads = $scope.filterArray
            }
            if ($scope.postedFromDate) {
                $scope.filterArray = $scope.myActiveLeads.filter(function (item) {
                    return ((item.postedOn.split(' ')[0]) >= ($scope.postedFromDate))

                });
                $scope.myActiveLeads = $scope.filterArray;
            }
            if ($scope.postedToDate) {
                $scope.filterArray = $scope.myActiveLeads.filter(function (item) {
                    return ((item.postedOn.split(' ')[0]) <= ($scope.postedToDate))

                });
                $scope.myActiveLeads = $scope.filterArray;
            }
            if ($scope.searchKeyword) {
                $scope.myActiveLeads = $scope.myActiveLeads.filter(function (req) {
                    return (String(req.requirementID).includes($scope.searchKeyword)
                        || String(req.title.toLowerCase()).includes($scope.searchKeyword.toLowerCase())
                        || String(req.quotationFreezTime).includes($scope.searchKeyword)
                        || String(req.startTime).includes($scope.searchKeyword)
                        || String(req.postedOn).includes($scope.searchKeyword)
                        || String(req.biddingType.toLowerCase()).includes($scope.searchKeyword.toLowerCase())
                        || String(req.requirementNumber.toLowerCase()).includes($scope.searchKeyword.toLowerCase())
                    );
                });
            }

            $scope.totalLeads = $scope.myActiveLeads.length;
        };

        $scope.getFile1 = function (id, itemid, ext) {
            $scope.file = $("#" + id)[0].files[0];
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    if (id === "excelquotation") {
                        var bytearray = new Uint8Array(result);
                        $scope.quotationAttachment = $.makeArray(bytearray);
                        $scope.uploadquotationsfromexcel();
                    }
                });
        };

        $scope.uploadquotationsfromexcel = function (status) {
            var params = {
                reqID: 0,
                userID: userService.getUserId(),
                sessionID: userService.getUserToken(),
                quotationAttachment: $scope.quotationAttachment
            };

            PRMForwardBidService.uploadquotationsfromexcel(params)
                .then(function (response) {
                    if (!response.errorMessage) {
                        swal("Success", "Uploaded Successfully!", 'success');
                        location.reload();
                    } else {
                        swal("Error", response.errorMessage, 'error');
                    }
                });
        };

        $scope.chatRequirement = function (requirement) {
            var url = $state.href('req-chat', { "reqId": requirement.requirementID });
            $window.open(url, '_blank');
        };
    });
﻿prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('saveFwdRequirementCtrl', ["$state", "$stateParams", "$scope", "$rootScope", "auctionsService", "userService", "$http", "$window", "domain",
        "fileReader", "growlService", "$log", "$filter", "ngDialog", "techevalService", "catalogService",
        "workflowService", "PRMPRServices", "PRMCustomFieldService", "reportingService", "PRMCustomFieldService", "PRMForwardBidService", "$modal",
        function ($state, $stateParams, $scope, $rootScope, auctionsService, userService, $http, $window, domain,
            fileReader, growlService, $log, $filter, ngDialog, techevalService, catalogService,
            workflowService, PRMPRServices, PRMCustomFieldService, reportingService, PRMCustomFieldService, PRMForwardBidService, $modal) {
            $scope.showCatalogQuotationTemplate = true;
            $scope.userID = userService.getUserId();
            $scope.cloneId = ($stateParams.reqObj || {}).cloneId;
            $scope.isClone = ($stateParams.reqObj && $stateParams.reqObj.cloneId);
            $scope.selectedPRNumbers = $stateParams.selectedPRNumbers;
            $scope.prmTemplates = [];
            $scope.selectedTemplate = {};
            $scope.requirementPRStatus = {
                MESSAGE: '',
                showStatus: false
            };
            $scope.totalAttachmentMaxSize = 6291456;
            $scope.totalRequirementSize = 0;
            $scope.totalRequirementItemSize = 0;
            $scope.userproductsLoading = false;
            $scope.prmFieldMappingDetails = {};
            $scope.requirementSettings = [];
            $scope.reqId = $stateParams.Id;
            $scope.contractPRItems = [];
            $scope.templateSubItems = [];
            $scope.currentSystemDate = null;
            $scope.widgetStates = {
                MIN: 0,
                MAX: 1,
                PIN: -1
            };
            $scope.isCasNumber = false;
            $scope.isMfcdCode = false;
            if ($stateParams.Id) {
                $scope.stateParamsReqID = $stateParams.Id;
                $scope.postRequestLoding = true;
                $scope.showCatalogQuotationTemplate = false;
                $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
                $scope.desigID = userService.getSelectedUserDesigID();
                $scope.deptTypeID = userService.getSelectedUserDepartmentDesignation().deptTypeID;

            } else {
                $scope.stateParamsReqID = 0;
                $scope.postRequestLoding = false;
                $scope.showCatalogQuotationTemplate = true;
                $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
                $scope.desigID = userService.getSelectedUserDesigID();
                $scope.deptTypeID = userService.getSelectedUserDepartmentDesignation().deptTypeID;

            };

            $scope.INCO_TERMS = [];
            $scope.companyItemUnits = [];
            $scope.customFieldList = [];
            $scope.selectedcustomFieldList = [];
            $scope.otherRequirementItems = {
                INSTALLATION_CHARGES: true,
                PACKAGING: true,
                FREIGHT_CHARGES: true
            };
            var selectedItemIds = [];
            $scope.selectAllCheckBox = 0;
            if (!$scope.reqId) {
                $scope.selectAllCheckBox = 1;
            }
            $scope.selectAllCheckBoxMandate = 0;
            if (!$scope.reqId) {
                $scope.selectAllCheckBoxMandate = 1;
            }
            $scope.selectAllQuotationTemplate = function (product, value) {

                if (value === 1) {
                    product.productQuotationTemplate.forEach(function (item) {
                        item.IS_VALID = 1;
                    })
                }
                else {
                    product.productQuotationTemplate.forEach(function (item) {
                        item.IS_VALID = 0;
                    })
                }
            }
            $scope.selectAllQuotationTemplateMandate = function (product, value) {

                if (value === 1) {
                    product.productQuotationTemplate.forEach(function (item) {
                        if (item.HAS_SPECIFICATION == 1) {
                            item.IS_MANDATE_TO_VENDOR = 1;
                        }
                    })
                }
                else {
                    product.productQuotationTemplate.forEach(function (item) {
                        item.IS_MANDATE_TO_VENDOR = 0;
                    })
                }
            }

            $scope.SelectedVendorsCount = 0;
            $scope.isTechEval = false;
            $scope.isForwardBidding = false;
            $scope.allCompanyVendors = [];
            $scope.selectedProducts = [];

            if ($stateParams.selectedTemplate && $stateParams.selectedTemplate.TEMPLATE_ID) {
                $scope.selectedTemplate = $stateParams.selectedTemplate;
            }

            var curDate = new Date();
            var today = moment();
            var tomorrow = today.add('days', 1);
            //var dateObj = $('.datetimepicker').datetimepicker({
            //    format: 'DD/MM/YYYY',
            //    useCurrent: false,
            //    minDate: tomorrow,
            //    keepOpen: false
            //});
            $scope.subcategories = [];
            $scope.sub = {
                selectedSubcategories: [],
            }
            $scope.selectedCurrency = {};
            $scope.currencies = [];
            $scope.questionnaireList = [];

            //$scope.postRequestLoding = false;
            $scope.selectedSubcategories = [];

            $scope.companyCatalogueList = [];

            $scope.selectVendorShow = true;
            $scope.isEdit = false;

            //$scope.vendorCountries = [{ id: 1, name: 'India' }, { id: 2, name: 'China' }, { id: 1, name: 'USA' }];
            $scope.filterVendorCountries = [];
            $scope.vendorCountries = [];
            //Input Slider
            this.nouisliderValue = 4;
            this.nouisliderFrom = 25;
            this.nouisliderTo = 80;
            this.nouisliderRed = 35;
            this.nouisliderBlue = 90;
            this.nouisliderCyan = 20;
            this.nouisliderAmber = 60;
            this.nouisliderGreen = 75;

            //Color Picker
            this.color = '#03A9F4';
            this.color2 = '#8BC34A';
            this.color3 = '#F44336';
            this.color4 = '#FFC107';

            $scope.Vendors = [];
            $scope.VendorsTemp = [];
            $scope.categories = [];
            $scope.selectedA = [];
            $scope.selectedB = [];
            $scope.showCategoryDropdown = false;
            $scope.checkVendorPhoneUniqueResult = false;
            $scope.checkVendorEmailUniqueResult = false;
            $scope.checkVendorPanUniqueResult = false;
            $scope.checkVendorTinUniqueResult = false;
            $scope.checkVendorStnUniqueResult = false;
            $scope.showFreeCreditsMsg = false;
            $scope.showNoFreeCreditsMsg = false;
            $scope.formRequest = {
                productNameSearch: false,
                productCodeSearch: false,
                productNoSearch: false,
                isTabular: true,
                isRFP: false,
                auctionVendors: [],
                listRequirementItems: [],
                isQuotationPriceLimit: false,
                quotationPriceLimit: 0,
                quotationFreezTime: '',
                deleteQuotations: false,
                expStartTimeName: '',
                isDiscountQuotation: 0,
                isRevUnitDiscountEnable: 0,
                multipleAttachments: [],
                customerEmails: [],
                contractStartTime: '',
                contractEndTime: '',
                isContract: false,
                isRFQ: 1,
                biddingType: 'REVERSE',
                projectName: '',
                projectDetails: '',
                generalTC: '',
                deleteQuotations: false,
                disableDeleteQuotations: false,
                selectedPRItemIds: ''
            };

            //If create RFP is clicked//
            if ($stateParams.isRFP) {
                $scope.formRequest.isRFP = true;
            }
            //If create RFP is clicked//

            $scope.Vendors.city = "";
            $scope.Vendors.quotationUrl = "";
            $scope.vendorsLoaded = false;
            $scope.requirementAttachment = [];
            $scope.selectedProjectId = 0;
            $scope.selectedProject = {};
            $scope.branchProjects = [
                { PROJECT_ID: "RAW MATERIALS", PROJECT_CODE: "RAW MATERIALS" },
                { PROJECT_ID: "CAPEX", PROJECT_CODE: "CAPEX" },
                { PROJECT_ID: "SOLAR", PROJECT_CODE: "SOLAR" }
            ];

            $scope.changeProject = function () {
                console.log($scope.selectedProjectId);
                var filterdProjects = _.filter($scope.branchProjects, function (o) {
                    return o.PROJECT_ID == $scope.selectedProjectId;
                });
                if (filterdProjects && filterdProjects.length > 0) {
                    $scope.selectedProject = filterdProjects[0];
                }
            }

            $scope.sessionid = userService.getUserToken();

            $scope.selectedQuestionnaire == {}

            $scope.formRequest.indentID = 0;

            $scope.cijList = [];

            $scope.indentList = [];
            $scope.companyCurrencies = [];

            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.workflowObj = {};
            $scope.formRequest.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'QUOTATION';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/
            $scope.companyPaymentTerms = [];
            $scope.compID = userService.getUserCompanyId();
            $scope.companyConfigStr = '';
            $scope.companyConfigurationArr = ['INCO', 'ITEM_UNITS', 'COUNTRY', 'PAYMENT_TERMS'];
            $scope.companyConfigStr = $scope.companyConfigurationArr.join(',').toString();
            $scope.companyConfigList = [];

            //$scope.vendorCountries = [];

            $scope.nonCoreproductsList = [];
            $scope.requirementAnalysisData = [];

            $scope.GetCompanyCurrencies = function () {
                auctionsService.GetCompanyCurrencies($scope.compID, userService.getUserToken())
                    .then(function (response) {
                        $scope.companyCurrencies = response;
                        $scope.companyCurrencies.forEach(function (currency, index) {
                            currency.StartTimeLocal = new Date(parseInt(currency.startTime.substr(6)));
                            currency.EndTimeLocal = new Date(parseInt(currency.endTime.substr(6)));
                        });
                    });
            };

            $scope.GetCompanyCurrencies();

            auctionsService.GetCompanyConfiguration($scope.compID, $scope.companyConfigStr, userService.getUserToken())
                .then(function (unitResponse) {

                    $scope.companyConfigList = unitResponse;
                    var countriesTemp = [];
                    $scope.companyConfigList.forEach(function (item, index) {
                        if (item.configKey == 'INCO') {
                            $scope.INCO_TERMS.push(item);
                        } else if (item.configKey == 'ITEM_UNITS') {
                            $scope.companyItemUnits.push(item);
                        } else if (item.configKey == 'COUNTRY') {
                            countriesTemp.push({ id: index, name: item.configValue });
                            $scope.vendorCountries = countriesTemp;
                        } else if (item.configKey == 'PAYMENT_TERMS') {
                            $scope.companyPaymentTerms.push(item);
                        }
                    });

                });

            catalogService.GetNonCoreProducts($scope.compID, userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.nonCoreproductsList = unitResponse;
                    $scope.getUserProducts(0, '', 0, '', '');
                });

            $scope.getPreviousItemPrice = function (itemDetails, index) {
                itemDetails.itemLastPriceArr = [];
                itemDetails.showTable = true;
                itemDetails.itemPreviousPrice = {};
                itemDetails.itemPreviousPrice.lastPrice = -1;
                itemDetails.bestPriceEnable = index;
                $log.info(itemDetails);
                itemDetails.sessionID = userService.getUserToken();
                itemDetails.compID = userService.getUserCompanyId();
                PRMForwardBidService.getPreviousItemPrice(itemDetails)
                    .then(function (response) {
                        if (response && response.errorMessage === '') {
                            itemDetails.itemPreviousPrice = {};
                            itemDetails.itemPreviousPrice.lastPrice = Number(response.initialPrice);
                            itemDetails.itemPreviousPrice.lastPriceDate = userService.toLocalDate(response.currentTime);
                            itemDetails.itemPreviousPrice.lastPriceVendor = response.companyName;

                            if (itemDetails.itemPreviousPrice.lastPrice) {
                                itemDetails.bestLastPriceEnable = index;
                                itemDetails.showTable = true;
                                itemDetails.itemLastPriceArr.push({
                                    isBestPrice: true,
                                    requirementID: '',
                                    unitPrice: itemDetails.itemPreviousPrice.lastPrice,
                                    quantity: '',
                                    companyName: itemDetails.itemPreviousPrice.lastPriceVendor,
                                    currentTime: itemDetails.itemPreviousPrice.lastPriceDate
                                });
                            }
                        }
                    });
            };

            $scope.dispalyLastprices = function (product, val) {
                product.showTable = val;
            }

            $scope.GetLastPrice = function (itemDetails, index) {
                itemDetails.itemLastPriceArr = [];
                itemDetails.bestLastPriceEnable = index;
                itemDetails.sessionID = userService.getUserToken();
                itemDetails.compID = userService.getUserCompanyId();
                PRMForwardBidService.GetLastPrice(itemDetails)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            itemDetails.itemLastPriceArr = response;
                            itemDetails.itemLastPriceArr.forEach(function (item, index) {
                                item.currentTime = userService.toLocalDate(item.currentTime);
                                item.isBestPrice = false;
                            });
                        }
                    });
            };

            $scope.budgetValidate = function () {
                if ($scope.formRequest.budget != "" && (isNaN($scope.formRequest.budget) || $scope.formRequest.budget.indexOf('.') > -1)) {
                    $scope.postRequestLoding = false;
                    swal({
                        title: "Error!",
                        text: "Please enter valid budget, budget should be greater than 1,00,000.",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {

                        });

                    $scope.formRequest.budget = "";
                }
            };

            $scope.clickToOpen = function () {
                ngDialog.open({ template: 'login/termsAddNewReq.html', width: 1000, height: 500 });
            };

            $scope.changeCategory = function () {

                $scope.selectedSubCategoriesList = [];
                $scope.formRequest.auctionVendors = [];
                $scope.loadSubCategories();
                //$scope.getvendors();
            }

            $scope.getCreditCount = function () {
                userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.userDetails = response;
                        //$scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                        //$scope.selectedCurrency = $scope.selectedCurrency[0];
                        if (response.creditsLeft) {
                            $scope.showFreeCreditsMsg = true;
                        } else {
                            $scope.showNoFreeCreditsMsg = true;
                        }
                    });
            }



            $scope.VendorsList = [];
            $scope.VendorsTempList1 = [];

            $scope.getvendors = function (catObj) {
                let countryFilter = '';

                if ($scope.filterVendorCountries && $scope.filterVendorCountries.length > 0) {
                    countryFilter = _.map($scope.filterVendorCountries, 'name').join();
                }
                $scope.ShowDuplicateVendorsNames = [];
                $scope.VendorsList = [];
                $scope.VendorsTempList1 = [];
                $scope.vendorsLoaded = false;
                var category = '';

                category = catObj ? catObj.catCode : '';

                var params = { 'Categories': category, 'locations': countryFilter, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                $http({
                    method: 'POST',
                    url: domain + 'getvendors',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            // filter vendors who has contracts
                            if ($scope.contractPRItems && $scope.contractPRItems.length > 0) {
                                let filteredVendors = _.filter(response.data, function (venObj) {
                                    return _.findIndex($scope.contractPRItems, (item) => { return item.CONTRACT_VENDOR === venObj.vendorID }) < 0;
                                });
                                response.data = filteredVendors;
                            }


                            $scope.VendorsList = response.data;
                            $scope.VendorsTempList = $scope.VendorsList;
                            $scope.Vendors.forEach(function (item, index) {
                                $scope.VendorsTempList.forEach(function (item1, index1) {
                                    if (item.vendorID == item1.vendorID) {
                                        $scope.ShowDuplicateVendorsNames.push(item1);
                                        $scope.VendorsList.splice(index1, 1);
                                    }
                                })
                            });

                            if ($scope.formRequest.auctionVendors.length > 0) {
                                $scope.formRequest.auctionVendors.forEach(function (item1, index1) {
                                    $scope.VendorsTempList.forEach(function (item2, index2) {
                                        if (item1.vendorID == item2.vendorID) {
                                            $scope.ShowDuplicateVendorsNames.push(item2);
                                            $scope.VendorsList.splice(index2, 1);
                                        }
                                    });
                                });
                            }


                            $scope.VendorsTempList1 = $scope.VendorsList;

                            if ($scope.ShowDuplicateVendorsNames.length > 0) {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                                //swal({
                                //    title: "Cancelled",
                                //    type: "error",
                                //    confirmButtonText: "Ok",
                                //    allowOutsideClick: true,
                                //    customClass: 'swal-wide'
                                //});
                                //$(".sweet-alert h2").html("oops...! Some vendors are already retrieved in products they are <h3 style='color:red'><div style='max-height: 400px;overflow-y: auto;overflow-x:scroll'>" + $scope.ShowDuplicateVendorsNames + "</div></h3> so vendors are not retrieved in the categories even though they are assigned.");
                            } else {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                            }

                        } else {
                            $scope.VendorsList = [];
                        }

                        if ($scope.searchCategoryVendorstring != '') {
                            $scope.searchingCategoryVendors($scope.searchCategoryVendorstring);
                        } else {
                            $scope.searchingCategoryVendors('');
                        }


                    } else {

                    }
                }, function (result) {
                });
            };

            $scope.getReqQuestionnaire = function () {


            }

            $scope.getQuestionnaireList = function () {
                techevalService.getquestionnairelist(0)
                    .then(function (response) {
                        $scope.questionnaireList = $filter('filter')(response, { reqID: 0 });
                        if ($stateParams.Id && $stateParams.Id > 0) {
                            techevalService.getreqquestionnaire($stateParams.Id, 1)
                                .then(function (response) {
                                    $scope.selectedQuestionnaire = response;

                                    if ($scope.selectedQuestionnaire && $scope.selectedQuestionnaire.evalID > 0) {
                                        $scope.isTechEval = true;
                                    }

                                    $scope.questionnaireList.push($scope.selectedQuestionnaire);
                                })
                        }
                    })
            };



            $scope.isRequirementPosted = 0;


            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));
                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    } else {
                    }
                }, function (result) {
                });

            };


            $scope.getCurrencies = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getkeyvaluepairs?parameter=CURRENCY&compid=' + $scope.compID,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.currencies = response.data;
                            if (!$scope.formRequest.currency) {
                                $scope.formRequest.currency = 'INR';
                            }

                            $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: $scope.formRequest.currency });
                            $scope.selectedCurrency = $scope.selectedCurrency[0];

                            $scope.getCreditCount();
                        }
                    } else {
                    }
                }, function (result) {
                });
            };




            $scope.getData = function () {

                // $scope.getCategories();

                if ($stateParams.Id) {
                    var id = $stateParams.Id;
                    $scope.isEdit = true;


                    PRMForwardBidService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), "userid": userService.getUserId() })
                        .then(function (response) {
                            $scope.GetReqPRItems();
                            $scope.selectedProjectId = response.projectId;
                            var category = response.category ? response.category[0] : '';
                            response.category = category;
                            response.taxes = parseInt(response.taxes);
                            //response.paymentTerms = parseInt(response.paymentTerms);
                            $scope.formRequest = response;
                            loadPRData();
                            $scope.formRequest.isBiddingTypeSpot = $scope.formRequest.biddingType === 'SPOT' ? true : false;
                            $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: $scope.formRequest.currency });
                            $scope.selectedCurrency = $scope.selectedCurrency[0];
                            //$scope.getPRNumber($scope.formRequest.PR_ID);
                            $scope.itemSNo = $scope.formRequest.itemSNoCount;
                            //$scope.GetIncoTerms();
                            $scope.formRequest.checkBoxEmail = true;
                            $scope.formRequest.checkBoxSms = true;
                            $scope.loadSubCategories();
                            $scope.getSubUserData();
                            $scope.isRequirementPosted = $scope.formRequest.auctionVendors.length;
                            if (!$scope.formRequest.multipleAttachments) {
                                $scope.formRequest.multipleAttachments = [];
                            }
                            $scope.formRequest.attFile = response.attachmentName;
                            if ($scope.formRequest.attFile != '' && $scope.formRequest.attFile != null && $scope.formRequest.attFile != undefined) {
                                var attchArray = $scope.formRequest.attFile.split(',');
                                attchArray.forEach(function (att, index) {

                                    var fileUpload = {
                                        fileStream: [],
                                        fileName: '',
                                        fileID: att
                                    };

                                    $scope.formRequest.multipleAttachments.push(fileUpload);
                                });
                            }

                            $scope.selectedSubcategories = response.subcategories.split(",");
                            for (i = 0; i < $scope.selectedSubcategories.length; i++) {
                                for (j = 0; j < $scope.subcategories.length; j++) {
                                    if ($scope.selectedSubcategories[i] == $scope.subcategories[j].subcategory) {
                                        $scope.subcategories[j].ticked = true;
                                    }
                                }
                            }

                            //$scope.getvendors();
                            $scope.selectSubcat();
                            $scope.formRequest.attFile = response.attachmentName;
                            $scope.formRequest.quotationFreezTime = userService.toLocalDate($scope.formRequest.quotationFreezTime);
                            //$scope.formRequest.quotationFreezTimeNew = userService.toLocalDate($scope.formRequest.quotationFreezTime);
                            //$scope.formRequest.urgency.push(urgency);

                            $scope.formRequest.expStartTime = userService.toLocalDate($scope.formRequest.expStartTime);
                            $scope.SelectedVendors = $scope.formRequest.auctionVendors;
                            $scope.SelectedVendorsCount = $scope.formRequest.auctionVendors.length;
                            $scope.formRequest.auctionVendors.forEach(function (vendor, vendorIndex) {
                                vendor.isSelected = true;
                            });

                            $scope.formRequest.listRequirementItems.forEach(function (item, itemIndex) {
                                item.AddMultipleQuantities = [];
                                item.CategoryId = item.CATEGORY_ID;
                                item.productQtyTemp = item.productQuantity;
                                item.isNonCoreProductCategory = !item.isCoreProductCategory; //Try to clean up keep only one property
                                if (item.productQuotationTemplateJson && item.productQuotationTemplateJson != '' && item.productQuotationTemplateJson != null && item.productQuotationTemplateJson != undefined) {
                                    item.productQuotationTemplateArray = JSON.parse(item.productQuotationTemplateJson);
                                    item.productQuotationTemplate = JSON.parse(item.productQuotationTemplateJson);

                                    /******** Changed for Multi Quantity *******/
                                    $scope.GetProductQuotationTemplate(item.catalogueItemID, itemIndex);
                                    /******** Changed for Multi Quantity *******/
                                }
                                else {
                                    item.productQuotationTemplateArray = [];
                                }

                                if (item.productQuotationTemplate && item.productQuotationTemplate.length > 0) {
                                    item.productQuotationTemplate.pop();
                                }
                            });

                            $scope.postRequestLoding = false;
                        });
                }

            };

            $scope.showSimilarNegotiationsButton = function (value, searchstring) {
                $scope.showSimilarNegotiations = value;
                if (!value) {
                    $scope.CompanyLeads = {};
                }
                if (value) {
                    if (searchstring.length < 3) {
                        $scope.CompanyLeads = {};
                    }
                    if (searchstring.length > 2) {
                        $scope.searchstring = searchstring;
                        $scope.GetCompanyLeads(searchstring);
                    }
                }
                return $scope.showSimilarNegotiations;
            }

            $scope.showSimilarNegotiations = false;

            $scope.CompanyLeads = {};

            $scope.searchstring = '';

            $scope.GetCompanyLeads = function (searchstring) {
                if (searchstring.length < 3) {
                    $scope.CompanyLeads = {};
                }
                if ($scope.showSimilarNegotiations && searchstring.length > 2) {
                    $scope.searchstring = searchstring;
                    var params = { "userid": userService.getUserId(), "searchstring": $scope.searchstring, "searchtype": 'Title', "sessionid": userService.getUserToken() };
                    PRMForwardBidService.GetCompanyLeads(params)
                        .then(function (response) {
                            $scope.CompanyLeads = response;
                            $scope.CompanyLeads.forEach(function (item, index) {
                                item.postedOn = userService.toLocalDate(item.postedOn);
                            })
                        });
                }
            }


            $scope.changeScheduledAuctionsLimit = function () {
                $scope.scheduledLimit = 8;
                $scope.getMiniItems();
            }

            $scope.loadSubCategories = function () {
                $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.formRequest.category });
                /*$scope.subcategories = _.map($scope.subcategories, 'subcategory');*/
            }

            $scope.selectedSubCategoriesList = [];

            $scope.selectSubcat = function (subcat) {

                $scope.selectedSubCategoriesList = [];

                if (!$scope.isEdit) {
                    $scope.formRequest.auctionVendors = [];
                }
                $scope.vendorsLoaded = false;
                var category = [];
                var count = 0;
                var succategory = "";
                $scope.sub.selectedSubcategories = $filter('filter')($scope.subcategories, { ticked: true });
                selectedcount = $scope.sub.selectedSubcategories.length;
                if (selectedcount > 0) {
                    succategory = _.map($scope.sub.selectedSubcategories, 'id');
                    category.push(succategory);

                    $scope.selectedSubCategoriesList = succategory;

                    //$scope.formRequest.category = category;
                    if ($scope.formRequest.category != undefined) {
                        var params = { 'Categories': succategory, 'sessionID': userService.getUserToken(), 'count': selectedcount, 'uID': userService.getUserId(), evalID: $scope.selectedQuestionnaire ? $scope.selectedQuestionnaire.evalID : 0 };
                        $http({
                            method: 'POST',
                            url: domain + 'getvendorsbycatnsubcat',
                            encodeURI: true,
                            headers: { 'Content-Type': 'application/json' },
                            data: params
                        }).then(function (response) {
                            if (response && response.data) {
                                if (response.data.length > 0) {
                                    $scope.Vendors = response.data;
                                    $scope.vendorsLoaded = true;
                                    for (var j in $scope.formRequest.auctionVendors) {
                                        for (var i in $scope.Vendors) {
                                            if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                                $scope.Vendors.splice(i, 1);
                                            }
                                        }
                                    }

                                    $scope.VendorsTemp = $scope.Vendors;
                                    $scope.searchVendors('');

                                }
                                //$scope.formRequest.auctionVendors =[];
                            } else {
                            }
                        }, function (result) {
                        });
                    }
                } else {
                    //$scope.getvendors();
                }

            }

            $scope.getData();

            $scope.checkVendorUniqueResult = function (idtype, inputvalue) {


                if (idtype == "PHONE") {
                    $scope.checkVendorPhoneUniqueResult = false;
                } else if (idtype == "EMAIL") {
                    $scope.checkVendorEmailUniqueResult = false;
                }
                else if (idtype == "PAN") {
                    $scope.checkVendorPanUniqueResult = false;
                }
                else if (idtype == "TIN") {
                    $scope.checkVendorTinUniqueResult = false;
                }
                else if (idtype == "STN") {
                    $scope.checkVendorStnUniqueResult = false;
                }

                if (inputvalue == "" || inputvalue == undefined) {
                    return false;
                }

                userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                    if (idtype == "PHONE") {
                        $scope.checkVendorPhoneUniqueResult = !response;
                    } else if (idtype == "EMAIL") {
                        $scope.checkVendorEmailUniqueResult = !response;
                    }
                    else if (idtype == "PAN") {
                        $scope.checkVendorPanUniqueResult = !response;
                    }
                    else if (idtype == "TIN") {
                        $scope.checkVendorTinUniqueResult = !response;
                    }
                    else if (idtype == "STN") {
                        $scope.checkVendorStnUniqueResult = !response;
                    }
                });
            };

            $scope.selectForA = function (vendor) {
                vendor.isSelected = true;
                if ($scope.formRequest.auctionVendors && $scope.formRequest.auctionVendors.length > 0) {
                    $scope.formRequest.auctionVendors = _.filter($scope.formRequest.auctionVendors, function (x) { return x.vendorID !== vendor.vendorID; });
                } else {
                    $scope.formRequest.auctionVendors = [];
                }

                $scope.formRequest.auctionVendors.push(vendor);
                $scope.GetRequirementVendorAnalysis();
                var tempVendor = _.filter($scope.Vendors, function (x) { return x.vendorID === vendor.vendorID; });
                if (tempVendor && tempVendor.length > 0) {
                    tempVendor[0].isSelected = true;
                }

                var unSelectedVendors = _.filter($scope.Vendors, function (x) { return !x.isSelected; });
                if (unSelectedVendors && unSelectedVendors.length > 0) {
                    $scope.selectAllVendors = false;
                }
            };

            $scope.selectForB = function (vendor) {
                vendor.isSelected = false;
                $scope.GetRequirementVendorAnalysis();
                $scope.formRequest.auctionVendors = _.filter($scope.formRequest.auctionVendors, function (x) { return x.vendorID !== vendor.vendorID; });
                var tempVendor = _.filter($scope.Vendors, function (x) { return x.vendorID === vendor.vendorID; });
                if (tempVendor && tempVendor.length > 0) {
                    tempVendor[0].isSelected = false;
                }

                var unSelectedVendors = _.filter($scope.Vendors, function (x) { return !x.isSelected; });
                if (unSelectedVendors && unSelectedVendors.length > 0) {
                    $scope.selectAllVendors = false;
                }
            };

            $scope.multipleAttachments = [];
            $scope.getFile = function () {
                $scope.progress = 0;
                $scope.totalRequirementSize = 0;
                //$scope.file = $("#attachement")[0].files[0];
                $scope.multipleAttachments = $("#attachement")[0].files;
                $scope.multipleAttachments = Object.values($scope.multipleAttachments);
                if ($scope.multipleAttachments && $scope.multipleAttachments.length > 0) {
                    $scope.multipleAttachments.forEach(function (item, index) {
                        $scope.totalRequirementSize = $scope.totalRequirementSize + item.size;
                    });
                }

                if (($scope.totalRequirementSize + $scope.totalRequirementItemSize) > $scope.totalAttachmentMaxSize) {
                    swal({
                        title: "Attachment size!",
                        text: "Total Attachments size cannot exceed 6MB",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            return;
                        });
                    return;
                }

                $scope.multipleAttachments.forEach(function (item, index) {
                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };

                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;
                            if (!$scope.formRequest.multipleAttachments) {
                                $scope.formRequest.multipleAttachments = [];
                            }

                            var ifExists = _.findIndex($scope.formRequest.multipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                            if (ifExists > -1) {
                            } else {
                                $scope.formRequest.multipleAttachments.push(fileUpload);

                            }

                        });
                });

            };


            $scope.newVendor = {};
            $scope.Attaachmentparams = {};
            $scope.deleteAttachment = function (reqid) {
                $scope.Attaachmentparams = {
                    reqID: reqid,
                    userID: userService.getUserId()
                }
                PRMForwardBidService.deleteAttachment($scope.Attaachmentparams)
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Attachment deleted Successfully", "inverse");
                            $scope.getData();
                        }
                    });
            }

            $scope.newVendor.panno = "";
            $scope.newVendor.vatNum = "";
            $scope.newVendor.serviceTaxNo = "";

            $scope.addVendor = function () {
                $scope.emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                $scope.mobileRegx = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
                $scope.panregx = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
                //$scope.emailRegx = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
                var addVendorValidationStatus = false;
                $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                if ($scope.newVendor.companyName == "" || $scope.newVendor.companyName === undefined) {
                    $scope.companyvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.firstName == "" || $scope.newVendor.firstName === undefined) {
                    $scope.firstvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.lastName == "" || $scope.newVendor.lastName === undefined) {
                    $scope.lastvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.contactNum == "" || $scope.newVendor.contactNum === undefined || isNaN($scope.newVendor.contactNum)) {
                    $scope.contactvalidation = true;
                    addVendorValidationStatus = true;
                }
                else if ($scope.newVendor.contactNum.length != 10) {
                    $scope.contactvalidationlength = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.email == "" || $scope.newVendor.email === undefined) {
                    $scope.emailvalidation = true;
                    addVendorValidationStatus = true;
                }
                else if (!$scope.emailRegx.test($scope.newVendor.email)) {
                    $scope.emailregxvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.vendorcurrency == "" || $scope.newVendor.vendorcurrency === undefined) {
                    $scope.vendorcurrencyvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.panno != "" && $scope.newVendor.panno != undefined && !$scope.panregx.test($scope.newVendor.panno)) {
                    $scope.panregxvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.vatNum != "" && $scope.newVendor.vatNum != undefined && $scope.newVendor.vatNum.length != 11) {
                    $scope.tinvalidation = true;
                    addVendorValidationStatus = true;
                }

                if ($scope.newVendor.serviceTaxNo != "" && $scope.newVendor.serviceTaxNo != undefined && $scope.newVendor.serviceTaxNo.length != 15) {
                    $scope.stnvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.formRequest.category == "" || $scope.formRequest.category === undefined) {
                    $scope.categoryvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.checkVendorEmailUniqueResult || $scope.checkVendorEmailUniqueResult) {
                    addVendorValidationStatus = true;
                }
                if (addVendorValidationStatus) {
                    return false;
                }
                var vendCAtegories = [];
                $scope.newVendor.category = $scope.formRequest.category;
                vendCAtegories.push($scope.newVendor.category);
                var params = {
                    "register": {
                        "firstName": $scope.newVendor.firstName,
                        "lastName": $scope.newVendor.lastName,
                        "email": $scope.newVendor.email,
                        "phoneNum": $scope.newVendor.contactNum,
                        "username": $scope.newVendor.contactNum,
                        "password": $scope.newVendor.contactNum,
                        "companyName": $scope.newVendor.companyName ? $scope.newVendor.companyName : "",
                        "isOTPVerified": 0,
                        "category": $scope.newVendor.category,
                        "userType": "VENDOR",
                        "panNumber": ("panno" in $scope.newVendor) ? $scope.newVendor.panno : "",
                        "stnNumber": ("serviceTaxNo" in $scope.newVendor) ? $scope.newVendor.serviceTaxNo : "",
                        "vatNumber": ("vatNum" in $scope.newVendor) ? $scope.newVendor.vatNum : "",
                        "referringUserID": userService.getUserId(),
                        "knownSince": ("knownSince" in $scope.newVendor) ? $scope.newVendor.knownSince : "",
                        "errorMessage": "",
                        "sessionID": "",
                        "userID": 0,
                        "department": "",
                        "currency": $scope.newVendor.vendorcurrency.key,
                        "altPhoneNum": $scope.newVendor.altPhoneNum,
                        "altEmail": $scope.newVendor.altEmail,
                        "subcategories": $scope.selectedSubCategoriesList
                    }
                };
                $http({
                    method: 'POST',
                    url: domain + 'register',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if ((response && response.data && response.data.errorMessage == "") || response.data.errorMessage == 'User assigned to Company.') {
                        $scope.formRequest.auctionVendors.push({ vendorName: $scope.newVendor.firstName + " " + $scope.newVendor.lastName, companyName: $scope.newVendor.companyName, vendorID: response.data.objectID });
                        $scope.GetRequirementVendorAnalysis();
                        $scope.newVendor = null;
                        $scope.newVendor = {};
                        //$scope.addVendorForm.$setPristine();
                        $scope.addVendorShow = false;
                        $scope.selectVendorShow = true;
                        $scope.newVendor = {};
                        $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                        growlService.growl("Vendor Added Successfully.", 'inverse');
                    } else if (response && response.data && response.data.errorMessage) {
                        growlService.growl(response.data.errorMessage, 'inverse');
                    } else {
                        growlService.growl('Unexpected Error Occurred', 'inverse');
                    }
                });
            }

            //$scope.checkboxModel = {
            //    value1: true
            //};

            $scope.formRequest.checkBoxEmail = true;
            $scope.formRequest.checkBoxSms = true;
            //$scope.postRequestLoding = false;
            $scope.formRequest.urgency = 'High (Will be Closed in 2 Days)';
            $scope.formRequest.deliveryLocation = '';
            $scope.formRequest.paymentTerms = '';
            $scope.formRequest.isSubmit = 0;


            $scope.titleValidation = $scope.attachmentNameValidation = false;
            $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = $scope.incoTermValidation = false;
            $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = false;

            $scope.postRequest = function (isSubmit, pageNo, navigateToView, stage) {

                $scope.titleValidation = $scope.attachmentNameValidation = false;
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = $scope.incoTermValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = false;

                $scope.postRequestLoding = true;
                $scope.formRequest.isSubmit = isSubmit;

                if (isSubmit == 1 || pageNo == 1) {
                    if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                        $scope.titleValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if (isSubmit == 1) {
                        if ($scope.formRequest.isTabular) {
                            $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                                if (!item.attachmentName || item.attachmentName == '') {
                                    $scope.attachmentNameValidation = true;
                                    return false;
                                }
                            })

                        }
                        if (!$scope.formRequest.isTabular) {
                            if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                                $scope.descriptionValidation = true;
                                $scope.postRequestLoding = false;
                                return false;
                            }
                        }
                    }
                }

                if (isSubmit == 1) {
                    if ($scope.generalTC == null || $scope.generalTC == '' || $scope.generalTC == undefined) {
                        $scope.incoTermValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }

                if (isSubmit == 1) {
                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= '') && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.isQuotationPriceLimit == false) {
                        $scope.formRequest.quotationPriceLimit = 0;
                    }
                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }

                if (!$scope.postRequestLoding) {
                    return false;
                }


                if (pageNo != 4) {
                    $scope.textMessage = "Save as Draft.";
                }

                let isValidVendorCurrency = true;
                let inValidVendorCurrencyValue = '';
                $scope.formRequest.auctionVendors.forEach(function (vendor, itemIndexs) {
                    if (vendor && vendor.vendorCurrency) {
                        let selecVendorCurrencyTemp = _.filter($scope.companyCurrencies, function (currItem) { return currItem.currency === vendor.vendorCurrency; });
                        if (selecVendorCurrencyTemp && selecVendorCurrencyTemp.length > 0) {
                            if (selecVendorCurrencyTemp[0].StartTimeLocal > $scope.currentSystemDateOnly || selecVendorCurrencyTemp[0].EndTimeLocal < $scope.currentSystemDateOnly) {
                                if (!inValidVendorCurrencyValue.includes(vendor.vendorCurrency)) {
                                    inValidVendorCurrencyValue += vendor.vendorCurrency + ',';
                                }
                                if (isValidVendorCurrency) {
                                    isValidVendorCurrency = false;
                                }
                            }
                        }
                    }
                });

                if ($scope.formRequest.checkBoxEmail == true && $scope.formRequest.checkBoxSms == true && pageNo == 4) {
                    $scope.textMessage = "This will send an email invite to all the vendors selected above.";
                }
                else if ($scope.formRequest.checkBoxEmail == true && pageNo == 4) {
                    $scope.textMessage = "This will send an email invite to all the vendors selected above.";
                }
                else if ($scope.formRequest.checkBoxSms == true && pageNo == 4) {
                    //$scope.textMessage = "This will send an SMS invite to all the vendors selected above.";
                    $scope.textMessage = "This will not send any communication to all the vendors selected above.";
                }
                else if (pageNo == 4) {
                    $scope.textMessage = "This will not send an EMAIL the vendors selected above.";
                }

                if ($scope.stateParamsReqID && !$scope.formRequest.deleteQuotations && pageNo == 4 && !$scope.isClone) {
                    $scope.textMessage = "Please select \"Request New Quotations\" to vendors if you modify any details in Requirement item specifications / Quantity. Click \"Cancel\" to select the option.";
                }

                $scope.formRequest.currency = $scope.selectedCurrency.value;
                $scope.formRequest.timeZoneID = 190;
                $scope.postRequestLoding = false;
                $scope.formRequest.isDiscountQuotation = $scope.formRequest.isDiscountQuotation === 3 ? 0 : $scope.formRequest.isDiscountQuotation;
                //This above line is just to know whether it's a spot bidding and would not have any downstream impact

                if (!isValidVendorCurrency) {
                    inValidVendorCurrencyValue = inValidVendorCurrencyValue.replace(/,\s*$/, "");
                    $scope.postRequestLoding = false;
                    swal({
                        title: "Error!",
                        title: "Selected vendor currency (" + inValidVendorCurrencyValue + ") conversion rate has expired, please validate.",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            return false;
                        });
                } else {


                    swal({
                        title: "Are you sure?",
                        text: $scope.textMessage,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function () {
                        $scope.postRequestLoding = true;
                        var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                        var m = moment(ts);
                        var quotationDate = new Date(m);
                        var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                        $scope.formRequest.quotationFreezTime = "/Date(" + milliseconds + "000++530)/";
                        // this post request



                        var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                        var m = moment(ts);
                        var quotationDate = new Date(m);
                        var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                        $scope.formRequest.expStartTime = "/Date(" + milliseconds + "000+0530)/";


                        if ($scope.formRequest.biddingType === 'SPOT') {
                            $scope.formRequest.quotationFreezTime = $scope.formRequest.expStartTime;
                        }

                        $scope.formRequest.requirementID = $stateParams.Id ? $stateParams.Id : -1;
                        $scope.formRequest.customerID = userService.getUserId();
                        $scope.formRequest.customerFirstname = userService.getFirstname();
                        $scope.formRequest.customerLastname = userService.getLastname();
                        $scope.formRequest.isClosed = "NOTSTARTED";
                        $scope.formRequest.prNumbers = $scope.selectedPRNumbers ? $scope.selectedPRNumbers : '';
                        $scope.formRequest.endTime = "";
                        $scope.formRequest.sessionID = userService.getUserToken();
                        $scope.formRequest.subcategories = "";
                        $scope.formRequest.budget = 100000;
                        $scope.formRequest.custCompID = userService.getUserCompanyId();
                        $scope.formRequest.customerCompanyName = userService.getUserCompanyId();
                        $scope.formRequest.generalTC = (($scope.generalTC || {}).configValue || '');

                        //for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        //    $scope.formRequest.subcategories += $scope.sub.selectedSubcategories[i].subcategory + ",";
                        //}
                        var category = [];
                        //category.push($scope.formRequest.category);
                        $scope.formRequest.category = category;


                        /***** Adding Multiple Quantity ******/

                        var multipleQuantites = [];

                        multipleQuantites = $scope.formRequest.listRequirementItems.filter(function (item) {
                            if (item.AddMultipleQuantities && item.AddMultipleQuantities.length > 0 && item.AddMultipleQuantities[0].productSNo === item.productSNo) {
                                return item.AddMultipleQuantities;
                            }
                        });

                        if (multipleQuantites && multipleQuantites.length > 0) {
                            multipleQuantites.forEach(function (multipleQtyItem, multipleQtyIndex) {
                                multipleQtyItem.AddMultipleQuantities.forEach(function (item, index) {
                                    if (item.productQuantity && item.productQuantity > 0) {
                                        var productCount = $scope.formRequest.listRequirementItems.length;
                                        item.productSNo = parseInt(productCount + 1);
                                        $scope.formRequest.listRequirementItems.push(item);
                                    }
                                })
                            })
                        }

                        /***** Adding Multiple Quantity ******/

                        $scope.formRequest.listRequirementItems.forEach(function (item, itemIndexs) {
                            if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                                item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                    subItem.dateCreated = "/Date(1561000200000+0530)/";
                                    subItem.dateModified = "/Date(1561000200000+0530)/";
                                })
                            }
                            if (item.productQuotationTemplate && item.productQuotationTemplate.length > 0) {
                                var filterPriceValidSubItems = item.productQuotationTemplate.filter(function (subItem, subItemIndex) {
                                    return subItem.HAS_PRICE && subItem.IS_VALID;
                                });

                                if (filterPriceValidSubItems && filterPriceValidSubItems.length > 0) {
                                    item.ceilingPrice = 0;
                                    item.ceilingPrice = _.sumBy(filterPriceValidSubItems, 'CEILING_PRICE');
                                }
                            }

                        });

                            $scope.formRequest.cloneID = 0;
                            if ($stateParams.reqObj && $stateParams.reqObj.cloneId && $stateParams.reqObj.cloneId !== "") {
                                $scope.formRequest.cloneID = $stateParams.reqObj.cloneId;
                                $scope.formRequest.auctionVendors = _.filter($scope.formRequest.auctionVendors, function (x) { return x.companyName !== 'PRICE_CAP'; });
                                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    $scope.formRequest.listRequirementItems.forEach(function (item, itemIndexs) {
                                        item.ceilingPrice = 0;
                                        if (item.productQuotationTemplate && item.productQuotationTemplate.length > 0) {
                                            item.productQuotationTemplate.forEach(function (subItem, subItemIndex) {
                                                if (subItem.CEILING_PRICE > 0 && subItem.HAS_PRICE) {
                                                    subItem.CEILING_PRICE = 0;
                                                }
                                            });
                                            item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplate);
                                        }
                                    });
                                }
                            }

                            if (selectedItemIds && selectedItemIds.length > 0) {
                                $scope.formRequest.selectedPRItemIds = selectedItemIds.join(',');
                            }

                            $scope.formRequest.templateId = $scope.selectedTemplate.TEMPLATE_ID;
                            $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                                if (item.isNonCoreProductCategory) {
                                    item.productQuotationTemplateJson = '';
                                }
                            });

                            $scope.formRequest.auctionVendors.forEach(function (vendor, index) {
                                if (vendor.listRequirementItems) {
                                    vendor.listRequirementItems.forEach(function (item, index) {
                                        if (item.isNonCoreProductCategory) {
                                            item.productQuotationTemplateJson = '';
                                        }
                                    });
                                }
                            });
                            PRMForwardBidService.postrequirementdata($scope.formRequest)
                                .then(function (response) {
                                    if (response.objectID != 0) {
                                        if ($scope.selectedProject) {
                                            $scope.selectedProject.sessionID = userService.getUserToken();
                                            $scope.selectedProject.requirement = {};
                                            $scope.selectedProject.requirement.requirementID = response.objectID;
                                            var param = { item: $scope.selectedProject };
                                        }

                                        $scope.saveRequirementSetting(response.objectID, 'TEMPLATE_ID', $scope.selectedTemplate.TEMPLATE_ID);
                                        $scope.SaveReqDepartments(response.objectID);
                                        $scope.saveRequirementCustomFields(response.objectID);
                                        //$scope.SaveReqDeptDesig(response.objectID);

                                        //$scope.DeleteRequirementTerms();
                                        //$scope.SaveRequirementTerms(response.objectID);

                                        if ($scope.selectedQuestionnaire != null && $scope.selectedQuestionnaire != undefined && $scope.selectedQuestionnaire.evalID != undefined) {
                                            techevalService.getquestionnaire($scope.selectedQuestionnaire.evalID, 1)
                                                .then(function (response1) {
                                                    $scope.selectedQuestionnaire = response1;

                                                    if ($scope.isTechEval) {
                                                        $scope.selectedQuestionnaire.reqID = response.objectID;
                                                    }
                                                    else {
                                                        $scope.selectedQuestionnaire.reqID = 0;
                                                    }
                                                    $scope.selectedQuestionnaire.sessionID = userService.getUserToken();
                                                    var params = {
                                                        questionnaire: $scope.selectedQuestionnaire
                                                    }
                                                    techevalService.assignquestionnaire(params)
                                                        .then(function (response) {
                                                        })
                                                })
                                        }
                                        if (stage) {
                                            $state.go('fwd-save-requirement', { Id: response.objectID });
                                        }
                                        swal({
                                            title: "Done!",
                                            text: "Requirement Saved Successfully",
                                            type: "success",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        },
                                            function () {
                                                //$scope.postRequestLoding = false;
                                                //$state.go('fwd-view-requirement');

                                                if (navigateToView && stage == undefined) {
                                                    $state.go('fwd-view-requirement', { 'Id': response.objectID });
                                                } else {
                                                    if ($scope.stateParamsReqID > 0 && stage == undefined) {
                                                        location.reload();
                                                    }
                                                    else {
                                                        if (stage == undefined) {
                                                            //$state.go('form.addnewrequirement', { 'Id': response.objectID });
                                                            $state.go('fwd-save-requirement', { Id: response.objectID });
                                                        }
                                                    }
                                                }
                                            });
                                    }
                                });
                    });
                    //$scope.postRequestLoding = false;
                }
            };


            $scope.ItemFile = '';
            $scope.itemSNo = 1;
            $scope.ItemFileName = '';

            $scope.itemnumber = $scope.formRequest.listRequirementItems.length;

            $scope.requirementItems =
            {
                productSNo: $scope.itemSNo++,
                ItemID: 0,
                productIDorName: '',
                productNo: '',
                productDescription: '',
                productQuantity: 0,
                productBrand: '',
                othersBrands: '',
                isDeleted: 0,
                itemAttachment: '',
                attachmentName: '',
                itemMinReduction: 0,
                splitenabled: 0,
                fromrange: 0,
                torange: 0,
                requiredQuantity: 0,
                productCode: '',
                CategoryId: '',
                productDeliveryDetails: ''
            }

            $scope.formRequest.listRequirementItems.push($scope.requirementItems);
            $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                item.AddMultipleQuantities = [];
            });
            $scope.AddItem = function () {

                //stop adding same item start
                if ($scope.formRequest.isRFP) {
                    
                }
                //stop adding same item end
                $window.scrollBy(0, 50);
                const maxSnoItem = _.maxBy($scope.formRequest.listRequirementItems, 'productSNo');
                $scope.itemnumber = maxSnoItem ? maxSnoItem.productSNo : 1;
                $scope.requirementItems =
                {
                    productSNo: $scope.itemnumber + 1,
                    ItemID: 0,
                    productIDorName: '',
                    productNo: '',
                    productDescription: '',
                    productQuantity: 0,
                    productBrand: '',
                    othersBrands: '',
                    isDeleted: 0,
                    itemAttachment: '',
                    attachmentName: '',
                    itemMinReduction: 0,
                    splitenabled: 0,
                    fromrange: 0,
                    torange: 0,
                    requiredQuantity: 0,
                    productCode: '',
                    productDeliveryDetails: '',
                    isNewQtyItem: true
                }

                $scope.formRequest.listRequirementItems.push($scope.requirementItems);

                //Widget Management
                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                    if (item.productCode) {
                        item.widgetState = 0;
                    }
                    if (!item.AddMultipleQuantities) {
                        item.AddMultipleQuantities = [];
                    }
                });
                //^Widget Management
            };

            $scope.handleWindowState = function (product, state) {
                // if (product.productCode !== '') { //item.productIDorName !== ''
                if (product.productIDorName !== '') { //item.productIDorName !== ''
                    product.widgetState = state;
                } else {
                    swal("Error!", "Item details cannot be empty", "error");
                }
            }

            $scope.AddOtherRequirementItem = function (nonCoreProduct) {
                var item = {};
                item = nonCoreProduct;
                //if (nonCoreProduct) {
                //    var output = $scope.productsList.filter(function (product) {
                //        return (String(product.prodCode).toUpperCase().includes(nonCoreProduct.prodCode.toUpperCase()) == true);
                //    });
                //    if (output) {
                //        nonCoreProduct.doHide = true;
                //        item = output[0];
                //    }
                //}

                if (item) {
                    var index = 0;
                    if (!$scope.formRequest.listRequirementItems[$scope.formRequest.listRequirementItems.length - 1].productCode) {
                        $scope.AddItem();
                        index = $scope.formRequest.listRequirementItems.length - 1;

                    } else {
                        $scope.AddItem();
                        index = $scope.formRequest.listRequirementItems.length - 1;
                    }

                    item.isNonCoreProductCategory = 1;
                    $scope.fillTextbox(item, index, '');
                }
            };

            $scope.deleteItem = function (product, isSubQuantity) {

                $scope.formRequest.deleteQuotations = false;
                $scope.formRequest.disableDeleteQuotations = false;

                if (!$scope.isEdit || $scope.isClone) {
                    if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 1) {
                        $scope.formRequest.listRequirementItems = _.filter($scope.formRequest.listRequirementItems, function (x) { return x.productSNo !== product.productSNo; });

                        if (!isSubQuantity && !$scope.formRequest.isRFP) {
                            $scope.formRequest.listRequirementItems = _.filter($scope.formRequest.listRequirementItems, function (x) { return x.catalogueItemID !== product.catalogueItemID; });
                        }
                    }
                } else {
                    product.isDeleted = 1;
                    if (!isSubQuantity) {
                        var tempItems = _.filter($scope.formRequest.listRequirementItems, function (x) { return x.catalogueItemID === product.catalogueItemID; });
                        if (tempItems && tempItems.length > 0) {
                            tempItems.forEach(function (tempItem, index) {
                                tempItem.isDeleted = 1;
                            });
                        }
                    }
                }

                //Delete Item//
                let isClearQuotesForItem = false;
                isClearQuotesForItem = _.some($scope.formRequest.listRequirementItems, function (reqItem) {
                    return ((reqItem.isNewQtyItem && !reqItem.isDeleted) || (!reqItem.isNewQtyItem && reqItem.isDeleted)) && $scope.reqId && !$scope.isClone && !isSubQuantity
                });
                if (isClearQuotesForItem) {
                    $scope.formRequest.deleteQuotations = true;
                    $scope.formRequest.disableDeleteQuotations = true;
                }
                //Delete Item//

                //Delete Quantity//
                let isClearQuotesForQty = false;
                isClearQuotesForQty = _.some($scope.formRequest.listRequirementItems, function (reqItem) {
                    return ((reqItem.isNewQty && !reqItem.isDeleted) || (!reqItem.isNewQty && reqItem.isDeleted)) && $scope.reqId && !$scope.isClone && isSubQuantity
                });
                if (isClearQuotesForQty) {
                    $scope.formRequest.deleteQuotations = true;
                    $scope.formRequest.disableDeleteQuotations = true;
                }
                //Delete Quantity//

                $scope.nonCoreproductsList.forEach(function (item, index) {
                    if (item.prodNo === product.productNo) {
                        item.doHide = false;
                    }
                });

                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length === 1
                    && $scope.formRequest.listRequirementItems[0].isNonCoreProductCategory === 1) {
                    location.reload();
                }

                $scope.postRequestLoding = false;
            };



            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];
                $scope.totalRequirementItemSize = 0;
                if (id != "itemsAttachment" && $scope.file) {
                    var index = _.indexOf($scope.formRequest.listRequirementItems, _.find($scope.formRequest.listRequirementItems, function (o) { return o.productSNo == id; }));
                    if (index >= 0) {
                        var listItem = $scope.formRequest.listRequirementItems[index];
                        listItem.fileSize = $scope.file.size;

                        $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                            if (item.fileSize) {
                                $scope.totalRequirementItemSize = $scope.totalRequirementItemSize + item.fileSize;
                            }
                        });
                    }
                }

                if (($scope.totalRequirementSize + $scope.totalRequirementItemSize) > $scope.totalAttachmentMaxSize) {
                    swal({
                        title: "Attachment size!",
                        text: "Total Attachments size cannot exceed 6MB",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            return;
                        });
                    return;
                }

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        if (id == "itemsAttachment") {
                            if (ext != "xlsx") {
                                swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
                                return;
                            }
                            var bytearray = new Uint8Array(result);
                            $scope.formRequest.itemsAttachment = $.makeArray(bytearray);
                            if (!$scope.isEdit) {
                                $scope.formRequest.listRequirementItems = [];
                            }

                            $scope.postRequest(0, 1, false);
                        }

                        if (id == "requirementItemsSveAttachment") {
                            if (ext != "xlsx") {
                                swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
                                return;
                            }

                            var bytearray = new Uint8Array(result);
                            $scope.uploadRequirementItemsSaveExcel($.makeArray(bytearray));
                            $scope.file = [];
                            $scope.file.name = '';
                            return;
                        }

                        if (id != "itemsAttachment") {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var index = _.indexOf($scope.formRequest.listRequirementItems, _.find($scope.formRequest.listRequirementItems, function (o) { return o.productSNo == id; }));
                            var obj = $scope.formRequest.listRequirementItems[index];
                            obj.itemAttachment = arrayByte;
                            obj.attachmentName = ItemFileName;
                            $scope.formRequest.listRequirementItems.splice(index, 1, obj);
                        }
                    });
            }

            $scope.pageNo = 1;

            $scope.nextpage = function (pageNo) {
                $scope.tableValidation = false;
                $scope.tableValidationMsg = '';
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = departmentsValidation = $scope.incoTermValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.remindersTimeIntervalValidation = $scope.questionnaireValidation = false;

                $scope.titleValidation = $scope.attachmentNameValidation = false;
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = $scope.expNegotiationTimeValidation = false;


                //stop adding same item start
                if ($scope.formRequest.isRFP) {
                    //if ($scope.formRequest.listRequirementItems.length > 0) {
                    //    var isAddStop = false;
                    //    var coreProducts = _.filter($scope.formRequest.listRequirementItems, function (o) {
                    //        return o.isNonCoreProductCategory != 1;
                    //    });
                    //    if (coreProducts.length > 1) {
                    //        coreProducts.forEach(function (item, itemIdx) {
                    //            if (itemIdx != 0) {
                    //                if (item.productIDorName != '' && coreProducts[itemIdx - 1].productIDorName == item.productIDorName) {
                    //                    coreProducts[itemIdx].productCode = '';
                    //                    coreProducts[itemIdx].productIDorName = '';
                    //                    coreProducts[itemIdx].productNo = '';
                    //                    coreProducts[itemIdx].productQuantity = 0;
                    //                    swal("Error!", "Please make sure that you are adding different item", "error");
                    //                    isAddStop = true;
                    //                    return;
                    //                } else if (item.productCode != '' && coreProducts[itemIdx - 1].productCode == item.productCode) {
                    //                    coreProducts[itemIdx].productCode = '';
                    //                    coreProducts[itemIdx].productIDorName = '';
                    //                    coreProducts[itemIdx].productNo = '';
                    //                    coreProducts[itemIdx].productQuantity = 0;
                    //                    swal("Error!", "Please make sure that you are adding different item", "error");
                    //                    isAddStop = true;
                    //                    return;
                    //                }
                    //                else if (item.productNo != '' && coreProducts[itemIdx - 1].productNo == item.productNo) {
                    //                    coreProducts[itemIdx].productCode = '';
                    //                    coreProducts[itemIdx].productIDorName = '';
                    //                    coreProducts[itemIdx].productNo = '';
                    //                    coreProducts[itemIdx].productQuantity = 0;
                    //                    swal("Error!", "Please make sure that you are adding different item", "error");
                    //                    isAddStop = true;
                    //                    return;
                    //                }
                    //            }


                    //        })
                    //    }
                    //    if (isAddStop) {
                    //        return;
                    //    }
                    //}
                }

                //stop adding same item end

                if (pageNo == 1 || pageNo == 4) {
                    $scope.formRequest.isDiscountQuotationTemp = 0;
                    $scope.formRequest.isDiscountQuotationTemp = $scope.formRequest.isDiscountQuotation;
                    if ($scope.isEdit) {
                        $scope.INCO_TERMS.forEach(function (term, termIdx) {
                            if (term.configValue == $scope.formRequest.generalTC) {
                                $scope.generalTC = term;
                            }
                        })
                    }


                    if ($scope.reqDepartments == null || $scope.reqDepartments == undefined || $scope.reqDepartments.length == 0) {
                        $scope.GetReqDepartments();
                    }

                    if ($scope.currencies == null || $scope.currencies == undefined || $scope.currencies.length == 0) {
                        //$scope.getCurrencies();
                    }

                    if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                        $scope.titleValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    var requirementItems = $scope.formRequest.listRequirementItems.filter(function (item) {
                        return !item.isDeleted;
                    });

                    if (!requirementItems || requirementItems.length <= 0) {
                        swal("Verify", "Cannot create requirement with no items.", "error");
                        return false;
                    }

                    if ($scope.formRequest.isTabular) {
                        $scope.selectedProducts = [];
                        $scope.formRequest.listRequirementItems.forEach(function (item, index) {

                            if ($scope.tableValidation || item.isDeleted) {
                                return false;
                            }

                            var sno = parseInt(index) + 1;

                            if (!$scope.formRequest.isRFP) {
                                if (item.productCode == null || item.productCode == '') {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please enter ' + $scope.prmFieldMappingDetails.PRODUCT_CODE.FIELD_LABEL + ' for item: ' + sno;
                                    return;
                                }
                            }

                            if (item.productIDorName == null || item.productIDorName == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter ' + $scope.prmFieldMappingDetails.PRODUCT_NAME.FIELD_LABEL + ' for item: ' + sno;
                                return;
                            }
                            else {
                                let productId = item.catalogueItemID ? item.catalogueItemID : item.productId;
                                $scope.selectedProducts.push(productId);
                                var productQuotationTemplateFiltered = _.filter(item.productQuotationTemplate, function (o) {
                                    return o.IS_VALID == 1;
                                });
                                if (productQuotationTemplateFiltered != null && productQuotationTemplateFiltered != undefined && productQuotationTemplateFiltered.length > 0) {



                                    item.productQuotationTemplateJson = JSON.stringify(productQuotationTemplateFiltered);
                                } else {
                                    item.productQuotationTemplateJson = '';
                                }


                            }
                            //if (item.productNo == null || item.productNo == '') {
                            //    $scope.tableValidation = true;
                            //}
                            //if (item.productDescription == null || item.productDescription == '') {
                            //    $scope.tableValidation = true;
                            //}
                            if (item.productQuantity <= 0 || item.productQuantity == undefined || item.productQuantity == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter product ' + $scope.prmFieldMappingDetails.PRODUCT_QUANTITY.FIELD_LABEL + ' for item: ' + sno;
                                return;
                            }
                            if (item.productQuantityIn == null || item.productQuantityIn == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter product ' + $scope.prmFieldMappingDetails.PRODUCT_UNITS.FIELD_LABEL + ' for item: ' + sno;
                                return;
                            }
                            if ($scope.formRequest.isSplitEnabled == true) {
                                if (item.fromrange == null || item.fromrange == '' || item.fromrange <= 0) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please enter From Range for item: ' + sno;
                                    return;
                                }
                                if (item.torange == null || item.torange == '' || item.torange <= 0) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please enter To Range for item: ' + sno;
                                    return;
                                }

                                if (parseFloat(item.fromrange) > parseFloat(item.torange)) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please make sure that To Range is higher than From Range for item: ' + sno;
                                    return;
                                }

                                //if (item.requiredQuantity == null || item.requiredQuantity == '' || item.requiredQuantity <= 0) {
                                //    $scope.tableValidation = true;
                                //    $scope.tableValidationMsg = 'Please enter Required Qty for item: ' + sno;
                                //    return;
                                //}


                            }
                        });


                        if ($scope.tableValidation) {
                            return false;
                        }

                    }
                    if (!$scope.formRequest.isTabular) {
                        if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                            $scope.descriptionValidation = true;
                            $scope.postRequestLoding = false;
                            return false;
                        }
                    }


                    var phn = '';
                    if ($scope.userObj && $scope.userObj.phoneNum) {
                        phn = $scope.userObj.phoneNum;
                        $scope.formRequest.phoneNum = phn;
                        $scope.formRequest.contactDetails = phn;
                    }
                    else {
                        $scope.formRequest.phoneNum = '';
                        $scope.formRequest.contactDetails = '';
                    }



                }

                if (pageNo == 2 || pageNo == 4) {
                    if (!$scope.questionnaireList == null || $scope.questionnaireList.length == 0) {
                        //$scope.getQuestionnaireList(); //Enable on Need basis
                    }


                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        var d = new Date();
                        d.setHours(18, 0, 0);
                        d = new moment(d).format("DD-MM-YYYY HH:mm");
                        $scope.formRequest.quotationFreezTime = d;

                    }


                    if ($scope.formRequest.expStartTime == null || $scope.formRequest.expStartTime == '') {
                        var dt = new Date();
                        var tomorrowNoon = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() + 1, 12, 0, 0);
                        tomorrowNoon = new moment(tomorrowNoon).format("DD-MM-YYYY HH:mm");
                        $scope.formRequest.expStartTime = tomorrowNoon;

                    }

                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.formRequest.noOfQuotationReminders = 3;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.formRequest.remindersTimeInterval = 2;
                    }
                    //if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                    //    $scope.deliveryTimeValidation = true;
                    //    $scope.postRequestLoding = false;
                    //    return false;
                    //}
                    //if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                    //    $scope.deliveryLocationValidation = true;
                    //    $scope.postRequestLoding = false;
                    //    return false;
                    //}
                    //if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                    //    $scope.paymentTermsValidation = true;
                    //    $scope.postRequestLoding = false;
                    //    return false;
                    //}

                    if ($scope.generalTC == null || $scope.generalTC == '' || $scope.generalTC == undefined) {
                        $scope.incoTermValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    if ($scope.reqDepartments.length > 0) {

                        $scope.departmentsValidation = true;

                        $scope.reqDepartments.forEach(function (item, index) {
                            if (item.isValid == true)
                                $scope.departmentsValidation = false;
                        });

                        if ($scope.departmentsValidation == true) {
                            return false;
                        };
                    }

                }

                if (pageNo == 3 || pageNo == 4) {
                    $scope.GetRequirementVendorAnalysis();
                    //if ($scope.categories == null || $scope.categories == undefined || $scope.categories.length == 0) {
                    //    $scope.getCategories();
                    //}
                    $scope.getproductvendors();

                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }


                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= 0) && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    var ts = moment($scope.formRequest.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var quotationFreezTime = new Date(m);

                    var ts = moment($scope.formRequest.expStartTime, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var expStartTime = new Date(m);

                    var tsDate = moment($scope.formRequest.expStartTime, "DD-MM-YYYY").valueOf();
                    var mDate = moment(tsDate);
                    var expStartTimeDate = new Date(mDate);

                    auctionsService.getdate()
                        .then(function (GetDateResponse) {
                            var CurrentDateToLocal = userService.toLocalDate(GetDateResponse);
                            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                            var m = moment(ts);
                            var deliveryDate = new Date(m);
                            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";
                            var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));
                            $scope.currentSystemDate = CurrentDate;

                            var CurrentDateOnlyLocal = userService.toLocalDate(GetDateResponse);
                            var ts1 = moment(CurrentDateOnlyLocal, "DD-MM-YYYY").valueOf();
                            var m1 = moment(ts1);
                            var deliveryDate1 = new Date(m1);
                            var milliseconds1 = parseInt(deliveryDate1.getTime() / 1000.0);
                            var CurrentDateOnlyTicks = "/Date(" + milliseconds1 + "000+0530)/";
                            var CurrentDateOnly = moment(new Date(parseInt(CurrentDateOnlyTicks.substr(6))));
                            $scope.currentSystemDateOnly = CurrentDateOnly;

                            if ($scope.formRequest.biddingType === 'SPOT') {
                                quotationFreezTime = new Date(moment(moment(moment(), "DD-MM-YYYY HH:mm").valueOf()));
                            }

                            if (quotationFreezTime < CurrentDate) {
                                $scope.quotationFreezTimeValidation = true;
                                $scope.postRequestLoding = false;
                                return false;
                            }
                            else
                                if (quotationFreezTime >= expStartTime) {
                                    $scope.expNegotiationTimeValidation = true;
                                    $scope.postRequestLoding = false;
                                    return false;
                                }

                            if ($scope.selectedCurrency) {
                                let selecCurrencyTemp = _.filter($scope.companyCurrencies, function (currItem) { return currItem.currency === $scope.selectedCurrency.value; });
                                if (selecCurrencyTemp && selecCurrencyTemp.length > 0) {
                                    if (quotationFreezTime > selecCurrencyTemp[0].StartTimeLocal && expStartTime > selecCurrencyTemp[0].StartTimeLocal && expStartTimeDate <= selecCurrencyTemp[0].EndTimeLocal) {
                                        //(quotationFreezTime > selecCurrencyTemp[0].StartTimeLocal && expStartTime > selecCurrencyTemp[0].StartTimeLocal &&
                                        //    expStartTime <= selecCurrencyTemp[0].EndTimeLocal && expStartTime <= selecCurrencyTemp[0].EndTimeLocal)
                                    } else {

                                        swal({
                                            title: "Error!",
                                            title: "Selected currency conversion rate has expired, please validate.",
                                            type: "error",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        },
                                            function () {
                                                return false;
                                            });

                                        return false;
                                    }
                                }
                            }

                            $scope.pageNo = $scope.pageNo + 1;
                        });



                    if (!($scope.formRequest.isDiscountQuotation == $scope.formRequest.isDiscountQuotationTemp)) {
                        $scope.formRequest.deleteQuotations = true;
                        $scope.formRequest.disableDeleteQuotations = true;
                        $scope.formRequest.disableDeleteQuotationsShow = true;
                        $scope.formRequest.disableDeleteQuotationsMessage = "This will delete all the vendor Quotations because Reduction Type is changed ";
                    } else if (($scope.formRequest.isDiscountQuotation == $scope.formRequest.isDiscountQuotationTemp)) {
                        //$scope.formRequest.deleteQuotations = false;
                        //$scope.formRequest.disableDeleteQuotations = false;
                        //$scope.formRequest.disableDeleteQuotationsShow = false;
                        //$scope.formRequest.disableDeleteQuotationsMessage = "";
                    }

                }

                if (pageNo == 4) {
                    $scope.formRequest.subcategoriesView = '';
                    for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        $scope.formRequest.subcategoriesView += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    }


                    $scope.pageNo = $scope.pageNo + 1;
                }

                if (pageNo != 3) {
                    $scope.pageNo = $scope.pageNo + 1;
                }

                // return $scope.pageNo;
                // location.reload();
            }

            $scope.previouspage = function () {
                $scope.pageNo = $scope.pageNo - 1;
                // location.reload();
                //return $scope.pageNo;
            }

            $scope.gotopage = function (pageNo) {
                $scope.pageNo = pageNo;


                return $scope.pageNo;
            }


            $scope.reqDepartments = [];
            $scope.userDepartments = [];

            $scope.GetReqDepartments = function () {
                $scope.reqDepartments = [];
                PRMForwardBidService.GetReqDepartments(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.reqDepartments = response;

                            $scope.noDepartments = true;

                            $scope.reqDepartments.forEach(function (item, index) {
                                if (item.isValid == true) {
                                    $scope.noDepartments = false;
                                }
                            });

                            if ($scope.reqDepartments && $scope.reqDepartments.length > 0 && $scope.reqDepartments.length === 1) {
                                $scope.reqDepartments[0].isValid = true;
                            }

                        }
                    });
            }

            // $scope.GetReqDepartments();


            $scope.SaveReqDepartments = function (reqID) {

                $scope.reqDepartments.forEach(function (item, index) {
                    item.reqID = reqID;
                    item.userID = userService.getUserId();
                })

                var params = {
                    "listReqDepartments": $scope.reqDepartments,
                    sessionID: userService.getUserToken()
                };
                PRMForwardBidService.SaveReqDepartments(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {

                        }
                    });
            };

            $scope.noDepartments = false;


            $scope.noDepartmentsFunction = function (value) {

                $scope.departmentsValidation = false;

                if (value == 'NA') {
                    $scope.reqDepartments.forEach(function (item, index) {
                        item.isValid = false;
                    });
                }
                else {
                    $scope.reqDepartments.forEach(function (item, index) {
                        if (item.isValid == true)
                            $scope.noDepartments = false;
                    });
                }
            };


            $scope.reqDeptDesig = [];

            $scope.GetReqDeptDesig = function () {
                $scope.reqDeptDesig = [];
                PRMForwardBidService.GetReqDeptDesig(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.reqDeptDesig = response;
                            $scope.noDepartments = true;
                            $scope.reqDeptDesig.forEach(function (item, index) {
                                if (item.isValid == true) {
                                    $scope.noDepartments = false;
                                }
                            });
                        }
                    });
            };

            // $scope.GetReqDeptDesig();


            $scope.SaveReqDeptDesig = function (reqID) {

                $scope.reqDeptDesig.forEach(function (item, index) {
                    item.reqID = reqID;
                    item.userID = userService.getUserId();
                })

                var params = {
                    "listReqDepartments": $scope.reqDeptDesig,
                    sessionID: userService.getUserToken()
                };

                PRMForwardBidService.SaveReqDeptDesig(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    });
            };

            $scope.AssignVendorToCompany = function (vendorPhone, vendorEmail) {

                if ($scope.newVendor.altPhoneNum == undefined) {
                    $scope.newVendor.altPhoneNum = '';
                }
                if ($scope.newVendor.altEmail == undefined) {
                    $scope.newVendor.altEmail = '';
                }

                var params = {
                    userID: userService.getUserId(),
                    vendorPhone: vendorPhone,
                    vendorEmail: vendorEmail,
                    sessionID: userService.getUserToken(),
                    category: $scope.formRequest.category,
                    subCategory: $scope.selectedSubCategoriesList,
                    altPhoneNum: $scope.newVendor.altPhoneNum,
                    altEmail: $scope.newVendor.altEmail
                };

                auctionsService.AssignVendorToCompany(params)
                    .then(function (response) {

                        $scope.vendor = response;

                        if ($scope.vendor.errorMessage == '' || $scope.vendor.errorMessage == 'VENDOR ADDED SUCCESSFULLY') {
                            $scope.formRequest.auctionVendors.push({ vendorName: $scope.vendor.firstName + ' ' + $scope.vendor.lastName, companyName: $scope.vendor.email, vendorID: $scope.vendor.userID });
                            $scope.GetRequirementVendorAnalysis();
                            $scope.newVendor = {};
                            $scope.checkVendorPhoneUniqueResult = false;
                            $scope.checkVendorEmailUniqueResult = false;
                            $scope.checkVendorPanUniqueResult = false;
                            $scope.checkVendorTinUniqueResult = false;
                            $scope.checkVendorStnUniqueResult = false;
                            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                            growlService.growl('Vendor Added Successfully', "success");
                        }
                        else {
                            $scope.newVendor = {};
                            $scope.checkVendorPhoneUniqueResult = false;
                            $scope.checkVendorEmailUniqueResult = false;
                            $scope.checkVendorPanUniqueResult = false;
                            $scope.checkVendorTinUniqueResult = false;
                            $scope.checkVendorStnUniqueResult = false;
                            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                            growlService.growl($scope.vendor.errorMessage, "inverse");
                        }
                    });
            };



            $scope.searchvendorstring = '';

            $scope.searchVendors = function (value) {
                $scope.Vendors = _.filter($scope.VendorsTemp, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                // $scope.vendorsLoaded = true;
                // $scope.Vendors = _.filter($scope.allCompanyVendors, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                // var temp = $scope.formRequest.auctionVendors;
                // $scope.formRequest.auctionVendors.forEach(function (item, index) {
                // $scope.Vendors = _.filter($scope.Vendors, function (vendor) { return vendor.vendorID !== item.vendorID; });
                // });
            }


            $scope.exportItemsToExcel = function () {
                var name = 'REQUIREMENT_SAVE';
                reportingService.downloadTemplate(name, userService.getUserId(), $scope.stateParamsReqID, ($scope.selectedTemplate.TEMPLATE_ID ? $scope.selectedTemplate.TEMPLATE_ID : 0));
            };

            $scope.cancelClick = function () {
                $window.history.back();
            };

            $scope.paymentRadio = false;
            $scope.paymentlist = [];
            $scope.addpaymentvalue = function () {
                var listpaymet =
                {
                    reqTermsID: 0,
                    reqID: $scope.stateParamsReqID,
                    userID: userService.getUserId(),
                    reqTermsDays: 0,
                    reqTermsPercent: 0,
                    reqTermsType: 'PAYMENT',
                    paymentType: '+',
                    isRevised: 0
                };
                $scope.paymentlist.push(listpaymet);
            };

            $scope.delpaymentvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }

                $scope.paymentlist.splice(val, 1);
            };

            $scope.resetpayment = function () {
                $scope.paymentlist = [];
            };




            $scope.deliveryRadio = false;
            $scope.deliveryList = [];

            $scope.adddeliveryvalue = function () {
                var deliveryObj =
                {
                    reqTermsID: 0,
                    reqID: $scope.stateParamsReqID,
                    userID: userService.getUserId(),
                    reqTermsDays: 0,
                    reqTermsPercent: 0,
                    reqTermsType: 'DELIVERY',
                    isRevised: 0
                };
                $scope.deliveryList.push(deliveryObj);
            };

            $scope.deldeliveryvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }
                $scope.deliveryList.splice(val, 1);
            };

            $scope.resetdelivery = function () {
                $scope.deliveryList = [];
            };










            $scope.listRequirementTerms = [];

            $scope.SaveRequirementTerms = function (reqID) {

                $scope.listRequirementTerms = [];

                $scope.deliveryList.forEach(function (item, index) {
                    item.reqID = reqID;
                    $scope.listRequirementTerms.push(item);
                });

                $scope.paymentlist.forEach(function (item, index) {

                    item.reqID = reqID;

                    if (item.paymentType == '-') {
                        item.reqTermsDays = -(item.reqTermsDays);
                    }

                    $scope.listRequirementTerms.push(item);
                });

                var params = {
                    "listRequirementTerms": $scope.listRequirementTerms,
                    sessionID: userService.getUserToken()
                };

                PRMForwardBidService.SaveRequirementTerms(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    });
            };




            $scope.GetRequirementTerms = function () {
                PRMForwardBidService.GetRequirementTerms(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.listRequirementTerms = response;

                            $scope.listRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }

                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });



                        }
                    });
            }

            // $scope.GetRequirementTerms();




            $scope.listTerms = [];

            $scope.DeleteRequirementTerms = function () {

                var params = {
                    "listTerms": $scope.listTerms,
                    sessionID: userService.getUserToken()
                };

                PRMForwardBidService.DeleteRequirementTerms(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {

                        }
                    });
            };




            $scope.paymentTypeFunction = function (type) {
                if (type == 'ADVANCE') {

                }
                if (type == 'POST') {

                }
            }


            $scope.changeIndent = function (val) {
            };

            $scope.formRequest.category = '';

            $scope.goToStage = function (stage, currentStage) {
                $scope.postRequestLoding = true;
                $scope.tableValidation = $scope.validationFailed = false;
                if (stage == 2 && currentStage == 1) {

                }
                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                    if (item.productQuantity <= 0 || item.productQuantity == undefined || item.productQuantity == '') {
                        $scope.tableValidation = true;
                        $scope.validationFailed = true;
                    }
                });
                if ($scope.tableValidation) {
                    $scope.postRequestLoding = false;
                    return false;
                }

                if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                    $scope.titleValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }

                if (currentStage == 2 && stage > currentStage) {
                    if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                        $scope.deliveryLocationValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    //if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                    //    $scope.paymentTermsValidation = true;
                    //    $scope.postRequestLoding = false;
                    //    return false;
                    //}
                    //if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                    //    $scope.deliveryTimeValidation = true;
                    //    $scope.postRequestLoding = false;
                    //    return false;
                    //}
                    if ($scope.generalTC == null || $scope.generalTC == '' || $scope.generalTC == undefined) {
                        $scope.incoTermValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }
                if (currentStage == 3 && stage > currentStage) {
                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= '') && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.isQuotationPriceLimit == false) {
                        $scope.formRequest.quotationPriceLimit = 0;
                    }

                    if ($scope.formRequest.noOfQuotationReminders > 0) {
                        $scope.formRequest.noOfQuotationReminders = parseInt($scope.formRequest.noOfQuotationReminders);
                    }
                    if ($scope.formRequest.remindersTimeInterval > 0) {
                        $scope.formRequest.remindersTimeInterval = parseInt($scope.formRequest.remindersTimeInterval);
                    }


                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }
                if (!$scope.postRequestLoding) {
                    return false;
                } else {
                    $scope.postRequestLoding = true;
                    var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.quotationFreezTime = "/Date(" + milliseconds + "000+0530)/";
                    var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.expStartTime = "/Date(" + milliseconds + "000+0530)/";

                    $scope.formRequest.requirementID = $stateParams.Id ? $stateParams.Id : -1;
                    $scope.formRequest.customerID = userService.getUserId();
                    $scope.formRequest.customerFirstname = userService.getFirstname();
                    $scope.formRequest.customerLastname = userService.getLastname();
                    $scope.formRequest.isClosed = "NOTSTARTED";
                    $scope.formRequest.endTime = "";
                    $scope.formRequest.sessionID = userService.getUserToken();
                    $scope.formRequest.subcategories = "";
                    $scope.formRequest.budget = 100000;
                    $scope.formRequest.custCompID = userService.getUserCompanyId();
                    $scope.formRequest.customerCompanyName = userService.getUserCompanyId();

                    for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        $scope.formRequest.subcategories += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    }

                    var category = [];
                    if ($scope.formRequest.category) {
                        category.push($scope.formRequest.category);
                    }
                    $scope.formRequest.category = category;
                    $scope.formRequest.cloneID = 0;
                        if ($stateParams.reqObj && $stateParams.reqObj.cloneId && $stateParams.reqObj.cloneId !== "") {
                            $scope.formRequest.cloneID = $stateParams.reqObj.cloneId;
                            if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                $scope.formRequest.listRequirementItems.forEach(function (item, itemIndexs) {
                                    item.ceilingPrice = 0;
                                });
                            }
                        }

                        $scope.formRequest.templateId = $scope.selectedTemplate.TEMPLATE_ID;
                        PRMForwardBidService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {
                                    if ($scope.selectedProject) {
                                        $scope.selectedProject.sessionID = userService.getUserToken();
                                        $scope.selectedProject.requirement = {};
                                        $scope.selectedProject.requirement.requirementID = response.objectID;
                                        var param = { item: $scope.selectedProject };
                                    }

                                    $scope.saveRequirementSetting(response.objectID, 'TEMPLATE_ID', $scope.selectedTemplate.TEMPLATE_ID);
                                    $scope.SaveReqDepartments(response.objectID);
                                    //$scope.SaveReqDeptDesig(response.objectID);

                                    //$scope.DeleteRequirementTerms();
                                    // $scope.SaveRequirementTerms(response.objectID);

                                    if ($scope.selectedQuestionnaire != null && $scope.selectedQuestionnaire != undefined && $scope.selectedQuestionnaire.evalID != undefined) {
                                        techevalService.getquestionnaire($scope.selectedQuestionnaire.evalID, 1)
                                            .then(function (response1) {
                                                $scope.selectedQuestionnaire = response1;

                                                if ($scope.isTechEval) {
                                                    $scope.selectedQuestionnaire.reqID = response.objectID;
                                                }
                                                else {
                                                    $scope.selectedQuestionnaire.reqID = 0;
                                                }
                                                $scope.selectedQuestionnaire.sessionID = userService.getUserToken();
                                                var params = {
                                                    questionnaire: $scope.selectedQuestionnaire
                                                }
                                                techevalService.assignquestionnaire(params)
                                                    .then(function (response) {
                                                    })
                                            })
                                    }
                                    if (stage) {
                                        $state.go('save-requirement', { Id: response.objectID });
                                    }
                                }
                            });
                }
                //$scope.postRequest(0, stage - 1, false, stage);                
            }

            $scope.goBack = function (stage, reqID) {
                $state.go('save-requirement', { Id: reqID });
            }

            $scope.removeAttach = function (index) {
                $scope.formRequest.multipleAttachments.splice(index, 1);
            }

            // Pagination For User Products//
            $scope.userProductsPage = 0;
            $scope.userProductsPageSize = 200;
            $scope.userProductsfetchRecordsFrom = $scope.userProductsPage * $scope.userProductsPageSize;
            $scope.userProductstotalCount = 0;
            // Pagination For User Products//

            $scope.productsList = [];
            $scope.nonCoreproductsList = [];
            $scope.getUserProducts = function (IsPaging, searchString, index, fieldType, field) {

                if ($scope.productsList.length <= 0 && !searchString) {
                    if ($scope.nonCoreproductsList && $scope.nonCoreproductsList.length > 0) {
                        $scope.nonCoreproductsList.forEach(function (product, index) {
                            if (!$scope.isEdit) {
                                product.doHide = true;
                                $scope.AddOtherRequirementItem(product);
                            }
                        });
                    }

                } else {
                    let isNameSearch = false;
                    if (fieldType === 'NAME') {
                        isNameSearch = true;
                    }

                    catalogService.getUserProducts($scope.userProductsfetchRecordsFrom, $scope.userProductsPageSize, searchString ? encodeURIComponent(searchString) : "", $scope.prmFieldMappingDetails.IS_SEARCH_FOR_CAS, $scope.prmFieldMappingDetails.IS_SEARCH_FOR_MFCD, isNameSearch).then(function (response) {
                        $scope.productsList = response;
                        $scope.userproductsLoading = false;



                        $scope.fillingProduct($scope.productsList, index, fieldType, searchString, $scope.prmFieldMappingDetails.IS_SEARCH_FOR_CAS, $scope.prmFieldMappingDetails.IS_SEARCH_FOR_MFCD);

                        //$scope.nonCoreproductsList = $scope.productsList.filter(function (product) {
                        //    return product.isCoreProductCategory == 0;
                        //});

                        //re-set values to back;

                    });
                }
            };
            //$scope.getUserProducts(0,'',0,'');

            //$scope.productsList = catalogService.getUserProducts();
            //$scope.nonCoreproductsList = $scope.productsList.filter(function (product) {
            //    return product.isCoreProductCategory == 0;
            //});
            //$scope.getProducts = function () {
            //    catalogService.getUserProducts($scope.compID, userService.getUserId())
            //        .then(function (response) {
            //            $scope.productsList = response;
            //            $scope.nonCoreproductsList = $scope.productsList.filter(function (product) {
            //                return product.isCoreProductCategory == 0;
            //            });
            //            console.log($scope.nonCoreproductsList);
            //        });
            //};

            //$scope.getProducts();

            // in form catalogue ctrl
            $scope.fillTextbox = function (selProd, index, field, field_1) {
                if ($scope.reqId) {
                    $scope.formRequest.deleteQuotations = true;
                    $scope.formRequest.disableDeleteQuotations = true;
                }

                let existingCatalogItemId = 0;
                if ($scope.formRequest.listRequirementItems[index].catalogueItemID) {
                    existingCatalogItemId = $scope.formRequest.listRequirementItems[index].catalogueItemID;
                }

                if (selProd.CONTRACT_VENDOR > 0) {
                    $scope.contractPRItems.push({
                        CONTRACT_VENDOR: selProd.CONTRACT_VENDOR,
                        PRODUCT_ID: selProd.prodId
                    });

                    swal({
                        title: "Pending Contract Items found in PR",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Continue With Requirement",
                        cancelButtonText: "Show Contracts",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                        function (isConfirm) {
                            if (!isConfirm) {
                                $scope.showContractProducts($scope.contractPRItems);
                            }
                        });
                }

                $scope['ItemSelected_' + index] = true;
                $scope.formRequest.listRequirementItems[index].productIDorName = selProd.prodName;
                $scope.formRequest.listRequirementItems[index].catalogueItemID = selProd.prodId;
                $scope.formRequest.listRequirementItems[index].isCoreProductCategory = selProd.isCoreProductCategory;

                $scope.formRequest.listRequirementItems[index].hsnCode = selProd.prodHSNCode;
                $scope.formRequest.listRequirementItems[index].productDescription = selProd.prodDesc;
                $scope.formRequest.listRequirementItems[index].productCode = selProd.prodCode;
                $scope.formRequest.listRequirementItems[index].productNo = selProd.prodNo;
                if ($scope.prmFieldMappingDetails.IS_SEARCH_FOR_CAS) {
                    $scope.formRequest.listRequirementItems[index].productCode = selProd.casNumber;
                }
                if ($scope.prmFieldMappingDetails.IS_SEARCH_FOR_MFCD) {
                    $scope.formRequest.listRequirementItems[index].productNo = selProd.mfcdCode;
                }
                //if (field == 'CAS Number' && (field_1 == 'Product No' || field_1 == 'Material No' || field_1 == 'Product Name' || field_1 == '')) {
                //    $scope.formRequest.listRequirementItems[index].productCode = selProd.casNumber;
                //    $scope.formRequest.listRequirementItems[index].productNo = selProd.prodNo;
                //} else {
                //    if (field == 'CAS Number' || field == 'MFCD Code') {
                //        $scope.formRequest.listRequirementItems[index].productCode = selProd.casNumber;
                //        $scope.formRequest.listRequirementItems[index].productNo = selProd.mfcdCode;
                //    } 
                //    else if (field == 'Product Code' || field == 'Material No' || field == 'Product Name' || field == '') {
                //        $scope.formRequest.listRequirementItems[index].productCode = selProd.prodCode;
                //        $scope.formRequest.listRequirementItems[index].productNo = selProd.prodNo;
                //    }
                //    else if (field != 'CAS Number' || field != 'MFCD Code') {
                //        $scope.formRequest.listRequirementItems[index].productCode = selProd.prodCode;
                //        $scope.formRequest.listRequirementItems[index].productNo = selProd.prodNo;
                //    }
                //}

                $scope.formRequest.listRequirementItems[index].productQuantityIn = selProd.prodQty;
                if (selProd.isNonCoreProductCategory) {
                    $scope.formRequest.listRequirementItems[index].productQuantity = 1;
                }
                $scope.formRequest.listRequirementItems[index].isNonCoreProductCategory = selProd.isNonCoreProductCategory;
                $scope.formRequest.listRequirementItems[index].CategoryId = selProd.categoryId;
                $scope.formRequest.listRequirementItems[index].itemLastPriceArr = [];
                $scope.formRequest.listRequirementItems[index].showTable = false;
                $scope['filterProducts_' + index] = null;
                $scope["filterProducts_Code_" + index] = null;
                $scope["filterProducts_Code1_" + index] = null;

                if (!selProd.isNonCoreProductCategory || selProd.isCoreProductCategory) {
                    $scope.GetProductQuotationTemplate(selProd.prodId, index, true);
                } else {
                    $scope.formRequest.listRequirementItems = _.sortBy($scope.formRequest.listRequirementItems, function (o) {
                        if (o.isNonCoreProductCategory) { return o.isNonCoreProductCategory; } else { return 0; }
                    });
                }

                if (existingCatalogItemId && existingCatalogItemId !== selProd.prodId) {
                    if (!$scope.isEdit) {
                        if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 1) {
                            $scope.formRequest.listRequirementItems = _.filter($scope.formRequest.listRequirementItems, function (x) { return x.catalogueItemID !== existingCatalogItemId; });
                        }
                    } else {
                        var tempItems = _.filter($scope.formRequest.listRequirementItems, function (x) { return x.catalogueItemID === existingCatalogItemId; });
                        if (tempItems && tempItems.length > 0) {
                            tempItems.forEach(function (tempItem, index) {
                                tempItem.isDeleted = 1;
                            });
                        }
                    }
                }
            };

            $scope.autofillProduct = function (prodName, index, fieldType, field) {
                if (fieldType === 'CODEMAIN' || fieldType === 'NAME' || fieldType === 'CODE') {
                    $scope.formRequest.productCodeSearch = (fieldType === 'CODEMAIN');
                    $scope.formRequest.productNameSearch = (fieldType === 'NAME');
                    $scope.formRequest.productNoSearch = (fieldType === 'CODE');
                    $window.onclick = function (event) {
                        $scope.formRequest.productCodeSearch = false;
                        $scope.formRequest.productNameSearch = false;
                        $scope.formRequest.productNoSearch = false;
                        $window.onclick = null;
                        $scope.$apply();
                    };
                }


                $scope['ItemSelected_' + index] = false;
                var output = [];
                if (prodName && prodName != '' && prodName.length > 0) {
                    $scope.searchingUserProducts = angular.lowercase(prodName);
                    $scope.userproductsLoading = true;
                    $scope.getUserProducts(0, $scope.searchingUserProducts, index, fieldType, field);
                }
            }

            $scope.onBlurProduct = function (index) {
                if ($scope['ItemSelected_' + index] == false) {
                    if (!$scope.formRequest.isRFP) {
                        $scope.formRequest.listRequirementItems[index].productIDorName = "";
                        $scope.formRequest.listRequirementItems[index].productNo = "";
                        $scope.formRequest.listRequirementItems[index].productCode = "";
                    }

                }
                //$scope['filterProducts_' + index] = null;
            }

            $scope.formSplitChange = function () {
                var splitenabled = $scope.formRequest.isSplitEnabled;
                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                    item.splitenabled = splitenabled;
                    if (splitenabled) {
                        item.productQuantity = 1;
                        $scope.formRequest.isDiscountQuotation = 0;
                    }
                });
            }

            $scope.onSplitChange = function (index) {


                if ($scope.formRequest.listRequirementItems[index].splitenabled == true) {
                    $scope.formRequest.listRequirementItems[index].productQuantity = 1;
                }
            }


            $scope.fillingProduct = function (output, index, fieldType, searchString) {

                output = $scope.productsList.filter(function (product) {
                    //if(product.isCoreProductCategory === 1) {
                    //    if (fieldType == 'NAME') {
                    //        return (String(product.prodName).toUpperCase().includes(searchString) == true);
                    //    }
                    //    else if (fieldType == 'CODE') {
                    //        return (String(product.prodNo).toUpperCase().includes(searchString) == true);
                    //    } 
                    //    else if (fieldType == 'CODEMAIN') {
                    //        return (String(product.prodCode).toUpperCase().includes(searchString) == true);
                    //    }
                    //}
                    return product.isCoreProductCategory === 1;
                });


                if (fieldType == 'NAME') {
                    $scope["filterProducts_" + index] = output;
                    // $scope.formRequest.listRequirementItems[index].productIDorName = searchString;
                }
                else if (fieldType == 'CODE') {
                    $scope["filterProducts_Code_" + index] = output;
                    // $scope.formRequest.listRequirementItems[index].productNo = searchString;
                }
                else if (fieldType == 'CODEMAIN') {
                    $scope["filterProducts_Code1_" + index] = output;
                    // $scope.formRequest.listRequirementItems[index].productCode = searchString;
                }
            }


            $scope.getCatalogCategories = function () {
                auctionsService.getcatalogCategories(userService.getUserId())
                    .then(function (response) {
                        $scope.companyCatalogueList = response;

                        $scope.catName = $scope.companyCatalogueList[0];
                        $scope.searchCategoryVendors($scope.catName);

                    });
            };

            $scope.searchCategoryVendors = function (str) {
                //var filterText = angular.lowercase(str);
                //$scope.getvendors(filterText);
                $scope.getvendors(str);

            };

            $scope.selectCompanyVendors = [];

            $scope.getproductvendors = function (type, vendorId) {
                if (!type) {
                    type = '';
                    vendorId = 0;
                }
                $scope.vendorsLoaded = false;
                $scope.Vendors = [];

                var params = { 'products': $scope.selectedProducts, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                $http({
                    method: 'POST',
                    url: domain + 'getvendorsbyproducts',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            // filter vendors who has contracts
                            if ($scope.contractPRItems && $scope.contractPRItems.length > 0) {
                                let filteredVendors = _.filter(response.data, function (venObj) {
                                    return _.findIndex($scope.contractPRItems, (item) => { return item.CONTRACT_VENDOR === venObj.vendorID }) < 0;
                                });

                                response.data = filteredVendors;
                            }

                            $scope.Vendors = response.data;

                            if (type === 'REQUIREMENT' && vendorId > 0) {
                                var filteredFromAddVendor = $scope.Vendors.filter(function (allVendors) {
                                    return vendorId === allVendors.vendorID;
                                });
                                if (filteredFromAddVendor && filteredFromAddVendor.length > 0) {
                                    $scope.selectForA(filteredFromAddVendor[0]);
                                }
                            }


                            $scope.selectCompanyVendors = response.data;
                            $scope.vendorsLoaded = true;
                            $scope.formRequest.auctionVendors.forEach(function (actionVendor, index) {
                                var filteredVendor = $scope.Vendors.filter(function (allVendor) {
                                    return actionVendor.vendorID === allVendor.vendorID;
                                });

                                if (filteredVendor && filteredVendor.length > 0) {
                                    filteredVendor[0].isSelected = true;
                                }
                            });

                            var unSelectedVendors = _.filter($scope.Vendors, function (x) { return !x.isSelected; });
                            if (unSelectedVendors && unSelectedVendors.length > 0) {
                                $scope.selectAllVendors = false;
                            }

                            $scope.VendorsTemp = $scope.Vendors;
                            if ($scope.searchvendorstring != '') {
                                $scope.searchVendors($scope.searchvendorstring);
                            } else {
                                $scope.searchVendors('');
                            }
                        }
                        //$scope.getvendorswithoutcategories();
                        $scope.getCatalogCategories();
                    } else {
                    }
                }, function (result) {
                });
                //}

                //$scope.getCatalogCategories();

            };

            $scope.getCurrencies();




            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListTemp = response;
                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule == $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                            }
                        });

                        //if (userService.getUserObj().isSuperUser) {
                        //    $scope.workflowList = $scope.workflowList;
                        //}
                        //else {
                        //    $scope.workflowList = $scope.workflowList.filter(function (item) {
                        //        return item.deptID == userService.getSelectedUserDeptID();

                        //    });
                        //}



                    });
            };
            //form Ctrl
            // $scope.getWorkflows();

            $scope.getItemWorkflow = function () {
                workflowService.getItemWorkflow(0, $stateParams.Id, $scope.WorkflowModule)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {

                                if (track.status == 'APPROVED' || track.status == 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status == 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }



                                if (track.status == 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status == 'PENDING' || track.status == 'HOLD') && count == 0) {
                                    count = count + 1;
                                    $scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status == 'PENDING' || track.status == 'HOLD' || track.status == 'REJECTED') && $scope.currentStep == 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });

            };

            $scope.updateTrack = function (step, status) {

                $scope.commentsError = '';

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = userService.getUserId();

                step.moduleName = $scope.WorkflowModule;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.getItemWorkflow();
                            //location.reload();
                            //     $state.go('list-pr');
                        }
                    })
            };

            $scope.assignWorkflow = function (moduleID) {
                workflowService.assignWorkflow(({ wID: $scope.formRequest.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionID }))
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.isSaveDisable = false;
                        }
                        else {
                            //  $state.go('list-pr');
                        }
                    })
            };

            $scope.IsUserApprover = false;

            $scope.functionResponse = false;

            $scope.IsUserApproverForStage = function (approverID) {
                workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                    .then(function (response) {
                        $scope.IsUserApprover = response;
                    });
            };

            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if (userService.getLocalDeptDesigt().deptId == step.department.deptID && userService.getLocalDeptDesigt().desigId == step.approver.desigID &&
                                (step.status == 'PENDING' || step.status == 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status == 'PENDING' || previousStep.status == 'HOLD' || previousStep.status == 'REJECTED') {
                                disable = true;
                            }
                            else if (userService.getLocalDeptDesigt().deptId == step.department.deptID && userService.getLocalDeptDesigt().desigId == step.approver.desigID &&
                                (step.status == 'PENDING' || step.status == 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                })

                return disable;
            };

            /*region end WORKFLOW*/

            $scope.getPRNumber = function (pr_ID) {
                var params = {
                    "prid": pr_ID,
                    "sessionid": userService.getUserToken()
                };
                PRMPRServices.getprdetails(params)
                    .then(function (response) {
                        $scope.PRDetails = response;
                        if ($scope.PRDetails.PR_ID == pr_ID) {
                            //$scope.formRequest.PR_ID = $scope.PRDetails.PR_ID;
                            $scope.formRequest.PR_NUMBER = $scope.PRDetails.PR_NUMBER;
                        }
                    })
            };

            $scope.prConsolidatedItems = [];
            $scope.fillReqItems = function (pr, type) {
                if (type === 'PR') {
                    $scope.formRequest.PR_ID = pr.PR_ID;
                    if (pr.isSelected) {
                        $scope.GetPRItemsList(pr, type);
                    } else if (!pr.isSelected) {
                        $scope.GetPRItemsList(pr, type);
                    }
                } else {
                    $scope.GetPRItemsList(pr, type);
                }
            };

            $scope.prDeleteItemsArr = [];
            $scope.GetPRItemsList = function (pr, type) {
                if (type === 'PR') {
                    if (pr && pr.PR_ID > 0) {
                        var params = {
                            "prid": pr.PR_ID,
                            "sessionid": userService.getUserToken()
                        }

                        PRMPRServices.getpritemslist(params)
                            .then(function (response) {
                                $scope.PRItemsList = response;

                                //Filter only selected PRs

                                let selectedPRItems = _.filter($scope.PRItemsList, function (prItem) {
                                    return _.findIndex(pr.PRItems, function (prObjItem) {
                                        return prObjItem.isSelected && prObjItem.ITEM_ID === prItem.ITEM_ID;
                                    }) >= 0;
                                });

                                $scope.PRItemsList = selectedPRItems;

                                //Map CAS or MFCD
                                $scope.PRItemsList.forEach(function (prItem, idx) {
                                    let temp = _.filter(pr.PRItems, function (prItem1) {
                                        return prItem1.isSelected && prItem1.ITEM_ID === prItem.ITEM_ID;
                                    });

                                    selectedItemIds.push(prItem.ITEM_ID.toString());

                                    if (temp && temp.length > 0) {
                                        prItem.ITEM_CODE = temp[0].ITEM_CODE;
                                        prItem.ITEM_NUM = temp[0].ITEM_NUM;
                                    }
                                });

                                $scope.prDeleteItemsArr = [];

                                if (!pr.isSelected) {
                                    if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                        $scope.PRItemsList.forEach(function (prItem, prIndex) {
                                            $scope.formRequest.listRequirementItems.forEach(function (rfqItem, rfqIndex) {
                                                if (prItem.PRODUCT_ID == rfqItem.productId) {
                                                    var deductQuantity = rfqItem.productQuantity - prItem.REQUIRED_QUANTITY;
                                                    rfqItem.productQuantity = deductQuantity;
                                                    if (rfqItem.productQuantity <= 0) {
                                                        $scope.prDeleteItemsArr.push(rfqItem);
                                                    }
                                                } else {
                                                    if (prItem.PR_ID == rfqItem.PR_ID) {
                                                        $scope.prDeleteItemsArr.push(rfqItem);
                                                    }
                                                }
                                            });
                                        });

                                        if ($scope.prDeleteItemsArr && $scope.prDeleteItemsArr.length > 0) {
                                            $scope.prDeleteItemsArr.forEach(function (item, index) {
                                                $scope.deleteItem(item);
                                            });
                                        }
                                        return;
                                    }
                                }

                                // First check whether this PR exists in requirement items or not if exists then reduce the quantity and return the item//



                                var validPRItems = $scope.PRItemsList.filter(function (prItem) {
                                    return (!prItem.REQ_ID || prItem.REQ_ID <= 0);
                                });

                                if (!validPRItems || validPRItems.length <= 0) {
                                    swal("Warning!", "All items in PR already linked to existing Requirement.", "error");
                                }

                                $scope.contractPRItems = [];
                                $scope.contractPRItems = $scope.PRItemsList.filter(function (prItem) {
                                    return prItem.CONTRACT_VENDOR > 0;
                                });

                                // filter vendors who has contracts
                                if ($scope.contractPRItems && $scope.contractPRItems.length > 0) {
                                    swal({
                                        title: "Pending Contract Items found in PR",
                                        text: "",
                                        type: "warning",
                                        showCancelButton: true,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Continue With Requirement",
                                        cancelButtonText: "Show Contracts",
                                        closeOnConfirm: true,
                                        closeOnCancel: true
                                    },
                                        function (isConfirm) {
                                            if (!isConfirm) {
                                                $scope.showContractProducts($scope.contractPRItems);
                                            }
                                        });
                                }

                                $scope.PRItemsList.forEach(function (item, index) {
                                    let isServicePRItem = false;
                                    if (!item.REQ_ID || item.REQ_ID <= 0) {
                                        if (item.PR_ITEM_TYPE && item.PR_ITEM_TYPE === 'SERVICE') {
                                            isServicePRItem = true;
                                        }
                                        if (!$scope.formRequest.projectName || $scope.formRequest.projectName.indexOf(pr.PROJECT_TYPE + ';') < 0) {
                                            if (pr.PROJECT_TYPE) {
                                                // $scope.formRequest.projectName = $scope.formRequest.projectName + pr.PROJECT_TYPE + ';';
                                            }
                                        }

                                        if (!$scope.formRequest.deliveryLocation || $scope.formRequest.deliveryLocation.indexOf(pr.PLANT_NAME + ';') < 0) {
                                            if (pr.PLANT_NAME) {
                                                $scope.formRequest.deliveryLocation = $scope.formRequest.deliveryLocation + pr.PLANT_NAME + ';';
                                            }
                                        }

                                        if (pr.REQUISITIONER_EMAIL) {
                                            if (!_.some($scope.formRequest.customerEmails, { 'mail': pr.REQUISITIONER_EMAIL, 'userID': 0, 'status': 1 })) {
                                                if (!$scope.formRequest.customerEmails) {
                                                    $scope.formRequest.customerEmails = [];
                                                }

                                                $scope.formRequest.customerEmails.push({ 'mail': pr.REQUISITIONER_EMAIL, 'userID': 0, 'status': 1 });
                                            }
                                        }

                                        if (item.DEPT_CODE) {
                                            var filterdDept = _.filter($scope.reqDepartments, function (o) {
                                                return o.deptCode == item.DEPT_CODE;
                                            });

                                            if (filterdDept && filterdDept.length > 0) {
                                                filterdDept.isValid = true;
                                            }
                                        }

                                        $scope.requirementItems =
                                        {
                                            productSNo: ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length) ? index + 1 : $scope.formRequest.listRequirementItems.length + 1,
                                            ItemID: 0,
                                            productIDorName: item.ITEM_NAME,
                                            productNo: '',//item.ITEM_NUM,
                                            hsnCode: item.HSN_CODE,
                                            productDescription: ((item.ITEM_DESCRIPTION ? item.ITEM_DESCRIPTION : '') + (item.ITEM_DESC ? ', ' + item.ITEM_DESC : '') + (item.ITEM_TEXT ? ', ' + item.ITEM_TEXT : '') + (item.PROJECT_TYPE ? ', ' + item.PROJECT_TYPE : '')).trim(),
                                            productQuantity: item.REQUIRED_QUANTITY,
                                            productQuantityIn: item.UOM ? item.UOM : item.UNITS,
                                            productBrand: item.BRAND,
                                            othersBrands: '',
                                            isDeleted: 0,
                                            productImageID: item.ATTACHMENTS,
                                            attachmentName: '',
                                            //budgetID: 0,
                                            //bcInfo: '',
                                            productCode: item.ITEM_CODE,
                                            splitenabled: 0,
                                            fromrange: 0,
                                            torange: 0,
                                            requiredQuantity: 0,
                                            I_LLP_DETAILS: "",
                                            itemAttachment: "",
                                            itemMinReduction: 0,
                                            productId: item.PRODUCT_ID,
                                            PR_ID: item.PR_ID,
                                            PR_ITEM_ID: item.ITEM_ID,
                                            PR_QUANTITY: item.REQUIRED_QUANTITY,
                                            isCoreProductCategory: 1,
                                            catalogueItemID: item.PRODUCT_ID,
                                            //productDeliveryDetails: pr.PLANT_NAME + "," + pr.PLANT_LOCATION + " " + "Delivery Details:" + userService.toLocalDate(item.DELIVERY_DATE)
                                            productDeliveryDetails: (pr.PLANT_NAME ? pr.PLANT_NAME : '') + "," + (pr.PLANT_LOCATION ? pr.PLANT_LOCATION : '')

                                        };

                                        if ($scope.requirementItems.productImageID == null || $scope.requirementItems.productImageID == '') {
                                            $scope.requirementItems.productImageID = 0;
                                        } else {
                                            $scope.requirementItems.productImageID == item.ATTACHMENTS;
                                        }

                                        if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                            let existingItem = $scope.formRequest.listRequirementItems.filter(function (currentItem) {
                                                return currentItem.productId == $scope.requirementItems.productId;
                                            });

                                            if (!isServicePRItem && existingItem && existingItem.length > 0) {
                                                existingItem[0].productQuantity += $scope.requirementItems.productQuantity;
                                                existingItem[0].PR_QUANTITY += item.REQUIRED_QUANTITY;
                                                existingItem[0].PR_ID = existingItem[0].PR_ID + ',' + item.PR_ID;
                                                existingItem[0].PR_ITEM_ID = existingItem[0].PR_ITEM_ID + ',' + item.ITEM_ID;
                                            } else {
                                                $scope.formRequest.listRequirementItems.push($scope.requirementItems);
                                            }
                                        } else {
                                            $scope.formRequest.listRequirementItems.push($scope.requirementItems);
                                        }


                                        //Correcting SNO (because Each and Every Time It is Filling with PR Items List)//

                                        if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                            $scope.formRequest.listRequirementItems.forEach(function (snoItem, snoIndex) {
                                                snoItem.productSNo = snoIndex + 1;
                                            })
                                        }

                                        //Correcting SNO (because Each and Every Time It is Filling with PR Items List)//

                                        $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                                            var selProd = $scope.productsList.filter(function (prod) {
                                                //return prod.prodCode == item.productIDorName;//prodName
                                                return prod.prodCode == item.productCode;
                                            });

                                            if (selProd && selProd != null && selProd.length > 0) {
                                                $scope.autofillProduct(item.productIDorName, index)
                                                $scope.fillTextbox(selProd[0], index, '');
                                            }

                                            $scope.GetProductQuotationTemplate($scope.requirementItems.productId, $scope.requirementItems.productSNo - 1, true); // Assign template based sub items
                                        });
                                    }
                                });

                            });

                        console.log("ids start");
                        console.log(selectedItemIds);
                        console.log("ids end");
                        $scope.addNoneCoreItems();
                    }
                } else {
                    if (pr && pr.length > 0) {
                        $scope.PRItemsList = pr;

                        $scope.formRequest.listRequirementItems = [];

                        $scope.PRItemsList.forEach(function (item, index) {
                            if (!item.REQ_ID || item.REQ_ID <= 0) {
                                let currentPRItemsIds = [];
                                let plants = '';
                                let plansArr = [];
                                if (item.ItemPRS) {
                                    item.ItemPRS.forEach(function (item, index) {
                                        if (!plansArr.includes(item.PLANT_NAME)) {
                                            plansArr.push(item.PLANT_NAME);
                                        }
                                        if (item.REFERRED_PR_ITEM_IDS) {
                                            selectedItemIds.push(item.REFERRED_PR_ITEM_IDS.toString());
                                            currentPRItemsIds.push(item.REFERRED_PR_ITEM_IDS.toString());
                                        }
                                    });

                                    plants = plansArr && plansArr.length > 0 ? plansArr.join() : '';
                                }

                                $scope.requirementItems =
                                {
                                    productSNo: index + 1,
                                    ItemID: 0,
                                    productIDorName: item.ITEM_NAME,
                                    productNo: '',//item.ITEM_NUM,
                                    hsnCode: item.HSN_CODE,
                                    productDescription: ((item.ITEM_DESCRIPTION ? item.ITEM_DESCRIPTION : '') + (item.ITEM_DESC ? ', ' + item.ITEM_DESC : '') + (item.ITEM_TEXT ? ', ' + item.ITEM_TEXT : '') + (item.PR_NOTE ? ', ' + item.PR_NOTE : '') + (item.PROJECT_TYPE ? ', ' + item.PROJECT_TYPE : '')).trim(),
                                    productQuantity: item.SELECTED_QUANTITY,
                                    productQuantityIn: item.UNITS ? item.UNITS : item.UOM,
                                    productBrand: item.BRAND,
                                    othersBrands: '',
                                    isDeleted: 0,
                                    productImageID: item.ATTACHMENTS,
                                    attachmentName: '',
                                    //budgetID: 0,
                                    //bcInfo: '',
                                    productCode: item.ITEM_CODE,
                                    splitenabled: 0,
                                    fromrange: 0,
                                    torange: 0,
                                    requiredQuantity: 0,
                                    I_LLP_DETAILS: "",
                                    itemAttachment: "",
                                    itemMinReduction: 0,
                                    productId: item.PRODUCT_ID,
                                    PR_ID: item.PR_ID,
                                    PR_ITEM_ID: _(currentPRItemsIds).join(),
                                    PR_QUANTITY: item.SELECTED_QUANTITY,
                                    isCoreProductCategory: 1,
                                    catalogueItemID: item.PRODUCT_ID,
                                    productDeliveryDetails: plants
                                    //,productDeliveryDetails: pr.PLANT + "Delivery Details:" + userService.toLocalDate(item.DELIVERY_DATE)
                                };

                                if ($scope.requirementItems.productImageID == null || $scope.requirementItems.productImageID == '') {
                                    $scope.requirementItems.productImageID = 0;
                                } else {
                                    $scope.requirementItems.productImageID == item.ATTACHMENTS;
                                }

                                $scope.formRequest.listRequirementItems.push($scope.requirementItems);

                                //Correcting SNO (because Each and Every Time It is Filling with PR Items List)//

                                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    $scope.formRequest.listRequirementItems.forEach(function (snoItem, snoIndex) {
                                        snoItem.productSNo = snoIndex + 1;
                                    });

                                    $scope.GetProductQuotationTemplate($scope.requirementItems.productId, $scope.requirementItems.productSNo - 1, true); // Assign template based sub items
                                }
                            }
                        });

                        $scope.formRequest.PR_ID = _($scope.PRItemsList).map('PR_ID').value().join();
                        $scope.addNoneCoreItems();
                    }
                }
            };


            $scope.addNoneCoreItems = function () {
                if ($scope.nonCoreproductsList && $scope.nonCoreproductsList.length > 0) {
                    $scope.nonCoreproductsList.forEach(function (product, index) {
                        if (!$scope.isEdit) {
                            product.doHide = true;
                            $scope.AddOtherRequirementItem(product);
                        }
                    });
                }
            };

            function assignTemplateSubItems(productId, index) {
                var currentItem = $scope.formRequest.listRequirementItems[index];
                if (!currentItem.productQuotationTemplate || currentItem.productQuotationTemplate.length <= 0) {
                    currentItem.productQuotationTemplate = [];
                }

                $scope.templateSubItems.forEach(function (subItem, index) {
                    let temp = currentItem.productQuotationTemplate.filter(function (template) {
                        return (template.T_ID === subItem.T_ID || template.NAME === subItem.NAME);
                    });

                    if (!temp || temp.length <= 0) {
                        subItem.PRODUCT_ID = productId;
                        currentItem.productQuotationTemplate.push(subItem);
                    }
                });
            };

            $scope.GetProductQuotationTemplate = function (productId, index, productChange) {
                var params = {
                    "catitemid": productId,
                    "sessionid": userService.getUserToken()
                };

                console.log(index);
                var currentItem = $scope.formRequest.listRequirementItems[index];
                console.log(currentItem);
                if (currentItem && !currentItem.isNonCoreProductCategory) {
                    catalogService.GetProductQuotationTemplate(params)
                        .then(function (response) {
                            console.log(response)
                            if (response) {
                                response.forEach(function (templateItem, itemIndexs) {
                                    if (templateItem.HAS_SPECIFICATION == 1) {
                                        templateItem.IS_MANDATE_TO_VENDOR = 1;
                                    }
                                });
                                currentItem.productQuotationTemplateMaster = response;
                                if (!currentItem.productQuotationTemplate || currentItem.productQuotationTemplate.length <= 0 ||
                                    !$scope.isEdit) {
                                    currentItem.productQuotationTemplate = response;
                                    if ($scope.isEdit) {
                                        currentItem.productQuotationTemplate.forEach(function (templateItem, itemIndexs) {
                                            templateItem.IS_VALID = 0;
                                        });
                                    }
                                }
                                else if (currentItem.productQuotationTemplateMaster && currentItem.productQuotationTemplateMaster.length > 0) {
                                    if (productChange) {
                                        currentItem.productQuotationTemplate = currentItem.productQuotationTemplateMaster;
                                    } else {
                                        currentItem.productQuotationTemplateMaster.forEach(function (templateItem, itemIndexs) {
                                            if (templateItem && templateItem.NAME) {
                                                var filterResult = currentItem.productQuotationTemplateArray.filter(function (template) {
                                                    return (template.NAME === templateItem.NAME);
                                                });

                                                if (!filterResult || filterResult.length <= 0) {
                                                    templateItem.IS_VALID = 0;
                                                    currentItem.productQuotationTemplate.unshift(templateItem);
                                                }
                                            }
                                        });
                                    }
                                }

                                $scope.formRequest.listRequirementItems = _.sortBy($scope.formRequest.listRequirementItems, function (o) {
                                    if (o.isNonCoreProductCategory) { return o.isNonCoreProductCategory; } else { return 0; }
                                });

                                //Assign Template Sub-Items
                                if (!currentItem.productQuotationTemplate || currentItem.productQuotationTemplate.length <= 0) {
                                    currentItem.productQuotationTemplate = [];
                                }

                                $scope.templateSubItems.forEach(function (subItem, index) {
                                    let temp = currentItem.productQuotationTemplate.filter(function (template) {
                                        return (template.NAME === subItem.NAME);
                                    });

                                    if (!temp || temp.length <= 0) {
                                        subItem.PRODUCT_ID = productId;
                                        let temp = JSON.stringify(subItem);
                                        currentItem.productQuotationTemplate.push(JSON.parse(temp));
                                    }
                                });
                                //^Assign Template Sub-Items
                            }
                        });
                }

            };

            //Below product sub item
            $scope.SaveProductQuotationTemplate = function (obj) {
                var sameNameError = false;
                obj.currentProductQuotationTemplate.forEach(function (item, itemIndex) {
                    console.log(item)
                    if (obj.NAME.toUpperCase() == item.NAME.toUpperCase() && obj.T_ID == 0 && item.IS_VALID !== -1) {
                        sameNameError = true;
                    }
                });

                if (sameNameError) { growlService.growl("Same name Error.", "inverse"); return false; }

                if (obj.HAS_SPECIFICATION) {
                    obj.NAME = obj.NAME.replace(/\'/gi, "");
                    obj.NAME = obj.NAME.replace(/\"/gi, "");
                    obj.NAME = obj.NAME.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    obj.NAME = obj.NAME.replace(/(\r\n|\n|\r)/gm, "");
                    obj.NAME = obj.NAME.replace(/\t/g, '');


                    obj.DESCRIPTION = obj.DESCRIPTION.replace(/\'/gi, "");
                    obj.DESCRIPTION = obj.DESCRIPTION.replace(/\"/gi, "");
                    obj.DESCRIPTION = obj.DESCRIPTION.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    obj.DESCRIPTION = obj.DESCRIPTION.replace(/(\r\n|\n|\r)/gm, "");
                    obj.DESCRIPTION = obj.DESCRIPTION.replace(/\t/g, '');
                }


                var params = {
                    "productquotationtemplate": obj,
                    "sessionid": userService.getUserToken()
                };

                catalogService.SaveProductQuotationTemplate(params)
                    .then(function (response) {
                        if (response) {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.resetTemplateObj();
                            if (obj.PRODUCT_ID > 0 && obj.index >= 0) {
                                $scope.GetProductQuotationTemplate(obj.PRODUCT_ID, obj.index, true);
                            }
                        }
                    });
            };

            $scope.deleteProductQuotationTemplate = function (obj) {
                obj.IS_VALID = -1;
                var params = {
                    "productquotationtemplate": obj,
                    "sessionid": userService.getUserToken()
                };

                catalogService.SaveProductQuotationTemplate(params)
                    .then(function (response) {
                        if (response) {
                            growlService.growl("Deleted Successfully.", "success");
                            $scope.resetTemplateObj();
                            if (obj.PRODUCT_ID > 0 && obj.index >= 0) {
                                $scope.GetProductQuotationTemplate(obj.PRODUCT_ID, obj.index, true);
                            }
                        }
                    });
            };


            $scope.resetTemplateObj = function (product, index) {
                if (!product) {
                    product = { catalogueItemID: 0, productQuotationTemplate: [] };
                }
                if (!index) {
                    index = 0;
                }

                $scope.QuotationTemplateObj = {
                    currentProductQuotationTemplate: product.productQuotationTemplate,
                    index: index,
                    T_ID: 0,
                    PRODUCT_ID: product.catalogueItemID,
                    NAME: '',
                    DESCRIPTION: '',
                    HAS_SPECIFICATION: 0,
                    HAS_PRICE: 0,
                    HAS_QUANTITY: 0,
                    CONSUMPTION: 1,
                    UOM: '',
                    HAS_TAX: 0,
                    IS_VALID: 1,
                    IS_MANDATE_TO_VENDOR: 1,
                    U_ID: userService.getUserId()
                };
            };

            $scope.resetTemplateObj();
            //^Above product sub item

            $scope.loadCustomFields = function () {
                var params = {
                    "compid": userService.getUserCompanyId(),
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.getCustomFieldList(params)
                    .then(function (response) {
                        $scope.customFieldList = response;
                        if ($scope.isEdit) {
                            params = {
                                "compid": userService.getUserCompanyId(),
                                "fieldmodule": 'REQUIREMENT',
                                "moduleid": $stateParams.Id,
                                "sessionid": userService.getUserToken()
                            };

                            PRMCustomFieldService.GetCustomFieldsByModuleId(params)
                                .then(function (response) {
                                    $scope.selectedcustomFieldList = response;
                                    $scope.selectedcustomFieldList.forEach(function (cfItem, index) {
                                        cfItem.IS_SELECTED == 1;

                                        var cfTempField = _.filter($scope.customFieldList, function (field) {
                                            return field.CUST_FIELD_ID == cfItem.CUST_FIELD_ID;
                                        });
                                        if (cfTempField && cfTempField.length > 0) {
                                            cfTempField[0].IS_SELECTED = 1;
                                            cfTempField[0].FIELD_VALUE = cfItem.FIELD_VALUE;
                                            cfTempField[0].MODULE_ID = cfItem.MODULE_ID;
                                        }
                                    });
                                });
                        }
                    });
            };

            //$scope.loadCustomFields();

            $scope.addCustomFieldToFrom = function (field) {
                $scope.selectedcustomFieldList = _.filter($scope.customFieldList, function (field) {
                    return field.IS_SELECTED == 1;
                });
            };

            $scope.validateSubItemQuantity = function (product, productQuotationTemplate) {
                if (!productQuotationTemplate.CONSUMPTION) {
                    productQuotationTemplate.CONSUMPTION = 1;
                } else {
                    product.productQuantity = 1;
                }
            };


            $scope.saveRequirementCustomFields = function (requirementId) {
                if (requirementId) {
                    $scope.selectedcustomFieldList.forEach(function (item, index) {
                        item.MODULE_ID = requirementId;
                        item.ModifiedBy = $scope.userID;
                    });

                    var params = {
                        "details": $scope.selectedcustomFieldList,
                        "sessionid": userService.getUserToken()
                    };

                    PRMCustomFieldService.saveCustomFieldValue(params)
                        .then(function (response) {

                        });
                }
            };

            $scope.GetPRMTemplateFields = function () {
                $scope.prmFieldMappingDetails = {};
                var template = $scope.selectedTemplate && $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : 'PRM-DEFAULT';
                var params = {
                    "templateid": 0,
                    "templatename": template,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        $scope.prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });

                    if ($scope.prmFieldMappingDetails) {
                        var searchForCas, searchForMfcd = false;
                        searchForCas = $scope.prmFieldMappingDetails.PRODUCT_CODE.FIELD_LABEL.toLowerCase().contains("cas number");
                        searchForMfcd = $scope.prmFieldMappingDetails.PRODUCT_NUMBER.FIELD_LABEL.toLowerCase().contains("mfcd code");
                        $scope.prmFieldMappingDetails.IS_SEARCH_FOR_CAS = searchForCas;
                        $scope.prmFieldMappingDetails.IS_SEARCH_FOR_MFCD = searchForMfcd;
                    }

                    //$scope.changeFeilds($scope.prmFieldMappingDetails);

                });

                if ($scope.selectedTemplate.TEMPLATE_ID) {
                    getTemplateSubItems($scope.selectedTemplate.TEMPLATE_ID);
                }
            };



            $scope.changeFeilds = function (data) {
                if (data.PRODUCT_CODE) {
                    if (data.PRODUCT_CODE.FIELD_LABEL == "CAS Number") {
                        data.PRODUCT_CODE.isCasNumber = true;
                    } else {
                        data.PRODUCT_CODE.isCasNumber = false;
                    }
                }
                if (data.PRODUCT_NUMBER) {
                    if (data.PRODUCT_NUMBER.FIELD_LABEL == "MFCD Code") {
                        data.PRODUCT_NUMBER.isMfcdCode = true;
                    } else {
                        data.PRODUCT_NUMBER.isMfcdCode = false;
                    }
                }
            }

            if (!$scope.reqId) {
                $scope.GetPRMTemplateFields();
            }

            $scope.GetPRMTemplates = function (defaultTemplateId) {
                PRMCustomFieldService.GetTemplates().then(function (response) {
                    $scope.prmTemplates = response;
                    if ($scope.prmTemplates && $scope.prmTemplates.length > 0) {
                        if (defaultTemplateId) {
                            let defaultTemplate = _.filter($scope.prmTemplates, function (template) { return template.TEMPLATE_ID === +defaultTemplateId; });
                            if (defaultTemplate && defaultTemplate.length > 0) {
                                $scope.selectedTemplate = defaultTemplate[0];
                            }
                        } else {
                            let defaultTemplate = _.filter($scope.prmTemplates, function (template) { return template.TEMPLATE_NAME === 'PRM-DEFAULT'; });
                            if (defaultTemplate && defaultTemplate.length > 0) {
                                $scope.selectedTemplate = defaultTemplate[0];
                            }
                        }

                        $scope.GetPRMTemplateFields();
                    }
                });
            };

            $scope.searchingCategoryVendors = function (value) {
                if (value) {
                    $scope.VendorsList = _.filter($scope.VendorsTempList1, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                }
            };

            $scope.selectFormCategorySearch = function (vendor) {
                var error = false;
                $scope.formRequest.auctionVendors.forEach(function (SV, SVIndex) {
                    if (SV.vendorID == vendor.vendorID) {
                        error = true;
                        growlService.growl("Vendor already selected", "inverse");

                    }
                });

                if (error == true) {
                    return;
                }

                $scope.Vendors.forEach(function (UV, UVIndex) {
                    if (UV.vendorID == vendor.vendorID) {
                        error = true;
                    }
                });

                if (error == false) {
                    vendor.isFromGlobalSearch = 1;
                    vendor.isSelected = true;
                    $scope.formRequest.auctionVendors.push(vendor);
                    $scope.GetRequirementVendorAnalysis();
                    if ($scope.Vendors && $scope.Vendors.length > 0) {
                        $scope.Vendors = $scope.Vendors.filter(function (x) {
                            return x.vendorID !== vendor.vendorID;
                        });
                    } else {
                        $scope.Vendors = [];
                    }

                    $scope.Vendors.push(vendor);

                    $scope.VendorsList = $scope.VendorsList.filter(function (obj) {
                        return obj.vendorID !== vendor.vendorID;
                    });

                    if ($scope.globalVendors) {
                        $scope.globalVendors = $scope.globalVendors.filter(function (obj) {
                            return obj != vendor;
                        });
                    }

                    $scope.VendorsTemp1 = $scope.VendorsTemp1.filter(function (obj) {
                        return obj != vendor;
                    });

                    $scope.ShowDuplicateVendorsNames.push(vendor);
                }
                else {
                    $scope.selectForA(vendor);
                    vendor.isChecked = false;
                }
            };

            // Category Based Vendors //
            $scope.VendorsList = [];
            $scope.VendorsTempList1 = [];

            $scope.getCategoryVendors = function (str) {
                $scope.ShowDuplicateVendorsNames = [];
                $scope.VendorsList = [];
                $scope.VendorsTempList1 = [];
                $scope.vendorsLoaded = false;
                var category = '';

                category = catObj ? catObj.catCode : '';

                var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                $http({
                    method: 'POST',
                    url: domain + 'getVendorsbyCategories',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.VendorsList = response.data;
                            $scope.VendorsTempList = $scope.VendorsList;
                            $scope.Vendors.forEach(function (item, index) {
                                $scope.VendorsTempList.forEach(function (item1, index1) {
                                    if (item.vendorID == item1.vendorID) {
                                        $scope.ShowDuplicateVendorsNames.push(item1);
                                        $scope.VendorsList.splice(index1, 1);
                                    }
                                })
                            });

                            if ($scope.formRequest.auctionVendors.length > 0) {
                                $scope.formRequest.auctionVendors.forEach(function (item1, index1) {
                                    $scope.VendorsTempList.forEach(function (item2, index2) {
                                        if (item1.vendorID == item2.vendorID) {
                                            $scope.ShowDuplicateVendorsNames.push(item2);
                                            $scope.VendorsList.splice(index2, 1);
                                        }
                                    });
                                });
                            }


                            $scope.VendorsTempList1 = $scope.VendorsList;

                            if ($scope.ShowDuplicateVendorsNames.length > 0) {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                            } else {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                            }

                        } else {
                            $scope.VendorsList = [];
                        }

                        if ($scope.searchCategoryVendorstring != '') {
                            $scope.searchingCategoryVendors($scope.searchCategoryVendorstring);
                        } else {
                            $scope.searchingCategoryVendors('');
                        }


                    } else {

                    }
                }, function (result) {
                });

            }
            // Category Based Vendors //

            $scope.validateLineItemsList = function () {
                if ($scope.nonCoreproductsList && $scope.nonCoreproductsList.length > 0 && $scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                    $scope.nonCoreproductsList.forEach(function (nonCoreProduct, index) {
                        var filterItems = $scope.formRequest.listRequirementItems.filter(function (product) {
                            return product.catalogueItemID === nonCoreProduct.prodId;
                        });

                        if (filterItems && filterItems.length > 0) {
                            nonCoreProduct.doHide = true;
                        }
                    });
                }
            };

            $('.selected-items-box').bind('click', function (e) {
                $('.multiple-selection-dropdown .list').slideToggle('fast');
            });

            $scope.createProduct = function (product, index) {
                $scope.itemProductNameErrorMessage = '';
                $scope.itemProductUnitsErrorMessage = '';


                if (!product.productIDorName || !product.productQuantityIn) {

                    if (!product.productIDorName) {
                        $scope.itemProductNameErrorMessage = 'Please Enter Product Name.';
                    } else if (!product.productQuantityIn) {
                        $scope.itemProductUnitsErrorMessage = 'Please Enter Units.';
                    }
                    return
                }


                $scope.productObj = {
                    prodId: 0,
                    prodCode: $scope.prmFieldMappingDetails.IS_SEARCH_FOR_CAS ? '' : product.productCode,
                    casNumber: $scope.prmFieldMappingDetails.IS_SEARCH_FOR_CAS ? product.productCode : '',
                    compId: userService.getUserCompanyId(),
                    prodName: product.productIDorName,
                    prodNo: $scope.prmFieldMappingDetails.IS_SEARCH_FOR_MFCD ? '' : product.productNo,
                    mfcdCode: $scope.prmFieldMappingDetails.IS_SEARCH_FOR_MFCD ? product.productNo : '',
                    prodQty: product.productQuantityIn,
                    isValid: 1,
                    ModifiedBy: userService.getUserId()
                };

                var params = {
                    reqProduct: $scope.productObj,
                    sessionID: userService.getUserToken()
                }

                catalogService.addproduct(params)
                    .then(function (response) {

                        if (response.repsonseId != -1) {
                            $scope.postRequestLoding = false;
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl("product Added Successfully.", "success");
                                $scope.postRequestLoding = false;
                                $scope.itemProductNameErrorMessage = '';
                                $scope.itemProductUnitsErrorMessage = '';
                                var productID = response.repsonseId;
                                $scope.formRequest.listRequirementItems[index].catalogueItemID = productID;
                                $scope.formRequest.listRequirementItems[index].isNewItem = true;
                                $scope.GetProductQuotationTemplate(productID, index, true);
                            }
                        } else {
                            if (response.errorMessage != '') {
                                $scope.postRequestLoding = true;
                                growlService.growl(response.errorMessage, "inverse");
                            }
                        }


                    });


            };

            $scope.selectAll = function (allVendors) {
                if (allVendors) {
                    $scope.Vendors.forEach(function (vendorItem, vendorIndex) {
                        if (!vendorItem.isSelected) {
                            $scope.selectForA(vendorItem);
                        }
                    });

                    $scope.selectAllVendors = true;
                } else {
                    $scope.formRequest.auctionVendors.forEach(function (vendorItem, vendorIndex) {
                        if (vendorItem.isSelected) {
                            $scope.selectForB(vendorItem);
                        }
                    });

                    $scope.selectAllVendors = false;
                }
            };

            $scope.getSubUserData = function () {
                userService.getSubUsersData({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        response.forEach(function (cust, index) {
                            if ($scope.formRequest.customerEmails.length > 0) {
                                $scope.formRequest.customerEmails.forEach(function (item, itmIndex) {
                                    if (item.userID == cust.userID && item.status == 1) {
                                        cust.isSelected = true;
                                    } else if ((item.userID == cust.userID && item.status == 0) || !cust.isSelected) {
                                        cust.isSelected = false;
                                    }
                                })
                            } else {
                                cust.isSelected = false;
                            }
                        });

                        $scope.subUsers = $filter('filter')(response, { isValid: true });
                    });
            };

            $scope.getSubUserData();

            $scope.customerEmails = [];
            $scope.searchCustomers = function (value) {
                $scope.customerEmails = [];
                if (value) {
                    $scope.customerEmails = _.filter($scope.subUsers, function (item) { return item.email.toUpperCase().indexOf(value.toUpperCase()) > -1; });

                } else {
                    $scope.customerEmails = [];
                }
            }

            $scope.addCustomerEmails = function (mail, id, action) {

                var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (id) {
                    $scope.subUsers.forEach(function (user, index) {
                        if (user.userID == id && user.isSelected == false) {
                            user.isSelected = true;
                            $scope.formRequest.customerEmails.push({ 'mail': mail, 'userID': id, 'status': 1 });
                        } else if (user.userID == id && user.isSelected == true) {
                            user.isSelected = false;
                            $scope.formRequest.customerEmails.forEach(function (cust, custIndex) {
                                if (cust.userID == id) {
                                    cust.status = 0;
                                }
                            })
                        }
                    });

                } else {
                    var intInc = 0;
                    $scope.formRequest.customerEmails.forEach(function (cust, index) {
                        if (cust.mail.trim() == mail.trim() && cust.status == 1 && action == 1) {
                            growlService.growl("email already added", "inverse");
                            intInc++;
                        }
                    });

                    if (action == 1 && intInc == 0) {
                        if (!emailRegex.test(mail)) {
                            growlservice.growl("please enter a valid email", "inverse");
                        } else {
                            $scope.formRequest.customerEmails.push({ 'mail': mail, 'userID': id, 'status': 1 });
                            $scope.searchcustomerstring = "";
                        }
                    } else if (action != 1) {
                        $scope.formRequest.customerEmails.forEach(function (cust, custIndex) {
                            if (cust.mail == mail) {
                                cust.status = 0;
                            }
                        })
                    }
                }

                //var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


                //if (id) {
                //    $scope.subUsers.forEach(function (user, index) {
                //        if (user.userID == id && user.isSelected == false) {
                //            user.isSelected = true;
                //            $scope.formRequest.customerEmails.push({ 'mail': mail, 'userID': id, 'status': 1 });
                //        } else if (user.userID == id && user.isSelected == true) {
                //            user.isSelected = false;
                //            $scope.formRequest.customerEmails.forEach(function (cust, custIndex) {
                //                if (cust.userID == id) {
                //                    cust.status = 0;
                //                }
                //            })
                //        }
                //    });

                //} else {
                //    var intInc = 0;
                //    $scope.formRequest.customerEmails.forEach(function (cust, index) {
                //        if (cust.mail.trim() == mail.trim() && cust.status == 1 && action == 1) {
                //            growlService.growl("email already added", "inverse");
                //            intInc++;
                //        }
                //    });

                //    if (action == 1 && intInc == 0) {
                //        if (!emailRegex.test(mail))
                //        {
                //            growlService.growl("please enter a valid email", "inverse");
                //            return;
                //        }
                //        $scope.formRequest.customerEmails.push({ 'mail': mail, 'userID': id, 'status': 1 });
                //        $scope.searchcustomerstring = "";
                //    } else if (action != 1) {
                //        $scope.formRequest.customerEmails.forEach(function (cust, custIndex) {
                //            if (cust.mail == mail) {
                //                cust.status = 0;
                //            }
                //        })
                //    }
                //}

            }


            $scope.Vendors1 = [];

            $scope.globalVendorsLoading = false;

            $scope.getvendorswithoutcategories = function (str) {
                $scope.vendorsLoaded = false;
                $scope.Vendors1 = [];
                $scope.globalVendorsLoading = true;
                //var category = [];searchVendors
                // category.push($scope.formRequest.category);
                //$scope.formRequest.category = category;
                var params = { 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0, 'sessionID': userService.getUserToken(), 'searchString': str };
                $http({
                    method: 'POST',
                    url: domain + 'getvendorswithoutcategories',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {

                            // filter vendors who has contracts
                            if ($scope.contractPRItems && $scope.contractPRItems.length > 0) {
                                let filteredVendors = _.filter(response.data, function (venObj) {
                                    return _.findIndex($scope.contractPRItems, (item) => { return item.CONTRACT_VENDOR === venObj.vendorID }) < 0;
                                });
                                response.data = filteredVendors;
                            }

                            $scope.Vendors1 = response.data;
                            $scope.vendorsLoaded = true;
                            if ($scope.formRequest.auctionVendors.length > 0) {
                                for (var j in $scope.formRequest.auctionVendors) {
                                    for (var i in $scope.Vendors1) {
                                        if ($scope.Vendors1[i].vendor.phoneNum == $scope.formRequest.auctionVendors[j].vendor.phoneNum) {
                                            $scope.Vendors1.splice(i, 1);
                                        }
                                    }
                                }
                            }
                            if ($scope.Vendors.length > 0) {
                                for (var j in $scope.Vendors) {
                                    for (var i in $scope.Vendors1) {
                                        if ($scope.Vendors1[i].vendor.phoneNum == $scope.Vendors[j].vendor.phoneNum) {
                                            $scope.Vendors1.splice(i, 1);
                                        }
                                    }
                                }
                            }
                            $scope.globalVendorsLoading = false;
                            //console.log($scope.Vendors1)
                            $scope.VendorsTemp1 = $scope.Vendors1;
                            $scope.globalVendors = $scope.VendorsTemp1;
                            //$scope.totalItems = $scope.globalVendors.length;
                            $scope.searchVendorsAllCategory('');

                        } else {
                            $scope.globalVendors = [];
                            $scope.globalVendorsLoading = false;
                        }
                        //$scope.formRequest.auctionVendors =[];
                    } else {
                        //$scope.globalVendors = [];
                        //$scope.globalVendorsLoading = false;
                    }
                }, function (result) {
                });

            };

            $scope.searchGlobalVendors = function (str) {
                if (str == '' || str == null || str == undefined) {
                    $scope.globalVendors = [];

                } else {
                    var strTemp = str;
                    strTemp = String(strTemp).toUpperCase();
                    //$scope.globalVendors = $scope.VendorsTemp1.filter(function (vendor) {
                    //    return (String(vendor.vendorName).toUpperCase().includes(str) == true
                    //        || String(vendor.companyName).toUpperCase().includes(str) == true);
                    //});
                    //$scope.donotFetchVendors();
                    $scope.getvendorswithoutcategories(str);

                    $scope.serchVendorString = str;
                }
            }


            $scope.searchVendorsAllCategory = function (value) {
                $scope.Vendors1 = _.filter($scope.VendorsTemp1, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                // $scope.vendorsLoaded = true;
                // $scope.Vendors = _.filter($scope.allCompanyVendors, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                // var temp = $scope.formRequest.auctionVendors;
                // $scope.formRequest.auctionVendors.forEach(function (item, index) {
                // $scope.Vendors = _.filter($scope.Vendors, function (vendor) { return vendor.vendorID !== item.vendorID; });
                // });
            };

            $scope.clearGobalVendors = function () {
                $scope.globalVendors = [];
                $scope.serchVendorString = "";
            };

            $scope.uploadRequirementItemsSaveExcel = function (fileContent) {
                var params = {
                    reqID: $scope.auctionItem ? $scope.auctionItem.requirementID : 0,
                    isrfp: $scope.formRequest ? $scope.formRequest.isRFP : false,
                    userID: userService.getUserId(),
                    compId: userService.getUserCompanyId(),
                    sessionID: userService.getUserToken(),
                    requirementItemsAttachment: fileContent,
                    templateid: $scope.selectedTemplate.TEMPLATE_ID ? $scope.selectedTemplate.TEMPLATE_ID : 0
                };

                PRMForwardBidService.uploadRequirementItemsSaveExcel(params)
                    .then(function (response) {
                        $("#requirementItemsSveAttachment").val(null);
                        if (response && response.length > 0) {

                            if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                $scope.formRequest.listRequirementItems.forEach(function (reqItem, index) {
                                    if (!reqItem.isNonCoreProductCategory && reqItem.productCode) {
                                        var filteredReqItems = response.filter(function (excelItem) {
                                            return (reqItem.productId == excelItem.catalogueItemID || reqItem.catalogueItemID == excelItem.catalogueItemID);
                                        });

                                        if (filteredReqItems && filteredReqItems.length <= 0) {
                                            reqItem.isDeleted = 1;
                                        }
                                    }
                                });
                            }

                            response.forEach(function (item, index) {
                                let maxSnoItemNo = 1;
                                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    maxSnoItemNo = $scope.formRequest.listRequirementItems.length;
                                    maxSnoItem = _.maxBy($scope.formRequest.listRequirementItems, 'productSNo');
                                    maxSnoItemNo = maxSnoItem ? maxSnoItem.productSNo : $scope.formRequest.listRequirementItems.length;
                                }

                                $scope.itemnumber = maxSnoItemNo;
                                //let itemIndex = $scope.formRequest.listRequirementItems ? $scope.formRequest.listRequirementItems.length : 1;
                                let tempRequirementItem =
                                {
                                    productSNo: $scope.itemnumber + 1,
                                    ItemID: 0,
                                    productIDorName: item.productIDorName,
                                    productNo: item.productNo,
                                    hsnCode: item.hsnCode,
                                    productDescription: item.productDescription,
                                    productQuantity: item.productQuantity,
                                    productQuantityIn: item.productQuantityIn,
                                    productBrand: item.productBrand,
                                    productDeliveryDetails: item.productDeliveryDetails,
                                    othersBrands: '',
                                    isDeleted: 0,
                                    productImageID: 0,
                                    attachmentName: '',
                                    //budgetID: 0,
                                    //bcInfo: '',
                                    productCode: item.productCode,
                                    splitenabled: 0,
                                    fromrange: 0,
                                    torange: 0,
                                    requiredQuantity: 0,
                                    I_LLP_DETAILS: "",
                                    itemAttachment: "",
                                    itemMinReduction: 0,
                                    isCoreProductCategory: 1,
                                    productId: item.catalogueItemID,
                                    catalogueItemID: item.catalogueItemID
                                };

                                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    var existingItem = $scope.formRequest.listRequirementItems.filter(function (currentItem) {
                                        return (currentItem.productId == tempRequirementItem.productId || currentItem.catalogueItemID == tempRequirementItem.productId);
                                    });

                                    if (existingItem && existingItem.length > 0 && !$scope.formRequest.isRFP) {
                                        existingItem[0].productQuantity = tempRequirementItem.productQuantity;
                                        existingItem[0].isDeleted = 0;
                                    } else {
                                        $scope.formRequest.listRequirementItems.push(tempRequirementItem);
                                    }
                                } else {
                                    $scope.formRequest.listRequirementItems.push(tempRequirementItem);
                                }
                            });

                            if ($scope.formRequest && $scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                if (!$scope.formRequest.listRequirementItems[0].productCode) {
                                    $scope.formRequest.listRequirementItems[0].isDeleted = 1;
                                }

                                $scope.formRequest.listRequirementItems = _.sortBy($scope.formRequest.listRequirementItems, function (o) {
                                    if (o.isNonCoreProductCategory) { return o.isNonCoreProductCategory; } else { return 0; }
                                });

                                let index = 0;
                                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                                    if (!item.isDeleted && !item.isNonCoreProductCategory) {
                                        $scope.GetProductQuotationTemplate(item.productId, index, true);
                                    }
                                    index++;
                                });
                            }

                            swal("Verify", "Please verify and Submit the requirement!", "success");
                        }
                    });
            };

            $scope.saveRequirementSetting = function (reqId, setting, settingValue) {
                let param = {
                    requirementSetting: {
                        REQ_ID: reqId,
                        REQ_SETTING: setting, //'TEMPLATE_ID',
                        REQ_SETTING_VALUE: settingValue, //$scope.selectedTemplate.TEMPLATE_ID,
                        USER: $scope.userID,
                        sessionID: $scope.sessionid
                    }
                };

                PRMForwardBidService.saveRequirementSetting(param)
                    .then(function (response) {

                    });
            };

            $scope.getRequirementSettings = function () {
                $scope.requirementSettings = [];
                if ($scope.reqId) {
                    PRMForwardBidService.getRequirementSettings({ "reqid": $scope.reqId, "sessionid": $scope.sessionid })
                        .then(function (response) {
                            $scope.requirementSettings = response;
                            if ($scope.requirementSettings && $scope.requirementSettings.length > 0) {
                                var template = $scope.requirementSettings.filter(function (setting) {
                                    return setting.REQ_SETTING === 'TEMPLATE_ID';
                                });

                                if (template && template.length > 0) {
                                    $scope.formRequest.templateId = +template[0].REQ_SETTING_VALUE;
                                    $scope.GetPRMTemplates(template[0].REQ_SETTING_VALUE);
                                }
                            }
                        });
                } else {
                    let templateId = 0;
                    if ($scope.selectedTemplate && $scope.selectedTemplate) {
                        templateId = $scope.selectedTemplate.TEMPLATE_ID;
                    }

                    $scope.GetPRMTemplates(templateId);
                }
            };

            $scope.getRequirementSettings();

            $scope.showContractProducts = function (contactsProducts) {
                $rootScope.PRPendingContracts = contactsProducts;
                let contractPopUpModalInstance = $modal.open({
                    templateUrl: 'pending-contracts-template-temporary.html',
                    controllerAs: '_modalCtrl',
                    appendTo: 'body',
                    size: 'lg',
                    resolve: {
                        data: {
                            contractProducts: contactsProducts
                        }
                    }
                });

                return contractPopUpModalInstance;
            };

            $scope.AddMultiQty1 = function (product, quantity, index) {
                if ($scope.reqId) {
                    $scope.formRequest.deleteQuotations = true;
                    $scope.formRequest.disableDeleteQuotations = true;
                }

                if (quantity && quantity > 0) {
                    let tempJsonString = JSON.stringify(product);
                    let tempJsonObj = JSON.parse(tempJsonString);
                    tempJsonObj.productSNo = ($scope.formRequest.listRequirementItems.length + 1);
                    tempJsonObj.itemID = 0;
                    tempJsonObj.isNewQty = true;
                    $scope.formRequest.listRequirementItems.push(tempJsonObj);
                } else {
                    swal("Error!", "Quantity Should be greater than zero.", "error");
                }
            };

            $scope.mapSimilarProducts = function (initialProduct, relatedItem) {
                relatedItem.productQuantityIn = initialProduct.productQuantityIn;
                if (relatedItem.similarProduct) {
                    relatedItem.similarProduct.push(initialProduct.productSNo + '-' + relatedItem.productSNo);
                } else {
                    relatedItem.similarProduct = [];
                    relatedItem.similarProduct.push(initialProduct.productSNo + '-' + relatedItem.productSNo);
                }
            };

            $scope.AddMultiQty = function (product, quantity, index) {
                if (quantity && quantity > 0) {
                    var productQuantityObj = {
                        productSNo: product.productSNo,
                        ItemID: 0,
                        productIDorName: product.productIDorName,
                        productNo: product.productNo,
                        productDescription: product.productDescription,
                        productBrand: product.productBrand,
                        othersBrands: product.othersBrands,
                        isDeleted: product.isDeleted,
                        itemAttachment: product.itemAttachment,
                        attachmentName: product.attachmentName,
                        itemMinReduction: product.itemMinReduction,
                        splitenabled: product.splitenabled,
                        fromrange: product.fromrange,
                        torange: product.torange,
                        requiredQuantity: product.requiredQuantity,
                        productCode: product.productCode,
                        hsnCode: product.hsnCode,
                        isNonCoreProductCategory: product.isNonCoreProductCategory,
                        catalogueItemID: product.catalogueItemID,
                        productQuantityIn: product.productQuantityIn,
                        productTempCount: $scope.formRequest.listRequirementItems[index].AddMultipleQuantities.length + 1
                    }
                    productQuantityObj.productQuantity = quantity;
                    $scope.formRequest.listRequirementItems[index].AddMultipleQuantities.push(productQuantityObj);
                } else {
                    swal("Error!", "Quantity Should be greater than zero.", "error");
                }
            };

            $scope.deleteMultiQtyItem = function (product, multiQtySNo, mainProdIdx) {
                if (product.AddMultipleQuantities && product.AddMultipleQuantities.length > 0) {
                    var foundIndex = _.findIndex($scope.formRequest.listRequirementItems[mainProdIdx].AddMultipleQuantities, function (multiQty) { return multiQty.productTempCount === multiQtySNo && multiQty.catalogueItemID === product.catalogueItemID; });
                    if (foundIndex >= 0) {
                        $scope.formRequest.listRequirementItems[mainProdIdx].AddMultipleQuantities.splice(foundIndex, 1);
                    }

                }
            };
            $scope.SelectedCategories = [];

            //$scope.getCategoriesByProducts = function () {

            //    $scope.formRequest.listRequirementItems.forEach(function (item, index) {
            //        if (item.catalogueItemID > 0) {
            //            if (item.CategoryId) {
            //                $scope.SelectedCategories = item.CategoryId.split(",");
            //            }
            //        }
            //    });
            //};

            $scope.GetRequirementVendorAnalysis = function () {
                let requirementItems = $scope.formRequest.listRequirementItems.filter(function (item) {
                    return !item.isNonCoreProductCategory;
                });
                let vendorsList = $scope.formRequest.auctionVendors.filter(function (vendor) {
                    return vendor.isSelected;
                });

                let itemids = _.map(requirementItems, 'catalogueItemID');
                let vendorids = _.map(vendorsList, 'vendorID');

                PRMForwardBidService.getRequirementVendorAnalysis({ "vendorids": vendorids.join(), "itemids": itemids.join(), "sessionid": $scope.sessionid })
                    .then(function (response) {
                        let totalAvgAmount = 0;
                        let totalL1Amount = 0;
                        $scope.requirementAnalysisData = response;
                        if ($scope.requirementAnalysisData && $scope.requirementAnalysisData.length > 0) {
                            let analysisDetails = $scope.requirementAnalysisData.filter(function (detail) {
                                return (detail.key1 !== 'MIN_QUOTATION_TIME' && detail.key1 !== 'MAX_QUOTATION_TIME');
                            });

                            if (analysisDetails && analysisDetails.length > 0) {
                                analysisDetails.forEach(function (detail, index) {
                                    let filteredItems = $scope.formRequest.listRequirementItems.filter(function (item) {
                                        return !item.isNonCoreProductCategory && item.catalogueItemID === detail.key;
                                    });

                                    if (filteredItems && filteredItems.length > 0) {
                                        filteredItems.forEach(function (itemTemp, index) {
                                            totalAvgAmount = totalAvgAmount + (itemTemp.productQuantity * detail.decimalVal);
                                            totalL1Amount = totalL1Amount + (itemTemp.productQuantity * detail.decimalVal1);
                                        });
                                    }
                                });
                            }
                        }

                        $scope.requirementAnalysisData.push({
                            'key': 'QUOTATION_DETAILS',
                            'decimalVal': totalAvgAmount,
                            'decimalVal1': totalL1Amount
                        });
                    });
            };

            $scope.getMomentTime = function (value, units) {
                return moment().add(units, value).format("DD-MMM-YYYY hh:mm:ss");
            };

            $scope.GetReqPRItems = function () {
                $scope.requirementPRStatus.MESSAGE = '';
                $scope.requirementPRStatus.showStatus = false;

                var params = {
                    "reqid": $scope.reqId,
                    "sessionid": userService.getUserToken()
                };

                PRMPRServices.GetPRItemsByReqId(params)
                    .then(function (response) {
                        console.log(response);
                        let inValidItems = _.filter(response, function (prItem) {
                            return prItem.IS_VALID <= 0;
                        });

                        if (inValidItems && inValidItems.length > 0 && false) {
                            swal({
                                title: "Linked PR items are been marked deleted.",
                                text: "",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Continue With Requirement, it will de-link PR items.",
                                cancelButtonText: "Ignore",
                                closeOnConfirm: true,
                                closeOnCancel: true
                            },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        PRMPRServices.DeLinkInValidPRItems(params)
                                            .then(function (response) {
                                                growlService.growl("In-Valid PR Items are de-linked with Requirement.", "success");
                                            });
                                    } else {
                                        $state.go('home');
                                    }
                                });
                        }

                        let modiFiedItems = _.filter(response, function (prItem) {
                            return prItem.IS_MODIFIED > 0;
                        });

                        if (modiFiedItems && modiFiedItems.length > 0) {
                            $scope.requirementPRStatus.MESSAGE = 'Please validate the PRs added to the RFQ, ';
                            $scope.requirementPRStatus.showStatus = true;

                            modiFiedItems.forEach(function (item, itemIndexs) {
                                $scope.requirementPRStatus.MESSAGE = $scope.requirementPRStatus.MESSAGE + '(' + item.PR_NUMBER + ' - ' + item.ITEM_NUM + ' - ' + item.ITEM_NAME + ' modified on ' + userService.toLocalDate(item.MODIFIED_DATE) + '), ';
                            });

                            $scope.requirementPRStatus.MESSAGE = $scope.requirementPRStatus.MESSAGE + ' the requirement last updated time (' + userService.toLocalDate($scope.formRequest.dateModified) + '). Please review the changes and do the needful.';
                        }
                    });
            };

            function getTemplateSubItems(templateId) {
                var params = {
                    "templateid": templateId,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.getTemplateSubItem(params)
                    .then(function (response) {
                        if (response) {
                            $scope.templateSubItems = response;
                            if ($scope.templateSubItems && $scope.templateSubItems.length > 0) {
                                let tempIndex = -1000;
                                $scope.templateSubItems.forEach(function (subItem, index) {
                                    subItem.T_ID = tempIndex - 1;
                                    subItem.PRODUCT_ID = 0;
                                });

                                if ($scope.formRequest && $scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    $scope.formRequest.listRequirementItems.forEach(function (currentItem, itemIndex) {
                                        if (!currentItem.isNonCoreProductCategory && currentItem.isCoreProductCategory && currentItem.isDeleted <= 0 && currentItem.catalogueItemID) {
                                            if (!currentItem.productQuotationTemplate || currentItem.productQuotationTemplate.length <= 0) {
                                                currentItem.productQuotationTemplate = [];
                                            }

                                            currentItem.productQuotationTemplate = _.filter(currentItem.productQuotationTemplate, function (subItem) {
                                                return subItem.T_ID > 0;
                                            });

                                            $scope.templateSubItems.forEach(function (subItem, index) {
                                                let temp = currentItem.productQuotationTemplate.filter(function (template) {
                                                    return (template.NAME === subItem.NAME);
                                                });

                                                if (!temp || temp.length <= 0) {
                                                    subItem.PRODUCT_ID = currentItem.catalogueItemID;
                                                    let temp = JSON.stringify(subItem);
                                                    currentItem.productQuotationTemplate.push(JSON.parse(temp));
                                                }
                                            });
                                        }
                                    });
                                }
                            } else {
                                if ($scope.formRequest && $scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    $scope.formRequest.listRequirementItems.forEach(function (currentItem, itemIndex) {
                                        if (!currentItem.isNonCoreProductCategory && currentItem.isCoreProductCategory && currentItem.isDeleted <= 0 && currentItem.catalogueItemID) {
                                            if (!currentItem.productQuotationTemplate || currentItem.productQuotationTemplate.length <= 0) {
                                                currentItem.productQuotationTemplate = [];
                                            }
                                            currentItem.productQuotationTemplate = _.filter(currentItem.productQuotationTemplate, function (subItem) {
                                                return subItem.T_ID > 0;
                                            });
                                        }
                                    });
                                }
                            }
                        }
                    });
            }

            $scope.isSpot = function (isSpot) {
                if (isSpot) {
                    $scope.formRequest.biddingType = 'SPOT';
                } else {
                    $scope.formRequest.biddingType = 'REVERSE';
                }
            };

            function loadPRData() {
                if ($stateParams.prDetailsList && $stateParams.prDetailsList.length > 0) {
                    $scope.formRequest.PR_ID = $stateParams.prDetailsList[0].PR_ID;
                    $scope.formRequest.PR_NUMBER = $stateParams.prDetailsList[0].PR_NUMBER;
                    if (!$scope.reqId) {
                        $scope.formRequest.listRequirementItems = [];
                    }

                    $stateParams.prDetailsList.forEach(function (prDetail) {
                        $scope.fillReqItems(prDetail, 'PR');
                    });
                } else if ($stateParams.prItemsList && $stateParams.prItemsList.length > 0) {
                    $scope.fillReqItems($stateParams.prItemsList, 'ITEM');
                }
            }

            if (!$scope.reqId) {
                loadPRData();
            }

            $scope.displaySerialNumber = function (product) {
                var arr = $scope.formRequest.listRequirementItems.filter(function (item, index) { return item.isDeleted === 0 });
                var index = _.findIndex(arr, function (reqItem) { return reqItem.catalogueItemID === product.catalogueItemID; });
                return index + 1;
            };

            $scope.paymentTermsDisplay = [];
            $scope.paymentTermsSearch = function (value, type) {

                if (type == 'PAYMENT_TERMS') {
                    if (value != '') {
                        $scope.showSelectedPaymentTrms = true;
                        $scope.paymentTermsDisplay = $scope.companyPaymentTerms.filter(function (item) {
                            return (String(item.configValue.toLowerCase()).includes(value.toLowerCase()) == true);
                        })
                    } else {
                        $scope.showSelectedPaymentTrms = false;
                    }
                }
                //   console.log("array display length>>>>" + $scope.paymentTermsDisplay.length);
            }

            $scope.fillValue = function (value, type) {
                if (type == 'PAYMENT') {
                    $scope.showSelectedPaymentTrms = false;
                    $scope.formRequest.paymentTerms = value;
                }
            }


            $scope.changeQty = function () {
                $scope.formRequest.deleteQuotations = false;
                $scope.formRequest.disableDeleteQuotations = false;
                let isClearQuotes = false;
                isClearQuotes = _.some($scope.formRequest.listRequirementItems, function (reqItem) {
                    return reqItem.productQtyTemp != parseFloat(reqItem.productQuantity) && $scope.reqId && !$scope.isClone
                });

                if (isClearQuotes || ($scope.selectedCurrency.value != $scope.formRequest.currency)) {
                    $scope.formRequest.deleteQuotations = true;
                    $scope.formRequest.disableDeleteQuotations = true;
                }

            };




        }]);prmApp.constant('PRMForwardBidDomain', 'forward-bidding/svc/PRMForwardBid.svc/REST/');
prmApp.constant('signalRFwdHubName', 'fwdRequirementHub');
prmApp.service('PRMForwardBidService', ["PRMForwardBidDomain", "userService", "httpServices", "$window",
    function (PRMForwardBidDomain, userService, httpServices, $window) {


        var PRMForwardBidService = this;

        PRMForwardBidService.getrequirementdata = function (params) {
            let url = PRMForwardBidDomain + 'getrequirementdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        PRMForwardBidService.postrequirementdata = function (params) {
            var myDate = new Date();
            var myEpoch = parseInt(myDate.getTime() / 1000);
            params.minBidAmount = 0;
            params.postedOn = "/Date(" + myEpoch + "000+0000)/";
            params.timeLeft = -1;
            params.price = -1;
            var requirement = {
                "requirement": {
                    "title": params.title,
                    "reqType": params.isRFP ? 2 : 1,
                    "description": params.description,
                    "category": params.category,
                    "subcategories": params.subcategories,
                    "urgency": params.urgency,
                    "budget": params.budget,
                    "attachmentName": params.attachmentName,
                    "deliveryLocation": params.deliveryLocation,
                    "taxes": params.taxes,
                    "paymentTerms": params.paymentTerms,
                    "lotId": params.lotId,
                    "requirementID": params.requirementID,
                    "customerID": params.customerID,
                    "isClosed": params.isClosed,
                    "endTime": null,
                    "sessionID": params.sessionID,
                    "errorMessage": "",
                    "timeLeft": -1,
                    "price": -1,
                    "auctionVendors": params.auctionVendors,
                    "startTime": null,
                    "status": "",
                    "postedOn": "/Date(" + myEpoch + "000+0000)/",
                    "custLastName": params.customerLastname,
                    "custFirstName": params.customerFirstname,
                    "deliveryTime": params.deliveryTime,
                    "includeFreight": params.includeFreight,
                    "inclusiveTax": params.inclusiveTax,
                    "minBidAmount": 0,
                    "checkBoxEmail": params.checkBoxEmail,
                    "checkBoxSms": params.checkBoxSms,
                    "quotationFreezTime": params.quotationFreezTime,
                    "currency": params.currency,
                    "timeZoneID": params.timeZoneID,
                    "listRequirementItems": params.listRequirementItems,
                    "isTabular": params.isTabular,
                    "reqComments": params.reqComments,
                    "itemsAttachment": params.itemsAttachment,
                    "isSubmit": params.isSubmit,
                    "isQuotationPriceLimit": params.isQuotationPriceLimit,
                    "quotationPriceLimit": params.quotationPriceLimit,
                    "noOfQuotationReminders": params.noOfQuotationReminders,
                    "remindersTimeInterval": params.remindersTimeInterval,
                    "custCompID": params.custCompID,
                    "customerEmails": params.customerEmails,
                    "customerCompanyName": params.customerCompanyName,
                    "deleteQuotations": params.deleteQuotations,
                    "indentID": params.indentID,
                    "expStartTime": params.expStartTime,
                    "cloneID": params.cloneID,
                    "isDiscountQuotation": params.isDiscountQuotation,
                    "contactDetails": params.contactDetails,
                    "generalTC": params.generalTC,
                    "isRevUnitDiscountEnable": params.isRevUnitDiscountEnable,
                    "multipleAttachments": params.multipleAttachments,
                    "contractStartTime": params.contractStartTime,
                    "contractEndTime": params.contractEndTime,
                    "isContract": params.isContract,
                    "prCode": params.prCode,
                    "PR_ID": params.PR_ID,
                    "PR_ITEM_IDS": params.selectedPRItemIds,
                    "isTechScoreReq": params.isTechScoreReq,
                    "auditComments": params.auditComments,
                    "biddingType": params.biddingType,
                    "templateId": params.templateId,
                    "projectName": params.projectName,
                    "projectDetails": params.projectDetails,
                    "prNumbers": params.PR_NUMBERS
                }, "attachment": params.attachment
            };
            let url = PRMForwardBidDomain + 'requirementsave';
            return httpServices.post(url, requirement);
        };

        PRMForwardBidService.deleteAttachment = function (Attaachmentparams) {
            let url = PRMForwardBidDomain + 'deleteattachment';
            return httpServices.post(url, Attaachmentparams);
        };

        PRMForwardBidService.getPreviousItemPrice = function (itemDetails) {
            let url = PRMForwardBidDomain + 'itempreviousprice?companyid=' + itemDetails.compID + '&productname=' + itemDetails.productIDorName.trim() + '&productno=' + itemDetails.productNo.trim() + '&brand=' + itemDetails.productBrand.trim() + '&sessionid=' + itemDetails.sessionID;
            return httpServices.get(url);
        };

        PRMForwardBidService.GetLastPrice = function (itemDetails) {
            let url = PRMForwardBidDomain + 'getlastprice?productIDorName=' + itemDetails.productIDorName.trim() + '&companyid=' + itemDetails.compID + '&sessionid=' + itemDetails.sessionID;
            return httpServices.get(url);
        };

        PRMForwardBidService.GetCompanyLeads = function (params) {
            let url = PRMForwardBidDomain + 'getcompanyleads?userid=' + params.userid + '&searchstring=' + params.searchstring + '&searchtype=' + params.searchtype + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.GetReqDepartments = function (userid, reqid, sessionid) {
            let url = PRMForwardBidDomain + 'getreqdepartments?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.SaveReqDepartments = function (params) {
            let url = PRMForwardBidDomain + 'savereqdepartments';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.GetReqDeptDesig = function (userid, reqid, sessionid) {
            var url = PRMForwardBidDomain + 'getreqdeptdesig?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.SaveReqDeptDesig = function (params) {
            var url = PRMForwardBidDomain + 'savereqdeptdesig';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.SaveRequirementTerms = function (params) {
            let url = PRMForwardBidDomain + 'saverequirementterms';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.GetRequirementTerms = function (userid, reqid, sessionID) {
            let url = PRMForwardBidDomain + 'getrequirementterms?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionID;
            return httpServices.get(url);
        };

        PRMForwardBidService.DeleteRequirementTerms = function (params) {
            let url = PRMForwardBidDomain + 'deleterequirementterms';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.saveRequirementSetting = function (params) {
            let url = PRMForwardBidDomain + 'saverequirementsetting';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.getRequirementSettings = function (params) {
            let url = PRMForwardBidDomain + 'requirementsettings?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.getRequirementVendorAnalysis = function (params) {
            let url = PRMForwardBidDomain + 'requirementvendoranalysis?vendorids=' + params.vendorids + '&itemids=' + params.itemids + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.uploadRequirementItemsSaveExcel = function (params) {
            var url = PRMForwardBidDomain + 'uploadrequirementitemssaveexcel';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.getmyAuctions = function (params) {
            if (!params.onlyrfq) {
                params.onlyrfq = 0;
            }

            if (!params.onlyrfp) {
                params.onlyrfp = 0;
            }
            let url = PRMForwardBidDomain + 'getmyauctions?userid=' + params.userid + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate + '&status=' + params.status + '&search=' + params.search + '&allBuyer=' + params.allBuyer + '&page=' + params.page + '&pagesize=' + params.pagesize + '&sessionid=' + params.sessionid + '&onlyrfq=' + params.onlyrfq + '&onlyrfp=' + params.onlyrfp;
            return httpServices.get(url);
        };

        PRMForwardBidService.getrequirementsreport = function (params) {
            var url = PRMForwardBidDomain + 'getrequirementsreport?userid=' + params.userid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.SaveRunningItemPrice = function (params) {
            let url = PRMForwardBidDomain + 'saverunningitemprice';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.GetBidHistory = function (params) {
            let url = PRMForwardBidDomain + 'getbidhistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        PRMForwardBidService.getpricecomparison = function (params) {
            let url = PRMForwardBidDomain + 'getpricecomparison?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.itemwiseselectvendor = function (params) {
            let url = PRMForwardBidDomain + 'itemwiseselectvendor';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.updateStatus = function (params) {
            let url = PRMForwardBidDomain + 'updatestatus';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.uploadquotationsfromexcel = function (params) {
            let url = PRMForwardBidDomain + 'uploadquotationsfromexcel';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.uploadclientsidequotation = function (params) {
            let url = PRMForwardBidDomain + 'uploadclientsidequotation';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.getrevisedquotations = function (params) {
            let url = PRMForwardBidDomain + 'getrevisedquotations';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.updatedeliverdate = function (params) {
            let url = PRMForwardBidDomain + 'updateexpdelandpaydate';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.updatepaymentdate = function (params) {
            let url = PRMForwardBidDomain + 'updateexpdelandpaydate';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.checkForRestartNegotiation = function (params) {
            let url = PRMForwardBidDomain + 'checkForRestartNegotiation';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.SaveReqNegSettings = function (params) {
            let url = PRMForwardBidDomain + 'SaveReqNegSettings';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.makeabid = function (params) {
            let url = PRMForwardBidDomain + 'makeabid';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.revquotationupload = function (params) {
            let url = PRMForwardBidDomain + 'revquotationupload';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.isnegotiationended = function (reqid, sessionid) {
            let url = PRMForwardBidDomain + 'isnegotiationended?reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.DeleteVendorFromAuction = function (params) {
            let url = PRMForwardBidDomain + 'deletevendorfromauction';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.uploadQuotation = function (params) {
            let url = PRMForwardBidDomain + 'uploadquotation';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.UploadRequirementItemsCeilingSave = function (params) {
            let url = PRMForwardBidDomain + 'uploadrequirementitemsceilingsave';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.QuatationAprovel = function (params) {
            let url = PRMForwardBidDomain + 'quatationaprovel';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.GetReportsRequirement = function (params) {
            let url = PRMForwardBidDomain + 'getreportsrequirement?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.UpdatePriceCap = function (params) {
            let url = PRMForwardBidDomain + 'updatepricecap';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.savepricecap = function (params) {
            let url = PRMForwardBidDomain + 'savepricecap';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.EnableMarginFields = function (params) {
            let url = PRMForwardBidDomain + 'enablemarginfields';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.SaveDifferentialFactor = function (params) {
            let url = PRMForwardBidDomain + 'savedifferentialfactor';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.UpdateCBTime = function (params) {
            let url = PRMForwardBidDomain + 'updatecbtime';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.CBStopQuotations = function (params) {
            let url = PRMForwardBidDomain + 'cbstopquotations';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.CBMarkAsComplete = function (params) {
            let url = PRMForwardBidDomain + 'cbmarkascomplete';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.markAsCompleteREQ = function (params) {
            let url = PRMForwardBidDomain + 'markAsCompleteREQ';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.toggleCounterBid = function (params) {
            let url = PRMForwardBidDomain + 'togglecounterbid';
            return httpServices.post(url, params);
        };
        
        PRMForwardBidService.saveRequirementItemList = function (params) {
            let url = PRMForwardBidDomain + 'saverequirementitemlist';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.ItemQuotationApproval = function (params) {
            let url = PRMForwardBidDomain + 'itemquotationapproval';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.refreshRequirementCurrency = function (params) {
            let url = PRMForwardBidDomain + 'refreshrequirementcurrency?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.validateCurrencyRate = function (params) {
            let url = PRMForwardBidDomain + 'validatecurrencyrate?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.SendQuotationApprovalStatusEmail = function (params) {
            let url = PRMForwardBidDomain + 'sendquotationapprovalstatusemail';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.getReductionSettings = function (params) {
            let url = PRMForwardBidDomain + 'getReductionSettings?reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.saveReductionLevelSetting = function (params) {
            let url = PRMForwardBidDomain + 'saveReductionLevelSetting';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.UploadQuotationForSelectVendorCeilingPrices = function (params) {
            let url = PRMForwardBidDomain + 'uploadQuotationForSelectVendorCeilingPrices';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.GetIsAuthorized = function (userid, reqid, sessionid) {
            let url = PRMForwardBidDomain + 'getisauthorized?userid=' + userid + '&reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.StartNegotiation = function (params) {
            var url = PRMForwardBidDomain + 'startNegotiation';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.GetReqData = function (params) {
            let url = PRMForwardBidDomain + 'getreqdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.SavePriceCap = function (params) {
            let url = PRMForwardBidDomain + 'savepricecap';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.getactiveleads = function (params) {
            let url = PRMForwardBidDomain + 'getactiveleads?userid=' + params.userid + '&page=' + params.page + '&limit=' + params.limit + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.getConsolidatedReport = function (fromDate, toDate) {
            let url = PRMForwardBidDomain + 'getconsolidatedreports?from=' + fromDate + '&to=' + toDate + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMForwardBidService.GetReqReportForExcel = function (reqid, sessionid) {
            let url = PRMForwardBidDomain + 'getreqreportforexcel?reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.GetPriceComparisonPreNegotiation = function (params) {
            let url = PRMForwardBidDomain + 'getpricecomparisonprenegotiation?reqid=' + params.reqid + "&userID=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.vendorreminders = function (params) {
            var url = PRMForwardBidDomain + 'vendorreminders';
            return httpServices.post(url, params);
        };

        PRMForwardBidService.GetVendorReminders = function (params) {
            let url = PRMForwardBidDomain + 'getvendorreminders?reqid=' + params.reqid + "&userid=" + params.userid + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.downloadTemplate = function (template, userid, reqid, templateid) {
            if (!templateid) {
                templateid = 0;
            }

            let url = PRMForwardBidDomain + 'gettemplates?template=' + template + '&userid=' + userid + '&reqid=' + reqid + '&compID=' + userService.getUserCompanyId() + '&templateid=' + templateid + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) {
                        console.log("date error");
                    }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }

        PRMForwardBidService.getLiveBiddingReport = function (reqID) {
            let url = PRMForwardBidDomain + 'getlivebiddingreport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMForwardBidService.getLiveBiddingReport = function (reqID) {
            let url = PRMForwardBidDomain + 'getlivebiddingreport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMForwardBidService.GetReqDetails = function (reqID) {
            let url = PRMForwardBidDomain + 'getreqdetails?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMForwardBidService.getItemWiseReport = function (reqID) {
            let url = PRMForwardBidDomain + 'getitemwisereport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMForwardBidService.getDeliveryDateReport = function (reqID) {
            let url = PRMForwardBidDomain + 'deliverytimelinereport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMForwardBidService.getPaymentTermsReport = function (reqID) {
            let url = PRMForwardBidDomain + 'paymenttermsreport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMForwardBidService.GetBidHistory = function (params) {
            let url = PRMForwardBidDomain + 'getbidhistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        PRMForwardBidService.GetCBPricesAudit = function (reqid, vendorid, sessionid) {
            let url = PRMForwardBidDomain + 'getcbpricesaudit?reqid=' + reqid + '&vendorid=' + vendorid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        PRMForwardBidService.GetItemBidHistory = function (params) {
            let url = PRMForwardBidDomain + 'GetItemBidHistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        PRMForwardBidService.getrequirementaudit = function (reqID) {
            let url = PRMForwardBidDomain + 'getrequirementaudit?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        return PRMForwardBidService;

}]);