prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('lotCtrl', ["$state", "$stateParams", "$scope", "auctionsService", "userService", "$http", "$window", "domain", "fileReader", "growlService", "$log", "$filter", "ngDialog", "techevalService", "catalogService", "PRMLotReqService", "SignalRFactory", "signalRHubName",
        function ($state, $stateParams, $scope, auctionsService, userService, $http, $window, domain, fileReader, growlService, $log, $filter, ngDialog, techevalService, catalogService, PRMLotReqService, SignalRFactory, signalRHubName) {
            $scope.stateParamsLotId = $stateParams.Id;
            $scope.compid = userService.getUserCompanyId();
            $scope.currentUserId = +userService.getUserId();
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
            $scope.lotRequirements = {};
            $scope.lotDetails = {
                LotId: 0,
                CompId: userService.getUserCompanyId(),
                LotTitle: '',
                LotDesc: '',
                StartTime: '',
                EndTime: '',
                Duration: 0,
                ProjectId: 0,
                TotalLots: 0,
                Status: ''
            };
            $scope.minDateMoment = moment();

            auctionsService.getdate()
                .then(function (GetDateResponse) {

                    $scope.minDateMoment = userService.toLocalDate(GetDateResponse);
                });

            $scope.parentRequirementDetails = {};

            //$scope.myAuctionStartTime = '';

            $scope.diffDate = function (startTime, endTime) {
                try {
                    var date1 = new moment(startTime);
                    var date2 = new moment(endTime);
                    var duration = date2.diff(date1, 'minutes');

                } catch (err) { }

                return $scope.lotDetails.Duration;
            };

            $scope.goToLotReq = function (requirement) {
                var url = $state.href("view-requirement", { "Id": requirement.requirementID });
                window.open(url, '_blank');
            };

            $scope.markLotAsComplete = function () {
                PRMLotReqService.markLotAsComplete($scope.stateParamsLotId, 0)
                    .then(function (response) {
                        console.log(response);
                        swal("Thanks !", "Requirement Marked as Completed", "success");

                    });
            };

            PRMLotReqService.lotdetails($scope.stateParamsLotId, 0)
                .then(function (response) {
                    $scope.lotDetails = response;
                    $scope.lotDetails.StartTime = new moment(response.StartTime).format("DD-MM-YYYY HH:mm");
                    $scope.lotDetails.EndTime = new moment(response.EndTime).format("DD-MM-YYYY HH:mm");
                    $scope.getData();

                });

            PRMLotReqService.lotRequirements($scope.stateParamsLotId, 0, ($scope.isCustomer ? 0 : $scope.currentUserId))
                .then(function (response) {
                    if (response) {
                        console.log(response);
                        $scope.lotRequirements = response;
                        $scope.lotRequirements.forEach(function (requirement, index) {
                            requirement.startTime = userService.toLocalDate(requirement.startTime);
                            requirement.endTime = userService.toLocalDate(requirement.endTime);
                            if (!index) {
                                $scope.lotRequirements.myAuctionStartTime = requirement.startTime;
                            }
                            //requirement.startTime = new moment(requirement.startTime).format("DD-MM-YYYY HH:mm");
                            //requirement.endTime = new moment(requirement.endTime).format("DD-MM-YYYY HH:mm");
                        });
                    }
                });

            $scope.getData = function (methodName, callerID) {
                auctionsService.getrequirementdata({ "reqid": $scope.lotDetails.ReqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        if (response) {
                            $scope.parentRequirementDetails = response;
                        }
                    });
                //$scope.$broadcast('timer-start');
            };

            $scope.setEndTime = function (value) {
                $scope.NegotiationSettings = $scope.parentRequirementDetails.NegotiationSettings;
                var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
                $scope.days = parseInt(duration[0]);
                duration = duration[1];
                duration = duration.split(":", 4);
                $scope.hours = parseInt(duration[0]);
                $scope.mins = parseInt(duration[1]);

                $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                if (value) {
                    var EndTime = moment(value, "DD-MM-YYYY HH:mm").add(moment.duration($scope.mins, 'minutes'));
                    EndTime = moment(EndTime, "DD-MM-YYYY HH:mm").add(moment.duration($scope.hours, 'hours'));
                    EndTime = moment(EndTime, "DD-MM-YYYY HH:mm").add(moment.duration($scope.days, 'days'));
                }

                return EndTime;
            };

            $scope.updateAuctionStart = function (value) {

                var StartTime = value;
                $scope.lotRequirements.forEach(function (requirement, index) {

                    var EndTime = $scope.setEndTime(StartTime);

                    //requirement.startTime = "/Date(" + userService.toUTCTicks(StartTime) + "+0530)/";
                    //requirement.endTime = "/Date(" + userService.toUTCTicks(EndTime) + "+0530)/";
                    if (!index) {
                        requirement.startTime = moment(StartTime, "DD-MM-YYYY HH:mm").format("DD-MM-YYYY HH:mm");
                    } else {
                        requirement.startTime = StartTime;
                    }

                    requirement.endTime = moment(EndTime).format("DD-MM-YYYY HH:mm");
                    StartTime = EndTime;
                });

                $scope.lotRequirements.forEach(function (obj, index) {
                    obj.startTime = "/Date(" + userService.toUTCTicks(obj.startTime) + "+0530)/";
                    obj.endTime = "/Date(" + userService.toUTCTicks(obj.endTime) + "+0530)/";
                });

                var params = {};
                params.itemids = $scope.lotRequirements;
                params.sessionid = userService.getUserToken();
                params.user = userService.getUserId();

                PRMLotReqService.updateAuctionStart(params)
                    .then(function (response) {

                        if (response.errorMessage != '') {
                            swal("Thanks !", "Auction Time Updated Successfully", "success");
                        } else {
                            location.reload();
                        }
                    });
            };

        }]);