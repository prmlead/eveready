﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data;
using MySql.Data.MySqlClient;
using MySql.Data.Types;
using System.Configuration;
using System.Data;

namespace PRMServices.SQLHelper
{
    public class PRMMySqlHelper
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public String ConnectionString = ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString;
        public MySqlConnection connection;

        #region Initiallize
        public PRMMySqlHelper()
        {
            Initialize();
        }

        //Initialize values
        private void Initialize()
        {
            ConnectionString = ReadConnectionString();
            connection = new MySqlConnection(ConnectionString);
        }

        public String ReadConnectionString()
        {
            return ConnectionString = ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString;
        }

        #endregion


        #region DB ConnectionOpen
        public bool OpenConnection()
        {
            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }
                
                return true;
            }
            catch (MySqlException ex)
            {
                logger.Error(ex, ex.Message);
            }

            return false;
        }
        #endregion

        #region DB Connection Close
        //Close connection
        public bool CloseConnection()
        {
            try
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }

                return true;
            }
            catch (MySqlException ex)
            {
                logger.Error(ex, ex.Message);
                return false;
            }
        }


        #endregion

        #region ExecuteNonQuery for insert/Update and Delete
        //For Insert/Update/Delete
        public int ExecuteNonQuery_IUD(String Querys)
        {
            int result=0;
            try
            {
                if (OpenConnection() == true)
                {
                    MySqlCommand cmd = new MySqlCommand(Querys, connection);
                    result = cmd.ExecuteNonQuery();
                    CloseConnection();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
            finally
            {
                CloseConnection();
            }

            return result;
        }
        #endregion

        #region Dataset for select result and return as Dataset
        //for select result and return as Dataset
        public DataSet DataSet_return(String Querys)
        {
            DataSet ds = new DataSet();
            try
            {
                if (OpenConnection() == true)
                {
                    MySqlCommand cmdSel = new MySqlCommand(Querys, connection);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmdSel);
                    da.Fill(ds);
                    CloseConnection();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
            finally
            {
                CloseConnection();
            }
            return ds;
        }
        #endregion

        #region DataTable for select result and return as DataTable
        //for select result and return as DataTable
        public DataTable DataTable_return(String Querys)
        {
            DataTable dt = new DataTable();
            try
            {
                if (OpenConnection() == true)
                {
                    MySqlCommand cmdSel = new MySqlCommand(Querys, connection);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmdSel);
                    da.Fill(dt);
                    CloseConnection();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
            finally
            {
                CloseConnection();
            }
            return dt;
        }
        #endregion
       
        #region DataTable for select result and return as DataTable
        //for select result and return as DataTable
        public DataSet SP_Dataset_return(String ProcName, int timeout,  params MySqlParameter[] commandParameters)
        {
            DataSet ds = new DataSet();
            try
            {
                if (OpenConnection() == true)
                {
                    MySqlCommand cmdSel = new MySqlCommand(ProcName, connection);
                    cmdSel.CommandTimeout = timeout;
                    cmdSel.CommandType = CommandType.StoredProcedure;
                    AssignParameterValues(commandParameters, commandParameters);
                    AttachParameters(cmdSel, commandParameters);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmdSel);
                    da.Fill(ds);
                    CloseConnection();
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
            finally
            {
                CloseConnection();
            }

            return ds;
        }

        public DataTable SP_DataTable_return(String ProcName, params MySqlParameter[] commandParameters)
        {
            DataSet ds = new DataSet();
            try
            {
                if (OpenConnection() == true)
                {
                    MySqlCommand cmdSel = new MySqlCommand(ProcName, connection);
                    cmdSel.CommandType = CommandType.StoredProcedure;
                    AssignParameterValues(commandParameters, commandParameters);
                    AttachParameters(cmdSel, commandParameters);
                    MySqlDataAdapter da = new MySqlDataAdapter(cmdSel);
                    da.Fill(ds);
                    CloseConnection();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, ex.Message);
            }
            finally
            {
                CloseConnection();
            }
            return ds.Tables[0];
        }

        private static void AttachParameters(MySqlCommand command, MySqlParameter[] commandParameters)
        {
            if (command == null) throw new ArgumentNullException("command");
            if (commandParameters != null)
            {
                foreach (MySqlParameter p in commandParameters)
                {
                    if (p != null)
                    {
                        // Check for derived output value with no value assigned
                        if ((p.Direction == ParameterDirection.InputOutput ||
                            p.Direction == ParameterDirection.Input) &&
                            (p.Value == null))
                        {
                            p.Value = DBNull.Value;
                        }
                        command.Parameters.Add(p);
                    }
                }
            }
        }

        private static void AssignParameterValues(MySqlParameter[] commandParameters, object[] parameterValues)
        {
            if ((commandParameters == null) || (parameterValues == null))
            {
                // Do nothing if we get no data
                return;
            }

            // We must have the same number of values as we pave parameters to put them in
            if (commandParameters.Length != parameterValues.Length)
            {
                throw new ArgumentException("Parameter count does not match Parameter Value count.");
            }

            // Iterate through the SqlParameters, assigning the values from the corresponding position in the 
            // value array
            for (int i = 0, j = commandParameters.Length; i < j; i++)
            {
                // If the current array value derives from IDbDataParameter, then assign its Value property
                if (parameterValues[i] is IDbDataParameter)
                {
                    IDbDataParameter paramInstance = (IDbDataParameter)parameterValues[i];
                    if (paramInstance.Value == null)
                    {
                        commandParameters[i].Value = DBNull.Value;
                    }
                    else
                    {
                        commandParameters[i].Value = paramInstance.Value;
                    }
                }
                else if (parameterValues[i] == null)
                {
                    commandParameters[i].Value = DBNull.Value;
                }
                else
                {
                    commandParameters[i].Value = parameterValues[i];
                }
            }
        }
        #endregion




    }
}