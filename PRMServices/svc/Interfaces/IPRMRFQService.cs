﻿using System.ServiceModel;
using System.ServiceModel.Web;
using PRMServices.Models;
using System.Collections.Generic;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMRFQService
    {

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrfqcijlist?userid={userID}&sessionid={sessionID}")]
        List<stringCIJ> GetRFQCIJList(int userID, string sessionID);
       
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrfqindentlist?userid={userID}&sessionid={sessionID}")]
        List<MPIndent> GetRFQIndentList(int userID, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrfqcreators?indentid={indentID}&sessionid={sessionID}")]
        List<int> GetRFQCreators(int indentID, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "saverfqcreators")]
        Response SaveRFQCreators(List<UserInfo> listUsers, int indentID, string sessionID);

        
    }
}
