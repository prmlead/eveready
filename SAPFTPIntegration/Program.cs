﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Configuration;
using System.Data;
using PRMServices.SQLHelper;
using System.Data.SqlClient;
using Renci.SshNet.Sftp;
using Renci.SshNet;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.Net.Mail;
using GRID = SendGrid;
using GRID_EMAIL = SendGrid.Helpers.Mail;
using CORE = PRM.Core.Common;
using PRMServices.Models;
using Newtonsoft.Json;

namespace SAPFTPIntegration
{
    class Program
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static char csvDelimiter = '|';
        private static string _archiveFolder = ConfigurationManager.AppSettings["ArchiveFolder"].ToString();
        private static string _errorFolder = ConfigurationManager.AppSettings["ErrorFolder"].ToString();
        private static string _stageFolder = ConfigurationManager.AppSettings["StageFolder"].ToString();
        private static string _processFolder = ConfigurationManager.AppSettings["ProcessFolder"].ToString();
        private static string _popdfFolder = ConfigurationManager.AppSettings["POPDFFolder"].ToString();
        private static string SAPFTPPRFilePattern = "PR";
        private static string SAPFTPVEndorFilePattern = "VEN";
        private static string SAPFTPGRNFilePattern = "GRN";
        private static string SAPFTPPOFilePattern = "PO";
        private static string SAPFTPPOPDFFilePattern = "PO";
        private static string SAPFTPPAYMENTFilePattern = "PMT";
        private static string SAPFTPMATERIALFilePattern = "ITM";


        static void Main(string[] args)
        {
            string jobType = ConfigurationManager.AppSettings["JOB_TYPE"].ToString();

            NetworkCredential credentials = new NetworkCredential(ConfigurationManager.AppSettings["UserId"], ConfigurationManager.AppSettings["UserPwd"]);
            string url = ConfigurationManager.AppSettings["Host"];

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "PR_JOB")
            {
                DownloadFtpDirectory(url + "/PR/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPPRFilePattern);
                ProcessPRFiles();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "VENDOR_JOB")
            {
                DownloadFtpDirectory(url + "/VENDOR/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPVEndorFilePattern);
                ProcessVendorFiles();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "GRN_JOB")
            {
                DownloadFtpDirectory(url + "/GRN/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPGRNFilePattern);
                ProcesGRNFiles();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "PO_JOB")
            {
                DownloadFtpDirectory(url + "/PO_DATA/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPPOFilePattern);
                ProcesPOScheduleFiles();
            }
            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "PO_PDF_JOB")
            {
                DownloadFtpDirectory(url + "/PO_PDF/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPPOPDFFilePattern);
                ProcessPOPDFFiles();
            }
            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "MATERIAL_JOB")
            {
                DownloadFtpDirectory(url + "/MATERIAL/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPMATERIALFilePattern);
                ProcessMaterialFiles();
            }
            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "INVOICE_JOB")
            {
                ExportInvoiceCSV();
            }
            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "POST_QCS_JOB")
            {
                ExportPostQCSCSV();
            }

            if (ConfigurationManager.AppSettings["JOB_TYPE"] == "PAYMENT_JOB")
            {
                DownloadFtpDirectory(url + "/PAYMENT/", credentials, ConfigurationManager.AppSettings["StageFolder"], SAPFTPPAYMENTFilePattern);
                ProcesPaymentScheduleFiles();
            }
        }

        private static void ProcessPRFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();
                foreach (var filepath in filePaths)
                {

                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPPRFilePattern.ToUpper()))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        try
                        {
                            List<string> columns = "PLANT_CODE;PURCHASE_GROUP_CODE;REQUISITION_DATE;PR_RELEASE_DATE;PR_NUMBER;REQUISITIONER_NAME;ITEM_OF_REQUESITION;MATERIAL_CODE;MATERIAL_TYPE;MATERIAL_HSN_CODE;MATERIAL_DESCRIPTION;UOM;QTY_REQUIRED;PRICE;AMOUNT;MATERIAL_DELIVERY_DATE;DISTRIBUTION_ID;DESTINATION_ORGANIZATION_ID;REQUISITION_LINE_ID;MATERIAL_GROUP_CODE;MATERIAL_GROUP_DESC".Split(';').ToList();
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"PR records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                                foreach (var chunk in prChunks)
                                {
                                    DataTable tempDT = new DataTable();
                                    tempDT.Clear();
                                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                                    foreach (var column in columns)
                                    {
                                        tempDT.Columns.Add(column);
                                    }

                                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                    foreach (DataColumn col in tempDT.Columns)
                                    {
                                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                    }

                                    foreach (var row in chunk)
                                    {
                                        var newRow = tempDT.NewRow();
                                        newRow["JOB_ID"] = guid1;
                                        newRow["PLANT_CODE"] = GetDataRowVaue(row, "LOCATION");
                                        newRow["PURCHASE_GROUP_CODE"] = GetDataRowVaue(row, "PURCHASE_GROUP_NAME");
                                        string requesitionDate = GetDataRowVaue(row, "REQUISITION_DATE");
                                        newRow["REQUISITION_DATE"] = (requesitionDate == "00000000" || requesitionDate == "0" || requesitionDate == "") ? null : requesitionDate;
                                        string prReleaseDate = GetDataRowVaue(row, "RELEASE_DATE");
                                        newRow["PR_RELEASE_DATE"] = (prReleaseDate == "00000000" || prReleaseDate == "0" || prReleaseDate == "") ? null : prReleaseDate;
                                        newRow["PR_NUMBER"] = GetDataRowVaue(row, "PR_NUMBER");
                                        newRow["REQUISITIONER_NAME"] = GetDataRowVaue(row, "REQUISITIONER_NAME");
                                        newRow["ITEM_OF_REQUESITION"] = GetDataRowVaue(row, "ITEM_OF_REQUESITION");
                                        newRow["MATERIAL_CODE"] = GetDataRowVaue(row, "MATERIAL_CODE");
                                        newRow["MATERIAL_TYPE"] = GetDataRowVaue(row, "MATERIAL_TYPE");
                                        newRow["MATERIAL_HSN_CODE"] = GetDataRowVaue(row, "MATERIAL_HSN_CODE");
                                        newRow["MATERIAL_DESCRIPTION"] = GetDataRowVaue(row, "MATERIAL_DESC");
                                        newRow["UOM"] = GetDataRowVaue(row, "UOM");
                                        string qtyReq = !string.IsNullOrEmpty(GetDataRowVaue(row, "REQUIRED_QUANTITY")) ? GetDataRowVaue(row, "REQUIRED_QUANTITY").Replace(",", "") : "0";
                                        string price = !string.IsNullOrEmpty(GetDataRowVaue(row, "PRICE")) ? GetDataRowVaue(row, "PRICE").Replace(",", "") : "0";
                                        string amount = !string.IsNullOrEmpty(GetDataRowVaue(row, "AMOUNT")) ? GetDataRowVaue(row, "AMOUNT").Replace(",", "") : "0";
                                        newRow["QTY_REQUIRED"] = qtyReq;
                                        newRow["PRICE"] = price;
                                        newRow["AMOUNT"] = amount;
                                        string materialDeliveryDate = GetDataRowVaue(row, "MATERIAL_DELIVERY_DATE");
                                        newRow["MATERIAL_DELIVERY_DATE"] = (materialDeliveryDate == "00000000" || materialDeliveryDate == "0" || materialDeliveryDate == "") ? null : materialDeliveryDate;
                                        newRow["DISTRIBUTION_ID"] = GetDataRowVaue(row, "DISTRIBUTION_ID");
                                        newRow["DESTINATION_ORGANIZATION_ID"] = GetDataRowVaue(row, "DESTINATION_ORGANIZATION_ID");
                                        newRow["REQUISITION_LINE_ID"] = GetDataRowVaue(row, "REQUISITION_LINE_ID");
                                        newRow["MATERIAL_GROUP_CODE"] = GetDataRowVaue(row, "MAT_GROUP_CODE");
                                        newRow["MATERIAL_GROUP_DESC"] = GetDataRowVaue(row, "MATERIAL_GROUP_DESC");
                                        newRow["DATE_CREATED"] = DateTime.UtcNow;


                                        tempDT.Rows.Add(newRow);
                                    }

                                    bizClass.BulkInsert(tempDT, "[dbo].[SAP_PR_DETAILS]", columnMappings);
                                    tempDT.Rows.Clear();
                                }
                            }

                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_JOB_ID", guid);
                            bizClass.SelectList("erp_process_sap_pr_details", sd);

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }

        private static void ProcessPOPDFFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();
                foreach (var filepath in filePaths)
                {

                    if (File.Exists(filepath))
                    {
                        var ticks = DateTime.Now.Ticks;

                        var splitFileName = Path.GetFileName(filepath).Split('_');

                        String firstName = splitFileName[0];
                        String secondName = splitFileName[1];
                        var lastName = splitFileName[2].Split('.');

                        string text = "PO" + '_' + firstName + '_' + secondName + '.' + lastName[1];
                        string popdfFolder = _popdfFolder + "\\" + text;
                        
                        File.Move(filepath, popdfFolder);

                        
                    }

                }
            }
        }

        private static void ProcessVendorFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();
                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPVEndorFilePattern.ToUpper()))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        try
                        {
                            List<string> columns = "NAMEV,NAME1,SMTP_ADDR,TELF1,NAME2,SITE_CODE,WAERS,LAND1,LIFNR,VENDOR_SITE_ID,VENDOR_STATUS,VENDOR_SITE_STATUS,FTP_VENDOR_ID,STCD3,PAN_NUMBER,OTHER_INFO1".Split(',').ToList();
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"Vendor records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                                foreach (var chunk in prChunks)
                                {
                                    DataTable tempDT = new DataTable();
                                    tempDT.Clear();
                                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                                    foreach (var column in columns)
                                    {
                                        tempDT.Columns.Add(column);
                                    }

                                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                    foreach (DataColumn col in tempDT.Columns)
                                    {
                                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                    }

                                    string query = @"INSERT INTO [dbo].[SAP_VENDOR_DETAILS] ([JOB_ID], [NAMEV], [NAME1], [SMTP_ADDR], [TELF1], [NAME2],[SITE_CODE], [WAERS],[LAND1], 
                                    [VENDOR_SITE_ID],[VENDOR_STATUS],[VENDOR_SITE_STATUS],[FTP_VENDOR_ID], [LIFNR],[STCD3], [PAN_NUMBER],
                                    [OTHER_INFO1], [DATE_CREATED]) VALUES ";

                                    foreach (var row in chunk)
                                    {
                                        int temp = 0;
                                        if(Int32.TryParse(GetDataRowVaue(row, "VENDOR_SITE_ID"), out temp) && Int32.TryParse(GetDataRowVaue(row, "VENDOR_ID"), out temp))
                                        {
                                            var newRow = tempDT.NewRow();
                                            newRow["JOB_ID"] = guid1;
                                            newRow["NAMEV"] = GetDataRowVaue(row, "FIRST_NAME");
                                            newRow["NAME1"] = GetDataRowVaue(row, "LAST_NAME");
                                            newRow["SMTP_ADDR"] = GetDataRowVaue(row, "EMAIL_ID");
                                            newRow["TELF1"] = GetDataRowVaue(row, "PHONE_NUMBER");
                                            newRow["NAME2"] = GetDataRowVaue(row, "COMPANY_NAME");
                                            newRow["SITE_CODE"] = GetDataRowVaue(row, "SUPPLIER_SITE");
                                            newRow["WAERS"] = GetDataRowVaue(row, "CURRENCY");
                                            newRow["LAND1"] = GetDataRowVaue(row, "COUNTRY");
                                            //newRow["VEN_ALT_FIR"] = GetDataRowVaue(row, "ALT_FIRST_NAME");
                                            //newRow["VEN_ALT_LAS"] = GetDataRowVaue(row, "ALT_LAST_NAME");
                                            //newRow["SMTP_ADDR1"] = GetDataRowVaue(row, "ALTEMAIL");
                                            //newRow["TEL_NUMBER"] = GetDataRowVaue(row, "ALTPHONE_NO");
                                            newRow["LIFNR"] = GetDataRowVaue(row, "VENDOR_CODE");
                                            newRow["VENDOR_SITE_ID"] = GetDataRowVaue(row, "VENDOR_SITE_ID");
                                            newRow["VENDOR_STATUS"] = GetDataRowVaue(row, "VENDOR_STATUS");
                                            newRow["VENDOR_SITE_STATUS"] = GetDataRowVaue(row, "VENDOR_SITE_STATUS");
                                            newRow["FTP_VENDOR_ID"] = GetDataRowVaue(row, "VENDOR_ID");
                                            newRow["STCD3"] = GetDataRowVaue(row, "GSTIN");
                                            newRow["PAN_NUMBER"] = GetDataRowVaue(row, "PAN");
                                            newRow["OTHER_INFO1"] = fileName;
                                            newRow["DATE_CREATED"] = DateTime.UtcNow;

                                            query += $@"('{guid}',";
                                            query += $@"'{GetDataRowVaue(row, "FIRST_NAME")}',";
                                            query += $@"'{GetDataRowVaue(row, "LAST_NAME")}',";
                                            query += $@"'{GetDataRowVaue(row, "EMAIL_ID")}',";
                                            query += $@"'{GetDataRowVaue(row, "PHONE")}',";
                                            query += $@"'{GetDataRowVaue(row, "COMPANY_NAME")}',";
                                            query += $@"'{GetDataRowVaue(row, "SUPPLIER_SITE")}',";
                                            query += $@"'{GetDataRowVaue(row, "CURRENCY")}',";
                                            query += $@"'{GetDataRowVaue(row, "COUNTRY")}',";
                                            query += $@"'{GetDataRowVaue(row, "VENDOR_CODE")}',";
                                            query += $@"'{GetDataRowVaue(row, "VENDOR_SITE_ID")}',";
                                            query += $@"'{GetDataRowVaue(row, "VENDOR_STATUS")}',";
                                            query += $@"'{GetDataRowVaue(row, "ALTEMAIL")}',";
                                            query += $@"'{GetDataRowVaue(row, "VENDOR_SITE_STATUS")}',";
                                            query += $@"'{GetDataRowVaue(row, "VENDOR_ID")}',";
                                            query += $@"'{GetDataRowVaue(row, "GSTIN")}',";
                                            query += $@"'{GetDataRowVaue(row, "PAN")}',";
                                            query += $@"'{fileName}',";
                                            query += $@"GETUTCDATE()),";

                                            tempDT.Rows.Add(newRow);
                                        }                                        
                                    }

                                    query = query.TrimEnd(',');
                                    //bizClass.ExecuteNonQuery_IUD(query);
                                    logger.Debug("END GetVENDORDetails QUERY: " + query);
                                    bizClass.BulkInsert(tempDT, "[dbo].[SAP_VENDOR_DETAILS]", columnMappings);
                                    tempDT.Rows.Clear();
                                }
                            }

                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_JOB_ID", guid);
                            bizClass.SelectList("erp_process_sap_vendor_details", sd);

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
                SendEmailToVendors(guid);
            }
        }


        private static void ProcessMaterialFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();
                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPMATERIALFilePattern.ToUpper()))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        try
                        {
                            List<string> columns = "MTART,MATNR,STEUC,MEINS,MATKL,FTPProductID".Split(',').ToList();
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"Vendor records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                                foreach (var chunk in prChunks)
                                {
                                    DataTable tempDT = new DataTable();
                                    tempDT.Clear();
                                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                                    foreach (var column in columns)
                                    {
                                        tempDT.Columns.Add(column);
                                    }

                                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                    foreach (DataColumn col in tempDT.Columns)
                                    {
                                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                    }

                                    string query = @"INSERT INTO [dbo].[SAP_MATERIAL_DETAILS] ([JOB_ID], [MTART], [MATNR], [STEUC], [MEINS],[MATKL], [FTPProductID],
                                     [DATE_CREATED]) VALUES ";

                                    foreach (var row in chunk)
                                    {
                                        var newRow = tempDT.NewRow();
                                        newRow["JOB_ID"] = guid1;
                                        newRow["MTART"] = GetDataRowVaue(row, "ProductName");
                                        newRow["MATNR"] = GetDataRowVaue(row, "ProductCode");
                                        newRow["STEUC"] = GetDataRowVaue(row, "HSNCode");
                                        newRow["MEINS"] = GetDataRowVaue(row, "QuantityUnits");
                                        newRow["MATKL"] = GetDataRowVaue(row, "ProductCategories");
                                        newRow["FTPProductID"] = GetDataRowVaue(row, "FTPProductID");
                                        newRow["DATE_CREATED"] = DateTime.UtcNow;

                                        query += $@"('{guid}',";
                                        query += $@"'{GetDataRowVaue(row, "ProductName")}',";
                                        query += $@"'{GetDataRowVaue(row, "ProductCode")}',";
                                        query += $@"'{GetDataRowVaue(row, "HSNCode")}',";
                                        query += $@"'{GetDataRowVaue(row, "QuantityUnits")}',";
                                        query += $@"'{GetDataRowVaue(row, "ProductDescription")}',";
                                        query += $@"'{GetDataRowVaue(row, "ProductCategories")}',";
                                        query += $@"'{GetDataRowVaue(row, "FTPProductID")}',";
                                        query += $@"'{fileName}',";
                                        query += $@"GETUTCDATE()),";

                                        tempDT.Rows.Add(newRow);
                                    }

                                    query = query.TrimEnd(',');
                                    //bizClass.ExecuteNonQuery_IUD(query);
                                    logger.Debug("END GetMATERIALDetails QUERY: " + query);
                                    bizClass.BulkInsert(tempDT, "[dbo].[SAP_MATERIAL_DETAILS]", columnMappings);
                                    tempDT.Rows.Clear();
                                }
                            }

                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_JOB_ID", guid);
                            bizClass.SelectList("erp_process_sap_material_details", sd);

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }

        private static void ProcesGRNFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();
                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPGRNFilePattern.ToUpper()))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        try
                        {
                            List<string> columns = @"EBELN;PO_HEADER_ID;PO_DISTRIBUTION_ID;EBELP;MATNR;MAKTX;MENGE1;MENGE;REJECTED_QTY;MBLNR;BUDAT;ZEILE".Split(';').ToList();
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"GRN records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                                foreach (var chunk in prChunks)
                                {
                                    DataTable tempDT = new DataTable();
                                    tempDT.Clear();
                                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                                    foreach (var column in columns)
                                    {
                                        tempDT.Columns.Add(column);
                                    }

                                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                    foreach (DataColumn col in tempDT.Columns)
                                    {
                                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                    }

                                    foreach (var row in chunk)
                                    {
                                        var newRow = tempDT.NewRow();
                                        newRow["JOB_ID"] = guid1;
                                        newRow["EBELN"] = GetDataRowVaue(row, "PO_NUM");
                                        newRow["PO_HEADER_ID"] = GetDataRowVaue(row, "PO_HEADER_ID");
                                        newRow["PO_DISTRIBUTION_ID"] = GetDataRowVaue(row, "PO_DISTRIBUTION_ID");
                                        newRow["EBELP"] = GetDataRowVaue(row, "LINE_NUM");
                                        newRow["MATNR"] = GetDataRowVaue(row, "ITEM_CODE");
                                        newRow["MAKTX"] = GetDataRowVaue(row, "ITEM_DESCRIPTION");
                                        newRow["MENGE1"] = string.IsNullOrEmpty(GetDataRowVaue(row, "QUANTITY")) ? "0" : GetDataRowVaue(row, "QUANTITY");
                                        newRow["MENGE"] = string.IsNullOrEmpty(GetDataRowVaue(row, "RCV_QTY")) ? "0" : GetDataRowVaue(row, "RCV_QTY");
                                        newRow["REJECTED_QTY"] = GetDataRowVaue(row, "REJ_QTY");
                                        newRow["MBLNR"] = GetDataRowVaue(row, "RECEIPT_NUM");
                                        newRow["BUDAT"] = GetDataRowVaue(row, "RECEIPT_DATE");
                                        newRow["ZEILE"] = newRow["EBELP"];
                                        newRow["DATE_CREATED"] = DateTime.UtcNow;



                                        tempDT.Rows.Add(newRow);
                                    }

                                    bizClass.BulkInsert(tempDT, "[dbo].[SAP_GRN_DETAILS]", columnMappings);
                                    tempDT.Rows.Clear();
                                }
                            }

                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_JOB_ID", guid);
                            bizClass.SelectList("erp_process_sap_grn_details", sd);

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }

        private static void ProcesPaymentScheduleFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();
                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPPAYMENTFilePattern.ToUpper()))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        try
                        {
                            List<string> columns = @"PAYMENT_TRANSACT_ID;INVOICE_NUMBER;VENDOR_CODE;PAYMENT_AMOUNT;PAYMENT_CODE;PAYMENT_DATE;PAYMENT_STATUS".Split(';').ToList();
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"PAYMENT records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                                foreach (var chunk in prChunks)
                                {
                                    DataTable tempDT = new DataTable();
                                    tempDT.Clear();
                                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                                    foreach (var column in columns)
                                    {
                                        tempDT.Columns.Add(column);
                                    }

                                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                    foreach (DataColumn col in tempDT.Columns)
                                    {
                                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                    }

                                    foreach (var row in chunk)
                                    {
                                        var newRow = tempDT.NewRow();
                                        newRow["JOB_ID"] = guid1;
                                        newRow["PAYMENT_TRANSACT_ID"] = GetDataRowVaue(row, "PAYMENT_REF_ID");
                                        newRow["INVOICE_NUMBER"] = GetDataRowVaue(row, "INVOICE_NUMBER");
                                        newRow["VENDOR_CODE"] = GetDataRowVaue(row, "VENDOR_CODE");
                                        newRow["PAYMENT_AMOUNT"] = string.IsNullOrEmpty(GetDataRowVaue(row, "PAYMENT_AMOUNT")) ? "0" : GetDataRowVaue(row, "PAYMENT_AMOUNT");
                                        newRow["PAYMENT_CODE"] = GetDataRowVaue(row, "UTR_NUMBER");
                                        string paymentDate = GetDataRowVaue(row, "PAYMENT_DATE");
                                        newRow["PAYMENT_DATE"] = (paymentDate == "00000000" || paymentDate == "0" || paymentDate == "") ? null : paymentDate;

                                        newRow["PAYMENT_STATUS"] = GetDataRowVaue(row, "PAYMENT_STATUS");
                                        newRow["DATE_CREATED"] = DateTime.UtcNow;



                                        tempDT.Rows.Add(newRow);
                                    }

                                    bizClass.BulkInsert(tempDT, "[dbo].[SAP_PAYMENT_DETAILS]", columnMappings);
                                    tempDT.Rows.Clear();
                                }
                            }

                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_JOB_ID", guid);
                            bizClass.SelectList("erp_process_sap_payment_details", sd);

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }


        private static void ProcesPOScheduleFiles()
        {
            string[] filePaths = Directory.GetFiles(_stageFolder);
            if (filePaths != null && filePaths.Length > 0)
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                var guid1 = Guid.NewGuid();
                string guid = guid1.ToString();
                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath) && filepath.ToUpper().Contains(SAPFTPPOFilePattern.ToUpper()))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.Replace(".xls", ".csv").Replace(".txt", ".csv");

                        try
                        {
                            List<string> columns = @"EBELN,VENDOR_ID,LIFNR,NAME1,VENDOR_SITE_CODE,PO_HEADER_ID,PO_DISTRIBUTION_ID,EBELP,MATNR,TXZ01,MEINS,ORD_QTY,EFFWR,NETWR1,BILL_TO_LOCATION,SHIP_TO_LOCATION,AEDAT,ERNAM,ZTERM,TEXT1,SGST,CGST,IGST,UGST,UDATE,EINDT,TOT_BASE_VAL,TOT_PO_AMT,WAERS,BNFPO,QCS_ID,ORG_ID".Split(',').ToList();
                            File.Move(filepath, processFolder);
                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                logger.Debug($"PO SCH records for - {Path.GetFileName(filepath)}: {dt.Rows.Count}");
                                var prChunks = dt.AsEnumerable().ToList().Chunk(10000);
                                foreach (var chunk in prChunks)
                                {
                                    DataTable tempDT = new DataTable();
                                    tempDT.Clear();
                                    tempDT.Columns.Add("JOB_ID", System.Type.GetType("System.Guid"));
                                    foreach (var column in columns)
                                    {
                                        tempDT.Columns.Add(column);
                                    }

                                    tempDT.Columns.Add("DATE_CREATED", System.Type.GetType("System.DateTime"));
                                    List<SqlBulkCopyColumnMapping> columnMappings = new List<SqlBulkCopyColumnMapping>();
                                    foreach (DataColumn col in tempDT.Columns)
                                    {
                                        columnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
                                    }

                                    foreach (var row in chunk)
                                    {
                                        var newRow = tempDT.NewRow();
                                        newRow["JOB_ID"] = guid1;


                                        newRow["EBELN"] = GetDataRowVaue(row, "PO_NUM");
                                        newRow["VENDOR_ID"] = GetDataRowVaue(row, "VENDOR_ID");
                                        newRow["LIFNR"] = GetDataRowVaue(row, "VENDOR_CODE");
                                        newRow["NAME1"] = GetDataRowVaue(row, "VENDOR_NAME");
                                        newRow["VENDOR_SITE_CODE"] = GetDataRowVaue(row, "VENDOR_SITE_CODE");
                                        newRow["PO_HEADER_ID"] = GetDataRowVaue(row, "PO_HEADER_ID");
                                        newRow["PO_DISTRIBUTION_ID"] = GetDataRowVaue(row, "PO_DISTRIBUTION_ID");
                                        newRow["EBELP"] = GetDataRowVaue(row, "LINE_NUM");
                                        newRow["MATNR"] = GetDataRowVaue(row, "ITEM_CODE");
                                        newRow["TXZ01"] = GetDataRowVaue(row, "ITEM_DESCRIPTION");
                                        newRow["MEINS"] = GetDataRowVaue(row, "UNIT_MEAS_LOOKUP_CODE");
                                        newRow["ORD_QTY"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "QUANTITY")) ? GetDataRowVaue(row, "QUANTITY") : "0";
                                        newRow["EFFWR"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "UNIT_PRICE")) ? GetDataRowVaue(row, "UNIT_PRICE") : "0";
                                        newRow["NETWR1"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "LINE_BASE_AMT")) ? GetDataRowVaue(row, "LINE_BASE_AMT") : "0";
                                        newRow["BILL_TO_LOCATION"] = GetDataRowVaue(row, "BILL_TO_LOCATION");
                                        newRow["SHIP_TO_LOCATION"] = GetDataRowVaue(row, "SHIP_TO_LOCATION");
                                        string poDate = GetDataRowVaue(row, "CREATION_DATE");
                                        newRow["AEDAT"] = (poDate == "00000000" || poDate == "0" || poDate == "") ? null : poDate;
                                        newRow["ERNAM"] = GetDataRowVaue(row, "BUYER_NAME");
                                        newRow["ZTERM"] = GetDataRowVaue(row, "TERMS_NAME");
                                        newRow["SGST"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "SGST_RATE")) ? GetDataRowVaue(row, "SGST_RATE") : "0";
                                        newRow["CGST"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "CGST_RATE")) ? GetDataRowVaue(row, "CGST_RATE") : "0";
                                        newRow["IGST"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "IGST_RATE")) ? GetDataRowVaue(row, "IGST_RATE") : "0";
                                        newRow["UGST"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "UGST_RATE")) ? GetDataRowVaue(row, "UGST_RATE") : "0";
                                        newRow["TEXT1"] = GetDataRowVaue(row, "TAX_RATE_NAME");
                                        string poReleaseDate = GetDataRowVaue(row, "APPROVED_DATE");
                                        newRow["UDATE"] = (poReleaseDate == "00000000" || poReleaseDate == "0" || poReleaseDate == "") ? null : poReleaseDate;
                                        string delvDate = GetDataRowVaue(row, "NEED_BY_DATE");
                                        newRow["EINDT"] = (delvDate == "00000000" || delvDate == "0" || delvDate == "") ? null : delvDate;
                                        newRow["TOT_BASE_VAL"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "TOT_BASE_VAL")) ? GetDataRowVaue(row, "TOT_BASE_VAL") : "0";
                                        newRow["TOT_PO_AMT"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "TOT_PO_AMT")) ? GetDataRowVaue(row, "TOT_PO_AMT") : "0";
                                        newRow["WAERS"] = GetDataRowVaue(row, "CURRENCY_CODE");
                                        newRow["BNFPO"] = GetDataRowVaue(row, "PR_NUM");
                                        newRow["QCS_ID"] = !string.IsNullOrEmpty(GetDataRowVaue(row, "QCS_ID")) ? GetDataRowVaue(row, "QCS_ID") : "0";
                                        newRow["ORG_ID"] = GetDataRowVaue(row, "ORG_ID");

                                        newRow["DATE_CREATED"] = DateTime.UtcNow;
                                        tempDT.Rows.Add(newRow);
                                    }

                                    bizClass.BulkInsert(tempDT, "[dbo].[SAP_PO_SCHEDULE_DETAILS]", columnMappings);
                                    tempDT.Rows.Clear();
                                }
                            }

                            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                            sd.Add("P_JOB_ID", guid);
                            bizClass.SelectList("erp_process_sap_po_schedule_details", sd);

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }
        }



        private static void DownloadFtpDirectory1(string url, NetworkCredential credentials, string localPath, string jobType)
        {
            FtpWebRequest listRequest = (FtpWebRequest)WebRequest.Create(url);
            listRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            listRequest.Credentials = credentials;

            List<string> lines = new List<string>();

            using (FtpWebResponse listResponse = (FtpWebResponse)listRequest.GetResponse())
            using (Stream listStream = listResponse.GetResponseStream())
            using (StreamReader listReader = new StreamReader(listStream))
            {
                while (!listReader.EndOfStream)
                {
                    lines.Add(listReader.ReadLine());
                }
            }

            foreach (string line in lines)
            {
                try
                {
                    string[] tokens = line.Split(new[] { ' ' }, 9, StringSplitOptions.RemoveEmptyEntries);
                    string name = tokens[3];

                    string localFilePath = Path.Combine(localPath, name);
                    string fileUrl = url + "/" + name;

                    if (tokens[2] == "<DIR>" && false)
                    {
                        if (!Directory.Exists(localFilePath))
                        {
                            Directory.CreateDirectory(localFilePath);
                        }

                        DownloadFtpDirectory(fileUrl + "/", credentials, localFilePath, jobType);
                    }
                    else if (tokens[2] != "<DIR>" && !string.IsNullOrWhiteSpace(name) && name.ToLower().Contains(".csv"))
                    {
                        if (fileUrl.ToUpper().Contains(jobType))
                        {
                            FtpWebRequest downloadRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
                            downloadRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                            downloadRequest.Credentials = credentials;
                            using (FtpWebResponse downloadResponse = (FtpWebResponse)downloadRequest.GetResponse())
                            using (Stream sourceStream = downloadResponse.GetResponseStream())
                            using (Stream targetStream = File.Create(localFilePath))
                            {
                                byte[] buffer = new byte[10240];
                                int read;
                                while ((read = sourceStream.Read(buffer, 0, buffer.Length)) > 0)
                                {
                                    targetStream.Write(buffer, 0, read);
                                }
                            }

                            FtpWebRequest fileMoveRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
                            fileMoveRequest.UseBinary = true;
                            fileMoveRequest.Method = WebRequestMethods.Ftp.Rename;
                            fileMoveRequest.Credentials = credentials;
                            fileMoveRequest.RenameTo = $"{ConfigurationManager.AppSettings["FTPArchiveFolder"]}{name}";
                            var response = (FtpWebResponse)fileMoveRequest.GetResponse();
                            bool Success = response.StatusCode == FtpStatusCode.CommandOK || response.StatusCode == FtpStatusCode.FileActionOK;
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error($"DOWNLOAD FTP FILE: {line} with ERROR:{ex.Message}, DETAILS: {ex.StackTrace}");
                }
            }
        }

        public static void DownloadFtpDirectory(string url, NetworkCredential credentials, string localPath, string jobType)
        {
            var host = ConfigurationManager.AppSettings["HostUrl"];
            var port = 22;
            var username = ConfigurationManager.AppSettings["UserId"];
            var password = ConfigurationManager.AppSettings["UserPwd"];

            using (var sftpClient = new SftpClient(host, port, username, password))
            {
                try
                {
                    sftpClient.Connect();
                    string destLocalPath = localPath;
                    string sourceRemotePath = url;
                    string clientFolderName = ConfigurationManager.AppSettings["ClientFolderName"];
                    string ftpArchiveFolder = ConfigurationManager.AppSettings["FTPArchiveFolder"];

                    sftpClient.ChangeDirectory(clientFolderName);
                    var files = sftpClient.ListDirectory(".").Where(file => (file.Name != ".") && (file.Name != "..")).ToList();
                    foreach (SftpFile file in files)
                    {
                        string destFilePath = Path.Combine(destLocalPath, file.Name);

                        if (file.IsDirectory)
                        {

                        }
                        else
                        {

                            using (Stream fileStream = File.Create(destFilePath))
                            {
                                sftpClient.DownloadFile(file.Name, fileStream);
                            }
                            file.MoveTo(ftpArchiveFolder + "/" + file.Name);
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("Exception is>>>>" + ex.Message);
                    logger.Error("Exception1 is>>>>" + ex.InnerException);
                }
            }

        }

        private static string GetDataRowVaue(DataRow row, string columnName)
        {
            if (row.Table.Columns.Contains(columnName))
            {
                return row[columnName] != DBNull.Value ? row[columnName].ToString() : string.Empty;
            }
            else
            {
                return string.Empty;
            }
        }

        private static string RemoveFirstandLastChar(DataRow row, string columnName)
        {
            if (row.Table.Columns.Contains(columnName))
            {
                string resultString = row[columnName] != DBNull.Value ? row[columnName].ToString() : string.Empty;
                string unQuotedString = "";
                char[] charArr = { '\'', '\"', ',', '"', '-' };
                foreach (char C in charArr)
                {
                    string C1 = Convert.ToString(C);
                    if (resultString.StartsWith(C1))
                    {
                        char C2 = Convert.ToChar(C1);
                        unQuotedString = resultString.TrimStart(C2);

                    }
                    else if (resultString.EndsWith(C1))
                    {
                        char C2 = Convert.ToChar(C1);
                        unQuotedString = resultString.TrimEnd(C2);
                    }
                    else
                    {
                        unQuotedString = resultString;
                    }
                }
                resultString = unQuotedString;

                return resultString;
            }
            else
            {
                return string.Empty;
            }
        }

        private static void ExportInvoiceCSV()
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("select distinct pid.PO_NUMBER,PO_HEADER_ID,INVOICE_NUMBER,CONVERT(CHAR(8),INVOICE_DATE,112) as INVOICE_DATE,INVOICE_AMOUNT from POInvoiceDetails pid " +
                "inner join POScheduleDetails psd on psd.PO_NUMBER = pid.PO_NUMBER and psd.PO_LINE_ITEM = pid.PO_LINE_ITEM and IS_PROCESSED = 0 and COMP_ID = (SELECT dbo.getCompanyID(a.PARAM_VALUE) FROM [parameters] a WHERE PARAM_CODE = 'CURRENT_SUPER_USER')"))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;

                       
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);

                            //Build the CSV file data as a Comma separated string.
                            string csv = string.Empty;
                            string invoiceNumbers = string.Empty;
                            foreach (DataColumn column in dt.Columns)
                            {
                                //Add the Header row for CSV file.
                                csv += column.ColumnName + '|';
                            }
                            csv = csv.Remove(csv.Length - 1, 1);

                            //Add new line.
                            csv += "\r\n";

                            foreach (DataRow row in dt.Rows)
                            {
                                foreach (DataColumn column in dt.Columns)
                                {
                                    //Add the Data rows.
                                    csv += row[column.ColumnName].ToString().Replace("|", ";") + '|';
                                }
                                if (!string.IsNullOrEmpty(row["INVOICE_NUMBER"].ToString()))
                                {
                                    invoiceNumbers += row["INVOICE_NUMBER"].ToString() + ',';
                                }
                                csv = csv.Remove(csv.Length - 1, 1);

                                //Add new line.
                                csv += "\r\n";
                            }
                           
                            if (!string.IsNullOrEmpty(csv))
                            {
                                if (invoiceNumbers.Length > 0)
                                {
                                    string InvoiceFile = @"D:\FTP\SAPFiles\EVEREADY\InvoiceFolder\Data_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".csv";
                                    File.WriteAllText(InvoiceFile, csv);
                                    IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
                                    string updatePOQuery = string.Empty;
                                    String[] delimiters = { "," };
                                    String[] result = invoiceNumbers.Split(delimiters, StringSplitOptions.None);

                                    for (int i = 0; i < result.Length; i++)
                                    {
                                        updatePOQuery += $@"Update POInvoiceDetails SET IS_PROCESSED=1 where INVOICE_NUMBER in ('{result[i]}');";
                                        sqlHelper.ExecuteNonQuery_IUD(updatePOQuery);
                                    }
                                    Save(InvoiceFile, "INVOICE");
                                }
                                
                            }
                        }
                    }
                }

            }

            

        }

        private static void ExportPostQCSCSV()
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("exec erp_process_post_Qcs"))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        logger.Debug("query>>>"+cmd.CommandText);
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;


                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);

                            //Build the CSV file data as a Comma separated string.
                            string csv = string.Empty;
                            string qcsId = string.Empty;



                            foreach (DataColumn column in dt.Columns)
                            {
                                //Add the Header row for CSV file.
                                csv += column.ColumnName + '|';
                            }
                            csv = csv.Remove(csv.Length - 1, 1);

                            //Add new line.
                            csv += "\r\n";
                            foreach (DataRow row in dt.Rows)
                            {
                                foreach (DataColumn column in dt.Columns)
                                {
                                    //Add the Data rows.
                                    csv += row[column.ColumnName].ToString().Replace("|", ";") + '|';
                                }
                                if (!string.IsNullOrEmpty(row["QCS_ID"].ToString()))
                                {
                                    qcsId += row["QCS_ID"].ToString() + ',';
                                }

                                csv = csv.Remove(csv.Length - 1, 1);

                                //Add new line.
                                csv += "\r\n";
                            }
                           

                            if (!string.IsNullOrEmpty(csv))
                            {
                                if (qcsId.Length > 0)
                                {
                                    string InvoiceFile = @"D:\FTP\SAPFiles\EVEREADY\PostQCSFolder\Data_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".csv";
                                    File.WriteAllText(InvoiceFile, csv);
                                    IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
                                    string updatePOQuery = string.Empty;
                                    qcsId = qcsId.Remove(qcsId.Length - 1, 1);
                                    updatePOQuery += $@"Update qcs_vendor_item_assignment SET IS_PROCESSED=1 where QCS_ID in ({qcsId});";
                                    sqlHelper.ExecuteNonQuery_IUD(updatePOQuery);
                                    Save(InvoiceFile, "POST_QCS");
                                }
                                
                            }
                        }
                    }
                }

            }



        }


        public static void Save(string url,string type)
        {
            var host = ConfigurationManager.AppSettings["HostUrl"];
            var port = 22;
            var username = ConfigurationManager.AppSettings["UserId"];
            var password = ConfigurationManager.AppSettings["UserPwd"];
            var invoiceString = ConfigurationManager.AppSettings["Invoice"];
            var postQcsString = ConfigurationManager.AppSettings["PostQCS"];

            using (var sftpClient = new SftpClient(host, port, username, password))
            {
                try
                {
                    Console.WriteLine("Connect to server");

                    sftpClient.Connect();
                    Console.WriteLine("Creating FileStream object to stream a file");


                    string sourceFile = url;
                    using (Stream stream = File.OpenRead(sourceFile))
                    {
                        if (type == "INVOICE")
                        {
                            sftpClient.UploadFile(stream, @invoiceString + DateTime.Now.ToString("yyyyMMdd_hhmmss"));
                        }
                        else if (type == "POST_QCS")
                        {
                            sftpClient.UploadFile(stream, @postQcsString + DateTime.Now.ToString("yyyyMMdd_hhmmss"));
                        }

                    }
                }
                catch (Exception ex)
                {
                    logger.Error("Exception is>>>>" + ex.Message);
                    logger.Error("Exception1 is>>>>" + ex.InnerException);
                }
            }

        }

        private static void SendEmailToVendors(string JOB_ID) 
        {
            try
            {
                MSSQLBizClass bizClass = new MSSQLBizClass();
                List<EMAIL> details = new List<EMAIL>();
                CORE.DataNamesMapper<EMAIL> mapper = new CORE.DataNamesMapper<EMAIL>();
                string query = $@"select * from sapvendorsemail where JOB_ID = '{JOB_ID}'";
                DataSet ds = bizClass.ExecuteQuery(query);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    details = mapper.Map(ds.Tables[0]).ToList();
                    if (details != null && details.Count > 0) 
                    {
                        string body = string.Empty;
                        string remoteAPI = ConfigurationManager.AppSettings["REMOTE_API"].ToString();
                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(remoteAPI);
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = "GET";
                        var result = string.Empty;
                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                        {
                            result = streamReader.ReadToEnd();
                        }
                        SMTP smtpDet = new SMTP();
                        smtpDet = JsonConvert.DeserializeObject<SMTP>(result);
                        foreach (EMAIL det in details) 
                        {
                            body = String.Format(smtpDet.EMAIL_BODY, det.FIRST_NAME, det.LAST_NAME,det.LG_LOGIN,det.LG_PASSWORD);
                            body = body.Replace("COMPANY_NAME", smtpDet.COMPANY_NAME + " TEAM");
                            SendEmail(det.VIE_ID,det.JOB_ID,det.EMAIL_ID, "Welcome to the " + smtpDet.COMPANY_NAME + " e-Procurement Solution.", body, smtpDet.FROM_ADDRESS, smtpDet.DISPLAY_NAME, smtpDet);
                        }

                    }

                }
            }
            catch (Exception ex) 
            {
                logger.Error("error in retrieving email details >>>"+ ex.Message);
            }
        }

        public static async Task SendEmail(int veID, string JOB_ID, string To, string Subject, string Body, string from, string displayName, SMTP smtpDet, string cc = "")
        {
            string clientType = smtpDet.SMTP_CLIENT_TYPE;
            if (!clientType.Equals("SENDGRID", StringComparison.InvariantCultureIgnoreCase))
            {
                await SendSMTPEmail(veID, JOB_ID,To, Subject, Body, from, displayName, smtpDet, cc);
            }
            else
            {
                await SendGridEmail(veID, JOB_ID,To, Subject, Body, from, displayName, smtpDet, cc);
            }
        }
        public static bool RemoteServerCertificateValidationCallback(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        public static async Task SendSMTPEmail(int veID, string JOB_ID, string To, string Subject, string Body, string from, string displayName, SMTP smtpDet, string cc = "")
        {
            MSSQLBizClass bizClass = new MSSQLBizClass();
            System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(RemoteServerCertificateValidationCallback);
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(smtpDet.MAILHOST_USER,smtpDet.MAILHOST_PWD);
            SmtpClient smtpClient = new SmtpClient(smtpDet.MAILHOST, smtpDet.MAILHOST_PORT);
            MailMessage mail = new MailMessage();

            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                       | SecurityProtocolType.Tls11
                       | SecurityProtocolType.Tls12
                       | SecurityProtocolType.Ssl3;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072; // .NET 4.0

                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = credentials;
                smtpClient.EnableSsl = Convert.ToBoolean(smtpDet.MAILHOST_SSL_ENABLED);
                mail.From = new MailAddress(from, displayName);
                mail.BodyEncoding = System.Text.Encoding.ASCII;
                List<string> ToAddresses = To.Split(',').ToList<string>();
                foreach (string address in ToAddresses)
                {
                    if (!string.IsNullOrEmpty(address))
                    {
                        mail.To.Add(new MailAddress(address));
                    }
                }

                if (!string.IsNullOrEmpty(cc))
                {
                    List<string> ccAddresses = cc.Split(',').ToList<string>();
                    foreach (string address in ccAddresses)
                    {
                        if (!string.IsNullOrEmpty(address))
                        {
                            mail.CC.Add(new MailAddress(address));
                        }
                    }
                }

                //logger.Debug("in send email>>>");
                mail.Subject = Subject;
                mail.IsBodyHtml = true;
                mail.Body = Body;
                mail.Subject = Subject;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                smtpClient.Send(mail);
                string query = $@"update sapvendorsemail set SENT_EMAIL = 1 where VIE_ID = {veID};";
                bizClass.ExecuteQuery(query);
            }
            catch (Exception ex)
            {
                logger.Error("Email sending failed for " + To + "in JOB_ID : " + JOB_ID + " and error is :" + ex.Message);
                string query = $@"update sapvendorsemail set SENT_EMAIL = 0,ERROR_REASON = '{ex.Message}' where VIE_ID = {veID};";
                bizClass.ExecuteQuery(query);
            }
            finally
            {
                mail = null;
                smtpClient = null;
            }
        }
        public static async Task SendGridEmail(int veID, string JOB_ID, string To, string Subject, string Body, string from, string displayName, SMTP smtpDet, string cc = "")
        {
            MSSQLBizClass bizClass = new MSSQLBizClass();
            GRID.SendGridClient sendGridClient = new GRID.SendGridClient(smtpDet.SEND_GRID_KEY);
            GRID_EMAIL.SendGridMessage sendGridMessage = new GRID_EMAIL.SendGridMessage();
            List<SendGrid.Helpers.Mail.Attachment> sendGridAttachements = new List<SendGrid.Helpers.Mail.Attachment>();

            try
            {
                SmtpClient smtpClient = new SmtpClient(smtpDet.MAILHOST);
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(smtpDet.MAILHOST_USER, smtpDet.MAILHOST_PWD);
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = credentials;
                smtpClient.EnableSsl = Convert.ToBoolean(smtpDet.MAILHOST_SSL_ENABLED);
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(from, displayName);
                sendGridMessage.From = new GRID_EMAIL.EmailAddress(from);
                mail.BodyEncoding = System.Text.Encoding.ASCII;
                List<string> ToAddresses = To.Split(',').ToList<string>();
                foreach (string address in ToAddresses)
                {
                    if (!string.IsNullOrEmpty(address))
                    {
                        mail.To.Add(new MailAddress(address));
                    }
                }

                if (!string.IsNullOrEmpty(cc))
                {
                    List<string> ccAddresses = cc.Split(',').ToList<string>();
                    foreach (string address in ccAddresses)
                    {
                        if (!string.IsNullOrEmpty(address))
                        {
                            mail.CC.Add(new MailAddress(address));
                        }
                    }
                }

                mail.Subject = Subject;
                mail.IsBodyHtml = true;
                mail.Body = Body;
                sendGridMessage.Subject = Subject;
                sendGridMessage.HtmlContent = Body;
                sendGridMessage.AddTos(mail.To.Distinct().ToList().Select(m => new GRID_EMAIL.EmailAddress(m.Address)).ToList());
                if (mail.CC != null && mail.CC.Count > 0)
                {
                    sendGridMessage.AddCcs(mail.CC.Distinct().ToList().Select(m => new GRID_EMAIL.EmailAddress(m.Address)).ToList());
                }

                if (sendGridAttachements != null && sendGridAttachements.Count > 0)
                {
                    sendGridMessage.Attachments = sendGridAttachements;
                }

                logger.Info("Send email Before");
                var sendGridResponse = await sendGridClient.SendEmailAsync(sendGridMessage);
                logger.Info(sendGridResponse.StatusCode);
                logger.Info("Send email after");
                string query = $@"update sapvendorsemail set SENT_EMAIL = 1 where VIE_ID = {veID};";
                bizClass.ExecuteQuery(query);

            }
            catch (Exception ex)
            {
                logger.Error("Email sending failed for " + To + " for subject " + Subject + " and error is :" + ex.Message);
                string query = $@"update sapvendorsemail set SENT_EMAIL = 0,ERROR_REASON = '{ex.Message}' where VIE_ID = {veID};";
                bizClass.ExecuteQuery(query);
            }
            finally
            {
                sendGridMessage = null;
                sendGridClient = null;
            }
        }
    }

}