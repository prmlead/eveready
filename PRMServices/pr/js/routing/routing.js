﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('save-pr-details', {
                    url: '/save-pr-details/:Id',
                    templateUrl: 'pr/views/save-pr-details.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('pr-actions', {
                    url: '/pr-actions/:Id',
                    templateUrl: 'pr/views/pr-actions.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('list-pr', {
                    url: '/list-pr',
                    templateUrl: 'pr/views/list-pr.html',
                    params: {
                        detailsObj: null
                    }
                })
                .state('list-archived-pr', {
                    url: '/list-archived-pr',
                    templateUrl: 'pr/views/list-archived-pr.html',
                    params: {
                        detailsObj: null
                    }
                })
				   .state('list-pr-v2', {
                    url: '/list-pr-v2',
                    templateUrl: 'pr/views/list-pr-v2.html',
                    params: {
                        detailsObj: null
                    }
                })
				;
        }]);