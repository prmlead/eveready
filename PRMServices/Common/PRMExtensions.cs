﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

using PRMServices.Models;

namespace PRMServices
{
    public static class PRMExtensions
    {
        public static object GetColumnValue(this DataRow row, string column, Type objectType)
        {
            object value = null;
            if (row.Table.Columns.Contains(column))
            {
                value = row[column] != DBNull.Value ? row[column] : DefaultValue(objectType);
            }
            else
            {
                value = DefaultValue(objectType);
            }

            return value;
        }

        public static object GetColumnObject(this DataRow row, string column)
        {
            object value = null;
            if (row.Table.Columns.Contains(column))
            {
                value = row[column];
            }

            return value;
        }

        private static object DefaultValue(Type objectType)
        {
            if(objectType == typeof(int))
            {
                return -1;
            }
            else if(objectType == typeof(string))
            {
                return string.Empty;
            }
            else if (objectType == typeof(decimal))
            {
                return 0;
            }
            else if (objectType == typeof(DateTime))
            {
                return DateTime.MaxValue;
            }
            else
            {
                return null;
            }
        }
    }
}