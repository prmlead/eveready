﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class CompanyDepartments : Entity
    {        
        //DEPT_ID, DEPT_CODE, DEPT_DESC, COMP_ID, CREATED_BY, MODIFIED_BY, DATE_CREATED, DATE_MODIFIED 

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "deptID")]
        public int DeptID { get; set; }

        [DataMember(Name = "FIELD_ID")]
        public int FIELD_ID { get; set; }

        string _deptCode = string.Empty;
        [DataMember(Name = "deptCode")]
        public string DeptCode {
            get
            {
                return _deptCode;
            }
            set
            {
                _deptCode = value;
            }
        }

        string _deptDesc = string.Empty;
        [DataMember(Name = "deptDesc")]
        public string DeptDesc
        {
            get
            {
                return _deptDesc;
            }
            set
            {
                _deptDesc = value;
            }
        }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "deptAdmin")]
        public int DeptAdmin { get; set; }

        [DataMember(Name = "deptAdminName")]
        public string DeptAdminName { get; set; }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }

        [DataMember(Name = "typeID")]
        public int TypeID { get; set; }

        [DataMember(Name = "selectedDeptType")]
        public CompanyDeptDesigTypes SelectedDeptType { get; set; }

        string _deptBranch = string.Empty;
        [DataMember(Name = "deptBranch")]
        public string DeptBranch
        {
            get
            {
                return _deptBranch;
            }
            set
            {
                _deptBranch = value;
            }
        }
        
        [DataMember(Name = "deptPlants")]
        public List<int> DeptPlants { get; set; }
    }
}