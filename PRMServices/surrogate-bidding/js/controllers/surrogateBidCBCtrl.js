﻿prmApp

    .controller('surrogateBidCBCtrl', ["$scope", "$rootScope", "$http", "$state", "domain", "$filter", "$log", "$stateParams", "$timeout", "auctionsService", "fwdauctionsService",
        "userService", "SignalRFactory", "fileReader", "growlService", "signalRHubName",
        function ($scope, $rootScope, $http, $state, domain, $filter, $log, $stateParams, $timeout, auctionsService, fwdauctionsService,
            userService, SignalRFactory, fileReader, growlService, signalRHubName) {
            $scope.decimalRound = +userService.getDecimalRoundingSetting();
            $scope.isCustomer = false;
            $scope.hideFreeze = false;
            $scope.vendorIncoTerms = {};
            $scope.userIsOwner = false;
            $scope.bidComments = '';
            $scope.timerTimeLeft = 0;
            $scope.timerStyle = { 'color': '#000' };
            $scope.auctionItemVendor = [];
            $scope.productConfig = [];
            $scope.cbInitiated = false;
            $scope.cbOffered = false;
            $scope.cbOfferedMessage = '';
            $scope.cbOfferedObj = {};
            $scope.CBPricesAudit = {};
            $scope.cbStartedInputColour = {};
            $scope.bidHistoryExpand = {};
            $scope.CurrentPrice = 0;
            $scope.ReduceBidAmountBy = 0;
            $scope.NewPriceToBeQuoted = 0;
            $scope.isBidHistoryExpand = false;

            //Surrogate Settings
            $scope.surrogateCustomer = +userService.getUserId();
            $scope.currentUserId = $stateParams.VendorId;
            $scope.currentSessionId = userService.getUserToken();
            $scope.currentUserCompID = $stateParams.compId;
            $scope.currentRequirementId = $stateParams.Id;
            //^Surrogate Settings

            $scope.goToListItem = function () {
                $state.go('view-requirement', { 'Id': $scope.currentRequirementId });
            };

            $scope.getData = function (applySignalR) {
                auctionsService.getrequirementdata({ "reqid": $scope.currentRequirementId, "sessionid": $scope.currentSessionId, "userid": $scope.currentUserId })
                    .then(function (response) {
                        if (response) {
                            auctionsService.GetCompanyConfiguration(response.custCompID, "ITEM_UNITS", $scope.currentSessionId)
                                .then(function (unitResponse) {
                                    $scope.companyItemUnits = unitResponse;
                                });
                        }
                        $scope.fieldValidation(response, applySignalR);
                        //$scope.setAuctionInitializer(response, applySignalR);
                    });
            };

            $scope.setAuctionInitializer = function (response, applySignalR) {
                $scope.auctionItem = response;

                $scope.CurrentPrice = $scope.auctionItem.auctionVendors[0].revVendorTotalPrice;

                $scope.auctionItem.auctionVendors[0].revfreightChargesDB = $scope.auctionItem.auctionVendors[0].revfreightCharges;
                $scope.auctionItem.auctionVendors[0].revpackingChargesDB = $scope.auctionItem.auctionVendors[0].revpackingCharges;
                $scope.auctionItem.auctionVendors[0].revinstallationChargesDB = $scope.auctionItem.auctionVendors[0].revinstallationCharges;

                $scope.auctionItem.auctionVendors[0].listRequirementItems.forEach(function (item, itemIndex) {
                    item.revUnitPriceDB = item.revUnitPrice;
                    item.unitDiscountTemp = item.unitDiscount > 0 ? item.unitDiscount : "0";
                    item.revUnitDiscountTemp = item.revUnitDiscount > 0 ? item.revUnitDiscount : "0";
                })

                if (!$scope.auctionItem ||
                    ($scope.auctionItem && $scope.auctionItem.auctionVendors &&
                        $scope.auctionItem.auctionVendors.length > 0 &&
                        $scope.auctionItem.auctionVendors[0].revVendorTotalPriceCB > 0 &&
                        $scope.auctionItem.auctionVendors[0].isQuotationRejected == 1) ||
                    $scope.auctionItem.IS_CB_ENABLED == 0 ||
                    $scope.auctionItem.IS_CB_COMPLETED == true) {

                    $scope.goToListItem();

                };

                if ($scope.auctionItem.auctionVendors[0].FREEZE_CB == false &&
                    $scope.auctionItem.auctionVendors[0].VEND_FREEZE_CB == false &&
                    $scope.auctionItem.auctionVendors[0].FREEZE_CB == false) {
                    $scope.cbStartedInputColour = {
                        'background-color': '#f5b2b2'
                    };
                }

                $scope.description = $scope.auctionItem.description.replace(/\u000a/g, "</br>");
                $scope.deliveryLocation = $scope.auctionItem.deliveryLocation.replace(/\u000a/g, "</br>");
                $scope.paymentTerms = $scope.auctionItem.paymentTerms.replace(/\u000a/g, "</br>");
                $scope.deliveryTime = $scope.auctionItem.deliveryTime.replace(/\u000a/g, "</br>");

                //var date = $scope.auctionItem.postedOn.split('+')[0].split('(')[1];
                //var newDate = new Date(parseInt(parseInt(date)));
                //$scope.auctionItem.postedOn = newDate.toString().replace('Z', '');

                $scope.auctionItem.postedOn = $scope.GetDateconverted($scope.auctionItem.postedOn);

                $scope.auctionItem.multipleAttachments = [];
                $scope.auctionItem.attFile = $scope.auctionItem.attachmentName;

                if ($scope.auctionItem.attFile != '' && $scope.auctionItem.attFile != null && $scope.auctionItem.attFile != undefined) {
                    var attchArray = $scope.auctionItem.attFile.split(',');
                    attchArray.forEach(function (att, index) {
                        var fileUpload = {
                            fileStream: [],
                            fileName: '',
                            fileID: att
                        };
                        $scope.auctionItem.multipleAttachments.push(fileUpload);
                    })
                }

                if (applySignalR) {
                    var auctionItem = $scope.auctionItem;
                    $scope.auctionItemVendor = auctionItem;
                    $scope.getCalculatedPrices();
                }


            };

            $scope.getCalculatedPrices = function () {
                // $scope.$broadcast('timer-set-countdown-seconds', $scope.auctionItem.CB_TIME_LEFT);
                $scope.hideFreeze = false;

                $scope.itemLevelCalculations();

                $scope.auctionItemVendor.auctionVendors[0].revPrice = _.sumBy($scope.auctionItemVendor.auctionVendors[0].listRequirementItems, 'revitemPrice');
                $scope.auctionItemVendor.auctionVendors[0].revPriceCB = _.sumBy($scope.auctionItemVendor.auctionVendors[0].listRequirementItems, 'revitemPriceCB');



                if ($scope.auctionItemVendor.auctionVendors[0].revfreightCharges > $scope.auctionItemVendor.auctionVendors[0].revfreightChargesDB) {
                    swal("Error!", 'Please enter price less than ' + $scope.auctionItemVendor.auctionVendors[0].revfreightChargesDB);
                    $scope.auctionItemVendor.auctionVendors[0].revfreightCharges = $scope.auctionItemVendor.auctionVendors[0].revfreightChargesDB;
                }

                if ($scope.auctionItemVendor.auctionVendors[0].revpackingCharges > $scope.auctionItemVendor.auctionVendors[0].revpackingChargesDB) {
                    swal("Error!", 'Please enter price less than ' + $scope.auctionItemVendor.auctionVendors[0].revpackingChargesDB);
                    $scope.auctionItemVendor.auctionVendors[0].revpackingCharges = $scope.auctionItemVendor.auctionVendors[0].revpackingChargesDB;
                }

                if ($scope.auctionItemVendor.auctionVendors[0].revinstallationCharges > $scope.auctionItemVendor.auctionVendors[0].revinstallationChargesDB) {
                    swal("Error!", 'Please enter price less than ' + $scope.auctionItemVendor.auctionVendors[0].revinstallationChargesDB);
                    $scope.auctionItemVendor.auctionVendors[0].revinstallationCharges = $scope.auctionItemVendor.auctionVendors[0].revinstallationChargesDB;
                }


                var revfreightCharges = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].revfreightCharges);
                var freightChargesTaxPercentage = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].freightChargesTaxPercentage);
                $scope.auctionItemVendor.auctionVendors[0].revfreightChargesWithTax = parseFloat(revfreightCharges +
                    ((revfreightCharges / 100) * freightChargesTaxPercentage));

                var revfreightChargesCB = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].revfreightChargesCB);
                var freightChargesTaxPercentage = 0 //parseFloat($scope.auctionItemVendor.auctionVendors[0].freightChargesTaxPercentage);
                $scope.auctionItemVendor.auctionVendors[0].revfreightChargesWithTaxCB = parseFloat(revfreightChargesCB +
                    ((revfreightChargesCB / 100) * freightChargesTaxPercentage));


                var revpackingCharges = 0; // parseFloat($scope.auctionItemVendor.auctionVendors[0].revpackingCharges);
                var packingChargesTaxPercentage = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].packingChargesTaxPercentage);
                $scope.auctionItemVendor.auctionVendors[0].revpackingChargesWithTax = parseFloat(revpackingCharges +
                    ((revpackingCharges / 100) * packingChargesTaxPercentage));

                var revpackingChargesCB = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].revpackingChargesCB);
                var packingChargesTaxPercentage = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].packingChargesTaxPercentage);
                $scope.auctionItemVendor.auctionVendors[0].revpackingChargesWithTaxCB = parseFloat(revpackingChargesCB +
                    ((revpackingChargesCB / 100) * packingChargesTaxPercentage));


                var revinstallationCharges = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].revinstallationCharges);
                var installationChargesTaxPercentage = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].installationChargesTaxPercentage);
                $scope.auctionItemVendor.auctionVendors[0].revinstallationChargesWithTax = parseFloat(revinstallationCharges +
                    ((revinstallationCharges / 100) * installationChargesTaxPercentage));

                var revinstallationChargesCB = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].revinstallationChargesCB);
                var installationChargesTaxPercentage = 0; //parseFloat($scope.auctionItemVendor.auctionVendors[0].installationChargesTaxPercentage);
                $scope.auctionItemVendor.auctionVendors[0].revinstallationChargesWithTaxCB = parseFloat(revinstallationChargesCB +
                    ((revinstallationChargesCB / 100) * installationChargesTaxPercentage));





                $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPriceCB = parseFloat($scope.auctionItemVendor.auctionVendors[0].revPriceCB);
                $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice = parseFloat($scope.auctionItemVendor.auctionVendors[0].revPrice);

                $scope.ReduceBidAmountBy = parseFloat($scope.CurrentPrice).toFixed(2) - parseFloat($scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice).toFixed(2);
                if ($scope.ReduceBidAmountBy != 0) {
                    $scope.hideFreeze = true;
                }
                if ($scope.ReduceBidAmountBy <= 0) {
                    $scope.ReduceBidAmountBy = 0;
                }
                $scope.NewPriceToBeQuoted = $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice;

            };

            $scope.itemLevelCalculations = function () {
                $scope.auctionItemVendor.auctionVendors[0].revPrice = 0;
                $scope.auctionItemVendor.auctionVendors[0].revPriceCB = 0;
                if ($scope.auctionItemVendor.isDiscountQuotation == 0) {
                    $scope.auctionItemVendor.auctionVendors[0].listRequirementItems.forEach(function (item, itemIndex) {

                        if (item.revUnitPrice > item.revUnitPriceDB) {
                            swal("Error!", 'Please enter price less than ' + item.revUnitPriceDB + ' of ' + item.productIDorName);
                            item.revUnitPrice = item.revUnitPriceDB;
                        }

                        //#region Item Level Calculations

                        item.itemPrice = item.unitPrice * item.productQuantity;
                        //item.cGst = item.Gst / 2;
                        //item.sGst = item.Gst / 2;
                        //item.iGst = 0;
                        item.cGst = item.cGst;
                        item.sGst = item.sGst;
                        item.iGst = item.iGst;
                        item.itemPrice = (item.itemPrice + ((item.itemPrice / 100) * (item.cGst + item.sGst + item.iGst)));
                        var itemFreightWithTax = 0;// (item.itemFreightCharges + ((item.itemFreightCharges / 100) * (item.itemFreightTAX)));
                        item.itemPrice = item.itemPrice + itemFreightWithTax;

                        var revUnitPrice = parseFloat(item.revUnitPrice);
                        var itemRevFreightCharges = parseFloat(item.itemRevFreightCharges);

                        item.revitemPrice = revUnitPrice * item.productQuantity;
                        //item.cGst = item.Gst / 2;
                        //item.sGst = item.Gst / 2;
                        //item.iGst = 0;
                        item.cGst = item.cGst;
                        item.sGst = item.sGst;
                        item.iGst = item.iGst;
                        item.revitemPrice = (item.revitemPrice + ((item.revitemPrice / 100) * (item.cGst + item.sGst + item.iGst)));
                        var revItemFreightWithTax = 0;//(itemRevFreightCharges + ((itemRevFreightCharges / 100) * (item.itemFreightTAX)));
                        item.revitemPrice = item.revitemPrice + revItemFreightWithTax;

                        item.revitemPrice = parseFloat(item.revitemPrice);

                        var revUnitPriceCB = parseFloat(item.revUnitPriceCB);
                        var itemRevFreightChargesCB = parseFloat(item.itemRevFreightChargesCB);

                        item.revitemPriceCB = revUnitPriceCB * item.productQuantity;
                        //item.cGst = item.Gst / 2;
                        //item.sGst = item.Gst / 2;
                        //item.iGst = 0;
                        item.cGst = item.cGst;
                        item.sGst = item.sGst;
                        item.iGst = item.iGst;
                        item.revitemPriceCB = (item.revitemPriceCB + ((item.revitemPriceCB / 100) * (item.cGst + item.sGst + item.iGst)));
                        var revItemFreightWithTaxCB = 0;//(itemRevFreightChargesCB + ((itemRevFreightChargesCB / 100) * (item.itemFreightTAX)));
                        item.revitemPriceCB = item.revitemPriceCB + revItemFreightWithTaxCB;

                        item.revitemPriceCB = parseFloat(item.revitemPriceCB);

                        //#endregion Item Level Calculations
                    })
                }
                if ($scope.auctionItemVendor.isDiscountQuotation == 1) {
                    $scope.auctionItemVendor.auctionVendors[0].listRequirementItems.forEach(function (item, itemIdx) {
                        if (item.unitDiscount == "" || item.unitDiscount == undefined || item.unitDiscount == null) {
                            item.unitDiscount = "0";
                        }
                        if (item.revUnitDiscount == "" || item.revUnitDiscount == undefined || item.revUnitDiscount == null) {
                            item.revUnitDiscount = "0";
                        }
                       // item.unitDiscount = parseFloat(item.unitDiscount);
                       // item.revUnitDiscount = parseFloat(item.revUnitDiscount);
                        //if (item.revUnitDiscount > 100 || item.unitDiscount > 100 ) {
                        //    swal("Error!", 'Please enter valid discount percentage ');
                        //    item.unitDiscount = item.unitDiscountTemp;
                        //    item.revUnitDiscount = item.revUnitDiscountTemp;
                        //}
                        //if (item.revUnitDiscount < item.revUnitDiscountCB) {
                        //    swal("Error!", 'Please enter discount greater than ' + item.revUnitDiscountCB + ' of ' + item.productIDorName);
                        //    item.revUnitDiscount = item.revUnitDiscountCB;
                        //}

                        item.tempUitMRP = 0;
                        item.tempRevUnitDiscount = 0;

                        item.tempUitMRP = item.unitMRP;
                        item.tempRevUnitDiscount = item.revUnitDiscount;

                        if (item.unitMRP == undefined || item.unitMRP <= 0) {
                            item.unitMRP = 0;
                        }
                        if (item.revUnitDiscount == undefined || item.revUnitDiscount <= 0) {
                            item.revUnitDiscount = 0;
                        }
                       // item.unitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.unitDiscount));
                        item.unitPrice = item.unitMRP;
                        item.reductionPrice = (item.unitMRP - item.unitPrice) * item.productQuantity;
                        item.reductionPrice = $scope.precisionRound(parseFloat(item.reductionPrice), $rootScope.companyRoundingDecimalSetting);

                        item.revUnitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.revUnitDiscount));
                        item.revReductionPrice = (item.unitMRP - item.revUnitPrice) * item.productQuantity;
                        item.revReductionPrice = $scope.precisionRound(parseFloat(item.revReductionPrice), $rootScope.companyRoundingDecimalSetting);

                        item.revUnitPrice_CB = item.unitMRP - ((item.unitMRP / 100) * (item.revUnitDiscountCB));
                        item.revReductionPriceCB = (item.unitMRP - item.revUnitPrice_CB) * item.productQuantity;
                        item.revReductionPriceCB = $scope.precisionRound(parseFloat(item.revReductionPriceCB), $rootScope.companyRoundingDecimalSetting);

                        item.revitemPrice = item.revUnitPrice * item.productQuantity;
                        item.revitemPrice = (item.revitemPrice + ((item.revitemPrice / 100) * (parseFloat(item.cGst) + parseFloat(item.sGst) + parseFloat(item.iGst))) + 0);

                        item.unitMRP = item.tempUitMRP;
                        item.revUnitDiscount = item.tempRevUnitDiscount;

                    })
                }
            };

            $scope.getData(true);

            $scope.precisionRound = function (number, precision) {
                var factor = Math.pow(10, precision);
                return Math.round(number * factor) / factor;
            }

            $scope.SaveCBPrices = function (vendorID, value) {

                if ($scope.auctionItemVendor.isDiscountQuotation == 1) {
                    var Initialobj = $scope.auctionItemVendor.auctionVendors[0].listRequirementItems.filter(function (item) {
                        return item.unitDiscount > 100;
                    });
                    if (Initialobj.length > 0) {
                        swal("Error!", 'Please enter valid discount percentage ');
                        return;
                    }

                    var obj = $scope.auctionItemVendor.auctionVendors[0].listRequirementItems.filter(function (item) {
                        return item.revUnitDiscount > 100;
                    });
                    if (obj.length > 0) {
                        swal("Error!", 'Please enter valid discount percentage ');
                        return;
                    }

                    var revUnitDisObj = $scope.auctionItemVendor.auctionVendors[0].listRequirementItems.filter(function (item) {
                        return item.revUnitDiscount < item.revUnitDiscountTemp;
                    })
                    if (revUnitDisObj.length > 0) {
                        swal("Error!", 'Please enter discount greater than ' + revUnitDisObj[0].revUnitDiscountTemp + ' of ' + revUnitDisObj[0].productIDorName);
                        revUnitDisObj[0].revUnitDiscount = revUnitDisObj[0].revUnitDiscountTemp;
                        return false;
                    }
                    //$scope.auctionItemVendor.auctionVendors[0].listRequirementItems.forEach(function (item,itemIdx) {
                    //    if (item.revUnitDiscount < item.revUnitDiscountCB) {
                    //        swal("Error!", 'Please enter discount greater than ' + item.revUnitDiscountCB + ' of ' + item.productIDorName);
                    //        item.revUnitDiscount = item.revUnitDiscountCB;
                    //        return false;
                    //    }
                    //})
                   

                }

                if ($scope.auctionItem.auctionVendors[0].payment == null || $scope.auctionItem.auctionVendors[0].payment == '' || $scope.auctionItem.auctionVendors[0].payment == undefined) {
                    $scope.auctionItem.auctionVendors[0].payment = '';
                    swal({
                        title: "Error",
                        text: "Please enter the Payment Terms",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    });
                    return false;
                }

                if (value == 0) {
                    if ($scope.ReduceBidAmountBy > (($scope.CurrentPrice) - (0.70 * ($scope.CurrentPrice)))) {
                        swal("Maximum Reduction Error!",
                            "You are reducing more than 30% of current bid amount. The Maximum reduction amount should not exceed more than 30% from current bid amount  " +
                            $scope.CurrentPrice + ". Incase if You want to Reduce more Please Do it in Multiple Bids", "error");
                        $scope.getData(true);
                        return false;
                    }
                    $scope.bidComments = validateStringWithoutSpecialCharacters($scope.bidComments);
                    swal({
                        title: "Are you sure?",
                        text: "Your prices will be updated to customer.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function () {
                        var params = {
                            auctionVendor: $scope.auctionItemVendor.auctionVendors[0],
                            userID: $scope.currentUserId,
                            isCustomer: 0,
                            sessionID: $scope.currentSessionId,
                            bidComments: $scope.bidComments,
                            vendorID: vendorID
                        };
                        auctionsService.SaveCBPrices(params)
                            .then(function (response) {
                                if (response.errorMessage != "") {
                                    growlService.growl(response.errorMessage, "inverse");
                                } else {
                                    $scope.bidComments = '';
                                    $scope.recalculate('SAVE_CB_PRICE_VENDOR', $scope.currentUserId, false);
                                    //growlService.growl('Saved Successfully', "inverse");
                                    $scope.GetCBPricesAudit();
                                    $scope.getData(true);
                                    swal("Your prices submitted to customer successfully");
                                }
                            });
                    });

                } else if (value == 1) {
                    $scope.bidComments = validateStringWithoutSpecialCharacters($scope.bidComments);
                    swal({
                        title: "Are you sure?",
                        text: "You are confirming that this is the last price you would be offering to the customer for this requirement. No further updates are allowed.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function () {
                        var params = {
                            auctionVendor: $scope.auctionItemVendor.auctionVendors[0],
                            userID: $scope.currentUserId,
                            isCustomer: 0,
                            sessionID: $scope.currentSessionId,
                            bidComments: $scope.bidComments,
                            vendorID: vendorID
                        };
                        auctionsService.SaveCBPrices(params)
                            .then(function (response) {
                                if (response.errorMessage != "") {
                                    growlService.growl(response.errorMessage, "inverse");
                                } else {
                                    $scope.bidComments = '';
                                    $scope.recalculate('SAVE_CB_PRICE_VENDOR', $scope.currentUserId, false);
                                    //growlService.growl('Saved Successfully', "inverse");
                                    swal("Your prices submitted to customer successfully");
                                    $scope.GetCBPricesAudit();
                                    $scope.getData(true);
                                    $scope.FreezeRegretCounterBid(1);
                                }
                            });
                    });
                }
            };

            $scope.FreezeCounterBid = function (freezeValue) {

                var params = {
                    reqID: $scope.currentRequirementId,
                    vendorID: $scope.currentUserId,
                    sessionID: $scope.currentSessionId,
                    freezedBy: 'VENDOR',
                    freezeValue: freezeValue,
                    payment: $scope.auctionItem.auctionVendors[0].payment
                };

                swal({
                    title: "Are you sure?",
                    text: "You are confirming that this is the last price you would be offering to the customer for this requirement. No further updates are allowed.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    auctionsService.FreezeCounterBid(params)
                        .then(function (response) {
                            if (response.errorMessage != "") {
                                growlService.growl(response.errorMessage, "inverse");
                            } else {
                                $scope.recalculate('FREEZE_CB_CUSTOMER', $scope.currentUserId, false);
                                growlService.growl('Saved Successfully', "inverse");
                            }
                        });
                });
            };


            $scope.FreezeRegretCounterBid = function (freezeValue) {

                var params = {
                    reqID: $scope.currentRequirementId,
                    vendorID: $scope.currentUserId,
                    sessionID: $scope.currentSessionId,
                    freezedBy: 'VENDOR',
                    freezeValue: freezeValue,
                    payment: $scope.auctionItem.auctionVendors[0].payment
                };

                //swal({
                //    title: "Are you sure?",
                //    text: "You are confirming that this is the last price you would be offering to the customer for this requirement. No further updates are allowed.",
                //    type: "warning",
                //    showCancelButton: true,
                //    confirmButtonColor: "#F44336",
                //    confirmButtonText: "OK",
                //    closeOnConfirm: true
                //}, function () {
                auctionsService.FreezeCounterBid(params)
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            $scope.recalculate('FREEZE_CB_CUSTOMER', $scope.currentUserId, false);
                            growlService.growl('Saved Successfully', "inverse");
                        }
                    });
            };

            $scope.GetCBPricesAudit = function () {
                auctionsService.GetCBPricesAudit($scope.currentRequirementId, $scope.currentUserId, $scope.currentSessionId)
                    .then(function (response) {
                        $scope.CBPricesAudit = response;
                        $scope.CBPricesAudit.forEach(function (bid, bidIndex) {
                            console.log(bid);
                            if (bid && bid.CREATED_BY > 0 && $scope.currentUserId > 0 && bid.CREATED_BY != $scope.currentUserId) {
                                if (bidIndex == 0) {
                                    $scope.cbOffered = true;
                                    $scope.cbOfferedMessage = 'A new counter prices has been offered.';
                                    $scope.cbOfferedObj = bid;
                                }
                                $scope.cbInitiated = true;
                            }
                        })
                    });
            };

            $scope.GetCBPricesAudit();

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };

            var requirementHub;

            $scope.recalculate = function (subMethodName, receiverId, showswal) {

                if (subMethodName == null || subMethodName == '' || subMethodName == undefined) {
                    subMethodName = '';
                }

                if (receiverId >= 0) {

                } else {
                    receiverId = -1;
                }

                var params = {};
                params.reqID = $scope.currentRequirementId;
                params.sessionID = $scope.currentSessionId;
                params.userID = $scope.currentUserId;
                var parties = $scope.currentRequirementId + "$" + $scope.currentUserId + "$" + $scope.currentSessionId + "$" + subMethodName + "$" + receiverId;
                $scope.invokeSignalR('CheckRequirement', parties);
            };

            $scope.checkConnection = function () {
                if (requirementHub) {
                    return requirementHub.getStatus();
                } else {
                    return 0;
                }
            };

            $scope.reconnectHub = function () {
                if (requirementHub) {
                    if (requirementHub.getStatus() == 0) {
                        requirementHub.reconnect();
                        return true;
                    }
                } else {
                    requirementHub = SignalRFactory('', signalRHubName);
                }
            }

            $scope.invokeSignalR = function (methodName, params, callback) {
                if ($scope.checkConnection() == 1) {
                    requirementHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    });
                } else {
                    $scope.reconnectHub();
                    requirementHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    });
                }
            };

            $log.info('trying to connect to service');
            requirementHub = SignalRFactory('', signalRHubName);

            let signalRGroupName = 'requirementGroup' + $scope.currentRequirementId + '_' + $scope.userID;           
            $scope.invokeSignalR('joinGroup', signalRGroupName);

            $log.info('connected to service');

            $scope.$on("$destroy", function () {
                $log.info('disconecting signalR');
                requirementHub.stop();
                $log.info('disconected signalR');
            });

            requirementHub.on('checkRequirement', function (items) {

                var applySignalR = false;

                console.log('-----------------OUT-------------------->');

                if (items.inv.subMethodName == 'UPDATE_CB_TIME_CUSTOMER') {
                    location.reload();
                    return;
                }

                if (items.inv.methodName == 'CheckRequirement' &&
                    (items.inv.callerID == $scope.currentUserId ||
                        items.inv.receiverId == $scope.currentUserId ||
                        items.inv.receiverId == 0 ||
                        items.inv.subMethodName == 'SAVE_CB_PRICE_TO_ALL_CUSTOMER')) {
                    console.log('----------------IN--------------------->');
                    console.log(items.inv.methodName);
                    console.log(items.inv.receiverId);
                    applySignalR = true;

                }

                if (items.inv.subMethodName != 'CB_STOP_QUOTATIONS_CUSTOMER') {
                    $scope.getData(applySignalR);
                    $scope.GetCBPricesAudit();
                }

            });

            $scope.$on('timer-tick', function (event, args) {
                var temp = event.targetScope.countdown;
                if (event.targetScope.seconds == 5 ||
                    event.targetScope.seconds == 15 ||
                    event.targetScope.seconds == 25 ||
                    event.targetScope.seconds == 35 ||
                    event.targetScope.seconds == 45 ||
                    event.targetScope.seconds == 55) {
                    //auctionsService.CheckSystemDateTime();
                }

                if (event.targetScope.days == 0 &&
                    event.targetScope.hours == 0 &&
                    event.targetScope.minutes == 0 &&
                    (event.targetScope.seconds == 3 || event.targetScope.seconds == 2)) {

                    //code before the pause
                    setTimeout(function () {
                        //do what you need here
                        location.reload();
                    }, 2000);

                }

                if (event.targetScope.days > 0 ||
                    event.targetScope.hours > 0 ||
                    event.targetScope.minutes > 0 ||
                    event.targetScope.seconds > 1) {
                    $scope.cbStartedInputColour = {
                        'background-color': '#f5b2b2'
                    };
                }
                if (event.targetScope.countdown <= 120) {
                    $scope.timerStyle = {
                        'color': '#f00',
                        '-webkit - animation': 'flash linear 1s infinite',
                        'animation': 'flash linear 1s infinite'
                    };
                }

                if (event.targetScope.countdown > 120 && $scope.auctionItem.status == 'NOTSTARTED') {
                    $scope.timerStyle = { 'color': '#000' };
                }
                if (event.targetScope.countdown > 120 && $scope.auctionItem.status != 'NOTSTARTED') {
                    $scope.timerStyle = { 'color': '#228B22' };
                }

            });

            $scope.bidHistoryExpand = {
                'max-height': '250px',
                'overflow': 'hidden'
            };

            $scope.bidHistoryExpandCollapse = function (expand) {
                $scope.isBidHistoryExpand = expand;
                if (expand) {
                    $scope.bidHistoryExpand = {
                        'max-height': '5000px',
                        'overflow': 'hidden'
                    };
                } else {
                    $scope.bidHistoryExpand = {
                        'max-height': '250px',
                        'overflow': 'hidden'
                    };
                }
            };

            $scope.fieldValidation = function (data, applySignalR) {
                console.log(data);
                if ((data.customerID == $scope.currentUserId || data.superUserID == $scope.currentUserId || data.customerReqAccess)) {
                    $scope.userIsOwner = true;
                }

                data.auctionVendors[0].listRequirementItems.forEach(function (item, index) {
                    item.isEdit = true;
                });

                if (data.auctionVendors.length > 0) {
                    data.auctionVendors.forEach(function (vendor, index) {
                        if (vendor.INCO_TERMS && !$scope.vendorIncoTerms[vendor.INCO_TERMS]) {
                            auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, $scope.currentSessionId)
                                .then(function (response) {
                                    $scope.vendorIncoTerms[vendor.INCO_TERMS] = response;
                                    validateIncoTerms(data, response);
                                    $scope.setAuctionInitializer(data, applySignalR);
                                });
                        } else {
                            validateIncoTerms(data, $scope.vendorIncoTerms[vendor.INCO_TERMS]);
                            $scope.setAuctionInitializer(data, applySignalR);
                        }
                    });
                }
            };

            function validateIncoTerms(requirementData, incoTerms) {
                incoTerms.forEach(function (incoItem, itemIndexs) {
                    requirementData.auctionVendors[0].listRequirementItems.forEach(function (item, index) {
                        if (item.isCoreProductCategory == 0 && item.catalogueItemID == incoItem.ProductId) {
                            if ($scope.userIsOwner && incoItem.IS_CUSTOMER_EDIT) {
                                item.isEdit = false;
                            } else if (!$scope.userIsOwner && incoItem.IS_VENDOR_EDIT) {
                                item.isEdit = false;
                            }
                        } else if (item.isCoreProductCategory == 1) {
                            item.isEdit = false;
                        }

                    });
                });
            }

            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            }

        }]);