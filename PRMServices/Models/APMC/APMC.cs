﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class APMC : Entity
    {
        [DataMember(Name = "apmcID")]
        public int APMCID { get; set; }

        [DataMember(Name = "apmcName")]
        public string APMCName { get; set; }

        [DataMember(Name = "apmcDescription")]
        public string APMCDescription { get; set; }

        [DataMember(Name = "company")]
        public Company Company { get; set; }

        [DataMember(Name = "customer")]
        public User Customer { get; set; }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }

        [DataMember(Name = "apmcSettings")]
        public APMCSettings apmcSettings { get; set; }

        [DataMember(Name = "apmcVendors")]
        public List<APMCVendor> apmcVendors { get; set; }

        [DataMember(Name = "category")]
        public string Category { get; set; }
    }
}