prmApp
    .service('priceCapServices', ["priceCapDomain", "userService", "httpServices",
        function (priceCapDomain, userService, httpServices) {
            var priceCapServices = this;

            priceCapServices.GetReqData = function (params) {
                let url = priceCapDomain + 'getreqdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid;
                return httpServices.get(url);
            };

            priceCapServices.SavePriceCap = function (params) {
                let url = priceCapDomain + 'savepricecap';
                return httpServices.post(url, params);
            };
            
            return priceCapServices;

        }]);