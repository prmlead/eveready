﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('supplier', {
                    url: '/supplier',
                    templateUrl: 'Reports/views/supplier.html'
                })
                .state('savingsDashBoard', {
                    url: '/savingsDashBoard',
                    templateUrl: 'Reports/views/savingsDashBoard.html'
                })
                .state('contractSpend', {
                    url: '/contractSpend',
                    templateUrl: 'Reports/views/contractSpend.html'
                })
                .state('averageProcure', {
                    url: '/averageProcure',
                    templateUrl: 'Reports/views/averageProcure.html'
                })
                .state('purchaseTurnOver', {
                    url: '/purchaseTurnOver',
                    templateUrl: 'Reports/views/purchaseTurnOver.html'
                })
                .state('purchaserAnalysis', {
                    url: '/purchaserAnalysis',
                    templateUrl: 'Reports/views/purchaserAnalysis.html'
                })

        }]);﻿prmApp

    .controller('averageProcureCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "PRMLotReqService", "PRMCustomFieldService", "workflowService",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices,
            PRMLotReqService, PRMCustomFieldService, workflowService) {

            $scope.averageContainer = function () {
                Highcharts.setOptions({
                    colors: ['#01BAF2', '#71BF45', '#FAA74B']
                });
                Highcharts.chart('container', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'On-Time Delivery,Commit Date,Supplier'
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                formatter: function () {
                                    return this.key + ': ' + this.y + '%';
                                }
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Percentage',
                        colorByPoint: true,
                        innerSize: '70%',
                        data: [{
                            name: 'On Time',
                            // color: '#01BAF2',
                            y: 14,
                        }, {
                            name: 'Early',
                            // color: '#71BF45',
                            y: 18
                        }, {
                            name: 'Late',
                            //color: '#FAA74B',
                            y: 68
                        }]
                    }]
                });

            }
            $scope.averageContainer();

        }
    ]);﻿prmApp

    .controller('contractSpendCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "PRMLotReqService", "PRMCustomFieldService", "workflowService",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices,
            PRMLotReqService, PRMCustomFieldService, workflowService) {

            $scope.contractSpend = function () {
                var gaugeOptions = {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'solidgauge'
                    },
                   // title: '% Contract Spend',
                    pane: {
                        center: ['50%', '85%'],
                        size: '100%',
                        startAngle: -90,
                        endAngle: 90,
                        background: {
                            backgroundColor:
                                Highcharts.defaultOptions.legend.backgroundColor || '#EEE',
                            innerRadius: '60%',
                            outerRadius: '100%',
                            shape: 'arc'
                        }
                    },
                    exporting: {
                        enabled: false
                    },
                    tooltip: {
                        enabled: false
                    },
                    // the value axis
                    yAxis: {
                        stops: [
                            [0.1, '#55BF3B'], // green
                            [0.5, '#DDDF0D'], // yellow
                            [0.9, '#DF5353'] // red
                        ],
                        lineWidth: 0,
                        tickWidth: 0,
                        minorTickInterval: null,
                        tickAmount: 2,
                        title: {
                            y: -70
                        },
                        labels: {
                            y: 16
                        }
                    },
                    plotOptions: {
                        solidgauge: {
                            dataLabels: {
                                y: 5,
                                borderWidth: 0,
                                useHTML: true
                            }
                        }
                    }
                };
                // The speed gauge
                var chartSpeed = Highcharts.chart('contractSpend', Highcharts.merge(gaugeOptions, {
                    title: '% Contract Spend',
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: 'Contract Spend'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Contract Spend',
                        data: [55],
                        dataLabels: {
                            format:
                                '<div style="text-align:center">' +
                                '<span style="font-size:25px"><strong>Contract Spend</strong></span><br/>' +
                                '<span style="font-size:25px">{y} %</span><br/>' +
                                '<span style="font-size:12px;opacity:0.4">Contract Spend %</span>' +
                                '</div>'
                        },
                        tooltip: {
                            valueSuffix: ' %'
                        }
                    }]

                }));

                // The RPM gauge
                var chartRpm = Highcharts.chart('totalSavings', Highcharts.merge(gaugeOptions, {
                    title: 'Total Savings',
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: 'Total Savings'
                        }
                    },

                    series: [{
                        name: 'Total Savings',
                        data: [25],
                        dataLabels: {
                            format:
                                '<div style="text-align:center">' +
                                '<span style="font-size:25px"><strong>Total Savings</strong></span><br/>' +
                                '<span style="font-size:25px">{y:.1f}</span><br/>' +
                                '<span style="font-size:12px;opacity:0.4">' +
                                'Potential Savings $6.14m' +
                                '</span>' +
                                '</div>'
                        },
                        tooltip: {
                            valueSuffix: ' revolutions/min'
                        }
                    }]

                }));

                // Bring life to the dials
                var point,
                    newVal,
                    inc;

                if (chartSpeed) {
                    point = chartSpeed.series[0].points[0];
                    inc = Math.round((Math.random() - 0.5) * 100);
                    newVal = point.y;

                    if (newVal < 0 || newVal > 200) {
                        newVal = point.y;
                    }

                    point.update(newVal);
                }
                //setInterval(function () {
                //    // Speed
                    

                    
                //}, 2000);
                //// RPM
                    if (chartRpm) {
                        point = chartRpm.series[0].points[0];
                        inc = Math.random() - 0.5;
                        newVal = point.y;

                        if (newVal < 0 || newVal > 5) {
                            newVal = point.y;
                        }

                        point.update(newVal);
                    }

            }
           $scope.contractSpend();


        

            $scope.categoriesBySpend = function () {
                Highcharts.chart('categoriesBySpend', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Top 5 Sub Categories by Spend'
                    },
                    xAxis: {
                        min: 0,
                       
                        categories: ['Copiers And Fax', 'Office Machines', 'Storage and Organisation', 'Tables', 'Telephones']
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        reversed: true
                    },

                    plotOptions: {
                        series: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: false
                            }
                        }
                    },
                    series: [{
                        showInLegend: false,  
                        data: [2,4, 6, 7, 9]
                    }, {
                            showInLegend: false,  
                        data: [1,3, 5, 9, 10]
                    }]
                });
            }
            $scope.categoriesBySpend();


            $scope.supplierPerMonth = function () {
                Highcharts.chart('supplierPerMonth', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Supplier Per Month'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {

                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Supplier in 2019: <b>{point.y:.1f} millions</b>'
                    },
                    series: [{
                        name: '',
                        data: [
                            ['January', 50],
                            ['February', 43],
                            ['March', 56],
                            ['April', 50],
                            ['May', 47],
                            ['June', 56]
                        ],
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    }]
                });
            }
            $scope.supplierPerMonth();

            $scope.savingsPerMonth = function () {
                Highcharts.chart('savingsPerMonth', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        zoomType: 'xy'
                    },
                    title: {
                        text: 'Savings Per Month'
                    },
                    //subtitle: {
                    //    text: 'Source: WorldClimate.com'
                    //},
                    xAxis: [{
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
                        crosshair: true
                    }],
                    yAxis: [{ // Primary yAxis
                        labels: {
                            format: '{value}K',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        title: {
                            text: 'Total Savings',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        }
                    }, { // Secondary yAxis
                        title: {
                            text: 'Savings %',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value} %',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        opposite: true
                    }],
                    tooltip: {
                        shared: true
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'left',
                        x: 120,
                        verticalAlign: 'top',
                        y: 100,
                        floating: true,
                        backgroundColor:
                            Highcharts.defaultOptions.legend.backgroundColor || // theme
                            'rgba(255,255,255,0.25)'
                    },
                    series: [{
                        name: 'Savings',
                        type: 'column',
                        yAxis: 1,
                        
                        data: [9, 10, 12, 11, 9, 10],
                        tooltip: {
                            valueSuffix: ' %'
                        }

                    }, {
                        name: 'Total Savings',
                        type: 'spline',
                            
                            data: [60, 80, 100, 80, 60, 80],
                        tooltip: {
                            valueSuffix: 'K'
                        }
                    }]
                });
            }
            $scope.savingsPerMonth();
        }
    ]);﻿prmApp

    .controller('purchaserAnalysisCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "PRMLotReqService", "PRMCustomFieldService", "workflowService","PRMAnalysisServices",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices,
            PRMLotReqService, PRMCustomFieldService, workflowService, PRMAnalysisServices) {
            $scope.sessionid = userService.getUserToken();
            $scope.compID = userService.getUserCompanyId();
            $scope.SUB_U_ID = 0;

            //if ($scope.SUB_U_ID == 0) {
            //    $scope.SUB_U_ID_TEMP = userService.getUserId();
            //} else {
            //    $scope.SUB_U_ID_TEMP = $scope.SUB_U_ID;
            //}
            $scope.SUB_U_ID_TEMP = 0;
            $scope.subUsers = [];
            $scope.subUsersTemp = [];
            $scope.PurchaserAnalysis = {
                U_ID:0,
                USER_SAVINGS: 0,
                TOTAL_MATERIAL: 0,
                TOTAL_RFQ: 0,
                TOTAL_VENDORS: 0,
                DELETED_VENDORS: 0,
                ADDED_VENDORS: 0,
                TOTAL_VOLUME: 0,
                RFQ_TAT: 0,
                QCS_TAT: 0,
                TOTAL_ITEM_SUM:0
            };
            userService.getSubUsersData({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.subUsers = response;
                    
                });


            var today = moment();
            $scope.FromDate = today.add('days', -30).format('YYYY-MM-DD');
            today = moment().format('YYYY-MM-DD');
            $scope.ToDate = today;

            $scope.D = 0;
            $scope.H = 0;
            $scope.M = 0;

            $scope.D1 = 0;
            $scope.H1 = 0;
            $scope.M1 = 0;
            
            $scope.GetPurchaserAnalysis = function (uid) {
                var params = {
                    userID: uid,
                    fromDate: $scope.FromDate,
                    toDate: $scope.ToDate
                };
                PRMAnalysisServices.GetPurchaserAnalysis(params)
                    .then(function (response) {

                        $scope.PurchaserAnalysis = response;

                        $scope.D = parseInt($scope.PurchaserAnalysis.RFQ_TAT / (24 * 60));
                        $scope.H = parseInt(($scope.PurchaserAnalysis.RFQ_TAT % (24 * 60)) / 60);
                        $scope.M = parseInt(($scope.PurchaserAnalysis.RFQ_TAT % (24 * 60)) % 60);

                        $scope.D1 = parseInt($scope.PurchaserAnalysis.QCS_TAT / (24 * 60));
                        $scope.H1 = parseInt(($scope.PurchaserAnalysis.QCS_TAT % (24 * 60)) / 60);
                        $scope.M1 = parseInt(($scope.PurchaserAnalysis.QCS_TAT % (24 * 60)) % 60);
                    });
            };

            $scope.getPurchaserAnalysis = function () {
                if ($scope.SUB_U_ID == 0) {
                    $scope.SUB_U_ID_TEMP = userService.getUserId();
                } else {
                    $scope.SUB_U_ID_TEMP = $scope.SUB_U_ID;
                }
                $scope.GetPurchaserAnalysis($scope.SUB_U_ID_TEMP);
            }
            $scope.getPurchaserAnalysis();
        }
    ]);﻿prmApp

    .controller('purchaseTurnOverCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "PRMLotReqService", "PRMCustomFieldService", "workflowService",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices,
            PRMLotReqService, PRMCustomFieldService, workflowService) {
            $scope.years = [{ year: 2020 }, { year: 2021 }, { year: 2022 }, { year: 2023 }, { year: 2024 }, { year: 2025 }];

            $scope.purchaseTurnOver = function () {
                Highcharts.setOptions({
                    colors: ['#01BAF2', '#71BF45', '#FAA74B']
                });
                Highcharts.chart('procureVolume', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Procurement Volumes'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Procurement Volumes',
                        colorByPoint: true,
                        data: [{
                            name: '268.943',
                            y: 268.943,
                        }, {
                                name: '243.987',
                            y: 243.987,
                            sliced: true,
                            selected: true
                        }, {
                                name: '260.205',
                            y: 260.205
                            }, {
                                name: '385.145',
                                y: 385.145
                            }]
                    }]
                });


                Highcharts.chart('maverick', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Maverick Buying'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        },
                        series: {
                           
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    series: [{
                        name: 'Procurement Volumes',
                        colorByPoint: true,
                        data: [{
                            name: 'Spend not in Control',
                            y: 30,
                        }, {
                            name: 'Contract-Based',
                            y: 20,
                            sliced: true,
                            selected: true
                        }, {
                            name: 'Po-Based',
                            y: 70
                        }]
                    }]
                });


                Highcharts.chart('frameWork', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Framework Contract Rate'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {

                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        max: 6000,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Contract Rate: <b>{point.y:.1f} millions</b>'
                    },
                    series: [{
                        name: '',
                        data: [
                            ['Product 1', 4000],
                            ['Product 2', 4348],
                            ['Product 3', 5674],
                            ['Product 4', 5000],
                            ['Product 5', 4567]
                        ],
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    }]
                });


                Highcharts.chart('deliveryQuota', {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Mis-Delivery Quota'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: ['Buy', 'Components', 'Clothing', 'Accessories'],
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Delivery Quota',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: ''
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: false
                            }
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 80,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor:
                            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                        shadow: false
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Total Deliveries',
                        data: [107, 31, 635, 203, 2]
                    }, {
                        name: 'Incorrect Deliveries',
                            data: [133, 156, 947, 408, 6]
                    }]
                });

                Highcharts.chart('noOfOrders', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Number Of Orders'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {

                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Contract Rate: <b>{point.y:.1f} millions</b>'
                    },
                    series: [{
                        name: '',
                        data: [
                            ['Jan', 5],
                            ['Feb', 20],
                            ['Mar', 28],
                            ['Apr', 45],
                            ['May', 15],
                            ['Jun', 20]
                        ],
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    }]
                });

                Highcharts.chart('PurchaseBuyer', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Purchase Volume Per Buyer'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {

                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Contract Rate: <b>{point.y:.1f} millions</b>'
                    },
                    series: [{
                        name: '',
                        data: [
                            ['Buyer 1', 15],
                            ['Buyer 2', 20],
                            ['Buyer 3', 28],
                            ['Buyer 4', 45],
                            ['Buyer 5', 15],
                            ['Buyer 6', 20]
                        ],
                        dataLabels: {
                            enabled: false,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    }]
                });

                Highcharts.chart('flexibility', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Delivery Flexibility'
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                formatter: function () {
                                    return this.key + ': ' + this.y + '%';
                                }
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Flexibility',
                        colorByPoint: true,
                        innerSize: '70%',
                        data: [{
                            name: 'Flexibility',
                            // color: '#01BAF2',
                            y: 60,
                        }, {
                                name: 'Flexibility',
                            // color: '#71BF45',
                            y: 40
                            }],
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    }]
                });
            }
            $scope.purchaseTurnOver();
        }
    ]);﻿prmApp

    .controller('savingsDashBoardCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "PRMLotReqService", "PRMCustomFieldService", "workflowService", 
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices,
            PRMLotReqService, PRMCustomFieldService, workflowService) {

            $scope.currentUserCompID = userService.getUserCompanyId();
            $scope.currentUserId = +userService.getUserId();
            $scope.currentSessionId = userService.getUserToken();
            $scope.categoryStatsArray = [];
            $scope.categoryStatsArrayTemp = [];
            $scope.monthlyStatsArray = [];
            $scope.monthlyArrayTemp = [];
            $scope.departmentStatsArray = [];
            $scope.departmentStatsArrayTemp = [];


            reportingService.GetCompanySavingStats({ compid: $scope.currentUserCompID, sessionid: $scope.currentSessionId })
                .then(function (response) {
                    if (response) {
                        var categoryStats = response.filter(function (item) {
                            return item.name === 'CATEGORY';
                        });

                        if (categoryStats && categoryStats.length > 0) {
                            $scope.categoryStatsArray = categoryStats[0].arrayPair;
                            $scope.categoryStatsArrayTemp = $scope.categoryStatsArray;
                            //$scope.categoryStatsArray.forEach(function (item, index) {
                            //    item.percentage = (item.decimalVal1) / (item.decimalVal1) * 100;
                            //})
                            //var categoryStatsArray = [];
                            //categoryStats[0].arrayPair.forEach(function (item, itemIndexs) {
                            //    categoryStatsArray.push([item.key1, item.decimalVal]);
                            //});

                            //$scope.savingsFunnel(categoryStatsArray);
                        }

                        var monthlyStats = response.filter(function (item) {
                            return item.name === 'MONTHLY';
                        });

                        if (monthlyStats && monthlyStats.length > 0) {
                            $scope.monthlyStatsArray = monthlyStats[0].arrayPair;
                            var monthlyStatsArrayTEMP = [];
                            monthlyStats[0].arrayPair.forEach(function (item, itemIndexs) {
                                monthlyStatsArrayTEMP.push([item.key1, item.decimalVal]);
                            });

                            $scope.monthlyArrayTemp = $scope.monthlyStatsArray;

                            $scope.savingsbymonth(monthlyStatsArrayTEMP);
                        }

                        var departmentStats = response.filter(function (item) {
                            return item.name === 'DEPARTMENT';
                        });

                        if (departmentStats && departmentStats.length > 0) {
                            $scope.departmentStatsArray = departmentStats[0].arrayPair;

                             $scope.departmentStatsArrayTemp = $scope.departmentStatsArray;
                            //var departmentStatsArray = [];
                            //departmentStats[0].arrayPair.forEach(function (item, itemIndexs) {
                            //    departmentStatsArray.push({ name: item.key1, y: item.decimalVal });
                            //});

                            //$scope.savingsPie(departmentStatsArray);
                        }

                    }
                });

            $scope.savingsFunnel = function (funnelData) {
                Highcharts.chart('savingsFunnel', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'funnel'
                    },
                    title: {
                        text: 'Savings By Categry'
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b> ({point.y:,.0f})',
                                softConnector: true
                            },
                            center: ['40%', '50%'],
                            neckWidth: '30%',
                            neckHeight: '25%',
                            width: '80%'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    series: [{
                        name: 'Categories',
                        data: funnelData
                    }],

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                plotOptions: {
                                    series: {
                                        dataLabels: {
                                            inside: true
                                        },
                                        center: ['50%', '50%'],
                                        width: '100%'
                                    }
                                }
                            }
                        }]
                    }
                });
            };

            //$scope.savingsFunnel();

            $scope.savingsPie = function (departmentStatsArray) {
                // Make monochrome colors
                var pieColors = (function () {
                    var colors = [],
                        base = Highcharts.getOptions().colors[0],
                        i;

                    for (i = 0; i < 10; i += 1) {
                        // Start out with a darkened base color (negative brighten), and end
                        // up with a much brighter color
                        colors.push(Highcharts.color(base).brighten((i - 3) / 7).get());
                    }
                    return colors;
                }());

                // Build the chart
                Highcharts.chart('savingsPie', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Savings by Department'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            colors: pieColors,
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                                distance: -50,
                                filter: {
                                    property: 'percentage',
                                    operator: '>',
                                    value: 4
                                }
                            }
                        }
                    },
                    series: [{
                        name: 'Share',
                        data: departmentStatsArray
                    }]
                });
            };
            


            $scope.targetBar = function () {
                Highcharts.chart('targetBar', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Savings Vs Org Target bar chart'
                    },
                    xAxis: {
                        categories: ['Savings', 'Target']
                    },
                    yAxis: {
                        min: 0,
                        max: 5000,
                        title: {
                            text: 'Gap to Target 162,616(4%)$'
                        }
                        
                    },
                    legend: {
                        reversed: true
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: [{
                        showInLegend: false,  
                       name:'',
                        data: [4000,3500]
                    }]
                });
            }
            //$scope.targetBar();

            $scope.savingsbymonth = function (monthlyStatsArray) {
                monthlyStatsArray = [
                    ['January',1660986.54],
                    ['February',2814033.24],
                    ['March',3616009.35],
                    ['April', 575250],
                    ['May', 1913617.5],
                    ['June', 751720.05],
                    ['July', 5357489.16],
                    ['August', 3100780.15],
                    ['September', 236301.35],
                    ['November', 1281.17],
                    ['December', 1926347.62]
                ]

                Highcharts.chart('savingsByMonth', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Savings By Month'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {

                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Savings: <b>{point.y:.1f} millions</b>'
                    },
                    series: [{
                         name: '',
                        data: monthlyStatsArray,
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    }]
                });
            };

            $scope.savingsbymonth();

            $scope.filterCategories = function (search) {

                search = search.toLowerCase();

                if (search) {
                    $scope.categoryStatsArray = $scope.categoryStatsArrayTemp.filter(function (item) {
                        return item.key1.toLowerCase().indexOf(search) >= 0;
                    });
                } else {
                    $scope.categoryStatsArray = $scope.categoryStatsArrayTemp;
                }
            };

            $scope.filterDeapartment = function (search) {

                search = search.toLowerCase();

                if (search) {
                    $scope.departmentStatsArray = $scope.departmentStatsArrayTemp.filter(function (item) {
                        return item.key1.toLowerCase().indexOf(search) >= 0;
                    });
                } else {
                    $scope.departmentStatsArray = $scope.departmentStatsArrayTemp;
                }
            };

            $scope.filterMonth = function (search) {

                search = search.toLowerCase();

                if (search) {
                    $scope.monthlyStatsArray = $scope.monthlyArrayTemp.filter(function (item) {
                        return item.key1.toLowerCase().indexOf(search) >= 0;
                    });
                } else {
                    $scope.monthlyStatsArray = $scope.monthlyArrayTemp;
                }
            };
            
        }
    ]);﻿prmApp

    .controller('supplierCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "PRMLotReqService", "PRMCustomFieldService", "workflowService",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices,
            PRMLotReqService, PRMCustomFieldService, workflowService) {
            
            $scope.show = false;
            $scope.show1 = false;
            $scope.show2 = false;
            $scope.show3 = false;


            $scope.renderChart = function () {
                //$scope.Name = 'Supplier';
                $scope.Name = 'Defect Rate >= 2.5 ';
                $scope.data = [0,1,1.5,2,2.5,3,3.5];
                $scope.startTime = '08-01-2020 10:59';
                $scope.endTime = '08-07-2020 10:59';
                $scope.chartOptions = {
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {
                      
                        title: {
                           // text: 'Time ( Start Time: ' + '08-01-2020 10:59' + ' - End Time: ' + '08-07-2020 10:59' + ')'
                            text: ''
                        }
                    },
                    yAxis: {
                        title: {
                            text: ''
                        }
                    },
                    series: [{
                        name: $scope.Name,
                       // name1: $scope.Name1,
                        data: $scope.data
                    }]
                };
                $scope.show = true;
            };
            $scope.renderChart();

            $scope.renderSupplierAvailability = function () {
                //$scope.Name = 'Supplier';
                $scope.Name = 'Supplier Availability < 90% ';
                $scope.data = [0, 1, 1.5, 2, 2.5, 3];
                $scope.startTime = '08-01-2020 10:59';
                $scope.endTime = '08-07-2020 10:59';
                $scope.supplierAvailability = {
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {

                        title: {
                            // text: 'Time ( Start Time: ' + '08-01-2020 10:59' + ' - End Time: ' + '08-07-2020 10:59' + ')'
                            text: ''
                        }
                    },
                    yAxis: {
                        title: {
                            text: ''
                        }
                    },
                    series: [{
                        name: $scope.Name,
                        // name1: $scope.Name1,
                        data: $scope.data
                    }]
                };
                $scope.show1 = true;
            };
            $scope.renderSupplierAvailability();

            $scope.renderLeadTime = function () {
                //$scope.Name = 'Supplier';
                $scope.Name = 'Lead Time > 5.2 ';
                $scope.data = [0, 1, 1.5, 2, 2.5, 3];
                $scope.startTime = '08-01-2020 10:59';
                $scope.endTime = '08-07-2020 10:59';
                $scope.leadTime = {
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: ''
                    },
                    xAxis: {

                        title: {
                            // text: 'Time ( Start Time: ' + '08-01-2020 10:59' + ' - End Time: ' + '08-07-2020 10:59' + ')'
                            text: ''
                        }
                    },
                    yAxis: {
                        title: {
                            text: ''
                        }
                    },
                    series: [{
                        name: $scope.Name,
                        // name1: $scope.Name1,
                        data: $scope.data
                    }]
                };
                $scope.show2 = true;
            };
            $scope.renderLeadTime();


            $scope.defectType = {};

            //$scope.supplierDefectType = function () {
            //    var charts = [],
            //        containers = document.querySelectorAll('#trellis td'),
            //        datasets = [{
            //            name: 'No Impact',
            //            data: [80, 66, 67, 60, 60]
            //        },
            //        {
            //            name: 'Impact',
            //            data: [5, 19, 11, 8, 21]
            //        },
            //        {
            //            name: 'Rejected',
            //            data: [15, 25, 22, 32,19]
            //        }
            //        ];

                

            //    datasets.forEach(function (dataset, i) {
            //        charts.push(Highcharts.chart(containers[i], {

            //            chart: {
            //                type: 'bar',
            //                marginLeft: i === 0 ? 100 : 10
            //            },

            //            title: {
            //                text: dataset.name,
            //                align: 'left',
            //                x: i === 0 ? 90 : 0
            //            },

            //            credits: {
            //                enabled: false
            //            },

            //            xAxis: {
            //                categories: ['Supplier 1', 'Supplier 2', 'Supplier 3', 'Supplier 4', 'Supplier 5'],
            //                labels: {
            //                    enabled: i === 0
            //                }
            //            },

            //            yAxis: {
            //                allowDecimals: false,
            //                title: {
            //                    text: null
            //                },
            //                min: 0,
            //                max: 100
            //            },


            //            legend: {
            //                enabled: false
            //            },

            //            series: [dataset]

            //        }));
            //        $scope.show3 = true;
            //    });
            //}
            //$scope.supplierDefectType();


            $scope.supplierDefectType = function () {
                Highcharts.chart('container', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Supplier Defect Rate & Defect Type'
                    },
                    xAxis: {
                        categories: ['Supplier 1', 'Supplier 2', 'Supplier 3', 'Supplier 4', 'Supplier 5']
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Defect Type'
                        },
                        stackLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                color: ( // theme
                                    Highcharts.defaultOptions.title.style &&
                                    Highcharts.defaultOptions.title.style.color
                                ) || 'gray'
                            }
                        }
                    },
                    legend: {
                        align: 'right',
                        x: -30,
                        verticalAlign: 'top',
                        y: 25,
                        floating: true,
                        backgroundColor:
                            Highcharts.defaultOptions.legend.backgroundColor || 'white',
                        borderColor: '#CCC',
                        borderWidth: 1,
                        shadow: false
                    },
                    tooltip: {
                        headerFormat: '<b>{point.x}</b><br/>',
                        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    series: [{
                        name: 'No Impact',
                        data: [80, 66, 67, 60, 60]
                    }, {
                        name: 'Impact',
                            data: [5, 19, 11, 8, 21]
                    }, {
                        name: 'Rejected',
                            data: [15, 25, 22, 32, 19]
                    }]
                });
            }
            $scope.supplierDefectType();

            $scope.deliveryTime = function () {
                Highcharts.chart('deliveryTime', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Delivery Time'
                    },
                    xAxis: {
                        categories: ['Supplier 1', 'Supplier 2', 'Supplier 3', 'Supplier 4', 'Supplier 5']
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        reversed: true
                    },

                    plotOptions: {
                        series: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    series: [{
                        name: 'No Impact',
                        data: [80, 66, 67, 60, 60]
                    }, {
                        name: 'Impact',
                        data: [5, 19, 11, 8, 21]
                    }, {
                        name: 'Rejected',
                        data: [15, 25, 22, 32, 19]
                    }]
                });
            }
            $scope.deliveryTime();
        }
    ]);﻿prmApp.constant('PRMAnalysisServicesDomain', 'Reports/svc/PRMAnalysisService.svc/REST/');
prmApp.constant('signalRFwdHubName', 'fwdRequirementHub');
prmApp.service('PRMAnalysisServices', ["PRMAnalysisServicesDomain", "userService", "httpServices",
    function (PRMAnalysisServicesDomain, userService, httpServices) {


        var PRMAnalysisServices = this;

        PRMAnalysisServices.GetPurchaserAnalysis = function (params) {
            let url = PRMAnalysisServicesDomain + 'GetPurchaserAnalysis?userID=' + params.userID + '&fromdate=' + params.fromDate + '&todate=' + params.toDate + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };
       

        

        
        return PRMAnalysisServices;

    }]);