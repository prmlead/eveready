angular = require('angular');

angular.module('profileModule')
.controller('profileMyLeadsCtrl',["$scope", "loginService", "profileService", function ($scope, loginService, profileService) {
 
	$scope.myActiveLeads = [];
	$scope.myAuctionsLoaded = false;
	$scope.totalItems = 0;
	$scope.totalLeads = 0;
	$scope.currentPage = 1;
	$scope.itemsPerPage = 5;
	$scope.maxSize = 5;
	$scope.myAuctionsMessage = '';

  
	$scope.getactiveleads = function(){
		profileService.getactiveleads({ "userid": loginService.getUserId(), "sessionid": loginService.getUserToken() })
			.then(function (response) {
				$scope.myActiveLeads = response;
				if ($scope.myActiveLeads.length > 0) {
					$scope.myAuctionsLoaded = true;
					$scope.totalLeads = $scope.myActiveLeads.length;
				} else {
					$scope.totalLeads = 0;
					$scope.myAuctionsLoaded = false;
					$scope.myAuctionsMessage = "There are no auctions running right now for you.";
				}
			});
	};
	$scope.getactiveleads();

	$scope.setPage = function (pageNo) {
	$scope.currentPage = pageNo;
	};

	$scope.pageChanged = function () {

        };
	}]);