﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models.Common
{
    [DataContract]
    public class FileUploadModel
    {
        [DataMember(Name = "fileStream")]
        public byte[] FileStream { get; set; }

        [DataMember(Name = "name")]
        public string FileName { get; set; }

        [DataMember(Name = "fileType")]
        public string FileType { get; set; }
    }
}