﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PRMServices.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.4.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://risepidev.biocon.com:50000/XISOAPAdapter/MessageServlet?senderParty=&sende" +
            "rService=BC_PRM360&receiverParty=&receiverService=&interface=SI_MaterialMaster_S" +
            "ync_Out&interfaceNamespace=http%3A%2F%2Fbiocon.com%2FPRM360%2FSAP%2FMaterialMast" +
            "er")]
        public string PRMServices_Syngene_Material_Service_SI_MaterialMaster_Sync_OutService {
            get {
                return ((string)(this["PRMServices_Syngene_Material_Service_SI_MaterialMaster_Sync_OutService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://risepiqas.biocon.com:50000/XISOAPAdapter/MessageServlet?senderParty=&sende" +
            "rService=BC_PRM360&receiverParty=&receiverService=&interface=SI_POCreate_Sync_Ou" +
            "t&interfaceNamespace=http%3A%2F%2Fbiocon.com%2FPRM360%2FSAP%2FPOCreate")]
        public string PRMServices_Syngene_PO_Service1_SI_POCreate_Sync_OutService {
            get {
                return ((string)(this["PRMServices_Syngene_PO_Service1_SI_POCreate_Sync_OutService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://risepidev.biocon.com:50000/XISOAPAdapter/MessageServlet?senderParty=&sende" +
            "rService=BC_PRM360&receiverParty=&receiverService=&interface=SI_POCreate_Sync_Ou" +
            "t&interfaceNamespace=http%3A%2F%2Fbiocon.com%2FPRM360%2FSAP%2FPOCreate")]
        public string PRMServices_Syngene_PO_Service2_SI_POCreate_Sync_OutService {
            get {
                return ((string)(this["PRMServices_Syngene_PO_Service2_SI_POCreate_Sync_OutService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://NLLCOSAPDEV01.NEULANDLABS.COM/sap/bc/srt/wsdl/flv_10002p111ad1/sdef_url/zm" +
            "m_poack")]
        public string PRMServices_Neuland_ACK_DEV1_ZMM_POACK {
            get {
                return ((string)(this["PRMServices_Neuland_ACK_DEV1_ZMM_POACK"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://NLLCOSAPDEV01.NEULANDLABS.COM/sap/bc/srt/wsdl/flv_10002p111ad1/sdef_url/zm" +
            "m_asn_u1")]
        public string PRMServices_Neuland_ASN_DEV_ZMM_ASN_U1 {
            get {
                return ((string)(this["PRMServices_Neuland_ASN_DEV_ZMM_ASN_U1"]));
            }
        }
    }
}
