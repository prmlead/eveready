﻿
prmApp
    .controller('fwdConsalidatedReportCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "PRMForwardBidService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, $http, domain, $rootScope, fileReader, $filter, $log, PRMForwardBidService) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;
            $scope.consalidatedReport = [];

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 8;
            $scope.revisedUserL1 = [];


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
            };

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            //$scope.reportFromDate = '';
            //$scope.reportToDate = '';

            $scope.reportToDate = moment().format('YYYY-MM-DD');
            $scope.reportFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            $scope.getConsalidatedReport = function () {
                $scope.errMessage = '';

                PRMForwardBidService.getConsolidatedReport($scope.reportFromDate, $scope.reportToDate)
                    .then(function (response) {
                        $scope.consalidatedReport = response;

                        $scope.totalItems = $scope.consalidatedReport.length;
                        $scope.consalidatedReport.forEach(function (item, index) {
                            item.closed = $scope.prmStatus($scope.isCustomer, item.closed);
                            item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                            item.reqPostedOn = $scope.GetDateconverted(item.reqPostedOn);
                            item.startTime = $scope.GetDateconverted(item.startTime);

                            if (String(item.startTime).includes('9999')) {
                                item.startTime = '';
                            }
                        });
                    });
            };

            $scope.getConsalidatedReport();

            $scope.GetReport = function () {

                alasql('SELECT requirementNumber as [Requirement Number],title as [Requirement Title],prNumbers as [PR Numbers],reqCategory as [Category], ' +
                    'closed as Status, ' +
                    'reqPostedOn as [Posted On], quotationFreezTime as [Closed bid deadline], startTime as [Scheduled],IL1_vendTotalPrice as [Initial Least Price], ' +
                    'RL1_companyName as [L1 Company Name],  RL1_revVendTotalPrice as [L1 Rev Price], ' +
                    'RL2_companyName as [L2 Company Name],  RL2_revVendTotalPrice as [L2 Rev Price],  ' +
                    'basePriceSavings as [Earnings],savingsPercentage as [Earnings %] ' +
                    'INTO XLSX(?, { headers: true, sheetid: "ConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                    ["ConsolidatedReport.xlsx", $scope.consalidatedReport]);
            };

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };
        }]);