﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class FileUpload
    {
        [DataMember(Name = "fileStream")]
        public byte[] FileStream { get; set; }

        [DataMember(Name = "fileName")]
        public string FileName { get; set; }
    }
}