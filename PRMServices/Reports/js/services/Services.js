﻿prmApp.constant('PRMAnalysisServicesDomain', 'Reports/svc/PRMAnalysisService.svc/REST/');
prmApp.constant('signalRFwdHubName', 'fwdRequirementHub');
prmApp.service('PRMAnalysisServices', ["PRMAnalysisServicesDomain", "userService", "httpServices",
    function (PRMAnalysisServicesDomain, userService, httpServices) {


        var PRMAnalysisServices = this;

        PRMAnalysisServices.GetPurchaserAnalysis = function (params) {
            let url = PRMAnalysisServicesDomain + 'GetPurchaserAnalysis?userID=' + params.userID + '&fromdate=' + params.fromDate + '&todate=' + params.toDate + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };
       

        

        
        return PRMAnalysisServices;

    }]);