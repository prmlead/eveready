﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;


namespace PRMServices.Models
{
    [DataContract]
    public class OpsReports : Entity
    {
        int objectId = 0;
        [DataMember(Name = "objectID")]
        public int ObjectID
        {
            get
            {
                return this.objectId;
            }
            set
            {
                this.objectId = value;
            }
        }


        [DataMember(Name = "reqTransactions")]
        public List<OpsRequirement> ReqTransactions
        {
            get;
            set;
        }

        [DataMember(Name = "subUsersRegistered")]
        public List<OpsUserDetails> SubUsersRegistered
        {
            get;
            set;
        }

        [DataMember(Name = "vendorsRegistered")]
        public List<OpsUserDetails> VendorsRegistered
        {
            get;
            set;
        }


        [DataMember(Name = "regularTransactions")]
        public List<Requirement> regularTransactions
        {
            get;
            set;
        }


        [DataMember(Name = "subUsersRegisteredAll")]
        public List<User> SubUsersRegisteredAll
        {
            get;
            set;
        }

        [DataMember(Name = "vendorsRegisteredAll")]
        public List<User> VendorsRegisteredAll
        {
            get;
            set;
        }
    }
}