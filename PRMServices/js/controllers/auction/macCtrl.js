﻿prmApp

    // =========================================================================
    // AUCTION ITEM
    // =========================================================================

    .controller('macCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "poService" ,
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices, poService) {
            //var liveId = "";
            //$scope.companyItemUnits = [];
            //var id = $stateParams.Id;
            $scope.reqId = $stateParams.reqID;

            $scope.requirement = [];
            $scope.companyItemUnits = [];
            
            $scope.getData = function ()
            {
                //auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                //    .then(function (response) {
                //        $scope.requirement = response;

                //        $scope.requirement.auctionVendors.forEach(function (item, index) {
                //            if (item.rank == 1)
                //            {
                //                item.RankOneVendorID = item.vendorID;
                //            }
                //        })


                //       $scope.getrevisedquotations($scope.reqId);
                        
                //    });
                reportingService.GetReqItemWiseVendors($scope.reqId, userService.getUserToken())
                    .then(function (response) {
                        $scope.requirement = response;

                        $scope.requirement.reqItems.forEach(function (item, index) {
                            item.reqVendors.forEach(function (item1,index1) {
                                if (item1.rank == 1) {
                                    $scope.vendorID = item1.vendorID;
                                    item.revVendorUnitPrice = item1.quotationPrices.revUnitPrice;
                                    item.poUnitPrice = item.revVendorUnitPrice;
                                }
                                item.totalItemPrice = item1.quotationPrices.revitemPrice;
                            })
                        })

                        //$scope.requirement.reqVendors.forEach(function (item, index) {


                        //})

                });
            }

            $scope.getData();
            $scope.compID = userService.getUserCompanyId();
            auctionsService.GetCompanyConfiguration($scope.compID, "ITEM_UNITS", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyItemUnits = unitResponse;
                });

            $scope.getrevisedquotations = function (reqid) {
                var params = {
                    reqID: reqid,
                    userID: userService.getUserId(),
                    sessionID: userService.getUserToken()
                };
                auctionsService.getrevisedquotations(params)
                    .then(function (response) {
                        if (response.errorMessage == '')
                        {
                            return response;
                          //  location.reload();
                        } else {
                           // swal("Error", response.errorMessage, 'error');
                          //  console.log(response.errorMessage);
                        }

                    })
            }


            $scope.changeVendorsPrices = function (vendorID)
            {
             //   console.log("vendor id>>>" + vendorID);
                $scope.requirement.reqItems.forEach(function (item, index) {
                    item.reqVendors.forEach(function (item1, index1) {
                        if (vendorID == item1.vendorID) {
                            item.vendorID = item1.vendorID;
                            item.revVendorUnitPrice = item1.quotationPrices.revUnitPrice;
                            item.poUnitPrice = item.revVendorUnitPrice;
                        }
                    })
                })
            }


            $scope.autoUpdateForAllItems = function () {

                $scope.requirement.reqItems.forEach(function (item, index) {
                    item.purchaseID = $scope.purchaseID;
                //    item.indentID = $scope.indentID;
                });

                //for (i = 0; i < $scope.po.listPOItems.length; i++) {
                //    $scope.po.listPOItems.expectedDeliveryDate = $scope.expectedDeliveryDate;
                //}
            }

            $scope.updateForAllItems = function () {
                    $scope.requirement.reqItems.forEach(function (item, index) {
                    item.expectedDeliveryDate = $scope.expectedDeliveryDate;
                    item.deliveryAddress = $scope.deliveryAddress;
                });
            }


            $scope.postRequest = function () {

                $scope.requirement.sessionID = userService.getUserToken();
                $scope.requirement.purchaseID = $scope.purchaseID;
                $scope.requirement.vendorID = $scope.vendorID;


                $scope.requirement.reqItems.forEach(function (item, index) {
                    var ts = moment(item.expectedDeliveryDate, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    item.expectedDeliveryDate = "/Date(" + milliseconds + "000+0530)/";
                });


                var params = {
                    'vendorpo': $scope.requirement
                }
                poService.SaveVendorPOInfo(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {

                            //$window.history.back();

                            swal({
                                title: "Done!",
                                text: "Purchase Orders Generated Successfully.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    //location.reload();
                                    $state.go('po-list', { "reqID": $stateParams.reqID, "vendorID": $stateParams.vendorID, "poID": 0 });
                                });
                        }
                    });
            }


            
        }]);