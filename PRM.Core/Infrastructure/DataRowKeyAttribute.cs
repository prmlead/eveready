﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Infrastructure
{
    public class DataRowKeyAttribute : Attribute
    {
        private readonly string _Key;

        public string Key
        {
            get { return _Key; }
        }

        public DataRowKeyAttribute(string key)
        {
            _Key = key;
        }
    }
}
