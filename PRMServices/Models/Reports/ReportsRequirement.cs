﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class ReportsRequirement : Entity
    {
        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "customerID")]
        public int CustomerID { get; set; }

        string title = string.Empty;
        [DataMember(Name = "title")]
        public string Title
        {
            get
            {
                if (!string.IsNullOrEmpty(title))
                {
                    return title;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    title = value;
                }
            }
        }

        [DataMember(Name = "postedOn")]
        public DateTime PostedOn { get; set; }
        
        [DataMember(Name = "startTime")]
        public DateTime StartTime { get; set; }        

        [DataMember(Name = "endTime")]
        public DateTime EndTime { get; set; }

        string negotiationDuration = string.Empty;
        [DataMember(Name = "negotiationDuration")]
        public string NegotiationDuration
        {
            get
            {
                if (!string.IsNullOrEmpty(negotiationDuration))
                {
                    return negotiationDuration;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    negotiationDuration = value;
                }
            }
        }

        [DataMember(Name = "savings")]
        public double Savings { get; set; }

        [DataMember(Name = "isTabular")]
        public bool IsTabular { get; set; }

        [DataMember(Name = "isUnitPriceBidding")]
        public int IsUnitPriceBidding { get; set; }

        [DataMember(Name = "noOfVendorsInvited")]
        public int NoOfVendorsInvited { get; set; }

        [DataMember(Name = "noOfVendorsParticipated")]
        public int NoOfVendorsParticipated { get; set; }

    }
}


    //REQ_ID, U_ID, REQ_TITLE, REQ_POSTED_ON, START_TIME, END_TIME, 
    //NEGOTIATION_DURATION,
    //SAVINGS,
    //REQ_IS_TABULAR, IS_UNIT_PRICE_BIDDING, 
    //NO_OF_VENDORS_INVITED, NO_OF_VENDORS_PARTICIPATED