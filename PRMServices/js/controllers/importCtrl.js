﻿prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('importCtrl', ["$state", "$stateParams", "$scope", "$rootScope", "auctionsService", "userService", "$http", "$window", "domain",
        "fileReader", "growlService", "$log", "$filter", "ngDialog", "techevalService", "fwdauctionsService", "catalogService",
        "workflowService", "PRMPRServices", "PRMCustomFieldService", "reportingService", "PRMCustomFieldService", "$modal",
        function ($state, $stateParams, $scope, $rootScope, auctionsService, userService, $http, $window, domain,
            fileReader, growlService, $log, $filter, ngDialog, techevalService, fwdauctionsService, catalogService,
            workflowService, PRMPRServices, PRMCustomFieldService, reportingService, PRMCustomFieldService, $modal) {


            $scope.formDetails = {};
            $scope.formDetails.items = [];
            $scope.addItem = function () {
                $scope.formDetails.items.push({
                    ITEM_OF_REQUESITION: '',
                    isDeleted: false
                })
            }
            $scope.deleteItem = function (item) {
                item.isDeleted = true;
            }




            $scope.pageNo = 1;
            $scope.nextpage = function (pageNo) {
                $scope.pageNo = $scope.pageNo + 1;
            }
            $scope.previouspage = function () {
                $scope.pageNo = $scope.pageNo - 1;

            }

        }]);