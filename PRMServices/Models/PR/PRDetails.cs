﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class PRDetails : Entity
    {

        [DataMember] [DataNames("PR_ITEM_ID")] public int PR_ITEM_ID { get; set; }
        [DataMember] [DataNames("PR_ID")] public int PR_ID { get; set; }
        [DataMember] [DataNames("PR_TITLE")] public string PR_TITLE { get; set; }
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("PR_NUMBER")] public string PR_NUMBER { get; set; }
        [DataMember] [DataNames("PR_TYPE")] public string PR_TYPE { get; set; }
        [DataMember] [DataNames("PR_TYPE1")] public string PR_TYPE1 { get; set; }
        [DataMember] [DataNames("ASSET_TYPE")] public string ASSET_TYPE { get; set; }
        [DataMember] [DataNames("PRIORITY")] public string PRIORITY { get; set; }
        [DataMember] [DataNames("PRIORITY_COMMENTS")] public string PRIORITY_COMMENTS { get; set; }
        [DataMember] [DataNames("DEPARTMENT")] public int DEPARTMENT { get; set; }
        [DataMember] [DataNames("REQUEST_DATE")] public DateTime? REQUEST_DATE { get; set; }
        [DataMember] [DataNames("MODIFIED_DATE")] public DateTime? MODIFIED_DATE { get; set; }
        [DataMember] [DataNames("REQUIRED_DATE")] public DateTime? REQUIRED_DATE { get; set; }
        [DataMember] [DataNames("ATTACHMENTS")] public string ATTACHMENTS { get; set; }
        [DataMember] [DataNames("TOTAL_BASE_PRICE")] public decimal TOTAL_BASE_PRICE { get; set; }
        [DataMember] [DataNames("TOTAL_GST_PRICE")] public decimal TOTAL_GST_PRICE { get; set; }
        [DataMember] [DataNames("TOTAL_PRICE")] public decimal TOTAL_PRICE { get; set; }
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("PRItemsList")] public List<PRItems> PRItemsList { get; set; }
        [DataMember] [DataNames("WF_ID")] public int WF_ID { get; set; }
        //[DataMember] [DataNames("DEPT_CODE")] public string DEPT_CODE { get; set; }
        [DataMember] [DataNames("U_NAME")] public string U_NAME { get; set; }
        [DataMember] [DataNames("CLASSIFICATION")] public string CLASSIFICATION { get; set; }
        [DataMember] [DataNames("COMPANY")] public string COMPANY { get; set; }
        [DataMember] [DataNames("WORK_ORDER_DURATION")] public string WORK_ORDER_DURATION { get; set; }
        [DataMember] [DataNames("PURPOSE_IN_BRIEF")] public string PURPOSE_IN_BRIEF { get; set; }
        [DataMember] [DataNames("PURCHASE")] public string PURCHASE { get; set; }
        [DataMember] [DataNames("TYPE")] public string TYPE { get; set; }

        [DataMember] [DataNames("SHORT_TEXT")] public string SHORT_TEXT { get; set; }
        [DataMember] [DataNames("DESIG_ID")] public int DESIG_ID { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }


        [DataMember] [DataNames("DELIVERY_DATE")] public DateTime? DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("CREATED_DATE")] public DateTime? CREATED_DATE { get; set; }


        [DataMember] [DataNames("PR_STATUS")] public string PR_STATUS { get; set; }
        [DataMember] [DataNames("PURCHASE_GROUP_CODE")] public string PURCHASE_GROUP_CODE { get; set; }
        [DataMember] [DataNames("PURCHASE_GROUP_NAME")] public string PURCHASE_GROUP_NAME { get; set; }
        //[DataMember] [DataNames("CURRENTAPPROVERNAME")] public string CURRENTAPPROVERNAME { get; set; }

        [DataMember] [DataNames("PLANT")] public string PLANT { get; set; }
        [DataMember] [DataNames("PLANT_NAME")] public string PLANT_NAME { get; set; }

        [DataMember] [DataNames("PLANT_LOCATION")] public string PLANT_LOCATION { get; set; }

        //[DataMember] [DataNames("DOC_TYPE")] public string DOC_TYPE { get; set; }
        //[DataMember] [DataNames("DOC_TYPE_NAME")] public string DOC_TYPE_NAME { get; set; }
        [DataMember] [DataNames("COMMENTS")] public string COMMENTS { get; set; }

        [DataMember] [DataNames("HEADER_NOTE")] public string HEADER_NOTE { get; set; }

        [DataMember] [DataNames("TOTAL_ITEMS")] public int TOTAL_ITEMS { get; set; }
        [DataMember] [DataNames("COMPLETED_ITEMS")] public int COMPLETED_ITEMS { get; set; }

        [DataMember] [DataNames("CREATED_BY_NAME")] public string CREATED_BY_NAME { get; set; }
        
        [DataMember] [DataNames("RELEASE_DATE")] public DateTime? RELEASE_DATE{ get; set; }
        //[DataMember] [DataNames("MAT_GROUP")] public string MAT_GROUP { get; set; }
        //[DataMember] [DataNames("MATERIAL_GROUP_LIST")] public string MATERIAL_GROUP_LIST { get; set; }
        [DataMember] [DataNames("REQ_TITLE")] public string REQ_TITLE { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("REQ_NUMBER")] public string REQ_NUMBER { get; set; }

        [DataMember] [DataNames("REQ_IDS")] public string REQ_IDS { get; set; }
        //[DataMember] [DataNames("REQUISITIONER_ID")] public string REQUISITIONER_ID { get; set; }
        //[DataMember] [DataNames("REQUISITIONER_NAME")] public string REQUISITIONER_NAME { get; set; }
        //[DataMember] [DataNames("REQUISITIONER_EMAIL")] public string REQUISITIONER_EMAIL { get; set; }
        [DataMember] [DataNames("PR_NOTE")] public string PR_NOTE { get; set; }
        //[DataMember] [DataNames("WBS_CODE")] public string WBS_CODE { get; set; }
        //[DataMember] [DataNames("PROJECT_DESCRIPTION")] public string PROJECT_DESCRIPTION { get; set; }
        //[DataMember] [DataNames("SECTION_HEAD")] public string SECTION_HEAD { get; set; }
        //[DataMember] [DataNames("PROJECT_TYPE")] public string PROJECT_TYPE { get; set; }
        //[DataMember] [DataNames("SUB_VERTICAL")] public string SUB_VERTICAL { get; set; }
        

        [DataMember] [DataNames("ACCOUNT_ASSIGNMENT_CATEGORY")] public string ACCOUNT_ASSIGNMENT_CATEGORY { get; set; }


        [DataMember] [DataNames("LOEKZ")] public string LOEKZ { get; set; }
        [DataMember] [DataNames("ERDAT")] public string ERDAT { get; set; }
        [DataMember] [DataNames("EBAKZ")] public string EBAKZ { get; set; }
        [DataMember] [DataNames("REQUISITIONERS")] public string REQUISITIONERS { get; set; }

        /********  CONSOLIDATE PR ********/

        [DataMember] [DataNames("REQUIRED_QUANTITY")] public decimal REQUIRED_QUANTITY { get; set; }
        [DataMember] [DataNames("PRODUCT_ID")] public int PRODUCT_ID { get; set; }
        [DataMember] [DataNames("TOTAL_PR_ITEM_QUANTITY")] public decimal TOTAL_PR_ITEM_QUANTITY { get; set; }

        [DataMember] [DataNames("UOM")] public string UOM { get; set; }
        [DataMember] [DataNames("REFERRED_PR_ITEM_IDS")] public string REFERRED_PR_ITEM_IDS { get; set; }

        /********  CONSOLIDATE PR ********/

        /******** SHUFFLED COLUMNS *******/
        [DataMember] [DataNames("GMP")] public string GMP { get; set; }
        [DataMember] [DataNames("PR_CREATOR_NAME")] public string PR_CREATOR_NAME { get; set; }
        [DataMember] [DataNames("TOTAL_PR_COUNT")] public int TOTAL_PR_COUNT { get; set; }
        [DataMember] [DataNames("newPRs")] public int newPRs { get; set; }
        [DataMember] [DataNames("partialPRs")] public int partialPRs { get; set; }
        [DataMember] [DataNames("totalPRItems")] public int totalPRItems { get; set; }
        [DataMember] [DataNames("totalRFQPosted")] public int totalRFQPosted { get; set; }
        [DataMember] [DataNames("inProgressPRs")] public int inProgressPRs { get; set; }
        [DataMember] [DataNames("NEW_PR_STATUS")] public string NEW_PR_STATUS { get; set; }
        [DataMember] [DataNames("ARCHIVED_DATE")] public DateTime? ARCHIVED_DATE { get; set; }
        [DataMember] [DataNames("ARCHIVED_BY")] public int ARCHIVED_BY { get; set; }
        [DataMember] [DataNames("ARCHIVED_BY_NAME")] public string ARCHIVED_BY_NAME { get; set; }
        /******** SHUFFLED COLUMNS *******/
    }
}