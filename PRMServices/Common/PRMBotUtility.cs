﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using PRMServices.Models;
using PRMServices.SQLHelper;

namespace PRMServices.Common
{
    public static class PRMBotUtility
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();        
        public static bool SendUserBotOTP(string userLoginId, long chatId, long chatUserId, bool isEmail)
        {
            bool status = false;
            int OTP = new Random().Next(1000000, 9999999);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            string uniqueID = Guid.NewGuid().ToString();
            sd.Add("P_CHAT_ID", chatId);
            sd.Add("P_CHAT_USER_ID", chatUserId);
            sd.Add("P_LOGIN_ID", userLoginId);
            sd.Add("P_OTP", OTP);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("bot_ConfirmUserIdForBot", sd);
            var body = $@"You OTP {OTP} for the BOT authentication.";
            PRMServices services = new PRMServices();
            if (isEmail)
            {
                services.SendEmail(userLoginId, "Password Reset Request", body, 0, 0, null, null, null).ConfigureAwait(false);
            }
            else
            {
               // services.SendSMS(string.Empty, userLoginId, body, 0, 0, string.Empty).ConfigureAwait(false);
            }

            return status;
        }

        public static Response ValidateUserBotOTP(long chatId, long chatUserId, string otp)
        {
            Response response = new Response();
            string message = "";
            UserInfo user = null;
            int userId = 0;
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            string uniqueID = Guid.NewGuid().ToString();
            sd.Add("P_CHAT_ID", chatId);
            sd.Add("P_CHAT_USER_ID", chatUserId);
            sd.Add("P_OTP", otp);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("bot_ValidateOTPForBot", sd);
            if (ds != null && ds.Tables.Count > 0)
            {
                userId = Convert.ToInt32(ds.Tables[0].Rows[0]["U_ID"]);
                PRMServices services = new PRMServices();
                user = services.GetUser(userId, "");
            }
            if(userId> 0 && user!=null)
            {
                message = $@"Welcome {user.FirstName}, we recognize you as {user.UserType} from company: {user.Institution}.
Please give us few more minutes to bring us requirements that are scheduled today.";
            }
            else
            {
                message = $@"Apolgies, we count validate with entered details. Please contact support.";
            }

            response.Message = message;
            response.UserInfo = user;
            response.ObjectID = userId;


            return response;
        }

        public static string GetUserRequirements(long chatId, long chatUserId)
        {
            string message = "No requirements scheduled today.";
            int userId = 0;
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            string uniqueID = Guid.NewGuid().ToString();
            sd.Add("P_CHAT_ID", chatId);
            sd.Add("P_CHAT_USER_ID", chatUserId);
            DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("bot_GetUserIdbyChatDetails", sd);
            if (ds != null && ds.Tables.Count > 0)
            {
                userId = Convert.ToInt32(ds.Tables[0].Rows[0]["U_ID"]);
            }

            if(userId > 0)
            {
                PRMServices services = new PRMServices();
                var requirements = services.GetMyAuctions(userId,"","", "","","",0,0,"");
                if (requirements!=null && requirements.Count > 0)
                {
                    requirements = requirements.OrderByDescending(r => r.StartTime).Take(5).ToList();
                    message = "";
                    foreach (var requirement in requirements)
                    {
                        string title = requirement.Title;
                        string desc = requirement.Description;
                        int itemsCount = requirement.ListRequirementItems != null ? requirement.ListRequirementItems.Count : 0;
                        message += $@"<b>Title: </b>{title}
<b>Description: </b>{requirement.Description}
<b>Start Time: </b>{requirement.StartTime}    
<b>Click for details: /REQ{requirement.RequirementID}</b>

" + Environment.NewLine;
                        //if (itemsCount > 0)
                        //{
                        //    string itemsHTML = "<tr><td colspan\"3\"><table border=\"1\" cellpading=0 cellspacing=0>";
                        //    foreach(var item in requirement.ListRequirementItems)
                        //    {
                        //        itemsHTML += "<tr>";
                        //        itemsHTML += "<td><b>"+item.ProductCode+ "</b></td>";
                        //        itemsHTML += "<td><b>" + item.ProductIDorName + "</b></td>";
                        //        itemsHTML += "<td>" + item.ProductQuantity + " (" + item.ProductQuantityIn + ")</td>";
                        //        itemsHTML += "</tr>";
                        //    }

                        //    itemsHTML += "</table></td></tr>";
                        //    message += itemsHTML;
                        //}
                    }
                }
            }
            
            return message;
        }

        public static string GetUserRequirementDetails(long chatId, long chatUserId, int reqid)
        {
            string message = "No detaiils found.";
            if (reqid > 0)
            {
                int userId = 0;
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                string uniqueID = Guid.NewGuid().ToString();
                sd.Add("P_CHAT_ID", chatId);
                sd.Add("P_CHAT_USER_ID", chatUserId);
                DataSet ds = DatabaseProvider.GetDatabaseProvider().SelectList("bot_GetUserIdbyChatDetails", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    userId = Convert.ToInt32(ds.Tables[0].Rows[0]["U_ID"]);
                }

                PRMServices services = new PRMServices();
                var requirementDetails = services.GetRequirementData(reqid, userId, "");
                if (requirementDetails != null)
                {
                    message = $@"<b>Title: </b>{requirementDetails.Title}
                    <b>Start Time: </b>{requirementDetails.StartTime}
                    <b>Quotation Time: </b>{requirementDetails.QuotationFreezTime}
                    <b>Item Details</b>
                    " + Environment.NewLine;

                    foreach (var item in requirementDetails.ListRequirementItems)
                    {
                        if (item != null)
                        {
                            message += $@"<b>Product Name:</b> {item.ProductIDorName}" + Environment.NewLine;
                            message += $@"<b>Quanity:</b> {item.ProductQuantity} ({item.ProductQuantityIn})" + Environment.NewLine;
                        }
                    }

                    message += Environment.NewLine;

                    message += $@"<b>Quotation Details:</b>" + Environment.NewLine;

                    foreach (var vendor in requirementDetails.AuctionVendors)
                    {
                        requirementDetails.AuctionVendors = requirementDetails.AuctionVendors.OrderBy(a => a.QuotationUrl).ToList();
                        if (vendor != null)
                        {
                            string status = "";
                            if(vendor.IsQuotationRejected == 0)
                            {
                                status = "APPROVED";
                            }

                            if (vendor.IsQuotationRejected > 0)
                            {
                                status = "REJECTED";
                            }

                            if (vendor.IsQuotationRejected < 0)
                            {
                                status = "PENDING APPROVAL";
                            }

                            if (vendor.QuotationUrl != null)
                            {
                                message += $@"Vendor <b>{vendor.VendorName}</b> has uploaded and status {status}" + Environment.NewLine; ;
                            }
                            else
                            {
                                message += $@"Vendor <b>{vendor.VendorName}</b> has not uploaded." + Environment.NewLine; ;
                            }
                        }
                    }
                }
            }

            return message;
        }
    }
}