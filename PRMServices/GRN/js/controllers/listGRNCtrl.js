﻿prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('listGRNCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMGRNService", "poService",
        "PRMCustomFieldService", "fileReader", "PRMUploadServices",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMGRNService, poService, PRMCustomFieldService, fileReader, PRMUploadServices) {

            $scope.isGRN = $stateParams.isGRN;
            $scope.grnNo = $stateParams.grnNo;
            console.log($scope.isGRN);

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            //$scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.sessionID = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();

            $scope.grnStats = {
                totalgrns: 0
            };

            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;

            $scope.filtersList = {
                itemList: [],
                supplierNameList: []
            };

            $scope.filters = {
                itemName: {},
                supplier: {},
                grnFromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
                grnToDate: moment().format('YYYY-MM-DD')
            };

            //$scope.filters.grnToDate = moment().format('YYYY-MM-DD');
            //$scope.filters.grnFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getGrnList(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            $scope.totalItems2 = 0;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage2 = 10;
            $scope.maxSize2 = 5;

            $scope.setPage2 = function (pageNo) {
                $scope.currentPage1 = pageNo;
            };

            $scope.pageChanged2 = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            /*PAGINATION CODE*/

            $scope.exportPOItemsToExcel = function (type) {
                PRMUploadServices.GetExcelTemplate(type);
            };

            $scope.exportErrorDetails = function (type) {
                $state.go('moduleErrorList', { 'moduleName': type });
            };

            $scope.showErrorPopup = false;
            $scope.rowErrors = [];

            $scope.uploadExcel = function (type, proceedFurther, fileName) {
                $scope.showErrorPopup = false;
                $scope.FILE_NAME = fileName ? fileName : $scope.FILE_NAME;
                var params = {
                    name: type,
                    compID: $scope.compId,
                    sessionID: $scope.sessionID,
                    tableName: 'SAP_GRN_DETAILS',
                    attachment: $scope.attachment,
                    proceedFurther: proceedFurther,
                    fileName: $scope.FILE_NAME
                };
                PRMUploadServices.uploadTemplate(params)
                    .then(function (response) {
                        //if (response.errorMessage == '') {
                        //    location.reload();
                        //} else {
                        //    swal("Error!", response.errorMessage, "warning");
                        //    $("#grnclientupload").val(null);
                        //    $scope.getPaymentInvoiceDetails();
                        //}
                        if (response && response.length > 0) {
                            if (response[0].IS_SUCCESS) {
                                angular.element('#errorPopUp').modal('hide');
                                swal("Status!", "Total Count : " + response[0].TOTAL_COUNT + " \n " + " Successfull Count : " + response[0].SUCCESS_COUNT + " \n " + " Failed Count : " + response[0].FAILED_COUNT, "warning");
                                $("#grnclientupload").val(null);
                                $scope.getGrnList(0, 10, $scope.filters.searchKeyword);
                            } else {
                                if (!response[0].IS_SUCCESS && proceedFurther === 1) {
                                    $scope.showErrorPopup = true;
                                    $scope.rowErrors = response;
                                    angular.element('#errorPopUp').modal('show');
                                    if (response[0].IS_NO_RECORDS) {
                                        swal("Status!", "Sheet is empty.. So cannot continue further ! Please validate the sheet again and upload.", "error");
                                    } else {
                                        swal("Status!", "All the records in this sheet contains errors.. So cannot continue further ! Please validate the sheet again and upload.", "error");
                                    }
                                    $("#grnclientupload").val(null);
                                    angular.element('#errorPopUp').modal('hide');
                                } else {
                                    $scope.showErrorPopup = true;
                                    $scope.rowErrors = response;
                                    angular.element('#errorPopUp').modal('show');
                                    $("#grnclientupload").val(null);
                                }
                            }
                        } else {
                            $scope.uploadExcel('GRN_DETAILS', 1);
                        }
                    });
            };

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);
                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];

                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            $scope.attachment = $.makeArray(bytearray);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;

                            if (id == "grnclientupload") {
                                $scope.uploadExcel('GRN_DETAILS', 1, fileUpload.fileName);
                                $scope.file = [];
                                $scope.file.name = '';
                            }
                        });
                });
            };

            $scope.grnList = [];
            $scope.filteredGrnsList = [];
            $scope.grnItems = [];

            $('.selected-items-box').bind('click', function (e) {
                $('.multiple-selection-dropdown .list').slideToggle('fast');
            });
            var isFilter = false;
            $scope.setFilters = function (currentPage) {
                $scope.grnStats.totalgrns = 0;


                $scope.filteredGrnsList = $scope.grnList;
                $scope.totalItems = $scope.filteredGrnsList.length;


                if ($scope.filters.searchKeyword || !_.isEmpty($scope.filters.itemName) || !_.isEmpty($scope.filters.supplier)) {
                    $scope.getGrnList(0, 10, $scope.filters.searchKeyword);
                } else {

                    if ($scope.initialgrnPageArray && $scope.initialgrnPageArray.length > 0) {
                        $scope.grnList = $scope.initialgrnPageArray;
                        if ($scope.grnList && $scope.grnList.length > 0) {

                            $scope.totalItems = $scope.grnList[0].TOTAL_COUNT;
                            $scope.grnStats.totalgrns = $scope.totalItems;

                            $scope.filteredGrnsList = $scope.grnList;
                        }

                    }
                }



            };

            $scope.filterByDate = function () {
                $scope.grnStats.totalgrns = 0;


                $scope.filteredGrnsList = $scope.grnList;
                $scope.totalItems = $scope.filteredGrnsList.length;
                $scope.getGrnList(0, 10, $scope.filters.searchKeyword);

            };

            $scope.totalCount = 0;
            $scope.searchString = '';
            $scope.initialgrnPageArray = [];

            $scope.getGrnList = function (recordsFetchFrom, pageSize, searchString) {

                var itemName, supplier, grnFromDate, grnToDate;


                if (_.isEmpty($scope.filters.grnFromDate)) {
                    grnFromDate = '';
                } else {
                    grnFromDate = $scope.filters.grnFromDate;
                }

                if (_.isEmpty($scope.filters.grnToDate)) {
                    grnToDate = '';
                } else {
                    grnToDate = $scope.filters.grnToDate;
                }
                //if (_.isEmpty($scope.filters.itemName)) {
                //    itemName = '';
                //} else if ($scope.filters.itemName && $scope.filters.itemName.length > 0) {
                //    var itemNames = _($scope.filters.itemName)
                //        .filter(item => item.name)
                //        .map('name')
                //        .value();
                //    itemName = itemNames.join(',');
                //}


                if (_.isEmpty($scope.filters.supplier)) {
                    supplier = '';
                } else if ($scope.filters.supplier && $scope.filters.supplier.length > 0) {
                    var suppliers = _($scope.filters.supplier)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    supplier = suppliers.join(',');
                }


                var params = {
                    "compid": $scope.isCustomer ? $scope.compId : 0,
                    "uid": $scope.isCustomer ? 0 : $scope.userID,
                    "search": searchString ? searchString : "",
                    //"item": itemName,
                    "supplier": supplier,
                    "fromdate": $scope.isGRN ? '1970-01-01' : grnFromDate,
                    "todate": $scope.isGRN ? '2100-01-01' : grnToDate,
                    "page": recordsFetchFrom * pageSize,
                    "pagesize": pageSize,
                    "sessionid": userService.getUserToken()

                };

                $scope.pageSizeTemp = (params.page + 1);
                $scope.NumberOfRecords = ((recordsFetchFrom + 1) * pageSize);

                PRMGRNService.getGRNDetailsList(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                item.ITEM_RECEIVED_DATE = item.ITEM_RECEIVED_DATE ? moment(item.ITEM_RECEIVED_DATE).format("DD-MM-YYYY") : '-';
                                item.RECEIPT_DATE = $scope.GetDateconverted(item.RECEIPT_DATE);

                            });
                        }
                        $scope.grnList = [];
                        $scope.filteredGrnsList = [];
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                $scope.grnList.push(item);
                                if ($scope.initialgrnPageArray.length <= 9) { // Push Initial 10 Records When Page is Loaded because needed in SetFilters function it's getting called every time (need to modify directive code)  #Crap Code need to remove(should think of another solution)

                                    //var ifExists1 = _.findIndex($scope.initialgrnPageArray, function (po) { return po.PO_NUMBER === item.PO_NUMBER });
                                    //if (ifExists1 <= -1) {
                                    //    $scope.initialgrnPageArray.push(item);
                                    //}

                                    //$scope.initialgrnPageArray.push(item);
                                }
                            });

                        }

                        if ($scope.grnList && $scope.grnList.length > 0) {
                            $scope.totalItems = $scope.grnList[0].TOTAL_COUNT;
                            $scope.grnStats.totalgrns = $scope.totalItems;

                            $scope.filteredGrnsList = $scope.grnList;
                        }


                    });
            };


            if ($scope.isGRN) {
                $scope.getGrnList(0, 10, $scope.grnNo);
            } else {
                $scope.getGrnList(0, 10, $scope.searchString);
            }

            $scope.filterValues = [];

            $scope.getFilterValues = function () {
                var params =
                {
                    "compid": $scope.isCustomer ? $scope.compId : 0
                };

                let itemListTemp = [];
                let supplierNameListTemp = [];


                PRMGRNService.getPOScheduleFilterValues(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.filterValues = response;
                            if ($scope.filterValues && $scope.filterValues.length > 0) {
                                $scope.filterValues.forEach(function (item, index) {
                                    if (item.name === 'ITEM') {
                                        item.arrayPair.forEach(function (item) {
                                            itemListTemp.push({ id: item.key, name: item.value });
                                        });
                                        departmentListTemp.push({ id: item.arrayPair.key, name: item.arrayPair.value });
                                    } else if (item.name === 'VENDORS') {
                                        item.arrayPair.forEach(function (item) {
                                            supplierNameListTemp.push({ id: item.key, name: item.value });
                                        });
                                    }

                                });


                                $scope.filtersList.itemList = itemListTemp;
                                $scope.filtersList.supplierNameList = supplierNameListTemp;

                            }
                        }
                    });

            };
            $scope.getFilterValues();


            $scope.processThreeWayMatching = function () {
                let params = {
                    isProcessed: 1
                };

                PRMGRNService.ProcessThreeWayMatching(params)
                    .then(function (response) {
                        if (response.errorMessage === '') {
                            growlService.growl('Successfully processed.', "success");
                        }
                        else {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    });
            };

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };









        }]);