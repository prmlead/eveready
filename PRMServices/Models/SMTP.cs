﻿using System;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class SMTP
    {

        [DataMember] [DataNames("SMTP_CLIENT_TYPE")] public string SMTP_CLIENT_TYPE { get; set; }
        [DataMember] [DataNames("MAILHOST")] public string MAILHOST { get; set; }
        [DataMember] [DataNames("MAILHOST_USER")] public string MAILHOST_USER { get; set; }
        [DataMember] [DataNames("MAILHOST_PWD")] public string MAILHOST_PWD { get; set; }
        [DataMember] [DataNames("MAILHOST_PORT")] public int MAILHOST_PORT { get; set; }
        [DataMember] [DataNames("MAILHOST_SSL_ENABLED")] public string MAILHOST_SSL_ENABLED { get; set; }
        [DataMember] [DataNames("SEND_GRID_KEY")] public string SEND_GRID_KEY { get; set; }
        [DataMember] [DataNames("PREFERRED_LOGIN")] public string PREFERRED_LOGIN { get; set; }
        [DataMember] [DataNames("COMPANY_NAME")] public string COMPANY_NAME { get; set; }
        [DataMember] [DataNames("FROM_ADDRESS")] public string FROM_ADDRESS { get; set; }
        [DataMember] [DataNames("DISPLAY_NAME")] public string DISPLAY_NAME { get; set; }
        [DataMember] [DataNames("EMAIL_BODY")] public string EMAIL_BODY { get; set; }
    }

    [DataContract]
    public class EMAIL
    {
        [DataMember] [DataNames("VIE_ID")] public int VIE_ID { get; set; }
        [DataMember] [DataNames("JOB_ID")] public string JOB_ID { get; set; }
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("EMAIL_ID")] public string EMAIL_ID { get; set; }
        [DataMember] [DataNames("FIRST_NAME")] public string FIRST_NAME { get; set; }
        [DataMember] [DataNames("LAST_NAME")] public string LAST_NAME { get; set; }
        [DataMember] [DataNames("LG_LOGIN")] public string LG_LOGIN { get; set; }
        [DataMember] [DataNames("LG_PASSWORD")] public string LG_PASSWORD { get; set; }
        [DataMember] [DataNames("COMP_NAME")] public string COMP_NAME { get; set; }
        [DataMember] [DataNames("SENT_EMAIL")] public int SENT_EMAIL { get; set; }
        [DataMember] [DataNames("ERROR_REASON")] public string ERROR_REASON { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime? DATE_MODIFIED { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }
    }
}