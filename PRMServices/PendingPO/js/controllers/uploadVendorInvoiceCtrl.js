﻿prmApp
    .controller('uploadVendorInvoiceCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPOService", "poService",
        "PRMCustomFieldService", "fileReader", "$uibModal", "$filter","workflowService",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter, workflowService) {
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.showAllInvoices = $stateParams.ID ? +$stateParams.ID : 0;
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
            $scope.myAuctions1 = [];
            $scope.myAuctionsFiltred = [];
            $scope.selectedPRItems = [];
            $scope.prmTemplates = [];
            $scope.selectedTemplate = {};
            $scope.selectedRFP;
            $scope.selectedPR;
            $scope.filteredRequirements = [];
            $scope.invoiceID = $scope.showAllInvoices;
            $scope.WorkflowModule = 'VENDOR_INVOICE';
            $scope.onlyContracts = $stateParams.onlyContracts ? 1 : 0;
            $scope.excludeContracts = $stateParams.excludeContracts ? 1 : 0;
            if (!$scope.onlyContracts && !$scope.excludeContracts) {
                $scope.onlyContracts = 0;
                $scope.excludeContracts = 0;
                if (window.location.href && window.location.href.toLowerCase().indexOf('list-pendingcontracts') >= 0) { //Backup on F5.
                    $scope.onlyContracts = 1;
                } else {
                    $scope.excludeContracts = 1;
                }
            }
            $scope.isAttachmentError = false;

            $scope.companyLocation1 =
                [
                    {
                        FIELD_NAME: 'Chirala',
                        FIELD_VALUE: 'Chirala'
                    },
                    {
                        FIELD_NAME: 'Anaparthy',
                        FIELD_VALUE: 'Anaparthy'
                    },
                    {
                        FIELD_NAME: 'Mysore',
                        FIELD_VALUE: 'Mysore'
                    },
                    {
                        FIELD_NAME: 'Guntur',
                        FIELD_VALUE: 'Guntur'
                    }
                ];

            $scope.pendingPOStats = {
                totalPendingPOs: 0,
                totalPOs: 0,
                totalAwaitingRecipt: 0,
                totalPOsNotInitiated: 0,
                totalPartialDeliverbles: 0
            };
            $scope.prExcelReport = [];
            $scope.invoiceList = [];

            $scope.invoiceDetails = {
                invoiceNumber: '',
                invoiceAmount: '',
                invoiceComments: ''
            };

            $scope.selectedPODetails = [];
            $scope.selectedIndex = 0;

            $scope.filtersList = {
                departmentList: [],
                categoryidList: [],
                productidList: [],
                supplierList: [],
                poStatusList: [],
                deliveryStatusList: [],
                plantList: [],
                purchaseGroupList: [],
                subUserList: [],
                vendorAckStatus: []
            };

            $scope.filters = {
                department: {},
                categoryid: {},
                productid: {},
                supplier: {},
                poStatus: {},
                deliveryStatus: {},
                plant: {},
                purchaseGroup: {},
                pendingPOFromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
                pendingPOToDate: moment().format('YYYY-MM-DD'),
                subuser: {},
                ackStatus: {}
            };

            //$scope.filters.pendingPOToDate = moment().format('YYYY-MM-DD');
            //$scope.filters.pendingPOFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;
            
            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            $scope.totalItems2 = 0;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage2 = 10;
            $scope.maxSize2 = 5;

            $scope.setPage2 = function (pageNo) {
                $scope.currentPage1 = pageNo;
            };

            $scope.pageChanged2 = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            /*PAGINATION CODE*/


            $scope.pendingPOList = [];
            $scope.filteredPendingPOsList = [];
            $scope.filteredPendingPOsList = [{ multipleAttachments: [] }];
            $scope.pendingPOItems = [];
            
            $scope.getPOInvoiceDetails = function (invoiceId) {
                $scope.params = {
                    "ponumber": '',
                    "sessionID": userService.getUserToken(),
                    "vendorID": !$scope.isCustomer && invoiceId <=0 ? +$scope.userID : 0,
                    "invoiceID": invoiceId > 0 ? invoiceId : 0
                };

                PRMPOService.getPOInvoiceDetails($scope.params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                item.multipleAttachments = [];
                            });
                        }
                        $scope.invoiceList = response;

                        if (invoiceId > 0) {
                            $scope.invoiceList.forEach(function (item,index) {
                                $scope.selectedPODetails.PO_NUMBER = item.PO_NUMBER;
                                $scope.selectedPODetails.INVOICE_NUMBER = item.INVOICE_NUMBER;
                                $scope.selectedPODetails.LOCATION = item.LOCATION;
                                $scope.selectedPODetails.INVOICE_AMOUNT = item.INVOICE_AMOUNT;
                                $scope.selectedPODetails.COMMENTS = item.COMMENTS;
                                $scope.selectedPODetails.multipleAttachments = item.attachmentsArray;
                            });
                        }
                        $scope.totalItems = $scope.invoiceList.length;
                    });
            };

            $scope.getPOInvoiceDetails($scope.invoiceID);
            
            $scope.getFile1 = function (id, itemid, ext) {
                //$scope.filteredPendingPOsList = [{ multipleAttachments: [] }];
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);
                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];

                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;
                            if (!$scope.filteredPendingPOsList[id].multipleAttachments) {
                                $scope.filteredPendingPOsList[id].multipleAttachments = [];
                            }

                            var ifExists = _.findIndex($scope.filteredPendingPOsList[id].multipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                            if (ifExists <= -1) {
                                $scope.filteredPendingPOsList[id].multipleAttachments.push(fileUpload);
                            }

                        });
                });
            };

            $scope.removeAttach = function (index, item) {
                $scope.filteredPendingPOsList[0].multipleAttachments.splice(index, 1);
            };

            $scope.savePOInvoice = function (item, id) {
                var params1 = {
                    details: {
                        "PO_NUMBER": item.PO_NUMBER,
                        "VENDOR_CODE": item.VENDOR_CODE,
                        "VENDOR_ID": $scope.userID,
                        "INVOICE_NUMBER": item.INVOICE_NUMBER,
                        "INVOICE_AMOUNT": item.INVOICE_AMOUNT,
                        "attachmentsArray": $scope.filteredPendingPOsList[id] ? $scope.filteredPendingPOsList[id].multipleAttachments : [],
                        "COMMENTS": item.COMMENTS,
                        "STATUS": item.STATUS,
                        "SessionID": $scope.sessionID,
                        "LOCATION": item.LOCATION,
                        "INVOICE_ID": $scope.invoiceID,
                        "WF_ID": getWorkflowId($scope.invoiceID),
                        "ATTACHMENTS": getAttachments($scope.invoiceID)
                    }
                };

                if (!$scope.invoiceID) {
                    if (params1.details.attachmentsArray.length == 0) {
                        $scope.isAttachmentError = true;
                        return;
                    }
                    if (params1.details.PO_NUMBER == undefined || params1.details.INVOICE_NUMBER == undefined) {
                        return;
                    }
                    PRMPOService.savePOInvoice(params1)
                        .then(function (response) {

                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                growlService.growl("Saved Successfully.", "success");
                                location.reload();
                            }

                        });
                } else {
                    PRMPOService.editPOInvoice(params1)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                swal("Error!", response.errorMessage, "error");
                            }
                            else {
                                growlService.growl("Saved Successfully.", "success");
                                $scope.goToVendorInvoice(0);
                            }
                        });
                }
            };


            $scope.deletePOInvoice = function (poNumber, invoiceNumber, invoiceId, wfId) {
                $scope.showErrorMessage = false;
                $scope.params = {
                    "ponumber": poNumber,
                    "invoicenumber": invoiceNumber,
                    "invoiceId": invoiceId,
                    "wfId": wfId,
                    "sessionid": userService.getUserToken()
                };
                
                swal({
                    title: "Are you sure?",
                    text: "This Will Delete the Invoice.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    PRMPOService.deletePOInvoice($scope.params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                $scope.showErrorMessage = true;
                            }
                            else {
                                growlService.growl("Deleted Successfully.", "success");
                                location.reload();

                            }
                        });
                    });
            };

            $scope.showCreateInvPage = function (val,disp) {
                if (disp && disp === 'CREATE') {
                    $scope.showAllInvoices = 1;
                } else {
                    $scope.goToVendorInvoice(val);
                    $scope.selectedPODetails.PO_NUMBER = '';
                    $scope.selectedPODetails.INVOICE_NUMBER = '';
                    $scope.selectedPODetails.INVOICE_AMOUNT = 0;
                    $scope.selectedPODetails.LOCATION = '';
                    $scope.selectedPODetails.COMMENTS = '';
                    if ($scope.filteredPendingPOsList.length>0) {
                        $scope.filteredPendingPOsList[0].multipleAttachments = [];
                    }
                    $scope.isAttachmentError = false;
                    $scope.showAllInvoices = 0;

                }
            };

            $scope.isFormdisabled = false;

            if ($scope.isCustomer)
            {
                $scope.isFormdisabled = true;
                $scope.getItemWorkflow = function () {
                    workflowService.getItemWorkflow(0, $scope.invoiceID, $scope.WorkflowModule)
                        .then(function (response) {
                            $scope.itemWorkflow = response;
                            if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                                $scope.currentStep = 0;

                                var count = 0;

                                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                    if (!track.multipleAttachments) {
                                        track.multipleAttachments = [];
                                    }

                                    if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                    if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                    if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                    if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                                    if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                        $scope.isFormdisabled = true;
                                    }

                                    if (track.status === 'APPROVED') {
                                        $scope.isWorkflowCompleted = true;
                                        $scope.orderInfo = track.order;
                                        $scope.assignToShow = track.status;

                                    }
                                    else {
                                        $scope.isWorkflowCompleted = false;
                                    }

                                    if (track.status === 'REJECTED' && count == 0) {
                                        count = count + 1;
                                    }

                                    if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                        count = count + 1;
                                        //$scope.IsUserApproverForStage(track.approverID);
                                        $scope.currentAccess = track.order;
                                    }

                                    if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                        $scope.currentStep = track.order;
                                        return false;
                                    }
                                });
                            }
                        });
                };

                $scope.updateTrack = function (step, status) {
                    $scope.disableAssignPR = true;
                    $scope.commentsError = '';
                    if (step.comments != null || step.comments != "" || step.comments != undefined) {
                        step.comments = validateStringWithoutSpecialCharacters(step.comments);
                    }
                    var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                    if (step.order == tempArray.order && status === 'APPROVED') {
                        $scope.disableAssignPR = false;
                    } else {
                        $scope.disableAssignPR = true;
                    }

                    if ($scope.isReject) {
                        $scope.commentsError = 'Please Save Rejected Items/Qty';
                        return false;
                    }

                    if (status === 'REJECTED' && (step.comments == null || step.comments == "")) {
                        $scope.commentsError = 'Please enter comments';
                        return false;
                    }

                    step.status = status;
                    step.sessionID = $scope.sessionID;
                    step.modifiedBy = userService.getUserId();

                    step.moduleName = $scope.WorkflowModule;

                    step.subModuleName = '';
                    step.subModuleID = 0;

                    workflowService.SaveWorkflowTrack(step)
                        .then(function (response) {
                            if (response.errorMessage) {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                $scope.getItemWorkflow();
                                location.reload();
                            }
                        });
                };

                $scope.getItemWorkflow();

                function validateStringWithoutSpecialCharacters(string) {
                    if (string) {
                        string = string.replace(/\'/gi, "");
                        string = string.replace(/\"/gi, "");
                        string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                        string = string.replace(/(\r\n|\n|\r)/gm, "");
                        string = string.replace(/\t/g, '');
                        return string;
                    }
                }

                $scope.isApproverDisable = function (index) {

                    var disable = true;

                    var previousStep = {};

                    $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                        if (index == stepIndex) {
                            if (stepIndex == 0) {
                                if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                    (step.status === 'PENDING' || step.status === 'HOLD')) {
                                    disable = false;
                                }
                                else {
                                    disable = true;
                                }
                            }
                            else if (stepIndex > 0) {
                                if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                    disable = true;
                                }
                                else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                    (step.status === 'PENDING' || step.status === 'HOLD')) {
                                    disable = false;
                                }
                                else {
                                    disable = true;
                                }
                            }
                        }
                        previousStep = step;
                    });

                    return disable;
                };


                $scope.deptIDs = [];
                $scope.desigIDs = [];
                $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
                if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                    $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                        $scope.deptIDs.push(item.deptID);
                        item.listDesignation.forEach(function (item1, index1) {
                            if (item1.isAssignedToUser && item1.isValid) {
                                $scope.desigIDs.push(item1.desigID);
                            }
                        });
                    });
                }


                $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                    var isEligible = true;

                    if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                        isEligible = true;
                    } else {
                        isEligible = false;
                    }

                    return isEligible;
                };

                $scope.showApprovedDate = function (date) {
                    return userService.toLocalDate(date);
                };
            }
            
            $scope.goToVendorInvoice = function (invoiceId) {
                var url4 = $state.href('uploadvendorinvoice', { "ID": invoiceId });
                $window.open(url4, '_self');
            };


            function getWorkflowId(invoiceId) {
                var wfId = 0;
                wfId = (invoiceId && invoiceId > 0) ? _.find($scope.invoiceList, { INVOICE_ID: invoiceId }).WF_ID : 0;
                return wfId;
            }

            function getAttachments(invoiceId) {
                var attachments = '';
                attachments = (invoiceId && invoiceId > 0) ? _.find($scope.invoiceList, { INVOICE_ID: invoiceId }).ATTACHMENTS : 0;
                return attachments;
            }

        }]);