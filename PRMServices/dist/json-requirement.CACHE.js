﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider

            .state('json-save-requirement', {
                url: '/json-save-requirement/:REQ_ID',
                templateUrl: 'json-requirement/views/json-save-requirement.html'
            })

        }]);﻿prmApp

    .controller('json-save-requirement-ctrl', ["$scope", "$http", "$state", "domain", "$filter", "$log", "$stateParams",
        "$timeout", "auctionsService", "fwdauctionsService", "userService", "SignalRFactory", "fileReader", "growlService",
        "PRMJSONRequirementService",
        function ($scope, $http, $state, domain, $filter, $log, $stateParams, $timeout,
            auctionsService, fwdauctionsService, userService, SignalRFactory, fileReader, growlService,
            PRMJSONRequirementService) {
        

            $scope.jsonRequirement = {
                REQ_ID: 0,
                TITLE: '',
                COMMENTS: '',
                REQ_ATTACHMENTS: '',
                DELIVERY_TERMS: '',
                DELIVERY_LOCATION: '',
                PAYMENT_TERMS: '',
                CONTACT_DETAILS: '',
                GENERAL_TERMS_CONDITIONS: '',
                IS_CONTRACT_ENABLED: 0,
                CONTRACT_START_TIME: '',
                CONTRACT_END_TIME: '',
                SELECTED_DEPARTMENTS: '',
                URGENCY: '',
                CURRENCY: '',
                QUOTATION_FREEZE_TIME: '',
                NUMBER_OF_REMINDERS: 0,
                REMINDER_TIME_INTERVEL: 0,
                EXPECTED_NEGOTIATION_START_TIME: '',
                IS_QUOTATION_PRICE_LIMIT_ENABLED: 0,
                QUOTATION_PRICE_LIMIT: 0,
                REDUCTION_TYPE: 0,
                IS_SMS_ENABLED: 1,
                IS_EMAIL_ENABLED: 1,

                jsonRequirementVendors: [
                    {
                        VENDOR_ID: 0,
                        VENDOR_NAME: '',
                        VENDOR_COMPANY_NAME: '',

                        jsonRequirementQuotation: {
                            RI_ID: 0,
                            CATALOG_ITEM_ID: 0,
                            ITEM_NAME: '',
                            ITEM_CODE: '',
                            ITEM_NUMBER: '',
                            HSN_CODE: '',
                            QUANTITY: 0,
                            UNITS: '',
                            BRAND: '',
                            DESCRIPTION: '',
                            ITEM_ATTACHMENTS: '',

                            UNIT_PRICE: 0,
                            ITEM_PRICE: 0,

                            LIVE_UNIT_PRICE: 0,
                            LIVE_ITEM_PRICE: 0,

                            REV_UNIT_PRICE: 0,
                            REV_ITEM_PRICE: 0,

                            DATE_CREATED: '',
                            DATE_MODIFIED: '',
                            CREATED_BY: '',
                            MODIFIED: ''
                        },

                        DATE_CREATED: '',
                        DATE_MODIFIED: '',
                        CREATED_BY: '',
                        MODIFIED: ''
                    }
                ],

                jsonRequirementItems: [
                    {
                        RI_ID: 0,
                        CATALOG_ITEM_ID: 0,
                        ITEM_NAME: '',
                        ITEM_CODE: '',
                        ITEM_NUMBER: '',
                        HSN_CODE: '',
                        QUANTITY: 0,
                        UNITS: '',
                        BRAND: '',
                        DESCRIPTION: '',
                        ITEM_ATTACHMENTS: '',

                        DATE_CREATED: '',
                        DATE_MODIFIED: '',
                        CREATED_BY: '',
                        MODIFIED: ''
                    }
                ],

                DATE_CREATED: '',
                DATE_MODIFIED: '',
                CREATED_BY: '',
                MODIFIED_BY: ''

            };

            $scope.jsonRequirementItems = {
                RI_ID: 0,
                CATALOG_ITEM_ID: 0,
                ITEM_NAME: '',
                ITEM_CODE: '',
                ITEM_NUMBER: '',
                HSN_CODE: '',
                QUANTITY: 0,
                UNITS: '',
                BRAND: '',
                DESCRIPTION: '',
                ITEM_ATTACHMENTS: '',

                DATE_CREATED: '',
                DATE_MODIFIED: '',
                CREATED_BY: '',
                MODIFIED: ''
            };


            $scope.JsonRequirementStructure = [];

            $scope.GetJsonRequirementStructure = function () {
                var params = {
                    "sessionid": userService.getUserToken(),
                };

                PRMJSONRequirementService.GetJsonRequirementStructure(params)
                    .then(function (response) {
                        $scope.JsonRequirementStructure = response;
                        console.log($scope.JsonRequirementStructure);
                    })

            };

            $scope.GetJsonRequirementStructure();


    }]);prmApp.constant('PRMJSONRequirementServiceDomain', 'json-requirement/svc/PRMJSONRequirementService.svc/REST/');

prmApp.service('PRMJSONRequirementService', ["PRMJSONRequirementServiceDomain", "userService", "httpServices",
    function (PRMJSONRequirementServiceDomain, userService, httpServices) {
        var PRMJSONRequirementService = this;

        PRMJSONRequirementService.GetJsonRequirementStructure = function (params) {
            let url = PRMJSONRequirementServiceDomain + 'getjsonrequirementstructure?sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMJSONRequirementService.SavePriceCap = function (params) {
            let url = PRMJSONRequirementServiceDomain + 'savepricecap';
            return httpServices.post(url, params);
        };
            
        return PRMJSONRequirementService;

}]);