﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('list-pendingPO', {
                    url: '/list-pendingPO',
                    templateUrl: 'PendingPO/views/list-pendingPO.html',
                    params: {
                        excludeContracts: true,
                        onlyContracts: false,
                        detailsObj: null
                    }
                })
                .state('list-pendingContracts', {
                    url: '/list-pendingContracts',
                    templateUrl: 'PendingPO/views/list-pendingPO.html',
                    params: {
                        excludeContracts: false,
                        onlyContracts: true,
                        detailsObj: null
                    }
                })
                .state('list-pendingPOOverall', {
                    url: '/list-pendingPOOverall/:poID',
                    templateUrl: 'PendingPO/views/list-pendingPOOverall.html',
                    params: {
                        detailsObj: null
                    }
                })

                .state('uploadvendorinvoice', {
                    url: '/uploadvendorinvoice/:ID',
                    templateUrl: 'PendingPO/views/uploadvendorinvoice.html'
                });

        }]);﻿prmApp
    .controller('listPendingPOCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPOService", "poService",
        "PRMCustomFieldService", "fileReader", "$uibModal", "$filter",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter) {
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            //$scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.sessionID = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.myAuctions1 = [];
            $scope.myAuctionsFiltred = [];
            $scope.selectedPRItems = [];
            $scope.prmTemplates = [];
            $scope.selectedTemplate = {};
            $scope.selectedRFP;
            $scope.selectedPR;
            $scope.filteredRequirements = [];
            $scope.onlyContracts = $stateParams.onlyContracts ? 1 : 0;
            $scope.excludeContracts = $stateParams.excludeContracts ? 1 : 0;
            if (!$scope.onlyContracts && !$scope.excludeContracts) {
                $scope.onlyContracts = 0;
                $scope.excludeContracts = 0;
                if (window.location.href && window.location.href.toLowerCase().indexOf('list-pendingcontracts') >= 0) { //Backup on F5.
                    $scope.onlyContracts = 1;
                } else {
                    $scope.excludeContracts = 1;
                }
            }
            $scope.attachmentError = false;
            $scope.pendingPOStats = {
                totalPendingPOs: 0,
                totalPOs: 0,
                totalAwaitingRecipt: 0,
                totalPOsNotInitiated: 0,
                totalPartialDeliverbles: 0
            };
            $scope.prExcelReport = [];
            $scope.invoiceList = [];

            $scope.invoiceDetails = {
                invoiceNumber: '',
                invoiceAmount: '',
                invoiceComments: ''
            };

            $scope.selectedPODetails = [];
            $scope.selectedIndex = 0;
            //$scope.isVendor = userService.getUserType() === "VENDOR" ? true : false;
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;

            $scope.filtersList = {
                departmentList: [],
                categoryidList: [],
                productidList: [],
                supplierList: [],
                poStatusList: [],
                deliveryStatusList: [],
                plantList: [],
                purchaseGroupList: [],
                subUserList: [],
                vendorAckStatus: []
            };

            $scope.filters = {
                department: {},
                categoryid: {},
                productid: {},
                supplier: {},
                poStatus: {},
                deliveryStatus: {},
                plant: {},
                purchaseGroup: {},
                pendingPOFromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
                pendingPOToDate: moment().format('YYYY-MM-DD'),
                subuser: {},
                ackStatus: {}
            };

            //$scope.filters.pendingPOToDate = moment().format('YYYY-MM-DD');
            //$scope.filters.pendingPOFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.subUsers = [];
            $scope.inactiveSubUsers = [];
            $scope.subUsers1 = [];

            $scope.getSubUserData = function () {
                userService.getSubUsersData({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {

                        $scope.subUsers = $filter('filter')(response, { isValid: true });

                        $scope.subUsers.forEach(function (user, userIndex) {
                            subUserListTemp.push({ id: user.userID, name: user.firstName + " " + user.lastName });
                        })
                        $scope.filtersList.subUserList = subUserListTemp;
                    });
            };

            // $scope.getSubUserData();

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getpendingPOlist(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            $scope.totalItems2 = 0;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage2 = 10;
            $scope.maxSize2 = 5;

            $scope.setPage2 = function (pageNo) {
                $scope.currentPage1 = pageNo;
            };

            $scope.pageChanged2 = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            /*PAGINATION CODE*/


            $scope.pendingPOList = [];
            $scope.filteredPendingPOsList = [];
            $scope.pendingPOItems = [];

            $('.selected-items-box').bind('click', function (e) {
                $('.multiple-selection-dropdown .list').slideToggle('fast');
            });
            var isFilter = false;
            $scope.setFilters = function (currentPage) {
                $scope.pendingPOStats.totalPendingPOs = 0;
                $scope.pendingPOStats.totalPOs = 0;
                $scope.pendingPOStats.totalAwaitingRecipt = 0;
                $scope.pendingPOStats.totalPOsNotInitiated = 0;
                $scope.pendingPOStats.totalPartialDeliverbles = 0;

                $scope.filteredPendingPOsList = $scope.pendingPOList;
                $scope.totalItems = $scope.filteredPendingPOsList.length;
                $scope.getpendingPOlist(0, 10, $scope.filters.searchKeyword);

                if ($scope.filters.searchKeyword || !_.isEmpty($scope.filters.department) || !_.isEmpty($scope.filters.categoryid) || !_.isEmpty($scope.filters.productid) ||
                    !_.isEmpty($scope.filters.supplier) || !_.isEmpty($scope.filters.poStatus) || !_.isEmpty($scope.filters.ackStatus) ||
                    !_.isEmpty($scope.filters.deliveryStatus) || !_.isEmpty($scope.filters.plant) || !_.isEmpty($scope.filters.purchaseGroup) || !_.isEmpty($scope.filters.subuser)) {
                    //$scope.getpendingPOlist(0, 10, $scope.filters.searchKeyword);
                } else {

                    if ($scope.initialPendingPOPageArray && $scope.initialPendingPOPageArray.length > 0) {
                        $scope.pendingPOList = $scope.initialPendingPOPageArray;
                        if ($scope.pendingPOList && $scope.pendingPOList.length > 0) {
                            //$scope.totalItems = $scope.pendingPOList[0].TOTAL_PENDING_PO_COUNT;
                            //$scope.pendingPOStats.totalPendingPOs = $scope.totalItems;
                            //$scope.pendingPOStats.totalPOs = $scope.pendingPOList[0].totalPOs;
                            //$scope.pendingPOStats.totalAwaitingRecipt = $scope.pendingPOList[0].totalAwaitingRecipt;
                            //$scope.pendingPOStats.totalPOsNotInitiated = $scope.pendingPOList[0].totalPOsNotInitiated;
                            //$scope.pendingPOStats.totalPartialDeliverbles = $scope.pendingPOList[0].totalPartialDeliverbles;
                            //$scope.filteredPendingPOsList = $scope.pendingPOList;


                            $scope.totalItems = $scope.pendingPOList[0].TOTAL_COUNT;
                            $scope.pendingPOStats.totalPendingPOs = $scope.totalItems;
                            $scope.pendingPOStats.totalPOs = $scope.pendingPOList[0].STATS_TOTAL_COUNT;
                            $scope.pendingPOStats.totalAwaitingRecipt = $scope.pendingPOList[0].STATS_PO_AWAITING_RECEIPT;
                            $scope.pendingPOStats.totalPOsNotInitiated = $scope.pendingPOList[0].STATS_PO_NOT_INITIATED;
                            $scope.pendingPOStats.totalPartialDeliverbles = $scope.pendingPOList[0].STATS_PO_PARTIAL_DELIVERY;
                            $scope.filteredPendingPOsList = $scope.pendingPOList;
                        }

                    }
                }

            };

            $scope.filterByDate = function () {
                $scope.pendingPOStats.totalPendingPOs = 0;
                $scope.pendingPOStats.totalPOs = 0;
                $scope.pendingPOStats.totalAwaitingRecipt = 0;
                $scope.pendingPOStats.totalPOsNotInitiated = 0;
                $scope.pendingPOStats.totalPartialDeliverbles = 0;

                $scope.filteredPendingPOsList = $scope.pendingPOList;
                $scope.totalItems = $scope.filteredPendingPOsList.length;
                $scope.getpendingPOlist(0, 10, $scope.filters.searchKeyword);

            };

            $scope.totalCount = 0;
            $scope.searchString = '';
            $scope.initialPendingPOPageArray = [];

            $scope.getpendingPOlist = function (recordsFetchFrom, pageSize, searchString, isExport) {

                var department, categoryid, productid, supplier, poStatus, ackStatus, deliveryStatus, plant, purchaseGroup, pendingPOFromDate, pendingPOToDate, buyer;


                if (_.isEmpty($scope.filters.pendingPOFromDate)) {
                    pendingPOFromDate = '';
                } else {
                    pendingPOFromDate = $scope.filters.pendingPOFromDate;
                }

                if (_.isEmpty($scope.filters.pendingPOToDate)) {
                    pendingPOToDate = '';
                } else {
                    pendingPOToDate = $scope.filters.pendingPOToDate;
                }
                if (_.isEmpty($scope.filters.plant)) {
                    plant = '';
                } else if ($scope.filters.plant && $scope.filters.plant.length > 0) {
                    var plants = _($scope.filters.plant)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    plant = plants.join(',');
                }



                if (_.isEmpty($scope.filters.categoryid)) {
                    categoryid = '';
                } else if ($scope.filters.categoryid && $scope.filters.categoryid.length > 0) {
                    var categories = _($scope.filters.categoryid)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    categoryid = categories.join(',');
                }

                if (_.isEmpty($scope.filters.productid)) {
                    productid = '';
                } else if ($scope.filters.productid && $scope.filters.productid.length > 0) {
                    var productids = _($scope.filters.productid)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    productid = productids.join(',');
                }

                if (_.isEmpty($scope.filters.supplier)) {
                    supplier = '';
                } else if ($scope.filters.supplier && $scope.filters.supplier.length > 0) {
                    var suppliers = _($scope.filters.supplier)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    supplier = suppliers.join(',');
                }

                if (_.isEmpty($scope.filters.poStatus)) {
                    poStatus = '';
                } else if ($scope.filters.poStatus && $scope.filters.poStatus.length > 0) {
                    var poStatuses = _($scope.filters.poStatus)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    poStatus = poStatuses.join(',');
                }

                if (_.isEmpty($scope.filters.ackStatus)) {
                    ackStatus = '';
                } else if ($scope.filters.ackStatus && $scope.filters.ackStatus.length > 0) {
                    var ackStatuses = _($scope.filters.ackStatus)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    ackStatus = ackStatuses.join(',');
                }

                if (_.isEmpty($scope.filters.deliveryStatus)) {
                    deliveryStatus = '';
                } else if ($scope.filters.deliveryStatus && $scope.filters.deliveryStatus.length > 0) {
                    var deliveryStatuses = _($scope.filters.deliveryStatus)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    deliveryStatus = deliveryStatuses.join(',');
                }

                if (_.isEmpty($scope.filters.subuser)) {
                    buyer = '';
                } else if ($scope.filters.subuser && $scope.filters.subuser.length > 0) {
                    var buyers = _($scope.filters.subuser)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    buyer = buyers.join(',');
                }

                //if (_.isEmpty($scope.filters.purchaseGroup)) {
                //    purchaseGroup = '';
                //} else if ($scope.filters.purchaseGroup && $scope.filters.purchaseGroup.length > 0) {
                //    var purchaseGroups = _($scope.filters.purchaseGroup)
                //        .filter(item => item.name)
                //        .map('name')
                //        .value();
                //    purchaseGroup = purchaseGroups.join(',');
                //}

                var params = {
                    "compid": $scope.isCustomer ? $scope.compId : 0,
                    "uid": $scope.isCustomer ? 0 : $scope.userID,
                    "search": searchString ? searchString : "",
                    "categoryid": categoryid,
                    "productid": productid,
                    "supplier": supplier,
                    "postatus": poStatus,
                    "deliverystatus": deliveryStatus,
                    "plant": plant,
                    "fromdate": pendingPOFromDate,
                    "todate": pendingPOToDate,
                    "page": recordsFetchFrom * pageSize,
                    "pagesize": pageSize,
                    "onlycontracts": $scope.onlyContracts,
                    "excludecontracts": $scope.excludeContracts,
                    "ackStatus": ackStatus,
                    "buyer": buyer,
                    "purchaseGroup": '',
                    "isExport": isExport,
                    "sessionid": userService.getUserToken()
                };

                $scope.pageSizeTemp = (params.page + 1);
                $scope.NumberOfRecords = ((recordsFetchFrom + 1) * pageSize);

                PRMPOService.getPOScheduleList(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response = _.orderBy(response, ['PO_DATE'], ['desc']);
                            response.forEach(function (item, index) {
                                item.qcsCodesArray = [];
                                if (item.QCS_CODE)
                                {
                                    var values = item.QCS_CODE.split(',');
                                    values = values.filter(function (val) {
                                        return (+val > 0);
                                    });
                                    var uniqValues = _.uniqBy(values);
                                    item.qcsCodesArray = uniqValues;
                                }
                                item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_CLOSED_DATE = item.PO_CLOSED_DATE ? moment(item.PO_CLOSED_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_RELEASE_DATE = item.PO_RELEASE_DATE ? moment(item.PO_RELEASE_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_RECEIPT_DATE = item.PO_RECEIPT_DATE ? moment(item.PO_RECEIPT_DATE).format("DD-MM-YYYY") : '-';
                                item.VENDOR_EXPECTED_DELIVERY_DATE_LOCAL = item.VENDOR_EXPECTED_DELIVERY_DATE ? moment(item.VENDOR_EXPECTED_DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_DATE = item.PO_DATE ? moment(item.PO_DATE).format("DD-MM-YYYY") : '-';
                                item.multipleAttachments = [];
                                item.INVOICE_AMOUNT = 0;
                                item.INVOICE_NUMBER = '';
                                item.COMMENTS = '';
                                item.isAcknowledgeOverall = false;
                                item.fontStyle = {};
                                //item.MODIFIED_DATE = userService.toLocalDate(item.MODIFIED_DATE).split(' ')[0];
                            });
                        }
                        if (!$scope.downloadExcel) {
                            $scope.pendingPOList = [];
                            $scope.filteredPendingPOsList = [];
                            if (response && response.length > 0) {
                                response.forEach(function (item, index) {
                                    $scope.pendingPOList.push(item);
                                    if ($scope.initialPendingPOPageArray.length <= 9) { // Push Initial 10 Records When Page is Loaded because needed in SetFilters function it's getting called every time (need to modify directive code)  #Crap Code need to remove(should think of another solution)

                                        //var ifExists1 = _.findIndex($scope.initialPendingPOPageArray, function (po) { return po.PO_NUMBER === item.PO_NUMBER });
                                        //if (ifExists1 <= -1) {
                                        //    $scope.initialPendingPOPageArray.push(item);
                                        //}

                                        //$scope.initialPendingPOPageArray.push(item);
                                    }
                                });

                            }

                            if ($scope.pendingPOList && $scope.pendingPOList.length > 0) {
                                $scope.totalItems = $scope.pendingPOList[0].TOTAL_COUNT;
                                $scope.pendingPOStats.totalPendingPOs = $scope.totalItems;
                                $scope.pendingPOStats.totalPOs = $scope.pendingPOList[0].STATS_TOTAL_COUNT;
                                $scope.pendingPOStats.totalAwaitingRecipt = $scope.pendingPOList[0].STATS_PO_AWAITING_RECEIPT;
                                $scope.pendingPOStats.totalPOsNotInitiated = $scope.pendingPOList[0].STATS_PO_NOT_INITIATED;
                                $scope.pendingPOStats.totalPartialDeliverbles = $scope.pendingPOList[0].STATS_PO_PARTIAL_DELIVERY;
                                $scope.filteredPendingPOsList = $scope.pendingPOList;
                            }
                        } else {
                            if (response && response.length > 0) {
                                $scope.pendingPOExcelReport = response;
                                downloadPRExcel()
                            } else {
                                swal("Error!", "No records.", "error");
                                $scope.downloadExcel = false;
                            }
                        }



                    });
            };

            $scope.getpendingPOlist(0, 10, $scope.searchString);
            $scope.filterValues = [];

            $scope.getFilterValues = function () {
                var params =
                {
                    "compid": $scope.isCustomer ? $scope.compId : 0
                };

                let departmentListTemp = [];
                let categoryidListTemp = [];
                let productidListTemp = [];
                let supplierListTemp = [];
                let poStatusListTemp = [];
                let deliveryStatusListTemp = [];
                let plantListTemp = [];
                let purchaseGroupListTemp = [];
                let subUserListTemp = [];
                let vendorAckStatusTemp = [];

                PRMPOService.getPOScheduleFilterValues(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.filterValues = response;
                            if ($scope.filterValues && $scope.filterValues.length > 0) {
                                $scope.filterValues.forEach(function (item, index) {
                                    if (item.name === 'DEPARTMENT') {
                                        item.arrayPair.forEach(function (item) {
                                            departmentListTemp.push({ id: item.key, name: item.value });
                                        });
                                        departmentListTemp.push({ id: item.arrayPair.key, name: item.arrayPair.value });
                                    } else if (item.name === 'CATEGORY') {
                                        item.arrayPair.forEach(function (item) {
                                            categoryidListTemp.push({ id: item.key, name: item.value });
                                        });
                                    } else if (item.name === 'PRODUCT') {
                                        item.arrayPair.forEach(function (item) {
                                            productidListTemp.push({ id: item.key, name: item.value });
                                        });
                                    } else if (item.name === 'VENDORS') {
                                        item.arrayPair.forEach(function (item) {
                                            supplierListTemp.push({ id: item.key, name: item.value });
                                        });
                                    } else if (item.name === 'PO_STATUS') {
                                        item.arrayPair.forEach(function (item) {
                                            poStatusListTemp.push({ id: item.key, name: item.value });
                                        });
                                        //poStatusListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.name === 'PLANT') {
                                        item.arrayPair.forEach(function (item) {
                                            plantListTemp.push({ id: item.key1, name: item.key1 + ' - ' + item.value });
                                        });
                                    } else if (item.name === 'DELIVERY_STATUS') {
                                        deliveryStatusListTemp.push({ id: item.NAME, name: item.ID });
                                    } else if (item.name === 'PURCHASE_GROUP') {
                                        item.arrayPair.forEach(function (item) {
                                            purchaseGroupListTemp.push({ id: item.key, name: item.value });
                                        });

                                    } else if (item.name === 'PO_CREATOR') {
                                        item.arrayPair.forEach(function (item) {
                                            subUserListTemp.push({ id: item.key, name: item.value });
                                        });
                                    } else if (item.name === 'VENDOR_ACK_STATUS') {
                                        item.arrayPair.forEach(function (item) {
                                            vendorAckStatusTemp.push({ id: item.key, name: item.value });
                                        });
                                    }

                                });
                                //vendorAckStatusTemp.forEach(function (item) {
                                //    poStatusListTemp.push({ id: item.id, name: item.name });
                                //})

                                //$scope.filtersList.departmentList = departmentListTemp;
                                $scope.filtersList.plantList = plantListTemp;
                                $scope.filtersList.categoryidList = categoryidListTemp;
                                $scope.filtersList.productidList = productidListTemp;
                                $scope.filtersList.supplierList = supplierListTemp;
                                $scope.filtersList.poStatusList = poStatusListTemp;
                                $scope.filtersList.deliveryStatusList = deliveryStatusListTemp;
                                $scope.filtersList.purchaseGroupList = purchaseGroupListTemp;
                                $scope.filtersList.subUserList = subUserListTemp;
                                $scope.filtersList.vendorAckStatus = vendorAckStatusTemp;

                            }
                        }
                    });

            };
            $scope.getFilterValues();



            $scope.getPendingPOItems = function (pendingPODetails) {
                if (pendingPODetails) {
                    var params = {
                        "ponumber": pendingPODetails.PO_NUMBER,
                        "moredetails": 0,
                        "forasn":false
                    };
                    PRMPOService.getPOScheduleItems(params)
                        .then(function (response) {
                            $scope.pendingPOItems = response;
                            $scope.pendingPOItems.forEach(function (item, index) {
                                item.LAST_RECEIVED_DATE = item.LAST_RECEIVED_DATE ? moment(item.LAST_RECEIVED_DATE).format("DD-MM-YYYY") : '-';
                                item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_ITEM_CHANGE_DATE = item.PO_ITEM_CHANGE_DATE ? moment(item.PO_ITEM_CHANGE_DATE).format("DD-MM-YYYY") : '-';
                                if (pendingPODetails.isAcknowledgeOverall) {
                                    item.isAcknowledge = 1;
                                } else {
                                    item.isAcknowledge = 0;
                                }
                                if (pendingPODetails.isEdit1) {
                                    item.isEditField = true;
                                } else {
                                    item.isEditField = false;
                                }
                                item.isError = false;
                            });

                            pendingPODetails.pendingPOItems = $scope.pendingPOItems;


                        });
                }
            };


            $scope.downloadExcel = false;
            $scope.GetReport = function () {
                $scope.pendingPOList = [];
                $scope.downloadExcel = true;
                $scope.getpendingPOlist(0, 0, $scope.searchString, true);
            };



            function downloadPRExcel() {
                alasql('SELECT PO_NUMBER as [PO Number],VENDOR_CODE as [Vendor Code],VENDOR_COMPANY as [Vendor Name], ' +
                    'PO_CREATOR_NAME as [PO creator], GRN_STATUS as [GRN],VENDOR_SITE_CODE as [Vendor Site Code], ' +
                    'BILL_TO_LOCATION as [Bill To Location],SHIP_TO_LOCATION as [Ship To Location], ' +
                    'PO_DATE as [Creation Date],PAYMENT_TERMS as [Terms Name],PO_RELEASE_DATE as [Approve Date],DELIVERY_DATE as [Need By Date],TOTAL_VALUE as [Total Base Value],TOT_PO_AMT as [Total PO Amount],CURRENCY as [Currency Code],PO_STATUS as [PO Status],' +
                    'PRODUCT_CODE as [Material Code],PRODUCT_NAME as [Item Name],UOM as [UOM],ORDER_QTY as [Ordered Qty],RECEIVED_QTY as [Received Qty],REJECTED_QTY as [Rejected Qty],NET_PRICE as [Unit Price],LINE_BASE_AMT as [Line Base Amount],TAX_CODE_DESC as [Tax Rate Name],PR_NUMBER as [PR Number],QCS_ID as [QCS Id]' +
                    'INTO XLSX(?, { headers: true, sheetid: "PO_DETAILS", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                    ["PO Details.xlsx", $scope.pendingPOExcelReport]);
                $scope.downloadExcel = false;
            }


            $scope.scrollWin = function (id, scrollPosition) {

                var elmnt = document.getElementById(id);
                elmnt.scrollIntoView();
                //document.getElementById(id).scrollIntoView();
                if (scrollPosition == 'BOTTOM') {
                    window.scroll({ bottom: elmnt.offsetBottom });
                }
                else {
                    window.scroll({ top: elmnt.offsetTop });
                }

            };

            $scope.viewPO = function (poOrderId) {
                var url = $state.href('list-pendingPOOverall', { "poID": poOrderId });

                $window.open(url, '_blank');
            };

            //$scope.viewGRN = function (poOrderId) {
            //    var url = $state.href('list-grn', { "poOrderId": poOrderId });

            //    $window.open(url, '_blank');
            //};

            $scope.goToDispatchTrackForm = function (poOrderId, dCode) {

                var url = $state.href("asnForm", { "poOrderId": poOrderId, "dCode": dCode });
                //window.open(url, '_self');
                $window.open(url, '_blank');

            };

            $scope.goToInvoice = function (poOrderId) {

                var url = $state.href("createInvoice", { "poNumber": poOrderId, "invoiceNumber": '', "invoiceID": 0 });
                //window.open(url, '_self');
                $window.open(url, '_blank');

            };


            $scope.notifyToVendor = function () {
                //auctionsService.savepricecap(params).then(function (req) {
                //    if (req.errorMessage == '') {
                //        setTimeout(function () {
                //            swal({
                //                text: 'Email Will be sent to vendor',
                //                type: "success",
                //                showCancelButton: false,
                //                confirmButtonColor: "#DD6B55",
                //                confirmButtonText: "Ok",
                //                closeOnConfirm: true
                //            },
                //                function () {

                //                    location.reload();
                //                });
                //        }, 1000);
                //    } else {
                //        swal("Error!", req.errorMessage, "error");
                //    }
                //});
            };

            $scope.getPOInvoiceDetails = function (item, index) {
                $scope.selectedPODetails = item;
                $scope.selectedIndex = index;
                $scope.params = {
                    "ponumber": $scope.selectedPODetails.PO_NUMBER,
                    "sessionID": userService.getUserToken()
                };

                PRMPOService.getPOInvoiceDetails($scope.params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                item.multipleAttachments = [];
                            });
                        }
                        $scope.invoiceList = response;
                        //$scope.invoiceList.forEach(function (item) {
                        //    var attchArray = item.ATTACHMENTS.split(',');
                        //    attchArray.forEach(function (att, index) {

                        //        var fileUpload = {
                        //            fileStream: [],
                        //            fileName: '',
                        //            fileID: parseInt(att)
                        //        };

                        //        item.multipleAttachments.push(fileUpload);
                        //    });

                        //})


                    });
            };

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);
                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];

                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;
                            if (!$scope.filteredPendingPOsList[id].multipleAttachments) {
                                $scope.filteredPendingPOsList[id].multipleAttachments = [];
                            }

                            var ifExists = _.findIndex($scope.filteredPendingPOsList[id].multipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                            if (ifExists <= -1) {
                                $scope.filteredPendingPOsList[id].multipleAttachments.push(fileUpload);
                            }

                        });
                })
            }
            //$scope.getFile1 = function (id, itemid, ext) {
            //    $scope.filesTemp = $("#" + id)[0].files;
            //    $scope.filesTemp = Object.values($scope.filesTemp);
            //    $scope.filesTemp.forEach(function (attach, attachIndex) {
            //        $scope.file = $("#" + id)[0].files[attachIndex];
            //        fileReader.readAsDataUrl($scope.file, $scope)
            //            .then(function (result) {
            //                var fileUpload = {
            //                    fileStream: [],
            //                    fileName: '',
            //                    fileID: 0
            //                };
            //                var bytearray = new Uint8Array(result);
            //                fileUpload.fileStream = $.makeArray(bytearray);
            //                fileUpload.fileName = attach.name;
            //                if (!$scope.filteredPendingPOsList[id].multipleAttachments) {
            //                    $scope.filteredPendingPOsList[id].multipleAttachments = [];
            //                }

            //                var ifExists = _.findIndex($scope.filteredPendingPOsList[id].multipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
            //                if (ifExists <= -1) {
            //                    $scope.filteredPendingPOsList[id].multipleAttachments.push(fileUpload);
            //                }

            //            });
            //    })
            //}

            $scope.removeAttach = function (index, item) {
                item.multipleAttachments.splice(index, 1);
            }

            $scope.savePOInvoice = function (item, id) {
                var params1 = {
                    details: {
                        "PO_NUMBER": item.PO_NUMBER,
                        "VENDOR_CODE": item.VENDOR_CODE,
                        "VENDOR_ID": $scope.userID,
                        "INVOICE_NUMBER": item.INVOICE_NUMBER,
                        "INVOICE_AMOUNT": item.INVOICE_AMOUNT,
                        "attachmentsArray": $scope.filteredPendingPOsList[id].multipleAttachments,
                        "COMMENTS": item.COMMENTS,
                        "STATUS": item.STATUS,
                        "SessionID": $scope.sessionID
                    }
                };
                if (params1.details.attachmentsArray.length == 0) {
                    $scope.attachmentError = true;
                    return;
                }
                PRMPOService.savePOInvoice(params1)
                    .then(function (response) {

                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.getPOInvoiceDetails($scope.selectedPODetails, $scope.selectedIndex);
                            item.INVOICE_NUMBER = '';
                            item.INVOICE_AMOUNT = 0;
                            item.multipleAttachments = [];
                            item.COMMENTS = '';
                            $scope.attachmentError = false;
                        }

                    });
            };


            $scope.deletePOInvoice = function (ponumber, invoiceNumber) {

                $scope.params = {
                    "ponumber": ponumber,
                    "invoicenumber": invoiceNumber,
                    "sessionid": userService.getUserToken()
                }


                PRMPOService.deletePOInvoice($scope.params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Deleted Successfully.", "success");
                            $scope.getPOInvoiceDetails($scope.selectedPODetails, $scope.selectedIndex);

                        }
                    });

            }


            $scope.cancelInvoice = function (item,id) {
                item.INVOICE_NUMBER = '';
                item.INVOICE_AMOUNT = 0;
                item.multipleAttachments = [];
                item.COMMENTS = '';
                $scope.attachmentError = false;
            };

            $scope.acknowledgeDetails = function (details, module, pendingPODetails) {

                if (!details.PRODUCT_NAME && module === 'ACKNOWLEDGE') {
                    //swal("Accept!", "All the items will be approved with ordered Qty for " + details.PO_NUMBER, "warning");
                    swal({
                        title: "Are you sure?",
                        text: "All the items will be approved with ordered Qty for " + details.PO_NUMBER ,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "ACKNOWLEDGE!",
                        closeOnConfirm: true
                    },
                        function (isConfirm1) {
                            if (isConfirm1) {
                                var params = {
                                    'ponumber': details.PO_NUMBER,
                                    'poitemline': '',
                                    'status': module,
                                    'isVendPoAck': details.IS_PO_ACK,
                                    'quantity': 0,
                                    'user': $scope.userID,
                                    'sessionid': userService.getUserToken()
                                }
                                poService.SavePOVendorQuantityAck(params)
                                    .then(function (response) {
                                        if (response.errorMessage != '') {
                                            growlService.growl(response.errorMessage, "inverse");
                                        }
                                        else {
                                            growlService.growl("Acknowledged Successfully.", "success");
                                            location.reload();
                                        }
                                    })
                            } else {
                                return;
                            }
                        });
                   
                }

                if (!details.PRODUCT_NAME && module === 'REJECTED') {
                    swal({
                        title: "Are you sure?",
                        text: " Reject all the Items of " + details.PO_NUMBER,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Continue!",
                        closeOnConfirm: false
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                //swal("Reject!", "Rejected items can not be approved .", "success");
                                swal({
                                    title: "Are you sure?",
                                    text: "Once you click on Reject button, rejected items cannot be approved" + details.PO_NUMBER,
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Reject!",
                                    closeOnConfirm: true
                                },
                                    function (isConfirm1) {
                                        if (isConfirm1) {
                                            var params = {
                                                'ponumber': details.PO_NUMBER,
                                                'poitemline': '',
                                                'status': module,
                                                'isVendPoAck': details.IS_PO_ACK,
                                                'quantity': 0,
                                                'user': $scope.userID,
                                                'sessionid': userService.getUserToken()
                                            }
                                            poService.SavePOVendorQuantityAck(params)
                                                .then(function (response) {
                                                    if (response.errorMessage != '') {
                                                        growlService.growl(response.errorMessage, "inverse");
                                                    }
                                                    else {
                                                        growlService.growl("Rejected Successfully.", "success");
                                                        location.reload();
                                                    }
                                                })
                                        } else {
                                            return;
                                        }
                                    });
                            } else {
                                return;
                            }
                        });
                }

                if (details.PRODUCT_NAME && module === 'ACKNOWLEDGE') {
                    //swal("Accept!", "Item will be approved with ordered Qty for " + details.PRODUCT_NAME, "warning");
                    swal({
                        title: "Are you sure?",
                        text: "Item will be approved with ordered Qty for " + details.PRODUCT_NAME,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "ACKNOWLEDGE!",
                        closeOnConfirm: true
                    },
                        function (isConfirm1) {
                            if (isConfirm1) {
                                var params = {
                                    'ponumber': details.PO_NUMBER,
                                    'poitemline': details.PO_LINE_ITEM,
                                    'status': module,
                                    'isVendPoAck': details.IS_PO_ACK,
                                    'quantity': details.ORDER_QTY,
                                    'user': $scope.userID,
                                    'sessionid': userService.getUserToken()
                                }
                                poService.SavePOVendorQuantityAck(params)
                                    .then(function (response) {
                                        if (response.errorMessage != '') {
                                            growlService.growl(response.errorMessage, "inverse");
                                        }
                                        else {
                                            growlService.growl("Acknowledged Successfully.", "success");
                                            location.reload();
                                        }
                                    })
                             
                            } else {
                                return;
                            }
                        });
                   
                }

                if (details.PRODUCT_NAME && module == 'REJECTED') {
                    swal({
                        title: "Are you sure?",
                        text: " You want to reject " + details.PRODUCT_NAME,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Continue!",
                        closeOnConfirm: false
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                swal({
                                    title: "Are you sure?",
                                    text: " Rejected item can not be approved " + details.PRODUCT_NAME,
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Reject!",
                                    closeOnConfirm: true
                                },
                                    function (isConfirm1) {
                                        if (isConfirm1) {
                                            var params = {
                                                'ponumber': details.PO_NUMBER,
                                                'poitemline': details.PO_LINE_ITEM,
                                                'status': module,
                                                'isVendPoAck': details.IS_PO_ACK,
                                                'quantity': 0,
                                                'user': $scope.userID,
                                                'sessionid': userService.getUserToken()
                                            }
                                            poService.SavePOVendorQuantityAck(params)
                                                .then(function (response) {
                                                    if (response.errorMessage != '') {
                                                        growlService.growl(response.errorMessage, "inverse");
                                                    }
                                                    else {
                                                        growlService.growl("Rejected Successfully.", "success");
                                                        location.reload();
                                                    }
                                                })
                                          
                                        } else {
                                            return;
                                        }
                                    });
                            } else {
                                return;
                            }
                        });

                }

                if (details.PRODUCT_NAME && module == 'EDIT') {
                    if (details.ACK_QTY <=0 ||details.ACK_QTY > details.ORDER_QTY) {
                        details.isError = true;
                        return;
                    }
                    swal({
                        title: "Are you sure?",
                        text: "On clicking on Modify, user will not be able to edit the quantity in future " + details.PRODUCT_NAME,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Modify!",
                        closeOnConfirm: false
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                swal({
                                    title: "Are you sure?",
                                    text: "On Click on Save following items will be modified " + details.PRODUCT_NAME,
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Save!",
                                    closeOnConfirm: true
                                },
                                    function (isConfirm1) {
                                        if (isConfirm1) {
                                            var params = {
                                                'ponumber': details.PO_NUMBER,
                                                'poitemline': details.PO_LINE_ITEM,
                                                'status': module,
                                                'isVendPoAck': details.IS_PO_ACK,
                                                'quantity': details.ACK_QTY,
                                                'user': $scope.userID,
                                                'sessionid': userService.getUserToken()
                                            }
                                            poService.SavePOVendorQuantityAck(params)
                                                .then(function (response) {

                                                    if (response.errorMessage != '') {
                                                        growlService.growl(response.errorMessage, "inverse");
                                                    }
                                                    else {
                                                        growlService.growl("Modified Successfully.", "success");
                                                        location.reload();
                                                    }
                                                })

                                        } else {
                                            return;
                                        }
                                    }); 
                            } else {
                                return;
                            }
                        });

                }

                //if (details.PRODUCT_NAME && module == 'EDIT') {

                //    if (details.ACK_QTY > details.ORDER_QTY) {
                //        details.isError = true;
                //        return;
                //    }
                //    //swal("Edit!", details.PRODUCT_NAME + "is Edited", "warning");
                //    swal({
                //        title: "Are you sure?",
                //        text: details.PRODUCT_NAME + "is Edited",
                //        type: "warning",
                //        showCancelButton: true,
                //        confirmButtonColor: "#DD6B55",
                //        confirmButtonText: "Edit!",
                //        closeOnConfirm: true
                //    },
                //        function (isConfirm1) {
                //            if (isConfirm1) {
                //                var params = {
                //                    'ponumber': details.PO_NUMBER,
                //                    'poitemline': details.PO_LINE_ITEM,
                //                    'status': module,
                //                    'isVendPoAck': details.IS_PO_ACK,
                //                    'quantity': details.ACK_QTY,
                //                    'user': $scope.userID,
                //                    'sessionid': userService.getUserToken()
                //                }
                //                poService.SavePOVendorQuantityAck(params)
                //                    .then(function (response) {

                //                        location.reload();
                //                    })
                                
                //            } else {
                //                return;
                //            }
                //        });
                   
                //}
            };

            $scope.goToDispatchTrackFormEdit = function (poNumber) {
                $scope.params = {
                    "ponumber": poNumber,
                    "vendorid": $scope.isCustomer? 0 : userService.getUserId(),
                    "senssionid": userService.getUserToken()
                };

                PRMPOService.getASNDetailsList($scope.params)
                    .then(function (response) {
                        $scope.asnList = response;
                    });
            }
            $scope.routeToPaymentTracking = function () {
                $state.go('paymentTracking')
            };

            $scope.goToQCS = function (reqId, qcsId) {
                return userService.goToQCS(0, $scope.sessionID, +qcsId);
            };

        }]);﻿prmApp
    .controller('listPendingPOOverallCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices", "poService",
        "PRMCustomFieldService", "PRMPOService", "$uibModal", "fileReader",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices, poService, PRMCustomFieldService, PRMPOService, $uibModal, fileReader) {
            $scope.poNumber = $stateParams.poID;
            $scope.fromDate = $stateParams.fromDate;
            $scope.toDate = $stateParams.toDate;
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();

            $scope.poOrderId = $stateParams.poOrderId;
            $scope.dCode = $stateParams.dCode;
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;

            $scope.compID = userService.getUserCompanyId();
            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5; //Number of pager buttons to show

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };
            $scope.selectedIndex = 0;
            $scope.filteredPendingPOsList = [];
            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            $scope.ackPO = { isPOAck : 0};
            $scope.vendAttachments = {
                vendorMultipleAttachments : []
            };
            $scope.approveRejectObj = { isApproveVendorPo: -1, isApproveRejectComments: '' };
            /*PAGINATION CODE*/
            $scope.billedQty = 0;
            $scope.receivedQty = 0;
            $scope.rejectedQty = 0;
            $scope.remainingQty = 0;
            $scope.removeQty = 0;

            $scope.ALTERNATIVE_UOM = '';
            $scope.ALTERNATIVE_UOM_QTY = '';
            $scope.SERVICE_CODE = '';
            $scope.SERVICE_DESCRIPTION = '';
            $scope.MISC_CHARGES = '';

            $scope.getPendingPOOverall = function () {
                $scope.billedQty = 0;
                $scope.receivedQty = 0;
                $scope.rejectedQty = 0;
                $scope.remainingQty = 0;
                $scope.removeQty = 0;

                var params = {
                    "compid": $scope.isCustomer ? $scope.compID : 0,
                    "uid": $scope.isCustomer ? 0 : +$scope.userID,
                    "search": $scope.poNumber,
                    "categoryid": '',
                    "productid": '',
                    "supplier": '',
                    "postatus": '',
                    "deliverystatus": '',
                    "plant": '',
                    "fromdate": '1970-01-01',
                    "todate": '2100-01-01',
                    "page": 0,
                    "pagesize": 10,
                    "ackStatus": '',
                    "buyer": '',
                    "purchaseGroup": '',
                    "sessionid": userService.getUserToken()

                };

                $scope.pageSizeTemp = (params.page + 1);

                PRMPOService.getPOScheduleList(params)
                    .then(function (response) {
                        $scope.pendingPOList = [];
                        $scope.filteredPendingPOsList = [];
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                item.multipleAttachments = [];
                                item.vendorMultipleAttachments = [];
                                item.vendAttachArray = [];
                                item.PO_DATE = item.PO_DATE ? moment(item.PO_DATE).format("DD-MM-YYYY") : '-';
                                item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_CLOSED_DATE = item.PO_CLOSED_DATE ? moment(item.PO_CLOSED_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_RELEASE_DATE = item.PO_RELEASE_DATE ? moment(item.PO_RELEASE_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_RECEIPT_DATE = item.PO_RECEIPT_DATE ? moment(item.PO_RECEIPT_DATE).format("DD-MM-YYYY") : '-';
                                item.vendAttachArray = item.VENDOR_ATTACHEMNTS != null || item.VENDOR_ATTACHEMNTS != undefined ? item.VENDOR_ATTACHEMNTS.split(',') : '';
                                $scope.approveRejectObj.isApproveVendorPo = item.VENDOR_ACK_STATUS == 'APPROVED' ? true : false;
                                $scope.approveRejectObj.isApproveRejectComments = item.VENDOR_ACK_REJECT_COMMENTS; 
                                $scope.ackPO.isPOAck = item.IS_PO_ACK;
                                $scope.pendingPOList.push(item);

                                //item.MODIFIED_DATE = userService.toLocalDate(item.MODIFIED_DATE).split(' ')[0];
                            });
                        }

                        $scope.filteredPendingPOsList = $scope.pendingPOList[0];

                    });


                var params1 = {
                    "ponumber": $scope.poNumber,
                    "moredetails": 0,
                    "forasn": false
                };
                PRMPOService.getPOScheduleItems(params1)
                    .then(function (response) {
                        $scope.pendingPOItems = response;
                        $scope.pendingPOItems.forEach(function (item, index) {
                            item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                            item.PR_RELEASE_DATE = item.PR_RELEASE_DATE ? moment(item.PR_RELEASE_DATE).format("DD-MM-YYYY") : '-';
                            item.PR_DELIVERY_DATE = item.PR_DELIVERY_DATE ? moment(item.PR_DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                            item.VALID_FROM = item.VALID_FROM ? moment(item.VALID_FROM).format("DD-MM-YYYY") : '-';
                            item.VALID_TO = item.VALID_TO ? moment(item.VALID_TO).format("DD-MM-YYYY") : '-';
                            //item.VENDOR_EXPECTED_DELIVERY_DATE_LOCAL = item.VENDOR_EXPECTED_DELIVERY_DATE ? moment(item.VENDOR_EXPECTED_DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                            if (item.VENDOR_EXPECTED_DELIVERY_DATE_STRING == "NOT_ACK_WITH_EXPECTED_DATE") {
                                item.VENDOR_EXPECTED_DELIVERY_DATE_LOCAL = '-';
                            } else {
                                item.VENDOR_EXPECTED_DELIVERY_DATE_LOCAL = userService.toOnlyDate(item.VENDOR_EXPECTED_DELIVERY_DATE);
                            }
                          //  item.VENDOR_EXPECTED_DELIVERY_DATE_LOCAL = item.VENDOR_EXPECTED_DELIVERY_DATE ? userService.toOnlyDate(item.VENDOR_EXPECTED_DELIVERY_DATE) : '-';
                            
                            item.PO_ITEM_CHANGE_DATE = item.PO_ITEM_CHANGE_DATE ? moment(item.PO_ITEM_CHANGE_DATE).format("DD-MM-YYYY") : '-';
                            item.ALTERNATIVE_UOM = $scope.ALTERNATIVE_UOM;
                            item.ALTERNATIVE_UOM_QTY = $scope.ALTERNATIVE_UOM_QTY;
                            item.SERVICE_CODE = $scope.SERVICE_CODE;
                            item.SERVICE_DESCRIPTION = $scope.SERVICE_DESCRIPTION;
                            item.MISC_CHARGES = $scope.MISC_CHARGES;

                            $scope.billedQty += item.ORDER_QTY;
                            $scope.receivedQty += item.RECEIVED_QTY;
                            $scope.rejectedQty += item.REJECTED_QTY;
                            $scope.remainingQty += item.REMAINING_QTY;
                            //$scope.removeQty += item.REMOVE_QTY;

                            if (item.GRNItems && item.GRNItems.length > 0) {
                                item.GRNItems.forEach(function (grnItem, index) {
                                    grnItem.GRN_DELIVERY_DATE_LOCAL = grnItem.GRN_DELIVERY_DATE ? moment(grnItem.GRN_DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                                });
                            }
                        });

                        //pendingPODetails.pendingPOItems = $scope.pendingPOItems;


                    });

            };

            $scope.getPendingPOOverall();


            $scope.getPOInvoiceDetails = function () {
                $scope.params = {
                    "ponumber": $scope.poNumber,
                    "sessionID": userService.getUserToken()
                };

                PRMPOService.getPOInvoiceDetails($scope.params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                item.multipleAttachments = [];
                            });
                        }
                        $scope.invoiceList = response;
                        //$scope.invoiceList.forEach(function (item) {
                        //    var attchArray = item.ATTACHMENTS.split(',');
                        //    attchArray.forEach(function (att, index) {

                        //        var fileUpload = {
                        //            fileStream: [],
                        //            fileName: '',
                        //            fileID: parseInt(att)
                        //        };

                        //        item.multipleAttachments.push(fileUpload);
                        //    });

                        //})


                    });
            };

            $scope.getPOInvoiceDetails()

            $scope.getASNDetails = function () {
                //$scope.params = {
                //    "compid": $scope.isCustomer ? $scope.compID : 0,
                //    "asnid": 0,
                //    "ponumber": $scope.poNumber,
                //    "grncode": 0,
                //    "asncode": 0,
                //    "vendorid": $scope.isCustomer ? 0 : $scope.userID,
                //    "sessionID": userService.getUserToken()
                //};

                //PRMPOService.getASNDetails($scope.params)
                //    .then(function (response) {
                //        if (response && response.length > 0) {
                //            $scope.asnList = response;
                //        }
                //    });

                $scope.params = {
                    "ponumber": $scope.poNumber,
                    "vendorid": 0,
                    "senssionid": userService.getUserToken()
                };

                PRMPOService.getASNDetailsList($scope.params)
                    .then(function (response) {
                        $scope.asnList = response;
                    });
            };

            $scope.getASNDetails();

            $scope.viewPO = function (grnNo) {
                var url = $state.href('list-ViewGRN', { "grnNo": grnNo, "isGRN": false });

                $window.open(url, '_blank');
            };

            //$scope.viewASN = function (asnNo, vendorCode) {
            //    var url = $state.href('viewPendingasn', { "asnNo": asnNo, "vendorCode": vendorCode, "isASN": false });

            //    $window.open(url, '_blank');
            //};

            $scope.viewASN = function (purchaseID, dispatchCode) {
                var url = $state.href("asnForm", { "poOrderId": purchaseID, "dCode": dispatchCode });
                window.open(url, '_blank');
            };




            $scope.saveAcknowledgementForm = function (obj) {
                $scope.ackExpectedDeldateError = '';
                $scope.ackcommentsError = '';
                //var ts = moment(obj.DELIVERY_DATE, "DD-MM-YYYY").valueOf();
                //var m = moment(ts);
                //var deliveryDate = new Date(m);
                //var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                //obj.DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";
                //if (obj == undefined || obj == null) {
                //    $scope.ackError = 'Please select expected delivery date and enter comments.';
                //    return;
                //} else {
                //    if (obj.EXPECTED_DELIVERY_DATE == null || obj.EXPECTED_DELIVERY_DATE == undefined || obj.EXPECTED_DELIVERY_DATE == '') {
                //        $scope.ackExpectedDeldateError = 'Please select expected delivery date.';
                //        return
                //    }
                //    if (obj.COMMENTS == null || obj.COMMENTS == undefined || obj.COMMENTS == '') {
                //        $scope.ackcommentsError = 'Please enter comments.';
                //        return
                //    }
                //}

                if (obj.EXPECTED_DELIVERY_DATE == null || obj.EXPECTED_DELIVERY_DATE == undefined || obj.EXPECTED_DELIVERY_DATE == '') {
                    obj.EXPECTED_DELIVERY_DATE_TEMP = 'NOT_ACK_WITH_EXPECTED_DATE';
                } else {
                    obj.EXPECTED_DELIVERY_DATE_TEMP = 'ACK_WITH_EXPECTED_DATE';
                }

                //var ts = moment(obj.EXPECTED_DELIVERY_DATE, "DD-MM-YYYY").valueOf();
                //var m = moment(ts);
                //var deliveryDate = new Date(m);
                //var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                //obj.EXPECTED_DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";

                //var ts = userService.toUTCTicks(obj.EXPECTED_DELIVERY_DATE);
                //var m = moment(ts);
                //var deliveryDate = new Date(m);
                //var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                //obj.EXPECTED_DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";

                var params = {
                    "vendordeliverydate": obj.EXPECTED_DELIVERY_DATE,
                    "vendordeliverydateString": obj.EXPECTED_DELIVERY_DATE_TEMP,
                        "poitemline": '',
                        "status": "ACKNOWLEDGE",
                        "ponumber": $scope.poNumber,
                        "user": userService.getUserId(),
                        "comments": obj.COMMENTS,
                    "isVendPoAck": $scope.ackPO.isPOAck,
                    "vendorAttachments": $scope.vendAttachments.vendorMultipleAttachments,
                    "ExistingVendorAttachments": obj.vendAttachArray,
                    "sessionid": $scope.sessionID

                };

                poService.SaveVendorAck(params).then(function (response) {
                    if (!response.errorMessage) {
                        angular.element('#acknowledgeModal').modal('hide');
                        growlService.growl("Vendor Acknowledged Successfully", "success");
                        location.reload();
                    }
                    else
                        growlService.growl("Error saving Acknowledgement.", "inverse");
                });

            };

            $scope.openAckModel = function (obj) {
                if (obj.isPOAck) {
                    $scope.ackExpectedDeldateError = '';
                    $scope.ackcommentsError = '';
                    angular.element('#acknowledgeModal').modal('show');
                }
                
            }

            $scope.closeAcknowledgementForm = function () {
                if ($scope.filteredPendingPOsList) {
                  
                    $scope.filteredPendingPOsList.EXPECTED_DELIVERY_DATE = '';
                    $scope.filteredPendingPOsList.COMMENTS = '';
                    $scope.vendAttachments.vendorMultipleAttachments = [];
                   // $scope.ackPO.isPOAck = 0;
                    angular.element('#acknowledgeModal').modal('hide');
                } else {
                    $scope.vendAttachments.vendorMultipleAttachments = [];
                    //$scope.ackPO.isPOAck = 0;
                    angular.element('#acknowledgeModal').modal('hide');
                }
               
                
            };

            $scope.removeAttach = function (index, item) {
                item.multipleAttachments.splice(index, 1);
            }
            $scope.removeVendAttach = function (index, item) {
                item.vendorAttachmentsArray.splice(index, 1);
            }
            $scope.removeVendAttachPopUp = function (index, item) {
                item.vendorMultipleAttachments.splice(index, 1);
            }
            $scope.removeAttach1 = function (index, item) {
                item.attachmentsArray.splice(index, 1);
            }

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);
                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];

                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;
                            if (id == "vendorAttach") {
                                if ( !$scope.vendAttachments.vendorMultipleAttachments) {
                                    $scope.vendAttachments.vendorMultipleAttachments = [];
                                }

                                var ifExists = _.findIndex($scope.vendAttachments.vendorMultipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                                if (ifExists <= -1) {
                                    $scope.vendAttachments.vendorMultipleAttachments.push(fileUpload);
                                }
                            } else {
                                if (!$scope.filteredPendingPOsList.multipleAttachments) {
                                    $scope.filteredPendingPOsList.multipleAttachments = [];
                                }

                                var ifExists = _.findIndex($scope.filteredPendingPOsList.multipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                                if (ifExists <= -1) {
                                    $scope.filteredPendingPOsList.multipleAttachments.push(fileUpload);
                                }
                            }
                          

                        });
                })
            }

            $scope.saveAttachments = function (item) {
                if ($scope.filteredPendingPOsList.multipleAttachments.length == 0) {
                    growlService.growl("Please upload attachments.", "inverse");
                    return;
                }
                var params1 = {
                    details: {
                        "PO_NUMBER": item.PO_NUMBER,
                        "VENDOR_CODE": item.VENDOR_CODE,
                        "VENDOR_ID": item.VENDOR_ID,
                        "attachmentsArray": $scope.filteredPendingPOsList.multipleAttachments,
                        "SessionID": $scope.sessionID
                    }
                };

                PRMPOService.savepoattachments(params1)
                    .then(function (response) {

                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.getPendingPOOverall();
                        }

                    });
            };

            $scope.PoApproval = function (item, approveObj) {
                if (approveObj.isApproveVendorPo == false) {
                    if (approveObj.isApproveRejectComments == '' || approveObj.isApproveRejectComments == null || approveObj.isApproveRejectComments == undefined) {
                        swal("Error!", "Please enter reject comments.", "error");
                        return;
                    }
                }
                var params = {
                    details: {
                        "PO_NUMBER": item.PO_NUMBER,
                        "VENDOR_CODE": item.VENDOR_CODE,
                        "VENDOR_ID": item.VENDOR_ID,
                        "attachmentsArray": $scope.filteredPendingPOsList.multipleAttachments,
                        "SessionID": $scope.sessionID
                    },
                    isPoApprove: approveObj.isApproveVendorPo,
                    isPoRejectComments: approveObj.isApproveRejectComments,
                    currentUserID: $scope.userID
                };

                PRMPOService.poApproval(params)
                    .then(function (response) {

                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.getPendingPOOverall();
                        }

                    });
            }

        }]);﻿prmApp
    .controller('paymenttrackingCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPOService", "poService",
        "PRMCustomFieldService", "fileReader", "$uibModal", "$filter", "$http", "poDomain","PRMUploadServices",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter, $http, poDomain, PRMUploadServices) {
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.customerType = userService.getUserType();


            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.pageChanged = function () {
            };
            /*PAGINATION CODE*/

            $scope.exportPOItemsToExcel = function (type) {
                PRMUploadServices.GetExcelTemplate(type);
            };

            $scope.showErrorPopup = false;
            $scope.rowErrors = [];

            $scope.uploadExcel = function (type, proceedFurther, fileName) {
                $scope.showErrorPopup = false;
                $scope.FILE_NAME = fileName ? fileName : $scope.FILE_NAME;
                var params = {
                    name: type,
                    compID: $scope.compId,
                    sessionID: $scope.sessionID,
                    tableName: 'SAP_PAYMENT_DETAILS',
                    attachment: $scope.attachment,
                    proceedFurther: proceedFurther,
                    fileName: $scope.FILE_NAME
                };
                PRMUploadServices.uploadTemplate(params)
                    .then(function (response) {
                        //if (response.errorMessage == '') {
                        //    location.reload();
                        //} else {
                        //    swal("Error!", response.errorMessage, "warning");
                        //    $("#paymentclientupload").val(null);
                        //    $scope.getPaymentInvoiceDetails();
                        //}
                        if (response && response.length > 0) {
                            if (response[0].IS_SUCCESS) {
                                angular.element('#errorPopUp').modal('hide');
                                swal("Status!", "Total Count : " + response[0].TOTAL_COUNT + " \n " + " Successfull Count : " + response[0].SUCCESS_COUNT + " \n " + " Failed Count : " + response[0].FAILED_COUNT, "warning");
                                $("#paymentclientupload").val(null);
                                $scope.getPaymentInvoiceDetails();
                            } else {
                                if (!response[0].IS_SUCCESS && proceedFurther === 1) {
                                    $scope.showErrorPopup = true;
                                    $scope.rowErrors = response;
                                    angular.element('#errorPopUp').modal('show');
                                    swal("Status!", "All the records in this sheet contains errors.. So cannot continue further ! Please validate the sheet again and upload.", "error");
                                    $("#paymentclientupload").val(null);
                                    angular.element('#errorPopUp').modal('hide');
                                } else {
                                    $scope.showErrorPopup = true;
                                    $scope.rowErrors = response;
                                    angular.element('#errorPopUp').modal('show');
                                    $("#paymentclientupload").val(null);
                                }
                            }
                        } else {
                            $scope.uploadExcel('PAYMENT',1);
                        }
                    });
            };


            //$scope.getFile1 = function (id, itemid, ext) {
            //    $scope.file = $("#" + id)[0].files[0];
            //    fileReader.readAsDataUrl($scope.file, $scope)
            //        .then(function (result) {

            //            if (id == "paymentclientupload") {
            //                var bytearray = new Uint8Array(result);
            //                $scope.attachment = $.makeArray(bytearray);
            //                $scope.uploadExcel('PAYMENT');
            //            }
            //        });
            //};

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);
                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];

                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            $scope.attachment = $.makeArray(bytearray);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;

                            if (id == "paymentclientupload") {
                                $scope.uploadExcel('PAYMENT', 0, fileUpload.fileName);
                                $scope.file = [];
                                $scope.file.name = '';
                            }
                        });
                });
            };

            $scope.gotoInvoicesList = function (invoiceId) {
                var url4 = $state.href('listInvoices');
                $window.open(url4, '_self');
            };

            $scope.goToPendingPO = function () {
                var url4 = $state.href('list-pendingPO');
                $window.open(url4, '_self');
            };

            $scope.paymentList = [];
            $scope.paymentListTemp = [];
            $scope.isCustomer = $scope.customerType == "CUSTOMER" ? true : false;


            $scope.getPaymentInvoiceDetails = function () {
                var params = {
                    sessionID: $scope.sessionID,
                    compId: +$scope.compId,
                    userId: $scope.isCustomer ? 0 : +$scope.userID
                };
                PRMPOService.getPaymentInvoiceDetails(params)
                    .then(function (response) {
                        if (response) {
                            $scope.paymentList = response;
                            $scope.paymentListTemp = response;
                            $scope.totalItems = $scope.paymentList.length;
                        }

                    });
            };
            $scope.getPaymentInvoiceDetails();

            $scope.convertDate = function (date) {
                return date ? userService.toLocalDate(date).split(' ')[0] : date;
            };


            $scope.searchTable = function (search) {
                if (search) {
                    $scope.paymentList = _.filter($scope.paymentListTemp, function (item) {
                        return (item.invoiceNumber.toUpperCase().indexOf(search.toUpperCase()) > -1);
                    });
                } else {
                    $scope.paymentList = $scope.paymentListTemp;
                }
                $scope.totalItems = $scope.paymentList.length;

            };

            $scope.exportErrorDetails = function (type) {
                $state.go('moduleErrorList', { 'moduleName': type});
            };

            $scope.getDate = function (value) {
                return userService.toLocalDate(value);
            }
            
        }]);﻿prmApp
    .controller('uploadVendorInvoiceCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPOService", "poService",
        "PRMCustomFieldService", "fileReader", "$uibModal", "$filter","workflowService",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter, workflowService) {
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.showAllInvoices = $stateParams.ID ? +$stateParams.ID : 0;
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
            $scope.myAuctions1 = [];
            $scope.myAuctionsFiltred = [];
            $scope.selectedPRItems = [];
            $scope.prmTemplates = [];
            $scope.selectedTemplate = {};
            $scope.selectedRFP;
            $scope.selectedPR;
            $scope.filteredRequirements = [];
            $scope.invoiceID = $scope.showAllInvoices;
            $scope.WorkflowModule = 'VENDOR_INVOICE';
            $scope.onlyContracts = $stateParams.onlyContracts ? 1 : 0;
            $scope.excludeContracts = $stateParams.excludeContracts ? 1 : 0;
            if (!$scope.onlyContracts && !$scope.excludeContracts) {
                $scope.onlyContracts = 0;
                $scope.excludeContracts = 0;
                if (window.location.href && window.location.href.toLowerCase().indexOf('list-pendingcontracts') >= 0) { //Backup on F5.
                    $scope.onlyContracts = 1;
                } else {
                    $scope.excludeContracts = 1;
                }
            }
            $scope.isAttachmentError = false;

            $scope.companyLocation1 =
                [
                    {
                        FIELD_NAME: 'Chirala',
                        FIELD_VALUE: 'Chirala'
                    },
                    {
                        FIELD_NAME: 'Anaparthy',
                        FIELD_VALUE: 'Anaparthy'
                    },
                    {
                        FIELD_NAME: 'Mysore',
                        FIELD_VALUE: 'Mysore'
                    },
                    {
                        FIELD_NAME: 'Guntur',
                        FIELD_VALUE: 'Guntur'
                    }
                ];

            $scope.pendingPOStats = {
                totalPendingPOs: 0,
                totalPOs: 0,
                totalAwaitingRecipt: 0,
                totalPOsNotInitiated: 0,
                totalPartialDeliverbles: 0
            };
            $scope.prExcelReport = [];
            $scope.invoiceList = [];

            $scope.invoiceDetails = {
                invoiceNumber: '',
                invoiceAmount: '',
                invoiceComments: ''
            };

            $scope.selectedPODetails = [];
            $scope.selectedIndex = 0;

            $scope.filtersList = {
                departmentList: [],
                categoryidList: [],
                productidList: [],
                supplierList: [],
                poStatusList: [],
                deliveryStatusList: [],
                plantList: [],
                purchaseGroupList: [],
                subUserList: [],
                vendorAckStatus: []
            };

            $scope.filters = {
                department: {},
                categoryid: {},
                productid: {},
                supplier: {},
                poStatus: {},
                deliveryStatus: {},
                plant: {},
                purchaseGroup: {},
                pendingPOFromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
                pendingPOToDate: moment().format('YYYY-MM-DD'),
                subuser: {},
                ackStatus: {}
            };

            //$scope.filters.pendingPOToDate = moment().format('YYYY-MM-DD');
            //$scope.filters.pendingPOFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;
            
            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            $scope.totalItems2 = 0;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage2 = 10;
            $scope.maxSize2 = 5;

            $scope.setPage2 = function (pageNo) {
                $scope.currentPage1 = pageNo;
            };

            $scope.pageChanged2 = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            /*PAGINATION CODE*/


            $scope.pendingPOList = [];
            $scope.filteredPendingPOsList = [];
            $scope.filteredPendingPOsList = [{ multipleAttachments: [] }];
            $scope.pendingPOItems = [];
            
            $scope.getPOInvoiceDetails = function (invoiceId) {
                $scope.params = {
                    "ponumber": '',
                    "sessionID": userService.getUserToken(),
                    "vendorID": !$scope.isCustomer && invoiceId <=0 ? +$scope.userID : 0,
                    "invoiceID": invoiceId > 0 ? invoiceId : 0
                };

                PRMPOService.getPOInvoiceDetails($scope.params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                item.multipleAttachments = [];
                            });
                        }
                        $scope.invoiceList = response;

                        if (invoiceId > 0) {
                            $scope.invoiceList.forEach(function (item,index) {
                                $scope.selectedPODetails.PO_NUMBER = item.PO_NUMBER;
                                $scope.selectedPODetails.INVOICE_NUMBER = item.INVOICE_NUMBER;
                                $scope.selectedPODetails.LOCATION = item.LOCATION;
                                $scope.selectedPODetails.INVOICE_AMOUNT = item.INVOICE_AMOUNT;
                                $scope.selectedPODetails.COMMENTS = item.COMMENTS;
                                $scope.selectedPODetails.multipleAttachments = item.attachmentsArray;
                            });
                        }
                        $scope.totalItems = $scope.invoiceList.length;
                    });
            };

            $scope.getPOInvoiceDetails($scope.invoiceID);
            
            $scope.getFile1 = function (id, itemid, ext) {
                //$scope.filteredPendingPOsList = [{ multipleAttachments: [] }];
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);
                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];

                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;
                            if (!$scope.filteredPendingPOsList[id].multipleAttachments) {
                                $scope.filteredPendingPOsList[id].multipleAttachments = [];
                            }

                            var ifExists = _.findIndex($scope.filteredPendingPOsList[id].multipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                            if (ifExists <= -1) {
                                $scope.filteredPendingPOsList[id].multipleAttachments.push(fileUpload);
                            }

                        });
                });
            };

            $scope.removeAttach = function (index, item) {
                $scope.filteredPendingPOsList[0].multipleAttachments.splice(index, 1);
            };

            $scope.savePOInvoice = function (item, id) {
                var params1 = {
                    details: {
                        "PO_NUMBER": item.PO_NUMBER,
                        "VENDOR_CODE": item.VENDOR_CODE,
                        "VENDOR_ID": $scope.userID,
                        "INVOICE_NUMBER": item.INVOICE_NUMBER,
                        "INVOICE_AMOUNT": item.INVOICE_AMOUNT,
                        "attachmentsArray": $scope.filteredPendingPOsList[id] ? $scope.filteredPendingPOsList[id].multipleAttachments : [],
                        "COMMENTS": item.COMMENTS,
                        "STATUS": item.STATUS,
                        "SessionID": $scope.sessionID,
                        "LOCATION": item.LOCATION,
                        "INVOICE_ID": $scope.invoiceID,
                        "WF_ID": getWorkflowId($scope.invoiceID),
                        "ATTACHMENTS": getAttachments($scope.invoiceID)
                    }
                };

                if (!$scope.invoiceID) {
                    if (params1.details.attachmentsArray.length == 0) {
                        $scope.isAttachmentError = true;
                        return;
                    }
                    if (params1.details.PO_NUMBER == undefined || params1.details.INVOICE_NUMBER == undefined) {
                        return;
                    }
                    PRMPOService.savePOInvoice(params1)
                        .then(function (response) {

                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                growlService.growl("Saved Successfully.", "success");
                                location.reload();
                            }

                        });
                } else {
                    PRMPOService.editPOInvoice(params1)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                swal("Error!", response.errorMessage, "error");
                            }
                            else {
                                growlService.growl("Saved Successfully.", "success");
                                $scope.goToVendorInvoice(0);
                            }
                        });
                }
            };


            $scope.deletePOInvoice = function (poNumber, invoiceNumber, invoiceId, wfId) {
                $scope.showErrorMessage = false;
                $scope.params = {
                    "ponumber": poNumber,
                    "invoicenumber": invoiceNumber,
                    "invoiceId": invoiceId,
                    "wfId": wfId,
                    "sessionid": userService.getUserToken()
                };
                
                swal({
                    title: "Are you sure?",
                    text: "This Will Delete the Invoice.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    PRMPOService.deletePOInvoice($scope.params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                $scope.showErrorMessage = true;
                            }
                            else {
                                growlService.growl("Deleted Successfully.", "success");
                                location.reload();

                            }
                        });
                    });
            };

            $scope.showCreateInvPage = function (val,disp) {
                if (disp && disp === 'CREATE') {
                    $scope.showAllInvoices = 1;
                } else {
                    $scope.goToVendorInvoice(val);
                    $scope.selectedPODetails.PO_NUMBER = '';
                    $scope.selectedPODetails.INVOICE_NUMBER = '';
                    $scope.selectedPODetails.INVOICE_AMOUNT = 0;
                    $scope.selectedPODetails.LOCATION = '';
                    $scope.selectedPODetails.COMMENTS = '';
                    if ($scope.filteredPendingPOsList.length>0) {
                        $scope.filteredPendingPOsList[0].multipleAttachments = [];
                    }
                    $scope.isAttachmentError = false;
                    $scope.showAllInvoices = 0;

                }
            };

            $scope.isFormdisabled = false;

            if ($scope.isCustomer)
            {
                $scope.isFormdisabled = true;
                $scope.getItemWorkflow = function () {
                    workflowService.getItemWorkflow(0, $scope.invoiceID, $scope.WorkflowModule)
                        .then(function (response) {
                            $scope.itemWorkflow = response;
                            if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                                $scope.currentStep = 0;

                                var count = 0;

                                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                    if (!track.multipleAttachments) {
                                        track.multipleAttachments = [];
                                    }

                                    if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                    if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                    if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                    if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                                    if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                        $scope.isFormdisabled = true;
                                    }

                                    if (track.status === 'APPROVED') {
                                        $scope.isWorkflowCompleted = true;
                                        $scope.orderInfo = track.order;
                                        $scope.assignToShow = track.status;

                                    }
                                    else {
                                        $scope.isWorkflowCompleted = false;
                                    }

                                    if (track.status === 'REJECTED' && count == 0) {
                                        count = count + 1;
                                    }

                                    if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                        count = count + 1;
                                        //$scope.IsUserApproverForStage(track.approverID);
                                        $scope.currentAccess = track.order;
                                    }

                                    if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                        $scope.currentStep = track.order;
                                        return false;
                                    }
                                });
                            }
                        });
                };

                $scope.updateTrack = function (step, status) {
                    $scope.disableAssignPR = true;
                    $scope.commentsError = '';
                    if (step.comments != null || step.comments != "" || step.comments != undefined) {
                        step.comments = validateStringWithoutSpecialCharacters(step.comments);
                    }
                    var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                    if (step.order == tempArray.order && status === 'APPROVED') {
                        $scope.disableAssignPR = false;
                    } else {
                        $scope.disableAssignPR = true;
                    }

                    if ($scope.isReject) {
                        $scope.commentsError = 'Please Save Rejected Items/Qty';
                        return false;
                    }

                    if (status === 'REJECTED' && (step.comments == null || step.comments == "")) {
                        $scope.commentsError = 'Please enter comments';
                        return false;
                    }

                    step.status = status;
                    step.sessionID = $scope.sessionID;
                    step.modifiedBy = userService.getUserId();

                    step.moduleName = $scope.WorkflowModule;

                    step.subModuleName = '';
                    step.subModuleID = 0;

                    workflowService.SaveWorkflowTrack(step)
                        .then(function (response) {
                            if (response.errorMessage) {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                $scope.getItemWorkflow();
                                location.reload();
                            }
                        });
                };

                $scope.getItemWorkflow();

                function validateStringWithoutSpecialCharacters(string) {
                    if (string) {
                        string = string.replace(/\'/gi, "");
                        string = string.replace(/\"/gi, "");
                        string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                        string = string.replace(/(\r\n|\n|\r)/gm, "");
                        string = string.replace(/\t/g, '');
                        return string;
                    }
                }

                $scope.isApproverDisable = function (index) {

                    var disable = true;

                    var previousStep = {};

                    $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                        if (index == stepIndex) {
                            if (stepIndex == 0) {
                                if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                    (step.status === 'PENDING' || step.status === 'HOLD')) {
                                    disable = false;
                                }
                                else {
                                    disable = true;
                                }
                            }
                            else if (stepIndex > 0) {
                                if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                    disable = true;
                                }
                                else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                    (step.status === 'PENDING' || step.status === 'HOLD')) {
                                    disable = false;
                                }
                                else {
                                    disable = true;
                                }
                            }
                        }
                        previousStep = step;
                    });

                    return disable;
                };


                $scope.deptIDs = [];
                $scope.desigIDs = [];
                $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
                if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                    $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                        $scope.deptIDs.push(item.deptID);
                        item.listDesignation.forEach(function (item1, index1) {
                            if (item1.isAssignedToUser && item1.isValid) {
                                $scope.desigIDs.push(item1.desigID);
                            }
                        });
                    });
                }


                $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                    var isEligible = true;

                    if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                        isEligible = true;
                    } else {
                        isEligible = false;
                    }

                    return isEligible;
                };

                $scope.showApprovedDate = function (date) {
                    return userService.toLocalDate(date);
                };
            }
            
            $scope.goToVendorInvoice = function (invoiceId) {
                var url4 = $state.href('uploadvendorinvoice', { "ID": invoiceId });
                $window.open(url4, '_self');
            };


            function getWorkflowId(invoiceId) {
                var wfId = 0;
                wfId = (invoiceId && invoiceId > 0) ? _.find($scope.invoiceList, { INVOICE_ID: invoiceId }).WF_ID : 0;
                return wfId;
            }

            function getAttachments(invoiceId) {
                var attachments = '';
                attachments = (invoiceId && invoiceId > 0) ? _.find($scope.invoiceList, { INVOICE_ID: invoiceId }).ATTACHMENTS : 0;
                return attachments;
            }

        }]);﻿prmApp.constant('PRMPOServiceDomain', 'svc/PRMPOService.svc/REST/');
prmApp.service('PRMPOService', ["PRMPOServiceDomain", "SAPIntegrationServicesDomain", "userService", "httpServices",
    function (PRMPOServiceDomain, SAPIntegrationServicesDomain, userService, httpServices) {

        var PRMPOService = this;

        PRMPOService.getPOScheduleList = function (params) {

            params.onlycontracts = params.onlycontracts ? params.onlycontracts : 0;
            params.excludecontracts = params.excludecontracts ? params.excludecontracts : 0;

            let api = 'getposchedulelist';

            if (params.isExport) {
                api = 'getposchedulelistexport';
            }

            let url = PRMPOServiceDomain + api + '?compid=' + params.compid + '&uid=' + params.uid + '&search=' + params.search + '&categoryid=' + params.categoryid
                + '&productid=' + params.productid + '&supplier=' + params.supplier + '&postatus=' + params.postatus
                + '&deliverystatus=' + params.deliverystatus
                + '&plant=' + params.plant
                + '&fromdate=' + params.fromdate + '&todate=' + params.todate
                + '&onlycontracts=' + params.onlycontracts + '&excludecontracts=' + params.excludecontracts + '&ackStatus=' + params.ackStatus + '&buyer=' + params.buyer + '&purchaseGroup=' + params.purchaseGroup
                + '&page=' + params.page
                + '&pagesize=' + params.pagesize + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOService.getPOScheduleItems = function (params) {
            let url = PRMPOServiceDomain + 'getposcheduleitems?ponumber=' + params.ponumber + '&moredetails=' + params.moredetails + '&forasn=' + params.forasn + '&sessionid=' + userService.getUserToken();

            return httpServices.get(url);
        };

        PRMPOService.getPOScheduleFilterValues = function (params) {
            let url = PRMPOServiceDomain + 'getposchedulefilters?compid=' + params.compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOService.editPOInvoice = function (params) {
            let url = PRMPOServiceDomain + 'editPOInvoice';
            return httpServices.post(url, params);
        };


        PRMPOService.getPOInvoiceDetails = function (params) {
            if (!params.vendorID) {
                params.vendorID = 0;
            }
            if (!params.invoiceID) {
                params.invoiceID = 0;
            }
            let url = PRMPOServiceDomain + 'getpoinvoicedetails?ponumber=' + params.ponumber + '&sessionid=' + userService.getUserToken() + '&vendorID=' + params.vendorID + '&invoiceID=' + params.invoiceID;
            return httpServices.get(url);
        };

        PRMPOService.getASNDetails = function (params) {
            let url = PRMPOServiceDomain + 'getasndetails?compid=' + params.compid + '&asnid=' + params.asnid + '&ponumber=' + params.ponumber + '&grncode=' + params.grncode + '&asncode=' + params.asncode + '&sessionid=' + userService.getUserToken() + '&vendorid=' + params.vendorid;
            return httpServices.get(url);
        };

        PRMPOService.getASNDetailsList = function (params) {
            let url = PRMPOServiceDomain + 'getasndetailslist?ponumber=' + params.ponumber + '&sessionid=' + userService.getUserToken() + '&vendorid=' + params.vendorid;
            return httpServices.get(url);
        };

        PRMPOService.saveASNdetails = function (params) {
            let url = PRMPOServiceDomain + 'saveasndetails';
            return httpServices.post(url, params);
        };

        PRMPOService.savePOInvoice = function (params) {
            let url = PRMPOServiceDomain + 'savepoinvoice';
            return httpServices.post(url, params);
        };

        PRMPOService.getPaymentInvoiceDetails = function (params) {
            let url = PRMPOServiceDomain + 'getPaymentInvoiceDetails?sessionid=' + params.sessionID + '&compId=' + params.compId + '&userId=' + params.userId;
            return httpServices.get(url);
        };

        PRMPOService.savePOInvoiceForm = function (params) {
            let url = PRMPOServiceDomain + 'savepoinvoiceform';
            return httpServices.post(url, params);
        };

        PRMPOService.getInvoiceList = function (params) {
            let url = PRMPOServiceDomain + 'getinvoicelist?compid=' + params.COMP_ID + '&userid=' + params.U_ID + '&sessionid=' + userService.getUserToken() + '&fromDate=' + params.fromDate + '&toDate=' + params.toDate + '&ponumber=' + params.PO_NUMBER + '&invoicenumber=' + params.INVOICE_NUMBER ;
            return httpServices.get(url);
        };

        PRMPOService.savepoattachments = function (params) {
            let url = PRMPOServiceDomain + 'savepoattachments';
            return httpServices.post(url, params);
        };


        PRMPOService.poApproval = function (params) {
            let url = PRMPOServiceDomain + 'poapproval';
            return httpServices.post(url, params);
        };

        PRMPOService.deletePOInvoice = function (params) {
            if (!params.invoiceId) {
                params.invoiceId = 0;
            }
            if (!params.wfId) {
                params.wfId = 0;
            }
            let url = PRMPOServiceDomain + 'deletepoinvoice?ponumber=' + params.ponumber + '&invoicenumber=' + params.invoicenumber + '&invoiceId=' + params.invoiceId + '&wfId=' + params.wfId + '&sessionid=' + params.sessionid;
            return httpServices.get(url, params);
        };

        return PRMPOService;

    }]);