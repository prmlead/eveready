﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
            .state('audit', {
                url: '/audit/:reqID',
                templateUrl: 'audit/views/Audit.html'
            })
                .state('paymentTracking', {
                    url: '/paymentTracking',
                    templateUrl: 'PendingPO/views/paymenttracking.html'
                })

        }]);prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('auditCtrl', ["$state", "$stateParams", "$scope", "auctionsService", "userService",
        "$http", "$window", "domain", "fileReader", "growlService", "$log", "$filter", "ngDialog", "auditreportingServices",
        function ($state, $stateParams, $scope, auctionsService, userService,
            $http, $window, domain, fileReader, growlService, $log, $filter, ngDialog, auditreportingServices) {
            $scope.auditLogs = [];
            $scope.reqId = $stateParams.reqID;
            $scope.requirementDetails = {};
            $scope.auditData = [];
            $scope.auditDetails = [];
            $scope.filteredDetails = [];
            $scope.getAuditData = function () {
                if ($scope.reqId) {
                    auditreportingServices.getrequirementaudit($scope.reqId)
                        .then(function (response) {
                            console.log(response);
                            if (response.errorMessage) {
                                console.log(response.errorMessage);
                            } else {
                                //$scope.createAuditTable(response);
                                $scope.auditData = response[0].Value;
                                $scope.auditDetails = response[1].Value;
                            }

                        });
                }
            };

            $scope.getAuditData();

            //$scope.createAuditTable = function (audits) {
            //    audits.forEach(function (item, index) {
            //        item.dateCreated = userService.toLocalDate(item.dateCreated);
            //        if (item.actionType === "I") {
            //            item.actionType = "INSERT";
            //        } else if (item.actionType === "D") {
            //            item.actionType = "DELETE";
            //        } else if (item.actionType === "U") {
            //            item.actionType = "UPDATE";
            //        }
            //    });
            //    $scope.auditLogs = audits;
            //};

            $scope.getAuditDetails = function (audit) {
                $scope.auditData.forEach(function (audit1, vI) {
                    audit1.expanded = false;
                });

                audit.expanded = true;
                $scope.filteredDetails = _.filter($scope.auditDetails, function (detail) {
                    return detail.auditVersion === audit.auditVersion;
                });

                if ($scope.filteredDetails) {
                    $scope.filteredDetails.forEach(function (val, vI) {
                        val.dateCreated1 = userService.toLocalDate(val.dateCreated);
                    });
                } else {
                    $scope.filteredDetails = [];
                }
            };

            

            $scope.getRequirementDetails = function () {
                auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        if (response) {
                            $scope.requirementDetails = response;
                        }
                    });
            };
            
            $scope.getRequirementDetails();

        }]);prmApp.constant('PRMAuditServiceDomain', 'audit/svc/PRMAuditService.svc/REST/');
prmApp
    .service('auditreportingServices', ["PRMAuditServiceDomain", "userService", "httpServices", "$window", function (PRMAuditServiceDomain, userService, httpServices, $window) {
        //var domain = 'http://182.18.169.32/services/';
        var auditreportingService = this;

        auditreportingService.getrequirementaudit = function (reqID) {
            let url = PRMAuditServiceDomain + 'getrequirementaudit?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        return auditreportingService;
    }]);prmApp.constant('PRMFwdReqServiceDomain', 'forward/svc/PRMFwdReqService.svc/REST/');
prmApp.constant('signalRFwdHubName', 'fwdRequirementHub');
prmApp.service('PRMFwdReqServiceService', ["PRMFwdReqServiceDomain", "userService", "httpServices",
    function (PRMFwdReqServiceDomain, userService, httpServices) {
        var PRMFwdReqServiceService = this;

        PRMFwdReqServiceService.getrequirementdata = function (params) {
            let url = PRMFwdReqServiceDomain + 'getrequirementdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        PRMFwdReqServiceService.RestartNegotiation = function (params) {
            var url = PRMFwdReqServiceDomain + 'restartnegotiation';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.makeabid = function (params) {
            let url = PRMFwdReqServiceDomain + 'makeabid';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.uploadQuotation = function (params) {
            let url = PRMFwdReqServiceDomain + 'uploadquotation';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.getrevisedquotations = function (params) {
            var url = PRMFwdReqServiceDomain + 'getrevisedquotations';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.revquotationupload = function (params) {
            let url = PRMFwdReqServiceDomain + 'revquotationupload';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.QuatationAprovel = function (params) {
            let url = PRMFwdReqServiceDomain + 'quatationapproval';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.postrequirementdata = function (params) {
            var myDate = new Date();
            var myEpoch = parseInt(myDate.getTime() / 1000);
            params.minBidAmount = 0;
            params.postedOn = "/Date(" + myEpoch + "000+0000)/";
            params.timeLeft = -1;
            params.price = -1;
            var requirement = {
                "requirement": {
                    "title": params.title,
                    "reqType": params.isRFP ? 2 : 1,
                    "description": params.description,
                    "category": params.category,
                    "subcategories": params.subcategories,
                    "urgency": params.urgency,
                    "budget": params.budget,
                    "attachmentName": params.attachmentName,
                    "deliveryLocation": params.deliveryLocation,
                    "taxes": params.taxes,
                    "paymentTerms": params.paymentTerms,
                    "requirementID": params.requirementID,
                    "customerID": params.customerID,
                    "isClosed": params.isClosed,
                    "endTime": null,
                    "sessionID": params.sessionID,
                    "errorMessage": "",
                    "timeLeft": -1,
                    "price": -1,
                    "auctionVendors": params.auctionVendors,
                    "startTime": null,
                    "status": "",
                    "postedOn": "/Date(" + myEpoch + "000+0000)/",
                    "custLastName": params.customerLastname,
                    "custFirstName": params.customerFirstname,
                    "deliveryTime": params.deliveryTime,
                    "includeFreight": params.includeFreight,
                    "inclusiveTax": params.inclusiveTax,
                    "minBidAmount": 0,
                    "checkBoxEmail": params.checkBoxEmail,
                    "checkBoxSms": params.checkBoxSms,
                    "quotationFreezTime": params.quotationFreezTime,
                    "currency": params.currency,
                    "timeZoneID": params.timeZoneID,
                    "listFwdRequirementItems": params.listFwdRequirementItems,
                    "isTabular": params.isTabular,
                    "reqComments": params.reqComments,
                    "itemsAttachment": params.itemsAttachment,
                    "isSubmit": params.isSubmit,
                    "isQuotationPriceLimit": params.isQuotationPriceLimit,
                    "quotationPriceLimit": params.quotationPriceLimit,
                    "noOfQuotationReminders": params.noOfQuotationReminders,
                    "remindersTimeInterval": params.remindersTimeInterval,
                    "custCompID": params.custCompID,
                    "customerCompanyName": params.customerCompanyName,
                    "deleteQuotations": params.deleteQuotations,
                    "indentID": params.indentID,
                    "expStartTime": params.expStartTime,
                    "cloneID": params.cloneID,
                    "isDiscountQuotation": params.isDiscountQuotation,
                    "contactDetails": params.contactDetails,
                    "generalTC": params.generalTC,
                    "isRevUnitDiscountEnable": params.isRevUnitDiscountEnable,
                    "multipleAttachments": params.multipleAttachments,
                    "contractStartTime": params.contractStartTime,
                    "contractEndTime": params.contractEndTime,
                    "isContract": params.isContract,
                    "prCode": params.prCode,
                    "PR_ID": params.PR_ID,
                    "listSlotDetails": params.listSlotDetails,
                    "showInspectionValue": params.showInspectionValue
                }, "attachment": params.attachment
            };
            let url = PRMFwdReqServiceDomain + 'requirementsave';
            return httpServices.post(url, requirement);
        };

        PRMFwdReqServiceService.savepricecap = function (params) {
            let url = PRMFwdReqServiceDomain + 'savepricecap';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.SaveRunningItemPrice = function (params) {
            var url = PRMFwdReqServiceDomain + 'saverunningitemprice';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.GetBidHistory = function (params) {
            let url = PRMFwdReqServiceDomain + 'getbidhistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        PRMFwdReqServiceService.getmyAuctions = function (params) {
            let url = PRMFwdReqServiceDomain + 'getmyauctions?userid=' + params.userid + '&page=' + params.page + '&limit=' + params.limit + '&reqstatus=' + params.reqstatus + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMFwdReqServiceService.getactiveleads = function (params) {
            let url = PRMFwdReqServiceDomain + 'getactiveleads?userid=' + params.userid + '&page=' + params.page + '&limit=' + params.limit + "&sessionid=" + params.sessionid;
            return httpServices.get(url);
        };

        PRMFwdReqServiceService.GetCompanyLeads = function (params) {
            let url = PRMFwdReqServiceDomain + 'getcompanyleads?userid=' + params.userid + '&searchstring=' + params.searchstring + '&searchtype=' + params.searchtype + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        PRMFwdReqServiceService.UpdatePriceCap = function (params) {
            let url = PRMFwdReqServiceDomain + 'updatepricecap';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.bookSlots = function (params) {
            let url = PRMFwdReqServiceDomain + 'bookSlots';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.getslotsbyslotid = function (params) {
            let url = PRMFwdReqServiceDomain + 'getslotsbyslotid?slotId=' + params.slotId + '&ReqId=' + params.ReqId + '&sessionID=' + params.sessionID;
            return httpServices.get(url);
        };

        PRMFwdReqServiceService.approveRejectSlot = function (params) {
            let url = PRMFwdReqServiceDomain + 'approveRejectSlot';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.DeleteSlot = function (params) {
            var url = PRMFwdReqServiceDomain + 'deleteSlot';
            return httpServices.post(url, params);
        };


        return PRMFwdReqServiceService;

}]);