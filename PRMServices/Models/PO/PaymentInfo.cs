﻿
using System;
using System.Runtime.Serialization;
using PRMServices.Models;

namespace PRMServices
{
    [DataContract]
    public class PaymentInfo : ResponseAudit
    {

        [DataMember(Name = "pdID")]
        public int PDID { get; set; }

        [DataMember(Name = "poID")]
        public int POID { get; set; }

        string paymentCode = string.Empty;
        [DataMember(Name = "paymentCode")]
        public string PaymentCode
        {
            get
            {
                if (!string.IsNullOrEmpty(paymentCode))
                { return paymentCode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { paymentCode = value; }
            }
        }

        [DataMember(Name = "paymentAmount")]
        public decimal PaymentAmount { get; set; }

        //[DataMember(Name = "invoiceAmount")]
        //public decimal InvoiceAmount { get; set; }
        string invoiceAmount = string.Empty;
        [DataMember(Name = "invoiceAmount")]
        public string InvoiceAmount
        {
            get
            {
                if (!string.IsNullOrEmpty(invoiceAmount))
                { return invoiceAmount; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { invoiceAmount = value; }
            }
        }


        DateTime paymentDate = DateTime.MaxValue;
        [DataMember(Name = "paymentDate")]
        public DateTime? PaymentDate
        {
            get
            {
                return paymentDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    paymentDate = value.Value;
                }
            }
        }

        [DataMember(Name = "paymentDateTemp")]
        public DateTime? PaymentDateTemp { get; set; }

        [DataMember(Name = "invoiceDateTemp")]
        public DateTime? InvoiceDateTemp { get; set; }

        DateTime invoiceDate = DateTime.MaxValue;
        [DataMember(Name = "invoiceDate")]
        public DateTime? InvoiceDate
        {
            get
            {
                return invoiceDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    invoiceDate = value.Value;
                }
            }
        }

        string paymentComments = string.Empty;
        [DataMember(Name = "paymentComments")]
        public string PaymentComments
        {
            get
            {
                if (!string.IsNullOrEmpty(paymentComments))
                { return paymentComments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { paymentComments = value; }
            }
        }

        string paymentMode = string.Empty;
        [DataMember(Name = "paymentMode")]
        public string PaymentMode
        {
            get
            {
                if (!string.IsNullOrEmpty(paymentMode))
                { return paymentMode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { paymentMode = value; }
            }
        }

        string transactionID = string.Empty;
        [DataMember(Name = "transactionID")]
        public string TransactionID
        {
            get
            {
                if (!string.IsNullOrEmpty(transactionID))
                { return transactionID; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { transactionID = value; }
            }
        }


        string ackComments = string.Empty;
        [DataMember(Name = "ackComments")]
        public string AckComments
        {
            get
            {
                if (!string.IsNullOrEmpty(ackComments))
                { return ackComments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { ackComments = value; }
            }
        }

        string dispatchCode = string.Empty;
        [DataMember(Name = "dispatchCode")]
        public string DispatchCode
        {
            get
            {
                if (!string.IsNullOrEmpty(dispatchCode))
                { return dispatchCode; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { dispatchCode = value; }
            }
        }

        string pr_number = string.Empty;
        [DataMember(Name = "pr_number")]
        public string PrNumber
        {
            get
            {
                if (!string.IsNullOrEmpty(pr_number))
                { return pr_number; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { pr_number = value; }
            }
        }

        string itemName = string.Empty;
        [DataMember(Name = "itemName")]
        public string ItemName
        {
            get
            {
                if (!string.IsNullOrEmpty(itemName))
                { return itemName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { itemName = value; }
            }
        }

        string department = string.Empty;
        [DataMember(Name = "department")]
        public string Department
        {
            get
            {
                if (!string.IsNullOrEmpty(department))
                { return department; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { department = value; }
            }
        }

        string invoiceNumber = string.Empty;
        [DataMember(Name = "invoiceNumber")]
        public string InvoiceNumber
        {
            get
            {
                if (!string.IsNullOrEmpty(invoiceNumber))
                { return invoiceNumber; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { invoiceNumber = value; }
            }
        }

        string paymentStatus = string.Empty;
        [DataMember(Name = "paymentStatus")]
        public string PaymentStatus
        {
            get
            {
                if (!string.IsNullOrEmpty(paymentStatus))
                { return paymentStatus; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { paymentStatus = value; }
            }
        }



        string category = string.Empty;
        [DataMember(Name = "category")]
        public string Category
        {
            get
            {
                if (!string.IsNullOrEmpty(category))
                { return category; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { category = value; }
            }
        }

        DateTime receivedDate = DateTime.MaxValue;
        [DataMember(Name = "receivedDate")]
        public DateTime? ReceivedDate
        {
            get
            {
                return receivedDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    receivedDate = value.Value;
                }
            }
        }




        [DataMember(Name = "poOrderId")]
        public string POOrderId { get; set; }

        [DataMember(Name = "vendorCode")]
        public string VendorCode { get; set; }

        [DataMember(Name = "isACK")]
        public bool IsACK { get; set; }

        [DataMember(Name = "totalPayments")]
        public int TotalPayments { get; set; }

        [DataMember(Name = "paymentDays")]
        public int PaymentDays { get; set; }
    }
}