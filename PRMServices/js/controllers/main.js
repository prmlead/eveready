prmApp
    // =========================================================================
    // Base controller for common functions
    // =========================================================================

    .controller('prmAppCtrl', ["$timeout", "$state", "$log", "$scope", "growlService", "userService", "auctionsService", "$http", "$rootScope", "$location", "SignalRFactory", "domain",
        function ($timeout, $state, $log, $scope, growlService, userService, auctionsService, $http, $rootScope, $location, SignalRFactory, domain) {
        $scope.loggedIn = userService.isLoggedIn();
        $scope.user = {};
            $scope.addrequirementaccess = true;

            //For FreshChat//

            //function initFreshChat() {
            //    window.fcWidget.init({

            //        token: "33d285cd-556c-4496-8cd5-569163faf9e1",
            //        host: "https://wchat.in.freshchat.com",
            //        siteId: "TRIAL" + "-" + userService.getUserType(),
            //        firstName: userService.userData.currentUser.firstName,
            //        lastName: userService.userData.currentUser.lastName,
            //        email: userService.getUserObj().email,
            //        phone: userService.getUserObj().phoneNum,
            //        designation: userService.getUserObj().DESIGNATION

            //    })

            //}
            
            //function initialize(i, t) {
            //    var e;
            //    i.getElementById(t) ? initFreshChat() :
            //        ((e = i.createElement("script")).id = t,
            //            e.async = !0, e.src = "https://wchat.in.freshchat.com/js/widget.js",
            //            e.onload = initFreshChat, i.head.appendChild(e))
            //}
            //function initiateCall() {
            //    initialize(document, "freshchat-js-sdk")
            //}
            //window.addEventListener ?
            //    window.addEventListener("load", initiateCall, !1) :
            //    window.attachEvent("load", initiateCall, !1);
        //For FreshChat

        $scope.showAddNewReq = function () {
            if (window.location.hash != "#/login" && userService.getUserType() == "CUSTOMER") {
                return true;
            } else {
                return false;
            }
        }

        $scope.showNavigation = function (pagename) {
            var userId = userService.getUserId();
            var isValid = false;
            if (pagename && pagename != '') {
                if (userId == 4777 || userId == 4865 || userId == 3762 || userId == 1069 || userId == 4900) {
                    isValid = true;
                }
                else if (userId == 4846 || userId == 4840 || userId == 4839 || userId == 4845 || userId == 4838) {
                    if (pagename == 'OpenPR' || pagename == 'OpenPO' || pagename == 'SHORTAGEREPORT') {
                        isValid = true;
                    }
                    else {
                        isValid = false;
                    }
                }
                else {
                    if (pagename == 'OpenPR' || pagename == 'OpenPO' || pagename == 'REQUIREMENTS' || pagename == 'LEADS' || pagename == 'CREATEREQUIREMENT' || pagename == 'DASHBOARD') {
                        isValid = true;
                    }
                    else {
                        isValid = false;
                    }
                }
            }
            else {
                isValid = false;
            }

            return isValid;
        }

            $scope.showIsUserVendor = function () {
                if (window.location.hash != "#/login" && userService.getUserType() == "VENDOR") {
                    return true;
                } else {
                    return false;
                }
            };


            $scope.getCurrentYear = function () {
                return new Date().getFullYear();
            };

            $scope.isCatalogueEnabled = function () {
                if (userService.getUserObj().isCatalogueEnabled) {
                    return true;
                } else {
                    return false;
                }
            };

            $scope.showIsSuperUser = function () {
                if (userService.getUserObj().isSuperUser) {
                    return true;
                } else {
                    return false;
                }
            };


        $scope.showAddButtons = function () {

        }

        $scope.registerobj = {};
        var options = {
            loop: true,
            margin: 10,
            dots: false,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: true,
            nav: false,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 3,
                },
                1000: {
                    items: 4
                }
            }
        }
        // $scope.formRequest = {};
        $scope.userError = {};
        $scope.msg = userService.getMessage();
        $scope.showMessage = true;
        $scope.successshowMessage = true;
        $scope.userData = userService.userData;
        if ($scope.userData.currentUser != null) {
            $scope.fullName = userService.userData.currentUser.firstName + " " + userService.userData.currentUser.lastName;
        } else {
            $scope.fullName = "";
        }

        $scope.closeMsg = function () {
            $scope.showMessage = false;
        }
            $scope.array = [1, 2, 3, 4];



            //$scope.comment = " ";

            $scope.requestSupport = function () {
                let userObj = userService.getUserObj();
                if ($scope.comment == '' || $scope.comment == null || $scope.comment == 'undefined') {
                    $scope.commentError = 'Comment is required .';
                    return;
                }
                //params = {
                //    "telegrammsg": {
                //        message: 'UserType: ' + userObj.userType + '<br/>Company Name: ' + userObj.institution + '<br/>User Name: ' + userObj.username +
                //            '<br/>Phone: ' + userObj.phoneNum + '<br/>Email:' + userObj.email + '<br/>userID: ' + userObj.userID + '<br/>Environment: ' + window.location + '<br/>Comment: ' + $scope.comment,
                //        moduleName: 'Support',
                //        group: 'Training Request dev'
                //    }
                //};

                //$http({
                //    method: 'POST',
                //    url: domain + 'requestSupport',
                //    encodeURI: true,
                //    headers: { 'Content-Type': 'application/json' },
                //    data: params
                //}).then(function (response) {
                //    if (response && response.data && (response.data.errorMessage == "" || response.data.errorMessage == null)) {
                //        $scope.NegotiationSettings = response.data;
                //        $scope.comment = '';
                //        growlService.growl('Successfully Requested Support', 'success');
                //    }
                //    else {
                //        growlService.growl('Failed to contact support', 'inverse');
                //    }
                //});

                $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

                userService.getCompanyName().then(function (response) {
                    if (response) {
                        $scope.companyName = response;
                        var tags = [$scope.companyName]

                        if ($scope.isCustomer) {
                            tags.push('Customer');
                        }
                        var tickectParams = {
                            name: userObj.firstName + ' ' + userObj.lastName,
                            email: userObj.email,
                            phone: userObj.phoneNum,
                            subject: $scope.isCustomer ? 'Support Request by ' + userObj.firstName + ' ' + userObj.lastName + ' for ' + $scope.companyName : 'Vendor Support Request by ' + userObj.firstName + ' ' + userObj.lastName + ' for ' + $scope.companyName,
                            status: 2,
                            priority: 3,
                            description: $scope.comment,
                            email_config_id: 81000024659,
                            group_id: $scope.isCustomer ? 81000193970 : 81000274957,
                            source: 2,
                            type: 'Support Request',
                            tags: tags

                        };
                        console.log(tickectParams);
                        
                        $http({
                            method: 'POST',
                            url: "https://prm360.freshdesk.com/api/v2/tickets",
                            encodeURI: true,
                            headers: {
                                'Content-Type': 'application/json',
                                "Authorization": "Basic " + window.btoa('oLRuHDsQvII8LrfthZAj' + ":x")
                            },
                            data: tickectParams,
                            dataType: "json"
                        }).then(function (response) {
                            $scope.comment = '';
                            return response.data;
                        });
                    } else {
                        //console.log(response.data[0].errorMessage);
                    };
                });
            };

        $scope.logout = function () {
            userService.logout().then(function () {
                $scope.loggedIn = userService.isLoggedIn();
            });
        }

        $scope.getErrMsg = function (message) {
            if (typeof message == 'string') {
                return message;
            } else if (typeof message == 'object') {
                var result = "";
                angular.forEach(message, function (value, key) {
                    result += value[0] + '<br />';
                });
                return result;
            }
        }
        // Detact Mobile Browser
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            angular.element('html').addClass('ismobile');
        }

        // By default Sidbars are hidden in boxed layout and in wide layout only the right sidebar is hidden.
        this.sidebarToggle = {
            left: false,
            right: false
        }

        // By default template has a boxed layout
        this.layoutType = localStorage.getItem('ma-layout-status');

        // For Mainmenu Active Class
        this.$state = $state;

        //Close sidebar on click
        this.sidebarStat = function (event) {
            //if (!angular.element(event.target).parent().hasClass('active')) {
            //    this.sidebarToggle.left = false;
            //}


            if (this.sidebarToggle.left == true) {
                this.sidebarToggle.left = false;
            } else {
                this.sidebarToggle.left = true;
            }

        }

        //Listview Search (Check listview pages)
        this.listviewSearchStat = false;

        this.lvSearch = function () {
            this.listviewSearchStat = true;
        }

        //Listview menu toggle in small screens
        this.lvMenuStat = false;

        //Blog
        this.wallCommenting = [];

        this.wallImage = false;
        this.wallVideo = false;
        this.wallLink = false;

        //Skin Switch
        this.currentSkin = 'blue';

        this.skinList = [
            'lightblue',
            'bluegray',
            'cyan',
            'teal',
            'green',
            'orange',
            'blue',
            'purple'
        ]

        this.skinSwitch = function (color) {
            this.currentSkin = color;
        };


        $scope.reduceTime = function (timerId, time) {
            addCDSeconds(timerId, time);
        }

        $scope.getUserDataNoCache = function () {
            userService.getUserDataNoCache()
                .then(function (response) {
                    $scope.userObj = response;
                })
        };

         $scope.getUserDataNoCache();

        $scope.stopBid = function (item) {
            $scope.myHotAuctions[0].TimeLeft = 60;
        }
        }]);



prmApp.config(['stConfig', function (stConfig) { stConfig.sort.delay = 100; stConfig.pipe.delay = 200; }]);
prmApp.factory("AngularUtility", [function () {
    var _ngUtility = {}

    _ngUtility.preparePager = function (tableState, colOptions) {
        var obj = {
            pageIndex: (tableState.pagination.start / tableState.pagination.number),
            pageSize: tableState.pagination.number,
            criteria: {
                orderby: [],
                filter: [],
                isStrictSearch: false,
                searchTerm: ""
            }
        }

        var _colOptions = [].concat(colOptions);

        //Filter
        var filter = angular.copy(tableState.search);
        if (!angular.equals(filter, {})) {

            var _filters = [];

            for (var property in filter.predicateObject) {
                if (filter.predicateObject.hasOwnProperty(property)) {
                    if (property !== "searchTerm") {
                        var _fil = { column: property, value: filter.predicateObject[property] }
                        if (_.some(_colOptions, function (col) { return col.field === property; })) {

                            var _col = _.chain(_colOptions).filter(function (x) { return x.field === property }).first().value()

                            _fil.type = _col.type
                        }
                        _filters.push(_fil);
                    }
                    else {
                        obj.criteria.searchTerm = filter.predicateObject.searchTerm
                    }

                }
            }

            obj.criteria.filter = _filters;
        }
        //Sort
        var sort = tableState.sort;
        if (!angular.equals(sort, {})) {

            var _sort = {
                column: sort.predicate,
                direction: sort.reverse ? "DESC" : "ASC"
            }

            obj.criteria.orderby.push(_sort);
        }
        return obj
    };


    _ngUtility.objectIsNotNull = function (obj) {
        if (!angular.equals(null, obj) && !angular.equals({}, obj) && !angular.equals(undefined, obj)) {
            return true;
        }
        return false;
    }

    return _ngUtility;


}]);