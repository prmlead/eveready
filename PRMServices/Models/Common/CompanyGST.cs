﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class CompanyGST : Entity
    {        

        [DataMember(Name = "companyGSTId")]
        [DataNames("COMP_GST_ID")]
        public int COMP_GST_ID { get; set; }

        [DataMember(Name = "vendorId")]
        [DataNames("VENDOR_ID")]
        public int VENDOR_ID { get; set; }

        [DataMember(Name = "gstNumber")]
        [DataNames("GST_NUMBER")]
        public string GST_NUMBER { get; set; }

        [DataMember(Name = "companyId")]
        [DataNames("COMP_ID")]
        public int COMP_ID
        {
            get;set;
        }

        [DataMember(Name = "gstAddr")]
        [DataNames("GST_ADDR")]
        public string GST_ADDR { get; set; }


        [DataMember(Name = "vendorCode")]
        [DataNames("VENDOR_CODE")]
        public string VENDOR_CODE { get; set; }

        [DataMember(Name = "vendorSiteCode")]
        [DataNames("VENDOR_SITE_CODE")]
        public string VENDOR_SITE_CODE { get; set; }

        [DataMember(Name = "vendorSiteStatus")]
        [DataNames("VENDOR_SITE_STATUS")]
        public int VENDOR_SITE_STATUS { get; set; }

    }
}