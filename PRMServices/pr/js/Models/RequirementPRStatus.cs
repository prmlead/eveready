﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class RequirementPRStatus
    {

        [DataMember]  public string MESSAGE { get; set; }

        [DataMember] public string MESSAGE_TYPE { get; set; }

        [DataMember] public int ACTION_REQUIRED { get; set; }

        [DataMember] public PRItems[] PRItemsList { get; set; }

        [DataMember] public Requirement RequirementDetails { get; set; }
    }
}