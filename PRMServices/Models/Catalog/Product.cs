﻿using PRM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models.Catalog
{
    [DataContract]
    public class Product : EntityExt
    {
        [DataMember(Name = "prodId")]
        public int ProductId { get; set; }

        [DataMember(Name = "totalProducts")]
        public int TotalProducts { get; set; }

        [DataMember(Name = "totalUserProducts")]
        public int TotalUserProducts { get; set; }

        [DataMember(Name = "compId")]
        public int CompanyId { get; set; }

        //[DataMember(Name = "prodCode")]
        //public string ProductCode { get; set; }


        string prodCode = string.Empty;
        [DataMember(Name = "prodCode")]
        public string ProductCode
        {
            get
            {
                return prodCode;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    prodCode = value;
                }
            }
        }

        [DataMember(Name = "prodName")]
        public string ProductName { get; set; }

        [DataMember(Name = "prodHSNCode")]
        public string ProductHSNCode { get; set; }

        [DataMember(Name = "prodNo")]
        public string ProductNo { get; set; }

        [DataMember(Name = "prodQty")]
        public string ProdQty { get; set; }

        [DataMember(Name = "prodDesc")]
        public string ProductDesc { get; set; }
        
        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }

        [DataMember(Name = "prodAlternateUnits")]
        public string ProdAlternativeUnits { get; set; }

        [DataMember(Name = "unitConversion")]
        public string UnitConversion { get; set; }

        [DataMember(Name = "shelfLife")]
        public string ShelfLife { get; set; }

        [DataMember(Name = "productVolume")]
        public string ProductVolume { get; set; }

        [DataMember(Name = "isCoreProductCategory")]
        public int IsCoreProductCategory { get; set; } = 1;

        //[DataMember(Name = "prodSelected")]
        //public int ProductSelected { get; set; }

        //[DataMember(Name = "prodChecked")]
        //public bool ProductChecked { get; set; }

        [DataMember(Name = "productGST")]
        public decimal ProductGST { get; set; }

        [DataMember(Name = "prefferedBrand")]
        public string PrefferedBrand { get; set; }

        [DataMember(Name = "alternateBrand")]
        public string AlternateBrand { get; set; }

        [DataMember(Name = "totPurchaseQty")]
        public int TotPurchaseQty { get; set; }

        [DataMember(Name = "inTransit")]
        public int InTransit { get; set; }

        [DataMember(Name = "leadTime")]
        public string LeadTime { get; set; }

        [DataMember(Name = "departments")]
        public string Departments { get; set; }

        [DataMember(Name = "deliveryTerms")]
        public string DeliveryTerms { get; set; }

        [DataMember(Name = "termsConditions")]
        public string TermsConditions { get; set; }

        [DataMember(Name = "listVendorDetails")]
        public List<ProductVendorDetails> ListVendorDetails { get; set; }

        [DataMember(Name = "multipleAttachments")]
        public List<FileUpload> MultipleAttachments { get; set; }


        string itemAttachments = string.Empty;
        [DataMember(Name = "itemAttachments")]
        public string ItemAttachments
        {
            get
            {
                return this.itemAttachments;
            }
            set
            {
                this.itemAttachments = value;
            }
        }

        [DataMember(Name = "contractManagement")]
        public List<ContractManagementDetails> ContractManagement { get; set; }

        [DataMember(Name = "CONTRACT_VENDOR")]  public int CONTRACT_VENDOR { get; set; }

        [DataMember(Name = "casNumber")]
        public string CasNumber { get; set; }

        [DataMember(Name = "mfcdCode")]
        public string MfcdCode { get; set; }

        [DataMember(Name = "categoryId")]
        public string CategoryId { get; set; }

        [DataMember(Name = "prodAlternativeUnits1")]
        public string ProdAlternativeUnits1 { get; set; }
        [DataMember(Name = "prodAlternativeUnits2")]
        public string ProdAlternativeUnits2 { get; set; }
        [DataMember(Name = "prodAlternativeUnits3")]
        public string ProdAlternativeUnits3 { get; set; }
        [DataMember(Name = "prodAlternativeUnits4")]
        public string ProdAlternativeUnits4 { get; set; }

        [DataMember(Name = "prodAlternativeUnits5")]
        public string ProdAlternativeUnits5 { get; set; }

        [DataMember(Name = "prodAlternativeUnits6")]
        public string ProdAlternativeUnits6 { get; set; }
        [DataMember(Name = "prodAlternativeUnits7")]
        public string ProdAlternativeUnits7 { get; set; }

        [DataMember(Name = "prodAlternativeUnits_con1")]
        public decimal ProdAlternativeUnits_con1 { get; set; }
        [DataMember(Name = "prodAlternativeUnits_con2")]
        public decimal ProdAlternativeUnits_con2 { get; set; }
        [DataMember(Name = "prodAlternativeUnits_con3")]
        public decimal ProdAlternativeUnits_con3 { get; set; }
        [DataMember(Name = "prodAlternativeUnits_con4")]
        public decimal ProdAlternativeUnits_con4 { get; set; }

        [DataMember(Name = "prodAlternativeUnits_con5")]
        public decimal ProdAlternativeUnits_con5 { get; set; }

        [DataMember(Name = "prodAlternativeUnits_con6")]
        public decimal ProdAlternativeUnits_con6 { get; set; }

        [DataMember(Name = "prodAlternativeUnits_con7")]
        public decimal ProdAlternativeUnits_con7 { get; set; }

        string productImage = string.Empty;
        [DataMember(Name = "productImage")]
        public string ProductImage
        {
            get
            {
                return productImage;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    productImage = value;
                }
            }
        }

        [DataMember(Name = "VendorPartNumber")]
        public string VendorPartNumber { get; set; }

    }

    [DataContract]
    public class ContractManagementDetails
    {
        [DataMember(Name = "PC_ID")]
        public int PC_ID { get; set; }

        [DataMember(Name = "number")]
        public string Number { get; set; }

        [DataMember(Name = "REQ_TYPE")]
        public string REQ_TYPE { get; set; }

        [DataMember(Name = "ITEM_ID")]
        public int ITEM_ID { get; set; }

        [DataMember(Name = "value")]
        public decimal Value { get; set; }

        [DataMember(Name = "price")]
        public double Price { get; set; }

        [DataMember(Name = "ProductId")]
        public int ProductId { get; set; }

        [DataMember(Name = "ProductName")]
        public string ProductName { get; set; }

        [DataMember(Name = "U_ID")]
        public int U_ID { get; set; }

        [DataMember(Name = "document")]
        public string Document { get; set; }

        [DataMember(Name = "quantity")]
        public decimal Quantity { get; set; }

        [DataMember(Name = "editQuantity")]
        public decimal editQuantity { get; set; }

        [DataMember(Name = "availedQuantity")]
        public decimal AvailedQuantity { get; set; }

        [DataMember(Name = "vendorId")]
        public int VendorId { get; set; }

        [DataMember(Name = "startTime")]
        public DateTime? StartTime { get; set; }

        [DataMember(Name = "endTime")]
        public DateTime? EndTime { get; set; }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "contractNumber")]
        public string ContractNumber { get; set; }

        [DataMember(Name = "contractValue")]
        public decimal ContractValue { get; set; }

        [DataMember(Name = "categories")]
        public string Categories { get; set; }

        [DataMember(Name = "totalNoOfContracts")]
        public int TotalNoOfContracts { get; set; }

        [DataMember(Name = "totalContractualValue")]
        public decimal TotalContractualValue { get; set; }

        [DataMember(Name = "contractsUtilization")]
        public decimal ContractsUtilization { get; set; }

        [DataMember(Name = "phoneNumber")]
        public string PhoneNumber { get; set; }

        [DataMember(Name = "supplierName")]
        public string SupplierName { get; set; }

        [DataMember(Name = "poNumber")]
        public string PONumber { get; set; }

        [DataMember(Name = "TARGET_VALUE")]
        public decimal TARGET_VALUE { get; set; }

        [DataMember(Name = "CURRENCY")]
        public string CURRENCY { get; set; }

        [DataMember(Name = "PAYMENT_TERMS")]
        public string PAYMENT_TERMS { get; set; }

        [DataMember(Name = "INCO_TERMS")]
        public string INCO_TERMS { get; set; }

        [DataMember(Name = "MANUFACTURER")]
        public string MANUFACTURER { get; set; }

        [DataMember(Name = "DELIVERY_ADDRESS")]
        public string DELIVERY_ADDRESS { get; set; }

        [DataMember(Name = "AGREEMENT_TYPE")]
        public string AGREEMENT_TYPE { get; set; }

        [DataMember(Name = "TAX_CODE")]
        public string TAX_CODE { get; set; }

        [DataMember(Name = "EXCH_RATE")]
        public decimal EXCH_RATE { get; set; }

        [DataMember(Name = "COMP_ID")]
        public int COMP_ID { get; set; }

        [DataMember(Name = "CREATED_BY")]
        public int CREATED_BY { get; set; }

        [DataMember(Name = "CREATED_DATE")]
        public DateTime? CREATED_DATE { get; set; }

        [DataMember(Name = "MODIFIED_BY")]
        public int MODIFIED_BY { get; set; }

        [DataMember(Name = "MODIFIED_DATE")]
        public DateTime? MODIFIED_DATE { get; set; }

        [DataMember(Name = "ADDRESS")]
        public string ADDRESS { get; set; }

        [DataMember(Name = "priceType")]
        public string PriceType { get; set; }

        [DataMember(Name = "selectedVendorCode")]
        public string SelectedVendorCode { get; set; }

        [DataMember(Name = "DELIVERY_TERMS")]
        public string DELIVERY_TERMS { get; set; }

        [DataMember(Name = "PLANT_CODE")]
        public string PLANT_CODE { get; set; }

        [DataMember(Name = "PLANT")]
        public string PLANT { get; set; }
        
        [DataMember(Name = "GENERAL_TERMS")]
        public string GENERAL_TERMS { get; set; }

        [DataMember(Name = "DELIVERY_FROM")]
        public string DELIVERY_FROM { get; set; }

        [DataMember(Name = "DELIVERY_AT")]
        public string DELIVERY_AT { get; set; }

        [DataMember(Name = "LEADTIME")]
        public string LEADTIME { get; set; }

        [DataMember(Name = "WARRANTY")]
        public string WARRANTY { get; set; }

        [DataMember(Name = "SPECIAL_INSTRUCTIONS")]
        public string SPECIAL_INSTRUCTIONS { get; set; }

        [DataMember(Name = "NET_PRICE")]
        public decimal NET_PRICE { get; set; }


        [DataMember(Name = "DISCOUNT")]
        public decimal DISCOUNT { get; set; }

        [DataMember(Name = "GST")]
        public decimal GST { get; set; }

        [DataMember(Name = "CUSTOM_DUTY")]
        public decimal CUSTOM_DUTY { get; set; }

        [DataMember(Name = "DISCOUNT_ELIGIBILITY")]
        public string DISCOUNT_ELIGIBILITY { get; set; }

        [DataMember(Name = "RECONCILIATION_TIMELINES")]
        public string RECONCILIATION_TIMELINES { get; set; }

        [DataMember(Name = "EXCHANGE_RATE")]
        public decimal EXCHANGE_RATE { get; set; }

        [DataMember(Name = "PO_CURRENCY")]
        public string PO_CURRENCY { get; set; }

        [DataMember(Name = "USER")]
        public int USER { get; set; }

        [DataMember(Name = "PRODUCT_CODE")]
        public string PRODUCT_CODE { get; set; }

        [DataMember(Name = "VENDOR_CODE")]
        public string VENDOR_CODE { get; set; }

        [DataMember(Name = "VendorPartNumber")]
        public string VendorPartNumber { get; set; }

        [DataMember(Name = "PR_ITEM_ID")]
        public string PR_ITEM_ID { get; set; }

        [DataMember(Name = "allocationPercentage")]
        public decimal AllocationPercentage { get; set; }

        [DataMember(Name = "contractStatus")]
        public string ContractStatus { get; set; }

        [DataMember(Name = "REQ_ID")]
        public int REQ_ID { get; set; }

        [DataMember(Name = "QCS_ID")]
        public int QCS_ID { get; set; }

        [DataMember(Name = "listUtilisationDetails")]
        public List<ContractUtilisation> ListUtilisationDetails { get; set; }

        [DataMember(Name = "multipleAttachments")]
        public List<FileUpload> MultipleAttachments { get; set; }

        [DataMember(Name = "sessionId")]
        public string SessionId { get; set; }

        [DataMember(Name = "VENDOR_SITE_CODE")]
        public string VENDOR_SITE_CODE { get; set; }

        [DataMember(Name = "SITE_CODE")]
        public string SITE_CODE { get; set; }

        [DataMember(Name = "PR_NUMBER")]
        public string PR_NUMBER { get; set; }

        [DataMember(Name = "REQUIRED_QUANTITY")]
        public decimal REQUIRED_QUANTITY { get; set; }

        [DataMember(Name = "VENDOR_COMPANY_ID")]
        public int VENDOR_COMPANY_ID { get; set; }

        [DataMember(Name = "errorMessage")]
        public string ErrorMessage { get; set; }

        [DataMember(Name = "PR_LINE_ITEM_INFO")]
        public string PR_LINE_ITEM_INFO { get; set; }

        [DataMember(Name = "allocationDetails")]
        public List<Allocation> allocationDetails { get; set; }

        [DataMember(Name = "startTimeTemp")]
        public DateTime? StartTimeTemp { get; set; }

        [DataMember(Name = "endTimeTemp")]
        public DateTime? EndTimeTemp { get; set; }

    }

    [DataContract]
    public class VendorWiseContractManagementDetails
    {
        [DataMember(Name = "VendorId")]
        public int VendorID { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "contractItems")]
        public List<ContractManagementDetails> ContractItems { get; set; }
    }


    [DataContract]
    public class ContractUtilisation
    {
        [DataMember(Name = "PC_ID")]
        public int PC_ID { get; set; }

        [DataMember(Name = "COMP_ID")]
        public int COMP_ID { get; set; }

        [DataMember(Name = "U_ID")]
        public int U_ID { get; set; }

        [DataMember(Name = "VENDOR_ID")]
        public int VENDOR_ID { get; set; }
        
        [DataMember(Name = "PR_NUMBER")]
        public string PR_NUMBER { get; set; }

        [DataMember(Name = "PR_LINE_ITEM")]
        public string PR_LINE_ITEM { get; set; }

        [DataMember(Name = "PO_NUMBER")]
        public string PO_NUMBER { get; set; }

        [DataMember(Name = "PO_LINE_ITEM")]
        public string PO_LINE_ITEM { get; set; }

        [DataMember(Name = "QTY_UTILISED")]
        public decimal QTY_UTILISED { get; set; }

        [DataMember(Name = "VENDOR_COMP_NAME")]
        public string VENDOR_COMP_NAME { get; set; }

        [DataMember(Name = "CONTRACT_NUMBER")]
        public string CONTRACT_NUMBER { get; set; }

        [DataMember(Name = "CREATED_BY_NAME")]
        public string CREATED_BY_NAME { get; set; }

        [DataMember(Name = "TOTAL_ROWS")]
        public int TOTAL_ROWS { get; set; }

        [DataMember(Name = "PRODUCT_NAME")]
        public string PRODUCT_NAME { get; set; }

        [DataMember(Name = "PLANT_CODE")]
        public string PLANT_CODE { get; set; }

        [DataMember(Name = "DATE_CREATED")]
        public DateTime? DATE_CREATED { get; set; }

        [DataMember(Name = "CREATED_DATE")]
        public DateTime? CREATED_DATE { get; set; }

        [DataMember(Name = "MODIFIED_DATE")]
        public DateTime? MODIFIED_DATE { get; set; }

        [DataMember(Name = "REQUIRED_QUANTITY")]
        public decimal REQUIRED_QUANTITY { get; set; }

        [DataMember(Name = "PR_ITEM_ID")]
        public string PR_ITEM_ID { get; set; }

        [DataMember(Name = "PRODUCT_ID")]
        public string PRODUCT_ID { get; set; }

    }

    [DataContract]
    public class Allocation
    {

        [DataMember(Name = "REQUIRED_QUANTITY")]
        public decimal REQUIRED_QUANTITY { get; set; }

        [DataMember(Name = "ASSIGN_QTY")]
        public decimal ASSIGN_QTY { get; set; }

        [DataMember(Name = "ASSIGN_PRICE")]
        public decimal ASSIGN_PRICE { get; set; }

        [DataMember(Name = "QTY_UTILISED")]
        public decimal QTY_UTILISED { get; set; }

        [DataMember(Name = "VENDOR_ID")]
        public int VENDOR_ID { get; set; }

        [DataMember(Name = "REQ_ID")]
        public int REQ_ID { get; set; }

        [DataMember(Name = "QCS_ID")]
        public int QCS_ID { get; set; }

        [DataMember(Name = "PR_NUMBER")]
        public string PR_NUMBER { get; set; }

        [DataMember(Name = "VENDOR_CODE")]
        public string VENDOR_CODE { get; set; }

        [DataMember(Name = "VENDOR_SITE_CODE")]
        public string VENDOR_SITE_CODE { get; set; }

        [DataMember(Name = "PR_LINE_ITEM")]
        public string PR_LINE_ITEM { get; set; }

        [DataMember(Name = "PC_ID")]
        public int PC_ID { get; set; }

        [DataMember(Name = "REQ_CURRENCY_FACTOR")]
        public decimal REQ_CURRENCY_FACTOR { get; set; }

        [DataMember(Name = "TOTAL_PRICE")]
        public decimal TOTAL_PRICE { get; set; }

        [DataMember(Name = "ITEM_ID")]
        public int ITEM_ID { get; set; }

        [DataMember(Name = "PR_ITEM_ID")]
        public string PR_ITEM_ID { get; set; }

        [DataMember(Name = "PLANT_CODE")]
        public string PLANT_CODE { get; set; }

    }


    [DataContract]
    public class PRITEM_IDS
    {
       
        [DataMember(Name = "PR_ITEM_ID")]
        public string PR_ITEM_ID { get; set; }


    }



    [DataContract]
    public class ImportEntity : Entity
    {
        [DataMember(Name = "entityName")]
        public string EntityName
        {
            get;
            set;
        }

        [DataMember(Name = "userid")]
        public int UserID
        {
            get;
            set;
        }

        [DataMember(Name = "attachment")]
        public byte[] Attachment
        {
            get;
            set;
        }

        [DataMember(Name = "attachmentFileName")]
        public string AttachmentFileName
        {
            get;
            set;
        }
    }

    [DataContract]
    public class ProductFilters
    {
        [DataMember(Name = "filterType")]
        public string FilterType
        {
            get;
            set;
        }

        [DataMember(Name = "filterField")]
        public string FilterField
        {
            get;
            set;
        }

        [DataMember(Name = "filterCond")]
        public string FilterCond
        {
            get;
            set;
        }

        [DataMember(Name = "filterValue")]
        public string FilterValue
        {
            get;
            set;
        }

        [DataMember(Name = "filterFieldDataType")]
        public string filterFieldDataType
        {
            get;
            set;
        }
    }

    [DataContract]
    public class CompanyConfiguration : Entity
    {

        [DataMember(Name = "compConfigID")]
        public int CompConfigID { get; set; }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "configKey")]
        public string ConfigKey { get; set; }

        [DataMember(Name = "configValue")]
        public string ConfigValue { get; set; }

        [DataMember(Name = "isValid")]
        public bool IsValid { get; set; }

        [DataMember(Name = "configText")]
        public string ConfigText { get; set; }

        

    }
}