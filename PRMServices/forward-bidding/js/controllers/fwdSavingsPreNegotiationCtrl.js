﻿prmApp
    .controller('fwdSavingsPreNegotiationCtrl', ["$scope", "$state", "$stateParams", "$window", "$timeout", "userService",
        "PRMCustomFieldService", "PRMForwardBidService",
        function ($scope, $state, $stateParams, $window, $timeout, userService, PRMCustomFieldService, PRMForwardBidService) {
            $scope.id = $stateParams.Id;
            $scope.totalItemsMinPrice = 0;
            $scope.totalLeastBidderPrice = 0;
            $scope.totalInitialPrice = 0;
            $scope.sessionid = userService.getUserToken();
            $scope.requirementSettings = [];
            $scope.selectedTemplate = {};
            $scope.prmFieldMappingDetails = {};

            $scope.getData = function () {
                PRMForwardBidService.GetPriceComparisonPreNegotiation({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.auctionItem = response.requirement;
                        $scope.priceCompObj = response;
                        $scope.totalItemsL1Price = 0;
                        $scope.totalItemsMinimunPrice = 0;
                        $scope.negotiationSavings = 0;
                        $scope.savingsByLeastBidder = 0;
                        $scope.savingsByItemMinPrice = 0;

                        $scope.L1CompanyName = '';
                        $scope.L1CompanyName = $scope.auctionItem.auctionVendors[0].companyName;

                        if ($scope.auctionItem.auctionVendors[0].companyName == "PRICE_CAP") {
                            $scope.L1CompanyName = $scope.auctionItem.auctionVendors[1].companyName;
                        }

                        $scope.priceCompObj.priceCompareObject.forEach(function (priceObj, vI) {
                            let factorTemp = _.filter($scope.auctionItem.auctionVendors, function (vendor) {
                                return Number(vendor.vendorID) === Number(priceObj.minPriceBidder);
                            });

                            priceObj.vendorCurrencyFactor = factorTemp[0].vendorCurrencyFactor;
                            $scope.totalItemsL1Price += priceObj.leastBidderPrice * factorTemp[0].vendorCurrencyFactor;
                            $scope.totalItemsMinimunPrice += priceObj.minPrice * factorTemp[0].vendorCurrencyFactor;
                        });

                        $scope.negotiationSavings = $scope.auctionItem.savings;
                        $scope.savingsByLeastBidder = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsL1Price;
                        $scope.savingsByItemMinPrice = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsMinimunPrice;

                        $scope.additionalSavings = 0;

                        $scope.additionalSavings = $scope.totalItemsL1Price - $scope.totalItemsMinimunPrice;

                    });
            };

            $scope.isReportGenerated = 0;

            $scope.GetReportsRequirement = function () {
                PRMForwardBidService.GetReportsRequirement({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.reports = response;
                        $scope.getData();
                        $scope.isReportGenerated = 1;
                    });
            };

            $scope.getData();

            $scope.doPrint = false;
            $scope.printReport = function () {
                $scope.priceCompObj.priceCompareObject.forEach(function (item, index) {
                    item.expanded = true;
                });

                $scope.doPrint = true;
                $timeout(function () {
                    $window.print();
                    $scope.doPrint = false;
                    $scope.priceCompObj.priceCompareObject.forEach(function (item, index) {
                        item.expanded = false;
                    });

                }, 1000);
            };

            $scope.getRequirementSettings = function () {
                $scope.requirementSettings = [];
                $scope.selectedTemplate.TEMPLATE_NAME = 'PRM_DEFAULT';
                PRMForwardBidService.getRequirementSettings({ "reqid": $scope.id, "sessionid": $scope.sessionid })
                    .then(function (response) {
                        $scope.requirementSettings = response;
                        if ($scope.requirementSettings && $scope.requirementSettings.length > 0) {
                            var template = $scope.requirementSettings.filter(function (setting) {
                                return setting.REQ_SETTING === 'TEMPLATE_ID';
                            });

                            if (template && template.length > 0) {
                                $scope.selectedTemplate.TEMPLATE_ID = template[0].REQ_SETTING_VALUE;
                            }
                        }

                        if ($scope.selectedTemplate.TEMPLATE_ID || $scope.selectedTemplate.TEMPLATE_NAME) {
                            $scope.GetPRMTemplateFields();
                        }
                    });
            };

            $scope.getRequirementSettings();

            $scope.GetPRMTemplateFields = function () {
                $scope.prmFieldMappingDetails = {};
                var params = {
                    "templateid": $scope.selectedTemplate.TEMPLATE_ID ? $scope.selectedTemplate.TEMPLATE_ID : 0,
                    "templatename": $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : '',
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        $scope.prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });
                });
            };
        }]);