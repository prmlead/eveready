﻿prmApp
    .controller('addnewstoreCtrl', ["$scope", "$stateParams", "$state", "$window", "$log", "userService", "storeService", "growlService",
        function ($scope, $stateParams, $state, $window, $log, userService, storeService, growlService) {
            $scope.storeID = $stateParams.storeID == "" ? 0 : $stateParams.storeID;
            $scope.compID = userService.getUserCompanyId();
            $scope.companyStores = [];
            $scope.isValidToSubmit = true;

            $scope.getcompanystores = function () {
                storeService.getcompanystores($scope.compID, 0)
                .then(function (response) {
                    $scope.companyStores = response;
                });
            }

            $scope.getcompanystores();


            $scope.companyID = userService.getUserCompanyId();
            $scope.sessionID = userService.getUserToken();

            $scope.storeObj = {
                storeID: $scope.storeID,
                companyID: $scope.companyID,
                //isMainBranch: 0,
                mainStoreID: 0,
                //storeCode: '',
                //storeDescription: '',
                //storeInCharge: '',
                //storeDetails: '',
                //isValid: 0,
                //storeAddress: '',
                //storeBranches: 0,
                sessionID: $scope.sessionID
            };

            $scope.goToBranches = function (store) {
                var url = $state.href('branches', { "storeID": store.storeID });
                $window.open(url, '_blank');
                //$state.go("branches", { "storeID": store.storeID });
            }

            if ($scope.storeID > 0) {
                if (!$stateParams.storeObj) {
                    storeService.getstores($scope.storeID, $scope.companyID)
                        .then(function (response) {
                            $scope.storeObj = response[0];
                        });
                }
                else {
                    $scope.storeObj = $stateParams.storeObj;
                }
            }




            $scope.saveStore = function () {
                validateData($scope.storeObj);
                if (!$scope.isValidToSubmit) {
                    growlService.growl("Please fill all required fields marked (*).", "inverse");
                    return false;
                }

                $scope.storeObj.isValid = 1;

                var params = {
                    "store": $scope.storeObj
                };

                storeService.savestore(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Store Saved Successfully.", "success");
                            $window.history.back();
                        }
                    })
            }

            function validateData(storeObj) {
                if (storeObj.storeCode == null || storeObj.storeCode == "") {
                    $scope.isValidToSubmit = false;
                }
                else if (storeObj.storeInCharge == null || storeObj.storeInCharge == "") {
                    $scope.isValidToSubmit = false;
                }
                else if (storeObj.storeDescription == null || storeObj.storeDescription == "") {
                    $scope.isValidToSubmit = false;
                }
                else if (storeObj.storeDetails == null || storeObj.storeDetails == "") {
                    $scope.isValidToSubmit = false;
                }
                else if (storeObj.storeAddress == null || storeObj.storeAddress == "") {
                    $scope.isValidToSubmit = false;
                }
                else {
                    $scope.isValidToSubmit = true;
                }
            }

        }]);