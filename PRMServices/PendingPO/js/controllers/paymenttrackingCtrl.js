﻿prmApp
    .controller('paymenttrackingCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPOService", "poService",
        "PRMCustomFieldService", "fileReader", "$uibModal", "$filter", "$http", "poDomain","PRMUploadServices",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter, $http, poDomain, PRMUploadServices) {
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.customerType = userService.getUserType();


            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.pageChanged = function () {
            };
            /*PAGINATION CODE*/

            $scope.exportPOItemsToExcel = function (type) {
                PRMUploadServices.GetExcelTemplate(type);
            };

            $scope.showErrorPopup = false;
            $scope.rowErrors = [];

            $scope.uploadExcel = function (type, proceedFurther, fileName) {
                $scope.showErrorPopup = false;
                $scope.FILE_NAME = fileName ? fileName : $scope.FILE_NAME;
                var params = {
                    name: type,
                    compID: $scope.compId,
                    sessionID: $scope.sessionID,
                    tableName: 'SAP_PAYMENT_DETAILS',
                    attachment: $scope.attachment,
                    proceedFurther: proceedFurther,
                    fileName: $scope.FILE_NAME
                };
                PRMUploadServices.uploadTemplate(params)
                    .then(function (response) {
                        //if (response.errorMessage == '') {
                        //    location.reload();
                        //} else {
                        //    swal("Error!", response.errorMessage, "warning");
                        //    $("#paymentclientupload").val(null);
                        //    $scope.getPaymentInvoiceDetails();
                        //}
                        if (response && response.length > 0) {
                            if (response[0].IS_SUCCESS) {
                                angular.element('#errorPopUp').modal('hide');
                                swal("Status!", "Total Count : " + response[0].TOTAL_COUNT + " \n " + " Successfull Count : " + response[0].SUCCESS_COUNT + " \n " + " Failed Count : " + response[0].FAILED_COUNT, "warning");
                                $("#paymentclientupload").val(null);
                                $scope.getPaymentInvoiceDetails();
                            } else {
                                if (!response[0].IS_SUCCESS && proceedFurther === 1) {
                                    $scope.showErrorPopup = true;
                                    $scope.rowErrors = response;
                                    angular.element('#errorPopUp').modal('show');
                                    swal("Status!", "All the records in this sheet contains errors.. So cannot continue further ! Please validate the sheet again and upload.", "error");
                                    $("#paymentclientupload").val(null);
                                    angular.element('#errorPopUp').modal('hide');
                                } else {
                                    $scope.showErrorPopup = true;
                                    $scope.rowErrors = response;
                                    angular.element('#errorPopUp').modal('show');
                                    $("#paymentclientupload").val(null);
                                }
                            }
                        } else {
                            $scope.uploadExcel('PAYMENT',1);
                        }
                    });
            };


            //$scope.getFile1 = function (id, itemid, ext) {
            //    $scope.file = $("#" + id)[0].files[0];
            //    fileReader.readAsDataUrl($scope.file, $scope)
            //        .then(function (result) {

            //            if (id == "paymentclientupload") {
            //                var bytearray = new Uint8Array(result);
            //                $scope.attachment = $.makeArray(bytearray);
            //                $scope.uploadExcel('PAYMENT');
            //            }
            //        });
            //};

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);
                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];

                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            $scope.attachment = $.makeArray(bytearray);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;

                            if (id == "paymentclientupload") {
                                $scope.uploadExcel('PAYMENT', 0, fileUpload.fileName);
                                $scope.file = [];
                                $scope.file.name = '';
                            }
                        });
                });
            };

            $scope.gotoInvoicesList = function (invoiceId) {
                var url4 = $state.href('listInvoices');
                $window.open(url4, '_self');
            };

            $scope.goToPendingPO = function () {
                var url4 = $state.href('list-pendingPO');
                $window.open(url4, '_self');
            };

            $scope.paymentList = [];
            $scope.paymentListTemp = [];
            $scope.isCustomer = $scope.customerType == "CUSTOMER" ? true : false;


            $scope.getPaymentInvoiceDetails = function () {
                var params = {
                    sessionID: $scope.sessionID,
                    compId: +$scope.compId,
                    userId: $scope.isCustomer ? 0 : +$scope.userID
                };
                PRMPOService.getPaymentInvoiceDetails(params)
                    .then(function (response) {
                        if (response) {
                            $scope.paymentList = response;
                            $scope.paymentListTemp = response;
                            $scope.totalItems = $scope.paymentList.length;
                        }

                    });
            };
            $scope.getPaymentInvoiceDetails();

            $scope.convertDate = function (date) {
                return date ? userService.toLocalDate(date).split(' ')[0] : date;
            };


            $scope.searchTable = function (search) {
                if (search) {
                    $scope.paymentList = _.filter($scope.paymentListTemp, function (item) {
                        return (item.invoiceNumber.toUpperCase().indexOf(search.toUpperCase()) > -1);
                    });
                } else {
                    $scope.paymentList = $scope.paymentListTemp;
                }
                $scope.totalItems = $scope.paymentList.length;

            };

            $scope.exportErrorDetails = function (type) {
                $state.go('moduleErrorList', { 'moduleName': type});
            };

            $scope.getDate = function (value) {
                return userService.toLocalDate(value);
            }
            
        }]);