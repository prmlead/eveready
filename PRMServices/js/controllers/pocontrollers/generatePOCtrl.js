﻿prmApp
    .controller('generatePOCtrl', ["$scope", "$state", "$stateParams", "$window", "userService", "growlService", "fileReader", "$log", "poService", "auctionsService" , 
        function ($scope, $state, $stateParams, $window, userService, growlService, fileReader, $log, poService, auctionsService) {
            $scope.reqID = $stateParams.reqID;
            $scope.vendorID = $stateParams.vendorID;
            $scope.currentPOId = $stateParams.poID;
            $scope.po = {};
            $scope.poScheduleList = [];
            $scope.auctionVendors = [];
            $scope.valiPOItems = true;
            $scope.sessionID = userService.getUserToken();
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
            $scope.compID = userService.getUserCompanyId();
            $scope.coreItemList = [];
            $scope.qcsVendorAssignmentList = [];

            if ($scope.isCustomer) {
                $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            }

            $scope.deliveryAddress = '';
            $scope.purchaseID = '';
            $scope.indentID = '';
            $scope.requirement;
            $scope.scheduleObj = {
                PURCHASE_ORDER_ID: $scope.purchaseID,
                ITEM_ID: 0,
                QUANITTY: 0,
                DELIVERY_DATE: '',
                COMMENTS: ''
            };


            $scope.getData = function () {
                auctionsService.getrequirementdata({ "reqid": $scope.reqID, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.requirement = response;
                        $scope.requirement.auctionVendors.forEach(function (item, index) {
                            if ($scope.vendorID == item.vendorID) {
                                $scope.vendorID = item.vendorID;
                            }
                        });
                    });
            };

            $scope.getData();


                
            $scope.GetSeries = function (series, deptID) {
                $scope.purchaseID = 'PRM/PO';
            };


            if (!$scope.currentPOId) {
                $scope.GetSeries('', '');
            }

            $scope.updateForAllItems = function () {
                $scope.po.listPOItems.forEach(function (item, index) {
                    item.contactStartDate = $scope.contactStartDate;
                    item.contactEndDate = $scope.contactEndDate;
                    item.expectedDeliveryDate = $scope.expectedDeliveryDate;                    
                    item.deliveryAddress = $scope.deliveryAddress;
                    item.expectedStartDate = $scope.expectedStartDate;
                });
            };

            $scope.autoUpdateForAllItems = function () {

                $scope.po.listPOItems.forEach(function (item, index) {
                    item.purchaseID = $scope.purchaseID;
                    item.indentID = $scope.indentID;
                });
            };

            //listPOItems


            $scope.changeVendorsPrices = function (vendorID) {
                $state.go('po', { "reqID": $scope.reqID, "vendorID": vendorID, "poID": 0 });
            };

            $scope.taxCalculation = function (item) {
                if (item) {
                    if (item.vendorPOQuantity && item.vendorPOQuantity > item.vendorPOQuantityOld) {
                        if ((item.vendorPOQuantity - item.vendorPOQuantityOld) > item.remainingQuantity) {
                            item.vendorPOQuantity = item.vendorPOQuantityOld;
                        }
                    }
                }

                $scope.po.listPOItems.forEach(function (item, index) {

                    var tempRevUnitPrice = item.poPrice;
                    var tempCGst = item.cGst;
                    var tempSGst = item.sGst;
                    var tempIGst = item.iGst;

                    if (item.poPrice == undefined || item.poPrice <= 0) {
                        item.poPrice = 0;
                    };

                    item.poTotalPrice = item.poPrice * item.vendorPOQuantity;

                    if (item.cGst == undefined || item.cGst <= 0) {
                        item.cGst = 0;
                    };
                    if (item.sGst == undefined || item.sGst <= 0) {
                        item.sGst = 0;
                    };
                    if (item.iGst == undefined || item.iGst <= 0) {
                        item.iGst = 0;
                    };

                    item.poTotalPrice = item.poTotalPrice + ((item.poTotalPrice / 100) * (item.cGst + item.sGst + item.iGst));

                    item.poPrice = tempRevUnitPrice;
                    item.cGst = tempCGst;
                    item.sGst = tempSGst;
                    item.iGst = tempIGst;

                    if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                        $scope.gstValidation = true;
                    };
                });
            };

            $scope.GetVendorPoInfo = function () {
                poService.GetVendorPoInfo($scope.reqID, $scope.vendorID, $scope.currentPOId)
                    .then(function (response) {
                        $scope.po = response;
                        $scope.deliveryAddress = $scope.po.req.deliveryLocation;
                        $scope.po.common = false;
                        
                        $scope.po.listPOItems.forEach(function (item, index) {
                            item.isSelected = true;
                            if (item.isCoreProductCategory > 0 && item.poID > 0) {
                                $scope.coreItemList.push(item);
                            }

                            if ($scope.currentPOId > 0) {
                                $scope.indentID = item.indentID;
                                $scope.purchaseID = $scope.coreItemList[0].purchaseID;
                            } else {
                                $scope.indentID = $scope.po.req.prCode;
                            }

                            if ($scope.currentPOId == 0) {
                                //item.vendorPOQuantity = item.reqQuantity;
                                item.poPrice = item.vendorPrice;
                                item.deliveryAddress = $scope.deliveryAddress;
                            }

                            item.expectedDeliveryDate = new moment(item.expectedDeliveryDate).format("DD-MM-YYYY");
                            item.poPrice = item.vendorPrice;
                            if (item.expectedDeliveryDate.indexOf('-9999') > -1) {
                                item.expectedDeliveryDate = "";
                            }
                        });

                        if ($scope.purchaseID) {
                            $scope.getPOSchedule();
                        }

                        $scope.taxCalculation();

                        if (!$scope.currentPOId || $scope.currentPOId <= 0) {
                            var validItems = $scope.po.listPOItems.filter(function (poItem) {
                                return poItem.isCoreProductCategory > 0 && poItem.remainingQuantity > 0;
                            });

                            if (validItems && validItems.length <= 0) {
                                $scope.valiPOItems = false;
                            }
                        }

                        $scope.getVendorItemAssignments();
                    });
            };

            $scope.GetVendorPoInfo();

            this.UploadPO = 0;

            $scope.converToDate = function (date) {
                var fDate = new moment(date).format("DD-MM-YYYY");

                if (fDate.indexOf('-9999') > -1) {
                    fDate = "-";
                }

                return fDate;
            };


            $scope.postRequest = function (sendToVendor) {

                $scope.po.sessionID = userService.getUserToken();
                $scope.po.isPOToVendor = sendToVendor;
                $scope.po.listPOItems.forEach(function (item, index) {
                    var ts = moment(item.expectedDeliveryDate, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);

                    var contractStartTS = moment($scope.contactStartDate, "DD-MM-YYYY").valueOf();
                    var contractStartDate = new Date(moment(contractStartTS));
                    var contractStartMS = parseInt(contractStartDate.getTime() / 1000.0);

                    var contactEndDateTS = moment($scope.contactEndDate, "DD-MM-YYYY").valueOf();
                    var contractEndDate = new Date(moment(contactEndDateTS));
                    var contractEndMS = parseInt(contractEndDate.getTime() / 1000.0);

                    item.expectedDeliveryDate = "/Date(" + milliseconds + "000+0530)/";
                    item.contractStartTime = "/Date(" + contractStartMS + "000+0530)/";
                    item.contractEndTime = "/Date(" + contractEndMS + "000+0530)/";
                    item.isPOToVendor = sendToVendor;
                    item.COMP_ID = $scope.compID;
                    item.DEPT_ID = $scope.deptID;
                    item.QCS_VENDOR_ITEM_ID = item.qcsVendorItemId;
                });

                var params = {
                    'vendorpo': $scope.po
                };

                poService.SaveVendorPOInfo(params)
                    .then(function (response) {
                        if (response.errorMessage === '') {
                            swal({
                                title: "Done!",
                                text: "Purchase Orders Generated Successfully.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    //location.reload();
                                    $state.go('po-list', { "reqID": $stateParams.reqID, "vendorID": $stateParams.vendorID, "poID": 0 });
                                });
                        }
                    });
            };


            $scope.goToDispatchTrack = function (poID, dTID) {
                //$state.go("reqTechSupport", { "reqId": $scope.reqId }, { reload: true, newtab: true });

                var url = $state.href("dispatchtrack", { "poID": poID, "dTID": dTID });
                window.open(url, '_blank');
            };


            $scope.cancelPO = function () {
                $state.go("po-list", { "reqID": $stateParams.reqID, "vendorID": $stateParams.vendorID, "poID": 0 });
            };



            $scope.getFile1 = function (id, doctype, ext) {
                $scope.progress = 0;
                $scope.file = $("#" + id)[0].files[0];
                $scope.docType = doctype + "." + ext;
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        if (id == "poFile") {
                            $scope.po.poFile = { "fileName": '', 'fileStream': null };
                            var bytearray = new Uint8Array(result);
                            $scope.po.poFile.fileStream = $.makeArray(bytearray);
                            $scope.po.poFile.fileName = $scope.file.name;
                        }

                    });
            };

            $scope.isExists = false;

            $scope.CheckUniqueIfExists = function (param, idtype) {

                $scope.isExists = false;

                $scope.params = {
                    param: param,
                    idtype: idtype,
                    sessionID: userService.getUserToken()
                };

                poService.CheckUniqueIfExists($scope.params)
                    .then(function (response) {
                        $scope.isExists = response;
                    });
            };

            $scope.newSchedule = function () {
                $scope.scheduleObj = {
                    PURCHASE_ORDER_ID: $scope.purchaseID,
                    ITEM_ID: 0,
                    QUANITTY: 0,
                    DELIVERY_DATE: '',
                    COMMENTS: ''
                };
            };

            $scope.getPOSchedule = function () {
                $scope.params = {
                    ponumber: $scope.purchaseID
                };

                poService.getPOSchedule($scope.params)
                    .then(function (response) {
                        $scope.poScheduleList = response;
                        $scope.poScheduleList.forEach(function (schedule, index) {
                            schedule.DELIVERY_DATE = new moment(schedule.DELIVERY_DATE).format("DD-MM-YYYY");
                            var tempItem = $scope.po.listPOItems.filter(function (poItem) {
                                return poItem.itemID === schedule.ITEM_ID;
                            });
                            if (tempItem && tempItem.length > 0) {
                                schedule.productName = tempItem[0].productIDorName;
                            }
                        });
                    });
            };

            $scope.savePOSchedule = function () {
                if ($scope.scheduleDeliveryDate && $scope.scheduleObj.ITEM_ID && $scope.scheduleObj.QUANITTY) {
                    var ts = moment($scope.scheduleDeliveryDate, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);

                    $scope.scheduleObj.DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";
                    $scope.scheduleObj.sessionID = userService.getUserToken();
                    $scope.params = {
                        details: $scope.scheduleObj
                    };

                    poService.SavePOSchedule($scope.params)
                        .then(function (response) {
                            swal("Success!", "Successfully saved.", "success");
                            $scope.getPOSchedule();
                        });
                }
            };

            $scope.getVendorItemAssignments = function () {
                $scope.qcsVendorAssignmentList = [];
                var params =
                {
                    reqid: $stateParams.reqID,
                    userid: $stateParams.vendorID,
                    sessionid: userService.getUserToken()
                };

                auctionsService.getQCSVendorItemAssignments(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {

                                var poItemTemp = $scope.po.listPOItems.filter(function (poListItem) {
                                    return poListItem.itemID == item.ITEM_ID;
                                });

                                if (poItemTemp && poItemTemp.length > 0) {
                                    poItemTemp[0].isItemAssigned = true;
                                    poItemTemp[0].vendorPOQuantity = item.ASSIGN_QTY;
                                    poItemTemp[0].qcsVendorItemId = item.QCS_VENDOR_ITEM_ID;

                                    $scope.taxCalculation(poItemTemp[0]);
                                }
                            });

                            $scope.po.listPOItems = $scope.po.listPOItems.filter(function (poListItem) {
                                return poListItem && poListItem.isItemAssigned;
                            });
                            console.log($scope.po.listPOItems);
                        }
                    });
            };

        }]);