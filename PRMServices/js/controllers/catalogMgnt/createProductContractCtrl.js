﻿//(function () { 
prmApp.controller('createProductContractCtrl', ['$scope', '$state', '$window', '$stateParams', '$filter', 'auctionsService', 'catalogService', 'userService', 'growlService', 'fileReader','$timeout',
    function ($scope, $state, $window, $stateParams, $filter, auctionsService, catalogService, userService, growlService, fileReader, $timeout) {

        $scope.compId = userService.getUserCompanyId();
        $scope.userId = userService.getUserId();
        $scope.REQ_ID = $stateParams.REQ_ID;
        $scope.QCS_ID = $stateParams.QCS_ID;
        $scope.PC_ID = $stateParams.PC_ID;
        $scope.productId = $stateParams.productId;
        $scope.vendorItemDetails = $stateParams.vendorAssignedDetails;
        $scope.sessionid = userService.getUserToken();
        $scope.catalogCompId = userService.getUserCatalogCompanyId();
        $scope.stateName = $state.current.name;
        $scope.ContractVendorsTemp = [];
        $scope.ContractVendors = [];


        var date = new Date();
        var dateFormat = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
        $scope.maxDate = dateFormat;

        $scope.arr = [];


        $scope.qcsvendordetails = {
            startTime: '',
            endTime: ''
        }



        if ($scope.PC_ID > 0) {
            catalogService.GetProductContracts($scope.productId, 0)
                .then(function (response) {
                    if (response) {
                        $scope.contractManagement = []
                        $scope.contractManagement = response;
                        if ($scope.contractManagement && $scope.contractManagement.length > 0) {

                            var tempArr = $scope.contractManagement.filter(function (item) {
                                if (item.PC_ID == $scope.PC_ID) {
                                    item.startTime = userService.toLocalDate(item.startTime);
                                    item.endTime = userService.toLocalDate(item.endTime);
                                    return item;
                                }
                            });

                            if (tempArr && tempArr.length > 0) {
                                var obj =
                                {
                                    "QCS_VENDOR_CONTRACT_ITEMS": tempArr,
                                    "vendorName": tempArr[0].companyName,
                                    "VENDOR_ID": tempArr[0].U_ID,
                                    "showData": false,
                                    "showItemData": true
                                };

                                $scope.ContractVendors.push(obj);
                                $scope.qcsvendordetails =
                                {
                                    "startTime": tempArr[0].startTime,
                                    "endTime": tempArr[0].endTime
                                };

                                var contract = $scope.ContractVendors[0].QCS_VENDOR_CONTRACT_ITEMS[0];

                                $scope.editProductCotractInfo(contract);

                                $scope.showContractForm = true;
                            } else {
                                $scope.showContractForm = false;
                            }

                        }
                    }
                });
        }

        $scope.editProductCotractInfo = function (contract) {

            $scope.selectedProductContract = contract;
            $scope.productId = $scope.selectedProductContract.productID;
            $scope.selectedProductContract.isNew = false;
            $scope.showContractForm = true;
            $scope.selectedProductContract.VENDOR_SITE_CODE = (contract.SITE_CODE ? contract.SITE_CODE : contract.VENDOR_SITE_CODE);
            $scope.selectedProductContract.PLANT_CODE = (contract.SITE_CODE ? contract.SITE_CODE.split('_')[0] : contract.VENDOR_SITE_CODE.split('_')[0]);

            let vendor = {
                vendorId: contract.U_ID,
                selectedVendorCode: contract.selectedVendorCode,
                selectedSite: contract.SITE_CODE ? contract.SITE_CODE.split('_')[0] : contract.VENDOR_SITE_CODE.split('_')[0],
                selectedVendorSiteCode: contract.SITE_CODE ? contract.SITE_CODE : contract.VENDOR_SITE_CODE
            };

            $scope.selectedProductContract.vendor = vendor;
            $scope.getVendorCodes(vendor);

            if ($scope.productId && ($scope.stateName == 'costProductContract' || $scope.stateName == 'importProductContract')) {
                catalogService.GetProductContracts($scope.productId, 0)
                    .then(function (response) {
                        if (response) {
                            $scope.prodEditDetails = {
                                contractManagement: []
                            };
                            $scope.prodEditDetails.contractManagement = response;
                            if ($scope.prodEditDetails.contractManagement && $scope.prodEditDetails.contractManagement.length > 0) {
                                $scope.prodEditDetails.contractManagement.forEach(function (item) {
                                    item.startTime = moment(item.startTime).format("YYYY-MM-DD");
                                    item.endTime = moment(item.endTime).format("YYYY-MM-DD");
                                    if (item.ProductId == $scope.productId && item.vendorId == $scope.selectedProductContract.vendorId && item.endTime > $scope.maxDate) {
                                        if (item.number) {
                                            $scope.showContractForm = false;
                                            swal({
                                                title: "Contract is Existing with the Vendor" + '  ' + $scope.selectedProductContract.companyName,
                                                text: 'Do You want to Proceed to Add New Contract',
                                                type: "warning",
                                                showCancelButton: true,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "Yes",
                                                closeOnConfirm: true
                                            }, function (isConfirm) {
                                                if (isConfirm) {
                                                    $timeout(function () {
                                                        $scope.$apply(function () {
                                                            $scope.showContractForm = true;
                                                            $scope.selectedProductContract.startTime = '';
                                                            $scope.selectedProductContract.endTime = '';

                                                        })
                                                    }, 100);
                                                } else {
                                                    $timeout(function () {
                                                        $scope.$apply(function () {
                                                            $scope.showContractForm = false;
                                                            $scope.selectedProductContract.startTime = $scope.vendorItemDetails[0].START_DATE;;
                                                            $scope.selectedProductContract.endTime = $scope.vendorItemDetails[0].END_DATE;;

                                                        })
                                                    }, 100);
                                                }
                                            })
                                            //swal('Error!', 'Contract is Existing for the Selected Item', 'error')
                                            //return;
                                        }
                                    }
                                    else {
                                        $scope.showContractForm = true;
                                        $scope.selectedProductContract.startTime = '';
                                        $scope.selectedProductContract.endTime = '';
                                    }
                                });
                            }
                            else {
                                $scope.showContractForm = true;
                                $scope.selectedProductContract.startTime = '';
                                $scope.selectedProductContract.endTime = '';
                            }
                        }
                    });
            }

        };


        $scope.getVendorCodes = function (vendor) {
            if (!vendor.vendorCodeList || vendor.vendorCodeList.length <= 0) {
                let params = {
                    'uid': vendor.vendorId,
                    'compid': $scope.compId,
                    'siteCodes': vendor.selectedSite,
                    'sessionid': $scope.sessionid
                };

                auctionsService.getVendorCodes(params)
                    .then(function (response) {
                        vendor.vendorCodeList = response;

                        var filteredArray = vendor.vendorCodeList.filter(function (item) {
                            if (item.VENDOR_SITE_CODE == vendor.selectedVendorSiteCode) {
                                return item;
                            }
                        });
                        vendor.vendorCodeList = filteredArray;

                        if (vendor.vendorCodeList && vendor.vendorCodeList.length > 0 && !$scope.selectedProductContract.selectedVendorCode) {
                            vendor.selectedVendorCode = vendor.vendorCodeList[0].VENDOR_CODE;
                            $scope.selectedProductContract.selectedVendorCode = vendor.vendorCodeList[0].VENDOR_CODE;
                        }

                        $scope.selectedProductContract = {};
                    });
            } else {
                if (vendor.vendorCodeList && vendor.vendorCodeList.length > 0 && !$scope.selectedProductContract.selectedVendorCode) {
                    vendor.selectedVendorCode = vendor.selectedVendorCode ? vendor.selectedVendorCode : vendor.vendorCodeList[0].VENDOR_CODE;
                    $scope.selectedProductContract.selectedVendorCode = vendor.vendorCodeList[0].VENDOR_CODE;
                }
            }
        };


        $scope.getItemDetailsforContract = function () {
            var params = {
                'REQ_ID': $scope.REQ_ID,
                'QCS_ID': $scope.QCS_ID,
                'sessionid': $scope.sessionid
            }
            catalogService.getItemDetailsforContract(params)
                .then(function (response) {
                    if (response) {

                        $scope.ContractVendorsTemp = response

                        $scope.ContractVendorsTemp.forEach(function (item) {
                            let obj1 = {
                                "vendorName": item.VENDOR_NAME,
                                "VENDOR_ID": item.VENDOR_ID,
                                "QCS_VENDOR_CONTRACT_ITEMS": [],
                                "showData": true,
                                "showItemData": false
                            };
                            item.QCS_VENDOR_CONTRACT_ITEMS.forEach(function (item) {
                                let obj = {};
                                obj.number = '';
                                obj.companyName = item.VENDOR_COMPANY_NAME;
                                obj.vendorName = item.VENDOR_NAME;
                                obj.selectedVendorCode = item.VENDOR_CODE;
                                obj.value = item.PRICE;
                                obj.quantity = item.QUANTITY;
                                obj.allocationPercentage = 0;
                                obj.availedQuantity = 0;
                                obj.startTime = '';
                                obj.endTime = '';
                                obj.companyName = item.VENDOR_COMPANY_NAME;
                                obj.PAYMENT_TERMS = item.PAYMENT_TERMS;
                                obj.INCO_TERMS = item.INCO_TERMS;
                                obj.DELIVERY_TERMS = '';
                                obj.GENERAL_TERMS = '';
                                obj.DELIVERY_FROM = '';
                                obj.DELIVERY_AT = '';
                                obj.LEADTIME = '';
                                obj.WARRANTY = '';
                                obj.SPECIAL_INSTRUCTIONS = '';
                                obj.NET_PRICE = item.PRICE;
                                obj.DISCOUNT = 0;
                                obj.GST = item.GST;
                                obj.CUSTOM_DUTY = 0
                                obj.DISCOUNT_ELIGIBILITY = 0
                                obj.RECONCILIATION_TIMELINES = ''
                                obj.CURRENCY = item.CURRENCY;
                                obj.EXCHANGE_RATE = item.CURRENCY_FACTOR;
                                obj.PO_CURRENCY = item.CURRENCY;
                                obj.PLANT_CODE = item.VENDOR_SITE_CODE ? item.VENDOR_SITE_CODE.split('_')[0] : '',
                                obj.VENDOR_GST_ADDRESS = item.VENDOR_GST_ADDRESS;
                                obj.REQ_ID = item.REQ_ID;
                                obj.QCS_ID = item.QCS_ID;
                                obj.vendorId = item.VENDOR_ID;
                                obj.ProductId = item.PRODUCT_ID;
                                obj.VENDOR_CODE_SELECTED = true;
                                obj.USER = $scope.userId;
                                obj.VENDOR_SITE_CODE = item.VENDOR_SITE_CODE;
                                obj.contractStatus = 'Active';
                                obj.isDisabled = true;
                                obj.ProductName = item.ITEM_NAME;

                                obj1.QCS_VENDOR_CONTRACT_ITEMS.push(obj);

                            });
                            $scope.ContractVendors.push(obj1);

                        })
                        console.log(response);
                    }
                });
        };


        if ($scope.PC_ID <= 0) {
            $scope.getItemDetailsforContract();
        };



        $scope.updateDateForAllItems = function () {
            $scope.qcsvendordetails.startTimeError = '';
            $scope.qcsvendordetails.endTimeError = '';
            if (!$scope.qcsvendordetails.startTime) {
                $scope.qcsvendordetails.startTimeError = 'border : 2px solid red';
            }
            if (!$scope.qcsvendordetails.endTime) {
                $scope.qcsvendordetails.endTimeError = 'border : 2px solid red';
            }
            if ($scope.qcsvendordetails.startTimeError || $scope.qcsvendordetails.endTimeError) {
                swal('Error!', 'Please input the Dates.', 'error');
                return;
            }
            if ($scope.qcsvendordetails.startTime && $scope.qcsvendordetails.endTime) {
                let startDate = userService.toUTCTicks($scope.qcsvendordetails.startTime);
                let startDateMoment = moment(startDate);
                let endDate = userService.toUTCTicks($scope.qcsvendordetails.endTime);
                let endDateMoment = moment(endDate);
                if (endDateMoment < startDateMoment) {
                    swal('Error!', 'Start Date cannot be Greater than End Date.', 'error');
                    return;
                }
            }

            $scope.ContractVendors.forEach(function (vendorItem, vendorItemIndex) {
                if (vendorItem != null) {
                    vendorItem.QCS_VENDOR_CONTRACT_ITEMS.forEach(function (contractItem, contractItemIndex) {
                        contractItem.startTime = $scope.qcsvendordetails.startTime;
                        contractItem.endTime = $scope.qcsvendordetails.endTime;
                    });
                }
            });
        }





        $scope.SaveProductContractInfo = function () {
            $scope.errorMessage = '';
            $scope.selectedProductContract = {
                "sessionId" : $scope.sessionid
            }
            $scope.qcsvendordetails.startTimeError = '';
            $scope.qcsvendordetails.endTimeError = '';
            $scope.showContractSaveValidationError = '';
            if (!$scope.qcsvendordetails.startTime) {
                $scope.qcsvendordetails.startTimeError = 'border : 2px solid red';
            }
            if (!$scope.qcsvendordetails.endTime) {
                $scope.qcsvendordetails.endTimeError = 'border : 2px solid red';
            }
            if ($scope.qcsvendordetails.startTimeError || $scope.qcsvendordetails.endTimeError) {
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                //swal('Error!', 'Please input the Dates.', 'error');
                return;
            }
            if ($scope.ContractVendors && $scope.ContractVendors.length > 0) {
                let validateData = [];
                $scope.ContractVendors.forEach(function (item, itemIndex) {
                    if (item.QCS_VENDOR_CONTRACT_ITEMS != null && item.QCS_VENDOR_CONTRACT_ITEMS.length > 0) {
                        item.QCS_VENDOR_CONTRACT_ITEMS.forEach(function (contItem, contItemIndex) {
                            let obj =
                            {
                                "errorMessage": ""
                            };
                            if ((!contItem.value ||
                                !contItem.endTime ||
                                !contItem.startTime ||
                                !contItem.companyName ||
                                !contItem.selectedVendorCode ||
                                !contItem.PLANT_CODE)) {
                                obj.errorMessage = 'Please fill in required fields marked *';
                            }
                            if (+contItem.quantity <= 0 || +contItem.value <= 0) {
                                if (+contItem.value <= 0) {
                                    obj.errorMessage = 'Contract Price should be greater than zero.';
                                }
                                if (+contItem.quantity <= 0) {
                                    obj.errorMessage = 'Quantity should be greater than zero.';
                                }
                            }
                            if (contItem.startTime && contItem.endTime) {
                                let startDate = userService.toUTCTicks(contItem.startTime);
                                contItem.startDateMoment = moment(startDate);


                                let endDate = userService.toUTCTicks(contItem.endTime);
                                contItem.endDateMoment = moment(endDate);


                                if (contItem.endDateMoment < contItem.startDateMoment) {
                                    contItem.validationError = 'Start Date cannot be Greater than End Date.';
                                    obj.errorMessage = contItem.validationError;
                                }
                            }

                            validateData.push(obj);

                        });
                    }
                });

                        if (validateData[0].errorMessage) {
                            $scope.showContractSaveValidationError = validateData[0].errorMessage;
                            return;
                        }
                        $scope.vendorWiseContractArr = [];
                       $scope.ContractVendors.forEach(function (vendorItem, vendorItemIndex) {
                            var obj =
                            {
                                "VendorId": vendorItem.VENDOR_ID,
                                "vendorName": vendorItem.VENDOR_NAME,
                                "type": $scope.PC_ID > 0 ? '' : 'QCS',
                                "contractItems": []
                            };

                    if (vendorItem.QCS_VENDOR_CONTRACT_ITEMS != null && vendorItem.QCS_VENDOR_CONTRACT_ITEMS.length > 0) {
                        vendorItem.QCS_VENDOR_CONTRACT_ITEMS.forEach(function (contItem, contItemIndex) {
                                    var copiedContractObj = Object.assign({}, contItem);
                                    var ts = moment(copiedContractObj.startTime, "DD-MM-YYYY").valueOf();
                                    var m = moment(ts);
                                    var quotationDate = new Date(m);
                                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                                    copiedContractObj.startTimeTemp = "/Date(" + milliseconds + "000+0530)/";
                                    var ts = moment(copiedContractObj.endTime, "DD-MM-YYYY").valueOf();
                                    var m = moment(ts);
                                    var quotationDate = new Date(m);
                                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                                    copiedContractObj.endTimeTemp = "/Date(" + milliseconds + "000+0530)/";
                                    copiedContractObj.sessionId = $scope.sessionid;
                                    copiedContractObj.isValid = 1;
                            if (!$scope.PC_ID) {
                                copiedContractObj.ProductId = copiedContractObj.ProductId;
                                    }
                                    delete copiedContractObj.startTime;
                                    delete copiedContractObj.endTime;
                                    obj.contractItems.push(copiedContractObj);
                                });
                                $scope.vendorWiseContractArr.push(obj);
                            }
                        });
                       
            } else {
                return;
        };
        var params = {
            "contract": $scope.selectedProductContract,
            "contractMultiple": $scope.vendorWiseContractArr
        };
        catalogService.SaveProductContract(params)
                .then(function (response) {
                    if (response) {
                        growlService.growl("Saved Successfully.", "success");
                        $scope.selectedProductContract = {
                            isDisabled: false,
                            isNew: true
                        };
                        //$scope.getProductContracts();
                        $state.go("pendingcontracts");
                        console.log(response);
                    }
                });
        };



        $scope.CancelProductContractInfo = function () {
           
            if (($scope.stateName == 'costProductContract' || $scope.stateName == 'importProductContract')) {
                if ($scope.stateName == 'costProductContract') {
                    $state.go("cost-comparisions-qcs", { "reqID": $scope.REQ_ID, "qcsID": $scope.QCS_ID });
                }
                if ($scope.stateName == 'importProductContract') {
                    $state.go("import-qcs", { "reqID": $scope.REQ_ID, "qcsID": $scope.QCS_ID });
                }
            } else {
                $state.go("pendingcontracts");
            }
        }




        $scope.calculateNetPrice = function (contract) {
            if (!contract.value) {
                contract.value = 0;
            }
            if (!contract.DISCOUNT) {
                contract.DISCOUNT = 0;
            }
            contract.NET_PRICE = (+contract.value) * (1 - ((+contract.DISCOUNT) / 100));
        };
        $scope.calculateCustomDuty = function (currentContract) {
            $scope.showContractSaveValidationError = '';
            if (+currentContract.CUSTOM_DUTY > 100) {
                $scope.showContractSaveValidationError = 'Custom Duty % Cannot Be Greater Than 100%.';
                currentContract.CUSTOM_DUTY = 0;
                document.body.scrollTop = document.body.scrollHeight - document.body.clientHeight;
                document.documentElement.scrollTop = document.documentElement.scrollHeight - document.documentElement.clientHeight;
                return;
            }
        }
        $scope.calculateGST = function (currentContract) {
            $scope.showContractSaveValidationError = '';
            if (+currentContract.GST > 100) {
                $scope.showContractSaveValidationError = 'GST % Cannot Be Greater Than 100%.';
                currentContract.GST = 0;
                document.body.scrollTop = document.body.scrollHeight - document.body.clientHeight;
                document.documentElement.scrollTop = document.documentElement.scrollHeight - document.documentElement.clientHeight;
                return;
            }
        }
        $scope.calculateAllocation = function (currentContract) {
            $scope.showContractSaveValidationError = '';
            if (+currentContract.allocationPercentage > 100) {
                $scope.showContractSaveValidationError = 'Allocation Percentage Cannot Be Greater Than 100%.';
                currentContract.allocationPercentage = 0;
                document.body.scrollTop = document.body.scrollHeight - document.body.clientHeight;
                document.documentElement.scrollTop = document.documentElement.scrollHeight - document.documentElement.clientHeight;
                return;
            }
        };
        $scope.calculateDiscount = function (currentContract) {
            $scope.showContractSaveValidationError = '';
            if (+currentContract.DISCOUNT > 100) {
                $scope.showContractSaveValidationError = 'Discount Cannot Be Greater Than 100%.';
                currentContract.DISCOUNT = 0;
                document.body.scrollTop = document.body.scrollHeight - document.body.clientHeight;
                document.documentElement.scrollTop = document.documentElement.scrollHeight - document.documentElement.clientHeight;
                return;
            }
        }


        $scope.routeToQCS = function () {
            if ($scope.stateName == 'costProductContract') {
                var url = $state.href("cost-comparisions-qcs", { "reqID": $scope.REQ_ID, "qcsID": $scope.QCS_ID });
                window.open(url, '_blank');
            }
            if ($scope.stateName == 'importProductContract') {
                var url = $state.href("import-qcs", { "reqID": $scope.REQ_ID, "qcsID": $scope.QCS_ID });
                window.open(url, '_blank');
            }
        }





    }]);
