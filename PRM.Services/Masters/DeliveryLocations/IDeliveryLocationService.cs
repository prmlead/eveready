﻿using PRM.Core.Domain.Masters.DeliveryLocations;
using PRM.Core.Pagers;
using System.Collections.Generic;

namespace PRM.Services.Masters.DeliveryLocations
{
    public interface IDeliveryLocationService
    {
        void InsertDeliveryLocation(DeliveryLocation entity);

        void UpdateDeliveryLocation(DeliveryLocation entity);

        void DeleteDeliveryLocation(DeliveryLocation entity);

        DeliveryLocation GetDeliveryLocationById(int id);

        IList<DeliveryLocation> GetDeliveryLocationList(object filter);

        IPagedList<DeliveryLocation> GetDeliveryLocationList(Pager pager,int companyId);
    }
}