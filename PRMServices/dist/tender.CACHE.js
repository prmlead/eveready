﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('view-tender', {
                    url: '/view-tender/:Id',
                    templateUrl: 'tender/views/tender-list-item.html',
                    params: {
                        detailsObj: null
                    }
                }).state('save-tender', {
                    url: '/save-tender/:Id',
                    templateUrl: 'tender/views/save-tender.html',
                    params: {
                        reqObj: null,
                        prDetail: null
                    }
                }).state('view-tenders', {
                    url: '/view-tenders',
                    templateUrl: 'tender/views/view-tenders.html',
                    params: {
                        reqObj: null,
                        prDetail: null
                    }
                });
        }]);prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('createTenderCtrl', ["$state", "$stateParams", "$scope", "auctionsService", "userService", "$http", "$window", "domain",
        "fileReader", "growlService", "$log", "$filter", "ngDialog", "techevalService", "fwdauctionsService", "catalogService", "prmCompanyService",
        "workflowService", "PRMPRServices", "PRMCustomFieldService",
        function ($state, $stateParams, $scope, auctionsService, userService, $http, $window, domain,
            fileReader, growlService, $log, $filter, ngDialog, techevalService, fwdauctionsService, catalogService, prmCompanyService,
            workflowService, PRMPRServices, PRMCustomFieldService) {
            $scope.showCatalogQuotationTemplate = true;
            $scope.userID = userService.getUserId();
            $scope.cloneId = ($stateParams.reqObj || {}).cloneId;
            $scope.isClone = ($stateParams.reqObj && $stateParams.reqObj.cloneId);
            $scope.prmFieldMappingTemplates = [];
            $scope.selectedTemplate = 'DEFAULT';
            $scope.prmFieldMappingDetails = {};
            $scope.widgetStates = {
                MIN: 0,
                MAX: 1,
                PIN: -1
            };

            if ($stateParams.Id) {
                $scope.stateParamsReqID = $stateParams.Id;
                $scope.postRequestLoding = true;
                $scope.showCatalogQuotationTemplate = false;
                $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
                $scope.desigID = userService.getSelectedUserDesigID();
                $scope.deptTypeID = userService.getSelectedUserDepartmentDesignation().deptTypeID;

            } else {
                $scope.stateParamsReqID = 0;
                $scope.postRequestLoding = false;
                $scope.showCatalogQuotationTemplate = true;
                $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
                $scope.desigID = userService.getSelectedUserDesigID();
                $scope.deptTypeID = userService.getSelectedUserDepartmentDesignation().deptTypeID;

            };
            $scope.itemPreviousPrice = {};
            $scope.itemPreviousPrice.lastPrice = -1;
            $scope.itemLastPrice = {};
            $scope.itemLastPrice.lastPrice = -1;
            $scope.bestPriceEnable = 0;
            $scope.bestLastPriceEnable = 0;
            $scope.companyItemUnits = [];
            $scope.customFieldList = [];
            $scope.selectedcustomFieldList = [];
            $scope.otherRequirementItems = {
                INSTALLATION_CHARGES: true,
                PACKAGING: true,
                FREIGHT_CHARGES: true
            };
            
            $scope.SelectedVendorsCount = 0;

            
            $scope.isTechEval = false;
            $scope.isForwardBidding = false;
            $scope.allCompanyVendors = [];
            $scope.selectedProducts = [];

            var curDate = new Date();
            var today = moment();
            var tomorrow = today.add('days', 1);
            //var dateObj = $('.datetimepicker').datetimepicker({
            //    format: 'DD/MM/YYYY',
            //    useCurrent: false,
            //    minDate: tomorrow,
            //    keepOpen: false
            //});
            $scope.subcategories = [];
            $scope.sub = {
                selectedSubcategories: [],
            }
            $scope.selectedCurrency = {};
            $scope.currencies = [];
            $scope.questionnaireList = [];

            //$scope.postRequestLoding = false;
            $scope.selectedSubcategories = [];

            $scope.companyCatalogueList = [];

            $scope.selectVendorShow = true;
            $scope.isEdit = false;
            //Input Slider
            this.nouisliderValue = 4;
            this.nouisliderFrom = 25;
            this.nouisliderTo = 80;
            this.nouisliderRed = 35;
            this.nouisliderBlue = 90;
            this.nouisliderCyan = 20;
            this.nouisliderAmber = 60;
            this.nouisliderGreen = 75;

            //Color Picker
            this.color = '#03A9F4';
            this.color2 = '#8BC34A';
            this.color3 = '#F44336';
            this.color4 = '#FFC107';

            $scope.Vendors = [];
            $scope.VendorsTemp = [];
            $scope.categories = [];
            $scope.selectedA = [];
            $scope.selectedB = [];
            $scope.showCategoryDropdown = false;
            $scope.checkVendorPhoneUniqueResult = false;
            $scope.checkVendorEmailUniqueResult = false;
            $scope.checkVendorPanUniqueResult = false;
            $scope.checkVendorTinUniqueResult = false;
            $scope.checkVendorStnUniqueResult = false;
            $scope.showFreeCreditsMsg = false;
            $scope.showNoFreeCreditsMsg = false;
            $scope.formRequest = {
                isTabular: true,
                isRFP: false,
                auctionVendors: [],
                listRequirementItems: [],
                isQuotationPriceLimit: false,
                quotationPriceLimit: 0,
                quotationFreezTime: '',
                deleteQuotations: false,
                expStartTimeName: '',
                isDiscountQuotation: 0,
                isRevUnitDiscountEnable: 0,
                multipleAttachments: [],
                contractStartTime: '',
                contractEndTime: '',
                isContract: false,
                isRFQ: 1,
                biddingType: 'TENDER'
            };
            $scope.Vendors.city = "";
            $scope.Vendors.quotationUrl = "";
            $scope.vendorsLoaded = false;
            $scope.requirementAttachment = [];
            $scope.selectedProjectId = 0;
            $scope.selectedProject = {};
            $scope.branchProjects = [
                { PROJECT_ID: "RAW MATERIALS", PROJECT_CODE: "RAW MATERIALS"},
                { PROJECT_ID: "CAPEX", PROJECT_CODE: "CAPEX" },
                { PROJECT_ID: "SOLAR", PROJECT_CODE: "SOLAR" }
            ];

            $scope.changeProject = function () {
                console.log($scope.selectedProjectId);
                var filterdProjects = _.filter($scope.branchProjects, function (o) {
                    return o.PROJECT_ID == $scope.selectedProjectId;
                });
                if (filterdProjects && filterdProjects.length > 0) {
                    $scope.selectedProject = filterdProjects[0];
                }
            }

            $scope.sessionid = userService.getUserToken();

            $scope.selectedQuestionnaire == {}

            $scope.formRequest.indentID = 0;

            $scope.cijList = [];

            $scope.indentList = [];


            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.workflowObj = {};
            $scope.formRequest.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'QUOTATION';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/


            
            $scope.compID = userService.getUserCompanyId();
            //prmCompanyService.getBranchProjects({ compid: $scope.compID, branchid: userService.getUserSelectedBranch(), projectid: 0, sessionid: userService.getUserToken() })
            //    .then(function (response) {
            //        $scope.branchProjects = response;
            //    });



            
            auctionsService.GetCompanyConfiguration($scope.compID, "ITEM_UNITS", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyItemUnits = unitResponse;
                });

            $scope.getPreviousItemPrice = function (itemDetails, index) {
                $scope.itemPreviousPrice = {};
                $scope.itemPreviousPrice.lastPrice = -1;
                $scope.bestPriceEnable = index;
                $log.info(itemDetails);
                itemDetails.sessionID = userService.getUserToken();
                itemDetails.compID = userService.getUserCompanyId();
                auctionsService.getPreviousItemPrice(itemDetails)
                .then(function (response) {
                    if (response && response.errorMessage == '') {
                        $scope.itemPreviousPrice.lastPrice = Number(response.initialPrice);
                        $scope.itemPreviousPrice.lastPriceDate = userService.toLocalDate(response.currentTime);
                        $scope.itemPreviousPrice.lastPriceVendor = response.companyName;
                        $log.info($scope.itemPreviousPrice);
                    }
                });
            };

            $scope.showTable = true;

            $scope.dispalyLastprices = function (val) {
                $scope.showTable = val;
            }

            $scope.GetLastPrice = function (itemDetails, index) {
                $scope.itemLastPrice = {};
                $scope.itemLastPrice.lastPrice = -1;
                $scope.bestLastPriceEnable = index;
                $log.info(itemDetails);
                itemDetails.sessionID = userService.getUserToken();
                itemDetails.compID = userService.getUserCompanyId();
                auctionsService.GetLastPrice(itemDetails)
                    .then(function (response) {
                        $scope.itemLastPrice = response;

                        $scope.itemLastPrice.forEach(function (item, index) {

                            item.currentTime = userService.toLocalDate(item.currentTime);
                        })
                    });
            };




            $scope.budgetValidate = function () {
                if ($scope.formRequest.budget != "" && (isNaN($scope.formRequest.budget) || $scope.formRequest.budget.indexOf('.') > -1)) {
                    $scope.postRequestLoding = false;
                    swal({
                        title: "Error!",
                        text: "Please enter valid budget, budget should be greater than 1,00,000.",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {

                        });

                    $scope.formRequest.budget = "";
                }
            };

            $scope.clickToOpen = function () {
                ngDialog.open({ template: 'login/termsAddNewReq.html', width: 1000, height: 500 });
            };

            $scope.changeCategory = function () {

                $scope.selectedSubCategoriesList = [];
                $scope.formRequest.auctionVendors = [];
                $scope.loadSubCategories();
                //$scope.getvendors();
            }

            $scope.getCreditCount = function () {
                userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.userDetails = response;
                        //$scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                        //$scope.selectedCurrency = $scope.selectedCurrency[0];
                        if (response.creditsLeft) {
                            $scope.showFreeCreditsMsg = true;
                        } else {
                            $scope.showNoFreeCreditsMsg = true;
                        }
                    });
            }



            $scope.VendorsList = [];
            $scope.VendorsTempList1 = [];

            $scope.getvendors = function (catObj) {
                $scope.ShowDuplicateVendorsNames = [];
                $scope.VendorsList = [];
                $scope.VendorsTempList1 = [];
                $scope.vendorsLoaded = false;
                var category = '';

                category = catObj.catCode;

                var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                $http({
                    method: 'POST',
                    url: domain + 'getvendors',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.VendorsList = response.data;
                            $scope.VendorsTempList = $scope.VendorsList;



                            $scope.Vendors.forEach(function (item, index) {
                                $scope.VendorsTempList.forEach(function (item1, index1) {
                                    if (item.vendorID == item1.vendorID) {
                                        $scope.ShowDuplicateVendorsNames.push(item1);
                                        $scope.VendorsList.splice(index1, 1);
                                    }
                                })
                            });

                            if ($scope.formRequest.auctionVendors.length > 0) {
                                $scope.formRequest.auctionVendors.forEach(function (item1, index1) {
                                    $scope.VendorsTempList.forEach(function (item2, index2) {
                                        if (item1.vendorID == item2.vendorID) {
                                            $scope.ShowDuplicateVendorsNames.push(item2);
                                            $scope.VendorsList.splice(index2, 1);
                                        }
                                    });
                                });
                            }


                            $scope.VendorsTempList1 = $scope.VendorsList;

                            if ($scope.ShowDuplicateVendorsNames.length > 0) {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                                //swal({
                                //    title: "Cancelled",
                                //    type: "error",
                                //    confirmButtonText: "Ok",
                                //    allowOutsideClick: true,
                                //    customClass: 'swal-wide'
                                //});
                                //$(".sweet-alert h2").html("oops...! Some vendors are already retrieved in products they are <h3 style='color:red'><div style='max-height: 400px;overflow-y: auto;overflow-x:scroll'>" + $scope.ShowDuplicateVendorsNames + "</div></h3> so vendors are not retrieved in the categories even though they are assigned.");
                            } else {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                            }

                        } else {
                            $scope.VendorsList = [];
                        }

                        if ($scope.searchCategoryVendorstring != '') {
                            $scope.searchingCategoryVendors($scope.searchCategoryVendorstring);
                        } else {
                            $scope.searchingCategoryVendors('');
                        }


                    } else {

                    }
                }, function (result) {
                });
            };

            $scope.getReqQuestionnaire = function () {


            }

            $scope.getQuestionnaireList = function () {
                techevalService.getquestionnairelist(0)
                    .then(function (response) {
                        $scope.questionnaireList = $filter('filter')(response, { reqID: 0 });
                        if ($stateParams.Id && $stateParams.Id > 0) {
                            techevalService.getreqquestionnaire($stateParams.Id, 1)
                                .then(function (response) {
                                    $scope.selectedQuestionnaire = response;

                                    if ($scope.selectedQuestionnaire && $scope.selectedQuestionnaire.evalID > 0) {
                                        $scope.isTechEval = true;
                                    }

                                    $scope.questionnaireList.push($scope.selectedQuestionnaire);
                                })
                        }
                    })
            };



            // $scope.getQuestionnaireList();

            /*$scope.getvendorsbysubcat = function () {
                $scope.vendorsLoaded = false;
                var category = [];
    
                category.push($scope.sub.selectedSubcategories);
                //$scope.formRequest.category = category;
                if ($scope.formRequest.category != undefined) {
                    var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId() };
                    $http({
                        method: 'POST',
                        url: domain + 'getvendorsbycatnsubcat',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data) {
                            if (response.data.length > 0) {
                                $scope.Vendors = response.data;
                                $scope.vendorsLoaded = true;
                                for (var j in $scope.formRequest.auctionVendors) {
                                    for (var i in $scope.Vendors) {
                                        if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                            $scope.Vendors.splice(i, 1);
                                        }
                                    }
                                }
                            }
                            //$scope.formRequest.auctionVendors =[];
                        } else {
                        }
                    }, function (result) {
                    });
                }
    
            };*/

            $scope.isRequirementPosted = 0;


            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));
                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    } else {
                    }
                }, function (result) {
                });
                
            };


            $scope.getCurrencies = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getkeyvaluepairs?parameter=CURRENCY',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.currencies = response.data;
                            if (!$scope.formRequest.currency) {
                                $scope.formRequest.currency = 'INR';
                            }

                            $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: $scope.formRequest.currency });
                            $scope.selectedCurrency = $scope.selectedCurrency[0];
                            
                            $scope.getCreditCount();
                        }
                    } else {
                    }
                }, function (result) {
                });
            };

            


            $scope.getData = function () {
                
                // $scope.getCategories();

                if ($stateParams.Id) {
                    var id = $stateParams.Id;
                    $scope.isEdit = true;

                    auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), "userid": userService.getUserId() })
                        .then(function (response) {
                            $scope.selectedProjectId = response.projectId;
                            var category = response.category[0];
                            response.category = category;
                            response.taxes = parseInt(response.taxes);
                            //response.paymentTerms = parseInt(response.paymentTerms);
                            $scope.formRequest = response;
                            $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: $scope.formRequest.currency });
                            $scope.selectedCurrency = $scope.selectedCurrency[0];

                            //$scope.getPRNumber($scope.formRequest.PR_ID);
                            $scope.itemSNo = $scope.formRequest.itemSNoCount;

                            $scope.formRequest.checkBoxEmail = true;
                            $scope.formRequest.checkBoxSms = true;
                            $scope.loadSubCategories();

                            $scope.isRequirementPosted = $scope.formRequest.auctionVendors.length;



                            if (!$scope.formRequest.multipleAttachments) {
                                $scope.formRequest.multipleAttachments = [];
                            }
                            $scope.formRequest.attFile = response.attachmentName;
                            if ($scope.formRequest.attFile != '' && $scope.formRequest.attFile != null && $scope.formRequest.attFile != undefined) {


                                var attchArray = $scope.formRequest.attFile.split(',');

                                attchArray.forEach(function (att, index) {

                                    var fileUpload = {
                                        fileStream: [],
                                        fileName: '',
                                        fileID: att
                                    };

                                    $scope.formRequest.multipleAttachments.push(fileUpload);
                                })

                            }


                            $scope.selectedSubcategories = response.subcategories.split(",");
                            for (i = 0; i < $scope.selectedSubcategories.length; i++) {
                                for (j = 0; j < $scope.subcategories.length; j++) {
                                    if ($scope.selectedSubcategories[i] == $scope.subcategories[j].subcategory) {
                                        $scope.subcategories[j].ticked = true;
                                    }
                                }
                            }
                            //$scope.getvendors();
                            $scope.selectSubcat();
                            $scope.formRequest.attFile = response.attachmentName;
                            $scope.formRequest.quotationFreezTime = userService.toLocalDate($scope.formRequest.quotationFreezTime);
                            //$scope.formRequest.quotationFreezTimeNew = userService.toLocalDate($scope.formRequest.quotationFreezTime);
                            //$scope.formRequest.urgency.push(urgency);

                            $scope.formRequest.expStartTime = userService.toLocalDate($scope.formRequest.expStartTime);

                            $scope.SelectedVendors = $scope.formRequest.auctionVendors;

                            $scope.SelectedVendorsCount = $scope.formRequest.auctionVendors.length;
                            

                            $scope.formRequest.listRequirementItems.forEach(function (item, itemIndex) {
                                item.isNonCoreProductCategory = !item.isCoreProductCategory; //Try to clean up keep only one property
                                if (item.productQuotationTemplateJson && item.productQuotationTemplateJson != '' && item.productQuotationTemplateJson != null && item.productQuotationTemplateJson != undefined) {
                                    item.productQuotationTemplateArray = JSON.parse(item.productQuotationTemplateJson);
                                    item.productQuotationTemplate = JSON.parse(item.productQuotationTemplateJson);
                                }
                                else {
                                    item.productQuotationTemplateArray = [];
                                }

                                if (item.productQuotationTemplate && item.productQuotationTemplate.length > 0) {
                                    item.productQuotationTemplate.pop();
                                }

                                $scope.GetProductQuotationTemplate(item.catalogueItemID, itemIndex);
                            });


                            $scope.postRequestLoding = false;
                            $scope.selectRequirementPRS();
                        });
                }

            };

            $scope.showSimilarNegotiationsButton = function (value, searchstring) {
                $scope.showSimilarNegotiations = value;
                if (!value) {
                    $scope.CompanyLeads = {};
                }
                if (value) {
                    if (searchstring.length < 3) {
                        $scope.CompanyLeads = {};
                    }
                    if (searchstring.length > 2) {
                        $scope.searchstring = searchstring;
                        $scope.GetCompanyLeads(searchstring);
                    }
                }
                return $scope.showSimilarNegotiations;
            }

            $scope.showSimilarNegotiations = false;

            $scope.CompanyLeads = {};

            $scope.searchstring = '';

            $scope.GetCompanyLeads = function (searchstring) {
                if (searchstring.length < 3) {
                    $scope.CompanyLeads = {};
                }
                if ($scope.showSimilarNegotiations && searchstring.length > 2) {
                    $scope.searchstring = searchstring;
                    var params = { "userid": userService.getUserId(), "searchstring": $scope.searchstring, "searchtype": 'Title', "sessionid": userService.getUserToken() };
                    auctionsService.GetCompanyLeads(params)
                        .then(function (response) {
                            $scope.CompanyLeads = response;
                            $scope.CompanyLeads.forEach(function (item, index) {
                                item.postedOn = userService.toLocalDate(item.postedOn);
                            })
                        });
                }
            }


            $scope.changeScheduledAuctionsLimit = function () {
                $scope.scheduledLimit = 8;
                $scope.getMiniItems();
            }

            $scope.loadSubCategories = function () {
                $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.formRequest.category });
                /*$scope.subcategories = _.map($scope.subcategories, 'subcategory');*/
            }

            $scope.selectedSubCategoriesList = [];

            $scope.selectSubcat = function (subcat) {

                $scope.selectedSubCategoriesList = [];

                if (!$scope.isEdit) {
                    $scope.formRequest.auctionVendors = [];
                }
                $scope.vendorsLoaded = false;
                var category = [];
                var count = 0;
                var succategory = "";
                $scope.sub.selectedSubcategories = $filter('filter')($scope.subcategories, { ticked: true });
                selectedcount = $scope.sub.selectedSubcategories.length;
                if (selectedcount > 0) {
                    succategory = _.map($scope.sub.selectedSubcategories, 'id');
                    category.push(succategory);

                    $scope.selectedSubCategoriesList = succategory;

                    //$scope.formRequest.category = category;
                    if ($scope.formRequest.category != undefined) {
                        var params = { 'Categories': succategory, 'sessionID': userService.getUserToken(), 'count': selectedcount, 'uID': userService.getUserId(), evalID: $scope.selectedQuestionnaire ? $scope.selectedQuestionnaire.evalID : 0 };
                        $http({
                            method: 'POST',
                            url: domain + 'getvendorsbycatnsubcat',
                            encodeURI: true,
                            headers: { 'Content-Type': 'application/json' },
                            data: params
                        }).then(function (response) {
                            if (response && response.data) {
                                if (response.data.length > 0) {
                                    $scope.Vendors = response.data;
                                    $scope.vendorsLoaded = true;
                                    for (var j in $scope.formRequest.auctionVendors) {
                                        for (var i in $scope.Vendors) {
                                            if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                                $scope.Vendors.splice(i, 1);
                                            }
                                        }
                                    }

                                    $scope.VendorsTemp = $scope.Vendors;
                                    $scope.searchVendors('');

                                }
                                //$scope.formRequest.auctionVendors =[];
                            } else {
                            }
                        }, function (result) {
                        });
                    }
                } else {
                    //$scope.getvendors();
                }

            }

            $scope.getData();

            $scope.checkVendorUniqueResult = function (idtype, inputvalue) {


                if (idtype == "PHONE") {
                    $scope.checkVendorPhoneUniqueResult = false;
                } else if (idtype == "EMAIL") {
                    $scope.checkVendorEmailUniqueResult = false;
                }
                else if (idtype == "PAN") {
                    $scope.checkVendorPanUniqueResult = false;
                }
                else if (idtype == "TIN") {
                    $scope.checkVendorTinUniqueResult = false;
                }
                else if (idtype == "STN") {
                    $scope.checkVendorStnUniqueResult = false;
                }

                if (inputvalue == "" || inputvalue == undefined) {
                    return false;
                }

                userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                    if (idtype == "PHONE") {
                        $scope.checkVendorPhoneUniqueResult = !response;
                    } else if (idtype == "EMAIL") {
                        $scope.checkVendorEmailUniqueResult = !response;
                    }
                    else if (idtype == "PAN") {
                        $scope.checkVendorPanUniqueResult = !response;
                    }
                    else if (idtype == "TIN") {
                        $scope.checkVendorTinUniqueResult = !response;
                    }
                    else if (idtype == "STN") {
                        $scope.checkVendorStnUniqueResult = !response;
                    }
                });
            };
            $scope.selectForA = function (item) {
                var index = $scope.selectedA.indexOf(item);
                if (index > -1) {
                    $scope.selectedA.splice(index, 1);
                } else {
                    $scope.selectedA.splice($scope.selectedA.length, 0, item);
                }
                for (i = 0; i < $scope.selectedA.length; i++) {
                    $scope.formRequest.auctionVendors.push($scope.selectedA[i]);
                    $scope.Vendors.splice($scope.Vendors.indexOf($scope.selectedA[i]), 1);
                    $scope.VendorsTemp.splice($scope.VendorsTemp.indexOf($scope.selectedA[i]), 1);
                }
                $scope.reset();
            }

            $scope.selectForB = function (item) {
                var index = $scope.selectedB.indexOf(item);
                if (index > -1) {
                    $scope.selectedB.splice(index, 1);
                } else {
                    $scope.selectedB.splice($scope.selectedA.length, 0, item);
                }
                for (i = 0; i < $scope.selectedB.length; i++) {
                    $scope.Vendors.push($scope.selectedB[i]);
                    $scope.VendorsTemp.push($scope.selectedB[i]);
                    $scope.formRequest.auctionVendors.splice($scope.formRequest.auctionVendors.indexOf($scope.selectedB[i]), 1);
                }
                $scope.reset();
            }

            $scope.AtoB = function () {

            }

            $scope.BtoA = function () {

            }

            $scope.reset = function () {
                $scope.selectedA = [];
                $scope.selectedB = [];
            }

            // $scope.getFile = function () {
            //     $scope.progress = 0;
            //     fileReader.readAsDataUrl($scope.file, $scope)
            //     .then(function(result) {
            //         $scope.formRequest.attachment = result;
            //     });
            // };

            $scope.multipleAttachments = [];

            $scope.getFile = function () {
                $scope.progress = 0;

                //$scope.file = $("#attachement")[0].files[0];
                $scope.multipleAttachments = $("#attachement")[0].files;

                $scope.multipleAttachments = Object.values($scope.multipleAttachments)


                $scope.multipleAttachments.forEach(function (item, index) {

                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };

                            var bytearray = new Uint8Array(result);

                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;

                            if (!$scope.formRequest.multipleAttachments) {
                                $scope.formRequest.multipleAttachments = [];
                            }

                            $scope.formRequest.multipleAttachments.push(fileUpload);

                        });

                })

            };


            $scope.newVendor = {};
            $scope.Attaachmentparams = {};
            $scope.deleteAttachment = function (reqid) {
                $scope.Attaachmentparams = {
                    reqID: reqid,
                    userID: userService.getUserId()
                }
                auctionsService.deleteAttachment($scope.Attaachmentparams)
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            growlService.growl("Attachment deleted Successfully", "inverse");
                            $scope.getData();
                        }
                    });
            }

            $scope.newVendor.panno = "";
            $scope.newVendor.vatNum = "";
            $scope.newVendor.serviceTaxNo = "";

            $scope.addVendor = function () {
                $scope.emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                $scope.mobileRegx = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
                $scope.panregx = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
                //$scope.emailRegx = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
                var addVendorValidationStatus = false;
                $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                if ($scope.newVendor.companyName == "" || $scope.newVendor.companyName === undefined) {
                    $scope.companyvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.firstName == "" || $scope.newVendor.firstName === undefined) {
                    $scope.firstvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.lastName == "" || $scope.newVendor.lastName === undefined) {
                    $scope.lastvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.contactNum == "" || $scope.newVendor.contactNum === undefined || isNaN($scope.newVendor.contactNum)) {
                    $scope.contactvalidation = true;
                    addVendorValidationStatus = true;
                }
                else if ($scope.newVendor.contactNum.length != 10) {
                    $scope.contactvalidationlength = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.email == "" || $scope.newVendor.email === undefined) {
                    $scope.emailvalidation = true;
                    addVendorValidationStatus = true;
                }
                else if (!$scope.emailRegx.test($scope.newVendor.email)) {
                    $scope.emailregxvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.vendorcurrency == "" || $scope.newVendor.vendorcurrency === undefined) {
                    $scope.vendorcurrencyvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.panno != "" && $scope.newVendor.panno != undefined && !$scope.panregx.test($scope.newVendor.panno)) {
                    $scope.panregxvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.newVendor.vatNum != "" && $scope.newVendor.vatNum != undefined && $scope.newVendor.vatNum.length != 11) {
                    $scope.tinvalidation = true;
                    addVendorValidationStatus = true;
                }

                if ($scope.newVendor.serviceTaxNo != "" && $scope.newVendor.serviceTaxNo != undefined && $scope.newVendor.serviceTaxNo.length != 15) {
                    $scope.stnvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.formRequest.category == "" || $scope.formRequest.category === undefined) {
                    $scope.categoryvalidation = true;
                    addVendorValidationStatus = true;
                }
                if ($scope.checkVendorEmailUniqueResult || $scope.checkVendorEmailUniqueResult) {
                    addVendorValidationStatus = true;
                }
                if (addVendorValidationStatus) {
                    return false;
                }
                var vendCAtegories = [];
                $scope.newVendor.category = $scope.formRequest.category;
                vendCAtegories.push($scope.newVendor.category);
                var params = {
                    "register": {
                        "firstName": $scope.newVendor.firstName,
                        "lastName": $scope.newVendor.lastName,
                        "email": $scope.newVendor.email,
                        "phoneNum": $scope.newVendor.contactNum,
                        "username": $scope.newVendor.contactNum,
                        "password": $scope.newVendor.contactNum,
                        "companyName": $scope.newVendor.companyName ? $scope.newVendor.companyName : "",
                        "isOTPVerified": 0,
                        "category": $scope.newVendor.category,
                        "userType": "VENDOR",
                        "panNumber": ("panno" in $scope.newVendor) ? $scope.newVendor.panno : "",
                        "stnNumber": ("serviceTaxNo" in $scope.newVendor) ? $scope.newVendor.serviceTaxNo : "",
                        "vatNumber": ("vatNum" in $scope.newVendor) ? $scope.newVendor.vatNum : "",
                        "referringUserID": userService.getUserId(),
                        "knownSince": ("knownSince" in $scope.newVendor) ? $scope.newVendor.knownSince : "",
                        "errorMessage": "",
                        "sessionID": "",
                        "userID": 0,
                        "department": "",
                        "currency": $scope.newVendor.vendorcurrency.key,
                        "altPhoneNum": $scope.newVendor.altPhoneNum,
                        "altEmail": $scope.newVendor.altEmail,
                        "subcategories": $scope.selectedSubCategoriesList
                    }
                };
                $http({
                    method: 'POST',
                    url: domain + 'register',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if ((response && response.data && response.data.errorMessage == "") || response.data.errorMessage == 'User assigned to Company.') {
                        $scope.formRequest.auctionVendors.push({ vendorName: $scope.newVendor.firstName + " " + $scope.newVendor.lastName, companyName: $scope.newVendor.companyName, vendorID: response.data.objectID });
                        $scope.newVendor = null;
                        $scope.newVendor = {};
                        //$scope.addVendorForm.$setPristine();
                        $scope.addVendorShow = false;
                        $scope.selectVendorShow = true;
                        $scope.newVendor = {};
                        $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                        growlService.growl("Vendor Added Successfully.", 'inverse');
                    } else if (response && response.data && response.data.errorMessage) {
                        growlService.growl(response.data.errorMessage, 'inverse');
                    } else {
                        growlService.growl('Unexpected Error Occurred', 'inverse');
                    }
                });
            }

            //$scope.checkboxModel = {
            //    value1: true
            //};

            $scope.formRequest.checkBoxEmail = true;
            $scope.formRequest.checkBoxSms = true;
            //$scope.postRequestLoding = false;
            $scope.formRequest.urgency = 'High (Will be Closed in 2 Days)';
            $scope.formRequest.deliveryLocation = '';
            $scope.formRequest.paymentTerms = '';
            $scope.formRequest.isSubmit = 0;


            $scope.titleValidation = $scope.attachmentNameValidation = false;
            $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
            $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = false;

            $scope.postRequest = function (isSubmit, pageNo, navigateToView, stage) {

                $scope.titleValidation = $scope.attachmentNameValidation = false;
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = false;

                $scope.postRequestLoding = true;
                $scope.formRequest.isSubmit = isSubmit;

                if (isSubmit == 1 || pageNo == 1) {
                    if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                        $scope.titleValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if (isSubmit == 1) {
                        if ($scope.formRequest.isTabular) {
                            $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                                if (!item.attachmentName || item.attachmentName == '') {
                                    $scope.attachmentNameValidation = true;
                                    return false;
                                }
                            })

                        }
                        if (!$scope.formRequest.isTabular) {
                            if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                                $scope.descriptionValidation = true;
                                $scope.postRequestLoding = false;
                                return false;
                            }
                        }
                    }
                }

                if (isSubmit == 1) {
                    if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                        $scope.deliveryTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                        $scope.deliveryLocationValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                        $scope.paymentTermsValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    
                }

                if (isSubmit == 1) {
                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    
                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= '') && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.isQuotationPriceLimit == false) {
                        $scope.formRequest.quotationPriceLimit = 0;
                    }
                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }

                if (!$scope.postRequestLoding) {
                    return false;
                }


                if (pageNo != 4) {
                    $scope.textMessage = "Save as Draft.";
                }

                if ($scope.formRequest.checkBoxEmail == true && $scope.formRequest.checkBoxSms == true && pageNo == 4) {
                    $scope.textMessage = "This will send an email and sms invite to all the vendors selected above.";
                }
                else if ($scope.formRequest.checkBoxEmail == true && pageNo == 4) {
                    $scope.textMessage = "This will send an email invite to all the vendors selected above.";
                }
                else if ($scope.formRequest.checkBoxSms == true && pageNo == 4) {
                    $scope.textMessage = "This will send an SMS invite to all the vendors selected above.";
                }
                else if (pageNo == 4) {
                    $scope.textMessage = "This will not send an SMS or EMAIL the vendors selected above.";
                }

                if ($scope.stateParamsReqID && !$scope.formRequest.deleteQuotations && pageNo == 4 && !$scope.isClone) {
                    $scope.textMessage = "Please select \"Request New Quotations\" to vendors if you modify any details in Requirement item specifications / Quantity. Click \"Cancel\" to select the option.";
                }

                $scope.formRequest.currency = $scope.selectedCurrency.value;
                $scope.formRequest.timeZoneID = 190;
                $scope.postRequestLoding = false;
                swal({
                    title: "Are you sure?",
                    text: $scope.textMessage,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    $scope.postRequestLoding = true;
                    var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.quotationFreezTime = "/Date(" + milliseconds + "000++530)/";
                    // this post request
                    


                    var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.expStartTime = "/Date(" + milliseconds + "000+0530)/";

                    $scope.formRequest.requirementID = $stateParams.Id ? $stateParams.Id : -1;
                    $scope.formRequest.customerID = userService.getUserId();
                    $scope.formRequest.customerFirstname = userService.getFirstname();
                    $scope.formRequest.customerLastname = userService.getLastname();
                    $scope.formRequest.isClosed = "NOTSTARTED";
                    $scope.formRequest.endTime = "";
                    $scope.formRequest.sessionID = userService.getUserToken();
                    $scope.formRequest.subcategories = "";
                    $scope.formRequest.budget = 100000;
                    $scope.formRequest.custCompID = userService.getUserCompanyId();
                    $scope.formRequest.customerCompanyName = userService.getUserCompanyId();

                    //for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                    //    $scope.formRequest.subcategories += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    //}
                    var category = [];
                    //category.push($scope.formRequest.category);
                    $scope.formRequest.category = category;

                    $scope.formRequest.listRequirementItems.forEach(function (item, itemIndexs) {
                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                subItem.dateCreated = "/Date(1561000200000+0530)/";
                                subItem.dateModified = "/Date(1561000200000+0530)/";
                            })
                        }
                    });

                    if (!$scope.formRequest.isForwardBidding) {
                        $scope.formRequest.cloneID = 0;
                        if ($stateParams.reqObj && $stateParams.reqObj.cloneId && $stateParams.reqObj.cloneId !== "") {
                            $scope.formRequest.cloneID = $stateParams.reqObj.cloneId;
                            $scope.formRequest.auctionVendors = _.filter($scope.formRequest.auctionVendors, function (x) { return x.companyName !== 'PRICE_CAP'; });
                        }

                        if ($scope.PRList && $scope.PRList.length > 0) {
                            var selectedPRs = _.filter($scope.PRList, function (x) { return x.isSelected; });
                            var selectedPRList = [];
                            if (selectedPRs && selectedPRs.length > 0) {
                                selectedPRs.forEach(function (selectedPR, itemIndexs) {
                                    selectedPRList.push(selectedPR.PR_ID);
                                });

                                $scope.formRequest.PR_ID = selectedPRList.join(',');
                            }                            
                        }

                        auctionsService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {
                                    if ($scope.selectedProject) {
                                        $scope.selectedProject.sessionID = userService.getUserToken();
                                        $scope.selectedProject.requirement = {};
                                        $scope.selectedProject.requirement.requirementID = response.objectID;
                                        var param = { item: $scope.selectedProject };
                                        //prmCompanyService.SaveProjectRequirement(param).then(function (projecgtMappingResponse) {
                                        //    console.log("saving project mapping");
                                        //    console.log(projecgtMappingResponse);
                                        //    console.log("END saving project mapping");
                                        //});
                                    }
                                    
                                    $scope.SaveReqDepartments(response.objectID);
                                    $scope.saveRequirementCustomFields(response.objectID);
                                    //$scope.SaveReqDeptDesig(response.objectID);

                                    //$scope.DeleteRequirementTerms();
                                    //$scope.SaveRequirementTerms(response.objectID);

                                    if ($scope.selectedQuestionnaire != null && $scope.selectedQuestionnaire != undefined && $scope.selectedQuestionnaire.evalID != undefined) {
                                        techevalService.getquestionnaire($scope.selectedQuestionnaire.evalID, 1)
                                            .then(function (response1) {
                                                $scope.selectedQuestionnaire = response1;

                                                if ($scope.isTechEval) {
                                                    $scope.selectedQuestionnaire.reqID = response.objectID;
                                                }
                                                else {
                                                    $scope.selectedQuestionnaire.reqID = 0;
                                                }
                                                $scope.selectedQuestionnaire.sessionID = userService.getUserToken();
                                                var params = {
                                                    questionnaire: $scope.selectedQuestionnaire
                                                }
                                                techevalService.assignquestionnaire(params)
                                                    .then(function (response) {
                                                    })
                                            })
                                    }
                                    if (stage) {
                                        $state.go('save-tender', { Id: response.objectID });
                                    }
                                    swal({
                                        title: "Done!",
                                        text: "Requirement Saved Successfully",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            //$scope.postRequestLoding = false;
                                            //$state.go('view-requirement');

                                            if (navigateToView && stage == undefined) {
                                                $state.go('view-tender', { 'Id': response.objectID });
                                            } else {
                                                if ($scope.stateParamsReqID > 0 && stage == undefined) {
                                                    location.reload();
                                                }
                                                else {
                                                    if (stage == undefined) {
                                                        //$state.go('form.addnewrequirement', { 'Id': response.objectID });
                                                        $state.go('save-tender', { Id: response.objectID });
                                                    }
                                                }
                                            }
                                        });
                                }
                            });
                    } else {
                        fwdauctionsService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {

                                    //$scope.SaveReqDepartments(response.objectID);
                                    $scope.SaveReqDeptDesig(response.objectID);

                                    swal({
                                        title: "Done!",
                                        text: "Requirement Created Successfully",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            //$scope.postRequestLoding = false;
                                            //$state.go('view-requirement');
                                            $state.go('fwd-view-req', { 'Id': response.objectID });
                                        });
                                }
                            });
                    }
                });
                //$scope.postRequestLoding = false;
            };


            $scope.ItemFile = '';
            $scope.itemSNo = 1;
            $scope.ItemFileName = '';

            $scope.itemnumber = $scope.formRequest.listRequirementItems.length;

            $scope.requirementItems =
                {
                    productSNo: $scope.itemSNo++,
                    ItemID: 0,
                    productIDorName: '',
                    productNo: '',
                    productDescription: '',
                    productQuantity: 0,
                    productBrand: '',
                    othersBrands: '',
                    isDeleted: 0,
                    itemAttachment: '',
                    attachmentName: '',
                    itemMinReduction: 0,
                    splitenabled: 0,
                    fromrange: 0,
                    torange: 0,
                    requiredQuantity: 0,
                    productCode: ''
                }

            $scope.formRequest.listRequirementItems.push($scope.requirementItems);
            $scope.AddItem = function () {
                $window.scrollBy(0, 50);
                $scope.itemnumber = $scope.formRequest.listRequirementItems.length;
                $scope.requirementItems =
                    {
                        productSNo: $scope.itemSNo++,
                        ItemID: 0,
                        productIDorName: '',
                        productNo: '',
                        productDescription: '',
                        productQuantity: 0,
                        productBrand: '',
                        othersBrands: '',
                        isDeleted: 0,
                        itemAttachment: '',
                        attachmentName: '',
                        itemMinReduction: 0,
                        splitenabled: 0,
                        fromrange: 0,
                        torange: 0,
                        requiredQuantity: 0,
                        productCode: ''
                    }

                $scope.formRequest.listRequirementItems.push($scope.requirementItems);

                //Widget Management
                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                    if (item.productSNo !== $scope.requirementItems.productSNo && item.widgetState !== -1 && item.productIDorName !== '') { //&& item.productCode !== ''
                        item.widgetState = 0;
                    }
                });
                //^Widget Management
            };

            $scope.handleWindowState = function (product, state) {
                // if (product.productCode !== '') { //item.productIDorName !== ''
                if (product.productIDorName !== '') { //item.productIDorName !== ''
                    product.widgetState = state;
                } else {
                    swal("Error!", "Item details cannot be empty", "error");
                }
            }

            $scope.AddOtherRequirementItem = function (nonCoreProduct) {
                var item = {};
                if (nonCoreProduct) {
                    var output = $scope.productsList.filter(function (product) {
                        return (String(product.prodCode).toUpperCase().includes(nonCoreProduct.prodCode.toUpperCase()) == true);
                    });
                    if (output) {
                        nonCoreProduct.doHide = true;
                        item = output[0];
                    }
                }

                if (item) {
                    var index = 0;
                    if (!$scope.formRequest.listRequirementItems[$scope.formRequest.listRequirementItems.length - 1].productCode) {
                        $scope.AddItem();
                        index = $scope.formRequest.listRequirementItems.length - 1;

                    } else {
                        $scope.AddItem();
                        index = $scope.formRequest.listRequirementItems.length - 1;
                    }

                    item.isNonCoreProductCategory = 1;
                    $scope.fillTextbox(item, index);
                }
            };

            $scope.deleteItem = function (product) {
                //$scope.formRequest.listRequirementItems.splice(SNo, 1);
                
                if (!$scope.isEdit) {
                    if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 1) {
                        $scope.formRequest.listRequirementItems = _.filter($scope.formRequest.listRequirementItems, function (x) { return x.productSNo !== product.productSNo; });
                    };
                } else {
                    product.isDeleted = 1;
                }



                $scope.nonCoreproductsList.forEach(function (item, index) {
                    if(item.prodNo === product.productNo) {
                        item.doHide = false;
                    }
                });
                
                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length === 1 
                    && $scope.formRequest.listRequirementItems[0].isNonCoreProductCategory === 1) {
                    location.reload();
                }
            };



            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        if (id == "itemsAttachment") {
                            if (ext != "xlsx") {
                                swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
                                return;
                            }
                            var bytearray = new Uint8Array(result);
                            $scope.formRequest.itemsAttachment = $.makeArray(bytearray);
                            if (!$scope.isEdit) {
                                $scope.formRequest.listRequirementItems = [];
                            }

                            $scope.postRequest(0, 1, false);
                            //$scope.formRequest.itemsAttachmentName = $scope.file.name;
                        }

                        if (id != "itemsAttachment") {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var index = _.indexOf($scope.formRequest.listRequirementItems, _.find($scope.formRequest.listRequirementItems, function (o) { return o.productSNo == id; }));
                            var obj = $scope.formRequest.listRequirementItems[index];
                            obj.itemAttachment = arrayByte;
                            obj.attachmentName = ItemFileName;
                            $scope.formRequest.listRequirementItems.splice(index, 1, obj);
                        }
                    });
            }

            $scope.pageNo = 1;

            $scope.nextpage = function (pageNo) {
                $scope.tableValidation = false;
                $scope.tableValidationMsg = '';
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = departmentsValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.remindersTimeIntervalValidation = $scope.questionnaireValidation = false;

                $scope.titleValidation = $scope.attachmentNameValidation = false;
                $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
                $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = $scope.expNegotiationTimeValidation = false;

                if (pageNo == 1 || pageNo == 4) {

                    if ($scope.reqDepartments == null || $scope.reqDepartments == undefined || $scope.reqDepartments.length == 0) {
                        $scope.GetReqDepartments();
                    }
                    
                    if ($scope.currencies == null || $scope.currencies == undefined || $scope.currencies.length == 0) {
                        //$scope.getCurrencies();
                    }

                    if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                        $scope.titleValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    if ($scope.formRequest.isTabular) {
                        $scope.selectedProducts = [];
                        $scope.formRequest.listRequirementItems.forEach(function (item, index) {

                            if ($scope.tableValidation || item.isDeleted) {
                                return false;
                            }

                            var sno = parseInt(index) + 1;

                            if (!$scope.formRequest.isRFP) {
                                if (item.productCode == null || item.productCode == '') {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please enter product Code for item: ' + sno;
                                    return;
                                }
                            }
                            
                            if (item.productIDorName == null || item.productIDorName == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter product Code/Name for item: ' + sno;
                                return;
                            }
                            else {
                                $scope.selectedProducts.push(item.catalogueItemID);
                                var productQuotationTemplateFiltered = _.filter(item.productQuotationTemplate, function (o) {
                                    return o.IS_VALID == 1;
                                });
                                if (productQuotationTemplateFiltered != null && productQuotationTemplateFiltered != undefined && productQuotationTemplateFiltered.length > 0) {

                                    

                                    item.productQuotationTemplateJson = JSON.stringify(productQuotationTemplateFiltered);
                                } else {
                                    item.productQuotationTemplateJson = '';
                                }


                            }
                            //if (item.productNo == null || item.productNo == '') {
                            //    $scope.tableValidation = true;
                            //}
                            //if (item.productDescription == null || item.productDescription == '') {
                            //    $scope.tableValidation = true;
                            //}
                            if (item.productQuantity <= 0 || item.productQuantity == undefined || item.productQuantity == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter product Quantity for item: ' + sno;
                                return;
                            }
                            if (item.productQuantityIn == null || item.productQuantityIn == '') {
                                $scope.tableValidation = true;
                                $scope.tableValidationMsg = 'Please enter product Units for item: ' + sno;
                                return;
                            }
                            if ($scope.formRequest.isSplitEnabled == true) {
                                if (item.fromrange == null || item.fromrange == '' || item.fromrange <= 0) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please enter From Range for item: ' + sno;
                                    return;
                                }
                                if (item.torange == null || item.torange == '' || item.torange <= 0) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please enter To Range for item: ' + sno;
                                    return;
                                }

                                if (parseFloat(item.fromrange) > parseFloat(item.torange)) {
                                    $scope.tableValidation = true;
                                    $scope.tableValidationMsg = 'Please make sure that To Range is higher than From Range for item: ' + sno;
                                    return;
                                }

                                //if (item.requiredQuantity == null || item.requiredQuantity == '' || item.requiredQuantity <= 0) {
                                //    $scope.tableValidation = true;
                                //    $scope.tableValidationMsg = 'Please enter Required Qty for item: ' + sno;
                                //    return;
                                //}


                            }
                        });


                        if ($scope.tableValidation) {
                            return false;
                        }

                    }
                    if (!$scope.formRequest.isTabular) {
                        if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                            $scope.descriptionValidation = true;
                            $scope.postRequestLoding = false;
                            return false;
                        }
                    }
                     

                    var phn = '';
                    if ($scope.userObj && $scope.userObj.phoneNum)
                    {
                        phn = $scope.userObj.phoneNum;
                        $scope.formRequest.phoneNum = phn;
                        $scope.formRequest.contactDetails = phn;
                    }
                    else {
                        $scope.formRequest.phoneNum = '';
                        $scope.formRequest.contactDetails = '';
                    }

                                       

                }

                if (pageNo == 2 || pageNo == 4) {

                    

                    if ($scope.questionnaireList == null || $scope.questionnaireList == undefined || $scope.questionnaireList.length == 0) {
                        $scope.getQuestionnaireList();
                    }

                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        var d = new Date();
                        d.setHours(18, 00, 00);
                        d = new moment(d).format("DD-MM-YYYY HH:mm");
                        $scope.formRequest.quotationFreezTime = d;
                        
                    }

                    
                    if ($scope.formRequest.expStartTime == null || $scope.formRequest.expStartTime == '') {
                        var dt = new Date();
                        var tomorrowNoon = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() + 1, 12, 0, 0);
                        tomorrowNoon = new moment(tomorrowNoon).format("DD-MM-YYYY HH:mm");
                        $scope.formRequest.expStartTime = tomorrowNoon;

                    }

                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.formRequest.noOfQuotationReminders = 3;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.formRequest.remindersTimeInterval = 2;
                    }
                    if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                        $scope.deliveryTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                        $scope.deliveryLocationValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                        $scope.paymentTermsValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    

                    if ($scope.reqDepartments.length > 0) {

                        $scope.departmentsValidation = true;

                        $scope.reqDepartments.forEach(function (item, index) {
                            if (item.isValid == true || $scope.noDepartments == true)
                                $scope.departmentsValidation = false;
                        });

                        if ($scope.departmentsValidation == true) {
                            return false;
                        };
                    }

                }

                if (pageNo == 3 || pageNo == 4) {

                    //if ($scope.categories == null || $scope.categories == undefined || $scope.categories.length == 0) {
                    //    $scope.getCategories();
                    //}
                    $scope.getproductvendors();

                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    

                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= 0) && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }

                    var ts = moment($scope.formRequest.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var quotationFreezTime = new Date(m);

                    var ts = moment($scope.formRequest.expStartTime, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var expStartTime = new Date(m);

                    auctionsService.getdate()
                        .then(function (GetDateResponse) {
                            //var CurrentDate = moment(new Date(parseInt(GetDateResponse.substr(6))));

                            var CurrentDateToLocal = userService.toLocalDate(GetDateResponse);

                            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                            var m = moment(ts);
                            var deliveryDate = new Date(m);
                            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                            var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));

                            if (quotationFreezTime < CurrentDate) {
                                $scope.quotationFreezTimeValidation = true;
                                $scope.postRequestLoding = false;
                                return false;
                            }
                            //else if (quotationFreezTime >= expStartTime) {
                            //        $scope.expNegotiationTimeValidation = true;
                            //        $scope.postRequestLoding = false;
                            //        return false;
                            //    }
                            $scope.pageNo = $scope.pageNo + 1;
                            //  console.log("page NO:::::::::::" + $scope.pageNo);

                        });

                }

                if (pageNo == 4) {
                    $scope.formRequest.subcategoriesView = '';
                    for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        $scope.formRequest.subcategoriesView += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    }
                    $scope.pageNo = $scope.pageNo + 1;
                }

                if (pageNo != 3) {
                    $scope.pageNo = $scope.pageNo + 1;
                }

                // return $scope.pageNo;
                // location.reload();
            }

            $scope.previouspage = function () {
                $scope.pageNo = $scope.pageNo - 1;
                // location.reload();
                //return $scope.pageNo;
            }

            $scope.gotopage = function (pageNo) {
                $scope.pageNo = pageNo;

                
                return $scope.pageNo;
            }


            $scope.reqDepartments = [];
            $scope.userDepartments = [];

            $scope.GetReqDepartments = function () {
                $scope.reqDepartments = [];
                auctionsService.GetReqDepartments(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.reqDepartments = response;

                            $scope.noDepartments = true;

                            $scope.reqDepartments.forEach(function (item, index) {
                                if (item.isValid == true) {
                                    $scope.noDepartments = false;
                                }
                            });

                        }
                    });
            }

            // $scope.GetReqDepartments();


            $scope.SaveReqDepartments = function (reqID) {

                $scope.reqDepartments.forEach(function (item, index) {
                    item.reqID = reqID;
                    item.userID = userService.getUserId();
                })

                var params = {
                    "listReqDepartments": $scope.reqDepartments,
                    sessionID: userService.getUserToken()
                };
                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                               
                            }
                        });
                } else {
                    auctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                               
                            }
                        });
                }


            };

            $scope.noDepartments = false;


            $scope.noDepartmentsFunction = function (value) {

                $scope.departmentsValidation = false;

                if (value == 'NA') {
                    $scope.reqDepartments.forEach(function (item, index) {
                        item.isValid = false;
                    });
                }
                else {
                    $scope.reqDepartments.forEach(function (item, index) {
                        if (item.isValid == true)
                            $scope.noDepartments = false;
                    });
                }
            };


            $scope.reqDeptDesig = [];

            $scope.GetReqDeptDesig = function () {
                $scope.reqDeptDesig = [];
                auctionsService.GetReqDeptDesig(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.reqDeptDesig = response;
                            $scope.noDepartments = true;
                            $scope.reqDeptDesig.forEach(function (item, index) {
                                if (item.isValid == true) {
                                    $scope.noDepartments = false;
                                }
                            });
                        }
                    });
            };

            // $scope.GetReqDeptDesig();


            $scope.SaveReqDeptDesig = function (reqID) {

                $scope.reqDeptDesig.forEach(function (item, index) {
                    item.reqID = reqID;
                    item.userID = userService.getUserId();
                })

                var params = {
                    "listReqDepartments": $scope.reqDeptDesig,
                    sessionID: userService.getUserToken()
                };

                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDeptDesig(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                } else {
                    auctionsService.SaveReqDeptDesig(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                }


            };

            $scope.AssignVendorToCompany = function (vendorPhone, vendorEmail) {

                if ($scope.newVendor.altPhoneNum == undefined) {
                    $scope.newVendor.altPhoneNum = '';
                }
                if ($scope.newVendor.altEmail == undefined) {
                    $scope.newVendor.altEmail = '';
                }

                var params = {
                    userID: userService.getUserId(),
                    vendorPhone: vendorPhone,
                    vendorEmail: vendorEmail,
                    sessionID: userService.getUserToken(),
                    category: $scope.formRequest.category,
                    subCategory: $scope.selectedSubCategoriesList,
                    altPhoneNum: $scope.newVendor.altPhoneNum,
                    altEmail: $scope.newVendor.altEmail
                };

                auctionsService.AssignVendorToCompany(params)
                    .then(function (response) {

                        $scope.vendor = response;

                        if ($scope.vendor.errorMessage == '' || $scope.vendor.errorMessage == 'VENDOR ADDED SUCCESSFULLY') {
                            $scope.formRequest.auctionVendors.push({ vendorName: $scope.vendor.firstName + ' ' + $scope.vendor.lastName, companyName: $scope.vendor.email, vendorID: $scope.vendor.userID });
                            $scope.newVendor = {};
                            $scope.checkVendorPhoneUniqueResult = false;
                            $scope.checkVendorEmailUniqueResult = false;
                            $scope.checkVendorPanUniqueResult = false;
                            $scope.checkVendorTinUniqueResult = false;
                            $scope.checkVendorStnUniqueResult = false;
                            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                            growlService.growl('Vendor Added Successfully', "success");
                        }
                        else {
                            $scope.newVendor = {};
                            $scope.checkVendorPhoneUniqueResult = false;
                            $scope.checkVendorEmailUniqueResult = false;
                            $scope.checkVendorPanUniqueResult = false;
                            $scope.checkVendorTinUniqueResult = false;
                            $scope.checkVendorStnUniqueResult = false;
                            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                            growlService.growl($scope.vendor.errorMessage, "inverse");
                        }
                    });
            };



            $scope.searchvendorstring = '';

            $scope.searchVendors = function (value) {
                $scope.Vendors = _.filter($scope.VendorsTemp, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                // $scope.vendorsLoaded = true;
                // $scope.Vendors = _.filter($scope.allCompanyVendors, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                // var temp = $scope.formRequest.auctionVendors;
                // $scope.formRequest.auctionVendors.forEach(function (item, index) {
                // $scope.Vendors = _.filter($scope.Vendors, function (vendor) { return vendor.vendorID !== item.vendorID; });
                // });
            }


            $scope.exportItemsToExcel = function () {
                var mystyle = {
                    sheetid: 'RequirementDetails',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                alasql('SELECT itemID as [ItemID], productIDorName as [ProductName], productNo as [ProductNumber], productDescription as [Description], productQuantity as [Quantity], productQuantityIn as [Units], productBrand as PreferredBrand, othersBrands as OtherBrands INTO XLSX(?,{headers:true,sheetid: "RequirementDetails", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["RequirementDetails.xlsx", $scope.formRequest.listRequirementItems]);
            }


            $scope.cancelClick = function () {
                $window.history.back();
            }




            $scope.paymentRadio = false;
            $scope.paymentlist = [];

            $scope.addpaymentvalue = function () {
                var listpaymet =
                    {
                        reqTermsID: 0,
                        reqID: $scope.stateParamsReqID,
                        userID: userService.getUserId(),
                        reqTermsDays: 0,
                        reqTermsPercent: 0,
                        reqTermsType: 'PAYMENT',
                        paymentType: '+',
                        isRevised: 0
                    };
                $scope.paymentlist.push(listpaymet);
            };

            $scope.delpaymentvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }

                $scope.paymentlist.splice(val, 1);
            };

            $scope.resetpayment = function () {
                $scope.paymentlist = [];
            };




            $scope.deliveryRadio = false;
            $scope.deliveryList = [];

            $scope.adddeliveryvalue = function () {
                var deliveryObj =
                    {
                        reqTermsID: 0,
                        reqID: $scope.stateParamsReqID,
                        userID: userService.getUserId(),
                        reqTermsDays: 0,
                        reqTermsPercent: 0,
                        reqTermsType: 'DELIVERY',
                        isRevised: 0
                    };
                $scope.deliveryList.push(deliveryObj);
            };

            $scope.deldeliveryvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }
                $scope.deliveryList.splice(val, 1);
            };

            $scope.resetdelivery = function () {
                $scope.deliveryList = [];
            };










            $scope.listRequirementTerms = [];

            $scope.SaveRequirementTerms = function (reqID) {

                $scope.listRequirementTerms = [];

                $scope.deliveryList.forEach(function (item, index) {
                    item.reqID = reqID;
                    $scope.listRequirementTerms.push(item);
                });

                $scope.paymentlist.forEach(function (item, index) {

                    item.reqID = reqID;

                    if (item.paymentType == '-') {
                        item.reqTermsDays = -(item.reqTermsDays);
                    }

                    $scope.listRequirementTerms.push(item);
                });

                var params = {
                    "listRequirementTerms": $scope.listRequirementTerms,
                    sessionID: userService.getUserToken()
                };
                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                } else {
                    auctionsService.SaveRequirementTerms(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                }


            };




            $scope.GetRequirementTerms = function () {
                auctionsService.GetRequirementTerms(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.listRequirementTerms = response;

                            $scope.listRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }

                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });



                        }
                    });
            }

           // $scope.GetRequirementTerms();




            $scope.listTerms = [];

            $scope.DeleteRequirementTerms = function () {

                var params = {
                    "listTerms": $scope.listTerms,
                    sessionID: userService.getUserToken()
                };

                if ($scope.formRequest.isForwardBidding) {
                    fwdauctionsService.SaveReqDepartments(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                } else {
                    auctionsService.DeleteRequirementTerms(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                }
            };




            $scope.paymentTypeFunction = function (type) {
                if (type == 'ADVANCE') {

                }
                if (type == 'POST') {

                }
            }







            $scope.GetCIJList = function () {
                auctionsService.GetCIJList($scope.compID, $scope.sessionid)
                    .then(function (response) {
                        $scope.cijList = response;

                        $scope.cijList.forEach(function (item, index) {
                            var cijObj = $.parseJSON(item.cij);
                            item.cij = cijObj;
                        });


                    })
            }


           // $scope.GetCIJList();



            $scope.GetIndentList = function () {
                auctionsService.GetIndentList(userService.getUserCompanyId(), userService.getUserToken())
                    .then(function (response) {
                        $scope.indentList = response;
                    })
            }

           // $scope.GetIndentList();


            $scope.changeIndent = function (val) {
            };

            $scope.formRequest.category = '';

            $scope.goToStage = function (stage, currentStage) {
                $scope.postRequestLoding = true;
                $scope.tableValidation = $scope.validationFailed = false;
                if (stage == 2 && currentStage == 1) {

                }
                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                    if (item.productQuantity <= 0 || item.productQuantity == undefined || item.productQuantity == '') {
                        $scope.tableValidation = true;
                        $scope.validationFailed = true;
                    }
                });
                if ($scope.tableValidation) {
                    $scope.postRequestLoding = false;
                    return false;
                }

                if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                    $scope.titleValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }

                if (currentStage == 2 && stage > currentStage) {
                    if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                        $scope.deliveryLocationValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                        $scope.paymentTermsValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                        $scope.deliveryTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }
                if (currentStage == 3 && stage > currentStage) {
                    if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                        $scope.urgencyValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    
                    if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= '') && $scope.formRequest.isQuotationPriceLimit == true) {
                        $scope.quotationPriceLimitValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.isQuotationPriceLimit == false) {
                        $scope.formRequest.quotationPriceLimit = 0;
                    }

                    if ($scope.formRequest.noOfQuotationReminders > 0)
                    {
                        $scope.formRequest.noOfQuotationReminders = parseInt($scope.formRequest.noOfQuotationReminders);
                    }
                    if ($scope.formRequest.remindersTimeInterval > 0) {
                        $scope.formRequest.remindersTimeInterval = parseInt($scope.formRequest.remindersTimeInterval);
                    }


                    if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                        $scope.noOfQuotationRemindersValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                        $scope.remindersTimeIntervalValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                        $scope.questionnaireValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                    if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                        $scope.quotationFreezTimeValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }
                if (!$scope.postRequestLoding) {
                    return false;
                } else {
                    $scope.postRequestLoding = true;
                    var ts = userService.toUTCTicks($scope.formRequest.quotationFreezTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.quotationFreezTime = "/Date(" + milliseconds + "000+0530)/";
                    var ts = userService.toUTCTicks($scope.formRequest.expStartTime);
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    $scope.formRequest.expStartTime = "/Date(" + milliseconds + "000+0530)/";

                    $scope.formRequest.requirementID = $stateParams.Id ? $stateParams.Id : -1;
                    $scope.formRequest.customerID = userService.getUserId();
                    $scope.formRequest.customerFirstname = userService.getFirstname();
                    $scope.formRequest.customerLastname = userService.getLastname();
                    $scope.formRequest.isClosed = "NOTSTARTED";
                    $scope.formRequest.endTime = "";
                    $scope.formRequest.sessionID = userService.getUserToken();
                    $scope.formRequest.subcategories = "";
                    $scope.formRequest.budget = 100000;
                    $scope.formRequest.custCompID = userService.getUserCompanyId();
                    $scope.formRequest.customerCompanyName = userService.getUserCompanyId();

                    for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                        $scope.formRequest.subcategories += $scope.sub.selectedSubcategories[i].subcategory + ",";
                    }

                    var category = [];
                    if ($scope.formRequest.category) {
                        category.push($scope.formRequest.category);
                    }
                    $scope.formRequest.category = category;
                    if (!$scope.formRequest.isForwardBidding) {
                        $scope.formRequest.cloneID = 0;
                        if ($stateParams.reqObj && $stateParams.reqObj.cloneId && $stateParams.reqObj.cloneId !== "") {
                            $scope.formRequest.cloneID = $stateParams.reqObj.cloneId;
                        }
                        auctionsService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {
                                    if ($scope.selectedProject) {
                                        $scope.selectedProject.sessionID = userService.getUserToken();
                                        $scope.selectedProject.requirement = {};
                                        $scope.selectedProject.requirement.requirementID = response.objectID;
                                        var param = { item: $scope.selectedProject };
                                        //prmCompanyService.SaveProjectRequirement(param).then(function (projecgtMappingResponse) {
                                        //    console.log("saving project mapping");
                                        //    console.log(projecgtMappingResponse);
                                        //    console.log("END saving project mapping");
                                        //});
                                    }
                                    $scope.SaveReqDepartments(response.objectID);
                                    //$scope.SaveReqDeptDesig(response.objectID);

                                    //$scope.DeleteRequirementTerms();
                                    // $scope.SaveRequirementTerms(response.objectID);

                                    if ($scope.selectedQuestionnaire != null && $scope.selectedQuestionnaire != undefined && $scope.selectedQuestionnaire.evalID != undefined) {
                                        techevalService.getquestionnaire($scope.selectedQuestionnaire.evalID, 1)
                                            .then(function (response1) {
                                                $scope.selectedQuestionnaire = response1;

                                                if ($scope.isTechEval) {
                                                    $scope.selectedQuestionnaire.reqID = response.objectID;
                                                }
                                                else {
                                                    $scope.selectedQuestionnaire.reqID = 0;
                                                }
                                                $scope.selectedQuestionnaire.sessionID = userService.getUserToken();
                                                var params = {
                                                    questionnaire: $scope.selectedQuestionnaire
                                                }
                                                techevalService.assignquestionnaire(params)
                                                    .then(function (response) {
                                                    })
                                            })
                                    }
                                    if (stage) {
                                        $state.go('save-requirement', { Id: response.objectID });
                                    }
                                }
                            });
                    } else {
                        fwdauctionsService.postrequirementdata($scope.formRequest)
                            .then(function (response) {
                                if (response.objectID != 0) {

                                    //$scope.SaveReqDepartments(response.objectID);
                                    $scope.SaveReqDeptDesig(response.objectID);

                                    swal({
                                        title: "Done!",
                                        text: "Requirement Created Successfully",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            $scope.postRequestLoding = false;
                                            //$state.go('view-requirement');
                                            $state.go('fwd-view-req', { 'Id': response.objectID });
                                        });
                                }
                            });
                    }
                }
                //$scope.postRequest(0, stage - 1, false, stage);                
            }

            $scope.goBack = function (stage, reqID) {
                $state.go('save-requirement', { Id: reqID });
            }

            $scope.removeAttach = function (index) {
                $scope.formRequest.multipleAttachments.splice(index, 1);
            }

            $scope.productsList = [];
            $scope.nonCoreproductsList = [];
            catalogService.getUserProducts().then(function (response) {
                        $scope.productsList = response;
                        $scope.nonCoreproductsList = $scope.productsList.filter(function (product) {
                            return product.isCoreProductCategory == 0;
                        });
                        
                        // re-set values to back;
                        if ($scope.nonCoreproductsList && $scope.nonCoreproductsList.length > 0) {
                            $scope.nonCoreproductsList.forEach(function (product, index) {
                                if (!$scope.isEdit) {
                                    product.doHide = true;
                                    $scope.AddOtherRequirementItem(product);
                                }
                            });
                        }
                    });
            //$scope.productsList = catalogService.getUserProducts();
            //$scope.nonCoreproductsList = $scope.productsList.filter(function (product) {
            //    return product.isCoreProductCategory == 0;
            //});
            //$scope.getProducts = function () {
            //    catalogService.getUserProducts($scope.compID, userService.getUserId())
            //        .then(function (response) {
            //            $scope.productsList = response;
            //            $scope.nonCoreproductsList = $scope.productsList.filter(function (product) {
            //                return product.isCoreProductCategory == 0;
            //            });
            //            console.log($scope.nonCoreproductsList);
            //        });
            //};

            //$scope.getProducts();

            // in form catalogue ctrl
            $scope.fillTextbox = function (selProd, index) {
                $scope['ItemSelected_' + index] = true;
                $scope.formRequest.listRequirementItems[index].productIDorName = selProd.prodName;
                $scope.formRequest.listRequirementItems[index].catalogueItemID = selProd.prodId;
                $scope.formRequest.listRequirementItems[index].productNo = selProd.prodNo
                $scope.formRequest.listRequirementItems[index].hsnCode = selProd.prodHSNCode; 
                $scope.formRequest.listRequirementItems[index].productDescription = selProd.prodDesc;
                $scope.formRequest.listRequirementItems[index].productCode = selProd.prodCode;
                //$scope.formRequest.listRequirementItems[index].productQuantityInList = []; //;
                //$scope.formRequest.listRequirementItems[index].productQuantityInList.push(selProd.prodQty);
                $scope.formRequest.listRequirementItems[index].productQuantityIn = selProd.prodQty;
                if (selProd.isNonCoreProductCategory) {
                    $scope.formRequest.listRequirementItems[index].productQuantity = 1;
                }
                $scope.formRequest.listRequirementItems[index].isNonCoreProductCategory = selProd.isNonCoreProductCategory;
                $scope['filterProducts_' + index] = null;
                $scope["filterProducts_Code_" + index] = null;
                $scope["filterProducts_Code1_" + index] = null;

                if (!selProd.isNonCoreProductCategory || selProd.isCoreProductCategory) {
                    $scope.GetProductQuotationTemplate(selProd.prodId, index, true);
                } else {
                    $scope.formRequest.listRequirementItems = _.sortBy($scope.formRequest.listRequirementItems, function (o) {
                        if (o.isNonCoreProductCategory) { return o.isNonCoreProductCategory; } else { return 0; }
                    });
                }
            }

            $scope.autofillProduct = function (prodName, index, fieldType) {
                $scope['ItemSelected_' + index] = false;
                var output = [];
                if (prodName && prodName != '' && prodName.length > 0) {
                    prodName = String(prodName).toUpperCase();
                    output = $scope.productsList.filter(function (product) {
                        if(product.isCoreProductCategory === 1) {
                            if (fieldType == 'NAME') {
                                return (String(product.prodName).toUpperCase().includes(prodName) == true);
                            }
                            else if (fieldType == 'CODE') {
                                return (String(product.prodNo).toUpperCase().includes(prodName) == true);
                            } 
                            else if (fieldType == 'CODEMAIN') {
                                return (String(product.prodCode).toUpperCase().includes(prodName) == true);
                            }
                        }
                    });
                }
                if (fieldType == 'NAME') {
                    $scope["filterProducts_" + index] = output;
                }
                else if (fieldType == 'CODE') {
                    $scope["filterProducts_Code_" + index] = output;
                }
                else if (fieldType == 'CODEMAIN') {
                    $scope["filterProducts_Code1_" + index] = output;
                }
            }

            $scope.onBlurProduct = function (index) {
                if ($scope['ItemSelected_' + index] == false) {
                    if (!$scope.formRequest.isRFP)
                    {
                        $scope.formRequest.listRequirementItems[index].productIDorName = "";
                        $scope.formRequest.listRequirementItems[index].productNo = "";
                        $scope.formRequest.listRequirementItems[index].productCode = "";
                    }

                }
                //$scope['filterProducts_' + index] = null;
            }

            $scope.formSplitChange = function () {
                var splitenabled = $scope.formRequest.isSplitEnabled;
                $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                    item.splitenabled = splitenabled;
                    if (splitenabled) {
                        item.productQuantity = 1;
                        $scope.formRequest.isDiscountQuotation = 0;
                    }
                });
            }

            $scope.onSplitChange = function (index) {


                if ($scope.formRequest.listRequirementItems[index].splitenabled == true) {
                    $scope.formRequest.listRequirementItems[index].productQuantity = 1;
                }
            }

            $scope.getCatalogCategories = function () {
                auctionsService.getcatalogCategories(userService.getUserId())
                    .then(function (response) {
                        $scope.companyCatalogueList = response;

                        $scope.catName = $scope.companyCatalogueList[0];
                        $scope.searchCategoryVendors($scope.catName);

                    });
            };

            $scope.searchCategoryVendors = function (str) {
                $scope.getvendors(str);
            };

            $scope.getproductvendors = function () {
                $scope.vendorsLoaded = false;
                $scope.Vendors = [];

                //products.push($scope.formRequest.category);
                //$scope.formRequest.category = category;
                //if ($scope.selectedProducts.length > 0) {
                    var params = { 'products': $scope.selectedProducts, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                    $http({
                        method: 'POST',
                        url: domain + 'getvendorsbyproducts',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data) {
                            if (response.data.length > 0) {
                                $scope.Vendors = response.data;
                                // if (allVendors === 1) {
                                // $scope.allCompanyVendors = response.data;
                                // allVendors = 0;
                                // }  
                                $scope.vendorsLoaded = true;
                                for (var j in $scope.formRequest.auctionVendors) {
                                    for (var i in $scope.Vendors) {
                                        if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                            $scope.Vendors.splice(i, 1);
                                        }
                                    }
                                }

                                $scope.VendorsTemp = $scope.Vendors;
                                if ($scope.searchvendorstring != '') {
                                    $scope.searchVendors($scope.searchvendorstring);
                                } else {
                                    $scope.searchVendors('');
                                }


                            }
                            $scope.getCatalogCategories();
                            //$scope.formRequest.auctionVendors =[];
                        } else {
                        }
                    }, function (result) {
                    });
                //}

                //$scope.getCatalogCategories();

            };

            $scope.getCurrencies();



            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListTemp = response;
                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule == $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                            }
                        });

                        //if (userService.getUserObj().isSuperUser) {
                        //    $scope.workflowList = $scope.workflowList;
                        //}
                        //else {
                        //    $scope.workflowList = $scope.workflowList.filter(function (item) {
                        //        return item.deptID == userService.getSelectedUserDeptID();

                        //    });
                        //}



                    });
            };
            //form Ctrl
           // $scope.getWorkflows();

            $scope.getItemWorkflow = function () {
                workflowService.getItemWorkflow(0, $stateParams.Id, $scope.WorkflowModule)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {

                                if (track.status == 'APPROVED' || track.status == 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status == 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }



                                if (track.status == 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status == 'PENDING' || track.status == 'HOLD') && count == 0) {
                                    count = count + 1;
                                    $scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status == 'PENDING' || track.status == 'HOLD' || track.status == 'REJECTED') && $scope.currentStep == 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });

            };

            $scope.updateTrack = function (step, status) {

                $scope.commentsError = '';

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = userService.getUserId();

                step.moduleName = $scope.WorkflowModule;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.getItemWorkflow();
                            //location.reload();
                            //     $state.go('list-pr');
                        }
                    })
            };

            $scope.assignWorkflow = function (moduleID) {
                workflowService.assignWorkflow(({ wID: $scope.formRequest.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionID }))
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.isSaveDisable = false;
                        }
                        else {
                            //  $state.go('list-pr');
                        }
                    })
            };

            $scope.IsUserApprover = false;

            $scope.functionResponse = false;

            $scope.IsUserApproverForStage = function (approverID) {
                workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                    .then(function (response) {
                        $scope.IsUserApprover = response;
                    });
            };

            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if (userService.getLocalDeptDesigt().deptId == step.department.deptID && userService.getLocalDeptDesigt().desigId == step.approver.desigID &&
                                (step.status == 'PENDING' || step.status == 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status == 'PENDING' || previousStep.status == 'HOLD' || previousStep.status == 'REJECTED') {
                                disable = true;
                            }
                            else if (userService.getLocalDeptDesigt().deptId == step.department.deptID && userService.getLocalDeptDesigt().desigId == step.approver.desigID &&
                                (step.status == 'PENDING' || step.status == 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                })

                return disable;
            };

            /*region end WORKFLOW*/

            $scope.PRList1 = [];

            $scope.GetReqPRList = function () {
                var params = {
                    "userid": userService.getUserId(),
                    "deptid": $scope.deptID,
                    "desigid": $scope.desigID,
                    "depttypeid": $scope.deptTypeID,
                    "onlyopen": 1,
                    "sessionid": userService.getUserToken(),
                }
                PRMPRServices.getreqprlist(params)
                    .then(function (response) {
                        $scope.PRList = response;
                        $scope.PRList1 = response;
                        $scope.selectRequirementPRS();
                        $scope.PRList.forEach(function (item, index) {
                            item.isSelected = false;
                        })

                        // console.log("main array count>>>" + $scope.PRList.length);

                        $scope.PRList = $scope.PRList.filter(function (prObj) {
                            prObj.CREATED_DATE = userService.toLocalDate(prObj.CREATED_DATE);
                            prObj.DELIVERY_DATE = userService.toLocalDate(prObj.DELIVERY_DATE);
                            return prObj.PR_STATUS !== 'COMPLETED';
                        });
                        //  console.log("filtered array count>>>" + $scope.PRList.length);
                        if ($stateParams.prDetail && $stateParams.prDetail.PR_ID) {
                            $scope.formRequest.PR_ID = $stateParams.prDetail.PR_ID;
                            $scope.formRequest.PR_NUMBER = $stateParams.prDetail.PR_NUMBER;
                            $stateParams.prDetail.isSelected = true;
                            $scope.fillReqItems($stateParams.prDetail);
                            //$scope.GetPRItemsList({ PR_ID: $stateParams.prDetail.PR_ID });
                        }
                    })
            };

            $scope.GetReqPRList();

            $scope.getPRNumber = function (pr_ID)
            {
                var params = {
                    "prid": pr_ID,
                    "sessionid": userService.getUserToken()
                };
                PRMPRServices.getprdetails(params)
                    .then(function (response) {
                        $scope.PRDetails = response;
                        if ($scope.PRDetails.PR_ID == pr_ID) {
                            //$scope.formRequest.PR_ID = $scope.PRDetails.PR_ID;
                            $scope.formRequest.PR_NUMBER = $scope.PRDetails.PR_NUMBER;
                        }
                    })
            }

            $scope.fillReqItems = function (pr) {
                $scope.formRequest.PR_ID = pr.PR_ID;
                if (pr.isSelected) {
                    $scope.GetPRItemsList(pr);
                } else if (!pr.isSelected) {
                    $scope.GetPRItemsList(pr);
                    

                }

                $scope.PRList = _.orderBy($scope.PRList, ['isSelected'], ['desc']);
            }


            $scope.prDeleteItemsArr = [];

            $scope.GetPRItemsList = function (pr) {
                if (pr && pr.PR_ID > 0) {
                    var params = {
                        "prid": pr.PR_ID,
                        "sessionid": userService.getUserToken()
                    }
                    PRMPRServices.getpritemslist(params)
                        .then(function (response) {
                            $scope.PRItemsList = response;


                            // First check whether this PR exists in requirement items or not if exists then reduce the quantity and return the item//

                            $scope.prDeleteItemsArr = [];

                            if (!pr.isSelected) {
                                if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                    //var success = $scope.PRItemsList.every(function (val) {//pritemlist array
                                    //    if ($scope.formRequest.listRequirementItems.some(productIds => productIds.productId === val.PRODUCT_ID)) {// requirement items array 
                                    //        var index = $scope.formRequest.listRequirementItems.findIndex(c => c.productId === val.PRODUCT_ID);
                                    //        var deductQuantity = $scope.formRequest.listRequirementItems[index].productQuantity - val.REQUIRED_QUANTITY;
                                    //        $scope.formRequest.listRequirementItems[index].productQuantity = deductQuantity;
                                    //    }
                                    //});

                                    //$scope.deleteItem(pr);
                                    //return;
                                    $scope.PRItemsList.forEach(function (prItem, prIndex) {
                                        $scope.formRequest.listRequirementItems.forEach(function (rfqItem, rfqIndex) {
                                            if (prItem.PRODUCT_ID == rfqItem.productId) {
                                                var deductQuantity = rfqItem.productQuantity - prItem.REQUIRED_QUANTITY;
                                                rfqItem.productQuantity = deductQuantity;
                                                if (rfqItem.productQuantity <= 0) {
                                                    $scope.prDeleteItemsArr.push(rfqItem);
                                                }
                                            } else {
                                                if (prItem.PR_ID == rfqItem.PR_ID)
                                                {
                                                    $scope.prDeleteItemsArr.push(rfqItem);
                                                }
                                            }
                                        });
                                    });

                                    if ($scope.prDeleteItemsArr && $scope.prDeleteItemsArr.length >0) {
                                        $scope.prDeleteItemsArr.forEach(function (item, index) {
                                            $scope.deleteItem(item);
                                        });
                                    }
                                    return;
                                }
                            }
                            
                            // First check whether this PR exists in requirement items or not if exists then reduce the quantity and return the item//


                            var selectedPrs = $scope.PRList.filter(function (pr) {
                                return pr.isSelected;
                            });

                            var validPRItems = $scope.PRItemsList.filter(function (prItem) {
                                return (!prItem.REQ_ID || prItem.REQ_ID <= 0);
                            });

                            //To check it is first PR selected in List
                            if (selectedPrs && selectedPrs.length <= 1 && validPRItems && validPRItems.length > 0) {
                                $scope.formRequest.listRequirementItems = [];
                            }

                            var currentPR = $scope.PRList.filter(function (prDetails) {
                                return prDetails.PR_ID === +pr.PR_ID;
                            });

                            if (currentPR && currentPR.length > 0) {
                                currentPR[0].isSelected = true;
                            }                            

                            if (!validPRItems || validPRItems.length <= 0) {
                                swal("Warning!", "All items in PR already linked to existing Requirement.", "error");   
                                if (currentPR && currentPR.length > 0) {
                                    currentPR[0].isSelected = false;
                                }
                            }

                            $scope.PRItemsList.forEach(function (item, index) {
                                if (!item.REQ_ID || item.REQ_ID <= 0) {
                                    $scope.requirementItems =
                                        {
                                            productSNo: index + 1,
                                            ItemID: 0,
                                            productIDorName: item.ITEM_NAME,
                                            productNo: item.ITEM_NUM,
                                            hsnCode: item.HSN_CODE,
                                            productDescription: item.ITEM_DESCRIPTION,
                                            productQuantity: item.REQUIRED_QUANTITY,
                                            productQuantityIn: item.UNITS,
                                            productBrand: item.BRAND,
                                            othersBrands: '',
                                            isDeleted: 0,
                                            productImageID: item.ATTACHMENTS,
                                            attachmentName: '',
                                            //budgetID: 0,
                                            //bcInfo: '',
                                            productCode: item.ITEM_CODE,
                                            splitenabled: 0,
                                            fromrange: 0,
                                            torange: 0,
                                            requiredQuantity: 0,
                                            I_LLP_DETAILS: "",
                                            itemAttachment: "",
                                            itemMinReduction: 0,
                                            productId: item.PRODUCT_ID,
                                            PR_ID:item.PR_ID
                                        };

                                    if ($scope.requirementItems.productImageID == null || $scope.requirementItems.productImageID == '') {
                                        $scope.requirementItems.productImageID = 0;
                                    } else {
                                        $scope.requirementItems.productImageID == item.ATTACHMENTS;
                                    }



                                    if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                        var existingItem = $scope.formRequest.listRequirementItems.filter(function (currentItem) {
                                            return currentItem.productId == $scope.requirementItems.productId;
                                        });

                                        if (existingItem && existingItem.length > 0) {
                                            existingItem[0].productQuantity += $scope.requirementItems.productQuantity;
                                        } else {
                                            $scope.formRequest.listRequirementItems.push($scope.requirementItems);
                                        }
                                    } else {
                                        $scope.formRequest.listRequirementItems.push($scope.requirementItems);
                                    }

                                    //Correcting SNO (because Each and Every Time It is Filling with PR Items List)//

                                    if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                                        $scope.formRequest.listRequirementItems.forEach(function (snoItem, snoIndex) {
                                            snoItem.productSNo = snoIndex + 1;
                                        })
                                    }

                                    //Correcting SNO (because Each and Every Time It is Filling with PR Items List)//

                                    $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                                        var selProd = $scope.productsList.filter(function (prod) {
                                            //return prod.prodCode == item.productIDorName;//prodName
                                            return prod.prodCode == item.productCode;
                                        });

                                        if (selProd && selProd != null && selProd.length > 0) {
                                            $scope.autofillProduct(item.productIDorName, index)
                                            $scope.fillTextbox(selProd[0], index);
                                        }
                                    });
                                }
                            });
                        });
                }
            };





            $scope.GetProductQuotationTemplate = function (productId, index, productChange) {

                var params = {
                    "catitemid": productId,
                    "sessionid": userService.getUserToken()
                };

                catalogService.GetProductQuotationTemplate(params)
                    .then(function (response) {
                        if (response) {
                            var currentItem = $scope.formRequest.listRequirementItems[index];
                            currentItem.productQuotationTemplateMaster = response;
                            if (!currentItem.productQuotationTemplate || currentItem.productQuotationTemplate.length <= 0 ||
                                !$scope.isEdit) {
                                currentItem.productQuotationTemplate = response;
                                if ($scope.isEdit) {
                                    currentItem.productQuotationTemplate.forEach(function (templateItem, itemIndexs) {
                                        templateItem.IS_VALID = 0;
                                    });
                                }
                            }
                            else if (currentItem.productQuotationTemplateMaster && currentItem.productQuotationTemplateMaster.length > 0) {
                                if (productChange) {
                                    currentItem.productQuotationTemplate = currentItem.productQuotationTemplateMaster;
                                } else {
                                    currentItem.productQuotationTemplateMaster.forEach(function (templateItem, itemIndexs) {
                                        if (templateItem && templateItem.NAME) {
                                            var filterResult = currentItem.productQuotationTemplateArray.filter(function (template) {
                                                return (template.NAME === templateItem.NAME);
                                            });

                                            if (!filterResult || filterResult.length <= 0) {
                                                templateItem.IS_VALID = 0;
                                                currentItem.productQuotationTemplate.unshift(templateItem);
                                            }
                                        }
                                    });
                                }
                            }

                            $scope.formRequest.listRequirementItems = _.sortBy($scope.formRequest.listRequirementItems, function (o) {
                                if (o.isNonCoreProductCategory) { return o.isNonCoreProductCategory; } else { return 0; }
                            });
                        }
                    });
            };

            //Below product sub item
            $scope.SaveProductQuotationTemplate = function (obj) {
                var sameNameError = false;
                obj.currentProductQuotationTemplate.forEach(function (item, itemIndex) {
                    if (obj.NAME.toUpperCase() == item.NAME.toUpperCase() && obj.T_ID == 0) {
                        sameNameError = true;
                    }
                });

                if (sameNameError) { growlService.growl("Same name Error.", "inverse"); return false; }

                var params = {
                    "productquotationtemplate": obj,
                    "sessionid": userService.getUserToken()
                };

                catalogService.SaveProductQuotationTemplate(params)
                    .then(function (response) {
                        if (response) {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.resetTemplateObj();
                            if (obj.PRODUCT_ID > 0 && obj.index >= 0) {
                                $scope.GetProductQuotationTemplate(obj.PRODUCT_ID, obj.index, true);
                            }
                        }
                    });
            };

            $scope.resetTemplateObj = function (product, index) {
                if (!product) {
                    product = { catalogueItemID: 0, productQuotationTemplate: []};
                }
                if (!index) {
                    index = 0;
                }

                $scope.QuotationTemplateObj = {
                    currentProductQuotationTemplate: product.productQuotationTemplate,
                    index: index,
                    T_ID: 0,
                    PRODUCT_ID: product.catalogueItemID,
                    NAME: '',
                    DESCRIPTION: '',
                    HAS_SPECIFICATION: 0,
                    HAS_PRICE: 0,
                    HAS_QUANTITY: 0,
                    CONSUMPTION: 1,
                    UOM: '',
                    HAS_TAX: 0,
                    IS_VALID: 1,
                    U_ID: userService.getUserId()
                };
            };

            $scope.resetTemplateObj();
            //^Above product sub item

            $scope.loadCustomFields = function () {
                var params = {
                    "compid": userService.getUserCompanyId(),
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.getCustomFieldList(params)
                    .then(function (response) {
                        $scope.customFieldList = response;
                            if ($scope.isEdit) {
                                params = {
                                    "compid": userService.getUserCompanyId(),
                                    "fieldmodule": 'REQUIREMENT',
                                    "moduleid": $stateParams.Id,
                                    "sessionid": userService.getUserToken()
                                };

                                PRMCustomFieldService.GetCustomFieldsByModuleId(params)
                                .then(function (response) {
                                    $scope.selectedcustomFieldList = response;
                                    $scope.selectedcustomFieldList.forEach(function (cfItem, index) {
                                        cfItem.IS_SELECTED == 1;

                                        var cfTempField = _.filter($scope.customFieldList, function (field) {
                                                return field.CUST_FIELD_ID == cfItem.CUST_FIELD_ID;
                                            });
                                        if(cfTempField && cfTempField.length > 0){
                                            cfTempField[0].IS_SELECTED = 1;
                                            cfTempField[0].FIELD_VALUE = cfItem.FIELD_VALUE;
                                            cfTempField[0].MODULE_ID = cfItem.MODULE_ID;
                                        }
                                    });
                                });
                             }
                        });
                };

            $scope.loadCustomFields();

            $scope.addCustomFieldToFrom = function (field) {
                $scope.selectedcustomFieldList = _.filter($scope.customFieldList, function (field) {
                    return field.IS_SELECTED == 1;
                });
            };

            $scope.validateSubItemQuantity = function (product, productQuotationTemplate){
                if (!productQuotationTemplate.CONSUMPTION) {
                    productQuotationTemplate.CONSUMPTION = 1;
                } else {
                    product.productQuantity = 1;
                }
            };

            
            $scope.saveRequirementCustomFields = function(requirementId) {
                if (requirementId) {
                     $scope.selectedcustomFieldList.forEach(function (item, index) {
                        item.MODULE_ID = requirementId;
                        item.ModifiedBy = $scope.userID;
                    });

                    var params = {
                        "details": $scope.selectedcustomFieldList,
                        "sessionid": userService.getUserToken()
                    };

                    PRMCustomFieldService.saveCustomFieldValue(params)
                        .then(function (response) {
                        
                        });
                }
            };

            $scope.GetPRMFieldMappingDetails = function () {
                $scope.prmFieldMappingDetails = {};
                var template = $scope.selectedTemplate ? $scope.selectedTemplate : 'DEFAULT';
                userService.GetPRMFieldMappingDetails(template).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        $scope.prmFieldMappingDetails[item.FIELD_NAME] = item.FIELD_ALIAS_NAME;
                    });
                });
            };

            $scope.GetPRMFieldMappingDetails();

            $scope.GetPRMFieldMappingTemplates = function () {
                PRMCustomFieldService.GetPRMFieldMappingTemplates().then(function (response) {
                    $scope.prmFieldMappingTemplates = response;
                });
            };

            $scope.GetPRMFieldMappingTemplates();


            $scope.searchingCategoryVendors = function (value) {
                if (value) {
                    $scope.VendorsList = _.filter($scope.VendorsTempList1, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1; });
                }
            };

            $scope.selectFormCategorySearch = function (vendor) {
                var error = false;

                $scope.formRequest.auctionVendors.forEach(function (SV, SVIndex) {
                    if (SV.vendorID == vendor.vendorID) {
                        error = true;
                        growlService.growl("Vendor already selected", "inverse");

                    }
                })

                if (error == true) {
                    return;
                }

                $scope.Vendors.forEach(function (UV, UVIndex) {
                    if (UV.vendorID == vendor.vendorID) {
                        error = true;
                    }
                })

                if (error == false) {
                    vendor.isFromGlobalSearch = 1;
                    $scope.formRequest.auctionVendors.push(vendor);
                    $scope.VendorsList = $scope.VendorsList.filter(function (obj) {
                        return obj != vendor;
                    });
                    $scope.ShowDuplicateVendorsNames.push(vendor);
                }
                else {
                    $scope.selectForA(vendor);
                    vendor.isChecked = false;
                }
            };

            // Category Based Vendors //
            $scope.VendorsList = [];
            $scope.VendorsTempList1 = [];

            $scope.getCategoryVendors = function (str) {
                $scope.ShowDuplicateVendorsNames = [];
                $scope.VendorsList = [];
                $scope.VendorsTempList1 = [];
                $scope.vendorsLoaded = false;
                var category = '';

                category = catObj.catCode;

                var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                $http({
                    method: 'POST',
                    url: domain + 'getVendorsbyCategories',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.VendorsList = response.data;
                            $scope.VendorsTempList = $scope.VendorsList;



                            $scope.Vendors.forEach(function (item, index) {
                                $scope.VendorsTempList.forEach(function (item1, index1) {
                                    if (item.vendorID == item1.vendorID) {
                                        $scope.ShowDuplicateVendorsNames.push(item1);
                                        $scope.VendorsList.splice(index1, 1);
                                    }
                                })
                            });

                            if ($scope.formRequest.auctionVendors.length > 0) {
                                $scope.formRequest.auctionVendors.forEach(function (item1, index1) {
                                    $scope.VendorsTempList.forEach(function (item2, index2) {
                                        if (item1.vendorID == item2.vendorID) {
                                            $scope.ShowDuplicateVendorsNames.push(item2);
                                            $scope.VendorsList.splice(index2, 1);
                                        }
                                    });
                                });
                            }


                            $scope.VendorsTempList1 = $scope.VendorsList;

                            if ($scope.ShowDuplicateVendorsNames.length > 0) {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                            } else {
                                $scope.totalItems = $scope.ShowDuplicateVendorsNames.length;
                            }

                        } else {
                            $scope.VendorsList = [];
                        }

                        if ($scope.searchCategoryVendorstring != '') {
                            $scope.searchingCategoryVendors($scope.searchCategoryVendorstring);
                        } else {
                            $scope.searchingCategoryVendors('');
                        }


                    } else {

                    }
                }, function (result) {
                });

            }
            // Category Based Vendors //

            $scope.validateLineItemsList = function () {
                if ($scope.nonCoreproductsList && $scope.nonCoreproductsList.length > 0 && $scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 0) {
                    $scope.nonCoreproductsList.forEach(function (nonCoreProduct, index) {
                        var filterItems = $scope.formRequest.listRequirementItems.filter(function (product) {
                            return product.catalogueItemID === nonCoreProduct.prodId;
                        });

                        if (filterItems && filterItems.length > 0) {
                            nonCoreProduct.doHide = true;
                        }
                    });
                }
            };

            $scope.selectRequirementPRS = function () {
                if ($stateParams.Id) {
                    if ($scope.PRList && $scope.PRList.length > 0 && $scope.formRequest.PR_ID && $scope.formRequest.PR_ID !== '') {
                        var requirementPRs = $scope.formRequest.PR_ID.split(',');
                        requirementPRs.forEach(function (requirementPRs, index) {
                            var filterPR = $scope.PRList.filter(function (pr) {
                                return pr.PR_ID === +requirementPRs;
                            });

                            if (filterPR && filterPR.length > 0) {
                                filterPR[0].isSelected = true;
                            }
                        });

                        $scope.PRList = _.orderBy($scope.PRList, ['isSelected'], ['desc']);
                    }
                }
            };


            $('.selected-items-box').bind('click', function(e) {
                $('.multiple-selection-dropdown .list').slideToggle('fast');
            });




            $scope.totalPRItems = 0;
            $scope.currentPRPage = 1;
            $scope.currentPRPage2 = 1;
            $scope.itemsPerPRPage = 10;
            $scope.itemsPerPRPage2 = 10;
            $scope.maxPRSize = 10;

            $scope.setPRPage = function (pageNo) {
                $scope.currentPRPage = pageNo;
            };

            $scope.pagePRChanged = function () {
            };


            $scope.searchTable = function (str) {
                var filterText = angular.lowercase(str);
                if (!str || str == '' || str == undefined || str == null) {
                    $scope.PRList = $scope.PRList1;
                }
                else {
                    $scope.PRList = $scope.PRList1.filter(function (products) {
                        return (String(angular.lowercase(products.PR_NUMBER)).includes(filterText) == true);
                    });
                }


                $scope.totalPRItems = $scope.PRList.length;
            }



            $scope.createProduct = function (product,index) {
                $scope.itemProductNameErrorMessage = '';
                $scope.itemProductUnitsErrorMessage = '';

                if (!product.productIDorName || !product.productQuantityIn) {

                    if (!product.productIDorName) {
                        $scope.itemProductNameErrorMessage = 'Please Enter Product Name.';
                    } else if (!product.productQuantityIn) {
                        $scope.itemProductUnitsErrorMessage = 'Please Enter Units.';
                    }
                    return 
                }


                $scope.productObj = {
                    prodId: 0,
                    prodCode: product.productCode,
                    compId : userService.getUserCompanyId(),
                    prodName: product.productIDorName,
                    prodNo:   product.productNo,
                    prodQty:  product.productQuantityIn,
                    isValid: 1,
                    ModifiedBy: userService.getUserId()
                };

                var params = {
                    reqProduct: $scope.productObj,
                    sessionID: userService.getUserToken()
                }

                catalogService.addproduct(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            //growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            //growlService.growl("product Added Successfully.", "success");
                            $scope.itemProductNameErrorMessage = '';
                            $scope.itemProductUnitsErrorMessage = '';
                            var productID = response.repsonseId;
                            $scope.formRequest.listRequirementItems[index].catalogueItemID = productID;
                            $scope.formRequest.listRequirementItems[index].isNewItem = true;
                            $scope.GetProductQuotationTemplate(productID, index, true);
                        }
                    });
                

            }

        }]);﻿prmApp

    // =========================================================================
    // AUCTION ITEM
    // =========================================================================

    .controller('tenderItemCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "PRMLotReqService", "PRMCustomFieldService", "workflowService",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices,
            PRMLotReqService, PRMCustomFieldService, workflowService) {
            $scope.vendorTenderQuotations = false;
            $scope.companyItemUnits = [];
            $scope.productConfig = [];
            $scope.selectedcustomFieldList = [];
            var id = $stateParams.Id;
            $scope.reqId = $stateParams.Id;
            $scope.IS_CB_ENABLED = false;
            $scope.IS_CB_NO_REGRET = false;
            if (!$scope.reqId) {
                $scope.reqId = $stateParams.reqID;
                $stateParams.Id = $stateParams.reqID;
            }

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer1 = userService.getUserType();
            //alert($scope.isCustomer1);
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            $scope.regretDetails = [];
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
            $scope.compID = userService.getUserCompanyId();

            //#CB-0-2018-12-05
            $scope.goToCbVendor = function (vendorID) {
                $state.go('cb-vendor', { 'reqID': $stateParams.Id, 'vendorID': vendorID });
            };


            $scope.goToAudit = function () {
                var url = $state.href("audit", { "reqID": $scope.reqId });
                window.open(url, '_blank');
            };

            $scope.numberOfItemDetails = 1;
            $scope.signalRCustomerAccess = false;
            $scope.Loding = false;
            $scope.makeaBidLoding = false;
            $scope.showTimer = false;
            $scope.userIsOwner = false;
            $scope.sessionid = userService.getUserToken();
            $scope.allItemsSelected = true;
            $scope.RevQuotationfirstvendor = "";
            $scope.disableBidButton = false;
            $scope.nonParticipatedMsg = '';
            $scope.quotationRejecteddMsg = '';
            $scope.quotationNotviewedMsg = '';
            $scope.revQuotationRejecteddMsg = '';
            $scope.revQuotationNotviewedMsg = '';
            $scope.quotationApprovedMsg = '';
            $scope.revQuotationApprovedMsg = '';
            $scope.reduceBidAmountNote = '';
            $scope.incTaxBidAmountNote = '';
            $scope.noteForBidValue = '';
            $scope.CSGSTFields = false;
            $scope.IGSTFilelds = false;
            $scope.disableDecreaseButtons = true;
            $scope.disableAddButton = true;
            $scope.NegotiationEnded = false;
            $scope.uploadQuotationButtonMsg = 'The Submit Quotation option would be available only if prices are entered above.';
            $scope.uploadQuotationTaxValidationMsg = 'Please check all Tax Feilds';
            $scope.currentID = -1;
            $scope.timerStyle = { 'color': '#000' };
            $scope.savingsStyle = { 'color': '#228B22' };
            $scope.restartStyle = { 'color': '#f00' };
            $scope.bidAttachement = [];
            $scope.bidAttachementName = "";
            $scope.bidAttachementValidation = false;
            $scope.bidPriceEmpty = false;
            $scope.bidPriceValidation = false;
            $scope.showStatusDropDown = false;
            $scope.vendorInitialPrice = 0;
            $scope.showGeneratePOButton = false;
            $scope.isDeleted = false;
            $scope.ratingForVendor = 0;
            $scope.ratingForCustomer = 0;
            $scope.participatedVendors = [];
            $scope.vendorApprovals = [];

            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.workflowObj = {};
            $scope.workflowObj.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'VENDOR';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/



            var requirementHub;
            $scope.auctionItem = {
                auctionVendors: [],
                listRequirementItems: [],
                listRequirementTaxes: []
            };

            $scope.auctionItemTemporary = {
                emailLinkVendors :[]
            };

            $scope.checkConnection = function () {
                if (requirementHub) {
                    return requirementHub.getStatus();
                } else {
                    return 0;
                }
            };
            $scope.auctionItemVendor = [];
            $scope.reconnectHub = function () {
                if (requirementHub) {
                    if (requirementHub.getStatus() == 0) {
                        requirementHub.reconnect();
                        return true;
                    }
                } else {
                    requirementHub = SignalRFactory('', signalRHubName);
                }

            }
            $scope.invokeSignalR = function (methodName, params, callback) {
                if ($scope.checkConnection() == 1) {
                    requirementHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    })
                } else {
                    $scope.reconnectHub();
                    requirementHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    })
                }
            }
            var requirementData = {};
            $scope.quotationAttachment = null;
            $scope.days = 0;
            $scope.hours = 0;
            $scope.mins = 0;
            $scope.NegotiationSettings = {
                negotiationDuration: ''
            };
            $scope.divfix = {};
            $scope.divfix3 = {};
            $scope.divfix1 = {};
            $scope.divfix2 = {};
            $scope.divfixMakeabid = {};
            $scope.divfixMakeabidError = {};
            $scope.boxfix = {};
            $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
            $scope.auctionItem.reqType = 'REGULAR';
            $scope.auctionItem.priceCapValue = 0;
            $scope.auctionItem.isUnitPriceBidding = 1;
            $log.info('trying to connect to service')
            requirementHub = SignalRFactory('', signalRHubName);
            $scope.invokeSignalR('joinGroup', 'requirementGroup' + $stateParams.Id);
            $log.info('connected to service')
            $scope.$on("$destroy", function () {
                $log.info('disconecting signalR')
                requirementHub.stop();
                $log.info('disconected signalR')
            });
            $scope.clickToOpen = function () {
                ngDialog.open({ template: 'login/termsUpdateStartTime.html', width: 1000, height: 500 });
            };

            $scope.updateTimeLeftSignalR = function () {
                var parties = id + "$" + userService.getUserId() + "$" + "10000" + "$" + userService.getUserToken();
                $scope.invokeSignalR('UpdateTime', parties);
            };

            $scope.subIemsColumnsVisibility = {
                hideSPECIFICATION: true,
                hideQUANTITY: true,
                hideTAX: true,
                hidePRICE: true
            };

            $scope.newPriceToBeQuoted = 0;

            $scope.reduceBidAmount = function () {
                if (!isNaN($scope.reduceBidValue) && $scope.reduceBidValue != "" && $scope.reduceBidValue >= 0 && $scope.auctionItem.minPrice > $scope.reduceBidValue) {
                    $("#reduceBidValue").val($scope.reduceBidValue);


                    $("#makebidvalue").val($scope.precisionRound(parseFloat($scope.auctionItem.minPrice - $scope.reduceBidValue), 2));
                    $("#reduceBidValue1").val($scope.precisionRound(parseFloat($scope.auctionItem.minPrice - $scope.reduceBidValue), 2));

                    //console.log($scope.auctionItem.minPrice - $scope.reduceBidValue);

                    //angular.element($event.target).parent().addClass('fg-line fg-toggled');
                }
                else {
                    $("#makebidvalue").val($scope.auctionItem.minPrice);
                    //swal("Error!", "Invalid bid value.", "error");
                    $("#reduceBidValue1").val($scope.auctionItem.minPrice);
                    $scope.reduceBidValue = 0;
                    $("#reduceBidValue").val($scope.reduceBidValue);
                }


            };

            $scope.bidAmount = function () {
                if ($scope.vendorBidPrice != "" && $scope.vendorBidPrice >= 0 && $scope.auctionItem.minPrice > $scope.vendorBidPrice) {
                    $("#reduceBidValue").val($scope.auctionItem.minPrice - $scope.vendorBidPrice);
                    /*angular.element($event.target).parent().addClass('fg-line fg-toggled');*/
                }
                else {
                    $("#reduceBidValue").val(0);
                    swal("Error!", "Invalid bid value.", "error");
                    $scope.vendorBidPrice = 0;
                }
            };

            $scope.formRequest = {
                selectedVendor: {},
                priceCapValue: 0,
                priceCapValueMsg: ''
            };

            $scope.selectVendor = function () {
                var selVendID = $scope.formRequest.selectedVendor.vendorID;
                var winVendID = $scope.auctionItem.auctionVendors[0].vendorID;
                if (!$scope.formRequest.selectedVendor.reason && selVendID != winVendID) {
                    growlService.growl("Please enter the reason for choosing the particular vendor", "inverse");
                    return false;
                }
                var params = {
                    userID: userService.getUserId(),
                    vendorID: $scope.formRequest.selectedVendor.vendorID,
                    reqID: $scope.auctionItem.requirementID,
                    reason: $scope.formRequest.selectedVendor.reason ? $scope.formRequest.selectedVendor.reason : (selVendID != winVendID ? $scope.formRequest.selectedVendor.reason : ""),
                    sessionID: userService.getUserToken()
                }
                auctionsService.selectVendor(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Vendor " + response.userInfo.firstName + " " + response.userInfo.lastName + " has been selected for the final  ", "inverse");
                            $scope.getData();
                        }
                    })
            }

            $scope.exportItemsToExcel = function () {
                var mystyle = {
                    sheetid: 'QuotationDetails',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                if ($scope.auctionItem.isDiscountQuotation == 0) {
                    var name = '';
                    if ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED') {
                        name = 'UNIT_QUOTATION_' + $scope.reqId;
                    } else {
                        name = 'UNIT_BIDDING_' + $scope.reqId;
                    }
                    reportingService.downloadTemplate(name, userService.getUserId(), $scope.reqId);
                    //  alasql('SELECT itemID as [ItemID], productIDorName as [ProductName], productNo as [ProductNumber], productDescription as [Description], productQuantity as [Quantity], productBrand as Brand, productQuantityIn as [Units],unitPrice as UnitPrice, cGst, sGst, iGst INTO XLSX(?,{headers:true,sheetid: "QuotationDetails", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["QuotationDetails.xlsx", $scope.auctionItemVendor.listRequirementItems]);
                }
                if ($scope.auctionItem.isDiscountQuotation == 1) {
                    alasql('SELECT itemID as [ItemID], productIDorName as [ProductName], productNo as [ProductNumber], productDescription as [Description], productQuantity as [Quantity], productBrand as Brand, productQuantityIn as [Units],unitMRP as UnitMRP, unitDiscount as UnitDiscount, cGst, sGst, iGst INTO XLSX(?,{headers:true,sheetid: "QuotationDetails", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["QuotationDetails.xlsx", $scope.auctionItemVendor.listRequirementItems]);
                }

            }

            $scope.rateVendor = function (vendorID) {
                var params = {
                    uID: userService.getUserId(),
                    userID: vendorID,
                    rating: $scope.ratingForVendor,
                    sessionID: userService.getUserToken()
                }
                auctionsService.rateVendor(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Rating saved successfully. You can edit your rating as well.", "inverse");
                        }
                    })
            }

            $scope.rateCustomer = function () {
                var params = {
                    uID: userService.getUserId(),
                    userID: $scope.auctionItem.customerID,
                    rating: $scope.ratingForCustomer,
                    sessionID: userService.getUserToken()
                }
                auctionsService.rateVendor(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Rating saved successfully. You can edit your rating as well.", "inverse");
                        }
                    })
            }

            $scope.makeABidDisable = false;

            $scope.companyINCOTerms = [];



            $scope.makeaBid1 = function () {
                $scope.makeABidDisable = true;
                var bidPrice = $("#makebidvalue").val();

                $log.info(bidPrice);

                if (bidPrice == "" || Number(bidPrice) <= 0) {
                    $scope.bidPriceEmpty = true;
                    $scope.bidPriceValidation = false;
                    $("#makebidvalue").val("");
                    $scope.makeABidDisable = false;
                    return false;
                } else if (!isNaN($scope.auctionItem.minPrice) && $scope.auctionItem.minPrice > 0 && bidPrice >= $scope.auctionItem.minPrice) {
                    $scope.bidPriceValidation = true;
                    $scope.bidPriceEmpty = false;
                    $scope.makeABidDisable = false;
                    $scope.makeABidDisable = false;
                    return false;
                } else {
                    $scope.bidPriceValidation = false;
                    $scope.bidPriceEmpty = false;
                }
                if (($scope.bidAttachementName == "" || $scope.bidAttachement.length == 0) && $scope.quotationStatus == false) {
                    $scope.bidAttachementValidation = true;
                    $scope.makeABidDisable = false;
                    return false;
                } else {
                    $scope.bidAttachementValidation = false;
                }
                if (bidPrice > $scope.auctionItem.auctionVendors[0].runningPrice - $scope.auctionItem.minBidAmount) {
                    $scope.reduceBidValue = "";
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    swal("Error!", "Your amount must be at least " + $scope.auctionItem.minBidAmount + " less than your previous bid.", 'error');
                    $scope.makeABidDisable = false;
                    return false;
                }
                if (bidPrice < (75 * $scope.auctionItem.auctionVendors[0].runningPrice / 100)) {
                    $scope.reduceBidValue = "";
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    swal("Maximum Reduction Error!", " You are reducing more than 25% of current bid amount. The Maximum reduction amount per bid should not exceed more than 25% from current bid amount  " + $scope.auctionItem.minPrice + ". Incase if You want to Reduce more Please Do it in Multiple Bids", "error");
                    $scope.makeABidDisable = false;
                    return false;
                }
                else {
                    var params = {};
                    params.reqID = parseInt(id);
                    params.sessionID = userService.getUserToken();
                    params.userID = parseInt(userService.getUserId());
                    params.price = $scope.precisionRound(parseFloat(bidPrice), 2);
                    params.quotationName = $scope.bidAttachementName;


                    //params.freightCharges = $scope.revfreightCharges;
                    //params.freightChargesTaxPercentage = $scope.freightChargesTaxPercentage;
                    //params.freightChargesWithTax = $scope.freightChargesWithTax;

                    //params.packingCharges = $scope.revpackingCharges;
                    //params.packingChargesTaxPercentage = $scope.packingChargesTaxPercentage;
                    //params.packingChargesWithTax = $scope.revpackingChargesWithTax;

                    //params.installationCharges = $scope.revinstallationCharges;
                    //params.installationChargesTaxPercentage = $scope.installationChargesTaxPercentage;
                    //params.installationChargesWithTax = $scope.revinstallationChargesWithTax;


                    if ($scope.auctionItem.isUnitPriceBidding == 1) {
                        $scope.items = {
                            itemsList: $scope.auctionItemVendor.listRequirementItems,
                            userID: userService.getUserId(),
                            reqID: params.reqID,
                            price: $scope.revtotalprice,
                            vendorBidPrice: $scope.revvendorBidPrice,

                            //freightCharges: $scope.revfreightCharges,
                            //freightChargesTaxPercentage: $scope.freightChargesTaxPercentage,
                            //freightChargesWithTax: $scope.freightChargesWithTax,

                            //packingCharges: $scope.revpackingCharges,
                            //packingChargesTaxPercentage: $scope.packingChargesTaxPercentage,
                            //packingChargesWithTax: $scope.revpackingChargesWithTax,

                            //installationCharges: $scope.revinstallationCharges,
                            //installationChargesTaxPercentage: $scope.installationChargesTaxPercentage,
                            //installationChargesWithTax: $scope.revinstallationChargesWithTax,

                            revfreightCharges: $scope.revfreightCharges,
                            revfreightChargesWithTax: $scope.revfreightChargesWithTax,

                            revpackingCharges: $scope.revpackingCharges,
                            revpackingChargesWithTax: $scope.revpackingChargesWithTax,

                            revinstallationCharges: $scope.revinstallationCharges,
                            revinstallationChargesWithTax: $scope.revinstallationChargesWithTax,


                            //itemRevFreightCharges: $scope.itemRevFreightCharges
                        };
                    }

                    requirementHub.invoke('MakeBid', params, function (req) {

                        if (req.errorMessage == '') {
                            $scope.makeABidDisable = false;

                            swal("Thanks !", "Your bidding process has been successfully updated", "success");

                            if ($scope.auctionItem.isUnitPriceBidding == 1) {
                                if ($scope.items.revinstallationCharges == null || $scope.items.revinstallationCharges == '' || $scope.items.revinstallationCharges == undefined || isNaN($scope.items.revinstallationCharges)) {
                                    $scope.items.revinstallationCharges = 0;
                                }                                
                                $scope.items.itemsList.forEach(function (item, itemIndexs) {
                                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                            subItem.dateCreated = "/Date(1561000200000+0530)/";
                                            subItem.dateModified = "/Date(1561000200000+0530)/";
                                        })
                                    }
                                })
                                auctionsService.SaveRunningItemPrice($scope.items)
                                    .then(function (response) {
                                        if (response.errorMessage != "") {
                                            growlService.growl(response.errorMessage, "inverse");
                                        } else {
                                            $scope.recalculate('', 0, false);
                                        }
                                    });
                            }

                            $scope.reduceBidValue = "";
                            $("#makebidvalue").val("");
                            $("#reduceBidValue").val("");

                        } else {
                            if (req.errorMessage != '') {

                                $scope.getData();
                                $scope.makeABidDisable = false;
                                var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                                requirementHub.invoke('CheckRequirement', parties, function () {
                                    
                                    $scope.reduceBidValue = "";
                                    $("#makebidvalue").val("");
                                    $("#reduceBidValue").val("");

                                    var htmlContent = 'We have some one in this range <h3>' + req.errorMessage + ' </h3> Kindly Quote some other value.'

                                    swal("Cancelled", "", "error");
                                    $(".sweet-alert h2").html("oops...! Your Price is Too close to another Bid <br> We have some one in this range <h3 style='color:red'>" + req.errorMessage + " </h3> Kindly Quote some other value.");

                                });

                            } else {
                                $scope.makeABidDisable = false;
                                swal("Error!", req.errorMessage, "error");
                            }
                        }
                    });

                }

            }

            $scope.recalculate = function (subMethodName, receiverId, showswal) {

                if (subMethodName == null || subMethodName == '' || subMethodName == undefined) {
                    subMethodName = '';
                }

                if (receiverId >= 0) {

                } else {
                    receiverId = -1;
                }

                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken() + "$" + subMethodName + "$" + receiverId;
                $scope.invokeSignalR('CheckRequirement', parties);
                if (showswal) {
                    swal("done!", "a refresh command has been sent to everyone.", "success");
                }
            }

            $scope.setFields = function () {
                if ($scope.auctionItem.status == "CLOSED") {
                    $scope.mactrl.skinSwitch('green');
                    if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess)) {
                        $scope.errMsg = "Negotiation has been completed. You can generate the Purchase Order by pressing the button below.";
                    }
                    else {
                        $scope.errMsg = "Negotiation has completed.";
                    }
                    $scope.showStatusDropDown = false;
                    $scope.showGeneratePOButton = true;
                } else if ($scope.auctionItem.status == "UNCONFIRMED" || $scope.auctionItem.status == "NOTSTARTED") {
                    $scope.mactrl.skinSwitch('teal');
                    $scope.errMsg = "Negotiation has not started yet.";
                    $scope.showStatusDropDown = false;
                    $scope.auctionStarted = false;
                    $scope.timeLeftMessage = "Negotiation Starts in: ";
                    $scope.startBtns = true;
                    $scope.customerBtns = false;
                } else if ($scope.auctionItem.status == "STARTED") {
                    $scope.mactrl.skinSwitch('orange');
                    $scope.errMsg = "Negotiation has started.";
                    $scope.showStatusDropDown = false;
                    $scope.auctionStarted = true;

                    // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION
                    $scope.timeLeftMessage = "Negotiation Ends in: ";
                    // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION

                    $scope.startBtns = false;
                    $scope.customerBtns = true;
                } else if ($scope.auctionItem.status == "DELETED") {
                    $scope.mactrl.skinSwitch('bluegray');
                    $scope.errMsg = "This requirement has been cancelled.";
                    $scope.showStatusDropDown = false;
                    $scope.isDeleted = true;
                } else if ($scope.auctionItem.status == "Negotiation Ended") {
                    $scope.mactrl.skinSwitch('bluegray');
                    $scope.errMsg = "Negotiation has been completed.";
                    $scope.showStatusDropDown = false;
                } else if ($scope.auctionItem.status == "Vendor Selected") {
                    $scope.mactrl.skinSwitch('bluegray');
                    if ($scope.auctionItem.isTabular) {
                        $scope.errMsg = "Please select vendors for all items in order to provide Purchase Order Information.";
                    } else {
                        $scope.errMsg = "Please click the button below to provide the Purchase Order information.";
                    }
                    $scope.showStatusDropDown = false;
                } else if ($scope.auctionItem.status == "PO Processing") {
                    $scope.mactrl.skinSwitch('lightblue');
                    $scope.errMsg = "The PO has been generated. Please find the PO here: ";
                    $scope.showStatusDropDown = false;
                } else {
                    $scope.mactrl.skinSwitch('lightblue');
                    $scope.showStatusDropDown = true;
                }
                if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess)) {
                    $scope.userIsOwner = true;
                    if ($scope.userIsOwner && !$rootScope.isUserEntitled(546654) && $scope.auctionItem.status == "STARTED") {
                        swal("Error!", "Live negotiation Access Denined", "error");
                        $state.go('home');
                    }
                    if ($scope.userIsOwner && !$rootScope.isUserEntitled(591159)) {
                        swal("Error!", "View Requirement Access Denined", "error");
                        $state.go('home');
                    }
                    $scope.options = ['PO Sent', 'Material Received', 'Payment Processing', 'Payment Released'];
                    $scope.options.push($scope.auctionItem.status);
                }
                var start = $scope.auctionItem.startTime.split('+')[0].split('(')[1];
                auctionsService.getdate()
                    .then(function (responseFromServer) {
                        var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                        $log.debug(dateFromServer);
                        var curDate = dateFromServer;

                        var myEpoch = curDate.getTime();
                        $scope.timeLeftMessage = "";
                        if (start > myEpoch) {
                            $scope.auctionStarted = false;
                            $scope.timeLeftMessage = "Negotiation Starts in: ";
                            $scope.startBtns = true;
                            $scope.customerBtns = false;
                        } else {
                            $scope.auctionStarted = true;

                            // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION
                            $scope.timeLeftMessage = "Negotiation Ends in: ";
                            // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION

                            $scope.startBtns = false;
                            $scope.customerBtns = true;
                        }
                        if ($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId() && !$scope.auctionItem.customerReqAccess) {
                            $scope.startBtns = false;
                            $scope.customerBtns = false;
                        }
                        if ($scope.auctionItem.timeLeft == null || $scope.auctionItem.timeLeft < 1) {
                            $scope.showTimer = false;
                            $scope.disableButtons();
                        } else {
                            $scope.showTimer = true;
                        }
                       
                        // // #INTERNATIONALIZATION-0-2019-02-12
                        $scope.auctionItem.postedOn = userService.toLocalDate($scope.auctionItem.postedOn);

                        var minPrice = 0;
                        if ($scope.auctionItem.status == "NOTSTARTED") {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].initialPrice : 0;
                        } else {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].runningPrice : 0;
                        }
                        //$scope.auctionItem.deliveryTime = $scope.auctionItem.deliveryTime.split("GMT")[0];
                        for (var i in $scope.auctionItem.auctionVendors) {

                            $scope.nonParticipatedMsg = '';
                            $scope.quotationRejecteddMsg = '';
                            $scope.quotationNotviewedMsg = '';
                            $scope.revQuotationRejecteddMsg = '';
                            $scope.revQuotationNotviewedMsg = '';
                            $scope.quotationApprovedMsg = '';
                            $scope.revQuotationApprovedMsg = '';

                            var vendor = $scope.auctionItem.auctionVendors[i]

                            if (vendor.vendorID == userService.getUserId() && vendor.quotationUrl == "") {
                                $scope.quotationStatus = false;
                                $scope.quotationUploaded = false;
                            } else {
                                $scope.quotationStatus = true;
                                if ($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId() && !$scope.auctionItem.customerReqAccess) {
                                    $scope.quotationUploaded = true;
                                }
                                $scope.quotationUrl = vendor.quotationUrl;
                                $scope.revquotationUrl = vendor.revquotationUrl;
                                $scope.vendorQuotedPrice = vendor.runningPrice;
                            }
                            if (i == 0 && vendor.initialPrice != 0) {
                                minPrice = vendor.initialPrice;
                            } else {
                                if (vendor.initialPrice < minPrice && vendor.initialPrice != 0) {
                                    minPrice = vendor.initialPrice;
                                }
                            }
                            $scope.vendorInitialPrice = minPrice;
                            var runningMinPrice = 0;
                            if ($scope.auctionItem.auctionVendors[i].runningPrice > 0 && $scope.auctionItem.auctionVendors[i].runningPrice < $scope.vendorInitialPrice) {
                                runningMinPrice = $scope.auctionItem.auctionVendors[i].runningPrice;
                            }
                            //$scope.auctionItem.minPrice = runningMinPrice;
                            if ($scope.auctionItem.auctionVendors[i].runningPrice == 0) {
                                $scope.auctionItem.auctionVendors[i].runningPrice = 'NA';
                                $scope.auctionItem.auctionVendors[i].totalPriceIncl = 'NA';
                                $scope.auctionItem.auctionVendors[i].rank = 'NA';
                            } else {
                                $scope.vendorRank = vendor.rank;
                                if (vendor.rank == 1) {
                                    $scope.toprankerName = vendor.vendorName;
                                    if (userService.getUserId() == vendor.vendorID) {
                                        $scope.options = ['PO Accepted', 'Material Dispatched', 'Payment Acknowledged'];
                                        $scope.options.push($scope.auctionItem.status);
                                        if ($scope.auctionItem.status == "STARTED") {
                                            $scope.enableMakeBids = true;
                                        }
                                    }
                                }
                                //$scope.auctionItem.auctionVendors[i].totalPriceIncl = $scope.auctionItem.auctionVendors[i].runningPrice + ($scope.auctionItem.auctionVendors[i].runningPrice * $scope.auctionItem.auctionVendors[i].taxes) / 100;
                                $scope.auctionItem.auctionVendors[i].totalPriceIncl = $scope.auctionItem.auctionVendors[i].runningPrice;
                            }
                            if ($scope.auctionItem.auctionVendors[i].initialPrice == 0) {
                                $scope.auctionItem.auctionVendors[i].initialPrice = 'NA';
                                $scope.nonParticipatedMsg = 'You have missed an opportunity to participate in this Negotiation.';
                            }

                            if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 1) {
                                $scope.quotationRejecteddMsg = 'Your quotation rejected by the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == -1) {
                                $scope.quotationNotviewedMsg = 'Your quotation submitted to the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == 1 && $scope.auctionItem.status == "Negotiation Ended") {
                                $scope.revQuotationRejecteddMsg = 'Your Rev.Quotation rejected by the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == -1 && $scope.auctionItem.status == 'Negotiation Ended') {
                                $scope.revQuotationNotviewedMsg = 'Your Rev.Quotation submitted to the customer.';
                            }

                            if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 0) {
                                //$scope.quotationApprovedMsg = 'Your Quotation Approved by the customer.';
                                $scope.quotationApprovedMsg = 'Your quotation has been shortlisted for further process.';
                            }
                            if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == 0) {
                                $scope.revQuotationApprovedMsg = 'Your Rev.Quotation Approved by the customer.';
                            }

                        }
                        $scope.$broadcast('timer-set-countdown-seconds', $scope.auctionItem.timeLeft);
                        $('.datetimepicker').datetimepicker({
                            useCurrent: false,
                            icons: {
                                time: 'glyphicon glyphicon-time',
                                date: 'glyphicon glyphicon-calendar',
                                up: 'glyphicon glyphicon-chevron-up',
                                down: 'glyphicon glyphicon-chevron-down',
                                previous: 'glyphicon glyphicon-chevron-left',
                                next: 'glyphicon glyphicon-chevron-right',
                                today: 'glyphicon glyphicon-screenshot',
                                clear: 'glyphicon glyphicon-trash',
                                close: 'glyphicon glyphicon-remove'

                            },

                            minDate: curDate
                        });
                    })


                $scope.reduceBidAmountNote = 'Specify the value to be reduce from bid Amount.';
                //$scope.incTaxBidAmountNote = 'Please Enter the consolidate bid value including tax amount.';
                $scope.incTaxBidAmountNote = '';
                //$scope.noteForBidValue = 'NOTE : If you fill one field, other will be autocalculated.';
                $scope.noteForBidValue = '';
            };

            $scope.vendorBidPrice = 0;
            $scope.revvendorBidPrice = 0;
            $scope.auctionStarted = true;
            $scope.customerBtns = true;
            $scope.showVendorTable = true;
            $scope.quotationStatus = true;
            $scope.toprankerName = "";
            $scope.vendorRank = 0;
            $scope.quotationUrl = "";
            $scope.revquotationUrl = "";
            $scope.vendorBtns = false;
            $scope.vendorQuotedPrice = 0;
            $scope.startBtns = false;
            $scope.commentsvalidation = false;
            $scope.enableMakeBids = false;
            $scope.price = "";
            $scope.startTime = '';
            $scope.customerID = userService.getUserId();

            $scope.priceSwitch = 0;

            $scope.poDetails = {};

            $scope.bidHistory = {};

            $scope.GetBidHistory = function () {
                auctionsService.GetBidHistory({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.bidHistory = response;
                    });
            }

            $scope.vendorID = 0;

            $scope.validity = '';

            if ($scope.auctionItem.urgency == 'Critical (Will be Closed Today)') {
                $scope.validity = '1 Day';
            }
            if ($scope.auctionItem.urgency == 'High (Will be Closed in 2 Days)') {
                $scope.validity = '2 Days';
            }
            if ($scope.auctionItem.urgency == 'Medium (Will be Closed in 5 Days)') {
                $scope.validity = '5 Days';
            }
            if ($scope.auctionItem.urgency == 'Low (Will Take 10-15 Days Time)') {
                $scope.validity = '10-15 Days';
            }
            $scope.warranty = 'As Per OEM';
            $scope.duration = $scope.auctionItem.deliveryTime;
            $scope.payment = $scope.auctionItem.paymentTerms;
            $scope.gstNumber = $scope.auctionItem.gstNumber;
            $scope.isQuotationRejected = -1;
            $scope.starttimecondition1 = 0;
            $scope.starttimecondition2 = 0;
            $scope.revQuotationUrl1 = 0;
            $scope.L1QuotationUrl = 0;
            $scope.notviewedcompanynames = '';
            $scope.starreturn = false;
            $scope.customerListRequirementTerms = [];
            $scope.customerDeliveryList = [];
            $scope.customerPaymentlist = [];
            $scope.listTerms = [];
            $scope.listRequirementTerms = [];
            $scope.deliveryRadio = false;
            $scope.deliveryList = [];
            $scope.paymentRadio = false;
            $scope.paymentlist = [];
            $scope.vendorsFromPRM = 1;
            $scope.vendorsFromSelf = 2;
            $scope.totalprice = 0;
            $scope.taxs = 0;
            $scope.vendorTaxes = 0;
            $scope.discountAmount = 0;
            $scope.totalpriceinctaxfreight = 0;
            $scope.vendorBidPriceWithoutDiscount = 0;
            $scope.revtotalprice = 0;
            $scope.revtaxs = 0;
            $scope.revvendorTaxes = $scope.vendorTaxes;
            $scope.revtotalpriceinctaxfreight = 0;
            $scope.priceValidationsVendor = '';
            $scope.calculatedSumOfAllTaxes = 0;
            $scope.taxValidation = false;
            $scope.ItemPriceValidation = false;
            $scope.discountfreightValidation = false;
            $scope.gstValidation = false;
            $scope.freightCharges = 0;
            $scope.revfreightCharges = 0;
            $scope.packingCharges = 0;
            $scope.revpackingCharges = 0;
            $scope.installationCharges = 0;
            $scope.revinstallationCharges = 0;

            $scope.additionalSavings = 0;

            $scope.getpricecomparison = function () {
                auctionsService.getpricecomparison({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.priceCompObj = response;

                        $scope.totalItemsL1Price = 0;
                        $scope.totalItemsMinimunPrice = 0;
                        $scope.negotiationSavings = 0;
                        $scope.savingsByLeastBidder = 0;
                        $scope.savingsByItemMinPrice = 0;

                        if ($scope.priceCompObj && $scope.priceCompObj.priceCompareObject && $scope.priceCompObj.priceCompareObject.length > 0) {
                            for (var i = 0; i < $scope.priceCompObj.priceCompareObject.length; i++) {
                                $scope.totalItemsL1Price += $scope.priceCompObj.priceCompareObject[i].leastBidderPrice;
                                $scope.totalItemsMinimunPrice += $scope.priceCompObj.priceCompareObject[i].minPrice;
                            }
                        }

                        $scope.negotiationSavings = $scope.priceCompObj.requirement.savings;
                        $scope.savingsByLeastBidder = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsL1Price;
                        $scope.savingsByItemMinPrice = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsMinimunPrice;

                        $scope.additionalSavings = 0;

                        $scope.additionalSavings = $scope.totalItemsL1Price - $scope.totalItemsMinimunPrice;

                    });
            };

            $scope.GetRequirementTerms = function () {
                auctionsService.GetRequirementTerms(userService.getUserId(), $scope.reqId, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.listRequirementTerms = response;
                            $scope.listRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }

                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });
                        }
                        else {

                            $scope.customerListRequirementTerms.forEach(function (item, index) {

                                var obj = {
                                    currentTime: item.currentTime,
                                    errorMessage: item.errorMessage,
                                    isRevised: item.isRevised,
                                    refReqTermID: item.reqTermsID,
                                    reqID: item.reqID,
                                    reqTermsDays: item.reqTermsDays,
                                    reqTermsID: 0,
                                    reqTermsPercent: item.reqTermsPercent,
                                    reqTermsType: item.reqTermsType,
                                    sessionID: item.sessionID,
                                    userID: item.userID
                                };

                                $scope.listRequirementTerms.push(obj);

                            });

                            $scope.listRequirementTerms.forEach(function (item, index) {

                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }

                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });
                        }

                    });
            };

            $scope.GetRequirementTermsCustomer = function () {
                auctionsService.GetRequirementTerms($scope.auctionItem.customerID, $scope.reqId, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {

                            $scope.customerListRequirementTerms = [];
                            $scope.customerDeliveryList = [];
                            $scope.customerPaymentlist = [];

                            $scope.customerListRequirementTerms = response;

                            $scope.customerListRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.customerDeliveryList.push(item);
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }
                                    else if (item.reqTermsDays == 0) {
                                        item.paymentType = '';
                                    }

                                    $scope.customerPaymentlist.push(item);
                                }
                            });
                        }
                    });
            };

            $scope.getUserCredentials = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getusercredentials?sessionid=' + userService.getUserToken() + "&userid=" + userService.getUserId(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {

                    $scope.CredentialsResponce = response.data;

                    $scope.CredentialsResponce.forEach(function (cred, credIndex) {
                        if (cred.fileType == "STN") {
                            $scope.gstNumber = cred.credentialID;
                        }
                    })

                }, function (result) {

                });
            };
            $scope.colspanDynamic = 11;
            $scope.getData = function (methodName, callerID) {
                auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        if (response) {

                            $scope.setAuctionInitializer(response, methodName, callerID);
                            auctionsService.GetCompanyConfiguration(response.custCompID, "ITEM_UNITS", userService.getUserToken())
                                .then(function (unitResponse) {
                                    $scope.companyItemUnits = unitResponse;
                                });
                        }
                    });
                //$scope.$broadcast('timer-start');
            };

            //Get Min reduction amount based on min quotation amount of 1%
            $scope.getMinReductionAmount = function () {
                if ($scope.auctionItem.minBidAmount > 0) {
                    $scope.NegotiationSettings.minReductionAmount = $scope.auctionItem.minBidAmount;
                    $scope.NegotiationSettings.rankComparision = $scope.auctionItem.minVendorComparision;
                }
                else {
                    var amount = $scope.NegotiationSettings.minReductionAmount;
                    if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                        var tempAuctionVendors = $scope.auctionItem.auctionVendors.filter(function (vendor) {
                            return (vendor.runningPrice > 0 && vendor.initialPrice > 0 && vendor.companyName != 'PRICE_CAP' && vendor.isQuotationRejected == 0);
                        });

                        var temp = _.minBy(tempAuctionVendors, 'initialPrice');
                        if (temp && temp.initialPrice && temp.initialPrice > 0) {
                            amount = temp.initialPrice * 0.005;
                        }
                    }
                    $scope.NegotiationSettings.minReductionAmount = amount;
                    $scope.NegotiationSettings.rankComparision = amount;

                }
            };


            $scope.CheckDisable = false;
            $scope.existingVendors = [];

            $scope.setAuctionInitializer = function (response, methodName, callerID) {
                
                $scope.auctionItem = response;

                $scope.auctionItemTemporary = response;
                //$scope.auctionItem.auctionVendors[0].INCO_TERMS

                if ($scope.auctionItemTemporary) {
                    $scope.auctionItemTemporary.emailLinkVendors = $scope.auctionItemTemporary.auctionVendors.filter(function (item) {
                        //item.listWorkflowsTemp.push($scope.workflowList);
                        item.listWorkflows = [];
                        if (item.isEmailSent == undefined || item.isEmailSent == '') {
                            item.isEmailSent = false;
                        } else {
                            item.isEmailSent = true;
                        }

                        if (item.companyName != 'PRICE_CAP')
                        {
                            $scope.existingVendors.push(item.vendorID);
                        }
                        
                        return item.companyName != 'PRICE_CAP';
                    });
                    
                    $scope.getWorkflows();

                }
                

                $scope.CB_END_TIME = $scope.auctionItem.CB_END_TIME;
                //#CB-0-2018-12-05
                if (!$scope.isCustomer && $scope.auctionItem && $scope.auctionItem.auctionVendors &&
                    $scope.auctionItem.auctionVendors.length > 0 && $scope.auctionItem.auctionVendors[0].revVendorTotalPriceCB > 0 && 
                    //$scope.auctionItem.auctionVendors[0].FREEZE_CB == false &&
                    $scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 &&
                    $scope.auctionItem.IS_CB_ENABLED == 1 && 
                    $scope.auctionItem.IS_CB_COMPLETED == false) {

                   
                    $scope.goToCbVendor($scope.auctionItem.auctionVendors[0].vendorID)
                };

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0
                    && $scope.auctionItem.auctionVendors[0].isQuotationRejected == 0) {
                    $scope.colspanDynamic = 14;
                } else {
                    $scope.colspanDynamic = 11;
                }
                //ack code start
                if ($scope.auctionItem.auctionVendors.length > 0) {
                    if ($scope.auctionItem.auctionVendors[0].quotationUrl == '' || $scope.auctionItem.auctionVendors[0].quotationUrl == undefined) {
                        $scope.auctionItem.auctionVendors[0].isVendAckChecked = false;
                    } else {
                        $scope.auctionItem.auctionVendors[0].isVendAckChecked = true;
                        $scope.CheckDisable = true;
                    }
                }
                
                    //ack code end


                $scope.IS_CB_ENABLED = $scope.auctionItem.IS_CB_ENABLED;
                $scope.IS_CB_NO_REGRET = $scope.auctionItem.IS_CB_NO_REGRET;
                $scope.auctionItem.isUOMDifferent = false;
                if (callerID == userService.getUserId() || callerID == undefined) {
                    $scope.auctionItemVendor = $scope.auctionItem;
                    if ($scope.auctionItemVendor.listRequirementItems != null) {

                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                            item.isEdit = true;
                            
                            if (item.cGst > 0 || item.sGst > 0) {
                                $scope.IGSTFields = true;
                                $scope.CSGSTFields = false;
                            } else if (item.iGst > 0) {
                                $scope.IGSTFields = false;
                                $scope.CSGSTFields = true;
                            }
                            item.oldRevUnitPrice = item.revUnitPrice;
                            item.TemperoryRevUnitPrice = 0;
                            item.TemperoryRevUnitPrice = item.revUnitPrice;
                            item.vendorUnits = (item.vendorUnits == '' || item.vendorUnits == undefined) ? item.productQuantityIn : item.vendorUnits;
                            if (item.productQuantityIn.toUpperCase() != item.vendorUnits.toUpperCase()) {
                                $scope.auctionItem.isUOMDifferent = true;
                            }
                        });
                        
                    }
                }
                $scope.isCurrency = false;
                $scope.fieldValidation();
                //$scope.reductionPricePercent();
                
                $scope.notviewedcompanynames = '';

                $scope.auctionItem.listRequirementItems.forEach(function (item, itemIndex) {
                    if (item.productQuotationTemplateJson && item.productQuotationTemplateJson != '' && item.productQuotationTemplateJson != null && item.productQuotationTemplateJson != undefined) {
                        item.productQuotationTemplateArray = JSON.parse(item.productQuotationTemplateJson);
                        item.productQuotationTemplateArray = item.productQuotationTemplateArray.filter(function (prodObj) {
                            if (prodObj) {
                                return prodObj;
                            }
                        });
                    } else {
                        item.productQuotationTemplateArray = [];
                    }
                });

                $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                    if (!item.bestPrice) {
                        item.bestPrice = 0;
                    }

                    item.quotationRejectedComment = item.quotationRejectedComment.replace("Approved-", "");
                    item.quotationRejectedComment = item.quotationRejectedComment.replace("Reject Comments-", "");
                    if (item.quotationRejectedComment) {
                        item.quotationRejectedComment = item.quotationRejectedComment.trim();
                    }

                    item.revQuotationRejectedComment = item.revQuotationRejectedComment.replace("Approved-", "");
                    item.revQuotationRejectedComment = item.revQuotationRejectedComment.replace("Reject Comments-", "");
                    if (item.revQuotationRejectedComment) {
                        item.revQuotationRejectedComment = item.revQuotationRejectedComment.trim();
                    }


                    if ($scope.auctionItem.LAST_BID_ID == item.vendorID &&
                        ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED' || $scope.auctionItem.status == 'UNCONFIRMED')) {
                        item.vendorColorStyle = { 'background-color': 'lightgreen' };
                    }
                    else if (item.FREEZE_CB == true) {
                        item.vendorColorStyle = { 'background-color': 'rgb(255, 228, 225)' };
                    }
                    else if (item.VEND_FREEZE_CB == true) {
                        item.vendorColorStyle = { 'background-color': 'rgb(176, 224, 230)' };
                        //rgb(176, 224, 230)
                        //#B0E0E6
                    }
                    else {
                        item.vendorColorStyle = {};
                    }

                    item.isQuotationRejectedDB = item.isQuotationRejected;
                    item.isRevQuotationRejectedDB = item.isRevQuotationRejected;

                    item.bestPrice = 0;
                    item.hasRegretItems = false;
                    item.listRequirementItems.forEach(function (item2, index) {
                        //Core products only
                        if (item2.isCoreProductCategory && $scope.auctionItem.lotId <= 0) {
                            item.bestPrice += (item2.revUnitPrice * item2.productQuantity);
                        }

                        if (item2.isRegret) {
                            item.hasRegretItems = true;
                        }

                        if (item2.productQuantityIn.toUpperCase() != item2.vendorUnits.toUpperCase()) {
                            item.isUOMDifferent = true;
                        }

                        if (item2.productQuotationTemplateJson && item2.productQuotationTemplateJson != '' && item2.productQuotationTemplateJson != null && item2.productQuotationTemplateJson != undefined) {
                            item2.productQuotationTemplateArray = JSON.parse(item2.productQuotationTemplateJson);
                        }
                        else {
                            item2.productQuotationTemplateArray = [];
                        }

                    });

                    if (!$scope.multipleAttachmentsList) {
                        $scope.multipleAttachmentsList = [];
                    }
                    if (!item.multipleAttachmentsList) {
                        item.multipleAttachmentsList = [];
                    }
                    //item.multipleAttachmentsArray = item.multipleAttachments;
                    if (item.multipleAttachments != '' && item.multipleAttachments != null && item.multipleAttachments != undefined) {
                        var multipleAttachmentsList = item.multipleAttachments.split(',');
                        item.multipleAttachmentsList = [];
                        $scope.multipleAttachmentsList = [];
                        multipleAttachmentsList.forEach(function (att, index) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: att
                            };

                            item.multipleAttachmentsList.push(fileUpload);
                            $scope.multipleAttachmentsList.push(fileUpload);
                        });
                    }

                    if (item.quotationUrl != '' && item.isQuotationRejected == -1) {
                        $scope.Loding = false;
                        $scope.notviewedcompanynames += item.companyName + ', ';
                        $scope.starreturn = true;
                    }

                    if (!$scope.isCustomer && (item.quotationUrl == '' || item.quotationUrl == null || item.quotationUrl == undefined)) {
                        $scope.getUserCredentials();
                    }
                });

                $scope.selectedcurr = '';
                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                    $scope.selectedcurr = $scope.auctionItem.auctionVendors[0].selectedVendorCurrency;
                }


                if (!$scope.auctionItem.multipleAttachments) {
                    $scope.auctionItem.multipleAttachments = [];
                }
                $scope.auctionItem.attFile = response.attachmentName;
                if ($scope.auctionItem.attFile != '' && $scope.auctionItem.attFile != null && $scope.auctionItem.attFile != undefined) {


                    var attchArray = $scope.auctionItem.attFile.split(',');

                    attchArray.forEach(function (att, index) {

                        var fileUpload = {
                            fileStream: [],
                            fileName: '',
                            fileID: att
                        };

                        $scope.auctionItem.multipleAttachments.push(fileUpload);
                    });
                }

                $scope.notviewedcompanynames = $scope.notviewedcompanynames.substring(0, $scope.notviewedcompanynames.length - 2);

                if ($scope.auctionItem.listRequirementTaxes.length == 0) {
                    $scope.auctionItem.listRequirementTaxes = $scope.listRequirementTaxes;
                }
                else {
                    $scope.listRequirementTaxes = $scope.auctionItem.listRequirementTaxes;
                }

                $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;

                $scope.reqTaxSNo = $scope.auctionItem.taxSNoCount;

                $scope.NegotiationSettings = $scope.auctionItem.NegotiationSettings;
                var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
                $scope.days = parseInt(duration[0]);
                duration = duration[1];
                duration = duration.split(":", 4);
                $scope.hours = parseInt(duration[0]);
                $scope.mins = parseInt(duration[1]);

                $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';

                if (!response.auctionVendors || response.auctionVendors.length == 0) {
                    $scope.auctionItem.auctionVendors = [];
                    $scope.auctionItem.description = "";
                }

                // // #INTERNATIONALIZATION-0-2019-02-12
                $scope.auctionItem.quotationFreezTime = userService.toLocalDate($scope.auctionItem.quotationFreezTime);
                $scope.checkQuotationFreezeTime();
                $scope.auctionItem.expStartTime = userService.toLocalDate($scope.auctionItem.expStartTime);
                if (($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length != 0) && (callerID == userService.getUserId() || callerID == undefined)) {
                    $scope.totalprice = $scope.auctionItem.auctionVendors[0].initialPriceWithOutTaxFreight;
                    $scope.vendorTaxes = $scope.auctionItem.auctionVendors[0].taxes;
                    $scope.vendorBidPrice = $scope.auctionItem.auctionVendors[0].totalInitialPrice;
                    
                    $scope.discountAmount = $scope.auctionItem.auctionVendors[0].discount;
                    $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice + $scope.discountAmount;
                    $scope.vendorID = $scope.auctionItem.auctionVendors[0].vendorID;
                    $scope.warranty = $scope.auctionItem.auctionVendors[0].warranty;
                    $scope.duration = $scope.auctionItem.auctionVendors[0].duration;
                    $scope.payment = $scope.auctionItem.auctionVendors[0].payment;
                    $scope.gstNumber = $scope.auctionItem.auctionVendors[0].gstNumber;
                    $scope.validity = $scope.auctionItem.auctionVendors[0].validity;
                    $scope.otherProperties = $scope.auctionItem.auctionVendors[0].otherProperties;

                    $scope.revvendorTaxes = $scope.auctionItem.auctionVendors[0].taxes;
                    $scope.isQuotationRejected = $scope.auctionItem.auctionVendors[0].isQuotationRejected;
                    $scope.quotationRejectedComment = $scope.auctionItem.auctionVendors[0].quotationRejectedComment;
                    $scope.revQuotationRejectedComment = $scope.auctionItem.auctionVendors[0].revQuotationRejectedComment;

                    $scope.freightCharges = $scope.auctionItem.auctionVendors[0].freightCharges;
                    $scope.freightChargesTaxPercentage = $scope.auctionItem.auctionVendors[0].freightChargesTaxPercentage;
                    $scope.freightChargesWithTax = $scope.auctionItem.auctionVendors[0].freightChargesWithTax;

                    $scope.packingCharges = $scope.auctionItem.auctionVendors[0].packingCharges;
                    $scope.packingChargesTaxPercentage = $scope.auctionItem.auctionVendors[0].packingChargesTaxPercentage;
                    $scope.packingChargesWithTax = $scope.auctionItem.auctionVendors[0].packingChargesWithTax;

                    $scope.installationCharges = $scope.auctionItem.auctionVendors[0].installationCharges;
                    $scope.installationChargesTaxPercentage = $scope.auctionItem.auctionVendors[0].installationChargesTaxPercentage;
                    $scope.installationChargesWithTax = $scope.auctionItem.auctionVendors[0].installationChargesWithTax;

                    $scope.revfreightCharges = $scope.auctionItem.auctionVendors[0].revfreightCharges;
                    $scope.TemperoryRevfreightCharges = $scope.revfreightCharges;
                    $scope.freightChargesWithTax = $scope.auctionItem.auctionVendors[0].freightChargesWithTax;

                    $scope.revpackingCharges = $scope.auctionItem.auctionVendors[0].revpackingCharges;
                    $scope.TemperoryRevpackingCharges = $scope.revpackingCharges;
                    $scope.revpackingChargesWithTax = $scope.auctionItem.auctionVendors[0].revpackingChargesWithTax;

                    $scope.revinstallationCharges = $scope.auctionItem.auctionVendors[0].revinstallationCharges;
                    $scope.TemperoryRevinstallationCharges = $scope.revinstallationCharges;
                    $scope.revinstallationChargesWithTax = $scope.auctionItem.auctionVendors[0].revinstallationChargesWithTax;

                    $scope.resetValues();

                    //$("#totalprice").val(0);
                    //$("#revtotalprice").val(0);
                    //if (callerID == userService.getUserId() || callerID == undefined) {
                    //    $("#packingCharges").val($scope.packingCharges);
                    //    $("#packingChargesTaxPercentage").val($scope.packingChargesTaxPercentage);
                    //    $("#packingChargesWithTax").val($scope.packingChargesWithTax);
                    //    $("#revpackingCharges").val($scope.revpackingCharges);
                    //    $("#revpackingChargesWithTax").val($scope.revpackingChargesWithTax);

                    //    $("#installationCharges").val($scope.installationCharges);
                    //    $("#installationChargesTaxPercentage").val($scope.installationChargesTaxPercentage);
                    //    $("#installationChargesWithTax").val($scope.installationChargesWithTax);
                    //    $("#revinstallationCharges").val($scope.revinstallationCharges);
                    //    $("#revinstallationChargesWithTax").val($scope.revinstallationChargesWithTax);

                    //    $("#freightCharges").val($scope.freightCharges);
                    //    $("#freightChargesTaxPercentage").val($scope.freightChargesTaxPercentage);
                    //    $("#freightChargesWithTax").val($scope.freightChargesWithTax);
                    //    $("#revfreightCharges").val($scope.revfreightCharges);
                    //    $("#revfreightChargesWithTax").val($scope.revfreightChargesWithTax);
                    //}


                    //$scope.revfreightCharges = $scope.auctionItem.auctionVendors[0].revfreightCharges;
                    //$("#revfreightCharges").val($scope.auctionItem.auctionVendors[0].revfreightCharges);
                    //$scope.TemperoryRevfreightCharges = $scope.revfreightCharges;


                    //$scope.revinstallationCharges = $scope.auctionItem.auctionVendors[0].revinstallationCharges;
                    //$("#revinstallationCharges").val($scope.auctionItem.auctionVendors[0].revinstallationCharges);
                    //$scope.TemperoryRevinstallationCharges = $scope.revinstallationCharges;


                    //$scope.revfreightCharges = $scope.auctionItem.auctionVendors[0].revfreightCharges;
                    //$("#revfreightCharges").val($scope.auctionItem.auctionVendors[0].revfreightCharges);
                    //$scope.TemperoryRevfreightCharges = $scope.revfreightCharges;





                    $scope.selectedVendorCurrency = $scope.auctionItem.auctionVendors[0].selectedVendorCurrency;

                    if ($scope.auctionItem.urgency == 'Critical (Will be Closed Today)' && $scope.validity == '') {
                        $scope.validity = '1 Day';
                    }
                    else if ($scope.auctionItem.urgency == 'High (Will be Closed in 2 Days)' && $scope.validity == '') {
                        $scope.validity = '2 Days';
                    }
                    else if ($scope.auctionItem.urgency == 'Medium (Will be Closed in 5 Days)' && $scope.validity == '') {
                        $scope.validity = '5 Days';
                    }
                    else if ($scope.auctionItem.urgency == 'Low (Will Take 10-15 Days Time)' && $scope.validity == '') {
                        $scope.validity = '10-15 Days';
                    }
                    if ($scope.warranty == '') {
                        $scope.warranty = 'As Per OEM';
                    }
                    if ($scope.duration == '') {
                        //$scope.duration = new moment($scope.auctionItem.deliveryTime).format("DD-MM-YYYY");
                        $scope.duration = $scope.auctionItem.deliveryTime;
                    }
                    if ($scope.payment == '') {
                        $scope.payment = $scope.auctionItem.paymentTerms;
                    }

                    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                        if (item.productQuantityIn.toUpperCase() != item.vendorUnits.toUpperCase()) {
                            $scope.auctionItem.isUOMDifferent = true;
                        }

                        item.Gst = item.cGst + item.sGst + item.iGst;

                        if ($scope.auctionItem.isDiscountQuotation == 2) {


                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                item.revCostPrice = item.costPrice;
                                item.revmarginAmount = item.marginAmount;
                            }

                            item = $scope.handlePrecision(item, 2);
                            item.Gst = item.cGst + item.sGst;
                            //item.costPrice = (1 + (item.Gst / 100));                            
                            item = $scope.handlePrecision(item, 2);
                            //item.costPrice = item.costPrice + (item.unitDiscount / 100);
                            //item.costPrice = item.unitMRP / item.costPrice;
                            //item.revCostPrice = item.unitMRP / (1 + (item.Gst / 100) + (item.revUnitDiscount / 100));


                            //item.costPrice = 100 * item.unitMRP / (100 + item.Gst)*(1 + (item.unitDiscount / 100));
                            //item.revCostPrice = 100 * item.unitMRP / (100 + item.Gst) * (1 + (item.revUnitDiscount / 100));


                            item.costPrice = (item.unitMRP * 100 * 100) / ((item.unitDiscount * 100) + 10000 + (item.unitDiscount * item.Gst) + (item.Gst * 100));
                            item.revCostPrice = (item.unitMRP * 100 * 100) / ((item.revUnitDiscount * 100) + 10000 + (item.revUnitDiscount * item.Gst) + (item.Gst * 100));


                            item.netPrice = item.costPrice * (1 + item.Gst / 100);
                            item.revnetPrice = item.revCostPrice * (1 + item.Gst / 100);

                            item.marginAmount = item.unitMRP - item.netPrice;
                            item.revmarginAmount = item.unitMRP - item.revnetPrice;

                            item = $scope.handlePrecision(item, 2);
                        }
                        if (!item.selectedVendorID || item.selectedVendorID == 0) {
                            $scope.allItemsSelected = false;
                        }
                        if ($scope.auctionItem.status == "Negotiation Ended" || $scope.auctionItem.status == "Vendor Selected" || $scope.auctionItem.status == "PO Processing") {
                            if (item.selectedVendorID > 0) {
                                var vendorArray = $filter('filter')($scope.auctionItem.auctionVendors, { vendorID: item.selectedVendorID });
                                if (vendorArray.length > 0) {
                                    item.selectedVendor = vendorArray[0];
                                }
                            } else {
                                item.selectedVendor = $scope.auctionItem.auctionVendors[0];
                            }
                        }
                    })
                    if (!$scope.auctionItem.isStopped && $scope.auctionItem.timeLeft > 0) {
                        $scope.disableAddButton = false;
                    }

                    //$scope.revtotalprice = $scope.auctionItem.auctionVendors[0].revPrice;                       
                    //$scope.revvendorBidPrice = $scope.auctionItem.auctionVendors[0].revVendorTotalPrice;

                    // $scope.vendorTaxes = $scope.auctionItem.auctionVendors[0].taxes;


                    
                    



                    //$scope.TemperoryRevtotalprice = $scope.revtotalprice;
                    //$scope.TemperoryRevvendorBidPrice = $scope.revvendorBidPrice

                    if (callerID == userService.getUserId() || callerID == undefined) {
                        $scope.revtotalprice = $scope.auctionItem.auctionVendors[0].revPrice;

                        $scope.revtotalprice = $scope.precisionRound(parseFloat($scope.revtotalprice), 2);

                        $scope.revvendorBidPrice = $scope.auctionItem.auctionVendors[0].revVendorTotalPrice;
                    }

                    $scope.starttimecondition1 = $scope.auctionItem.auctionVendors[0].isQuotationRejected;


                    $scope.revQuotationUrl1 = $scope.auctionItem.auctionVendors[0].revquotationUrl;

                    if ($scope.auctionItem.auctionVendors[0].companyName == 'PRICE_CAP') {
                        $scope.revQuotationUrl1 = $scope.auctionItem.auctionVendors[1].revquotationUrl;
                    }

                    $scope.L1QuotationUrl = $scope.auctionItem.auctionVendors[0].quotationUrl;


                    //= $filter('filter')($scope.auctionItem.auctionVendors, {revquotationUrl > ''});
                    //$scope.RevQuotationfirstvendor 
                   // $scope.isRejectedPOEnable = true;
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        if (item.revquotationUrl != '') {
                            $scope.RevQuotationfirstvendor = item.revquotationUrl;
                        }
                        //if (item.isRevQuotationRejected == 1) {
                        //    $scope.isRejectedPOEnable = false;
                        //} else {
                        //    $scope.isRejectedPOEnable = true;
                        //}

                    })

                }

                if ($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length >= 2) {
                    $scope.starttimecondition2 = $scope.auctionItem.auctionVendors[1].isQuotationRejected;
                }
                //$scope.vendorID = $scope.auctionItem.auctionVendors[0].vendorID;

                $scope.participatedVendors = $scope.auctionItem.auctionVendors.filter(function (vendor) {
                    return (vendor.runningPrice > 0 && vendor.companyName != 'PRICE_CAP');
                });

                $scope.revisedParticipatedVendors = $scope.participatedVendors.filter(function (vendor) {
                    return (vendor.revquotationUrl != null && vendor.revquotationUrl != '' && vendor.companyName != 'PRICE_CAP');
                })

                $scope.revisedApprovedVendors = $scope.participatedVendors.filter(function (vendor) {
                    return (vendor.revquotationUrl != null && vendor.revquotationUrl != '' && vendor.companyName != 'PRICE_CAP' && vendor.isRevQuotationRejected == 0);
                })


                if ($scope.auctionItem.status != 'DELETED' && $scope.auctionItem.status != 'UNCONFIRMED' && $scope.auctionItem.status != 'STARTED' && $scope.auctionItem.status != 'NOTSTARTED' && $scope.auctionItem.status != 'Negotiation Ended') {
                    auctionsService.getpodetails({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                        .then(function (response) {
                            $scope.poDetails = response;
                            //$scope.updatedeliverydateparams.date = $scope.poDetails.expectedDelivery;
                            if (response != undefined) {
                                //var date = $scope.poDetails.expectedDelivery.split('+')[0].split('(')[1];
                                var date1 = moment($scope.poDetails.expectedDelivery).format('DD/MM/YYYY  HH:mm');
                                $scope.updatedeliverydateparams.date = date1;

                                //var date1 = $scope.poDetails.paymentScheduleDate.split('+')[0].split('(')[1];
                                //var newDate1 = new Date(parseInt(parseInt(date1)));
                                var date2 = moment($scope.poDetails.expPaymentDate).format('DD/MM/YYYY  HH:mm');
                                $scope.updatepaymentdateparams.date = date2;
                                if ($scope.updatepaymentdateparams.date == '31/12/9999') {
                                    $scope.updatepaymentdateparams.date = 'Payment Date';
                                }
                            }
                        });
                }

                //  $scope.auctionItem.description = $scope.auctionItem.description.replace("\u000a", "\n")
                $scope.description = $scope.auctionItem.description.replace(/\u000a/g, "</br>");
                $scope.deliveryLocation = $scope.auctionItem.deliveryLocation.replace(/\u000a/g, "</br>");
                $scope.paymentTerms = $scope.auctionItem.paymentTerms.replace(/\u000a/g, "</br>");
                $scope.deliveryTime = $scope.auctionItem.deliveryTime.replace(/\u000a/g, "</br>");


                var id = parseInt(userService.getUserId());
                var result = $scope.auctionItem.auctionVendors.filter(function (obj) {
                    return obj.vendorID == id;
                });
                if (id != $scope.auctionItem.customerID && id != $scope.auctionItem.superUserID && !$scope.auctionItem.customerReqAccess && result.length == 0) {
                    swal("Access denied", "You do not have access to this requirement because you are not part of this requirements process.", 'error');
                    $state.go('home');
                } else {
                    $scope.setFields();
                    //auctionsService.getcomments({ "reqid": $stateParams.Id, "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    //    .then(function (response) {
                    //        $scope.Comments = response;
                    //    });

                }


                $scope.quotationApprovedColor = {};
                $scope.quotationNotApprovedColor = {};

                if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 && $scope.auctionItem.isUnitPriceBidding == 1) {
                    $scope.quotationApprovedColor = {
                        'background-color': '#f5b2b2'
                    };
                    $scope.quotationNotApprovedColor = {};
                } else if ($scope.auctionItem.isUnitPriceBidding == 1) {
                    $scope.quotationNotApprovedColor = {
                        'background-color': '#f5b2b2'
                    };
                    $scope.quotationApprovedColor = {};
                }

                //$scope.getRevTotalPrice($scope.revfreightCharges, $scope.revtotalprice);
                $scope.getMinReductionAmount();
                $scope.getpricecomparison();

            };

            $scope.getData();

            if ($scope.isCustomer) {
                auctionsService.GetIsAuthorized(userService.getUserId(), $scope.reqId, userService.getUserToken())
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                            $state.go('home');
                            return false;
                        }
                    });
            }

            $scope.GetBidHistory();

            $scope.AmountSaved = 0;

            $scope.selectItemVendor = function (itemID, vendorID) {
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: userService.getUserId(),
                    itemID: itemID,
                    vendorID: vendorID,
                    sessionID: userService.getUserToken()
                };
                auctionsService.itemwiseselectvendor(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            var index = -1;
                            for (var i = 0, len = $scope.auctionItemVendor.listRequirementItems.length; i < len; i++) {
                                if ($scope.auctionItemVendor.listRequirementItems[i].itemID === itemID) {
                                    index = i;
                                    $scope.auctionItemVendor.listRequirementItems[i].selectedVendorID = vendorID;
                                    break;
                                }
                                if ($scope.auctionItemVendor.listRequirementItems[i].selectedVendor == 0) {
                                    $scope.allItemsSelected = false;
                                } else {
                                    $scope.allItemsSelected = true;
                                }
                            }
                            swal("Success!", "This item has been assigned to Vendor", "success");
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })
            }

            $scope.handlePrecision = function (item, precision) {

                //item.sGst = $scope.precisionRound(parseFloat(item.sGst), precision);
                //item.cGst = $scope.precisionRound(parseFloat(item.cGst), precision);
                //item.Gst = $scope.precisionRound(parseFloat(item.Gst), precision);
                //item.unitMRP = $scope.precisionRound(parseFloat(item.unitMRP), precision);
                item.unitDiscount = $scope.precisionRound(parseFloat(item.unitDiscount), precision + 6);
                item.revUnitDiscount = $scope.precisionRound(parseFloat(item.revUnitDiscount), precision + 6);
                //item.costPrice = $scope.precisionRound(parseFloat(item.costPrice), precision);
                //item.revCostPrice = $scope.precisionRound(parseFloat(item.revCostPrice), precision);
                //item.netPrice = $scope.precisionRound(parseFloat(item.netPrice), precision);
                //item.revnetPrice = $scope.precisionRound(parseFloat(item.revnetPrice), precision);
                //item.marginAmount = $scope.precisionRound(parseFloat(item.marginAmount), precision);
                //item.revmarginAmount = $scope.precisionRound(parseFloat(item.revmarginAmount), precision);
                //$scope.totalprice = $scope.precisionRound(parseFloat($scope.totalprice), precision);
                //$scope.revtotalprice = $scope.precisionRound(parseFloat($scope.revtotalprice), precision);
                //return item;
                return item;
            };

            $scope.$watch('auctionItemVendor.listRequirementItems', function (oldVal, newVal) {
                $log.debug("items list has changed");
                if (!newVal) { return; }
                newVal.forEach(function (item, index) {
                    if (!item.selectedVendorID || item.selectedVendorID == 0) {
                        $scope.allItemsSelected = false;
                    }
                })
            })

            $scope.generatePDFonHTML = function () {
                auctionsService.getdate()
                    .then(function (response) {
                        var date = new Date(parseInt(response.substr(6)));
                        var obj = $scope.auctionItem;
                        $scope.POTemplate = "<div id='POTemplate' style='display:none;'><html><head><title>PRM360</title><style>.date{margin-left: 850px;}.to{margin-left: 250px;}.name{margin-left: 300px;}.sub{margin-left: 450px;}img{position: absolute; left: 750px; top:75px; z-index: -1;}</style></head><body><header><br><br><br><img src='acads360.jpg' width='50' height='50'><h1 align='center'>PRM360<img </h1></header><br><div class='date'><p><b>Date:</b> " + date + "</p><p><b>PO No:</b> " + obj.requirementID + "</p></div><div class='to'><p>To,</p><p><b>" + obj.CompanyName + ",</b></p><p><b>" + obj.deliveryLocation + ".</b></p></div><p class='name'><b>Hello </b> " + obj.auctionVendors[0].vendorName + "</p><p class='sub'><b>Sub:</b> " + obj.title + "</p><p align='center'><b>Bill of Material</b></p><table border='1' cellpadding='2' style='width:60%' align='center'><tr><th>Product Name</th><th>Description</th><th>Price</th></tr><tr><td>" + obj.title + "</td><td>" + obj.description + "</td><td>" + obj.price + "</td></tr></table><p class='to'><b>Terms & Conditions</b></p><div class='name'> <p>1. Payment : " + obj.paymentTerms + ".</p><p>2. Delivery : " + obj.deliveryLocation + ".</p><p>3. Tax : " + obj.taxes + ".</p></div><p class='to'><b>Billing and Shipping Address:</b></p><p class='to'>Savvy Associates, # 3-4-174/21/2, Radha Krishna Nagar, Attapur, Hyderguda, Hyderabad - 500048</p><p align=center>This is a system generated PO, henceforth sign and seal is not required.</p><br><footer class='to'>M/s. Savvy Associates, H.No: 3-4-174/21/2, Radha Krishna Nagar, Attapur, Hyderguda, Hyderabad – 48Contact,M: 91-9949245791.,<br>E: savvyassociates@gmail.com.<br><b>URL:</b> www.savvyassociates.com. </footer></body></html></div>";
                        var content = document.getElementById('content');
                        content.insertAdjacentHTML('beforeend', $scope.POTemplate);
                    })
            }

            $scope.generatePOasPDF = function (divName) {
                $scope.generatePDFonHTML();
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;

                if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
                    var popupWin = window.open('', '_blank', 'width=600,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                    popupWin.window.focus();
                    popupWin.document.write('<!DOCTYPE html><html><head>' +
                        '<link rel="stylesheet" type="text/css" href="style.css" />' +
                        '</head><body onload="window.print()"><div class="reward-body">' + printContents + '</div></html>');
                    popupWin.onbeforeunload = function (event) {
                        popupWin.close();
                        return '.\n';
                    };
                    popupWin.onabort = function (event) {
                        popupWin.document.close();
                        popupWin.close();
                    }
                } else {
                    var popupWin = window.open('', '_blank', 'width=800,height=600');
                    popupWin.document.open();
                    popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</html>');
                    popupWin.document.close();
                }
                popupWin.document.close();
                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: userService.getUserId(),
                    status: 'PO Processing',
                    type: "WINVENDOR",
                    sessionID: userService.getUserToken()
                };
                auctionsService.updateStatus(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                            //doc.save("DOC.PDF");
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })

                return true;
            }

            $scope.generatePO = function () {
                var doc = new jsPDF();
                doc.setFontSize(40);
                //doc.text(40, 30, "Octocat loves jsPDF", 4);
                /*doc.fromHTML($("#POTemplate")[0], 15, 15, {
                    "width": 170,
                    function() {
                        $scope.POFile = $.makeArray(new Uint8Array(doc.output('arraybuffer')));
                    }
                })*/

                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: userService.getUserId(),
                    status: 'PO Processing',
                    type: "WINVENDOR",
                    sessionID: userService.getUserToken()
                };
                auctionsService.updateStatus(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                            doc.save("DOC.PDF");
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })
            }

            $scope.generatePOinServer = function () {
                if ($scope.POTemplate == "") {
                    $scope.generatePDFonHTML();

                }
                var doc = new jsPDF('p', 'in', 'letter');
                var specialElementHandlers = {};
                var doc = new jsPDF();
                //doc.setFontSize(40);
                doc.fromHTML($scope.POTemplate, 0.5, 0.5, {
                    'width': 7.5, // max width of content on PDF
                });
                //doc.save("DOC.PDF");
                doc.output("dataurl");
                $scope.POFile = $.makeArray(new Uint8Array(doc.output('arraybuffer')));
                var params = {
                    POfile: $scope.POFile,
                    reqid: $scope.auctionItem.requirementID,
                    userid: userService.getUserId(),
                    POfileName: 'PO_req_' + $scope.auctionItem.requirementID + '.pdf',
                    sessionID: userService.getUserToken()
                }

                auctionsService.generatePOinServer(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.showStatusDropDown = true;
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })
            }

            $scope.updateStatus = function (status) {
                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: userService.getUserId(),
                    status: status,
                    type: "ALLVENDORS",
                    sessionID: userService.getUserToken()
                };
                auctionsService.updateStatus(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })
            }

            $scope.uploadquotationsfromexcel = function (status) {
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: userService.getUserId(),
                    sessionID: userService.getUserToken(),
                    quotationAttachment: $scope.quotationAttachment
                };
                auctionsService.uploadquotationsfromexcel(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            location.reload();
                        } else {
                            swal("Error", response.errorMessage, 'error');
                        }

                    })
            }


            $scope.uploadclientsidequotation = function () {
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: userService.getUserId(),
                    sessionID: userService.getUserToken(),
                    quotationAttachment: $scope.quotationAttachment
                };
                auctionsService.uploadclientsidequotation(params)
                    .then(function (response) {
                        var validation = true;
                        var itemID = 0;
                        for (item in $scope.auctionItem.listRequirementItems) {
                            var newItem = $filter('filter')(response, { itemID: $scope.auctionItem.listRequirementItems[item].itemID })[0];
                            if ($scope.auctionItem.status == "STARTED" && $scope.auctionItem.listRequirementItems[item].revUnitPrice < newItem.revUnitPrice) {
                                validation = false;
                                itemID = newItem.itemID;
                            }

                        }
                        if (validation) {
                            for (item in $scope.auctionItem.listRequirementItems) {
                                var newItem = $filter('filter')(response, { itemID: $scope.auctionItem.listRequirementItems[item].itemID })[0];
                                $scope.auctionItem.listRequirementItems[item].unitPrice = newItem.unitPrice;
                                $scope.auctionItem.listRequirementItems[item].revUnitPrice = newItem.revUnitPrice;
                                $scope.auctionItem.listRequirementItems[item].cGst = newItem.cGst;
                                $scope.auctionItem.listRequirementItems[item].sGst = newItem.sGst;
                                $scope.auctionItem.listRequirementItems[item].iGst = newItem.iGst;
                                $scope.auctionItem.listRequirementItems[item].Gst = newItem.iGst + newItem.sGst + newItem.cGst;

                                if ($scope.auctionItem.status == "UNCONFIRMED" || $scope.auctionItem.status == "NOTSTARTED") {
                                    $scope.auctionItem.listRequirementItems[item].productIDorName = newItem.productIDorName;
                                    $scope.auctionItem.listRequirementItems[item].productNo = newItem.productNo;
                                    $scope.auctionItem.listRequirementItems[item].productBrand = newItem.productBrand;
                                    $scope.freightCharges += newItem.revisedfreightCharges;
                                    $("#freightCharges").val($scope.freightCharges);
                                }

                                //$scope.unitPriceCalculation('GST');


                            }
                            $("#clientsideupload").val(null);
                        } else {
                            swal("Error", 'New bid for item cannot be lower than the old bid. Please check the values in the excel sheet for item: ' + itemID, 'error');
                        }

                        $scope.unitPriceCalculation();
                        swal("Verify", "Please verify and Submit your prices!", "success");
                        return;

                    })
            }

            $scope.getrevisedquotations = function () {
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: userService.getUserId(),
                    sessionID: userService.getUserToken()
                };
                auctionsService.getrevisedquotations(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            location.reload();
                        } else {
                            swal("Error", response.errorMessage, 'error');
                            console.log(response.errorMessage);
                        }

                    })
            }

            $scope.updatedeliverydateparams = {
                date: ''
            };

            $scope.updatedeliverdate = function () {
                var ts = moment($scope.updatedeliverydateparams.date, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                $scope.updatedeliverydateparams.date = "/Date(" + milliseconds + "000+0530)/";

                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: userService.getUserId(),
                    date: $scope.updatedeliverydateparams.date,
                    type: "DELIVERY",
                    sessionID: userService.getUserToken()
                };
                auctionsService.updatedeliverdate(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })

            }

            $scope.updatepaymentdateparams = {
                date: ''
            };

            $scope.updatepaymentdate = function () {
                var ts = moment($scope.updatepaymentdateparams.date, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                $scope.updatepaymentdateparams.date = "/Date(" + milliseconds + "000+0530)/";

                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: userService.getUserId(),
                    date: $scope.updatepaymentdateparams.date,
                    type: "PAYMENT",
                    sessionID: userService.getUserToken()
                };
                auctionsService.updatepaymentdate(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })
            }

            $scope.RestartNegotiation = function () {
                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                $scope.isnegotiationended = false;
                $scope.NegotiationEnded = false;
                $scope.invokeSignalR('RestartNegotiation', parties);
            }

            $scope.saveComment = function () {
                var commentText = "";
                if ($scope.newComment && $scope.newComment != "") {
                    commentText = $socpe.newComment;
                } else if ($("#comment")[0].value != '') {
                    commentText = $("#comment")[0].value;
                } else {
                    $scope.commentsvalidation = true;
                }
                if (!$scope.commentsvalidation) {
                    var params = {};
                    auctionsService.getdate()
                        .then(function (response) {
                            var date = new Date(parseInt(response.substr(6)));
                            var myEpoch = date.getTime();
                            params.requirementID = id;
                            params.firstName = "";
                            params.lastName = "";
                            params.replyCommentID = -1;
                            params.commentID = -1;
                            params.errorMessage = "";
                            params.createdTime = "/Date(" + myEpoch + "+0000)/";
                            params.sessionID = userService.getUserToken();
                            params.userID = userService.getUserId();
                            params.commentText = commentText;
                            $scope.invokeSignalR('SaveComment', params);
                            //requirementHub.invoke('SaveComment', params, function (response) {
                            //	//auctionsService.savecomment(params).then(function (response) {
                            //	$scope.getData();
                            //	$scope.newComment = "";
                            //	$scope.commentsvalidation = false;
                            //});
                        });
                }
            }

            $scope.multipleAttachments = [];
            //$scope.multipleAttachmentsList = [];

            if (!$scope.multipleAttachmentsList) {
                $scope.multipleAttachmentsList = [];
            }

            $scope.getFile = function () {
                $scope.progress = 0;
                //var quotation = $("#quotation")[0].files[0];
                //if (quotation != undefined && quotation != '') {
                //    $scope.file = $("#quotation")[0].files[0];
                //    $log.info($("#quotation")[0].files[0]);
                //}

                //fileReader.readAsDataUrl($scope.file, $scope)
                //    .then(function (result) {
                //        var bytearray = new Uint8Array(result);
                //        if (quotation != undefined && quotation != '') {
                //            $scope.bidAttachement = $.makeArray(bytearray);
                //            $scope.bidAttachementName = $scope.file.name;
                //            $scope.enableMakeBids = true;
                //        }
                //    });

                $scope.multipleAttachments = $("#attachement")[0].files;
                $scope.multipleAttachments = Object.values($scope.multipleAttachments)

                $scope.multipleAttachments.forEach(function (item, index) {
                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;
                            if (!$scope.multipleAttachmentsList) {
                                $scope.multipleAttachmentsList = [];
                            }
                            $scope.multipleAttachmentsList.push(fileUpload);
                        });
                })

            };

            $scope.updateTime = function (time) {
                var isDone = false;
                $scope.disableDecreaseButtons = true;
                $scope.disableAddButton = true;
                $scope.$on('timer-tick', function (event, args) {
                    var temp = event.targetScope.countdown;

                    if (!isDone) {
                        if (time < 0 && temp + time < 0) {
                            growlService.growl("You cannot reduce the time when it is already below 60 seconds", "inverse");
                            isDone = true;
                            return false;
                        }

                        isDone = true;
                        var params = {};
                        params.reqID = id;
                        params.sessionID = userService.getUserToken();
                        params.userID = userService.getUserId();
                        params.newTicks = ($scope.countdownVal + time);
                        var parties = id + "$" + userService.getUserId() + "$" + params.newTicks + "$" + userService.getUserToken();
                        $scope.invokeSignalR('UpdateTime', parties, function () { addCDSeconds("timer", time); });
                    }
                });
            }

            $scope.Loding = false;

            $scope.priceCapError = false;

            $scope.updateAuctionStart = function () {
                $scope.priceCapError = false;
                $scope.Loding = true;
                $scope.NegotiationSettingsValidationMessage = '';


                $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);
                if ($scope.NegotiationSettingsValidationMessage != '') {
                    $scope.Loding = false;
                    return;
                }

                var params = {};
                params.auctionID = id;

                var startValue = $("#startTimeDate").val(); //$scope.startTime; //Need fix on this.

                if (startValue && startValue != null && startValue != "") {

                    //var ts = moment($scope.startTime, "DD-MM-YYYY HH:mm").valueOf();
                    var ts = moment(startValue, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var auctionStartDate = new Date(m);
                    auctionsService.getdate()
                        .then(function (response1) {
                            
                            var CurrentDateToLocal = userService.toLocalDate(response1);

                            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                            var m = moment(ts);
                            var deliveryDate = new Date(m);
                            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                            var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));

                            $log.debug(CurrentDate < auctionStartDate);
                            $log.debug('div' + auctionStartDate);
                            if (CurrentDate >= auctionStartDate) {
                                $scope.Loding = false;
                                swal("Done!", "Your Negotiation Start Time should be greater than current time.", "error");
                                return;
                            }


                            var milliseconds = parseInt(auctionStartDate.getTime() / 1000.0);
                            // // #INTERNATIONALIZATION-0-2019-02-12
                            params.postedOn = "/Date(" + userService.toUTCTicks(startValue) + "+0530)/";
                            params.auctionEnds = "/Date(" + userService.toUTCTicks(startValue) + "+0530)/";
                            params.customerID = userService.getUserId();
                            params.sessionID = userService.getUserToken();

                            $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                            params.NegotiationSettings = $scope.NegotiationSettings;

                            //$scope.auctionItem.auctionVendors.forEach(function (item, index) {
                            //    if (item.companyName == "PRICE_CAP") {
                            //        if (item.totalRunningPrice > $scope.auctionItem.auctionVendors[0].totalRunningPrice) {
                            //            $scope.priceCapError = true;
                            //        }
                            //    }
                            //})

                            if ($scope.starreturn) {
                                $scope.Loding = false;
                                swal("Not Allowed", "You are not allowed to schedule the Negotiation time until all the Vendor Quotations are validated. (They should be either approved/rejected). The following vendors quotations are yet to be validated. (" + $scope.notviewedcompanynames + ")", "error");
                                return;
                            }

                            if ($scope.auctionItem.auctionVendors.length == 0 || $scope.auctionItem.auctionVendors[0].quotationUrl == "" || $scope.auctionItem.auctionVendors[1].quotationUrl == "" || $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 || $scope.auctionItem.auctionVendors[1].isQuotationRejected != 0 || $scope.starttimecondition1 != 0 || $scope.starttimecondition2 != 0) {
                                $scope.Loding = false;
                                swal("Not Allowed", "You are not allowed to create a start time until at least 2 vendors Approved.", "error");
                                return;
                            }
                            if ($scope.auctionItem.auctionVendors[0].companyName == "") {
                                $scope.Loding = false;
                                swal("Not Allowed", "You are not allowed to create a start time until at least 2 vendors Approved.", "error");
                                return;
                            }
                            if ($scope.priceCapError == true) {
                                $scope.Loding = false;
                                swal("Not Allowed", "Price CAP Value should be less then all the vendors price.", "error");
                                return;
                            }
                            else {
                                $scope.Loding = true;
                                requirementHub.invoke('UpdateAuctionStartSignalR', params, function (req) {
                                    $scope.Loding = false;
                                    swal("Done!", "Your Negotiation Start Time Updated Successfully!", "success");
                                    $scope.Loding = false;
                                    var start = $scope.auctionItem.startTime.split('+')[0].split('(')[1];
                                    auctionsService.getdate()
                                        .then(function (response) {
                                            //var curDate = new Date(parseInt(response.substr(6)));

                                            var CurrentDateToLocal = userService.toLocalDate(response);

                                            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                                            var m = moment(ts);
                                            var deliveryDate = new Date(m);
                                            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                                            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                                            var curDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));

                                            var myEpoch = curDate.getTime();
                                            if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess) && start > myEpoch) {
                                                $scope.startBtns = true;
                                                $scope.customerBtns = false;
                                            } else {

                                                $scope.startBtns = false;
                                                $scope.disableButtons();
                                                $scope.customerBtns = true;
                                            }

                                            if ($scope.auctionItem.timeLeft == null || $scope.auctionItem.timeLeft < 0) {
                                                $scope.showTimer = false;
                                            } else {

                                                $scope.showTimer = true;
                                            }
                                        })
                                })
                            }
                        })

                    $scope.Loding = false;


                } else {
                    $scope.Loding = false;
                    alert("Please enter the date and time to update Start Time to.");
                }
                $scope.Loding = false;
            }

            $scope.makeaBidLoding = false;

            $scope.makeaBid = function (call, isReviced) {
                $scope.makeaBidLoding = true;

                var bidPrice = 0;

                if (isReviced == 0) {
                    var bidPrice = $("#quotationamount").val();
                }
                if (isReviced == 1) {

                    var bidPrice = $("#revquotationamount").val();
                }

                var params = {};
                params.reqID = parseInt(id);
                params.sessionID = userService.getUserToken();
                params.userID = parseInt(userService.getUserId());

                params.price = $scope.precisionRound(parseFloat(bidPrice), 2);
                params.quotation = $scope.bidAttachement;
                params.quotationName = $scope.bidAttachementName;
                params.discountAmount = $scope.discountAmount;

                params.tax = $scope.vendorTaxes;

                params.warranty = $scope.warranty;
                params.duration = $scope.duration;
                params.payment = $scope.payment;
                params.gstNumber = $scope.gstNumber;
                params.validity = $scope.validity;

                params.otherProperties = $scope.otherProperties;

                params.quotationType = 'USER';
                params.type = 'quotation';

                params.freightCharges = $scope.freightCharges;
                params.freightChargesTaxPercentage = $scope.freightChargesTaxPercentage;
                params.freightChargesWithTax = $scope.freightChargesWithTax;

                params.packingCharges = $scope.packingCharges;
                params.packingChargesTaxPercentage = $scope.packingChargesTaxPercentage;
                params.packingChargesWithTax = $scope.packingChargesWithTax;

                params.installationCharges = $scope.installationCharges;
                params.installationChargesTaxPercentage = $scope.installationChargesTaxPercentage;
                params.installationChargesWithTax = $scope.installationChargesWithTax;

                //params.unitPrice = $scope.auctionItem.unitPrice;
                //params.productQuantity = $scope.auctionItem.productQuantity;
                //params.cGst = $scope.auctionItem.cGst;
                //params.sGst = $scope.auctionItem.sGst;
                //params.iGst = $scope.auctionItem.iGst;



                params.listRequirementTaxes = $scope.listRequirementTaxes;

                params.quotationObject = $scope.auctionItemVendor.listRequirementItems;

                if (isReviced == 0) {
                    params.revised = isReviced;
                    params.priceWithoutTax = $scope.totalprice;
                    params.price = $scope.vendorBidPrice;
                    params.freightCharges = $scope.freightCharges;
                    params.discountAmount = $scope.discountAmount;
                    params.packingCharges = $scope.packingCharges;
                    params.packingChargesTaxPercentage = $scope.packingChargesTaxPercentage;
                    params.packingChargesWithTax = $scope.packingChargesWithTax;
                    params.installationCharges = $scope.installationCharges;
                    params.installationChargesTaxPercentage = $scope.installationChargesTaxPercentage;
                    params.installationChargesWithTax = $scope.installationChargesWithTax;
                    params.itemFreightCharges = $scope.itemRevFreightCharges;
                }
                if (isReviced == 1) {
                    params.revised = isReviced;
                    params.priceWithoutTax = $scope.revtotalprice;
                    params.price = $scope.revvendorBidPrice;
                    params.freightCharges = $scope.revfreightCharges;
                    params.discountAmount = $scope.discountAmount;
                    params.packingCharges = $scope.revpackingCharges;
                    params.packingChargesTaxPercentage = $scope.packingChargesTaxPercentage;
                    params.packingChargesWithTax = $scope.revpackingChargesWithTax;
                    params.installationCharges = $scope.revinstallationCharges;
                    params.installationChargesTaxPercentage = $scope.installationChargesTaxPercentage;
                    params.installationChargesWithTax = $scope.revinstallationChargesWithTax;
                    params.itemFreightCharges = $scope.itemRevFreightCharges;

                }

                if (($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isQuotationPriceLimit && $scope.vendorBidPrice > $scope.auctionItem.quotationPriceLimit) {
                    swal("Error!", 'Your Amount Should be less than Quotation Price limit :' + $scope.auctionItem.quotationPriceLimit, "error");
                    return;
                };





                auctionsService.getdate()
                    .then(function (responseFromServer) {
                        var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                        var auctionStart = new Date(parseInt($scope.auctionItem.startTime.substr(6)));
                        var timeofauctionstart = auctionStart.getTime();
                        var currentServerTime = dateFromServer.getTime();




                        if ($scope.auctionItem.status == "Negotiation Ended") {



                            // $scope.DeleteRequirementTerms();


                            if ($scope.auctionItem.status == 'Negotiation Ended') {
                            } else {
                                // $scope.SaveRequirementTerms($scope.reqId);
                            }

                            auctionsService.makeabid(params).then(function (req) {
                                if (req.errorMessage == '') {

                                    swal({
                                        title: "Thanks!",
                                        text: "Your Quotation has been Uploaded Successfully",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            location.reload();
                                        });

                                    $("#quotationamount").val("");
                                    $scope.quotationStatus = true;
                                    $scope.auctionStarted = false;
                                    $scope.getData();
                                    $scope.makeaBidLoding = false;
                                } else {
                                    $scope.makeaBidLoding = false;
                                    swal({
                                        title: "Error",
                                        text: req.errorMessage,
                                        type: "error",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    });
                                }
                            });

                        } else {
                            if (timeofauctionstart - currentServerTime >= 3600000 && $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                                // $scope.DeleteRequirementTerms();


                                if ($scope.auctionItem.status == 'Negotiation Ended') {
                                } else {
                                    // $scope.SaveRequirementTerms($scope.reqId);
                                }

                                auctionsService.makeabid(params).then(function (req) {
                                    if (req.errorMessage == '') {

                                        swal({
                                            title: "Thanks!",
                                            text: "Your Quotation has been Uploaded Successfully",
                                            type: "success",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        },
                                            function () {
                                                location.reload();
                                            });

                                        $("#quotationamount").val("");
                                        $scope.quotationStatus = true;
                                        $scope.auctionStarted = false;
                                        $scope.getData();
                                        $scope.makeaBidLoding = false;
                                    } else {
                                        $scope.makeaBidLoding = false;
                                        swal({
                                            title: "Error",
                                            text: req.errorMessage,
                                            type: "error",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        });
                                    }
                                });
                            }

                            else {

                                var message = "You have missed the deadline for Quotation Submission"
                                if ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 && $scope.auctionItem.status != "Negotiation Ended") { message = "Your Quotation Already Approved You cant Update Quotation Please Contact PRM360" }

                                $scope.makeaBidLoding = false;
                                swal({
                                    title: "Error",
                                    text: message,
                                    type: "error",
                                    showCancelButton: false,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: true
                                });
                                $scope.getData();
                            }



                        }












                    })

            }

            $scope.revquotationupload = function () {
                $scope.makeaBidLoding = true;
                if (($scope.bidAttachementName == "" || $scope.bidAttachement.length == 0) && $scope.quotationStatus == false) {
                    $scope.bidAttachementValidation = true;
                    $scope.makeaBidLoding = false;
                    return false;
                } else {
                    $scope.bidAttachementValidation = false;
                }
                var params = {};
                params.reqID = parseInt(id);
                params.sessionID = userService.getUserToken();
                params.userID = parseInt(userService.getUserId());
                params.price = 0;
                params.quotation = $scope.bidAttachement;
                params.quotationName = $scope.bidAttachementName;
                params.tax = 0;
                auctionsService.revquotationupload(params).then(function (req) {
                    if (req.errorMessage == '') {
                        swal({
                            title: "Thanks!",
                            text: "Your Revised Quotation Uploaded Successfully",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                location.reload();
                            });
                        $("#quotationamount1").val("");
                        $scope.quotationStatus = true;
                        $scope.auctionStarted = false;
                        $scope.getData();
                        $scope.makeaBidLoding = false;
                    } else {
                        $scope.makeaBidLoding = false;
                        swal("Error!", req.errorMessage, "error");
                    }
                });
            }

            $scope.$on('timer-tick', function (event, args) {
                if (event.targetScope.seconds == 5 ||
                   event.targetScope.seconds == 15 ||
                   event.targetScope.seconds == 25 ||
                   event.targetScope.seconds == 35 ||
                   event.targetScope.seconds == 45 ||
                   event.targetScope.seconds == 55) {
                    //auctionsService.CheckSystemDateTime();
                }
                if ($scope.auctionItem.IS_CB_ENABLED == false || $scope.auctionItem.IS_CB_ENABLED == 0) {
                    $scope.countdownVal = event.targetScope.countdown;
                    if (event.targetScope.countdown < 61 && !$scope.disableStopBids && !$scope.disablereduceTime) {
                        $timeout($scope.disableButtons(), 1000);
                    }
                    if (event.targetScope.countdown > 60) {
                        $timeout($scope.enableButtons(), 1000);
                    }
                    if (event.targetScope.countdown == 60 || event.targetScope.countdown == 59 || event.targetScope.countdown == 30 || event.targetScope.countdown == 29) {
                        //var msie = $(document) [0].documentMode;
                        var ua = window.navigator.userAgent;
                        var msieIndex = ua.indexOf("MSIE ");
                        var msieIndex2 = ua.indexOf("Trident");
                        // if is IE (documentMode contains IE version)
                        var params = {};
                        params.reqID = id;
                        params.sessionID = userService.getUserToken();
                        params.userID = userService.getUserId();
                        //var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                        //requirementHub.invoke('CheckRequirement', parties, function () {
                        //    $scope.getData();
                        //    //$scope.$broadcast('timer-set-countdown-seconds', 0);
                        //    //swal("Negotiation Completed!", "Congratulations! you procurement process is now completed. " + $scope.toprankerName + " is the least bider with the value " + $scope.auctionItem.minPrice + " \n Your savings through PRM :" + ($scope.vendorInitialPrice - $scope.auctionItem.minPrice), "success");
                        //});
                        if (msieIndex > 0 || msieIndex2 > 0) {
                            $scope.getData();
                        }
                    }
                    if (event.targetScope.countdown <= 0) {
                        if ($scope.auctionStarted && ($scope.auctionItem.status == "CLOSED" || $scope.auctionItem.status == "STARTED")) {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0].runningPrice;
                            //if (!$scope.NegotiationEnded) {
                            //    auctionsService.isnegotiationended(id, userService.getUserToken())
                            //        .then(function (response) {
                            //            if (response.errorMessage == '') {
                            //                if (response.objectID == 1) {
                            //                    var params = {};
                            //                    params.reqID = id;
                            //                    params.sessionID = userService.getUserToken();
                            //                    params.userID = userService.getUserId();
                            //                    var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                            //                    $scope.NegotiationEnded = true;
                            //                    $scope.invokeSignalR('EndNegotiation', parties);

                            //                }
                            //            }
                            //        })
                            //}
                        } else if ($scope.auctionItem.status == "NOTSTARTED") {
                            var params = {};
                            params.reqID = id;
                            params.sessionID = userService.getUserToken();
                            params.userID = userService.getUserId();

                            auctionsService.StartNegotiation(params)
                                .then(function (response) {
                                    if (response.errorMessage == '') {
                                        $scope.getData();
                                    }
                                })

                            var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                            $scope.invokeSignalR('CheckRequirement', parties);
                            //requirementHub.invoke('CheckRequirement', parties, function () {
                            //    $scope.getData();
                            //    //$scope.$broadcast('timer-set-countdown-seconds', 0);
                            //    //swal("Negotiation Completed!", "Congratulations! you procurement process is now completed. " + $scope.toprankerName + " is the least bider with the value " + $scope.auctionItem.minPrice + " \n Your savings through PRM :" + ($scope.vendorInitialPrice - $scope.auctionItem.minPrice), "success");
                            //});
                        }

                        //$scope.getData();
                    }
                    if (event.targetScope.countdown <= 120) {
                        $scope.timerStyle = {
                            'color': '#f00',
                            '-webkit - animation': 'flash linear 1s infinite',
                            'animation': 'flash linear 1s infinite'
                        };
                    }
                    //$log.info($('#reqTitle').visible());
                    //console.log($('#reqTitle').visible());
                    $scope.timerFloat = $('#reqTitle').show();
                    if ($scope.auctionItem.status == 'STARTED' && !$scope.timerFloat) {
                        $scope.divfix = {
                            'bottom': '2%',
                            'left': '3%',
                            'position': 'fixed',
                            'z-index': '3000'
                        };
                        $scope.divfix3 = {
                            'bottom': '9%',
                            'left': '3%',
                            'position': 'fixed',
                            'z-index': '3000'
                        };
                        $scope.divfix1 = {
                            'bottom': '8%',
                            'left': '3%',
                            'position': 'fixed',
                            'z-index': '3000'
                        };
                        $scope.divfix2 = {
                            'bottom': '13%',
                            'left': '3%',
                            'position': 'fixed',
                            'z-index': '3000'
                        };
                        $scope.divfixMakeabid = {
                            'bottom': '14%',
                            'left': '10%',
                            'position': 'fixed',
                            'z-index': '3000'
                        };
                        $scope.divfixMakeabidError = {
                            'bottom': '13%',
                            'left': '18%',
                            'position': 'fixed',
                            'z-index': '3000',
                            'background-color': 'lightgrey'
                        };

                        if (!$scope.isCustomer) {
                            $scope.boxfix = {
                                'background-color': 'lightgrey',
                                'width': '280px',
                                'height': '120px',
                                //'border': '25px solid green',
                                'padding': '25px',
                                'margin': '25px',


                                'bottom': '-2%',
                                'left': '0%',
                                'position': 'fixed',
                                'z-index': '3000'
                            }
                        }
                        else {
                            $scope.boxfix = {
                                'background-color': 'lightgrey',
                                'width': '280px',
                                'height': '90px',
                                //'border': '25px solid green',
                                'padding': '25px',
                                'margin': '25px',


                                'bottom': '-2%',
                                'left': '0%',
                                'position': 'fixed',
                                'z-index': '3000'
                            }
                        }

                    }
                    else {
                        $scope.divfix = {};
                        $scope.divfix3 = {};
                        $scope.divfix1 = {};
                        $scope.divfix2 = {};
                        $scope.divfixMakeabid = {};
                        $scope.divfixMakeabidError = {};
                        $scope.boxfix = {};
                    };
                    if ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') {

                        if ($scope.auctionItem.isUnitPriceBidding == 1 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isTabular == true) {
                            $scope.divBorder = {
                                'background-color': '#f5b2b2'
                            };
                        }

                        if ($scope.auctionItem.isUnitPriceBidding == 0 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isTabular == true) {
                            $scope.divBorder1 = {
                                'background-color': '#f5b2b2'
                            };
                        }
                    }
                    if (event.targetScope.countdown > 120 && $scope.auctionItem.status == 'NOTSTARTED') {
                        $scope.timerStyle = { 'color': '#000' };
                    }
                    if (event.targetScope.countdown > 120 && $scope.auctionItem.status != 'NOTSTARTED') {
                        $scope.timerStyle = { 'color': '#228B22' };
                    }
                    if (event.targetScope.countdown <= 60 && $scope.auctionItem.status == 'STARTED') {
                        $scope.disableDecreaseButtons = true;
                    }
                    if (event.targetScope.countdown > 60 && $scope.auctionItem.status == 'STARTED') {
                        $scope.disableDecreaseButtons = false;
                    }
                    if (event.targetScope.countdown <= 0 && $scope.auctionItem.status == 'NOTSTARTED') {
                        $scope.showTimer = false;

                        //$scope.getData();
                        window.location.reload();
                    }
                    if (event.targetScope.countdown < 1 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'Negotiation Ended')) {
                        //var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                        //$scope.invokeSignalR('CheckRequirement', parties);  
                        $scope.showTimer = false;
                        $scope.auctionItem.status = 'Negotiation Ended';

                        swal({
                            title: "Thanks!",
                            text: "Negotiation complete! Thank you for being a part of this.",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                location.reload();
                                $log.info("Negotiation Ended.");
                            });
                    }
                    if (event.targetScope.countdown <= 3) {
                        $scope.disableAddButton = true;
                    }
                    if (event.targetScope.countdown < 6) {
                        $scope.disableBidButton = true;
                    }
                    if (event.targetScope.countdown > 3) {
                        //$scope.getData();
                        $scope.disableAddButton = false;
                    }
                    if (event.targetScope.countdown >= 6) {
                        $scope.disableBidButton = false;
                    }
                }
                else if ($scope.auctionItem.IS_CB_ENABLED == true || $scope.auctionItem.IS_CB_ENABLED == 1) {
                    //var temp = event.targetScope.countdown;
                    //console.log(temp);
                    //$scope.auctionItem.CB_TIME_LEFT = $scope.auctionItem.CB_TIME_LEFT - 1;

                    if (event.targetScope.minutes < 2) {
                        $scope.timerStyle = {
                            'color': '#f00',
                            '-webkit - animation': 'flash linear 1s infinite',
                            'animation': 'flash linear 1s infinite'
                        };
                    }

                    if (event.targetScope.minutes >= 2 || event.targetScope.days > 0 || event.targetScope.hours > 0) {
                        $scope.timerStyle = { 'color': '#228B22' };
                    }


                    if (event.targetScope.days == 0 &&
                        event.targetScope.hours == 0 &&
                        event.targetScope.minutes == 0 &&
                        (event.targetScope.seconds == 1)) {
                        location.reload();
                    }
                }

            });

            requirementHub.on('checkRequirement', function (items) {
                var req = items.inv;
                var iteminvcallerID = 0;
                if ($scope.auctionItem.IS_CB_ENABLED == false || $scope.auctionItem.IS_CB_ENABLED == 0) {
                    if (items.inv) {
                        iteminvcallerID = items.inv.callerID;
                    }
                    if ($scope.isCustomer) {
                        var itemTemp = _.filter(items.payLoad, function (userItem) {

                            if (String(userItem.superUserID) === String($scope.auctionItem.superUserID)) {
                                $scope.signalRCustomerAccess = true;
                            }

                            return String(userItem.superUserID) === String($scope.auctionItem.superUserID)
                        });
                    }
                    else {
                        var itemTemp = _.filter(items.payLoad, function (userItem) {
                            return String(userItem.entityID) === userService.getUserId()
                        });
                    }
                    let item = itemTemp[0];
                    $scope.setAuctionInitializer(item, '', iteminvcallerID);
                    $scope.$broadcast('timer-set-countdown-seconds', $scope.auctionItem.timeLeft);
                    $('.datetimepicker').datetimepicker({
                        useCurrent: false,
                        icons: {
                            time: 'glyphicon glyphicon-time',
                            date: 'glyphicon glyphicon-calendar',
                            up: 'glyphicon glyphicon-chevron-up',
                            down: 'glyphicon glyphicon-chevron-down',
                            previous: 'glyphicon glyphicon-chevron-left',
                            next: 'glyphicon glyphicon-chevron-right',
                            today: 'glyphicon glyphicon-screenshot',
                            clear: 'glyphicon glyphicon-trash',
                            close: 'glyphicon glyphicon-remove'

                        },

                        minDate: item.currentTime
                    });
                    if (!$scope.NegotiationEnded) {
                        $scope.$broadcast('timer-start');
                    }
                    if (id == req.requirementID && ($scope.signalRCustomerAccess || userService.getUserId() == req.customerID || userService.getUserId() == req.superUserID || req.userIDList.indexOf(parseInt(userService.getUserId())) > -1 || req.custCompID == userService.getUserCompanyId())) {
                        if (req.methodName == "UpdateTime" && req.userIDList.indexOf(parseInt(userService.getUserId())) > -1) {
                            growlService.growl("Negotiation time has been updated.", 'inverse');
                        } else if (req.methodName == "MakeBid" && ($scope.signalRCustomerAccess || userService.getUserId() == req.customerID || req.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess || req.custCompID == userService.getUserCompanyId())) {
                            growlService.growl("A vendor has made a bid.", "success");
                        } else if (req.methodName == "RestartNegotiation") {
                            window.location.reload();
                        } else if (req.methodName == "RevUploadQuotation") {

                        } else if (req.methodName == "StopBids") {
                            if (req.userIDList.indexOf(parseInt(userService.getUserId())) > -1) {
                                growlService.growl("Negotiation Time reduced to 1 minute by the customer. New bids will not extend time.");
                            }
                        } else if (req.methodName == "SAVE_DIFFERENTIAL_FACTOR") {
                            if ($scope.auctionItem.status == 'STARTED') {
                                growlService.growl("A vendor has made a bid.", "success");
                            } else {
                                //growlService.growl(req.status, "success");
                            }
                        } else if (!$scope.NegotiationEnded) {
                            auctionsService.isnegotiationended(id, userService.getUserToken())
                                .then(function (response) {
                                    if (response.errorMessage == '') {
                                        if (response.objectID == 1) {
                                            $scope.NegotiationEnded = true;
                                            if (($scope.signalRCustomerAccess || userService.getUserId() == req.customerID || req.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess)) {
                                                swal("Negotiation Completed!", "Congratulations! your procurement process is now completed. " + $scope.toprankerName + " is the least bidder with the value " + $scope.auctionItem.minPrice + " " + $scope.auctionItem.currency + " \n Your savings through PRM 360 :" + ($scope.auctionItem.savings) + " " + $scope.auctionItem.currency, "success");
                                            } else if (req.userIDList.indexOf(parseInt(userService.getUserId())) > -1 && $scope.timeLeftMessage == "Negotiation Ends in: ") {
                                                if ($scope.vendorRank == 1) {
                                                    swal("Negotiation Completed!", "Congratulations! you are the least bidder for this requirement. Your price is : " + $scope.auctionItem.minPrice + " " + $scope.auctionItem.currency + "\n Customer would be reaching out you shortly. All the best!", "success");
                                                } else {
                                                    swal("Negotiation Completed!", "Bidding Completed.\n Thank You for your interest on this requirement. You ranked " + $scope.vendorRank + " in this requirement. Thank you for your participation.", "success");
                                                }
                                            }
                                        }
                                    } else {
                                    }
                                })
                        }
                    } else {
                        if (req.methodName == "SAVE_DIFFERENTIAL_FACTOR" && iteminvcallerID != userService.getUserId()) {
                            growlService.growl("A vendor has made a bid.", "success");
                        }
                    }
                }
                else if ($scope.auctionItem.IS_CB_ENABLED) {

                    if ($scope.userIsOwner) {


                        if (items.inv.methodName == 'CheckRequirement') {
                            var vendorCompanyName = '';
                            $scope.auctionItem.requirementVendorsList.forEach(function (vendor, vendorIndex) {
                                if (items.inv.callerID == vendor.vendorID) {
                                    vendorCompanyName = vendor.companyName;
                                    growlService.growl(vendorCompanyName + ' has made a bid.', "success");
                                }
                            })
                            
                        }


                        $scope.getData();
                    } else if ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0) {
                        $scope.getData();
                    }

                    
                }
            })

            $scope.stopBids = function () {
                swal({
                    title: "Are you sure?",
                    text: "The Negotiation will be stopped after one minute.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Yes, Stop Bids!",
                    closeOnConfirm: true
                }, function () {
                    var params = {};
                    params.reqID = id;
                    params.sessionID = userService.getUserToken();
                    params.userID = userService.getUserId();
                    var parties = params.reqID + "$" + params.userID + "$" + params.sessionID;
                    $scope.invokeSignalR('StopBids', parties, function () {
                        $scope.$broadcast('timer-set-countdown-seconds', 60);
                        $scope.disableButtons();
                        swal("Done!", "Negotiation time reduced to one minute.", "success");
                    });

                    //requirementHub.invoke('StopBids', parties, function (req) {
                    //    $scope.$broadcast('timer-set-countdown-seconds', 60);
                    //    $scope.disableButtons();
                    //    swal("Done!", "Negotiation time reduced to one minute.", "success");
                    //});
                });
            };

            $scope.disableButtons = function () {
                $scope.buttonsDisabled = true;
            };

            $scope.enableButtons = function () {
                $scope.buttonsDisabled = false;
            };

            $scope.editRequirement = function () {
                $log.info('in edit' + $stateParams.Id);
                //$state.go('form.addnewrequirement', { 'Id': $stateParams.Id });
                $state.go('save-tender', { 'Id': $stateParams.Id });
            };

            $scope.goToReqReport = function (reqID) {
                //$state.go("reports", { "reqID": reqID });

                var url = $state.href('reports', { "reqID": reqID });
                $window.open(url, '_blank');

            };

            $scope.generatePOforUser = function () {
                $state.go('po', { 'reqID': $stateParams.Id });
            };

            $scope.metrialDispatchmentForm = function () {
                $state.go('material-dispatchment', { 'Id': $stateParams.Id });
            };

            $scope.paymentdetailsForm = function () {
                $state.go('payment-details', { 'Id': $stateParams.Id });
            }

            $scope.deleteRequirement = function () {
                swal({
                    title: "Are you sure?",
                    text: "The Negotiation will be deleted and an email will be sent out to all vendors involved.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Yes, I am sure",
                    closeOnConfirm: true
                }, function () {
                    var params = {};
                    params.reqID = id;
                    params.sessionID = userService.getUserToken();
                    params.userID = userService.getUserId();
                    // auctionsService.updatebidtime(params);
                    // swal("Done!", "Auction time reduced to oneminute.", "success");
                    var parties = params.reqID + "$" + params.userID + "$" + params.sessionID + "$" + $scope.reason;
                    $scope.invokeSignalR('DeleteRequirement', parties);
                    //requirementHub.invoke('DeleteRequirement', parties, function (req) {
                    //    $scope.$broadcast('timer-set-countdown-seconds', 60);
                    //    $scope.disableButtons();
                    //    swal("Done!", "Requirement has been cancelled", "success");
                    //});
                });
            }

            $scope.DeleteVendorFromAuction = function (VendoID, quotationUrl) {
                if (($scope.auctionItem.auctionVendors.length > 2 || quotationUrl == "") && (quotationUrl == "" || (quotationUrl != "" && $scope.auctionItem.auctionVendors[2].quotationUrl != ""))) {
                    swal({
                        title: "Are you sure?",
                        text: "The Vendor will be deleted and an email will be sent out to The vendor.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "Yes, I am sure",
                        closeOnConfirm: true
                    }, function () {
                        var params = {};
                        params.reqID = id;
                        params.sessionID = userService.getUserToken();
                        params.userID = VendoID;

                        auctionsService.DeleteVendorFromAuction(params)
                            .then(function (response) {
                                if (response.errorMessage == '') {

                                    $scope.getData();
                                    swal("Done!", "Done! Vendor Deleted Successfully!", "success");
                                } else {
                                    swal("Error", "You cannot Delete Vendor", "inverse");
                                }
                            })
                    });
                }
                else {
                    swal("Not Allowed", "You are not allowed to Delete the Vendors.", "error");
                }
            }



            $scope.makeBidUnitPriceValidation = function (revUnitPrice, TemperoryRevUnitPrice, productIDorName) {
                if (TemperoryRevUnitPrice < revUnitPrice && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'Negotiation Ended') && $scope.auctionItem.isDiscountQuotation == 0) {
                    $scope.getData();
                    swal("Error!", 'Please enter price less than ' + TemperoryRevUnitPrice + ' of ' + productIDorName);
                }
                if ($scope.TemperoryRevfreightCharges < $scope.revfreightCharges && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'Negotiation Ended') && $scope.auctionItem.isDiscountQuotation == 0) {
                    $scope.getData();
                    swal("Error!", 'Please enter freight price less than ' + $scope.TemperoryRevfreightCharges);
                }
            }

            $scope.makeBidOtherChargesValidation = function () {
                if(($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'Negotiation Ended') && $scope.auctionItem.isDiscountQuotation == 0){
                    if ($scope.TemperoryRevpackingCharges < $scope.revpackingCharges) {
                        $("#revpackingCharges").val($scope.TemperoryRevpackingCharges);
                        $scope.getData();
                        swal("Error!", 'Please enter packing charges less than ' + $scope.TemperoryRevpackingCharges);
                    }
                    else if ($scope.TemperoryRevinstallationCharges < $scope.revinstallationCharges) {
                        $("#revinstallationCharges").val($scope.TemperoryRevinstallationCharges);
                        $scope.getData();
                        swal("Error!", 'Please enter installation charges less than ' + $scope.TemperoryRevinstallationCharges);
                    }
                    else if ($scope.TemperoryRevfreightCharges < $scope.revfreightCharges) {
                        $("#revfreightCharges").val($scope.TemperoryRevfreightCharges);
                        $scope.getData();
                        swal("Error!", 'Please enter freight charges less than ' + $scope.TemperoryRevfreightCharges);
                    }
                }
            }

            $scope.unitPriceCalculation = function (val) {

                $scope.gstValidation = false;

                var itemfreight = 0;
                var itemRevfreight = 0;
                var myInit = 1;

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                    
                    if ((val == 'CGST' || val == 'SGST') && (item.sGst != 0 || item.cGst != 0) && myInit == 1) {
                        $scope.IGSTFields = true;
                        $scope.CSGSTFields = false;
                        myInit++;
                    } else if (val == 'IGST' && item.iGst != 0 && myInit == 1) {
                        $scope.CSGSTFields = true;
                        $scope.IGSTFields = false;
                        myInit++;
                    } else if (myInit == 1 && val != "PRC") {
                        $scope.CSGSTFields = false;
                        $scope.IGSTFields = false;
                    }

                    // item.cGst = item.Gst / 2;
                    // item.sGst = item.Gst / 2;

                    item.itemfreight = ((parseFloat(item.itemFreightCharges)) + ((parseFloat(item.itemFreightCharges / 100)) * (item.itemFreightTAX)));
                    itemRevfreight = ((parseFloat(item.itemRevFreightCharges)) + ((parseFloat(item.itemRevFreightCharges / 100)) * (item.itemFreightTAX)));
                    //  item.itemPrice = item.itemPrice + ((item.itemPrice / 100) * (item.cGst + item.sGst + item.iGst));
                    if (val == 'GST') {

                        if ($scope.auctionItem.isDiscountQuotation == 2) {

                            item = $scope.handlePrecision(item, 2);
                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                item.revCostPrice = item.costPrice;
                                item.revmarginAmount = item.marginAmount;
                            }

                            item.cGst = item.Gst / 2;
                            item.sGst = item.Gst / 2;
                            item.netPrice = 1 + item.Gst / 100;
                            item = $scope.handlePrecision(item, 2);
                            item.netPrice = item.costPrice * item.netPrice;
                            item = $scope.handlePrecision(item, 2);
                            item.marginAmount = item.unitMRP - item.netPrice;

                            item.revnetPrice = 1 + item.Gst / 100;
                            item = $scope.handlePrecision(item, 2);
                            item.revnetPrice = item.revCostPrice * item.revnetPrice;
                            item = $scope.handlePrecision(item, 2);
                            item.revmarginAmount = item.unitMRP - item.revnetPrice;

                            item = $scope.handlePrecision(item, 2);
                            //item.unitDiscount = (item.marginAmount / item.costPrice);
                            item.unitDiscount = (item.marginAmount / item.netPrice);
                            item.unitDiscount = item.unitDiscount * 100;
                            item = $scope.handlePrecision(item, 2);
                        }
                    }
                    if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                        if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                            item.revUnitPrice = item.unitPrice;
                            item.itemRevFreightCharges = item.itemFreightCharges;
                        }

                        //item.sGst = item.cGst;
                        var tempUnitPrice = item.unitPrice;
                        var tempCGst = item.cGst;
                        var tempSGst = item.sGst;
                        var tempIGst = item.iGst;
                        if (item.unitPrice == undefined || item.unitPrice <= 0) {
                            item.unitPrice = 0;
                        };

                        item.itemPrice = item.unitPrice * item.productQuantity;
                        if (item.cGst == undefined || item.cGst <= 0) {
                            item.cGst = 0;
                        } else if (val == 'cGST') {
                            // item.sGst = item.cGst;
                            item.iGst = 0;
                            tempCGst = item.cGst;
                            tempSGst = item.sGst;
                            tempIGst = item.iGst;
                        }

                        if (item.sGst == undefined || item.sGst <= 0) {
                            item.sGst = 0;
                        } else if (val == 'sGST') {
                            // item.cGst = item.sGst;
                            item.iGst = 0;

                            tempCGst = item.cGst;
                            tempSGst = item.sGst;
                            tempIGst = item.iGst;
                        }
                        if (item.iGst == undefined || item.iGst <= 0) {
                            item.iGst = 0;
                        } else if (val == 'iGST') {
                            item.cGst = item.sGst = 0;

                            tempCGst = item.cGst;
                            tempSGst = item.sGst;
                            tempIGst = item.iGst;
                        }

                        //if (item.cGst < 10) {
                        //    if (item.cGst.toString().length > 4) {
                        //        tempCGst = 0;
                        //        item.cGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        //if (item.cGst >= 10 || item.cGst <= 100) {
                        //    if (item.cGst.toString().length > 5) {
                        //        tempCGst = 0;
                        //        item.cGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        //if (item.sGst < 10) {
                        //    if (item.sGst.toString().length > 4) {
                        //        tempSGst = 0;
                        //        item.sGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        //if (item.sGst >= 10 || item.sGst <= 100) {
                        //    if (item.sGst.toString().length > 5) {
                        //        tempSGst = 0;
                        //        item.sGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        //if (item.iGst < 10) {
                        //    if (item.iGst.toString().length > 4) {
                        //        tempIGst = 0;
                        //        item.iGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        //if (item.iGst >= 10 || item.iGst <= 100) {
                        //    if (item.iGst.toString().length > 5) {
                        //        tempIGst = 0;
                        //        item.iGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        if ((parseFloat(item.cGst) + parseFloat(item.sGst)) > 100) {
                            item.cGst = item.sGst = tempCGst = tempSGst = 0;
                            swal("Error!", 'Please enter valid TAX %');
                        } else if (item.iGst > 100) {
                            item.iGst = tempIGst = 0;
                            swal("Error!", 'Please enter valid TAX %');
                        }

                        if (item.itemFreightTAX > 100) {
                            item.itemFreightTAX = 0;
                            swal("Error!", 'Please enter valid Freight TAX %');
                        }


                        item.itemPrice = (item.itemPrice + ((item.itemPrice / 100) * (parseFloat(item.cGst) + parseFloat(item.sGst) + parseFloat(item.iGst))) + (parseFloat(item.itemfreight)));

                        item.unitPrice = tempUnitPrice;
                        item.cGst = tempCGst
                        item.sGst = tempSGst;
                        item.iGst = tempIGst;

                        if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                            // $scope.gstValidation = true;
                            // not mandatory
                        };
                    };


                    if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'Negotiation Ended' || $scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                       
                        var tempRevUnitPrice = item.revUnitPrice;
                        var tempitemRevFreightCharges = itemRevfreight;
                        tempCGst = item.cGst;
                        tempSGst = item.sGst;
                        tempIGst = item.iGst;

                        if (item.revUnitPrice == undefined || item.revUnitPrice <= 0) {
                            item.revUnitPrice = 0;
                        };
                        if (item.itemRevFreightCharges == undefined || item.itemRevFreightCharges <= 0) {
                            item.itemRevFreightCharges = 0;
                        };

                        item.revitemPrice = item.revUnitPrice * item.productQuantity;
                        

                        if (item.cGst == undefined || item.cGst <= 0) {
                            item.cGst = 0;
                        };
                        if (item.sGst == undefined || item.sGst <= 0) {
                            item.sGst = 0;
                        };
                        if (item.iGst == undefined || item.iGst <= 0) {
                            item.iGst = 0;
                        };

                        //RevfreightCharges = itemRevfreight + ((itemRevfreight / 100) * (item.cGst + item.sGst + item.iGst))

                        item.revitemPrice = (item.revitemPrice + ((item.revitemPrice / 100) * (parseFloat(item.cGst) + parseFloat(item.sGst) + parseFloat(item.iGst))) + (parseFloat(itemRevfreight)));
                        itemRevfreight = tempitemRevFreightCharges;
                        item.revUnitPrice = tempRevUnitPrice;
                        item.cGst = tempCGst
                        item.sGst = tempSGst;
                        item.iGst = tempIGst;

                        if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                            // $scope.gstValidation = true;
                            // not mandatory
                        }
                    }
                });


                // For disablig non core item prices when total price is not gretaer than 0
                if ($scope.totalprice > 0)
                {
                    $scope.fieldValidation();
                }
                // For disablig non core item prices when total price is not gretaer than 0

            };

            $scope.regretError = false;


            $scope.freightCharges = 0;
            $scope.freightChargesTaxPercentage = 0;
            $scope.freightChargesWithTax = 0;

            $scope.revfreightCharges = 0;
            $scope.revfreightChargesWithTax = 0;

            $scope.freightChargeslocal = 0;
            $scope.freightChargesTaxPercentagelocal = 0;
            $scope.freightChargesWithTaxlocal = 0;

            $scope.revfreightChargeslocal = 0;
            $scope.revfreightChargesWithTaxlocal = 0;




            $scope.packingCharges = 0;
            $scope.packingChargesTaxPercentage = 0;
            $scope.packingChargesWithTax = 0;

            $scope.revpackingCharges = 0;
            $scope.revpackingChargesWithTax = 0;

            $scope.packingChargeslocal = 0;
            $scope.packingChargesTaxPercentagelocal = 0;
            $scope.packingChargesWithTaxlocal = 0;

            $scope.revpackingChargeslocal = 0;
            $scope.revpackingChargesWithTaxlocal = 0;





            $scope.installationCharges = 0;
            $scope.installationChargesTaxPercentage = 0;
            $scope.installationChargesWithTax = 0;

            $scope.revinstallationCharges = 0;
            $scope.revinstallationChargesWithTax = 0;

            $scope.installationChargeslocal = 0;
            $scope.installationChargesTaxPercentagelocal = 0;
            $scope.installationChargesWithTaxlocal = 0;

            $scope.revinstallationChargeslocal = 0;
            $scope.revinstallationChargesWithTaxlocal = 0;




            $scope.getTotalPrice = function (discountAmount, freightCharges, totalprice,
                packingCharges, packingChargesTaxPercentage, packingChargesWithTax,
                installationCharges, installationChargesTaxPercentage, installationChargesWithTax,
                freightChargesTaxPercentage, freightChargesWithTax) {
                if (!$scope.auctionItem) {
                    return;
                }


                $scope.regretError = false;
                $scope.priceValidationsVendor = '';
                $scope.taxValidation = false;
                $scope.ItemPriceValidation = false;
                $scope.discountfreightValidation = false;

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors.length > 0 && $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {

                    $scope.revfreightCharges = $scope.precisionRound(parseFloat(freightCharges), 2);
                    $scope.freightChargesTaxPercentage = $scope.precisionRound(parseFloat(freightChargesTaxPercentage), 2);
                    $scope.revfreightChargesWithTax = $scope.precisionRound(parseFloat(freightChargesWithTax), 2);

                    $scope.revpackingCharges = $scope.precisionRound(parseFloat(packingCharges), 2);
                    $scope.packingChargesTaxPercentage = $scope.precisionRound(parseFloat(packingChargesTaxPercentage), 2);
                    $scope.revpackingChargesWithTax = $scope.precisionRound(parseFloat(packingChargesWithTax), 2);

                    $scope.revinstallationCharges = $scope.precisionRound(parseFloat(installationCharges), 2);
                    $scope.installationChargesTaxPercentage = $scope.precisionRound(parseFloat(installationChargesTaxPercentage), 2);
                    $scope.revinstallationChargesWithTax = $scope.precisionRound(parseFloat(installationChargesWithTax), 2);
                }

                $scope.freightCharges = $scope.precisionRound(parseFloat(freightCharges), 2);
                $scope.freightChargesTaxPercentage = $scope.precisionRound(parseFloat(freightChargesTaxPercentage), 2);
                $scope.freightChargesWithTax = $scope.precisionRound(parseFloat(freightChargesWithTax), 2);

                $scope.packingCharges = $scope.precisionRound(parseFloat(packingCharges), 2);
                $scope.packingChargesTaxPercentage = $scope.precisionRound(parseFloat(packingChargesTaxPercentage), 2);
                $scope.packingChargesWithTax = $scope.precisionRound(parseFloat(packingChargesWithTax), 2);

                $scope.installationCharges = $scope.precisionRound(parseFloat(installationCharges), 2);
                $scope.installationChargesTaxPercentage = $scope.precisionRound(parseFloat(installationChargesTaxPercentage), 2);
                $scope.installationChargesWithTax = $scope.precisionRound(parseFloat(installationChargesWithTax), 2);

                $scope.discountAmount = $scope.precisionRound(parseFloat(discountAmount), 2);

                if (!$scope.auctionItem.isTabular) {
                    $scope.totalprice = totalprice;
                    if (isNaN($scope.totalprice) || $scope.totalprice == undefined || $scope.totalprice < 0 || $scope.totalprice == '') {
                        //$("#totalprice").val(0);
                        $scope.vendorBidPrice = 0;
                        $scope.vendorBidPriceWithoutDiscount = 0;
                    }
                }
                else {
                    $scope.auctionItem.isUOMDifferent = false;
                    if ($scope.auctionItemVendor && $scope.auctionItemVendor.listRequirementItems.length > 0) {

                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                            if (item.unitPrice > 0) {
                                item.isRegret = false;
                                item.regretComments = '';
                            }

                            if (item.isRegret && (item.regretComments == '' || item.regretComments == undefined || item.regretComments == null)) {
                                $scope.regretError = true;
                            }

                            if (item.productQuantityIn.toUpperCase() != item.vendorUnits.toUpperCase()) {
                                $scope.auctionItem.isUOMDifferent = true;
                            }

                            if (isNaN(item.itemPrice) || item.itemPrice == undefined || item.itemPrice < 0) {
                                $scope.ItemPriceValidation = true;
                            }

                            $scope.regretHandle(item);

                        });

                    }
                }



                $scope.freightChargeslocal = $scope.precisionRound(parseFloat($scope.freightCharges), 2);
                if (isNaN($scope.freightCharges) || $scope.freightCharges == undefined || $scope.freightCharges < 0) {
                    $scope.freightChargeslocal = 0;
                    $scope.discountfreightValidation = true;
                }
                $scope.freightChargeslocal = $scope.precisionRound(parseFloat($scope.freightChargeslocal), 2);

                $scope.freightChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.freightChargesTaxPercentage), 2);
                if (isNaN($scope.freightChargesTaxPercentage) || $scope.freightChargesTaxPercentage == undefined || $scope.freightChargesTaxPercentage < 0) {
                    $scope.freightChargesTaxPercentagelocal = 0;
                }
                $scope.freightChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.freightChargesTaxPercentagelocal), 2);

                $scope.freightChargesWithTax = $scope.precisionRound(parseFloat($scope.freightChargeslocal +
                    (($scope.freightChargeslocal / 100) * ($scope.freightChargesTaxPercentagelocal))), 2);
                $scope.freightChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.freightChargesWithTax), 2);
                if (isNaN($scope.freightChargesWithTax) || $scope.freightChargesWithTax == undefined || $scope.freightChargesWithTax < 0) {
                    $scope.freightChargesWithTaxlocal = 0;
                }
                $scope.freightChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.freightChargesWithTaxlocal), 2);




                $scope.packingChargeslocal = $scope.precisionRound(parseFloat($scope.packingCharges), 2);
                if (isNaN($scope.packingCharges) || $scope.packingCharges == undefined || $scope.packingCharges < 0) {
                    $scope.packingChargeslocal = 0;
                }
                $scope.packingChargeslocal = $scope.precisionRound(parseFloat($scope.packingChargeslocal), 2);

                $scope.packingChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.packingChargesTaxPercentage), 2);
                if (isNaN($scope.packingChargesTaxPercentage) || $scope.packingChargesTaxPercentage == undefined || $scope.packingChargesTaxPercentage < 0) {
                    $scope.packingChargesTaxPercentagelocal = 0;
                }
                $scope.packingChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.packingChargesTaxPercentagelocal), 2);

                $scope.packingChargesWithTax = $scope.precisionRound(parseFloat($scope.packingChargeslocal +
                    (($scope.packingChargeslocal / 100) * ($scope.packingChargesTaxPercentagelocal))), 2);
                $scope.packingChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.packingChargesWithTax), 2);
                if (isNaN($scope.packingChargesWithTax) || $scope.packingChargesWithTax == undefined || $scope.packingChargesWithTax < 0) {
                    $scope.packingChargesWithTaxlocal = 0;
                }
                $scope.packingChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.packingChargesWithTaxlocal), 2);

                $scope.installationChargeslocal = $scope.precisionRound(parseFloat($scope.installationCharges), 2);
                if (isNaN($scope.installationCharges) || $scope.installationCharges == undefined || $scope.installationCharges < 0) {
                    $scope.installationChargeslocal = 0;
                }
                $scope.installationChargeslocal = $scope.precisionRound(parseFloat($scope.installationChargeslocal), 2);

                $scope.installationChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.installationChargesTaxPercentage), 2);
                if (isNaN($scope.installationChargesTaxPercentage) || $scope.installationChargesTaxPercentage == undefined ||
                    $scope.installationChargesTaxPercentage < 0) {
                    $scope.installationChargesTaxPercentagelocal = 0;
                }
                $scope.installationChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.installationChargesTaxPercentagelocal), 2);

                $scope.installationChargesWithTax = $scope.precisionRound(parseFloat($scope.installationChargeslocal +
                    (($scope.installationChargeslocal / 100) * ($scope.installationChargesTaxPercentagelocal))), 2);
                $scope.installationChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.installationChargesWithTax), 2);
                if (isNaN($scope.installationChargesWithTax) || $scope.installationChargesWithTax == undefined || $scope.installationChargesWithTax < 0) {
                    $scope.installationChargesWithTaxlocal = 0;
                }
                $scope.installationChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.installationChargesWithTaxlocal), 2);


                if ($scope.freightChargesTaxPercentage > 100 || parseFloat($("#freightChargesTaxPercentage").val()) > 100) {
                    $("#freightChargesTaxPercentage").val(0);
                    swal("Error!", 'Please enter valid TAX % for Freight Charges');
                };

                if ($scope.packingChargesTaxPercentage > 100 || parseFloat($("#packingChargesTaxPercentage").val()) > 100) {
                    $("#packingChargesTaxPercentage").val(0);
                    swal("Error!", 'Please enter valid TAX % for Packing Charges');
                };

                if ($scope.installationChargesTaxPercentage > 100 || parseFloat($("#installationChargesTaxPercentage").val()) > 100) {
                    $("#installationChargesTaxPercentage").val(0);
                    swal("Error!", 'Please enter valid TAX % for Installation Charges');
                };


                $scope.discountAmountlocal = $scope.discountAmount;
                if (isNaN($scope.discountAmount) || $scope.discountAmount == undefined || $scope.discountAmount < 0) {
                    $scope.discountAmountlocal = 0;
                    $scope.discountfreightValidation = true;
                }
                $scope.discountAmountlocal = $scope.precisionRound(parseFloat($scope.discountAmountlocal), 2);

                if ($scope.auctionItem.isTabular) {



                    $scope.totalprice = _.sumBy($scope.auctionItemVendor.listRequirementItems, 'itemPrice');

                    $scope.totalprice = $scope.precisionRound(parseFloat($scope.totalprice), 2);


                    if ($scope.auctionItem.isDiscountQuotation == 2) {
                        $scope.totalprice = 0;
                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                            item = $scope.handlePrecision(item, 2);
                            $scope.totalprice += item.netPrice * item.productQuantity;
                            item = $scope.handlePrecision(item, 2);
                        });
                    }

                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.totalprice), 2) +
                        $scope.precisionRound(parseFloat($scope.freightChargesWithTaxlocal), 2) +
                        $scope.precisionRound(parseFloat($scope.packingChargesWithTaxlocal), 2) +
                        $scope.precisionRound(parseFloat($scope.installationChargesWithTaxlocal), 2);

                    $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice;
                    $scope.vendorBidPrice = $scope.vendorBidPrice - $scope.discountAmountlocal;
                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.vendorBidPrice), 2);
                    if (!$scope.vendorBidPrice || isNaN($scope.vendorBidPrice)) {
                        $scope.vendorBidPrice = 0;
                    }
                }
                if (!$scope.auctionItem.isTabular) {
                    $scope.totalprice = $scope.precisionRound(parseFloat($scope.totalprice), 2);

                    $scope.calculatedSumOfAllTaxes = 0;
                    $scope.listRequirementTaxes.forEach(function (tax, index) {
                        if (tax.taxPercentage != '' && tax.taxPercentage != undefined) {
                            $scope.calculatedSumOfAllTaxes += $scope.precisionRound(parseFloat($scope.totalprice), 2)
                                * $scope.precisionRound(parseFloat(tax.taxPercentage), 2) / 100;
                        }
                    })

                    $scope.vendorTaxes = $scope.calculatedSumOfAllTaxes;

                    $scope.listRequirementTaxes.forEach(function (item, index) {
                        if (item.taxName == null || item.taxName == '' || item.taxName == undefined) {
                            $scope.taxValidation = true;
                        }
                        if (item.taxPercentage < 0 || item.taxPercentage == null || item.taxPercentage == undefined) {
                            $scope.taxValidation = true;
                        }
                        if (item.taxPercentage <= 0) {
                            item.taxPercentage = 0;
                        }
                    });

                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.totalprice), 2) +
                        $scope.precisionRound(parseFloat($scope.vendorTaxes), 2) +
                        $scope.precisionRound(parseFloat($scope.freightChargesWithTaxlocal), 2) +
                        $scope.precisionRound(parseFloat($scope.packingChargesWithTaxlocal), 2) +
                        $scope.precisionRound(parseFloat($scope.installationChargesWithTaxlocal), 2);

                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.vendorBidPrice), 2);

                    $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice;
                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.vendorBidPrice), 2) -
                        $scope.precisionRound(parseFloat($scope.discountAmountlocal), 2);
                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.vendorBidPrice), 2);
                    if (!$scope.vendorBidPrice || isNaN($scope.vendorBidPrice)) {
                        $scope.vendorBidPrice = 0;
                    }
                }
            };


            $scope.getRevTotalPrice = function (revfreightCharges, revtotalprice,
                revpackingCharges, packingChargesTaxPercentage, revpackingChargesWithTax,
                revinstallationCharges, installationChargesTaxPercentage, revinstallationChargesWithTax,
                freightChargesTaxPercentage, revfreightChargesWithTax)
            {

                $scope.revfreightCharges = revfreightCharges;
                $scope.revpackingCharges = revpackingCharges;
                $scope.revinstallationCharges = revinstallationCharges;
                $scope.ItemPriceValidation = false;
                $scope.discountfreightValidation = false;

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors.length > 0 && $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {

                    $scope.revfreightCharges = $scope.precisionRound(parseFloat(revfreightCharges), 2);
                    $scope.freightChargesTaxPercentage = $scope.precisionRound(parseFloat(freightChargesTaxPercentage), 2);
                    $scope.revfreightChargesWithTax = $scope.precisionRound(parseFloat(revfreightChargesWithTax), 2);

                    $scope.revfreightCharges = $scope.precisionRound(parseFloat(revfreightCharges), 2);
                    $scope.freightChargesTaxPercentage = $scope.precisionRound(parseFloat(freightChargesTaxPercentage), 2);
                    $scope.revfreightChargesWithTax = $scope.precisionRound(parseFloat(revfreightChargesWithTax), 2);

                    $scope.revinstallationCharges = $scope.precisionRound(parseFloat(revinstallationCharges), 2);
                    $scope.installationChargesTaxPercentage = $scope.precisionRound(parseFloat(installationChargesTaxPercentage), 2);
                    $scope.revinstallationChargesWithTax = $scope.precisionRound(parseFloat(revinstallationChargesWithTax), 2);
                }

                $scope.freightChargesTaxPercentage = $scope.precisionRound(parseFloat(freightChargesTaxPercentage), 2);
                $scope.packingChargesTaxPercentage = $scope.precisionRound(parseFloat(packingChargesTaxPercentage), 2);
                $scope.installationChargesTaxPercentage = $scope.precisionRound(parseFloat(installationChargesTaxPercentage), 2);

                $scope.revfreightCharges = $scope.precisionRound(parseFloat(revfreightCharges), 2);
                $scope.revfreightChargesWithTax = $scope.precisionRound(parseFloat(revfreightChargesWithTax), 2);

                $scope.revpackingCharges = $scope.precisionRound(parseFloat(revpackingCharges), 2);
                $scope.revpackingChargesWithTax = $scope.precisionRound(parseFloat(revpackingChargesWithTax), 2);

                $scope.revinstallationCharges = $scope.precisionRound(parseFloat(revinstallationCharges), 2);
                $scope.revinstallationChargesWithTax = $scope.precisionRound(parseFloat(revinstallationChargesWithTax), 2);


                if (!$scope.auctionItem.isTabular) {
                    $scope.revtotalprice = revtotalprice;
                    if (isNaN($scope.revtotalprice) || $scope.revtotalprice == undefined || $scope.revtotalprice < 0 || $scope.revtotalprice == '') {
                        $("#revtotalprice").val(0);
                        $scope.revvendorBidPrice = 0;
                    }
                }
                else {
                    if ($scope.auctionItemVendor.length > 0) {
                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                            if (item.oldRevUnitPrice > 0 && item.revUnitPrice > 0 && item.oldRevUnitPrice > item.revUnitPrice && $scope.auctionItem.status == 'STARTED') {
                                if ($scope.auctionItem.isDiscountQuotation == 0) {
                                    var reductionval = parseFloat(item.oldRevUnitPrice).toFixed(2) - parseFloat(item.revUnitPrice).toFixed(2);
                                    //reductionval = parseFloat(reductionval).toFixed(2);                            
                                    if (reductionval >= item.itemMinReduction && reductionval > 0 && item.itemMinReduction > 0) {
                                        $scope.ItemPriceValidation = false;

                                    } else if (reductionval < item.itemMinReduction && reductionval > 0 && item.itemMinReduction > 0) {
                                        $scope.ItemPriceValidation = true;
                                        item.revUnitPrice = item.oldRevUnitPrice;
                                        $scope.getData();
                                        swal("Error!", 'Your Unit price reduction should be greater than Min.unit Price limit :' + item.itemMinReduction, "error");
                                        return;
                                    }
                                }

                            }

                            if (isNaN(item.revitemPrice) || item.revitemPrice == undefined || item.revitemPrice < 0) {
                                $scope.ItemPriceValidation = true;
                            }
                        })
                    }
                }


                $scope.revfreightChargeslocal = $scope.revfreightCharges;
                if (isNaN($scope.revfreightCharges) || $scope.revfreightCharges == undefined || $scope.revfreightCharges < 0) {
                    $scope.revfreightChargeslocal = 0;
                    $scope.discountfreightValidation = true;
                }
                $scope.revfreightChargeslocal = $scope.precisionRound(parseFloat($scope.revfreightChargeslocal), 2);

                $scope.freightChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.freightChargesTaxPercentage), 2);
                if (isNaN($scope.freightChargesTaxPercentage) || $scope.freightChargesTaxPercentage == undefined || $scope.freightChargesTaxPercentage < 0) {
                    $scope.freightChargesTaxPercentagelocal = 0;
                }
                $scope.freightChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.freightChargesTaxPercentagelocal), 2);

                $scope.revfreightChargesWithTax = $scope.precisionRound(parseFloat($scope.revfreightChargeslocal +
                    (($scope.revfreightChargeslocal / 100) * ($scope.freightChargesTaxPercentagelocal))), 2);
                $scope.revfreightChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.revfreightChargesWithTax), 2);
                if (isNaN($scope.revfreightChargesWithTax) || $scope.revfreightChargesWithTax == undefined || $scope.revfreightChargesWithTax < 0) {
                    $scope.revfreightChargesWithTaxlocal = 0;
                }
                $scope.revfreightChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.revfreightChargesWithTaxlocal), 2);


                               
                $scope.revpackingChargeslocal = $scope.precisionRound(parseFloat($scope.revpackingCharges), 2);
                if (isNaN($scope.revpackingCharges) || $scope.revpackingCharges == undefined || $scope.revpackingCharges < 0) {
                    $scope.revpackingChargeslocal = 0;
                }
                $scope.revpackingChargeslocal = $scope.precisionRound(parseFloat($scope.revpackingChargeslocal), 2);

                $scope.packingChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.packingChargesTaxPercentage), 2);
                if (isNaN($scope.packingChargesTaxPercentage) || $scope.packingChargesTaxPercentage == undefined || $scope.packingChargesTaxPercentage < 0) {
                    $scope.packingChargesTaxPercentagelocal = 0;
                }
                $scope.packingChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.packingChargesTaxPercentagelocal), 2);

                $scope.revpackingChargesWithTax = $scope.precisionRound(parseFloat($scope.revpackingChargeslocal +
                    (($scope.revpackingChargeslocal / 100) * ($scope.packingChargesTaxPercentagelocal))), 2);
                $scope.revpackingChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.revpackingChargesWithTax), 2);
                if (isNaN($scope.revpackingChargesWithTax) || $scope.revpackingChargesWithTax == undefined || $scope.revpackingChargesWithTax < 0) {
                    $scope.revpackingChargesWithTaxlocal = 0;
                }
                $scope.revpackingChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.revpackingChargesWithTaxlocal), 2);



                $scope.revinstallationChargeslocal = $scope.precisionRound(parseFloat($scope.revinstallationCharges), 2);
                if (isNaN($scope.revinstallationCharges) || $scope.revinstallationCharges == undefined || $scope.revinstallationCharges < 0) {
                    $scope.revinstallationChargeslocal = 0;
                }
                $scope.revinstallationChargeslocal = $scope.precisionRound(parseFloat($scope.revinstallationChargeslocal), 2);

                $scope.installationChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.installationChargesTaxPercentage), 2);
                if (isNaN($scope.installationChargesTaxPercentage) || $scope.installationChargesTaxPercentage == undefined ||
                    $scope.installationChargesTaxPercentage < 0) {
                    $scope.installationChargesTaxPercentagelocal = 0;
                }
                $scope.installationChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.installationChargesTaxPercentagelocal), 2);

                $scope.revinstallationChargesWithTax = $scope.precisionRound(parseFloat($scope.revinstallationChargeslocal +
                    (($scope.revinstallationChargeslocal / 100) * ($scope.installationChargesTaxPercentagelocal))), 2);
                $scope.revinstallationChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.revinstallationChargesWithTax), 2);
                if (isNaN($scope.revinstallationChargesWithTax) || $scope.revinstallationChargesWithTax == undefined || $scope.revinstallationChargesWithTax < 0) {
                    $scope.revinstallationChargesWithTaxlocal = 0;
                }
                $scope.revinstallationChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.revinstallationChargesWithTaxlocal), 2);




                if ($scope.auctionItem.isTabular) {
                    $scope.revtotalprice = _.sumBy($scope.auctionItemVendor.listRequirementItems, 'revitemPrice');
                    if ($scope.auctionItem.isDiscountQuotation == 2) {

                        $scope.revtotalprice = 0;

                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                            item = $scope.handlePrecision(item, 2);
                            $scope.revtotalprice += item.revnetPrice * item.productQuantity;
                            item = $scope.handlePrecision(item, 2);
                        });
                    }



                    $scope.calculatedSumOfAllTaxes = 0;
                    $scope.listRequirementTaxes.forEach(function (tax, index) {
                        if (tax.taxPercentage != '' && tax.taxPercentage != undefined) {
                            $scope.calculatedSumOfAllTaxes += $scope.revtotalprice * tax.taxPercentage / 100;
                        }
                    })
                    $scope.vendorTaxes = $scope.calculatedSumOfAllTaxes;

                    $scope.revvendorBidPrice = $scope.precisionRound(parseFloat($scope.revtotalprice), 2) +
                        $scope.precisionRound(parseFloat($scope.vendorTaxes), 2) +

                        $scope.precisionRound(parseFloat($scope.revfreightChargesWithTaxlocal), 2) +
                        $scope.precisionRound(parseFloat($scope.revpackingChargesWithTaxlocal), 2) +
                        $scope.precisionRound(parseFloat($scope.revinstallationChargesWithTaxlocal), 2);

                    $scope.revvendorBidPrice = $scope.precisionRound(parseFloat($scope.revvendorBidPrice), 2);
                    if (!$scope.revvendorBidPrice || isNaN($scope.revvendorBidPrice)) {
                        $scope.revvendorBidPrice = 0;
                    }

                    $scope.reduceBidValue = $scope.precisionRound(parseFloat($scope.auctionItem.minPrice - $scope.revvendorBidPrice), 2);

                    $scope.reduceBidAmount();

                    //reduceBidValue

                }
                if (!$scope.auctionItem.isTabular) {
                    $scope.revtotalprice = $scope.precisionRound(parseFloat(revtotalprice), 2);


                    $scope.calculatedSumOfAllTaxes = 0;
                    $scope.listRequirementTaxes.forEach(function (tax, index) {
                        if (tax.taxPercentage != '' && tax.taxPercentage != undefined) {
                            $scope.calculatedSumOfAllTaxes += $scope.revtotalprice * tax.taxPercentage / 100;
                        }
                    })
                    $scope.vendorTaxes = $scope.calculatedSumOfAllTaxes;

                    $scope.revvendorBidPrice = $scope.precisionRound(parseFloat($scope.revtotalprice), 2) +
                        $scope.precisionRound(parseFloat($scope.vendorTaxes), 2) +

                        $scope.precisionRound(parseFloat($scope.revfreightChargesWithTaxlocal), 2) +
                        $scope.precisionRound(parseFloat($scope.revpackingChargesWithTaxlocal), 2) +
                        $scope.precisionRound(parseFloat($scope.revinstallationChargesWithTaxlocal), 2);

                    $scope.revvendorBidPrice = $scope.precisionRound(parseFloat($scope.revvendorBidPrice), 2);
                    if (!$scope.revvendorBidPrice || isNaN($scope.revvendorBidPrice)) {
                        $scope.revvendorBidPrice = 0;
                    }
                }

            }





            $scope.currencyValidationStyle = {};

            $scope.uploadQuotation = function (quotationType) {
                $scope.currencyValidationStyle = {};
                if ($scope.auctionItem.auctionVendors[0].selectedVendorCurrency == null ||
                    $scope.auctionItem.auctionVendors[0].selectedVendorCurrency == '' ||
                    $scope.auctionItem.auctionVendors[0].selectedVendorCurrency == undefined) {
                    document.getElementById('priceDiv').scrollIntoView();
                    $scope.currencyValidationStyle = {
                        'border-color': 'red'
                    };
                    growlService.growl('Please Select Currency', "inverse");
                    return;
                }

                //if ($scope.regretError) {
                //    growlService.growl("Please enter regret comments.", "inverse");
                //    return;
                //}

                var uploadQuotationError = false;

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                    if (uploadQuotationError == true) {
                        return false;
                    }
                    var slno = index + 1;
                    if (item.isRegret) {
                        if (item.regretComments == '' || item.regretComments == null || item.regretComments == undefined) {
                            growlService.growl("Please enter regret comments.", "inverse");
                            uploadQuotationError = true;
                            return false;
                        }
                    } else {
                        if (item.unitPrice <= 0 && item.isCoreProductCategory > 0) {
                            growlService.growl("Please enter Unit price for Product " + slno + ".", "inverse");
                            uploadQuotationError = true;
                            return false;
                        }
                        else if (item.unitPrice > 0 && (+item.iGst == 0 && (+item.cGst == 0 && +item.sGst == 0)) && ($scope.auctionItem.auctionVendors[0].selectedVendorCurrency == 'INR')) {
                            growlService.growl("Please enter SGST/CGST/IGST for Product " + slno + ".", "inverse");
                            uploadQuotationError = true;
                            return false;
                        }
                        else if (item.unitPrice > 0 && (+item.cGst == 0 && +item.sGst > 0) && ($scope.auctionItem.auctionVendors[0].selectedVendorCurrency == 'INR')) {
                            growlService.growl("Please enter CGST for Product " + slno + ".", "inverse");
                            uploadQuotationError = true;
                            return false;
                        }
                        else if (item.unitPrice > 0 && (+item.cGst > 0 && +item.sGst == 0) && ($scope.auctionItem.auctionVendors[0].selectedVendorCurrency == 'INR')) {
                            growlService.growl("Please enter SGST for Product " + slno + ".", "inverse");
                            uploadQuotationError = true;
                            return false;
                        }
                    }


                });

                var TaxFiledValidation = 0;

                if ($scope.CSGSTFields == false) {
                    TaxFiledValidation = 1;
                }

                if (uploadQuotationError == true) {
                    return;
                }

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            subItem.dateCreated = "/Date(1561000200000+0530)/";
                            subItem.dateModified = "/Date(1561000200000+0530)/";
                        });
                    }
                });

                var params = {
                    'quotationObject': $scope.auctionItemVendor.listRequirementItems, // 1
                    'userID': userService.getUserId(),
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': userService.getUserToken(),
                    'price': $scope.totalprice, // 5
                    'tax': $scope.vendorTaxes,
                    'vendorBidPrice': $scope.vendorBidPrice,
                    'warranty': $scope.warranty,
                    'duration': $scope.duration,
                    'payment': $scope.payment, // 10
                    'gstNumber': $scope.gstNumber,
                    'validity': $scope.validity,
                    'otherProperties': $scope.otherProperties,
                    'quotationType': quotationType,
                    'revised': 0, // 15
                    'discountAmount': $scope.discountAmount,
                    'listRequirementTaxes': $scope.listRequirementTaxes,
                    'quotationAttachment': $scope.quotationAttachment,
                    'multipleAttachments': $scope.multipleAttachmentsList,
                    'TaxFiledValidation': TaxFiledValidation,

                    'freightCharges': $scope.freightCharges, // 20
                    'freightChargesTaxPercentage': $scope.freightChargesTaxPercentage,
                    'freightChargesWithTax': $scope.freightChargesWithTax,

                    'packingCharges': $scope.packingCharges,
                    'packingChargesTaxPercentage': $scope.packingChargesTaxPercentage,
                    'packingChargesWithTax': $scope.packingChargesWithTax, // 25

                    'installationCharges': $scope.installationCharges,
                    'installationChargesTaxPercentage': $scope.installationChargesTaxPercentage,
                    'installationChargesWithTax': $scope.installationChargesWithTax,

                    'revfreightCharges': $scope.revfreightCharges,
                    'revfreightChargesWithTax': $scope.revfreightChargesWithTax, // 30

                    'revpackingCharges': $scope.revpackingCharges,
                    'revpackingChargesWithTax': $scope.revpackingChargesWithTax,

                    'revinstallationCharges': $scope.revinstallationCharges,
                    'revinstallationChargesWithTax': $scope.revinstallationChargesWithTax,

                    'selectedVendorCurrency': $scope.auctionItem.auctionVendors[0].selectedVendorCurrency, //35
                    'isVendAckChecked': $scope.auctionItem.auctionVendors[0].isVendAckChecked
                };

                params.unitPrice = $scope.auctionItem.unitPrice;
                params.productQuantity = $scope.auctionItem.productQuantity;
                params.cGst = $scope.auctionItem.cGst;
                params.sGst = $scope.auctionItem.sGst;
                params.iGst = $scope.auctionItem.iGst;
                params.INCO_TERMS = $scope.auctionItem.auctionVendors[0].INCO_TERMS;

                //auctionItem.auctionVendors[0].INCO_TERMS

                if (params.warranty == null || params.warranty == '' || params.warranty == undefined) {
                    params.warranty = '';
                    growlService.growl('Please enter Warranty.', "inverse");
                    return false;
                }

                if (params.validity == null || params.validity == '' || params.validity == undefined) {
                    params.validity = '';
                    growlService.growl('Please enter Price Validity.', "inverse");
                    return false;
                }

                if (params.otherProperties == null || params.otherProperties == '' || params.otherProperties == undefined) {
                    params.otherProperties = '';
                }

                if (params.duration == null || params.duration == '' || params.duration == undefined) {
                    params.duration = '';
                    growlService.growl('Please enter Delivery Terms.', "inverse");
                    return false;
                }

                if (params.payment == null || params.payment == '' || params.payment == undefined) {
                    params.payment = '';
                    growlService.growl('Please enter Payment Terms.', "inverse");
                    return false;
                }

                //if (params.INCO_TERMS == null || params.INCO_TERMS == '' || params.INCO_TERMS == undefined) {
                //    params.payment = '';
                //    if ($scope.totalprice > 0) {
                //        growlService.growl('Please select Inco Terms.', "inverse");
                //        return false;
                //    }
                //}

                if (params.gstNumber == null || params.gstNumber == '' || params.gstNumber == undefined) {
                    params.gstNumber = '';
                }

                if ($scope.auctionItem.status == 'Negotiation Ended') {
                    params.price = $scope.revtotalprice,
                        params.tax = $scope.vendorTaxes,
                        params.vendorBidPrice = $scope.revvendorBidPrice,
                        params.revised = 1,

                        params.freightCharges = $scope.revfreightCharges,
                        params.revinstallationCharges = $scope.revinstallationCharges,
                        params.packingCharges = $scope.revpackingCharges
                }

                if (($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isQuotationPriceLimit && $scope.vendorBidPrice > $scope.auctionItem.quotationPriceLimit) {
                    swal("Error!", 'Your Amount Should be less than Quotation Price limit :' + $scope.auctionItem.quotationPriceLimit, "error");
                    return;
                }

                $scope.taxEmpty = false;
                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                    if ((item.unitMRP > 0 && $scope.auctionItem.isDiscountQuotation == 1) || (item.unitPrice > 0 && $scope.auctionItem.isDiscountQuotation == 0)) {
                        if ((item.cGst == 0 || item.sGst == 0 || item.cGst == undefined || item.sGst == undefined || item.cGst > 100 || item.sGst > 100) && (item.iGst == 0 || item.iGst == undefined || item.iGst > 100)) {
                            // $scope.taxEmpty = true;
                            // NOT MANDATORY
                        }
                    } else {
                        if ((item.cGst == undefined || item.sGst == undefined) && (item.iGst == undefined)) {
                            // $scope.taxEmpty = true;
                            // NOT MANDATORY
                        }
                    }
                });


                if ($scope.taxEmpty === true) {

                    swal({
                        title: "warning!",
                        text: "Please fill TAXES slab",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    });

                }
                else {
                    auctionsService.IsValidQuotationUpload({ reqId: $scope.reqId, sessionid: userService.getUserToken()})
                        .then(function (isValid) {
                            auctionsService.getdate()
                                .then(function (responseFromServer) {
                                    var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                                    var auctionStart = new Date(parseInt($scope.auctionItem.startTime.substr(6)));
                                    var timeofauctionstart = auctionStart.getTime();
                                    var currentServerTime = dateFromServer.getTime();

                                    var quotationFreeTime = moment($scope.auctionItem.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                                    var quotationFreeTimeMoment = moment(quotationFreeTime);
                                    var today = moment();
                                    if (!isValid) {
                                        swal({
                                            title: "Thanks!",
                                            text: 'Quotation Freeze time has passed, you have lost an opportunity to participate in negotiation.',
                                            type: 'warning',
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        },
                                            function () {
                                                return;
                                            });

                                        return;
                                    }

                                    if ($scope.auctionItem.status == "Negotiation Ended") {
                                        auctionsService.uploadQuotation(params)
                                            .then(function (req) {
                                                if (req.errorMessage == '' || req.errorMessage == 'reviced') {
                                                    if (req.errorMessage == '') {
                                                        var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                                                    }

                                                    let successText = 'Your Quotation uploaded successfully';
                                                    let type = "success";


                                                    swal({
                                                        title: "Thanks!",
                                                        text: successText,
                                                        type: type,
                                                        showCancelButton: false,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "Ok",
                                                        closeOnConfirm: true
                                                    },
                                                        function () {
                                                            location.reload();
                                                        });

                                                } else {
                                                    swal({
                                                        title: "Error",
                                                        text: req.errorMessage,
                                                        type: "error",
                                                        showCancelButton: false,
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "Ok",
                                                        closeOnConfirm: true
                                                    });
                                                }
                                            });

                                    }
                                    else {
                                        if (timeofauctionstart - currentServerTime >= 3600000 &&
                                            $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                                            auctionsService.uploadQuotation(params)
                                                .then(function (req) {
                                                    if (req.errorMessage == '' || req.errorMessage == 'reviced') {
                                                        if (req.errorMessage == '') {
                                                            var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                                                        }

                                                        let successText = 'Your Quotation uploaded successfully';
                                                        let type = "success";

                                                        swal({
                                                            title: "Thanks!",
                                                            text: successText,
                                                            type: type,
                                                            showCancelButton: false,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Ok",
                                                            closeOnConfirm: true
                                                        },
                                                            function () {
                                                                location.reload();
                                                            });

                                                    } else {
                                                        swal({
                                                            title: "Error",
                                                            text: req.errorMessage,
                                                            type: "error",
                                                            showCancelButton: false,
                                                            confirmButtonColor: "#DD6B55",
                                                            confirmButtonText: "Ok",
                                                            closeOnConfirm: true
                                                        });
                                                    }
                                                });
                                        } else {

                                            var message = "You have missed the deadline for Quotation Submission"
                                            if ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 && $scope.auctionItem.status != "Negotiation Ended") { message = "Your Quotation Already Approved You cant Update Quotation Please Contact PRM360" }

                                            swal({
                                                title: "Error",
                                                text: message,
                                                type: "error",
                                                showCancelButton: false,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "Ok",
                                                closeOnConfirm: true
                                            });
                                            $scope.getData();
                                        }
                                    }
                                });
                        });
                }
            };

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {

                        if (id == "excelquotation") {
                            var bytearray = new Uint8Array(result);
                            $scope.quotationAttachment = $.makeArray(bytearray);
                            $scope.uploadquotationsfromexcel();
                        } else if (id == "clientsideupload") {
                            var bytearray = new Uint8Array(result);
                            $scope.quotationAttachment = $.makeArray(bytearray);
                            $scope.uploadclientsidequotation();
                            $scope.quotationAttachment = null;
                            $scope.file = [];
                            $scope.file.name = '';
                        }
                        else {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var index = _.indexOf($scope.auctionItemVendor.listRequirementItems, _.find($scope.auctionItemVendor.listRequirementItems, function (o) {
                                return o.productSNo == id;
                            }));
                            var obj = $scope.auctionItemVendor.listRequirementItems[index];
                            obj.itemAttachment = arrayByte;
                            obj.attachmentName = ItemFileName;
                            $scope.auctionItemVendor.listRequirementItems.splice(index, 1, obj);
                        }
                    });
            }

            $scope.QuotationRatioButton = {
                upload: 'generate'
            }

            $scope.rejectreson = '';
            $scope.rejectresonValidation = false;

            $scope.QuatationAprovelvalue = true;

            $scope.QuatationAprovel = function (userID, value, comment, action, auctionVendor) {
                var operationResult = 'Quotation Approved Successfully';
                if (value) {
                    operationResult = "Quotation Rejected Successfully";
                }
                if (auctionVendor && $scope.auctionItem.isTechScoreReq && auctionVendor.technicalScore <= 0 && !value) {
                    swal("Error!", 'Vendor can be approved only after Technical Score.', "error");
                    return;
                }

                if (auctionVendor.quotationRejectedComment) {
                    auctionVendor.quotationRejectedComment = auctionVendor.quotationRejectedComment.trim();
                }

                if ((userID == auctionVendor.vendorID) && value && (auctionVendor.quotationRejectedComment == '' || auctionVendor.quotationRejectedComment == undefined)) {
                    auctionVendor.rejectresonValidation = true;
                    return false;
                } else {
                    auctionVendor.rejectresonValidation = false;
                }

                var params = {
                    'vendorID': userID,
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': userService.getUserToken(),
                    'customerID': userService.getUserId(),
                    'value': value,
                    'reason': comment,
                    'technicalScore': auctionVendor.technicalScore,
                    'action': action
                };

                
                auctionsService.QuatationAprovel(params).then(function (req) {
                    if (req.errorMessage === '') {
                        swal({
                            title: "Done!",
                            text: operationResult,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                //$scope.recalculate('QUOTATION_APPROVAL_CUSTOMER', userID);
                                location.reload();
                            });

                    } else {
                        swal("Error!", req.errorMessage, "error");
                    }
                });
            };

            $scope.submitButtonShow = 0;

            $scope.submitButtonShowfunction = function (value, auctionVendor) {

                auctionVendor.quotationRejectedComment = '';
                $scope.submitButtonShow = value;
                let tempArr = _.filter($scope.vendorApprovals, function (vendor) {
                    return Number(vendor.vendorID) !== Number(auctionVendor.vendorID)
                });

                tempArr.push(auctionVendor);
                $scope.vendorApprovals = tempArr;

                $scope.auctionItem.auctionVendors.forEach(function (v, vI) {
                    if (v.vendorID == auctionVendor.vendorID) {
                        v.isApproveOrRejectClicked = true;
                    }
                })

            }

            $scope.saveApprovalStatus = function () {
                $scope.vendorApprovals.forEach(function (auctionVendor, index) {
                    if ($scope.auctionItem.status == 'Negotiation Ended') {
                        $scope.QuatationAprovel(auctionVendor.vendorID, auctionVendor.isRevQuotationRejected, auctionVendor.revQuotationRejectedComment, 'revquotation', auctionVendor);
                    } else {
                        $scope.QuatationAprovel(auctionVendor.vendorID, auctionVendor.isQuotationRejected, auctionVendor.quotationRejectedComment, 'quotation', auctionVendor);
                    }
                });
            };

            $scope.NegotiationSettingsValidationMessage = '';

            $scope.NegotiationTimeValidation = function (days, hours, mins, minReduction, rankComparision) {

                $scope.days = days;
                $scope.hours = hours;
                $scope.mins = mins;


                $log.info($scope.days);
                $log.info($scope.hours);
                $log.info($scope.mins);

                $scope.NegotiationSettingsValidationMessage = '';

                if ($scope.auctionItem.isDiscountQuotation != 2) {
                    if (parseFloat(minReduction) <= 0 || minReduction == undefined) {
                        $scope.NegotiationSettingsValidationMessage = 'Set Min 0.01 for Min.Amount reduction';
                        return;
                    }

                    if (parseFloat(rankComparision) <= 0 || rankComparision == undefined) {
                        $scope.NegotiationSettingsValidationMessage = 'Set Min 0.01 for Rank Comparision price';
                        return;
                    }

                    if (parseFloat(minReduction) >= 0 && parseFloat(rankComparision) > parseFloat(minReduction)) {
                        $scope.NegotiationSettingsValidationMessage = 'Please enter Valid Rank Comparision price less than Min. Amount reduction';
                        return;
                    }
                } else if ($scope.auctionItem.isDiscountQuotation == 2) {
                    $scope.NegotiationSettings.minReductionAmount = 0;
                    $scope.NegotiationSettings.rankComparision = 0;
                }

                if (days == undefined || days < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Days';
                    return;
                }
                if (hours < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                    return;
                }
                if (mins < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minunts';
                    return;
                }
                if (mins >= 60 || mins == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minunts';
                    return;
                }
                if (hours > 24 || hours == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                    return;
                }
                if (hours == 24 && mins > 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours & Minuts';
                    return;
                }
                if (mins < 5 && hours == 0 && days == 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Min 5 Minutes';
                    return;
                }
            }

            $scope.listRequirementTaxes = [];
            $scope.reqTaxSNo = 1;

            $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;

            $scope.requirementTaxes =
                {
                    taxSNo: $scope.reqTaxSNo++,
                    taxName: '',
                    taxPercentage: 0
                }

            $scope.AddTax = function () {

                if ($scope.listRequirementTaxes.length > 4) {
                    return;
                }
                $scope.requirementTaxes =
                    {
                        taxSNo: $scope.reqTaxSNo++,
                        taxName: '',
                        taxPercentage: 0
                    }
                $scope.listRequirementTaxes.push($scope.requirementTaxes);
                $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
                $scope.getTotalPrice($scope.discountAmount, $scope.freightCharges, $scope.totalprice);
            };

            $scope.deleteTax = function (SNo) {
                $scope.listRequirementTaxes = _.filter($scope.listRequirementTaxes, function (x) { return x.taxSNo !== SNo; });
                $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
                $scope.getTotalPrice($scope.discountAmount, $scope.freightCharges, $scope.totalprice);
            };

            $scope.reports = {};

            $scope.isReportGenerated = 0;

            $scope.GetReportsRequirement = function () {
                auctionsService.GetReportsRequirement({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.reports = response;
                        $scope.getData();
                        $scope.isReportGenerated = 1;
                    });
            }

            $scope.generateReports = function () {
                $scope.isReportGenerated = 0;
            }

            $scope.updatePriceCap = function (auctionVendor) {
                let levelPrice = 1;
                $scope.formRequest.priceCapValueMsg = '';
                let l1Vendor = $scope.precisionRound(parseFloat($scope.auctionItem.auctionVendors[0].totalPriceIncl), 2);

                if ($scope.auctionItem.auctionVendors[0].companyName == 'PRICE_CAP') {
                    levelPrice = 2;
                    l1Vendor = $scope.precisionRound(parseFloat($scope.auctionItem.auctionVendors[1].totalPriceIncl), 2);
                }

                if ($scope.precisionRound(parseFloat($scope.formRequest.priceCapValue), 2) > 0 && $scope.precisionRound(parseFloat($scope.formRequest.priceCapValue), 2) < l1Vendor) {
                    let priceDetails = {
                        uID: auctionVendor.vendorID,
                        reqID: $scope.reqId,
                        price: $scope.formRequest.priceCapValue,
                        sessionID: userService.getUserToken()
                    };

                    auctionsService.UpdatePriceCap(priceDetails)
                        .then(function (response) {
                            if (response.errorMessage == '') {
                                growlService.growl('Successfully updated price cap.', "success");
                                $scope.formRequest.priceCapValueMsg = '';
                                auctionVendor.totalPriceIncl = $scope.formRequest.priceCapValue;
                            }
                            else {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                        });
                }
                else {
                    $scope.formRequest.priceCapValueMsg = 'Price cap amount should be less L' + levelPrice + ' price ' + l1Vendor;
                    // growlService.growl('Price cap amount should be more than 0  and less than L1 price ' + l1Vendor, "inverse");
                }
            }

            $scope.savepricecap = function () {

                //if ($scope.IS_CB_ENABLED == true) {
                //    $scope.auctionItem.reqType = 'REGULAR';
                //    $scope.auctionItem.priceCapValue = 0;
                //}

                $scope.l1Price = $scope.auctionItem.auctionVendors[0].totalPriceIncl;

                $scope.l1CompnyName = $scope.auctionItem.auctionVendors[0].companyName;

                if ($scope.l1CompnyName == 'PRICE_CAP') {
                    $scope.l1Price = $scope.auctionItem.auctionVendors[1].totalPriceIncl;
                }


                if (!$scope.IS_CB_ENABLED)
                {
                    $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);
                    if ($scope.NegotiationSettingsValidationMessage !== '') {
                        $scope.Loding = false;
                        return;
                    }

                    $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                }
                
                if ($scope.auctionItem.reqType == 'PRICE_CAP' && ($scope.auctionItem.priceCapValue <= 0 || $scope.auctionItem.priceCapValue == undefined || $scope.auctionItem.priceCapValue == '')) {
                    swal("Error!", 'Please Enter Valid Price cap', "error");
                    return;
                }

                var params = {
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': userService.getUserToken(),
                    'userID': userService.getUserId(),
                    'reqType': $scope.auctionItem.reqType,
                    'priceCapValue': $scope.auctionItem.priceCapValue,
                    'isUnitPriceBidding': $scope.auctionItem.isUnitPriceBidding,
                    //#CB-0-2018-12-05
                    'IS_CB_ENABLED': $scope.IS_CB_ENABLED,
                    'IS_CB_NO_REGRET': $scope.IS_CB_NO_REGRET
                };

                if ($scope.IS_CB_ENABLED == true) {

                    swal({
                        title: "Type of bidding cannot be changed once selected. The only way to change it is by creating a new requirement.",
                        text: "Kindly confirm the action.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function () {
                        auctionsService.savepricecap(params).then(function (req) {
                            if (req.errorMessage == '') {
                                //code before the pause
                                setTimeout(function () {
                                    //do what you need here
                                    swal({
                                        title: "Done!",
                                        text: 'Requirement Type and Bidding Type Saved Successfully',
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                    function () {
                                        if ($scope.auctionItem.status == 'STARTED') {
                                            $scope.recalculate('', 0, true);
                                        }
                                        location.reload();
                                    });
                                }, 1000);
                            } else {
                                swal("Error!", req.errorMessage, "error");
                            }
                        });
                    })
                    
                } else {
                    auctionsService.savepricecap(params).then(function (req) {
                        if (req.errorMessage == '') {
                            swal({
                                title: "Done!",
                                text: 'Requirement Type and Bidding Type Saved Successfully',
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    if ($scope.auctionItem.status == 'STARTED') {
                                        $scope.recalculate('', 0, true);
                                    }
                                    location.reload();
                                });
                        } else {
                            swal("Error!", req.errorMessage, "error");
                        }
                    });
                }

            }

            $scope.goToReqTechSupport = function () {
                //$state.go("reqTechSupport", { "reqId": $scope.reqId }, { reload: true, newtab: true });

                var url = $state.href("reqTechSupport", { "reqId": $scope.reqId });
                window.open(url, '_blank');
            }

            $scope.goToMaterialReceived = function () {
                //$state.go("reqTechSupport", { "reqId": $scope.reqId }, { reload: true, newtab: true });

                var url = $state.href("Material-Received", { "Id": $scope.reqId });
                window.open(url, '_blank');
            }

            $scope.goToVendorPo = function (vendorID) {
                var url = $state.href("po-list", { "reqID": $scope.reqId, "vendorID": vendorID, "poID": 0 });
                window.open(url, '_blank');
            }

            $scope.goToDescPo = function () {
                var url = $state.href("desc-po", { "reqID": $scope.reqId });
                window.open(url, '_blank');
            }

            $scope.goToPreNegotiation = function (Id) {
                var url = $state.href("req-savingsPreNegotiation", { "Id": $scope.reqId });
                window.open(url, '_blank');
            }

            $scope.goToComparatives = function (reqID) {
                if ($scope.auctionItem.isDiscountQuotation != 2) {
                    var url = $state.href("comparatives", { "reqID": $scope.reqId });
                    window.open(url, '_blank');
                }
                else if ($scope.auctionItem.isDiscountQuotation == 2) {
                    var url = $state.href("marginComparatives", { "reqID": $scope.reqId });
                    window.open(url, '_blank');
                }
            }

            $scope.goToQCS = function (reqID, qcsID) {
                var url = $state.href("qcs", { "reqID": $scope.reqId, "qcsID": qcsID });
                window.open(url, '_self');
            };

            $scope.goToQCSList = function (reqID) {
                var url = $state.href("list-qcs", { "reqID": $scope.reqId});
                window.open(url, '_blank');
            };

            $scope.goToCostQCS = function (reqID) {
                var url = $state.href("cost-comparisions-qcs", { "reqID": $scope.reqId });
                window.open(url, '_blank');
            };

            $scope.goToRMQCS = function (reqID, itemID) {
                var url = $state.href("qcsRM", { "reqID": $scope.reqId, "itemID": itemID });
                window.open(url, '_blank');
            };

            $scope.isItemChanged = function (product) {
                var isSame = 0;
                var filterCustItem = _.filter($scope.auctionItem.listRequirementItems, function (custItem) {
                    return custItem.itemID === product.itemID;
                });

                if (filterCustItem.length > 0) {
                    if (filterCustItem[0].productIDorNameCustomer.toUpperCase() !== product.productIDorName.toUpperCase()
                        || filterCustItem[0].productNoCustomer.toUpperCase() !== product.productNo.toUpperCase()) {
                        isSame = 1;
                    }
                }

                if (filterCustItem.length <= 0) {
                    isSame = 2;
                }

                return isSame;
            }

            $scope.paymentRadio = false;

            $scope.paymentlist = [];

            $scope.addpaymentvalue = function () {
                var listpaymet =
                {
                    reqTermsID: 0,
                    reqID: $scope.stateParamsReqID,
                    userID: userService.getUserId(),
                    reqTermsDays: 0,
                    reqTermsPercent: 0,
                    reqTermsType: 'PAYMENT',
                    paymentType: '+'
                };

                if ($scope.auctionItem.status == 'Negotiation Ended') {
                    listpaymet.isRevised = 1
                } else {
                    listpaymet.isRevised = 0
                }

                $scope.paymentlist.push(listpaymet);
            };

            $scope.delpaymentvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }

                $scope.paymentlist.splice(val, 1);
            };

            $scope.resetpayment = function () {
                $scope.paymentlist = [];
            };

            $scope.deliveryRadio = false;

            $scope.deliveryList = [];

            $scope.adddeliveryvalue = function (val) {
                var deliveryObj =
                {
                    reqTermsID: 0,
                    reqID: $scope.stateParamsReqID,
                    userID: userService.getUserId(),
                    reqTermsDays: 0,
                    reqTermsPercent: 0,
                    reqTermsType: 'DELIVERY',
                    refReqTermID: val
                };


                if ($scope.auctionItem.status == 'Negotiation Ended') {
                    deliveryObj.isRevised = 1
                } else {
                    deliveryObj.isRevised = 0
                }

                $scope.deliveryList.push(deliveryObj);
            };

            $scope.deldeliveryvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }


                $scope.deliveryList.splice(val, 1);
            };

            $scope.resetdelivery = function () {
                $scope.deliveryList = [];
            };

            $scope.listRequirementTerms = [];

            $scope.SaveRequirementTerms = function (reqID) {

                $scope.listRequirementTerms = [];

                $scope.deliveryList.forEach(function (item, index) {
                    item.reqID = reqID;
                    $scope.listRequirementTerms.push(item);
                });

                $scope.paymentlist.forEach(function (item, index) {

                    item.reqID = reqID;

                    if (item.paymentType == '-') {
                        item.reqTermsDays = -(item.reqTermsDays);
                    }

                    $scope.listRequirementTerms.push(item);
                });

                var params = {
                    "listRequirementTerms": $scope.listRequirementTerms,
                    sessionID: userService.getUserToken()
                };
                if ($scope.formRequest.isForwardBidding) {

                } else {
                    auctionsService.SaveRequirementTerms(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                }


            };

            $scope.listTerms = [];

            $scope.DeleteRequirementTerms = function () {

                var params = {
                    "listTerms": $scope.listTerms,
                    sessionID: userService.getUserToken()
                };

                auctionsService.DeleteRequirementTerms(params)
                    .then(function (response) {
                        if (response.errorMessage !== '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    });
            };

            $scope.mrpDiscountCalculation = function () {

                if ($scope.auctionItem.isDiscountQuotation == 1 || $scope.auctionItem.isDiscountQuotation == 2) {

                    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                        if ($scope.auctionItem.isDiscountQuotation == 2) {


                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                item.revCostPrice = item.costPrice;
                                item.revmarginAmount = item.marginAmount;
                            }

                            item = $scope.handlePrecision(item, 2);
                            item.cGst = item.Gst / 2;
                            item.sGst = item.Gst / 2;
                            item = $scope.handlePrecision(item, 2);
                            item.netPrice = 1 + (item.Gst / 100);
                            item = $scope.handlePrecision(item, 2);
                            item.netPrice = item.costPrice * item.netPrice;
                            item = $scope.handlePrecision(item, 2);
                            item.marginAmount = item.unitMRP - item.netPrice;

                            item.revnetPrice = 1 + (item.Gst / 100);
                            item = $scope.handlePrecision(item, 2);
                            item.revnetPrice = item.revCostPrice * item.revnetPrice;
                            item = $scope.handlePrecision(item, 2);
                            item.revmarginAmount = item.unitMRP - item.revnetPrice;


                            item = $scope.handlePrecision(item, 2);
                            //item.unitDiscount = item.marginAmount / item.costPrice;
                            item.unitDiscount = item.marginAmount / item.netPrice;
                            item.unitDiscount = item.unitDiscount * 100;
                            item = $scope.handlePrecision(item, 2);
                            item.revitemPrice = item.revCostPrice * (1 + item.Gst / 100);
                            item = $scope.handlePrecision(item, 2);
                            item.revmarginAmount = item.unitMRP - item.revitemPrice;
                            item = $scope.handlePrecision(item, 2);
                            //item.revUnitDiscount = item.revmarginAmount / item.revCostPrice;
                            item.revUnitDiscount = item.revmarginAmount / item.revnetPrice;

                            item.revUnitDiscount = item.revUnitDiscount * 100;
                            item = $scope.handlePrecision(item, 2);
                        }

                        if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                            item.revUnitDiscount = item.unitDiscount;

                        }


                        if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                            item.tempUitMRP = 0;
                            item.tempUnitDiscount = 0;

                            item.tempUitMRP = item.unitMRP;
                            item.tempUnitDiscount = item.unitDiscount;

                            if (item.unitMRP == undefined || item.unitMRP <= 0) {
                                item.unitMRP = 0;
                            }
                            if (item.unitDiscount == undefined || item.unitDiscount <= 0) {
                                item.unitDiscount = 0;
                            }

                            item.unitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.unitDiscount));




                            item.unitMRP = item.tempUitMRP;
                            item.unitDiscount = item.tempUnitDiscount;

                        }

                        item.reductionPrice = (item.unitMRP - item.unitPrice) * item.productQuantity;
                        item.reductionPrice = $scope.precisionRound(parseFloat(item.reductionPrice), 2);

                        if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'Negotiation Ended' || $scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {

                            item.tempUitMRP = 0;
                            item.tempRevUnitDiscount = 0;

                            item.tempUitMRP = item.unitMRP;
                            item.tempRevUnitDiscount = item.revUnitDiscount;

                            if (item.unitMRP == undefined || item.unitMRP <= 0) {
                                item.unitMRP = 0;
                            }
                            if (item.revUnitDiscount == undefined || item.revUnitDiscount <= 0) {
                                item.revUnitDiscount = 0;
                            }

                            item.revUnitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.revUnitDiscount));

                            item.revReductionPrice = (item.unitMRP - item.revUnitPrice) * item.productQuantity;

                            item.revReductionPrice = $scope.precisionRound(parseFloat(item.revReductionPrice), 2);


                            item.unitMRP = item.tempUitMRP;
                            item.revUnitDiscount = item.tempRevUnitDiscount;

                        }

                    });
                }
            }

            $scope.precisionRound = function (number, precision) {
                var factor = Math.pow(10, precision);
                return Math.round(number * factor) / factor;
            }

            $scope.downloadTemplate = function () {

                var name = '';
                if ($scope.auctionItem.isDiscountQuotation == 2) {
                    if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                        name = 'MARGIN_QUOTATION_' + $scope.reqId;
                    }
                    else {
                        name = 'MARGIN_REV_QUOTATION_' + $scope.reqId;
                    }
                } else {
                    name = 'UNIT_PRICE_QUOTATION_' + $scope.reqId;
                }


                reportingService.downloadTemplate(name, userService.getUserId(), $scope.reqId);
            };

            $scope.getReport = function (name) {
                reportingService.downloadTemplate(name + "_" + $scope.reqId, userService.getUserId(), $scope.reqId);
            }

            $scope.EnableMarginFields = function (val) {
                var params = {
                    isEnabled: val,
                    reqID: $scope.reqId,
                    sessionID: userService.getUserToken()
                };

                auctionsService.EnableMarginFields(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Saved Successfully.", "success");
                        }
                        else {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    });
            };

            $scope.numToDecimal = function (val, decimalRound) {

                val = parseFloat(val);
                return val.toFixed(decimalRound);
            };

            $scope.GetReqDataPriceCap = function () {

                var params = {
                    'reqid': $scope.reqId,
                    'sessionid': userService.getUserToken()
                };

                priceCapServices.GetReqData(params)
                    .then(function (response) {
                        $scope.priceCapData = response;

                        if ($scope.priceCapData && $scope.priceCapData.listRequirementItems && $scope.priceCapData.listRequirementItems.length > 0) {
                            $scope.priceCapData.listRequirementItems.forEach(function (item, index) {
                                item.Gst = item.cGst + item.sGst + item.iGst;
                            })
                        }

                        $scope.priceCapCalculations();
                    });
            };

            $scope.GetReqDataPriceCap();

            $scope.SavePriceCapItemLevel = function () {
                var params = {
                    reqID: $scope.reqId,
                    listReqItems: $scope.priceCapData.listRequirementItems,
                    price: $scope.formRequest.priceCapValue,
                    isDiscountQuotation: $scope.auctionItem.isDiscountQuotation,
                    sessionID: userService.getUserToken()
                }
                priceCapServices.SavePriceCap(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Saved Successfully.", "success");
                            if ($scope.auctionItem.status == 'STARTED') {
                                $scope.recalculate('', 0, true);
                            }
                            location.reload();
                        }
                        else {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    })
            }

            $scope.GetDifferentialFactorPrice = function (vendorID) {



                $scope.auctionItem.auctionVendors.forEach(function (vendor, index) {

                    if (vendorID == vendor.vendorID) {
                        vendor.changedDF = {
                            'background-color': 'yellow'
                        }
                    }
                    var QP = vendor.QP;
                    var BP = vendor.BP;
                    var RP = vendor.RP;
                    var DIFFERENTIAL_FACTOR = vendor.DIFFERENTIAL_FACTOR;
                    vendor.DF_REV_VENDOR_TOTAL_PRICE = parseFloat(vendor.revVendorTotalPrice) + parseFloat(DIFFERENTIAL_FACTOR);

                    if ($scope.auctionItem.status == 'Negotiation Ended' || $scope.auctionItem.status == 'STARTED') {
                        vendor.DF_REVISED_VENDOR_TOTAL_PRICE = parseFloat(vendor.revVendorTotalPrice) + parseFloat(DIFFERENTIAL_FACTOR);


                        vendor.QP = parseFloat(vendor.initialPrice) + parseFloat(vendor.DIFFERENTIAL_FACTOR);
                        vendor.BP = parseFloat(vendor.totalPriceIncl) + parseFloat(vendor.DIFFERENTIAL_FACTOR);
                        vendor.RP = parseFloat(vendor.revVendorTotalPrice) + parseFloat(vendor.DIFFERENTIAL_FACTOR);
                    }
                })

            };


            $scope.SaveDifferentialFactor = function (auctionVendor) {


                var params = {
                    'vendorID': auctionVendor.vendorID,
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': userService.getUserToken(),
                    'customerID': userService.getUserId(),
                    'value': auctionVendor.DIFFERENTIAL_FACTOR,
                    'reason': ''
                };

                auctionsService.SaveDifferentialFactor(params).then(function (req) {
                    if (req.errorMessage == '') {
                        growlService.growl('Saved Successfully.', "success");

                        var parties = params.reqID + "$" + userService.getUserId() + "$" + userService.getUserToken() + "$" + "SAVE_DIFFERENTIAL_FACTOR";

                        $scope.invokeSignalR('CheckRequirement', parties);
                        //swal({
                        //    title: "Done!",
                        //    text: 'Saved Successfully',
                        //    type: "success",
                        //    showCancelButton: false,
                        //    confirmButtonColor: "#DD6B55",
                        //    confirmButtonText: "Ok",
                        //    closeOnConfirm: true
                        //},
                        //    function () {
                        //        location.reload();
                        //    });

                    } else {
                        swal("Error!", req.errorMessage, "error");
                    }
                });
            }

            //$scope.priceCapCalculations = function () {
            //    $scope.priceCapData.forEach(function (item, index) {

            //    })
            //}

            $scope.priceCapCalculations = function () {

                if ($scope.auctionItem.isDiscountQuotation == 1 || $scope.auctionItem.isDiscountQuotation == 0) {

                    $scope.formRequest.priceCapValue = 0;
                    if ($scope.priceCapData && $scope.priceCapData.listRequirementItems) {
                        $scope.priceCapData.listRequirementItems.forEach(function (item, index) {

                            //if ($scope.priceCapData.auctionVendors[0].isQuotationRejected != 0) {
                            //    item.revUnitDiscount = item.unitDiscount;
                            //}

                            item.cGst = item.Gst / 2;
                            item.sGst = item.Gst / 2;

                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                if ($scope.auctionItem.isDiscountQuotation == 1) {
                                    item.revUnitDiscount = item.unitDiscount;
                                    item.tempUitMRP = 0;
                                    item.tempUnitDiscount = 0;

                                    item.tempUitMRP = item.unitMRP;
                                    item.tempUnitDiscount = item.unitDiscount;

                                    if (item.unitMRP == undefined || item.unitMRP <= 0) {
                                        item.unitMRP = 0;
                                    }
                                    if (item.unitDiscount == undefined || item.unitDiscount <= 0) {
                                        item.unitDiscount = 0;
                                    }

                                    item.unitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.unitDiscount));

                                    item.unitMRP = item.tempUitMRP;
                                    item.unitDiscount = item.tempUnitDiscount;


                                    if ($scope.auctionItem.isDiscountQuotation == 1) {
                                        item.reductionPrice = (item.unitMRP - item.unitPrice) * item.productQuantity;
                                        item.reductionPrice = $scope.precisionRound(parseFloat(item.reductionPrice), 2);
                                    }

                                }


                                ////////////////////
                                item.sGst = item.cGst;
                                var tempUnitPrice = item.unitPrice;
                                var tempCGst = item.cGst;
                                var tempSGst = item.sGst;
                                var tempIGst = item.iGst;
                                if (item.unitPrice == undefined || item.unitPrice <= 0) {
                                    item.unitPrice = 0;
                                };

                                if ($scope.auctionItem.isDiscountQuotation == 0) {
                                    item.revUnitPrice = item.unitPrice;
                                }

                                item.itemPrice = item.unitPrice * item.productQuantity;


                                item.itemPrice = item.itemPrice + ((item.itemPrice / 100) * (item.cGst + item.sGst + item.iGst));

                                item.unitPrice = tempUnitPrice;
                                item.cGst = tempCGst
                                item.sGst = tempSGst;
                                item.iGst = tempIGst;

                                if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                                    // $scope.gstValidation = true;
                                    // not mandatory
                                };
                                /////////////////////

                            }

                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'Negotiation Ended' || $scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {

                                if ($scope.auctionItem.isDiscountQuotation == 1) {
                                    item.tempUitMRP = 0;
                                    item.tempRevUnitDiscount = 0;

                                    item.tempUitMRP = item.unitMRP;
                                    item.tempRevUnitDiscount = item.revUnitDiscount;

                                    if (item.unitMRP == undefined || item.unitMRP <= 0) {
                                        item.unitMRP = 0;
                                    }
                                    if (item.revUnitDiscount == undefined || item.revUnitDiscount <= 0) {
                                        item.revUnitDiscount = 0;
                                    }

                                    item.revUnitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.revUnitDiscount));

                                    item.revReductionPrice = (item.unitMRP - item.revUnitPrice) * item.productQuantity;

                                    item.revReductionPrice = $scope.precisionRound(parseFloat(item.revReductionPrice), 2);


                                    item.unitMRP = item.tempUitMRP;
                                    item.revUnitDiscount = item.tempRevUnitDiscount;
                                }

                                ////////////////////////
                                var tempRevUnitPrice = item.revUnitPrice;
                                var tempCGst = item.cGst;
                                var tempSGst = item.sGst;
                                var tempIGst = item.iGst;

                                if (item.revUnitPrice == undefined || item.revUnitPrice <= 0) {
                                    item.revUnitPrice = 0;
                                };

                                item.revitemPrice = item.revUnitPrice * item.productQuantity;

                                if (item.cGst == undefined || item.cGst <= 0) {
                                    item.cGst = 0;
                                };
                                if (item.sGst == undefined || item.sGst <= 0) {
                                    item.sGst = 0;
                                };
                                if (item.iGst == undefined || item.iGst <= 0) {
                                    item.iGst = 0;
                                };

                                item.revitemPrice = item.revitemPrice + ((item.revitemPrice / 100) * (item.cGst + item.sGst + item.iGst));

                                item.revUnitPrice = tempRevUnitPrice;
                                item.cGst = tempCGst
                                item.sGst = tempSGst;
                                item.iGst = tempIGst;

                                if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                                    // $scope.gstValidation = true;
                                    // not mandatory
                                }
                                //////////////////////

                            }

                            if (item.revitemPrice > 0) {
                                $scope.formRequest.priceCapValue = parseFloat($scope.formRequest.priceCapValue);
                                $scope.formRequest.priceCapValue += item.revitemPrice;
                            }

                            //item.unitPrice = $scope.numToDecimal(item.unitPrice, $rootScope.companyRoundingDecimalSetting);
                            item.itemPrice = $scope.numToDecimal(item.itemPrice, $rootScope.companyRoundingDecimalSetting);

                            //item.revUnitPrice = $scope.numToDecimal(item.revUnitPrice, $rootScope.companyRoundingDecimalSetting);
                            item.revitemPrice = $scope.numToDecimal(item.revitemPrice, $rootScope.companyRoundingDecimalSetting);


                            $scope.formRequest.priceCapValue = $scope.numToDecimal($scope.formRequest.priceCapValue, $rootScope.companyRoundingDecimalSetting);
                        });
                    }
                }
            };

            $scope.removeAttach = function (index) {
                $scope.multipleAttachmentsList.splice(index, 1);
                $scope.auctionItem.auctionVendors[0].multipleAttachmentsList.splice(index, 1);
            };

            $scope.regretHandle = function (item) {
                if (item.isRegret) {
                    item.unitPrice = 0;
                    item.Gst = 0;
                    item.sGst = 0;
                    item.cGst = 0;
                    item.iGst = 0;
                    //item.regretComments = '';
                    item.itemFreightCharges = 0;
                    item.itemFreightTAX = 0;
                } else {
                    item.regretComments = '';
                }


                if ($scope.totalprice == 0) {

                    $("#totalprice").val(0);
                    $("#revtotalprice").val(0);

                    $("#packingCharges").val(0);
                    $("#packingChargesTaxPercentage").val(0);
                    $("#packingChargesWithTax").val(0);
                    $("#revpackingCharges").val(0);
                    $("#revpackingChargesWithTax").val(0);

                    $("#installationCharges").val(0);
                    $("#installationChargesTaxPercentage").val(0);
                    $("#installationChargesWithTax").val(0);
                    $("#revinstallationCharges").val(0);
                    $("#revinstallationChargesWithTax").val(0);

                    $("#freightCharges").val(0);
                    $("#freightChargesTaxPercentage").val(0);
                    $("#freightChargesWithTax").val(0);
                    $("#revfreightCharges").val(0);
                    $("#revfreightChargesWithTax").val(0);

                    $("#vendorBidPrice").val(0);
                    $("#revvendorBidPrice").val(0);

                } 
            }

            $scope.showVendorRegretDetails = function (vendor) {
                $scope.regretDetails = vendor.listRequirementItems;
            };

            //#CB-0-2018-12-05
            $scope.goToCbCustomer = function (vendorID) {
                var url = $state.href("cb-customer", { "reqID": $scope.reqId, "vendorID": vendorID });
                window.open(url, '_blank');
            };

            //$scope.UpdateCBTime = function () {

            //    var ts = moment($scope.formRequest.CB_END_TIME, "DD-MM-YYYY HH:mm").valueOf();
            //    var m = moment(ts);
            //    var date = new Date(m);
            //    var milliseconds = parseInt(date.getTime() / 1000.0);
            //    var cbEndTime = "/Date(" + milliseconds + "000+0530)/";

            //    var params = {
            //        cbEndTime: cbEndTime,
            //        userID: userService.getUserId(),
            //        reqID: $scope.reqId,
            //        sessionID: userService.getUserToken()
            //    }

            //    auctionsService.UpdateCBTime(params)
            //        .then(function (response) {
            //            if (response.errorMessage != "") {
            //                growlService.growl(response.errorMessage, "inverse");
            //            } else {
            //                $scope.recalculate();
            //                growlService.growl('Saved Successfully', "inverse");
            //            }
            //        });
            //};

            $scope.UpdateCBTime = function () {




                var CB_END_TIME = $("#CB_END_TIME").val(); //$scope.startTime; //Need fix on this.

                if (CB_END_TIME && CB_END_TIME != null && CB_END_TIME != "") {

                    //var ts = moment($scope.startTime, "DD-MM-YYYY HH:mm").valueOf();
                    var ts = moment(CB_END_TIME, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var auctionStartDate = new Date(m);
                    auctionsService.getdate()
                        .then(function (getdateResponse) {
                            //var CurrentDate = moment(new Date(parseInt(getdateResponse.substr(6))));

                            var CurrentDateToLocal = userService.toLocalDate(getdateResponse);

                            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                            var m = moment(ts);
                            var deliveryDate = new Date(m);
                            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                            var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));

                            if (CurrentDate >= auctionStartDate) {
                                $scope.Loding = false;
                                swal("Done!", "End time should be greater than current time.", "error");
                                return;
                            }

                            swal({
                                title: "Are you sure?",
                                text: "Your negotiation process will be automatically closed at " + $scope.formRequest.CB_END_TIME + ".",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#F44336",
                                confirmButtonText: "OK",
                                closeOnConfirm: true
                            }, function () {

                                var ts = moment($scope.formRequest.CB_END_TIME, "DD-MM-YYYY HH:mm").valueOf();
                                var m = moment(ts);
                                var date = new Date(m);
                                var milliseconds = parseInt(date.getTime() / 1000.0);
                                // // #INTERNATIONALIZATION-0-2019-02-12
                                var cbEndTime = "/Date(" + userService.toUTCTicks(CB_END_TIME) + "+0530)/";
                                //000

                                var params = {
                                    cbEndTime: cbEndTime,
                                    userID: userService.getUserId(),
                                    reqID: $scope.reqId,
                                    sessionID: userService.getUserToken()
                                }

                                auctionsService.UpdateCBTime(params)
                                    .then(function (response) {
                                        if (response.errorMessage != "") {
                                            growlService.growl(response.errorMessage, "inverse");
                                        } else {
                                            $scope.recalculate('UPDATE_CB_TIME_CUSTOMER', 0, false);
                                            growlService.growl('Saved Successfully', "inverse");
                                        }
                                    });
                            })


                        })
                }
                else {
                    swal("Done!", "Please enter the date and time to update End Time.", "error");
                    //alert("Please enter the date and time to update Start Time to.");
                }


            };

            $scope.CBStopQuotations = function (value) {
                
                var params = {
                    stopCBQuotations: value,
                    userID: userService.getUserId(),
                    reqID: $scope.reqId,
                    sessionID: userService.getUserToken()
                }

                swal({
                    title: "Are you sure?",
                    text: "You will not receive any further quotations.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    auctionsService.CBStopQuotations(params)
                        .then(function (response) {
                            if (response.errorMessage != "") {
                                growlService.growl(response.errorMessage, "inverse");
                            } else {
                                $scope.recalculate('CB_STOP_QUOTATIONS_CUSTOMER', 0, false);
                                growlService.growl('Saved Successfully', "inverse");
                            }
                        });
                })
            };

            $scope.CBMarkAsComplete = function (value) {

                var params = {
                    isCBCompleted: value,
                    userID: userService.getUserId(),
                    reqID: $scope.reqId,
                    sessionID: userService.getUserToken()
                }

            /*    swal({
                    title: "Once requirement is marked as completed user will not have privileges to edit and negotiate with vendors.",
                    text: "Kindly confirm.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {*/
                    auctionsService.CBMarkAsComplete(params)
                        .then(function (response) {
                            if (response.errorMessage != "") {
                                growlService.growl(response.errorMessage, "inverse");
                            } else {
                                $scope.recalculate('CB_MARK_AS_COMPLETED_CUSTOMER', 0, false);
                                $scope.getrevisedquotations();
                                growlService.growl('Saved Successfully', "inverse");
                            }
                        });
              //  })
            };

            $scope.changeBiddingType = function (value) {
                if (value == '1' || value == '2') {
                    $scope.IS_CB_ENABLED = true;
                    if (value == '2') {
                        $scope.IS_CB_NO_REGRET = true;
                    } else {
                        $scope.IS_CB_NO_REGRET = false;
                    }
                }
                if (value == '0') {
                    $scope.IS_CB_ENABLED = false;
                }
            }

            $scope.resetValues = function () {
                    $("#packingCharges").val($scope.packingCharges);
                    $("#packingChargesTaxPercentage").val($scope.packingChargesTaxPercentage);
                    $("#packingChargesWithTax").val($scope.packingChargesWithTax);
                    $("#revpackingCharges").val($scope.revpackingCharges);
                    $("#revpackingChargesWithTax").val($scope.revpackingChargesWithTax);

                    $("#installationCharges").val($scope.installationCharges);
                    $("#installationChargesTaxPercentage").val($scope.installationChargesTaxPercentage);
                    $("#installationChargesWithTax").val($scope.installationChargesWithTax);
                    $("#revinstallationCharges").val($scope.revinstallationCharges);
                    $("#revinstallationChargesWithTax").val($scope.revinstallationChargesWithTax);

                    $("#freightCharges").val($scope.freightCharges);
                    $("#freightChargesTaxPercentage").val($scope.freightChargesTaxPercentage);
                    $("#freightChargesWithTax").val($scope.freightChargesWithTax);
                    $("#revfreightCharges").val($scope.revfreightCharges);
                    $("#revfreightChargesWithTax").val($scope.revfreightChargesWithTax);
            }
            
            auctionsService.GetCompanyConfiguration($scope.compID, "INCO", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyINCOTerms = unitResponse;
                });

            $scope.shouldShow = function (permissionLevel) {
                // put your authorization logic here
                if (permissionLevel) {
                    return true;
                } else {
                    return false;
                }
            }


            $scope.markAsCompleteREQ = function (reqId,value,isMACForCBOrNot) {
                var params = {
                    isREQCompleted: value,
                    userID: userService.getUserId(),
                    reqID: reqId,
                    sessionID: userService.getUserToken()
                }

                swal({
                    title: "Once requirement is marked as completed user will not have privileges to edit this requirement.",
                    text: "Kindly confirm.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    auctionsService.markAsCompleteREQ(params)
                        .then(function (response) {

                            //$scope.CBMarkAsComplete(1);

                            if (response.errorMessage != "") {
                                growlService.growl(response.errorMessage, "inverse");
                            } else {
                             //   $scope.recalculate('CB_MARK_AS_COMPLETED_CUSTOMER', 0, false);
                             //   $scope.getrevisedquotations();
                                growlService.growl('Saved Successfully', "success");
                                location.reload();
                                //auctionItem.IS_CB_ENABLED
                                if (isMACForCBOrNot == true) {
                                    $scope.CBMarkAsComplete(1);
                                } else {

                                }
                                
                              
                            }


                        });
                })
            }








            $scope.GetItemUnitPrice = function (isPerUnit, templateIndex) {
                //if (itemTemplate && itemTemplate.HAS_PRICE <= 0) {
                //    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                //        if (item.productQuotationTemplateArray != null && item.productQuotationTemplateArray != undefined && item.productQuotationTemplateArray.length > 0) {
                //            item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplateArray);
                //        } else {
                //            item.productQuotationTemplateJson = '';
                //        }
                //    });
                //    return;
                //}
                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (isPerUnit) {
                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                            item.unitPrice = 0;
                            item.revUnitPrice = 0;
                            item.UNIT_PRICE = 0;
                            item.REV_UNIT_PRICE = 0;
                            item.margin15 = 0;
                            item.revmargin15 = 0;
                            item.margin3 = 0;
                            item.revmargin3 = 0;

                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                if (templateIndex == subItemIndex || templateIndex == -1) {
                                    if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                                        subItem.REV_BULK_PRICE = subItem.BULK_PRICE;
                                        subItem.REV_CONSUMPTION = subItem.CONSUMPTION;
                                        subItem.REV_TAX = subItem.TAX;
                                        subItem.REV_UNIT_PRICE = subItem.UNIT_PRICE;
                                    }

                                    var BULK_PRICE = 0;
                                    if (parseFloat(subItem.BULK_PRICE) > 0) {
                                        BULK_PRICE = parseFloat(subItem.BULK_PRICE);
                                    }
                                    var REV_BULK_PRICE = 0;
                                    if (parseFloat(subItem.REV_BULK_PRICE) > 0) {
                                        REV_BULK_PRICE = parseFloat(subItem.REV_BULK_PRICE);
                                    }

                                    var CONSUMPTION = 0;
                                    if (parseFloat(subItem.CONSUMPTION) > 0) {
                                        CONSUMPTION = parseFloat(subItem.CONSUMPTION);
                                    }
                                    var REV_CONSUMPTION = 0;
                                    if (parseFloat(subItem.REV_CONSUMPTION) > 0) {
                                        REV_CONSUMPTION = parseFloat(subItem.REV_CONSUMPTION);
                                    }

                                    var TAX = 0;
                                    if (parseFloat(subItem.TAX) > 0) {
                                        TAX = parseFloat(subItem.TAX);
                                    }
                                    var REV_TAX = 0;
                                    if (parseFloat(subItem.REV_TAX) > 0) {
                                        REV_TAX = parseFloat(subItem.REV_TAX);
                                    }

                                    var UNIT_PRICE = 0;
                                    if (parseFloat(subItem.UNIT_PRICE) > 0) {
                                        UNIT_PRICE = parseFloat(subItem.UNIT_PRICE);
                                    }
                                    var REV_UNIT_PRICE = 0;
                                    if (parseFloat(subItem.REV_UNIT_PRICE) > 0) {
                                        REV_UNIT_PRICE = parseFloat(subItem.REV_UNIT_PRICE);
                                    }

                                    //UNIT_PRICE = BULK_PRICE * CONSUMPTION;
                                    //REV_UNIT_PRICE = REV_BULK_PRICE * REV_CONSUMPTION;
                                    if (CONSUMPTION > 0) {
                                        subItem.BULK_PRICE = UNIT_PRICE / CONSUMPTION;
                                    }
                                    else {
                                        subItem.BULK_PRICE = 0;
                                    }
                                    if (REV_CONSUMPTION > 0) {
                                        subItem.REV_BULK_PRICE = REV_UNIT_PRICE / REV_CONSUMPTION;
                                    }
                                    else {
                                        subItem.REV_BULK_PRICE = 0;
                                    }
                                    //subItem.UNIT_PRICE = UNIT_PRICE;
                                    //subItem.REV_UNIT_PRICE = REV_UNIT_PRICE;

                                    //if (subItem.IS_CALCULATED == 0) {
                                    //    //item.unitPrice += UNIT_PRICE;
                                    //    //item.revUnitPrice += REV_UNIT_PRICE;
                                    //    item.UNIT_PRICE += UNIT_PRICE;
                                    //    item.REV_UNIT_PRICE += REV_UNIT_PRICE;

                                    //}
                                }
                            });
                        }
                    }
                    else {
                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0 && $scope.isUnitPriceEditableForSubItems(item.productQuotationTemplateArray)) {
                            item.unitPrice = 0;
                            item.revUnitPrice = 0;
                            item.UNIT_PRICE = 0;
                            item.REV_UNIT_PRICE = 0;
                            item.margin15 = 0;
                            item.revmargin15 = 0;
                            item.margin3 = 0;
                            item.revmargin3 = 0;

                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                if (templateIndex == subItemIndex || templateIndex == -1) {
                                    if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                                        subItem.REV_BULK_PRICE = subItem.BULK_PRICE;
                                        subItem.REV_CONSUMPTION = subItem.CONSUMPTION;
                                        subItem.REV_TAX = subItem.TAX;
                                        subItem.REV_UNIT_PRICE = subItem.UNIT_PRICE;
                                    }

                                    var BULK_PRICE = 0;
                                    if (parseFloat(subItem.BULK_PRICE) > 0) {
                                        BULK_PRICE = parseFloat(subItem.BULK_PRICE);
                                    }
                                    var REV_BULK_PRICE = 0;
                                    if (parseFloat(subItem.REV_BULK_PRICE) > 0) {
                                        REV_BULK_PRICE = parseFloat(subItem.REV_BULK_PRICE);
                                    }

                                    var CONSUMPTION = 0;
                                    if (parseFloat(subItem.CONSUMPTION) > 0) {
                                        CONSUMPTION = parseFloat(subItem.CONSUMPTION);
                                    }
                                    var REV_CONSUMPTION = 0;
                                    if (parseFloat(subItem.REV_CONSUMPTION) > 0) {
                                        REV_CONSUMPTION = parseFloat(subItem.REV_CONSUMPTION);
                                    }

                                    var TAX = 0;
                                    if (parseFloat(subItem.TAX) > 0) {
                                        TAX = parseFloat(subItem.TAX);
                                    }
                                    var REV_TAX = 0;
                                    if (parseFloat(subItem.REV_TAX) > 0) {
                                        REV_TAX = parseFloat(subItem.REV_TAX);
                                    }

                                    var UNIT_PRICE = 0;
                                    if (parseFloat(subItem.UNIT_PRICE) > 0) {
                                        UNIT_PRICE = parseFloat(subItem.UNIT_PRICE);
                                    }
                                    var REV_UNIT_PRICE = 0;
                                    if (parseFloat(subItem.REV_UNIT_PRICE) > 0) {
                                        REV_UNIT_PRICE = parseFloat(subItem.REV_UNIT_PRICE);
                                    }

                                    UNIT_PRICE = BULK_PRICE * CONSUMPTION + ((BULK_PRICE * CONSUMPTION) * (TAX / 100));
                                    REV_UNIT_PRICE = REV_BULK_PRICE * REV_CONSUMPTION + ((REV_BULK_PRICE * REV_CONSUMPTION) * (TAX / 100));

                                    subItem.UNIT_PRICE = UNIT_PRICE;
                                    subItem.REV_UNIT_PRICE = REV_UNIT_PRICE;

                                    //if (subItem.IS_CALCULATED == 0) {
                                    //    //item.unitPrice += UNIT_PRICE;
                                    //    //item.revUnitPrice += REV_UNIT_PRICE;
                                    //    item.UNIT_PRICE += UNIT_PRICE;
                                    //    item.REV_UNIT_PRICE += REV_UNIT_PRICE;
                                    //}
                                }
                            });
                        }
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.IS_CALCULATED == 0) {
                                if (subItem.UNIT_PRICE > 0) {
                                    item.UNIT_PRICE += parseFloat(subItem.UNIT_PRICE);
                                }
                                if (subItem.REV_UNIT_PRICE > 0) {
                                    item.REV_UNIT_PRICE += parseFloat(subItem.REV_UNIT_PRICE);
                                }
                            }
                        });
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        //item.margin15 = (item.UNIT_PRICE / 100) * 15;
                        //item.revmargin15 = (item.REV_UNIT_PRICE / 100) * 15;

                        //item.margin3 = (item.UNIT_PRICE / 100) * 3;
                        //item.revmargin3 = (item.REV_UNIT_PRICE / 100) * 3;
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.IS_CALCULATED == 1) {
                                subItem.UNIT_PRICE = item.UNIT_PRICE;
                                subItem.REV_UNIT_PRICE = item.REV_UNIT_PRICE;
                            }
                            if (subItem.IS_CALCULATED == 2) {
                                subItem.UNIT_PRICE = item.margin15;
                                subItem.REV_UNIT_PRICE = item.revmargin15;
                            }
                            if (subItem.IS_CALCULATED == 3) {
                                subItem.UNIT_PRICE = item.margin3;
                                subItem.REV_UNIT_PRICE = item.revmargin3;
                            }
                            if (subItem.IS_CALCULATED == 4) {
                                item.UNIT_PRICE = parseFloat(item.UNIT_PRICE) + parseFloat(subItem.UNIT_PRICE);
                                item.REV_UNIT_PRICE = parseFloat(item.REV_UNIT_PRICE) + parseFloat(subItem.REV_UNIT_PRICE);
                            }
                        });
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0 && $scope.isUnitPriceEditableForSubItems(item.productQuotationTemplateArray)) {
                        item.unitPrice = parseFloat(item.UNIT_PRICE) + parseFloat(item.margin15) + parseFloat(item.margin3);
                        item.revUnitPrice = parseFloat(item.REV_UNIT_PRICE) + parseFloat(item.revmargin15) + parseFloat(item.revmargin3);
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.IS_CALCULATED == 5) {
                                subItem.UNIT_PRICE = item.unitPrice;
                                subItem.REV_UNIT_PRICE = item.revUnitPrice;
                            }
                        })
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray != null && item.productQuotationTemplateArray != undefined && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplateArray);
                    } else {
                        item.productQuotationTemplateJson = '';
                    }
                });

            };


            $scope.VendorItemlevelPrices = [];
            $scope.showVendorItemlevelPrices = function (vendor) {
                $scope.VendorItemlevelPrices = [];
                $scope.VendorItemlevelPrices = vendor.listRequirementItems;
            };



            $scope.createLot = function (requirementId) {

                swal({
                    title: "Once Lot Requirement is Created Vendors cannot submit Quotations for this Requirement.",
                    text: "Kindly confirm.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    PRMLotReqService.lotdetails(0, requirementId)
                        .then(function (response) {
                            if (response) {
                                if (response) {
                                    $state.go("save-lot-details", { "Id": response.LotId, "reqId": requirementId });
                                }
                            }
                        });
                });

                
            };

            $scope.isValidLotBidding = function () {
                if ($scope.auctionItem.auctionVendors.length == 0 || $scope.auctionItem.auctionVendors[0].quotationUrl == "" || $scope.auctionItem.auctionVendors[1].quotationUrl == "" || $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 || $scope.auctionItem.auctionVendors[1].isQuotationRejected != 0 || $scope.starttimecondition1 != 0 || $scope.starttimecondition2 != 0) {
                    return false;
                }
                else {
                    return true;
                }
            };

            $scope.handleSubItemsVisibility = function (product) {
                product.subIemsColumnsVisibility = {
                    hideSPECIFICATION: true,
                    hideQUANTITY: true,
                    hideTAX: true,
                    hidePRICE: true
                };
                var productQuotationTemplateArray = product.productQuotationTemplateArray;
                if (productQuotationTemplateArray && productQuotationTemplateArray.length > 0) {
                    var items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_SPECIFICATION === 1);
                    });

                    if (items && items.length > 0) {
                        product.subIemsColumnsVisibility.hideSPECIFICATION = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_QUANTITY === 1);
                    });

                    if (items && items.length > 0) {
                        product.subIemsColumnsVisibility.hideQUANTITY = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_TAX === 1);
                    });

                    if (items && items.length > 0) {
                        product.subIemsColumnsVisibility.hideTAX = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_PRICE === 1);
                    });

                    if (items && items.length > 0) {
                        product.subIemsColumnsVisibility.hidePRICE = false;
                    }

                    //product.subIemsColumnsVisibility = _.clone($scope.subIemsColumnsVisibility);
                }
            };

            $scope.isUnitPriceEditableForSubItems = function(productQuotationTemplateArray) {
                var isDisabled = false;
                if(productQuotationTemplateArray && productQuotationTemplateArray.length > 0) {
                    var hasOnlySpecificationItems = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_PRICE === 1);
                    });

                    if(hasOnlySpecificationItems && hasOnlySpecificationItems.length > 0) {
                        isDisabled = true;
                    }
                }

                return isDisabled;
            };

            $scope.isTaxFieldEditableForSubItems = function(productQuotationTemplateArray) {
                var isDisabled = false;
                if(productQuotationTemplateArray && productQuotationTemplateArray.length > 0) {
                    var hasOnlySpecificationItems = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_TAX === 1);
                    });

                    if(hasOnlySpecificationItems && hasOnlySpecificationItems.length > 0) {
                        isDisabled = true;
                    }
                }

                return isDisabled;
            };
            
            $scope.getCustomFieldsByModuleId = function () {
                 params = {
                    "compid": userService.getUserCompanyId(),
                    "fieldmodule": 'REQUIREMENT',
                    "moduleid": $stateParams.Id,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetCustomFieldsByModuleId(params)
                .then(function (response) {
                    $scope.selectedcustomFieldList = response;                   
                });
            };

            $scope.getCustomFieldsByModuleId();

            $scope.IsAllChecked = false;
            $scope.isSaveEmailDisabled = true;
           // $scope.disableToOtherUsers = [];



            $scope.SendEmailLinksToVendors = function (emailToVendors) {

                $scope.vendors = [];

                emailToVendors.forEach(function (item, index) {
                    if (item.isEmailSent == true) {
                        if (item.WF_ID > 0) {
                            item.VendorObj =
                                {
                                    vendorID: item.vendorID,
                                    WF_ID: item.WF_ID
                                }
                            item = item.VendorObj;
                            $scope.vendors.push(item);
                        } else {
                            $scope.auctionItemTemporary.emailLinkVendors.forEach(function (mand, mandIndex) {
                                if (mand.vendorID == item.vendorID) {
                                    mand.showingWorkflowErrorMessage = 'Please Select Workflow';
                                }
                            })
                            return;
                        }
                    }
                    
                });
                
                if ($scope.vendors.length <= 0) {
                    growlService.growl("Mandatory fields are required.", "inverse");
                    return;
                }

                
                var params =
                {
                    userID: userService.getUserId(),
                    vendorIDs: $scope.vendors,
                    sessionID: userService.getUserToken()
                }

                auctionsService.shareemaillinkstoVendors(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            angular.element('#showVendors').modal('hide');
                            growlService.growl("Email Has Been Send To The Selected Vendors ", "success");
                            $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                                item.isEmailSent = false;
                                item.WF_ID = 0;
                                item.showingWorkflowErrorMessage = '';
                            });
                            $scope.IsAllChecked = false;

                         //   $scope.disableToOtherUsers = angular.copy($scope.vendors);
                        }
                    });

            };



            $scope.showErrorMessagesForVendors = function () {
                $scope.disableVendors = [];
                var vendorsString = '';
                vendorsString = $scope.existingVendors.join(',');

                var params =
                {
                    userID: userService.getUserId(),
                    vendorIDs: vendorsString,
                    sessionID: userService.getUserToken()
                }

                auctionsService.GetSharedLinkVendors(params)
                    .then(function (response) {
                        //$scope.auctionItemTemporary.emailLinkVendors
                        $scope.disableVendors = response;

                        $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                            item.isDisable = false;
                            item.message = '';
                            item.errorMessage = '';
                            $scope.disableVendors.forEach(function (item1, index1) {
                                if (item.vendorID == item1.vendorID)
                                {
                                    item.isDisable = true;
                                    item.message = 'Vendor has already received the registration link by'; 
                                    item.errorMessage = item1.errorMessage;
                                }
                            })
                        })
                    });

            }



            $scope.selectAll = function (value) {
                if (value) {
                    $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                        item.isEmailSent = true;
                        $scope.disableVendors.forEach(function (item1, index1) {
                            if (item.vendorID == item1.vendorID) {
                                item.isEmailSent = false;
                            }
                            return;
                        })
                    });
                } else {
                    $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                        item.isEmailSent = false;
                    });
                    //$scope.isSaveEmailDisabled = true;
                }
                
            };


            $scope.selectedVendor = function () {
                //console.log($scope.vendors.length);
            }


            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListTemp = response;
                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule == $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                                
                            }
                        });

                        if ($scope.auctionItemTemporary) {
                            $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                                item.listWorkflows = $scope.workflowList;
                            });
                        }
                        
                        if (userService.getUserObj().isSuperUser) {
                            $scope.workflowList = $scope.workflowList;
                        }
                        else {
                            $scope.workflowList = $scope.workflowList.filter(function (item) {
                                return item.deptID == userService.getSelectedUserDepartmentDesignation().deptID;

                            });
                        }



                    });
            };
            

            /*region end WORKFLOW*/

            $scope.fieldValidation = function () {
                if ($scope.auctionItem.auctionVendors.length > 0) {
                    $scope.auctionItem.auctionVendors.forEach(function (vendor, index) {
                        if (vendor.INCO_TERMS) {
                            auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                                .then(function (response) {
                                    $scope.productConfig = response;
                                    $scope.productConfig.forEach(function (incoItem, itemIndexs) {
                                        //$scope.auctionItem.listRequirementItems.forEach(function (item, index) {

                                        //    if (item.isCoreProductCategory == 0 && item.catalogueItemID == incoItem.ProductId) {
                                        //        if ($scope.userIsOwner && incoItem.IS_CUSTOMER_EDIT) {
                                        //            item.isEdit = false;
                                        //        } else if (!$scope.userIsOwner && incoItem.IS_VENDOR_EDIT && $scope.totalprice > 0 ) {
                                        //            item.isEdit = false;
                                        //        }
                                        //    } else if (item.isCoreProductCategory == 1) {
                                        //        item.isEdit = false;
                                        //    }

                                        //});

                                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                                            if (item.isCoreProductCategory == 0 && item.catalogueItemID == incoItem.ProductId) {
                                                if ($scope.userIsOwner && incoItem.IS_CUSTOMER_EDIT) {
                                                    item.isEdit = false;
                                                } else if (!$scope.userIsOwner && incoItem.IS_VENDOR_EDIT && $scope.totalprice > 0) {
                                                    item.isEdit = false;
                                                }
                                            } else if (item.isCoreProductCategory == 1) {
                                                item.isEdit = false;
                                            }

                                        });
                                    });

                                });
                        }
                    });
                }
            };


            $scope.getDFTotalPrice = function (qp, oc) {
                qp = parseFloat(qp);
                oc = parseFloat(oc);
                var result = parseFloat(qp + oc).toFixed(2);
                return result;
            };

            $scope.closeTender = function () {
                var showWarning = false;
                $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                    if (!item.bestPrice) {
                        item.bestPrice = 0;
                    }

                    item.quotationRejectedComment = item.quotationRejectedComment.replace("Approved-", "");
                    item.quotationRejectedComment = item.quotationRejectedComment.replace("Reject Comments-", "");
                    if (item.quotationRejectedComment) {
                        item.quotationRejectedComment = item.quotationRejectedComment.trim();
                    }

                    item.revQuotationRejectedComment = item.revQuotationRejectedComment.replace("Approved-", "");
                    item.revQuotationRejectedComment = item.revQuotationRejectedComment.replace("Reject Comments-", "");
                    if (item.revQuotationRejectedComment) {
                        item.revQuotationRejectedComment = item.revQuotationRejectedComment.trim();
                    }


                    if ($scope.auctionItem.LAST_BID_ID == item.vendorID &&
                        ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED' || $scope.auctionItem.status == 'UNCONFIRMED')) {
                        item.vendorColorStyle = { 'background-color': 'lightgreen' };
                    }
                    else if (item.FREEZE_CB == true) {
                        item.vendorColorStyle = { 'background-color': 'rgb(255, 228, 225)' };
                    }
                    else if (item.VEND_FREEZE_CB == true) {
                        item.vendorColorStyle = { 'background-color': 'rgb(176, 224, 230)' };
                        //rgb(176, 224, 230)
                        //#B0E0E6
                    }
                    else {
                        item.vendorColorStyle = {};
                    }

                    item.isQuotationRejectedDB = item.isQuotationRejected;
                    item.isRevQuotationRejectedDB = item.isRevQuotationRejected;

                    item.bestPrice = 0;
                    item.hasRegretItems = false;
                    item.listRequirementItems.forEach(function (item2, index) {
                        //Core products only
                        if (item2.isCoreProductCategory && $scope.auctionItem.lotId <= 0) {
                            item.bestPrice += (item2.revUnitPrice * item2.productQuantity);
                        }

                        if (item2.isRegret) {
                            item.hasRegretItems = true;
                        }

                        if (item2.productQuantityIn.toUpperCase() != item2.vendorUnits.toUpperCase()) {
                            item.isUOMDifferent = true;
                        }

                        if (item2.productQuotationTemplateJson && item2.productQuotationTemplateJson != '' && item2.productQuotationTemplateJson != null && item2.productQuotationTemplateJson != undefined) {
                            item2.productQuotationTemplateArray = JSON.parse(item2.productQuotationTemplateJson);
                        }
                        else {
                            item2.productQuotationTemplateArray = [];
                        }

                    });

                    if (!$scope.multipleAttachmentsList) {
                        $scope.multipleAttachmentsList = [];
                    }
                    if (!item.multipleAttachmentsList) {
                        item.multipleAttachmentsList = [];
                    }

                    if (item.multipleAttachments != '' && item.multipleAttachments != null && item.multipleAttachments != undefined) {
                        var multipleAttachmentsList = item.multipleAttachments.split(',');
                        item.multipleAttachmentsList = [];
                        $scope.multipleAttachmentsList = [];
                        multipleAttachmentsList.forEach(function (att, index) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: att
                            };

                            item.multipleAttachmentsList.push(fileUpload);
                            $scope.multipleAttachmentsList.push(fileUpload);
                        });
                    }

                    if (item.quotationUrl != '' && item.isQuotationRejected == -1) {
                        $scope.Loding = false;
                        $scope.notviewedcompanynames += item.companyName + ', ';
                        showWarning = true;
                    }

                    if (!$scope.isCustomer && (item.quotationUrl == '' || item.quotationUrl == null || item.quotationUrl == undefined)) {
                        $scope.getUserCredentials();
                    }

                });

                if (showWarning) {
                    $scope.Loding = false;
                    swal("Not Allowed", "You are not allowed to Mark as Complete the Negotiation time until all the Vendor Quotations are validated. (They should be either approved/rejected). The following vendors quotations are yet to be validated. (" + $scope.notviewedcompanynames + ")", "error");
                    return;
                } else {
                    var params = {};
                    params.reqID = $scope.reqId;
                    params.sessionID = userService.getUserToken();
                    params.userID = userService.getUserId();
                    auctionsService.endNegotiation(params)
                        .then(function (response) {
                            window.location.reload();
                        });
                }
            };

            $scope.checkQuotationFreezeTime = function() {
                auctionsService.getdate()
                    .then(function (serverTime) {
                        var CurrentDateToLocal = userService.toLocalDate(serverTime);
                        var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                        var currentMoment = moment(ts);

                        var quotationFreeTime = moment($scope.auctionItem.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                        var quotationFreeTimeMoment = moment(quotationFreeTime);

                        if (quotationFreeTimeMoment < currentMoment) {
                            $scope.vendorTenderQuotations = true;
                        }

                    });
            };

        }]);﻿
prmApp
    .controller('viewTendersCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService", "$stateParams",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, $stateParams) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.totalVendors = 0;
            $scope.totalSubuser = 0;
            $scope.totalInactiveVendors = 0;
            $scope.totalLeads = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage = 8;
            $scope.itemsPerPage2 = 8;
            $scope.maxSize = 8;
            $scope.myAuctions = [];
            $scope.categories = [];
            $scope.reqStatus = 'ALL';
            $scope.biddingType = 'ALL';
            $scope.category = 'ALL CATEGORIES';

            $scope.isSuperUser = userService.getUserObj().isSuperUser;

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            // Clickable dashboard //
            $scope.NavigationFilters = $stateParams.filters;
            // Clickable dashboard //

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };

            $scope.pageChanged = function () {
                //$scope.getAuctions();
            };

            $scope.getAuctions = function (isfilter) {
                $log.info($scope.formRequest.isForwardBidding);
                if (!$scope.formRequest.isForwardBidding) {


                    if (isfilter) {
                        $scope.totalItems = 0;
                        $scope.currentPage = 1;
                        $scope.currentPage2 = 1;
                        $scope.itemsPerPage = 8;
                        $scope.itemsPerPage2 = 8;
                        $scope.maxSize = 8;
                        $scope.myAuctions = [];
                    }

                    auctionsService.getmyAuctions({ "userid": userService.getUserId(), "reqstatus": $scope.reqStatus, "sessionid": userService.getUserToken() })
                        .then(function (response) {
                            $scope.myAuctions1 = [];
                            //$scope.myAuctions1 = $scope.myAuctions.concat(response);
                            $scope.myAuctions1 = response;
                            $scope.myAuctions1 = $scope.myAuctions1.filter(function (auction) {
                                return (auction.biddingType === 'TENDER');
                            });

                            $scope.myAuctions1.forEach(function (item, index) {
                                item.postedOn = $scope.GetDateconverted(item.postedOn);
                                item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                                item.expStartTime = $scope.GetDateconverted(item.expStartTime);
                                item.startTime = $scope.GetDateconverted(item.startTime);

                                if (String(item.startTime).includes('9999')) {
                                    item.startTime = '';
                                }


                                if (item.status == 'UNCONFIRMED') {
                                    item.statusColor = 'text-warning';
                                }
                                else if (item.status == 'NOT STARTED') {
                                    item.statusColor = 'text-warning';
                                }
                                else if (item.status == 'STARTED') {
                                    item.statusColor = 'text-danger';
                                }
                                else if (item.status == 'Negotiation Ended') {
                                    item.statusColor = 'text-success';
                                }
                                else if (item.status == 'Negotiation Ended') {
                                    item.statusColor = 'text-success';
                                }
                                else {
                                    item.statusColor = '';
                                }


                                if (item.isRFP) {
                                    item.rfpStatusColor = 'orangered';
                                } else {
                                    item.rfpStatusColor = 'darkgreen';
                                }



                            })

                            $scope.myAuctions = $scope.myAuctions1;

                            //$scope.participatedVendors = $scope.auctionItem.auctionVendors.filter(function (vendor) {
                            //    return (vendor.runningPrice > 0 && vendor.companyName != 'PRICE_CAP');
                            //});

                            //if ($scope.myAuctions1 && $scope.myAuctions1.length > 0)
                            //{
                            //    $scope.totalItems = $scope.myAuctions1[0].totalCount;
                            //    for (var i = 0; i <= $scope.myAuctions1.length; i++) {
                            //        if ($scope.myAuctions1[i] && $scope.myAuctions1[i].requirementID > 0)
                            //        {
                            //            $scope.myAuctions[(($scope.currentPage - 1) * $scope.itemsPerPage) + i] = $scope.myAuctions1[i];
                            //        }

                            //    }
                            //}




                            if ($scope.myAuctions.length > 0) {
                                $scope.myAuctionsLoaded = true;
                                $scope.totalItems = $scope.myAuctions.length;
                            } else {
                                $scope.myAuctionsLoaded = false;
                                $scope.totalItems = 0;
                                $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                            }



                            $scope.tempStatusFilter = angular.copy($scope.myAuctions);
                            $scope.tempCategoryFilter = angular.copy($scope.myAuctions);


                            // Clickable dashboard //

                            if ($scope.NavigationFilters && $scope.NavigationFilters.status) {
                                $scope.getStatusFilter($scope.NavigationFilters.status);
                                $scope.reqStatus = $scope.NavigationFilters.status;
                            }

                            // Clickable dashboard //

                        });
                }
                else {
                    fwdauctionsService.getmyAuctions({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                        .then(function (response) {
                            $scope.myAuctions = response;
                            if ($scope.myAuctions.length > 0) {
                                $scope.myAuctionsLoaded = true;
                                $scope.totalItems = $scope.myAuctions.length;
                            } else {
                                $scope.myAuctionsLoaded = false;
                                $scope.totalItems = 0;
                                $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                            }
                        });
                }
            };

            $scope.getAuctions();

            $scope.GetRequirementsReport = function () {
                auctionsService.getrequirementsreport({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.reqReport = response;
                        alasql.fn.handleDate = function (date) {
                            return new moment(date).format("MM/DD/YYYY");
                        }
                        alasql('SELECT requirementID as RequirementID, title as RequirementTitle, handleDate(postedDate) as [PostedDate], status as Status, vendorID as VendorID, companyName as CompanyName, userPhone as Phone,userEmail as Email, itemID as [ItemID], productBrand as Brand, productQuantity as Quantity, productQuantityIn as [Units], costPrice as [InitialCostPrice], revCostPrice as [FinalCostPrice], netPrice as [InitialNetPrice], marginAmount as [InitialMarginAmount], unitDiscount as [MarginPercentage],revUnitDiscount as [RevisedMarginPercentage],unitMRP as [MRP],gst as [GST],savings as [Savings], handleDate(startTime) as [StartTime], status as [Status], othersBrands as [ManufacturerName] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xlsx", $scope.reqReport]);
                    });

                //alasql('SELECT requirementID as [RequirementID],title as [Title],vendorID as [VendorID],companyName as [Company],userPhone as [Phone],userEmail as [Email],itemID as [ItemID],productBrand as [Brand],productQuantity as [Quantity], productQuantityIn as [Units],costPrice as [InitialCostPrice],revCostPrice as [FinalCostPrice],netPrice as [NetPrice],marginAmount as [InitialMarginAmount],unitDiscount as [InitialMargin],revUnitDiscount as [FinalMargin],unitMRP as [MRP],gst as [GST],maxInitMargin as [MaximumInitialMargin],maxFinalMargin as [MaximumFinalMargin],savings as [Savings] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xslx", $scope.reqReport]);

            };
            
            $scope.goToReqReport = function (reqID) {
                    //$state.go("reports", { "reqID": reqID });

                    var url = $state.href('reports', { "reqID": reqID });
                    $window.open(url, '_blank');

            };

            $scope.cloneRequirement = function (requirement) {
                let tempObj = {};
                let stateView = 'save-requirementAdv';
                if (requirement.biddingType && requirement.biddingType === 'TENDER') {
                    stateView = 'save-tender';
                }
                
                tempObj.cloneId = requirement.requirementID;
                $state.go(stateView, { 'Id': requirement.requirementID, reqObj: tempObj });
            };

            $scope.searchTable = function (str) {

                //$scope.category = 'ALL CATEGORIES';
                //$scope.reqStatus = 'ALL';
                //$scope.myAuctions = $scope.myAuctions1.filter(function (req) {
                //    return (String(req.requirementID).includes(str) == true || String(req.title).includes(str) == true || req.category[0].includes(str) == true );
                //});

                // RFQ Date Based Filter // 

                str = str.toLowerCase();
                $scope.category = 'ALL CATEGORIES';
                $scope.reqStatus = 'ALL';
                $scope.myAuctions = $scope.myAuctions1.filter(function (req) {
                    return (String(req.requirementID).includes(str) == true
                        || String(req.title.toLowerCase()).includes(str) == true
                        || String(req.userName.toLowerCase()).includes(str) == true
                        || String(req.postedOn).includes(str) == true
                        || req.category[0].toLowerCase().includes(str) == true);
                });

                // RFQ Date Based Filter //

                $scope.totalItems = $scope.myAuctions.length;
            };

            
            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));

                            $scope.categories.push('ALL CATEGORIES');

                            //categories.splice(categories.indexOf("ALL"), 1);
                            //categories.unshift('ALL');

                            $scope.categories = $scope.categories.filter(item => item !== "ALL CATEGORIES" && item !== "");
                            $scope.categories.unshift("ALL CATEGORIES");

                            //console.log($scope.categories);

                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    }
                }, function (result) {
                });

            };


            $scope.getCategories();

            $scope.getStatusFilter = function (filterVal) {
                $scope.filterArray = [];
                if (filterVal == 'ALL') {
                    $scope.myAuctions = $scope.myAuctions1;

                } else {
                    $scope.filterArray = $scope.tempStatusFilter.filter(function (item) {
                        return $scope.prmStatus($scope.isCustomer, item.status) === $scope.prmStatus($scope.isCustomer, filterVal);
                    });
                    $scope.myAuctions = $scope.filterArray;
                }

                $scope.totalItems = $scope.myAuctions.length;

            };

            $scope.getCategoryFilter = function (filterCategoryVal) {
                $scope.filterCatArray = [];
                if (filterCategoryVal == 'ALL CATEGORIES') {
                    $scope.myAuctions = $scope.myAuctions1;
                } else {
                    $scope.filterCatArray = $scope.tempCategoryFilter.filter(function (item) {
                        return item.category[0] == filterCategoryVal;
                    });
                    $scope.myAuctions = $scope.filterCatArray;

                }

                $scope.totalItems = $scope.myAuctions.length;
            };
           
        }]);