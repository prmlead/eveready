﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Models.Masters
{
    public class DeliveryTermGetModel
    {
        public int[] Departments { get; set; }

        public int[] Projects { get; set; }
    }
}