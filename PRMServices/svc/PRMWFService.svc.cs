﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Common;
using PRMServices.Models;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Xml;
using PRMServices.SQLHelper;
using CORE = PRM.Core.Common;
using System.Net.Http;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMWFService : IPRMWFService
    {
        private string templateFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");
        PRMServices prm = new PRMServices();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #region Get
        public List<stringCIJ> GetRFQCIJList(int userID, string sessionID)
        {
            List<stringCIJ> listStstringCIJ = new List<stringCIJ>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("wf_GetRFQCIJList", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        stringCIJ stringcij = new stringCIJ();

                        stringcij.CijID = row["CIJ_ID"] != DBNull.Value ? Convert.ToInt32(row["CIJ_ID"]) : 0;
                        stringcij.CijCode = row["CIJ_CODE"] != DBNull.Value ? Convert.ToString(row["CIJ_CODE"]) : string.Empty;
                        stringcij.Cij = row["CIJ_STRING"] != DBNull.Value ? Convert.ToString(row["CIJ_STRING"]) : string.Empty;
                        stringcij.CijType = row["CIJ_TYPE"] != DBNull.Value ? Convert.ToString(row["CIJ_TYPE"]) : string.Empty;
                        stringcij.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                        stringcij.Status = row["CURRENT_STATUS"] != DBNull.Value ? Convert.ToString(row["CURRENT_STATUS"]) : "PENDING";
                        stringcij.RequestedBy = row["REQUESTOR_NAME"] != DBNull.Value ? Convert.ToString(row["REQUESTOR_NAME"]) : "";

                        listStstringCIJ.Add(stringcij);
                    }

                    listStstringCIJ = listStstringCIJ.OrderByDescending(v => v.CijID).ToList();

                }
            }
            catch (Exception ex)
            {
                stringCIJ stringcij = new stringCIJ();
                stringcij.ErrorMessage = ex.Message;
                listStstringCIJ.Add(stringcij);
            }

            return listStstringCIJ;
        }

        public List<MPIndent> GetRFQIndentList(int userID, string sessionID)
        {
            List<MPIndent> listMPIndent = new List<MPIndent>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("wf_GetRFQIndentList", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        MPIndent mpindent = new MPIndent();
                        mpindent.IndID = row["I_ID"] != DBNull.Value ? Convert.ToInt32(row["I_ID"]) : 0;
                        mpindent.Status = row["CURRENT_STATUS"] != DBNull.Value ? Convert.ToString(row["CURRENT_STATUS"]) : string.Empty;
                        mpindent.CIJ = new stringCIJ();
                        mpindent.CIJ.CijID = row["CIJ_ID"] != DBNull.Value ? Convert.ToInt32(row["CIJ_ID"]) : 0;
                        mpindent.CIJ.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                        mpindent.IndNo = row["INDENT_CODE"] != DBNull.Value ? Convert.ToString(row["INDENT_CODE"]) : string.Empty;
                        mpindent.Department = row["DEPT"] != DBNull.Value ? Convert.ToString(row["DEPT"]) : string.Empty;
                        mpindent.RequestDate = row["CREATED_DATE"] != DBNull.Value ? Convert.ToString(row["CREATED_DATE"]) : string.Empty;
                        mpindent.RequestedBy = row["REQUESTED_BY"] != DBNull.Value ? Convert.ToString(row["REQUESTED_BY"]) : string.Empty;
                        mpindent.ApprovedBy = row["APPROVED_BY"] != DBNull.Value ? Convert.ToString(row["APPROVED_BY"]) : string.Empty;
                        mpindent.PurchaseIncharge = row["PURCHASE_INCHARGE"] != DBNull.Value ? Convert.ToString(row["PURCHASE_INCHARGE"]) : string.Empty;
                        mpindent.CreatedBy = row["CREATED_BY"] != DBNull.Value ? Convert.ToInt32(row["CREATED_BY"]) : 0;
                        listMPIndent.Add(mpindent);
                    }

                    listMPIndent = listMPIndent.OrderByDescending(v => v.IndID).ToList();

                }
            }
            catch (Exception ex)
            {
                MPIndent mpindent = new MPIndent();
                mpindent.ErrorMessage = ex.Message;
                listMPIndent.Add(mpindent);
            }

            return listMPIndent;
        }

        public Workflow[] GetWorkflows(int compID, int wfID, int deptID, string sessionID)
        {
            List<Workflow> details = new List<Workflow>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_WF_ID", wfID);
                sd.Add("P_U_DEPT_ID", deptID);
                DataSet ds = sqlHelper.SelectList("wf_GetWorkflows", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Workflow detail = WFUtility.GetWorkflowObject(row);
                        details.Add(detail);
                    }
                }

                if (ds != null && ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                {
                    foreach (Workflow wf in details)
                    {
                        List<WorkflowStages> stages = new List<WorkflowStages>();
                        DataRow[] rows = (from DataRow dr in ds.Tables[1].Rows
                                          where Convert.ToInt32(dr["WF_ID"]) == wf.WorkflowID
                                          select dr).ToArray();
                        foreach (DataRow row in rows)
                        {
                            WorkflowStages stage = WFUtility.GetWorkflowStagesObject(row);
                            stages.Add(stage);
                        }

                        wf.Stages = stages.ToArray();
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                Workflow error = new Workflow();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public Workflow[] GetItemWorkflows(int wfID, int moduleID, string module, string sessionID, int userID)
        {
            List<Workflow> details = new List<Workflow>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_WF_ID", wfID);
                sd.Add("P_MODULE_ID", moduleID);
                sd.Add("P_WF_MODULE", module);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("wf_GetItemWorkflow", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Workflow detail = WFUtility.GetWorkflowObject(row);
                        details.Add(detail);
                    }
                }

                if (ds != null && ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                {
                    foreach (Workflow wf in details)
                    {
                        List<WorkflowTrack> stages = new List<WorkflowTrack>();
                        DataRow[] rows = (from DataRow dr in ds.Tables[1].Rows
                                          where Convert.ToInt32(dr["WF_ID"]) == wf.WorkflowID
                                          select dr).ToArray();
                        foreach (DataRow row in rows)
                        {
                            WorkflowTrack stage = WFUtility.GetWorkflowTrackObject(row);
                            stages.Add(stage);
                        }

                        wf.Tracks = stages.ToArray();
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                Workflow error = new Workflow();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public WorkflowAudit[] GetWorkflowAudit(int trackID, string sessionID)
        {
            List<WorkflowAudit> details = new List<WorkflowAudit>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_WF_TRACK_ID", trackID);
                DataSet ds = sqlHelper.SelectList("wf_GetWorkflowAudit", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        WorkflowAudit detail = WFUtility.GetWorkflowAuditObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                WorkflowAudit error = new WorkflowAudit();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public WorkflowTrack[] GetWorkflowTrack(int trackID, string sessionID)
        {
            List<WorkflowTrack> details = new List<WorkflowTrack>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_WF_TRACK_ID", trackID);
                sd.Add("P_WF_ID", 0);
                DataSet ds = sqlHelper.SelectList("wf_GetWorkflowTrack", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        WorkflowTrack detail = WFUtility.GetWorkflowTrackObject(ds.Tables[0].Rows[0]);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                WorkflowTrack error = new WorkflowTrack();
                error.Message = ex.Message;
                details.Clear();
                details.Add(error);
            }

            return details.ToArray();
        }

        public List<UserInfo> GetUsersForWFEmails(int approverID, string sessionID, int deptID, int desigID, string type)
        {
            List<UserInfo> details = new List<UserInfo>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_APPROVER_ID", approverID);
                sd.Add("P_DEPT_ID", deptID);
                sd.Add("P_DESIG_ID", desigID);
                sd.Add("P_TYPE", type);
                DataSet ds = sqlHelper.SelectList("wf_GetWfEmailUsers", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        UserInfo detail = WFUtility.GetUsersForWFEmailsObj(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                UserInfo error = new UserInfo();
                error.ErrorMessage = ex.Message;
                details.Clear();
                details.Add(error);
            }

            return details;
        }

        public bool IsUserApproverForStage(int approverID, int userID, string sessionID)
        {
            bool isuserapproverforstage = false;
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_APPROVER_ID", approverID);
                sd.Add("P_DEPT_ID", 0);
                sd.Add("P_DESIG_ID", 0);
                sd.Add("P_TYPE", string.Empty);
                DataSet ds = sqlHelper.SelectList("wf_GetWfEmailUsers", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        UserInfo detail = WFUtility.GetUsersForWFEmailsObj(ds.Tables[0].Rows[0]);
                        if (detail.UserID == userID.ToString())
                        {
                            isuserapproverforstage = true;
                        }
                        else
                        {
                            isuserapproverforstage = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                UserInfo error = new UserInfo();
                error.ErrorMessage = ex.Message;
                isuserapproverforstage = false;
            }

            return isuserapproverforstage;
        }

        public List<Workflow> GetMyWorkflows(int userID, string status, string sessionID)
        {
            List<Workflow> listWorkflows = new List<Workflow>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                sd.Add("P_STATUS", status);
                DataSet ds = sqlHelper.SelectList("wf_GetMyWorkflows", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Workflow workflow = new Workflow();
                        workflow.WorkflowModule = row["WF_MODULE"] != DBNull.Value ? Convert.ToString(row["WF_MODULE"]) : string.Empty;
                        WorkflowTrack workflowtrack = new WorkflowTrack();
                        workflowtrack.Status = row["WF_STATUS"] != DBNull.Value ? Convert.ToString(row["WF_STATUS"]) : string.Empty;
                        MPIndent mpindent = new MPIndent();
                        mpindent.IndNo = row["INDENT_CODE"] != DBNull.Value ? Convert.ToString(row["INDENT_CODE"]) : string.Empty;
                        mpindent.Department = row["DEPT"] != DBNull.Value ? Convert.ToString(row["DEPT"]) : string.Empty;
                        mpindent.RequestDate = row["CREATED_DATE"] != DBNull.Value ? Convert.ToString(row["CREATED_DATE"]) : string.Empty;
                        mpindent.RequestedBy = row["REQUESTED_BY"] != DBNull.Value ? Convert.ToString(row["REQUESTED_BY"]) : string.Empty;
                        mpindent.ApproximateCostRange = row["APPROX_COST_RANGE"] != DBNull.Value ? Convert.ToDecimal(row["APPROX_COST_RANGE"]) : 0;
                        mpindent.IndID = row["I_ID"] != DBNull.Value ? Convert.ToInt32(row["I_ID"]) : 0;
                        mpindent.IsMedical = row["IS_MEDICAL"] != DBNull.Value ? Convert.ToInt32(row["IS_MEDICAL"]) : 0;
                        mpindent.IsCapex = row["IS_CAPEX"] != DBNull.Value ? Convert.ToInt32(row["IS_CAPEX"]) : 0;
                        mpindent.DeptID = row["DEPT_ID"] != DBNull.Value ? Convert.ToInt32(row["DEPT_ID"]) : 0;
                        mpindent.UserStatus = row["USER_STATUS"] != DBNull.Value ? Convert.ToString(row["USER_STATUS"]) : string.Empty;
                        mpindent.UserPriority = row["USER_PRIORITY"] != DBNull.Value ? Convert.ToString(row["USER_PRIORITY"]) : string.Empty;
                        mpindent.ExpectedDelivery = row["EXPECTED_DELIVERY"] != DBNull.Value ? Convert.ToString(row["EXPECTED_DELIVERY"]) : string.Empty;
                        workflow.Track = workflowtrack;
                        workflow.Indent = mpindent;
                        listWorkflows.Add(workflow);
                    }

                    listWorkflows = listWorkflows.OrderBy(v => v.Indent.RequestDate).ToList();
                }
            }
            catch (Exception ex)
            {
                Workflow workflow = new Workflow();
                workflow.ErrorMessage = ex.Message;
                listWorkflows.Add(workflow);
            }

            return listWorkflows;
        }

        public List<MPIndent> GetMyIndents(int userID, string sessionID)
        {
            List<MPIndent> listIndents = new List<MPIndent>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("wf_GetMyIndents", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        MPIndent mpindent = new MPIndent();

                        mpindent.IndID = row["I_ID"] != DBNull.Value ? Convert.ToInt32(row["I_ID"]) : 0;
                        mpindent.IndNo = row["INDENT_CODE"] != DBNull.Value ? Convert.ToString(row["INDENT_CODE"]) : string.Empty;
                        mpindent.Department = row["DEPT"] != DBNull.Value ? Convert.ToString(row["DEPT"]) : string.Empty;
                        mpindent.RequestDate = row["CREATED_DATE"] != DBNull.Value ? Convert.ToString(row["CREATED_DATE"]) : string.Empty;
                        mpindent.RequestedBy = row["REQUESTED_BY"] != DBNull.Value ? Convert.ToString(row["REQUESTED_BY"]) : string.Empty;
                        mpindent.ApproximateCostRange = row["APPROX_COST_RANGE"] != DBNull.Value ? Convert.ToDecimal(row["APPROX_COST_RANGE"]) : 0;
                        mpindent.ReqID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        mpindent.UserID = row["CREATED_BY"] != DBNull.Value ? Convert.ToInt32(row["CREATED_BY"]) : 0;
                        listIndents.Add(mpindent);
                    }
                }
            }
            catch (Exception ex)
            {
                MPIndent mpindent = new MPIndent();
                mpindent.ErrorMessage = ex.Message;
                listIndents.Add(mpindent);
            }

            return listIndents;
        }

        public List<MPIndent> DBGetMyPendingApprovals(int userID, string sessionID)
        {
            List<MPIndent> listIndents = new List<MPIndent>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("wf_DBGetMyPendingApprovals", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        MPIndent mpindent = new MPIndent();
                        mpindent.IndID = row["I_ID"] != DBNull.Value ? Convert.ToInt32(row["I_ID"]) : 0;
                        mpindent.IndNo = row["INDENT_CODE"] != DBNull.Value ? Convert.ToString(row["INDENT_CODE"]) : string.Empty;
                        mpindent.Department = row["DEPT"] != DBNull.Value ? Convert.ToString(row["DEPT"]) : string.Empty;
                        mpindent.RequestDate = row["CREATED_DATE"] != DBNull.Value ? Convert.ToString(row["CREATED_DATE"]) : string.Empty;
                        mpindent.RequestedBy = row["REQUESTED_BY"] != DBNull.Value ? Convert.ToString(row["REQUESTED_BY"]) : string.Empty;
                        mpindent.ApproximateCostRange = row["APPROX_COST_RANGE"] != DBNull.Value ? Convert.ToDecimal(row["APPROX_COST_RANGE"]) : 0;
                        mpindent.ReqID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        mpindent.UserID = row["CREATED_BY"] != DBNull.Value ? Convert.ToInt32(row["CREATED_BY"]) : 0;
                        mpindent.IsMedical = row["IS_MEDICAL"] != DBNull.Value ? Convert.ToInt32(row["IS_MEDICAL"]) : 0;
                        mpindent.Status = row["WF_STATUS"] != DBNull.Value ? Convert.ToString(row["WF_STATUS"]) : string.Empty;
                        listIndents.Add(mpindent);
                    }
                }
            }
            catch (Exception ex)
            {
                MPIndent mpindent = new MPIndent();
                mpindent.ErrorMessage = ex.Message;
                listIndents.Add(mpindent);
            }

            return listIndents;
        }

        public List<WFPendingApprovals> GetMyPendingApprovals(int userID, string deptID, string desigID, string sessionID)
        {
            List<WFPendingApprovals> listWFPendingApprovals = new List<WFPendingApprovals>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                sd.Add("P_DEPT_ID", deptID);
                sd.Add("P_DESIG_ID", desigID);
                DataSet ds = sqlHelper.SelectList("wf_GetMyPendingApprovals", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        WFPendingApprovals wfpendingapprovals = new WFPendingApprovals();

                        wfpendingapprovals.WorkflowID = row["WF_ID"] != DBNull.Value ? Convert.ToInt32(row["WF_ID"]) : 0;
                        wfpendingapprovals.CompanyID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                        wfpendingapprovals.WorkflowTitle = row["WF_TITLE"] != DBNull.Value ? Convert.ToString(row["WF_TITLE"]) : string.Empty;
                        //wfpendingapprovals.WorkflowStatus = row["DEPT"] != DBNull.Value ? Convert.ToString(row["DEPT"]) : string.Empty;
                        wfpendingapprovals.WorkflowModule = row["WF_MODULE"] != DBNull.Value ? Convert.ToString(row["WF_MODULE"]) : string.Empty;
                        wfpendingapprovals.WorkflowModuleID = row["MODULE_ID"] != DBNull.Value ? Convert.ToInt32(row["MODULE_ID"]) : 0;
                        wfpendingapprovals.WorkflowModuleCode = row["WF_MD_CODE"] != DBNull.Value ? Convert.ToString(row["WF_MD_CODE"]) : string.Empty;
                        wfpendingapprovals.WorkflowModuleUserStatus = row["WF_USER_STATUS"] != DBNull.Value ? Convert.ToString(row["WF_USER_STATUS"]) : string.Empty;
                        wfpendingapprovals.WorkflowModuleViewID = row["WF_MODULE_VIEW_ID"] != DBNull.Value ? Convert.ToInt32(row["WF_MODULE_VIEW_ID"]) : 0;
                        wfpendingapprovals.QcsWorkFlow = row["QCS_TYPE"] != DBNull.Value ? Convert.ToString(row["QCS_TYPE"]) : string.Empty;
                        wfpendingapprovals.CreatorName = row["CREATOR_NAME"] != DBNull.Value ? Convert.ToString(row["CREATOR_NAME"]) : string.Empty;
                        wfpendingapprovals.ReqId = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        wfpendingapprovals.ReqNumber = row["REQ_NUMBER"] != DBNull.Value ? Convert.ToString(row["REQ_NUMBER"]) : string.Empty;

                        listWFPendingApprovals.Add(wfpendingapprovals);
                    }
                }
            }
            catch (Exception ex)
            {
                WFPendingApprovals wfpendingapprovals = new WFPendingApprovals();
                wfpendingapprovals.ErrorMessage = ex.Message;
                listWFPendingApprovals.Add(wfpendingapprovals);
            }

            return listWFPendingApprovals;
        }



        public Response GetSeries(string series, string seriesType, string sessionID, int compID, int deptID)
        {
            Response response = new Response();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_SERIES", series);
                sd.Add("P_SERIES_TYPE", seriesType);
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_DEPT_ID", deptID);
                DataSet ds = sqlHelper.SelectList("wf_GetSeries", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.Message = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    response.Message = response.Message.ToUpper();
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public List<int> GetRFQCreators(int indentID, string sessionID)
        {
            List<int> listAssignedUser = new List<int>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_INDENT_ID", indentID);
                DataSet ds = sqlHelper.SelectList("wf_GetRFQCreators", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        int userID = 0;

                        userID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;


                        listAssignedUser.Add(userID);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return listAssignedUser;
        }

        public DBStatus GetDBStatus(int userID, string sessionID, int department, string startDate, string endDate, int isCapex, int isMedical)
        {
            DBStatus dbstatus = new DBStatus();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                sd.Add("P_F_DEPT", department);
                sd.Add("P_F_START_DATE", startDate);
                sd.Add("P_F_END_DATE", endDate);
                sd.Add("P_F_IS_CAPEX", isCapex);
                sd.Add("P_F_IS_MEDICAL", isMedical);
                DataSet ds = sqlHelper.SelectList("wf_DB_Status", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];

                    dbstatus.NoOfReqClosed = row["PARAM_NO_OF_REQ_CLOSED"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_REQ_CLOSED"]) : 0;
                    dbstatus.NoOfCapexClosed = row["PARAM_NO_OF_CAPEX_CLOSED"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_CAPEX_CLOSED"]) : 0;
                    dbstatus.NoOfNonCapexClosed = row["PARAM_NO_OF_NON_CAPEX_CLOSED"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_NON_CAPEX_CLOSED"]) : 0;
                    dbstatus.NoOfCapexPending = row["PARAM_NO_OF_CAPEX_PENDING"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_CAPEX_PENDING"]) : 0;
                    dbstatus.NoOfNonCapexPending = row["PARAM_NO_OF_NON_CAPEX_PENDING"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_NON_CAPEX_PENDING"]) : 0;
                    dbstatus.NoOfWorkorderClosed = row["PARAM_NO_OF_WORKORDER_CLOSED"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_WORKORDER_CLOSED"]) : 0;
                    dbstatus.NoOfWorkorderPending = row["PARAM_NO_OF_WORKORDER_PENDING"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_WORKORDER_PENDING"]) : 0;

                }
            }
            catch (Exception ex)
            {
                dbstatus.ErrorMessage = ex.Message;
            }

            return dbstatus;
        }

        public List<DashboardUser> GetUserWiseDashboard(int userID, string sessionID, int department, string startDate, string endDate, int isCapex, int isMedical)
        {
            List<DashboardUser> listDashboardUser = new List<DashboardUser>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                sd.Add("P_F_DEPT", department);
                sd.Add("P_F_START_DATE", startDate);
                sd.Add("P_F_END_DATE", endDate);
                sd.Add("P_F_IS_CAPEX", isCapex);
                sd.Add("P_F_IS_MEDICAL", isMedical);
                DataSet ds = sqlHelper.SelectList("wf_GetUserWiseDashboard", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DashboardUser dashboardUser = new DashboardUser();
                        dashboardUser.DashboardProp = new DashboardStats();
                        dashboardUser.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        dashboardUser.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        dashboardUser.LastName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        dashboardUser.PhoneNum = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                        dashboardUser.Email = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                        dashboardUser.DashboardProp.NoOfIndentsReceived = row["NO_OF_INDENTS_RECEIVED"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_INDENTS_RECEIVED"]) : 0;
                        dashboardUser.DashboardProp.NoOfPendingRFQ = row["NO_OF_PENDING_RFQ"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_PENDING_RFQ"]) : 0;
                        dashboardUser.DashboardProp.NoOfClosedRFQ = row["NO_OF_CLOSED_RFQ"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_CLOSED_RFQ"]) : 0;
                        listDashboardUser.Add(dashboardUser);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return listDashboardUser;
        }

        public List<DashboardUser> GetDeptWiseDashboard(int userID, string sessionID, int department, string startDate, string endDate, int isCapex, int isMedical)
        {
            List<DashboardUser> listDashboardUser = new List<DashboardUser>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                sd.Add("P_F_DEPT", department);
                sd.Add("P_F_START_DATE", startDate);
                sd.Add("P_F_END_DATE", endDate);
                sd.Add("P_F_IS_CAPEX", isCapex);
                sd.Add("P_F_IS_MEDICAL", isMedical);
                DataSet ds = sqlHelper.SelectList("cp_GetDeptWiseDashboard", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DashboardUser dashboardUser = new DashboardUser();
                        dashboardUser.DashboardProp = new DashboardStats();
                        dashboardUser.UserID = row["DEPT_ID"] != DBNull.Value ? Convert.ToInt32(row["DEPT_ID"]) : 0;
                        dashboardUser.FirstName = row["DEPT_CODE"] != DBNull.Value ? Convert.ToString(row["DEPT_CODE"]) : string.Empty;
                        dashboardUser.DashboardProp.NoOfIndentsReceived = row["NO_OF_INDENTS_RECEIVED"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_INDENTS_RECEIVED"]) : 0;
                        dashboardUser.DashboardProp.NoOfPendingRFQ = row["NO_OF_PENDING_RFQ"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_PENDING_RFQ"]) : 0;
                        dashboardUser.DashboardProp.NoOfClosedRFQ = row["NO_OF_CLOSED_RFQ"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_CLOSED_RFQ"]) : 0;
                        listDashboardUser.Add(dashboardUser);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return listDashboardUser;
        }


        public List<WFApproval> GetApprovalList(int userID, string deptID, string desigID, string type, string sessionID)
        {
            List<WFApproval> approvalList = new List<WFApproval>();
            try
            {
                Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                sd.Add("P_DEPT_ID", deptID);
                sd.Add("P_DESIG_ID", desigID);
                sd.Add("P_TYPE", type);
                CORE.DataNamesMapper<WFApproval> mapper = new CORE.DataNamesMapper<WFApproval>();
                var dataset = sqlHelper.SelectList("wf_GetApprovalList", sd);
                approvalList = mapper.Map(dataset.Tables[0]).ToList();

            }
            catch (Exception ex)
            {

            }

            return approvalList;
        }
        


        #endregion

        #region Post

        public Response SaveWorkflow(Workflow workflow)
        {
            Response response = new Response();
            try
            {
                int a = 0;
                string error = "";
                DataSet ds = WFUtility.SaveWorkflowObject(workflow);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                     a = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (a == -2)
                    {
                        error = "Workflow Name Already Exists";
                    }
                    else {
                        error = "";
                    }
                }
                if (a > 0)
                {
                    if (workflow.Stages != null && workflow.Stages.Length > 0)
                    {
                        foreach (var stage in workflow.Stages)
                        {
                            if (stage.WorkflowID <= 0 && ds.Tables.Count > 0)
                            {
                                stage.WorkflowID = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                            }

                            ds = WFUtility.SaveWorkflowStageObject(stage);
                        }
                    }
                }
                else
                {

                }
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = error;
                }


            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveWorkflowStage(WorkflowStages stage)
        {
            Response response = new Response();
            try
            {
                DataSet ds = WFUtility.SaveWorkflowStageObject(stage);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveWorkflowAudit(WorkflowAudit audit)
        {
            Response response = new Response();
            try
            {
                DataSet ds = WFUtility.SaveWorkflowAuditObject(audit);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveWorkflowTrack(WorkflowTrack track)
        {
            Response response = new Response();
            try
            {
                DataSet ds = WFUtility.SaveWorkflowTrackObject(track);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    SaveWorkflowTrackAlerts(track);
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveApprovalTrack(WorkflowTrack track)
        {
            Response response = new Response();
            try
            {
                DataSet ds = WFUtility.SaveApprovalTrackObject(track);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    //SaveWorkflowTrackAlerts(track);
                    DateTime dateTime = DateTime.Now;
                    UserInfo createdBy = prm.GetUser(track.CreatedBy, track.SessionID);
                    UserInfo modifiedBy = prm.GetUser(track.ModifiedBY, track.SessionID);
                    var requirement = prm.GetRequirementDataLite(track.SubModuleID, track.SessionID);
                    string body = string.Empty;
                    string bodySMS = string.Empty;
                    string subject = string.Empty;

                    if (track.Status.ToUpper() == "APPROVED")
                    {
                        body = GenerateEmailBody("QCSApprovedSelfEmail");
                        bodySMS = GenerateEmailBody("QCSApprovedSelfSMS");
                        subject = "QCS has been approved for REQ-NUMBER: " + requirement.REQ_NUMBER + " and REQ-TITLE: " + track.SubModuleName;
                    }
                    else
                    {
                        body = GenerateEmailBody("QCSRejectedSelfEmail");
                        bodySMS = GenerateEmailBody("QCSRejectedSelfSMS");
                        subject = "QCS has been Rejected for REQ-NUMBER: " + requirement.REQ_NUMBER + " and REQ-TITLE: " + track.SubModuleName;
                    }

                    body = String.Format(body, createdBy.FirstName, createdBy.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                    prm.SendEmail(createdBy.Email, subject, body, 0, 0, "WORKFLOWTRACK", track.SessionID).ConfigureAwait(false);
                    //SendEmail(createdBy.Email, subject, body, track.SessionID, null, ListAttachment).ConfigureAwait(false);
                    bodySMS = String.Format(bodySMS, createdBy.FirstName, createdBy.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                    bodySMS = bodySMS.Replace("<br/>", "");
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }




        private Response SaveWorkflowTrackAlerts(WorkflowTrack track)
        {
            Response response = new Response();
            List<UserInfo> usersToEmail = GetUsersForWFEmails(track.ApproverID, track.SessionID, track.Approver.DeptID, track.Approver.DesigID,"TRACK");
            DateTime dateTime = DateTime.Now;
            List<Attachment> ListAttachment = new List<Attachment>();
            List<string> emails = new List<string>();
            Attachment emailAttachment = null;
            UserInfo createdBy = prm.GetUser(track.CreatedBy, track.SessionID);
            UserInfo modifiedBy = prm.GetUser(track.ModifiedBY, track.SessionID);
            if (track.ModuleName == "QUOTATION")
            {
                string body = string.Empty;
                string bodySMS = string.Empty;
                foreach (UserInfo user in usersToEmail)
                {
                    string subject = string.Empty;
                    if (track.Status.ToUpper() == "APPROVED")
                    {
                        body = GenerateEmailBody("QUOTATIONWFApprovedSelfEmail");
                        bodySMS = GenerateEmailBody("QUOTATIONWFApprovedSelfSMS");
                        subject = "Quotation has been approved";
                    }
                    else
                    {
                        body = GenerateEmailBody("QUOTATIONWFRejectedSelfEmail");
                        bodySMS = GenerateEmailBody("QUOTATIONWFRejectedSelfSMS");
                        subject = "Quotation has been Rejected";
                    }

                    if (track.Status.ToUpper() == "APPROVED")
                    {
                        body = GenerateEmailBody("QUOTATIONWFApprovedEndUserEmail");
                        bodySMS = GenerateEmailBody("QUOTATIONWFApprovedEndUserSMS");
                        subject = "Quotation has been approved";
                    }
                    else
                    {
                        body = GenerateEmailBody("QUOTATIONWFRejectedEndUserEmail");
                        bodySMS = GenerateEmailBody("QUOTATIONWFRejectedEndUserSMS");
                        subject = "Quotation has been Rejected";
                    }

                    body = String.Format(body, createdBy.FirstName, createdBy.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                    SendEmail(createdBy.Email, subject, body, track.SessionID, null, ListAttachment).ConfigureAwait(false);
                    bodySMS = String.Format(bodySMS, createdBy.FirstName, createdBy.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                    bodySMS = bodySMS.Replace("<br/>", "");
                }

                Workflow workflow = GetItemWorkflows(track.WorkflowDetails.WorkflowID, track.ModuleID, track.WorkflowDetails.WorkflowModule, track.SessionID, track.CreatedBy).FirstOrDefault();
                if (workflow != null && workflow.Tracks != null && workflow.Tracks.Length > 0)
                {
                    string subject = string.Empty;
                    WorkflowTrack nextTrack = null;
                    if (track.Status.ToUpper() == "APPROVED")
                    {
                        nextTrack = workflow.Tracks.Where(t => t.Order == track.Order + 1).FirstOrDefault();
                    }
                    else if (track.Status.ToUpper() == "REJECTED")
                    {
                        nextTrack = workflow.Tracks.Where(t => t.Order == track.Order - 1).FirstOrDefault();
                    }

                    if (nextTrack != null)
                    {
                        List<UserInfo> nextUsersToNotify = GetUsersForWFEmails(nextTrack.ApproverID, track.SessionID, nextTrack.Approver.DeptID, nextTrack.Approver.DesigID, "TRACK");
                        foreach (UserInfo user in nextUsersToNotify)
                        {
                            if (track.Status.ToUpper() == "APPROVED")
                            {
                                body = GenerateEmailBody("QUOTATIONWFApprovedForwardEmail");
                                bodySMS = GenerateEmailBody("QUOTATIONWFApprovedForwardSMS");
                                subject = "Quotation has been approved";
                            }
                            else if (track.Status.ToUpper() == "REJECTED")
                            {
                                body = GenerateEmailBody("QUOTATIONWFRejectedReverseEmail");
                                bodySMS = GenerateEmailBody("INDENTWFRejectedReverseSMS");
                                subject = "Quotation has been Rejected";
                            }

                            body = String.Format(body, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                            SendEmail(user.Email, subject, body, track.SessionID, null, ListAttachment).ConfigureAwait(false);

                            bodySMS = String.Format(bodySMS, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                            bodySMS = bodySMS.Replace("<br/>", "");
                        }
                    }
                }
            }
            else if (track.ModuleName == "QCS")
            {
                var requirement = prm.GetRequirementDataLite(track.SubModuleID, track.SessionID);
                string body = string.Empty;
                string bodySMS = string.Empty;
                var rfqCreatedByEmail = sqlHelper.ExecuteQuery($"SELECT U.U_EMAIL,U.U_FNAME,U.U_LNAME,U.U_ID,U.U_GENDER,U.U_PHONE,U.U_ALTPHONE,U.U_BIRTHDATE,UA.UAD_DISPNAME,UA.UAD_LINE1,UA.UAD_LINE2,UA.UAD_LINE3,UA.UAD_CITY,UA.UAD_STATE,UA.UAD_COUNTRY,UA.UAD_ZIP,UA.UAD_PHONE, UA.UAD_EXT1,UA.UAD_EXT2,UR.U_TYPE, UL.LG_LOGIN FROM [User] U INNER JOIN UserData UD ON UD.U_ID = U.U_ID INNER JOIN UserAddress UA on UA.U_ID = U.U_ID INNER JOIN UserRoles UR on UR.U_ID = U.U_ID INNER JOIN userlogins UL on UL.U_ID = U.U_ID WHERE U.U_ID IN (select u_id from requirementdetails where req_id in (select req_id from qcs_Details where qcs_id = {track.ModuleID})) AND UD.IS_VALID=1");
                if (rfqCreatedByEmail != null && rfqCreatedByEmail.Tables.Count > 0 && rfqCreatedByEmail.Tables[0].Rows.Count > 0)
                {
                    DataRow row = rfqCreatedByEmail.Tables[0].Rows[0];
                    string email = rfqCreatedByEmail.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToString(rfqCreatedByEmail.Tables[0].Rows[0][0]) : string.Empty;
                    string fname = rfqCreatedByEmail.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(rfqCreatedByEmail.Tables[0].Rows[0][1]) : string.Empty;
                    string lname = rfqCreatedByEmail.Tables[0].Rows[0][2] != DBNull.Value ? Convert.ToString(rfqCreatedByEmail.Tables[0].Rows[0][2]) : string.Empty;
                    emails.Add(email);
                    if (!string.IsNullOrEmpty(email)) {
                        string subject = string.Empty;
                        if (track.Status.ToUpper() == "APPROVED")
                        {
                            body = GenerateEmailBody("QCSApprovedEndUserEmail");
                            subject = "QCS has been approved for REQ-NUMBER: " + requirement.REQ_NUMBER + " and REQ-TITLE: " + track.SubModuleName;
                        }
                        else if (track.Status.ToUpper() == "REJECTED")
                        {
                            body = GenerateEmailBody("QCSRejectedEndUserEmail");
                            subject = "QCS has been Rejected for REQ-NUMBER: " + requirement.REQ_NUMBER + " and REQ-TITLE: " + track.SubModuleName;
                        }

                        if (track.Status.ToUpper() == "APPROVED" || track.Status.ToUpper() == "REJECTED")
                        {
                            body = String.Format(body, fname, lname, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                            prm.SendEmail(email, subject, body, 0, 0, "WORKFLOWTRACK", track.SessionID).ConfigureAwait(false);
                        }
                    }
                }
                foreach (UserInfo user in usersToEmail.Where(a=>a.Email != emails.FirstOrDefault()))
                {
                    string subject = string.Empty;
                    if (track.Status.ToUpper() == "APPROVED")
                    {
                        body = GenerateEmailBody("QCSApprovedSelfEmail");
                        bodySMS = GenerateEmailBody("QCSApprovedSelfSMS");
                        subject = "QCS has been approved for REQ-NUMBER: " + requirement.REQ_NUMBER + " and REQ-TITLE: " + track.SubModuleName;
                    }
                    else if(track.Status.ToUpper() == "REJECTED")
                    {
                        body = GenerateEmailBody("QCSRejectedSelfEmail");
                        bodySMS = GenerateEmailBody("QCSRejectedSelfSMS");
                        subject = "QCS has been Rejected for REQ-NUMBER: " + requirement.REQ_NUMBER + " and REQ-TITLE: " + track.SubModuleName;
                    }

                    if (track.Status.ToUpper() == "APPROVED")
                    {
                        body = GenerateEmailBody("QCSApprovedEndUserEmail");
                        bodySMS = GenerateEmailBody("QCSApprovedEndUserSMS");
                        subject = "QCS has been approved for REQ-NUMBER: " + requirement.REQ_NUMBER + " and REQ-TITLE: " + track.SubModuleName;
                    }
                    else if(track.Status.ToUpper() == "REJECTED")
                    {
                        body = GenerateEmailBody("QCSRejectedEndUserEmail");
                        bodySMS = GenerateEmailBody("QCSRejectedEndUserSMS");
                        subject = "QCS has been Rejected for REQ-NUMBER: " + requirement.REQ_NUMBER + " and REQ-TITLE: " + track.SubModuleName;
                    }

                    if (track.Status.ToUpper() == "APPROVED" || track.Status.ToUpper() == "REJECTED")
                    {
                        body = String.Format(body, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                        prm.SendEmail(user.Email, subject, body, 0, 0, "WORKFLOWTRACK", track.SessionID).ConfigureAwait(false);
                        //SendEmail(createdBy.Email, subject, body, track.SessionID, null, ListAttachment).ConfigureAwait(false);
                        bodySMS = String.Format(bodySMS, createdBy.FirstName, createdBy.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                        bodySMS = bodySMS.Replace("<br/>", "");
                    }
                }

                Workflow workflow = GetItemWorkflows(track.WorkflowDetails.WorkflowID, track.ModuleID, track.WorkflowDetails.WorkflowModule, track.SessionID, track.CreatedBy).FirstOrDefault();
                if (workflow != null && workflow.Tracks != null && workflow.Tracks.Length > 0)
                {
                    string subject = string.Empty;
                    WorkflowTrack nextTrack = null;
                    if (track.Status.ToUpper() == "APPROVED")
                    {
                        nextTrack = workflow.Tracks.Where(t => t.Order == track.Order + 1).FirstOrDefault();
                    }
                    else if (track.Status.ToUpper() == "REJECTED")
                    {
                        nextTrack = workflow.Tracks.Where(t => t.Order == track.Order - 1).FirstOrDefault();
                    }

                    if (nextTrack != null)
                    {
                        List<UserInfo> nextUsersToNotify = GetUsersForWFEmails(nextTrack.ApproverID, track.SessionID, nextTrack.Approver.DeptID, nextTrack.Approver.DesigID, "TRACK");
                        foreach (UserInfo user in nextUsersToNotify)
                        {
                            if (track.Status.ToUpper() == "APPROVED")
                            {
                                body = GenerateEmailBody("QCSApprovedForwardEmail");
                                bodySMS = GenerateEmailBody("QCSApprovedForwardSMS");
                                subject = "QCS has been approved for REQ-NUMBER: " + requirement.REQ_NUMBER + " and REQ-TITLE: " + track.SubModuleName;
                            }
                            else if (track.Status.ToUpper() == "REJECTED")
                            {
                                body = GenerateEmailBody("QCSRejectedReverseEmail");
                                bodySMS = GenerateEmailBody("QCSRejectedReverseSMS");
                                subject = "QCS has been Rejected for REQ-NUMBER: " + requirement.REQ_NUMBER + " and REQ-TITLE: " + track.SubModuleName;
                            }

                            if (track.Status.ToUpper() == "APPROVED" || track.Status.ToUpper() == "REJECTED")
                            {
                                body = String.Format(body, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                                //SendEmail(user.Email, subject, body, track.SessionID, null, ListAttachment).ConfigureAwait(false);
                                prm.SendEmail(user.Email, subject, body, 0, 0, "WORKFLOWTRACK", track.SessionID).ConfigureAwait(false);

                                bodySMS = String.Format(bodySMS, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                                bodySMS = bodySMS.Replace("<br/>", "");
                            }
                        }
                    }
                }
            }
            else if (track.ModuleName == "PR")
            {

                string body = string.Empty;
                string bodySMS = string.Empty;
                
                PRMPRService prmPr = new PRMPRService();
                PRDetails prDet = prmPr.GetPRDetails(track.ModuleID, track.SessionID);

                int i = 1;

                string itemsText = "<table class=\" table table-vmiddle table-responsive\" style=\"background-color:#f5f5f5;border-collapse: collapse;\" border=\"1\">" +
                                    "<tr style=\"width:100%;\" class=\" dl-horizontal\">" +
                                    "<th style=\"width:5%;text-align:center\"> Sl.No </th>" +
                                    "<th style=\"width:10%;text-align:center\"> ITEM NAME </th>" +
                                    "<th style=\"width:15%;text-align:center\"> DESCRIPTION OF MATERIAL </th>" +
                                    "<th style=\"width:8%;text-align:center\"> MAKE </th>" +
                                    "<th style=\"width:8%;text-align:center\"> UOM </th>" +
                                    "<th style=\"width:8%;text-align:center\"> STOCK IN STORES / PIPELINE </th>" +
                                    "<th style=\"width:8%;text-align:center\"> UNIT PRICE </th>" +
                                    "<th style=\"width:8%;text-align:center\"> TAXES(%) </th>" +
                                    "<th style=\"width:10%;text-align:center\"> TOTAL QTY TO BE PROCURED </th>" +
                                    "<th style=\"width:10%;text-align:center\"> TOTAL ESTIMATED COST </th>" +
                                    "</tr>";
                foreach (PRItems prItem in prDet.PRItemsList)
                {
                    if (prItem.REQUIRED_QUANTITY > 0)
                    {
                        string text = "<tr style=\"text-align:center\">" +
                                        "<td>" + i + "</td>" +
                                        "<td>" + prItem.ITEM_NAME + "</td>" +
                                        "<td>" + prItem.ITEM_DESCRIPTION + "</td>" +
                                        "<td>" + prItem.BRAND + "</td>" +
                                        "<td>" + prItem.UNITS + "</td>" +
                                        "<td>" + prItem.EXIST_QUANTITY + "</td>" +
                                        "<td>" + prItem.UNIT_PRICE + "</td>" +
                                        "<td>" + prItem.C_GST_PERCENTAGE + "</td>" +
                                        "<td>" + prItem.REQUIRED_QUANTITY + "</td>" +
                                        "<td>" + prItem.TOTAL_PRICE + "</td>" +
                                       //"<td>" + prItem.TOTAL_PRICE + "</td>" +
                                       "</tr>";
                        itemsText += text;
                        i++;
                    }
                }

                itemsText += "<tr><td colspan='9' style=\"text-align:center\">Grand Total:</td><td>" + prDet.TOTAL_PRICE +"</td></tr>";

                itemsText += "</table>";

                track.ModuleCode = prDet.PR_NUMBER;

                foreach (UserInfo user in usersToEmail)
                {
                    string subject = string.Empty;

                    if (track.Status.ToUpper() == "APPROVED")
                    {
                        body = GenerateEmailBody("PRWFApprovedSelfEmail");
                        bodySMS = GenerateEmailBody("PRWFApprovedSelfSMS");
                        subject = "PR No:-" + track.ModuleCode + " has been approved";
                    }
                    else if (track.Status.ToUpper() == "REJECTED")
                    {
                        body = GenerateEmailBody("PRWFRejectedSelfEmail");
                        bodySMS = GenerateEmailBody("PRWFRejectedSelfSMS");
                        subject = "PR No:-" + track.ModuleCode + " has been Rejected";
                    }

                    //body = String.Format(body, modifiedBy.FirstName, modifiedBy.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime);
                    //SendEmail(user.Email, subject, body, track.SessionID, null, ListAttachment).ConfigureAwait(false);

                    //bodySMS = String.Format(bodySMS, modifiedBy.FirstName, modifiedBy.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime);
                    //bodySMS = bodySMS.Replace("<br/>", "");
                    //SendSMS(string.Empty, user.PhoneNum + "," + user.AltPhoneNum, System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), track.SessionID).ConfigureAwait(false);

                    if (track.Status.ToUpper() == "APPROVED")
                    {
                        body = GenerateEmailBody("PRWFApprovedEndUserEmail");
                        bodySMS = GenerateEmailBody("PRWFApprovedEndUserSMS");
                        subject = "PR No:-" + track.ModuleCode + " has been approved";
                    }
                    else if (track.Status.ToUpper() == "REJECTED")
                    {
                        body = GenerateEmailBody("PRWFRejectedEndUserEmail");
                        bodySMS = GenerateEmailBody("PRWFRejectedEndUserSMS");
                        subject = "PR No:-" + track.ModuleCode + " has been Rejected";
                    }

                    body = String.Format(body, createdBy.FirstName, createdBy.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName, itemsText);
                    SendEmail(createdBy.Email, subject, body, track.SessionID, null, ListAttachment).ConfigureAwait(false);

                    bodySMS = String.Format(bodySMS, createdBy.FirstName, createdBy.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName, itemsText);
                    bodySMS = bodySMS.Replace("<br/>", "");
                    //SendSMS(string.Empty, createdBy.PhoneNum + "," + createdBy.AltPhoneNum, System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), track.SessionID).ConfigureAwait(false);

                }

                Workflow workflow = GetItemWorkflows(track.WorkflowDetails.WorkflowID, track.ModuleID, track.WorkflowDetails.WorkflowModule, track.SessionID, track.CreatedBy).FirstOrDefault();
                if (workflow != null && workflow.Tracks != null && workflow.Tracks.Length > 0)
                {
                    string subject = string.Empty;

                    WorkflowTrack nextTrack = null;
                    if (track.Status.ToUpper() == "APPROVED")
                    {
                        nextTrack = workflow.Tracks.Where(t => t.Order == track.Order + 1).FirstOrDefault();
                    }
                    else if (track.Status.ToUpper() == "REJECTED")
                    {
                        nextTrack = workflow.Tracks.Where(t => t.Order == track.Order - 1).FirstOrDefault();
                    }
                    if (nextTrack != null)
                    {
                        List<UserInfo> nextUsersToNotify = GetUsersForWFEmails(nextTrack.ApproverID, track.SessionID, nextTrack.Approver.DeptID, nextTrack.Approver.DesigID, "TRACK");
                        foreach (UserInfo user in nextUsersToNotify)
                        {
                            if (track.Status.ToUpper() == "APPROVED")
                            {
                                body = GenerateEmailBody("PRWFApprovedForwardEmail");
                                bodySMS = GenerateEmailBody("PRWFApprovedForwardSMS");
                                subject = "PR No:-" + track.ModuleCode + "has been approved";
                            }
                            else if (track.Status.ToUpper() == "REJECTED")
                            {
                                body = GenerateEmailBody("PRWFRejectedReverseEmail");
                                bodySMS = GenerateEmailBody("PRWFRejectedReverseSMS");
                                subject = "PR No:-" + track.ModuleCode + "has been Rejected";
                            }

                            body = String.Format(body, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName, itemsText);
                            SendEmail(user.Email, subject, body, track.SessionID, null, ListAttachment).ConfigureAwait(false);

                            bodySMS = String.Format(bodySMS, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName, itemsText);
                            bodySMS = bodySMS.Replace("<br/>", "");
                            //SendSMS(string.Empty, user.PhoneNum + "," + user.AltPhoneNum, System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), track.SessionID).ConfigureAwait(false);
                        }
                    }
                }
                

            }


            else if (track.ModuleName == "VENDOR" || track.ModuleName == "UNBLOCK_VENDOR")
            {

                string body = string.Empty;
                string bodySMS = string.Empty;

                UserDetails vendorInfo =  prm.GetUserDetails(track.ModuleID, track.SessionID);



                int i = 1;
                
                foreach (UserInfo user in usersToEmail)
                {
                    string subject = string.Empty;

                    if (track.Status.ToUpper() == "APPROVED")
                    {
                        body = GenerateEmailBody("VENDORWFApprovedSelfEmail");
                        bodySMS = GenerateEmailBody("VENDORWFApprovedSelfSMS");
                        subject = "VENDOR NAME:-" + vendorInfo.FirstName + ' '+ vendorInfo.LastName  + " has been approved";
                    }
                    else if (track.Status.ToUpper() == "REJECTED")
                    {
                        body = GenerateEmailBody("VENDORWFRejectedSelfEmail");
                        bodySMS = GenerateEmailBody("VENDORWFRejectedSelfSMS");
                        subject = "VENDOR NAME:-" + vendorInfo.FirstName + ' ' + vendorInfo.LastName + " has been Rejected";
                    }
                    
                    if (track.Status.ToUpper() == "APPROVED")
                    {
                        body = GenerateEmailBody("VENDORWFApprovedEndUserEmail");
                        bodySMS = GenerateEmailBody("VENDORWFApprovedEndUserSMS");
                        subject = "VENDOR NAME:-" + vendorInfo.FirstName + ' ' + vendorInfo.LastName + " has been approved";
                    }
                    else if (track.Status.ToUpper() == "REJECTED")
                    {
                        body = GenerateEmailBody("VENDORWFRejectedEndUserEmail");
                        bodySMS = GenerateEmailBody("VENDORWFRejectedEndUserSMS");
                        subject = "VENDOR NAME:-" + vendorInfo.FirstName + ' ' + vendorInfo.LastName + " has been Rejected";
                    }

                    body = String.Format(body, createdBy.FirstName, createdBy.LastName, vendorInfo.FirstName + ' ' + vendorInfo.LastName, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                    //SendEmail(createdBy.Email, subject, body, track.SessionID, null, ListAttachment).ConfigureAwait(false);
                    prm.SendEmail(createdBy.Email, subject, body, 0, 0, "VENDORTRACK", track.SessionID).ConfigureAwait(false);
                    bodySMS = String.Format(bodySMS, createdBy.FirstName, createdBy.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                    bodySMS = bodySMS.Replace("<br/>", "");
                    //SendSMS(string.Empty, createdBy.PhoneNum + "," + createdBy.AltPhoneNum, System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), track.SessionID).ConfigureAwait(false);

                }

                Workflow workflow = GetItemWorkflows(track.WorkflowDetails.WorkflowID, track.ModuleID, track.WorkflowDetails.WorkflowModule, track.SessionID, track.CreatedBy).FirstOrDefault();
                if (workflow != null && workflow.Tracks != null && workflow.Tracks.Length > 0)
                {
                    string subject = string.Empty;

                    WorkflowTrack nextTrack = null;
                    if (track.Status.ToUpper() == "APPROVED")
                    {
                        nextTrack = workflow.Tracks.Where(t => t.Order == track.Order + 1).FirstOrDefault();
                    }
                    else if (track.Status.ToUpper() == "REJECTED")
                    {
                        nextTrack = workflow.Tracks.Where(t => t.Order == track.Order - 1).FirstOrDefault();
                    }
                    if (nextTrack != null)
                    {
                        List<UserInfo> nextUsersToNotify = GetUsersForWFEmails(nextTrack.ApproverID, track.SessionID, nextTrack.Approver.DeptID, nextTrack.Approver.DesigID, "TRACK");
                        foreach (UserInfo user in nextUsersToNotify)
                        {
                            if (track.Status.ToUpper() == "APPROVED")
                            {
                                body = GenerateEmailBody("VENDORWFApprovedForwardEmail");
                                bodySMS = GenerateEmailBody("VENDORWFApprovedForwardSMS");
                                subject = "VENDOR NAME:-" + vendorInfo.FirstName + ' ' + vendorInfo.LastName + " has been approved";
                            }
                            else if (track.Status.ToUpper() == "REJECTED")
                            {
                                body = GenerateEmailBody("VENDORWFRejectedReverseEmail");
                                bodySMS = GenerateEmailBody("VENDORWFRejectedReverseSMS");
                                subject = "VENDOR NAME:-" + vendorInfo.FirstName + ' ' + vendorInfo.LastName + " has been Rejected";
                            }

                            body = String.Format(body, user.FirstName, user.LastName, vendorInfo.FirstName + ' ' + vendorInfo.LastName, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                            //SendEmail(user.Email, subject, body, track.SessionID, null, ListAttachment).ConfigureAwait(false);
                            prm.SendEmail(user.Email, subject, body, 0, 0, "VENDORTRACK", track.SessionID).ConfigureAwait(false);

                            bodySMS = String.Format(bodySMS, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                            bodySMS = bodySMS.Replace("<br/>", "");
                            //SendSMS(string.Empty, user.PhoneNum + "," + user.AltPhoneNum, System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), track.SessionID).ConfigureAwait(false);
                        }
                    }
                }


            }




            else if (track.ModuleName == "UNBLOCK_VENDOR")
            {

                //string body = string.Empty;
                //string bodySMS = string.Empty;

                //UserDetails vendorInfo = prm.GetUserDetails(track.ModuleID, track.SessionID);



                //int i = 1;

                //foreach (UserInfo user in usersToEmail)
                //{
                //    string subject = string.Empty;

                //    if (track.Status.ToUpper() == "APPROVED")
                //    {
                //        body = GenerateEmailBody("VENDORWFApprovedSelfEmail");
                //        bodySMS = GenerateEmailBody("VENDORWFApprovedSelfSMS");
                //        subject = "VENDOR NAME:-" + vendorInfo.FirstName + ' ' + vendorInfo.LastName + " has been approved";
                //    }
                //    else if (track.Status.ToUpper() == "REJECTED")
                //    {
                //        body = GenerateEmailBody("VENDORWFRejectedSelfEmail");
                //        bodySMS = GenerateEmailBody("VENDORWFRejectedSelfSMS");
                //        subject = "VENDOR NAME:-" + vendorInfo.FirstName + ' ' + vendorInfo.LastName + " has been Rejected";
                //    }

                //    if (track.Status.ToUpper() == "APPROVED")
                //    {
                //        body = GenerateEmailBody("VENDORWFApprovedEndUserEmail");
                //        bodySMS = GenerateEmailBody("VENDORWFApprovedEndUserSMS");
                //        subject = "VENDOR NAME:-" + vendorInfo.FirstName + ' ' + vendorInfo.LastName + " has been approved";
                //    }
                //    else if (track.Status.ToUpper() == "REJECTED")
                //    {
                //        body = GenerateEmailBody("VENDORWFRejectedEndUserEmail");
                //        bodySMS = GenerateEmailBody("VENDORWFRejectedEndUserSMS");
                //        subject = "VENDOR NAME:-" + vendorInfo.FirstName + ' ' + vendorInfo.LastName + " has been Rejected";
                //    }

                //    body = String.Format(body, createdBy.FirstName, createdBy.LastName, vendorInfo.FirstName + ' ' + vendorInfo.LastName, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                //    SendEmail(createdBy.Email, subject, body, track.SessionID, null, ListAttachment).ConfigureAwait(false);

                //    bodySMS = String.Format(bodySMS, createdBy.FirstName, createdBy.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                //    bodySMS = bodySMS.Replace("<br/>", "");
                //    //SendSMS(string.Empty, createdBy.PhoneNum + "," + createdBy.AltPhoneNum, System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), track.SessionID).ConfigureAwait(false);

                //}

                //Workflow workflow = GetItemWorkflows(track.WorkflowDetails.WorkflowID, track.ModuleID, track.WorkflowDetails.WorkflowModule, track.SessionID).FirstOrDefault();
                //if (workflow != null && workflow.Tracks != null && workflow.Tracks.Length > 0)
                //{
                //    string subject = string.Empty;

                //    WorkflowTrack nextTrack = null;
                //    if (track.Status.ToUpper() == "APPROVED")
                //    {
                //        nextTrack = workflow.Tracks.Where(t => t.Order == track.Order + 1).FirstOrDefault();
                //    }
                //    else if (track.Status.ToUpper() == "REJECTED")
                //    {
                //        nextTrack = workflow.Tracks.Where(t => t.Order == track.Order - 1).FirstOrDefault();
                //    }
                //    if (nextTrack != null)
                //    {
                //        List<UserInfo> nextUsersToNotify = GetUsersForWFEmails(nextTrack.ApproverID, track.SessionID, nextTrack.Approver.DeptID, nextTrack.Approver.DesigID,"TRACK");
                //        foreach (UserInfo user in nextUsersToNotify)
                //        {
                //            if (track.Status.ToUpper() == "APPROVED")
                //            {
                //                body = GenerateEmailBody("VENDORWFApprovedForwardEmail");
                //                bodySMS = GenerateEmailBody("VENDORWFApprovedForwardSMS");
                //                subject = "VENDOR NAME:-" + vendorInfo.FirstName + ' ' + vendorInfo.LastName + " has been approved";
                //            }
                //            else if (track.Status.ToUpper() == "REJECTED")
                //            {
                //                body = GenerateEmailBody("VENDORWFRejectedReverseEmail");
                //                bodySMS = GenerateEmailBody("VENDORWFRejectedReverseSMS");
                //                subject = "VENDOR NAME:-" + vendorInfo.FirstName + ' ' + vendorInfo.LastName + " has been Rejected";
                //            }

                //            body = String.Format(body, user.FirstName, user.LastName, vendorInfo.FirstName + ' ' + vendorInfo.LastName, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                //            SendEmail(user.Email, subject, body, track.SessionID, null, ListAttachment).ConfigureAwait(false);

                //            bodySMS = String.Format(bodySMS, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                //            bodySMS = bodySMS.Replace("<br/>", "");
                //            //SendSMS(string.Empty, user.PhoneNum + "," + user.AltPhoneNum, System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), track.SessionID).ConfigureAwait(false);
                //        }
                //    }
                //}


            }


            else if (track.ModuleName == "VENDOR_INVOICE")
            {
                PRMPOService poservice = new PRMPOService();
                string emailContent = string.Empty;
                string bodySMS = string.Empty;
                string bodyTemplate = string.Empty;
                string subject = string.Empty;
                string body = string.Empty;
                POInvoice poInvoice = poservice.GetInvoiceDetails(track.ModuleID, track.SessionID);
                foreach (UserInfo user in usersToEmail)
                {
                    if (track.Status.ToUpper() == "APPROVED")
                    {
                        body = GenerateEmailBody("VendorInvoiceApprovedSelfEmail");
                        subject = "Vendor Invoice has been approved for Po Number: " + poInvoice.PO_NUMBER;
                    }
                    else
                    {
                        body = GenerateEmailBody("VendorInvoiceRejectedSelfEmail");
                        subject = "Vendor Invoice has been Rejected for Po Number: " + poInvoice.PO_NUMBER;
                    }

                    if (track.Status.ToUpper() == "APPROVED")
                    {
                        body = GenerateEmailBody("VendorInvoiceApprovedEndUserEmail");
                        subject = "Vendor Invoice has been approved for Po Number: " + poInvoice.PO_NUMBER;
                    }
                    else
                    {
                        body = GenerateEmailBody("VendorInvoiceRejectedEndUserEmail");
                        subject = "Vendor Invoice has been Rejected for Po Number: " + poInvoice.PO_NUMBER;
                    }

                    body = String.Format(body, createdBy.FirstName, createdBy.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName, poInvoice.VENDOR_COMP_NAME);
                    prm.SendEmail(createdBy.Email, subject, body, 0, 0, "WORKFLOWTRACK", track.SessionID).ConfigureAwait(false);
                }

                Workflow workflow = GetItemWorkflows(track.WorkflowDetails.WorkflowID, track.ModuleID, track.WorkflowDetails.WorkflowModule, track.SessionID, track.CreatedBy).FirstOrDefault();
                if (workflow != null && workflow.Tracks != null && workflow.Tracks.Length > 0)
                {
                    WorkflowTrack nextTrack = null;
                    if (track.Status.ToUpper() == "APPROVED")
                    {
                        nextTrack = workflow.Tracks.Where(t => t.Order == track.Order + 1).FirstOrDefault();
                    }
                    else if (track.Status.ToUpper() == "REJECTED")
                    {
                        nextTrack = workflow.Tracks.Where(t => t.Order == track.Order - 1).FirstOrDefault();
                    }

                    if (nextTrack != null)
                    {
                        List<UserInfo> nextUsersToNotify = GetUsersForWFEmails(nextTrack.ApproverID, track.SessionID, nextTrack.Approver.DeptID, nextTrack.Approver.DesigID, "TRACK");
                        foreach (UserInfo user in nextUsersToNotify)
                        {
                            if (track.Status.ToUpper() == "APPROVED")
                            {
                                body = GenerateEmailBody("VendorInvoiceApprovedForwardEmail");
                                subject = "Vendor Invoice has been approved for Po Number: " + poInvoice.PO_NUMBER;
                            }
                            else if (track.Status.ToUpper() == "REJECTED")
                            {
                                body = GenerateEmailBody("VendorInvoiceRejectedReverseEmail");
                                subject = "Vendor Invoice has been Rejected for Po Number: " + poInvoice.PO_NUMBER;
                            }

                            body = String.Format(body, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName, poInvoice.VENDOR_COMP_NAME);
                            prm.SendEmail(user.Email, subject, body, 0, 0, "WORKFLOWTRACK", track.SessionID).ConfigureAwait(false);
                        }
                    }
                }
            }


            return response;
        }

        public Response SaveWorkflowTrackEmail(WorkflowTrack track)
        {
            Response response = new Response();
            try
            {
                DataSet ds = WFUtility.SaveWorkflowTrackEmailObject(track);
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response AssignWorkflow(int wID, int moduleID, int user, string sessionID, string SubModuleName = null, int SubModuleID = 0)
        {
            Response response = new Response();
            if (Utilities.ValidateSession(sessionID) <= 0)
            {
                throw new Exception("Invalid Session. Please Re-Login.");
            }

            try
            {
                DataSet ds = WFUtility.AssignWorkflow(wID, moduleID, user);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    WorkflowTrack track = new WorkflowTrack();
                    track.Approver = new CompanyDesignations();
                    track.ApproverID = row["WF_APPROVER"] != DBNull.Value ? Convert.ToInt32(row["WF_APPROVER"]) : 0;
                    track.Approver.DeptID = row["WF_APPROVER_DEPT_ID"] != DBNull.Value ? Convert.ToInt32(row["WF_APPROVER_DEPT_ID"]) : 0;
                    track.Approver.DesigID = row["WF_APPROVER_DESIG_ID"] != DBNull.Value ? Convert.ToInt32(row["WF_APPROVER_DESIG_ID"]) : 0;
                    track.CreatedBy = user;
                    track.ModifiedBY = user;
                    track.ModuleName = row["WF_MODULE"] != DBNull.Value ? Convert.ToString(row["WF_MODULE"]) : string.Empty;
                    track.ModuleCode = row["MODULE_CODE"] != DBNull.Value ? Convert.ToString(row["MODULE_CODE"]) : string.Empty;
                    track.Status = string.Empty;
                    track.WorkflowDetails = new Workflow();
                    track.WorkflowDetails.WorkflowID = wID;
                    track.ModuleID = moduleID;
                    track.WorkflowDetails.WorkflowModule = track.ModuleName;
                    track.SubModuleName = SubModuleName;
                    track.SubModuleID = SubModuleID;
                    AssignWorkflowAlerts(track, sessionID);
                }

                response.ObjectID = 1;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        private Response AssignWorkflowAlerts(WorkflowTrack track, string sessionID)
        {
            Response response = new Response();
            List<UserInfo> usersToEmail = GetUsersForWFEmails(track.ApproverID, sessionID, track.Approver.DeptID, track.Approver.DesigID, "CREATE");
            DateTime dateTime = DateTime.Now;
            List<Attachment> ListAttachment = new List<Attachment>();
            Attachment emailAttachment = null;
            UserInfo createdBy = prm.GetUser(track.CreatedBy, sessionID);
            
            if (track.ModuleName == "QUOTATION")
            {
                string body = string.Empty;
                string bodySMS = string.Empty;
                Workflow workflow = GetItemWorkflows(track.WorkflowDetails.WorkflowID, track.ModuleID, track.WorkflowDetails.WorkflowModule, sessionID, track.CreatedBy).FirstOrDefault();
                if (workflow != null && workflow.Tracks != null && workflow.Tracks.Length > 0)
                {
                    string subject = string.Empty;
                    WorkflowTrack nextTrack = null;
                    nextTrack = workflow.Tracks.Where(t => t.Order == 0 + 1).FirstOrDefault();
                    if (nextTrack != null)
                    {
                        List<UserInfo> nextUsersToNotify = GetUsersForWFEmails(nextTrack.ApproverID, sessionID, nextTrack.Approver.DeptID, nextTrack.Approver.DesigID, "CREATE");
                        foreach (UserInfo user in nextUsersToNotify)
                        {
                            body = GenerateEmailBody("QUOTATIONWFCreatedForwardEmail");
                            bodySMS = GenerateEmailBody("QUOTATIONWFCreatedForwardSMS");
                            subject = "Quotation has been created";

                            body = String.Format(body, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, createdBy.FirstName, createdBy.LastName);
                            SendEmail(user.Email, subject, body, sessionID, null, ListAttachment).ConfigureAwait(false);
                            bodySMS = String.Format(bodySMS, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, createdBy.FirstName, createdBy.LastName);
                            bodySMS = bodySMS.Replace("<br/>", "");
                        }
                    }
                }
            }

            else if (track.ModuleName == "QCS")
            {
                string body = string.Empty;
                string bodySMS = string.Empty;
                Workflow workflow = GetItemWorkflows(track.WorkflowDetails.WorkflowID, track.ModuleID, track.WorkflowDetails.WorkflowModule, sessionID, track.CreatedBy).FirstOrDefault();
                if (workflow != null && workflow.Tracks != null && workflow.Tracks.Length > 0)
                {
                    string subject = string.Empty;
                    WorkflowTrack nextTrack = null;
                    nextTrack = workflow.Tracks.Where(t => t.Order == 0 + 1).FirstOrDefault();
                    var requirement = prm.GetRequirementDataLite(track.SubModuleID, sessionID);
                    if (nextTrack != null)
                    {
                        List<UserInfo> nextUsersToNotify = GetUsersForWFEmails(nextTrack.ApproverID, sessionID, nextTrack.Approver.DeptID, nextTrack.Approver.DesigID, "CREATE");
                        foreach (UserInfo user in nextUsersToNotify)
                        {
                            body = GenerateEmailBody("QCSCreatedEmail");
                            bodySMS = GenerateEmailBody("QCSCreatedSMS");
                            subject = "QCS has been created for REQ-Number: "+ requirement .REQ_NUMBER+ " and REQ-TITLE: "+track.SubModuleName;

                            body = String.Format(body, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, createdBy.FirstName, createdBy.LastName);
                            prm.SendEmail(user.Email, subject, body, 0, 0, "WORKFLOWCREATE", sessionID).ConfigureAwait(false);
                            //prm.SendEmail(user.Email, subject, body, sessionID, null, ListAttachment).ConfigureAwait(false);
                            //prmservices.SendEmail(vendor.Email + "," + vendor.AltEmail,"REQ-ID: " + reqID + " - " + subject,bodyEMAIL, reqVendor.RequirementID, v.VendorID,sessionID).ConfigureAwait(false);
                            bodySMS = String.Format(bodySMS, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, createdBy.FirstName, createdBy.LastName);
                            bodySMS = bodySMS.Replace("<br/>", "");
                        }
                    }
                }
            }

            else if (track.ModuleName == "PR")
            {
                string body = string.Empty;
                string bodySMS = string.Empty;
                //PRDetails pr = 
                PRMPRService prmPr = new PRMPRService();
                PRDetails prDet = prmPr.GetPRDetails(track.ModuleID, sessionID);
                //MPIndent indentItem = GetIndentDetails(track.ModuleID, sessionID);
                //if (indentItem.PdfID > 0)
                //{
                //    var fileData = Utilities.DownloadFile(Convert.ToString(indentItem.PdfID), sessionID);
                //    if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                //    {
                //        emailAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                //        ListAttachment.Add(emailAttachment);
                //    }
                //}
                if (prDet.ATTACHMENTS != null && prDet.ATTACHMENTS != "")
                {
                    var fileData = Utilities.DownloadFile(prDet.ATTACHMENTS, sessionID);
                    if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                    {
                        emailAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                        ListAttachment.Add(emailAttachment);
                    }
                }

                foreach (UserInfo user in usersToEmail)
                {
                    string subject = string.Empty;
                    body = GenerateEmailBody("PRWFCreatedEndUserEmail");
                    bodySMS = GenerateEmailBody("PRWFCreatedEndUserSMS");
                    subject = "PR Number:-  " + track.ModuleCode + " has been created";
                    body = String.Format(body, createdBy.FirstName, createdBy.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime);
                    SendEmail(createdBy.Email, subject, body, sessionID, null, ListAttachment).ConfigureAwait(false);
                    bodySMS = String.Format(bodySMS, createdBy.FirstName, createdBy.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime);
                    bodySMS = bodySMS.Replace("<br/>", "");
                }

                Workflow workflow = GetItemWorkflows(track.WorkflowDetails.WorkflowID, track.ModuleID, track.WorkflowDetails.WorkflowModule, sessionID, track.CreatedBy).FirstOrDefault();
                if (workflow != null && workflow.Tracks != null && workflow.Tracks.Length > 0)
                {
                    string subject = string.Empty;
                    WorkflowTrack nextTrack = null;
                    nextTrack = workflow.Tracks.Where(t => t.Order == 0 + 1).FirstOrDefault();
                    if (nextTrack != null)
                    {
                        List<UserInfo> nextUsersToNotify = GetUsersForWFEmails(nextTrack.ApproverID, sessionID, nextTrack.Approver.DeptID, nextTrack.Approver.DesigID, "CREATE");
                        foreach (UserInfo user in nextUsersToNotify)
                        {
                            body = GenerateEmailBody("PRWFCreatedForwardEmail");
                            bodySMS = GenerateEmailBody("PRWFCreatedForwardSMS");
                            subject = "PR Number:-  " + track.ModuleCode + " has been created";
                            string emailSessionID = Guid.NewGuid().ToString();
                            Utilities.CreateSession(emailSessionID, Convert.ToInt32(user.UserID), "WEB");
                            prDet = prmPr.GetPRDetails(track.ModuleID, emailSessionID);
                            int i = 1;
                            string itemsText = "<table class=\" table table-vmiddle table-responsive\" style=\"background-color:#f5f5f5;border-collapse: collapse;\" border=\"1\">" +
                                    "<tr style=\"width:100%;\" class=\" dl-horizontal\">" +
                                    "<th style=\"width:5%;text-align:center\"> Sl.No </th>" +
                                    "<th style=\"width:10%;text-align:center\"> ITEM NAME </th>" +
                                    "<th style=\"width:15%;text-align:center\"> DESCRIPTION OF MATERIAL </th>" +
                                    "<th style=\"width:8%;text-align:center\"> MAKE </th>" +
                                    "<th style=\"width:8%;text-align:center\"> UOM </th>" +
                                    "<th style=\"width:8%;text-align:center\"> STOCK IN STORES / PIPELINE </th>" +
                                    "<th style=\"width:8%;text-align:center\"> UNIT PRICE </th>" +
                                    "<th style=\"width:8%;text-align:center\"> TAXES(%) </th>" +
                                    "<th style=\"width:10%;text-align:center\"> TOTAL QTY TO BE PROCURED </th>" +
                                    "<th style=\"width:10%;text-align:center\"> TOTAL ESTIMATED COST </th>" +
                                    "</tr>";
                            foreach (PRItems prItem in prDet.PRItemsList)
                            {
                                if (prItem.REQUIRED_QUANTITY > 0)
                                {
                                    string text = "<tr style=\"text-align:center\">" +
                                                    "<td>" + i + "</td>" +
                                                    "<td>" + prItem.ITEM_NAME + "</td>" +
                                                    "<td>" + prItem.ITEM_DESCRIPTION + "</td>" +
                                                    "<td>" + prItem.BRAND + "</td>" +
                                                    "<td>" + prItem.UNITS + "</td>" +
                                                    "<td>" + prItem.EXIST_QUANTITY + "</td>" +
                                                    "<td>" + prItem.UNIT_PRICE + "</td>" +
                                                    "<td>" + prItem.C_GST_PERCENTAGE + "</td>" +
                                                    "<td>" + prItem.REQUIRED_QUANTITY + "</td>" +
                                                    "<td>" + prItem.TOTAL_PRICE + "</td>" +
                                                   //"<td>" + prItem.TOTAL_PRICE + "</td>" +
                                                   "</tr>";
                                    itemsText += text;
                                    i++;
                                }
                            }

                            itemsText += "</table>";
                            string approveLink = Utilities.SITE_LINK + "#/workflow-approval/" + track.ApproverID + "/" + track.ModuleID + "/APPROVED/" + emailSessionID + "/" + user.UserID + "/" + track.WorkflowDetails.WorkflowID + "/" + track.WorkflowDetails.WorkflowModule;
                            string rejectLink = Utilities.SITE_LINK + "#/workflow-approval/" + track.ApproverID + "/" + track.ModuleID + "/REJECTED/" + emailSessionID + "/" + user.UserID + "/" + track.WorkflowDetails.WorkflowID + "/" + track.WorkflowDetails.WorkflowModule;
                            body = String.Format(body, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, itemsText);//, approveLink, rejectLink
                            SendEmail(user.Email, subject, body, sessionID, null, ListAttachment).ConfigureAwait(false);
                            bodySMS = String.Format(bodySMS, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, createdBy.FirstName, createdBy.LastName);
                            bodySMS = bodySMS.Replace("<br/>", "");
                        }
                    }
                }
            }

            else if (track.ModuleName == "VENDOR")
            {
                
            }

            else if (track.ModuleName == "VENDOR_INVOICE")
            {
                PRMPOService poservice = new PRMPOService();
                string body = string.Empty;
                string bodySMS = string.Empty;
                Workflow workflow = GetItemWorkflows(track.WorkflowDetails.WorkflowID, track.ModuleID, track.WorkflowDetails.WorkflowModule, sessionID, track.CreatedBy).FirstOrDefault();
                if (workflow != null && workflow.Tracks != null && workflow.Tracks.Length > 0)
                {
                    string subject = string.Empty;
                    WorkflowTrack nextTrack = null;
                    nextTrack = workflow.Tracks.Where(t => t.Order == 0 + 1).FirstOrDefault();
                    POInvoice poInvoice = poservice.GetInvoiceDetails(track.ModuleID,sessionID);
                    if (nextTrack != null)
                    {
                        List<UserInfo> nextUsersToNotify = GetUsersForWFEmails(nextTrack.ApproverID, sessionID, nextTrack.Approver.DeptID, nextTrack.Approver.DesigID, "CREATE");
                        foreach (UserInfo user in nextUsersToNotify)
                        {
                            body = GenerateEmailBody("VendorInvoiceCreatedEmail");
                            subject = "Invoice has been created by Vendor Company Name: " + poInvoice.VENDOR_COMP_NAME + " for Po Number : " + poInvoice.PO_NUMBER;
                            body = String.Format(body, user.FirstName, user.LastName, track.ModuleCode, createdBy.FirstName, createdBy.LastName, dateTime, createdBy.FirstName, createdBy.LastName, poInvoice.VENDOR_COMP_NAME);
                            prm.SendEmail(user.Email, subject, body, 0, 0, "WORKFLOWCREATE", sessionID).ConfigureAwait(false);
                        }
                    }
                }
            }
            return response;
        }

        public Response DeleteWorkflow(int wID, string sessionID)
        {
            Response response = new Response();
            Utilities.ValidateSession(sessionID);

            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_WF_ID", wID);
                DataSet ds = sqlHelper.SelectList("wf_DeleteWorkflow", sd);
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response DeleteWorkflowStage(int stageID, string sessionID)
        {
            Response response = new Response();
            Utilities.ValidateSession(sessionID);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

            try
            {
                sd.Add("P_WF_STAGE_ID", stageID);
                DataSet ds = sqlHelper.SelectList("wf_DeleteWorkflowStage", sd);
                response.ObjectID = 1;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveRFQCreators(List<UserInfo> listUsers, int indentID, string sessionID)
        {
            Response response = new Response();
            try
            {
                foreach (UserInfo UI in listUsers)
                {
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_U_ID", UI.UserID);
                    int IsIndentAssigned = 0;
                    if (UI.IsIndentAssigned)
                    {
                        IsIndentAssigned = 1;
                    }

                    sd.Add("P_IS_ASSIGNED", IsIndentAssigned);
                    sd.Add("P_INDENT_ID", indentID);
                    DataSet ds1 = sqlHelper.SelectList("wf_SaveRFQCreators", sd);
                    if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0 && ds1.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = ds1.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString()) : -1;
                        response.ErrorMessage = ds1.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds1.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveUserStatus(string module, int id, string status, int userId, string sessionID)
        {
            Response response = new Response();
            try
            {
                string query = string.Empty;
                if (module.ToUpper() == "CIJ")
                {
                    query = string.Format("update cij set USER_STATUS = '{0}' where CIJ_ID={1}", status, id);
                }
                else if (module.ToUpper() == "INDENT")
                {
                    query = string.Format("update indent set USER_STATUS = '{0}' where I_ID={1}", status, id);
                }

                if (!string.IsNullOrEmpty(query))
                {
                    DataSet ds = sqlHelper.ExecuteQuery(query);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        response.Message = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][0].ToString()) : string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            return response;
        }

        #endregion

        #region Private

        public MPIndent GetIndentDetails(int indentID, string sessionID)
        {
            MPIndent mpindent = new MPIndent();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_I_ID", indentID);
                DataSet ds = sqlHelper.SelectList("cp_GetIndentDetails", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    mpindent.IndID = row["I_ID"] != DBNull.Value ? Convert.ToInt32(row["I_ID"]) : 0;
                    mpindent.CIJ = new stringCIJ();
                    mpindent.CIJ.CijID = row["CIJ_ID"] != DBNull.Value ? Convert.ToInt32(row["CIJ_ID"]) : 0;
                    mpindent.CIJ.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                    mpindent.IndNo = row["INDENT_CODE"] != DBNull.Value ? Convert.ToString(row["INDENT_CODE"]) : string.Empty;
                    mpindent.Department = row["DEPT"] != DBNull.Value ? Convert.ToString(row["DEPT"]) : string.Empty;
                    mpindent.RequestDate = row["CREATED_DATE"] != DBNull.Value ? Convert.ToString(row["CREATED_DATE"]) : string.Empty;
                    mpindent.RequestedBy = row["REQUESTED_BY"] != DBNull.Value ? Convert.ToString(row["REQUESTED_BY"]) : string.Empty;
                    mpindent.ApprovedBy = row["APPROVED_BY"] != DBNull.Value ? Convert.ToString(row["APPROVED_BY"]) : string.Empty;
                    mpindent.PurchaseIncharge = row["PURCHASE_INCHARGE"] != DBNull.Value ? Convert.ToString(row["PURCHASE_INCHARGE"]) : string.Empty;
                    mpindent.ApproximateCostRange = row["APPROX_COST_RANGE"] != DBNull.Value ? Convert.ToDecimal(row["APPROX_COST_RANGE"]) : 0;
                    mpindent.IsMedical = row["IS_MEDICAL"] != DBNull.Value ? Convert.ToInt32(row["IS_MEDICAL"]) : 0;
                    List<IndentItems> listIndentItems = new List<IndentItems>();
                    foreach (DataRow row1 in ds.Tables[1].Rows)
                    {
                        IndentItems indentitems = new IndentItems();
                        indentitems.IndentItemID = row1["II_ID"] != DBNull.Value ? Convert.ToInt32(row1["II_ID"]) : 0;
                        indentitems.MaterialDescription = row1["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row1["DESCRIPTION"]) : string.Empty;
                        indentitems.ExistFunctional = row1["FUNCTIONAL_QTY"] != DBNull.Value ? Convert.ToInt32(row1["FUNCTIONAL_QTY"]) : 0;
                        indentitems.ExistNonFunctional = row1["NON_FUNCTIONAL_QTY"] != DBNull.Value ? Convert.ToInt32(row1["NON_FUNCTIONAL_QTY"]) : 0;
                        indentitems.RequiredQuantity = row1["REQUIRED_QTY"] != DBNull.Value ? Convert.ToInt32(row1["REQUIRED_QTY"]) : 0;
                        indentitems.PurposeOfMaterial = row1["PURPOSE"] != DBNull.Value ? Convert.ToString(row1["PURPOSE"]) : string.Empty;
                        listIndentItems.Add(indentitems);
                    }

                    mpindent.ListIndentItems = listIndentItems;
                }
            }
            catch (Exception ex)
            {
                mpindent.ErrorMessage = ex.Message;
            }

            return mpindent;
        }

        public string GenerateEmailBody(string TemplateName)
        {
            string body = string.Empty;
            XmlDocument doc = new XmlDocument();
            doc.Load(templateFolderPath + "/EmailTemplates/EmailFormats.xml");
            XmlNode node = doc.DocumentElement.SelectSingleNode(TemplateName);
            body = node.InnerText;
            string footerName = "";
            if (TemplateName.ToLower().Contains("email"))
            {
                footerName = "EmailFooter";
            }
            else if (TemplateName.ToLower().Contains("sms"))
            {
                footerName = "SMSFooter";
            }
            else
            {
                footerName = "FooterXML";
            }

            if (footerName == "FooterXML")
            {
            }
            else
            {
                XmlNode footernode = doc.DocumentElement.SelectSingleNode(footerName);
                body += footernode.InnerText;
            }

            return body;
        }

        public Communication GetCommunicationData(string communicationType, string sessionID = null)
        {
            Communication communication = new Communication();
            communication.User = new User();
            communication.Company = new Company();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                Utilities.ValidateSession(sessionID);
                sd.Add("P_SESSION", sessionID);
                sd.Add("P_C_TYPE", communicationType); // (SMS or EMAIL or TELEGRAM)
                DataSet ds = sqlHelper.SelectList("cp_GetCommunicationData", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    communication.User.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    communication.User.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    communication.User.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    communication.User.Email = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    communication.User.PhoneNum = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                    communication.Company.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    communication.Company.CompanyID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                    communication.CompanyAddress = row["ADDRESS"] != DBNull.Value ? Convert.ToString(row["ADDRESS"]) : string.Empty;
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0 && ds.Tables[1].Rows[0][0] != null)
                    {
                        DataRow row1 = ds.Tables[1].Rows[0];
                        communication.FromAdd = row1["CONFIG_VALUE"] != DBNull.Value ? Convert.ToString(row1["CONFIG_VALUE"]) : string.Empty;
                        communication.DisplayName = row1["CONFIG_TEXT"] != DBNull.Value ? Convert.ToString(row1["CONFIG_TEXT"]) : string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                communication.ErrorMessage = ex.Message;
            }

            return communication;
        }

        public async Task SendEmail(string To, string Subject, string Body, string sessionID = null, Attachment attachment = null, List<Attachment> ListAttachment = null)
        {
            try
            {
                Communication communication = new Communication();
                communication = Utilities.GetCommunicationData("EMAIL_FROM", sessionID);
                Body = Body.Replace("COMPANY_LOGO_ID", communication.Company.CompanyID > 0 ? communication.Company.CompanyID.ToString() : "0");
                Body = Body.Replace("COMPANY_NAME", !string.IsNullOrEmpty(communication.Company.CompanyName) ? communication.Company.CompanyName : "PRM360");
                Body = Body.Replace("USER_NAME", !string.IsNullOrEmpty(communication.User.FirstName + " " + communication.User.LastName) ? communication.User.FirstName + " " + communication.User.LastName : "");
                Body = Body.Replace("COMPANY_ADDRESS", !string.IsNullOrEmpty(communication.CompanyAddress) ? communication.CompanyAddress : "");
                SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MAILHOST"].ToString());
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MAILHOST_USER"].ToString(), ConfigurationManager.AppSettings["MAILHOST_PWD"].ToString());
                smtpClient.Credentials = credentials;
                smtpClient.EnableSsl = false;
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(prm.GetFromAddress(""), prm.GetFromDisplayName(""));
                mail.BodyEncoding = System.Text.Encoding.ASCII;
                //if (attachment != null)
                //{
                //    mail.Attachments.Add(attachment);
                //}

                //if (ListAttachment != null)
                //{
                //    foreach (Attachment singleAttachment in ListAttachment)
                //    {
                //        if (singleAttachment != null)
                //        {
                //            mail.Attachments.Add(singleAttachment);
                //        }
                //    }
                //}

                List<string> ToAddresses = To.Split(',').ToList<string>();
                foreach (string address in ToAddresses)
                {
                    if (!string.IsNullOrEmpty(address))
                    {
                        mail.To.Add(new MailAddress(address));
                    }

                }

                mail.Subject = Subject;
                mail.IsBodyHtml = true;
                mail.Body = Body;
                await smtpClient.SendMailAsync(mail);
                Response response = new Response();
                response.ObjectID = 1;
            }
            catch (Exception ex)
            {
                Response response = new Response();
                response.ErrorMessage = ex.Message;
            }
        }

        public async Task<string> SendSMS(string from, string to, string message, string sessionID = null)
        {
            try
            {
                Communication communication = new Communication();
                communication = GetCommunicationData("SMS_FROM", sessionID);
                message = message.Replace("COMPANY_LOGO_ID", communication.Company.CompanyID > 0 ? communication.Company.CompanyID.ToString() : "0");
                message = message.Replace("COMPANY_NAME", !string.IsNullOrEmpty(communication.Company.CompanyName) ? communication.Company.CompanyName : "PRM360");
                message = message.Replace("USER_NAME", !string.IsNullOrEmpty(communication.User.FirstName + " " + communication.User.LastName) ? communication.User.FirstName + " " + communication.User.LastName : "");
                if (string.IsNullOrEmpty(from))
                {
                    from = ConfigurationManager.AppSettings["SMSFROM"].ToString();

                    if (!string.IsNullOrEmpty(communication.FromAdd))
                    {
                        from = communication.FromAdd;
                    }
                }

                message = message.Replace("&", "and").Replace("<br/>", Environment.NewLine);
                string strUrl = string.Format(ConfigurationManager.AppSettings["SMSURL"].ToString(), from, to, message);
                using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                {
                    HttpResponseMessage httpResponse = client.GetAsync(strUrl).Result;
                    httpResponse.EnsureSuccessStatusCode();
                    string result = httpResponse.Content.ReadAsStringAsync().Result;
                }

                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
        #endregion
    }
}  