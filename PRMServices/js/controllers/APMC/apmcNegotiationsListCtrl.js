﻿prmApp
    .controller('apmcNegotiationsListCtrl', ["$scope", "$stateParams", "$state", "$window", "$log", "$filter", "$http", "domain", "userService", "growlService", "apmcService", 
        function ($scope, $stateParams, $state, $window, $log, $filter, $http, domain, userService, growlService, apmcService) {

            $scope.apmc = [];

            var today = moment();
            $scope.dashboardFromDate = today.add('days', -30).format('YYYY-MM-DD');
            today = moment().format('YYYY-MM-DD');
            $scope.dashboardToDate = today;

            $scope.getapmcnegotiationlist = function () {
                apmcService.getapmcnegotiationlist($scope.dashboardFromDate, $scope.dashboardToDate)
                    .then(function (response) {
                        $scope.apmc = response;
                        if ($scope.apmc && $scope.apmc.apmcNegotiationList)
                        {
                            $scope.apmc.apmcNegotiationList.forEach(function (item, index) {
                                item.createdDate = new moment(item.createdDate).format("DD-MM-YYYY HH:mm:ss");
                            });

                        }
                        
                    })
            }

            $scope.goToapmcnegotiationdata = function (apmcnegID) {
                var url = $state.href("apmcnegotiationdata", { "apmcnegID": apmcnegID });
                window.open(url, '_blank');
            }

            $scope.getapmcnegotiationlist();

        }]);