﻿using System.Collections.Generic;

namespace PRM.Services.Common
{
    public interface IExcelService
    {

        IList<T> To<T>(string sheetName,string filePath) where T : class, new();
        IList<T> To<T>(string sheetName, byte[] stream) where T : class, new();

    }
}