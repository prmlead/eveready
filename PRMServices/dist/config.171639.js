//prmApp.constant('domain','http://prm360.com/services/');
prmApp.constant('domain', 'svc/PRMService.svc/REST/');
prmApp.constant('fwddomain', 'svc/PRMFwdService.svc/REST/');
prmApp.constant('storeDomain', 'svc/PRMStoreService.svc/REST/');
prmApp.constant('techevalDomain', 'svc/PRMTechEvalService.svc/REST/');
prmApp.constant('poDomain', 'svc/PRMPOService.svc/REST/');
prmApp.constant('reportingDomain', 'svc/PRMReportService.svc/REST/');
prmApp.constant('workflowDomain', 'svc/PRMWFService.svc/REST/');
prmApp.constant('logisticDomain', 'svc/PRMLogisticsService.svc/REST/');
prmApp.constant('apmcDomain', 'svc/PRMAPMCService.svc/REST/');
prmApp.constant('settingDomian', 'svc/prmsettingsService.svc/REST/');
prmApp.constant('vendorDomian', 'svc/prmvendorService.svc/REST/');
prmApp.constant('reatimepriceDomian', 'svc/prmrealtimepriceservice.svc/REST/');
prmApp.constant('logisticComparsion', 'svc/PRMLogisticQuotationService.svc/REST/');
prmApp.constant('catalogDomain', 'svc/PRMCatalogService.svc/REST/');
prmApp.constant('masterDomain', 'svc/PRMMasterService.svc/REST/');
prmApp.constant('priceCapDomain', 'svc/PRMPriceCapService.svc/REST/');
prmApp.constant('requirementDomain', 'svc/PRMRequirementService.svc/REST/');
prmApp.constant('cijindentDomain', 'svc/PRMCijIndentService.svc/REST/');
prmApp.constant('rfqDomain', 'svc/PRMRFQService.svc/REST/');
prmApp.constant('PRMCompanyDomain', 'svc/PRMCompanyService.svc/REST/');
prmApp.constant('prpoDomain', 'prpo/svc/PRMPRPOReportService.svc/REST/');
prmApp.constant('catalogReportsServicesDomain', 'svc/PRMCatalogReportsService.svc/REST/');
prmApp.constant('SAPIntegrationServicesDomain', 'sapintegration/svc/PRMSAPIntegrationService.svc/REST/');

prmApp.constant('version', 'v1');
prmApp.constant('signalR', '~');
prmApp.constant('signalRHubName', 'requirementHub');
prmApp.constant('apmcHubName', 'apmcHub');
prmApp.constant('logisticsHubName', 'logisticsHub');

prmApp.run(["store", "$rootScope", "userService", "$state", "authService", "auctionsService", function (store, $rootScope, userService, $state, authService, auctionsService) {
        $rootScope.fieldsLoaded = false;
    //$state.go('login');

        $rootScope.$on('$stateChangeStart',function(e, to,toparm, from, fromParam) {
            authService.isValidSession(store.get('sessionid'))
            .then(function (responseObj) {
                if (responseObj && responseObj.objectID > 0)
                {
                    if (to.name != 'resetpassword') {
                        if (to.name === 'login' || to.name === 'logincontainer') {
                            if (store.get('sessionid')) {
                                if (userService.getUserId() != null) {
                                    if (store.get('verified') && store.get('emailverified') && store.get('credverified')) { /*&& store.get('credverified')*/
                                        if (store.get('currentUser').userType === 'VENDOR' && store.get('currentUser').subUserPassword.startsWith("PRM-")) {
                                            $state.go('pages.profile.profile-password-management');
                                            swal("Warning", "Please change Password", "warning");
                                        } else {
                                            $state.go('home');
                                        }
                                    } else {
                                        e.preventDefault();
                                        $state.go('pages.profile.profile-about');
                                        swal("Warning", "Please verify your Credential documents/OTP", "warning");
                                    }
                                }
                            }
                        } else if (to.name === 'pages.profile.profile-about') {
                            if (store.get('sessionid') && userService.getUserId() != null) {
                                if (store.get('verified') && store.get('emailverified') && store.get('credverified')) {
                                    if (store.get('currentUser').userType === 'VENDOR' && store.get('currentUser').subUserPassword.startsWith("PRM-")) {
                                        $state.go('pages.profile.profile-password-management');
                                        swal("Warning", "Please change Password", "warning");
                                    }
                                }
                            } else {
                                e.preventDefault();
                                $state.go('login');
                            }
                        } else {
                            if (store.get('sessionid') && userService.getUserId() != null) {
                                if (store.get('verified') && store.get('emailverified') && store.get('credverified')) {  /*&& store.get('credverified')*/
                                    if (store.get('currentUser').userType === 'VENDOR' && store.get('currentUser').subUserPassword.startsWith("PRM-")) {
                                        $state.go('pages.profile.profile-password-management');
                                        swal("Warning", "Please change Password", "warning");
                                    }
                                } else {
                                    e.preventDefault();
                                    $state.go('pages.profile.profile-about');
                                    swal("Warning", "Please verify your Credential documents/OTP", "warning");
                                }
                            } else {
                                e.preventDefault();
                                $state.go('login');
                            }
                        }
                    }
                }
                else
                {
                    if (to.name === 'login' || to.name === 'logincontainer') {
                        $state.go(to.name);
                    }
                    else {
                        $state.go('login');
                    }                    
                }
            });
        });
        
        $rootScope.entitlements = userService.getLocalEntitlement();
        $rootScope.isUserEntitled = function (moduleId)
        {
            var isEntitled = false;

            if ($rootScope.entitlements && $rootScope.entitlements.length > 0)
            {
                for (i = 0; i <= $rootScope.entitlements.length - 1; i++) {
                    if ($rootScope.entitlements[i].acsID == moduleId && $rootScope.entitlements[i].isEntitled) {
                        isEntitled = true;
                    }
                }
            }

            return isEntitled;
    }
        $rootScope.rootDetails = userService.getLocalDeptDesigt();

    }]);

    prmApp.directive('errSrc', function() {
      return {
        link: function(scope, element, attrs) {
          element.bind('error', function() {
            if (attrs.src != attrs.errSrc) {
              attrs.$set('src', attrs.errSrc);
            }
          });
          attrs.$observe('ngSrc', function(value) {
            if (!value && attrs.errSrc) {
              attrs.$set('src', attrs.errSrc);
            }
          });
        }
      }
    });




prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
        $urlRouterProvider.otherwise('/login');

        $provide.factory('httpInterceptor', function($q,$injector,store,$rootScope) {
            return {
                'request': function(config) {
                  if(store.get('sessionid') && store.get('verified') && store.get('emailverified')){
                    config.headers['X-AUTHORIZATION'] = store.get('sessionid');
                  }
                  if(config.url.indexOf('v/') == 0){
                        config.url = domain+config.url.replace("v/",version);
                  }
                  return config;
                },
                'response': function(response) {

                    if(response.data.error && response.data.message == "Unauthorized"){
                        store.remove('sessionid');
                        store.remove('verified');
                        location.reload();
                    }
                    return response;
                }
            };
        });

        //    $provide.factory('httpRequestInterceptor', function ($q, $injector, store, $rootScope) {
        //    return {
        //        request: function (config) {
        //            if (window.localStorage.getItem('authentication')) {
        //                config.headers['Authorization'] = 'Bearer ' + window.localStorage.getItem('authentication').replace(/"/g, '');
        //            }

        //            return config;
        //        }
        //    };
        //});

      //  $httpProvider.interceptors.push('httpRequestInterceptor');
        $httpProvider.interceptors.push('httpInterceptor');
        $httpProvider.defaults.useXDomain = true;
        //delete $httpProvider.defaults.headers.common['X-Requested-With'];

        //added by Nikhil
        //$httpProvider.defaults.headers.common = {};
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
        $httpProvider.defaults.headers.post = {};
        $httpProvider.defaults.headers.put = {};
        $httpProvider.defaults.headers.patch = {};

        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};
        }

        $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
        // extra
        $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache, no-store, must-revalidate';
        $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
        $httpProvider.defaults.headers.get['Expires'] = '0';

        $stateProvider

            //------------------------------
            // HOME
            //------------------------------

            .state('home', {
                url: '/home',
                templateUrl: 'views/home.html',
                onEnter: function (store, $state) {
                    ////console.log('entry');
                    if (store.get('sessionid') && store.get('verified') && store.get('emailverified')) {
                        ////console.log('verified user');
                    } else {
                        $state.go('pages.profile.profile-about');
                        swal("Warning", "Please verify your Credential documents/OTP", "warning");
                    }
                },
                onExit: function () {
                    //write code when you change state
                    ////console.log('exit');
                },
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/fullcalendar/dist/fullcalendar.min.css',
                                ]
                            },
                            {
                                name: 'vendors',
                                insertBefore: '#app-level-js',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/sparklines/jquery.sparkline.min.js',
                                    'vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js',
                                    'vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js'
                                ]
                            }
                        ])
                    }
                }
            })

            .state('dashboard', {
                url: '/dashboard',
                templateUrl: 'views/dashboard.html'
            })
            .state('projectStatus', {
                url: '/projectStatus',
                templateUrl: 'views/projectStatus.html'
            })

            //------------------------------
            // FORMS
            //------------------------------
            .state('form', {
                url: '/form',
                templateUrl: 'views/common.html'
            })

            /*.state ('subuser', {
                url: '/subuser',
                templateUrl: 'views/resetpassword.html'
            })*/

            .state('resetpassword', {
                url: '/resetpassword/:email/:sessionid',
                templateUrl: 'views/resetpassword-1.html',

                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/nouislider/jquery.nouislider.css',
                                    'vendors/farbtastic/farbtastic.css',
                                    'vendors/bower_components/summernote/dist/summernote.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/bower_components/chosen/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/input-mask/input-mask.min.js',
                                    'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                    //'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/bower_components/summernote/dist/summernote.min.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/chosen/chosen.jquery.js',
                                    'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                    'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'
                                ]
                            }
                        ])
                    }
                }
            })

            //------------------------------
            // login
            //------------------------------
            .state('login', {
                url: '/login',
                templateUrl: 'js/login/login1.html',
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/nouislider/jquery.nouislider.css',
                                    'vendors/farbtastic/farbtastic.css',
                                    'vendors/bower_components/summernote/dist/summernote.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/bower_components/chosen/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/input-mask/input-mask.min.js',
                                    'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                    //'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/bower_components/summernote/dist/summernote.min.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/chosen/chosen.jquery.js',
                                    'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                    'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'
                                ]
                            }
                        ])
                    }
                }
            })

            .state('logincontainer', {
                url: '/logincontainer',
                templateUrl: 'js/login/login1.html',
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/nouislider/jquery.nouislider.css',
                                    'vendors/farbtastic/farbtastic.css',
                                    'vendors/bower_components/summernote/dist/summernote.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/bower_components/chosen/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/input-mask/input-mask.min.js',
                                    'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                    //'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/bower_components/summernote/dist/summernote.min.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/chosen/chosen.jquery.js',
                                    'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                    'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'
                                ]
                            }
                        ])
                    }
                }
            })

            .state('termsandconditions', {
                url: '/termsandconditions',
                templateUrl: 'js/login/termsConditions.html'
            })

            //------------------------------
            // CHARTS
            //------------------------------

            .state('charts', {
                url: '/charts',
                templateUrl: 'views/common.html'
            })

            .state('charts.flot-charts', {
                url: '/flot-charts',
                templateUrl: 'views/flot-charts.html',
            })

            //------------------------------
            // CALENDAR
            //------------------------------

            .state('calendar', {
                url: '/calendar',
                templateUrl: 'views/calendar.html',
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/fullcalendar/dist/fullcalendar.min.css',
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    //'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/fullcalendar/dist/fullcalendar.min.js'
                                ]
                            }
                        ])
                    }
                }
            })

           .state('popup', {
               url: '/popup',
               templateUrl: 'views/popup.html',
               resolve: {
                   loadPlugin: function ($ocLazyLoad) {
                       return $ocLazyLoad.load([
                           {
                               name: 'css',
                               insertBefore: '#app-level',
                               files: [
                                   'vendors/bower_components/fullcalendar/dist/fullcalendar.min.css',
                               ]
                           },
                           {
                               name: 'vendors',
                               files: [
                                   'vendors/bower_components/ofline/offline.js',
                                   //'vendors/bower_components/moment/min/moment.min.js',
                                   'vendors/bower_components/fullcalendar/dist/fullcalendar.min.js'
                               ]
                           }
                       ])
                   }
               }
           })

            //------------------------------
            // PAGES
            //------------------------------

            .state('pages', {
                url: '/pages',
                templateUrl: 'views/common.html'
            })

            .state('requirement-search', {
                url: '/requirement-search',
                templateUrl: 'views/requirement-search.html'
            })

            //Profile

            .state('pages.profile', {
                url: '/profile',
                templateUrl: 'views/profile.html'
            })

            .state('pages.profile.profile-about', {
                url: '/profile-about',
                templateUrl: 'views/profile-about.html'
            })

            .state('pages.profile.profile-timeline_SubUsers', {
                url: '/timeline_SubUsers',
                templateUrl: 'views/profile-timeline_SubUsers.html',
                onEnter: function (store, $state) {
                    ////console.log('entry');
                    if (store.get('sessionid') && store.get('verified') && store.get('emailverified')) {
                        ////console.log('verified user');
                    } else {
                        $state.go('pages.profile.profile-about');
                        swal("Warning", "Please verify your Credential documents/OTP", "warning");
                    }
                }
            })

            .state('pages.profile.profile-timeline', {
                url: '/profile-timeline',
                templateUrl: 'views/profile-timeline.html',
                // Clickable dashboard //
                params: { filters: null },
                // Clickable dashboard //
                onEnter: function (store, $state) {
                    ////console.log('entry');
                    if (store.get('sessionid') && store.get('verified') && store.get('emailverified')) {
                        ////console.log('verified user');
                    } else {
                        $state.go('pages.profile.profile-about');
                        swal("Warning", "Please verify your Credential documents/OTP", "warning");
                    }
                },
                onExit: function () {
                    //write code when you change state
                    ////console.log('exit');
                },
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/bower_components/lightgallery/light-gallery/js/lightGallery.min.js'
                                ]
                            }
                        ])
                    }
                }
            })

            .state('pages.profile.profile-timeline_1', {
                url: '/profile-timeline_1',
                templateUrl: 'views/profile-timeline_1.html',
                onEnter: function (store, $state) {
                    ////console.log('entry');
                    if (store.get('sessionid') && store.get('verified') && store.get('emailverified')) {
                        ////console.log('verified user');
                    } else {
                        $state.go('pages.profile.profile-about');
                        swal("Warning", "Please verify your Credential documents/OTP", "warning");
                    }
                },
                onExit: function () {
                    //write code when you change state
                    ////console.log('exit');
                },
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/bower_components/lightgallery/light-gallery/js/lightGallery.min.js'
                                ]
                            }
                        ])
                    }
                }
            })

            .state('pages.profile.requirement-comments', {
                url: '/requirement-comments',
                templateUrl: 'views/requirementcomments.html',
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/bower_components/lightgallery/light-gallery/js/lightGallery.min.js'
                                ]
                            }
                        ])
                    }
                }
            })

            .state('pages.profile.prof-info', {
                url: '/prof-info',
                templateUrl: 'views/prof-info.html',
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/bower_components/lightgallery/light-gallery/js/lightGallery.min.js'
                                ]
                            }
                        ])
                    }
                }
            })

            .state('pages.profile.new-vendor', {
                url: '/new-vendor',
                templateUrl: 'views/new-vendor.html',
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/bower_components/lightgallery/light-gallery/js/lightGallery.min.js'
                                ]
                            }
                        ])
                    }
                }
            })

            .state('pages.profile.profile-password-management', {
                url: '/profile-password-management',
                templateUrl: 'views/profile-password-management.html',
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/lightgallery/light-gallery/css/lightGallery.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/bower_components/lightgallery/light-gallery/js/lightGallery.min.js'
                                ]
                            }
                        ])
                    }
                }
            })
             .state('pages.profile.profile-settings', {
                 url: '/profile-settings',
                 templateUrl: 'views/profile-settings.html'
             })


            .state('pages.profile.profile-smarttable', {
                url: '/smarttable',
                templateUrl: 'views/smarttable.html'
            })

            .state('pages.profile.vendor-catalog', {
                url: '/vendor-catalog',
                templateUrl: 'views/vendor-catalog.html'
            })



            //------------------------------
            // BREADCRUMB DEMO
            //------------------------------
            .state('breadcrumb-demo', {
                url: '/breadcrumb-demo',
                templateUrl: 'views/breadcrumb-demo.html'
            })

            //--CUSTOME STATES
            //----New requirement
            .state('form.addnewrequirement', {
                url: '/addnewrequirement/:Id',
                templateUrl: 'views/addnewrequirement.html',
                params: {
                    reqObj: null
                },
                onEnter: function (store, $state) {
                    ////console.log('entry');
                    if (store.get('sessionid') && store.get('verified') && store.get('emailverified')) {
                        ////console.log('verified user');
                    } else {

                        $state.go('pages.profile.profile-about');
                        swal("Warning", "Please verify your Credential documents/OTP", "warning");
                    }
                },
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/nouislider/jquery.nouislider.css',
                                    'vendors/farbtastic/farbtastic.css',
                                    'vendors/bower_components/summernote/dist/summernote.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/bower_components/chosen/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/input-mask/input-mask.min.js',
                                    'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                    //'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/bower_components/summernote/dist/summernote.min.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/chosen/chosen.jquery.js',
                                    'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                    'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'
                                ]
                            }
                        ])
                    }
                }
            })

            //---Reports
            .state('charts.reports', {
                url: '/reports',
                templateUrl: 'views/flot-charts.html',
            })

            //---suppport

            //--myaccount
            .state('pages.profile.myaccount', {
                url: '/myaccount',
                templateUrl: 'views/profile-about.html'
            })


            //View Profile

            /*.state ('pages.profile.viewprofile', {
                url: '/viewprofile',
                templateUrl: 'views/view-profile.html'
            })*/

            .state('viewProfile', {
                url: '/viewprofile/:Id',
                templateUrl: 'views/viewProfile.html',
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/nouislider/jquery.nouislider.css',
                                    'vendors/farbtastic/farbtastic.css',
                                    'vendors/bower_components/summernote/dist/summernote.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/bower_components/chosen/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/input-mask/input-mask.min.js',
                                    'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                    //'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/bower_components/summernote/dist/summernote.min.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/chosen/chosen.jquery.js',
                                    'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                    'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'
                                ]
                            }
                        ])
                    }
                }
            })

             .state('bidhistory', {
                 url: '/bidhistory/:Id/:reqID/:isfwd',
                 templateUrl: 'views/bidHistory.html',
                 resolve: {
                     loadPlugin: function ($ocLazyLoad) {
                         return $ocLazyLoad.load([
                             {
                                 name: 'css',
                                 insertBefore: '#app-level',
                                 files: [
                                     'vendors/bower_components/nouislider/jquery.nouislider.css',
                                     'vendors/farbtastic/farbtastic.css',
                                     'vendors/bower_components/summernote/dist/summernote.css',
                                     'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                     'vendors/bower_components/chosen/chosen.min.css'
                                 ]
                             },
                             {
                                 name: 'vendors',
                                 files: [
                                     'vendors/bower_components/ofline/offline.js',
                                     'vendors/input-mask/input-mask.min.js',
                                     'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                     //'vendors/bower_components/moment/min/moment.min.js',
                                     'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                     'vendors/bower_components/summernote/dist/summernote.min.js',
                                     'vendors/fileinput/fileinput.min.js',
                                     'vendors/bower_components/chosen/chosen.jquery.js',
                                     'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                     'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'
                                 ]
                             }
                         ])
                     }
                 }
             })

            .state('ui-bootstrap', {
                url: '/ui-bootstrap',
                templateUrl: 'views/ui-bootstrap.html'
            })

            .state('view-requirement', {
                url: '/view-requirement/:Id',
                templateUrl: 'views/list-item.html',
                onEnter: function (store, $state) {
                    ////console.log('entry');
                    if (store.get('sessionid') && store.get('verified') && store.get('emailverified')) {
                        ////console.log('verified user');
                    } else {
                        $state.go('login');
                    }
                },
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/nouislider/jquery.nouislider.css',
                                    'vendors/farbtastic/farbtastic.css',
                                    'vendors/bower_components/summernote/dist/summernote.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/bower_components/chosen/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/input-mask/input-mask.min.js',
                                    'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                    //'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/bower_components/summernote/dist/summernote.min.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/chosen/chosen.jquery.js',
                                    'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                    'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'

                                ]
                            }
                        ]);
                    }
                }
            })
            .state('fwd-view-requirement', {
                url: '/fwd-view-requirement/:Id',
                templateUrl: 'forward-bidding/views/fwd-list-item.html',
                onEnter: function (store, $state) {
                    if (!(store.get('sessionid') && store.get('verified') && store.get('emailverified'))) {
                        $state.go('login');
                    }
                },
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/nouislider/jquery.nouislider.css',
                                    'vendors/farbtastic/farbtastic.css',
                                    'vendors/bower_components/summernote/dist/summernote.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/bower_components/chosen/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/input-mask/input-mask.min.js',
                                    'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                    //'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/bower_components/summernote/dist/summernote.min.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/chosen/chosen.jquery.js',
                                    'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                    'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'

                                ]
                            }
                        ]);
                    }
                }
            })

            .state('Meterial-Dispatchment', {
                url: '/Meterial-Dispatchment',
                templateUrl: 'Meterial-Dispatchment.html'
            })

            .state('generate-po', {
                url: '/generate-po/:Id',
                templateUrl: 'views/POForm.html',
                onEnter: function (store, $state) {
                    ////console.log('entry');
                    if (store.get('sessionid') && store.get('verified') && store.get('emailverified')) {
                        ////console.log('verified user');
                    } else {
                        $state.go('login');
                    }
                },
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/nouislider/jquery.nouislider.css',
                                    'vendors/farbtastic/farbtastic.css',
                                    'vendors/bower_components/summernote/dist/summernote.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/bower_components/chosen/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/input-mask/input-mask.min.js',
                                    'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                    //'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/bower_components/summernote/dist/summernote.min.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/chosen/chosen.jquery.js',
                                    'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                    'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'
                                ]
                            }
                        ])
                    }
                }
            })

            .state('reminders', {
                url: '/reminders/:Id',
                templateUrl: 'views/reminders.html',
                onEnter: function (store, $state) {
                    if (store.get('sessionid') && store.get('verified') && store.get('emailverified')) {
                    } else {
                        $state.go('login');
                    }
                },
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/nouislider/jquery.nouislider.css',
                                    'vendors/farbtastic/farbtastic.css',
                                    'vendors/bower_components/summernote/dist/summernote.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/bower_components/chosen/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/input-mask/input-mask.min.js',
                                    'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                    //'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/bower_components/summernote/dist/summernote.min.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/chosen/chosen.jquery.js',
                                    'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                    'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'
                                ]
                            }
                        ])
                    }
                }
            })

            .state('material-dispatchment', {
                url: '/material-dispatchment/:Id',
                templateUrl: 'views/Material-Dispatchment.html',
                onEnter: function (store, $state) {
                    ////console.log('entry');
                    if (store.get('sessionid') && store.get('verified') && store.get('emailverified')) {
                        ////console.log('verified user');
                    } else {
                        $state.go('login');
                    }
                },
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/nouislider/jquery.nouislider.css',
                                    'vendors/farbtastic/farbtastic.css',
                                    'vendors/bower_components/summernote/dist/summernote.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/bower_components/chosen/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/input-mask/input-mask.min.js',
                                    'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                    //'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/bower_components/summernote/dist/summernote.min.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/chosen/chosen.jquery.js',
                                    'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                    'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'
                                ]
                            }
                        ])
                    }
                }
            })

            .state('payment-details', {
                url: '/payment-details/:Id',
                templateUrl: 'views/payment-details.html',
                onEnter: function (store, $state) {
                    ////console.log('entry');
                    if (store.get('sessionid') && store.get('verified') && store.get('emailverified')) {
                        ////console.log('verified user');
                    } else {
                        $state.go('login');
                    }
                },
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/nouislider/jquery.nouislider.css',
                                    'vendors/farbtastic/farbtastic.css',
                                    'vendors/bower_components/summernote/dist/summernote.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/bower_components/chosen/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/input-mask/input-mask.min.js',
                                    'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                    //'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/bower_components/summernote/dist/summernote.min.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/chosen/chosen.jquery.js',
                                    'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                    'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'
                                ]
                            }
                        ])
                    }
                }
            })

            .state('view-requirement-test', {
                url: '/view-requirement-test/:Id',
                templateUrl: 'views/list-item-new.html',

            })

            .state('view-requirement-customer', {
                url: '/view-requirement-customer/:Id',
                templateUrl: 'views/list-item-new-customer.html',

            })

            .state('view-requirement-margin', {
                url: '/view-requirement-margin/:Id',
                templateUrl: 'views/list-item-new-margin.html',

            })

            .state('view-requirement-price', {
                url: '/view-requirement-price/:Id',
                templateUrl: 'views/list-item-new-price.html',

            })

            .state('view-requirement-discount', {
                url: '/view-requirement-discount/:Id',
                templateUrl: 'views/list-item-new-discount.html',

            })


            .state('form.editrequirement', {
                url: '/editrequirement/:Id',
                templateUrl: 'views/addnewrequirement.html',
                onEnter: function (store, $state) {
                    ////console.log('entry');
                    if (store.get('sessionid') && store.get('verified') && store.get('emailverified')) {
                        ////console.log('verified user');
                    } else {
                        $state.go('login');
                    }
                },
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/nouislider/jquery.nouislider.css',
                                    'vendors/farbtastic/farbtastic.css',
                                    'vendors/bower_components/summernote/dist/summernote.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/bower_components/chosen/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/input-mask/input-mask.min.js',
                                    'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                    //'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/bower_components/summernote/dist/summernote.min.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/chosen/chosen.jquery.js',
                                    'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                    'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'
                                ]
                            }
                        ])
                    }
                }
            })
            .state('view-order', {
                url: '/view-order',
                templateUrl: 'views/my-order.html',
                resolve: {
                    loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load([
                            {
                                name: 'css',
                                insertBefore: '#app-level',
                                files: [
                                    'vendors/bower_components/nouislider/jquery.nouislider.css',
                                    'vendors/farbtastic/farbtastic.css',
                                    'vendors/bower_components/summernote/dist/summernote.css',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                    'vendors/bower_components/chosen/chosen.min.css'
                                ]
                            },
                            {
                                name: 'vendors',
                                files: [
                                    'vendors/bower_components/ofline/offline.js',
                                    'vendors/input-mask/input-mask.min.js',
                                    'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                    //'vendors/bower_components/moment/min/moment.min.js',
                                    'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                    'vendors/bower_components/summernote/dist/summernote.min.js',
                                    'vendors/fileinput/fileinput.min.js',
                                    'vendors/bower_components/chosen/chosen.jquery.js',
                                    'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                    'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'
                                ]
                            }
                        ])
                    }
                }
            })

        .state('req-savings', {
            url: '/req-savings/:Id',
            templateUrl: 'views/req-savings.html',
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'css',
                            insertBefore: '#app-level',
                            files: [
                                'vendors/bower_components/nouislider/jquery.nouislider.css',
                                'vendors/farbtastic/farbtastic.css',
                                'vendors/bower_components/summernote/dist/summernote.css',
                                'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                'vendors/bower_components/chosen/chosen.min.css'
                            ]
                        },
                        {
                            name: 'vendors',
                            files: [
                                'vendors/bower_components/ofline/offline.js',
                                'vendors/input-mask/input-mask.min.js',
                                'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                //'vendors/bower_components/moment/min/moment.min.js',
                                'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                'vendors/bower_components/summernote/dist/summernote.min.js',
                                'vendors/fileinput/fileinput.min.js',
                                'vendors/bower_components/chosen/chosen.jquery.js',
                                'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'
                            ]
                        }
                    ])
                }
            }
        })

        .state('req-savingsPreNegotiation', {
            url: '/req-savingsPreNegotiation/:Id',
            templateUrl: 'views/req-savingsPreNegotiation.html',
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'css',
                            insertBefore: '#app-level',
                            files: [
                                'vendors/bower_components/nouislider/jquery.nouislider.css',
                                'vendors/farbtastic/farbtastic.css',
                                'vendors/bower_components/summernote/dist/summernote.css',
                                'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
                                'vendors/bower_components/chosen/chosen.min.css'
                            ]
                        },
                        {
                            name: 'vendors',
                            files: [
                                'vendors/bower_components/ofline/offline.js',
                                'vendors/input-mask/input-mask.min.js',
                                'vendors/bower_components/nouislider/jquery.nouislider.min.js',
                                //'vendors/bower_components/moment/min/moment.min.js',
                                'vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
                                'vendors/bower_components/summernote/dist/summernote.min.js',
                                'vendors/fileinput/fileinput.min.js',
                                'vendors/bower_components/chosen/chosen.jquery.js',
                                'vendors/bower_components/angular-chosen-localytics/chosen.js',
                                'vendors/bower_components/angular-farbtastic/angular-farbtastic.js'
                            ]
                        }
                    ])
                }
            }
        })


        .state('pages.profile.companyDepartments', {
            url: '/companyDepartments',
            templateUrl: 'views/companyDepartments.html'
        })

        .state('pages.profile.companyDesignations', {
            url: '/companyDesignations',
            templateUrl: 'views/companyDesignations.html'
        })
        .state('pages.profile.currencySettings', {
            url: '/currencySettings',
            templateUrl: 'views/currencySettings.html'
        })

        .state('pages.profile.companyBranches', {
            url: '/companyBranches',
            templateUrl: 'views/companyBranches.html'
        })

       //.state('catalogMgmt', {
       //         url: '/catalogMgmt',
       //         templateUrl: 'views/catalogMgmt.html'
       //})

       //.state('productAttribute', {
       //    url: '/productAttribute',
       //    templateUrl: 'views/productAttribute.html'
       //})


        .state('reqTechSupport', {
            url: '/reqTechSupport/:reqId',
            templateUrl: 'views/reqTechSupport.html'
        })


        .state('comparatives', {
            url: '/comparatives/:reqID',
            templateUrl: 'views/comparatives.html'
        })

        .state('marginComparatives', {
            url: '/marginComparatives/:reqID',
            templateUrl: 'views/marginComparatives.html'
        })

        .state('Material-Received', {
            url: '/Material-Received/:Id',
            templateUrl: 'views/Material-Received.html'
        })

        .state('category', {
                url: '/category',
                templateUrl: 'views/category.html',
        })

        



        .state('vendorTechnicalManual', {
            url: '/vendorTechnicalManual',
            templateUrl: 'views/vendorTechnicalManual.html'
            })

            .state('consalidatedReport', {
                url: '/consalidatedReport',
                templateUrl: 'views/reportingviews/consalidated-report.html'
            })

            .state('consalidatedBasePriceReport', {
                url: '/consalidatedBasePriceReport',
                templateUrl: 'views/reportingviews/consalidatedBasePriceReport.html'
            })
            .state('materialReport', {
                url: '/materialReport',
                templateUrl: 'views/reportingviews/materialReport.html'
            })
            //temporary form routes
            .state('domesticForm', {
                url: '/domesticForm',
                templateUrl: 'views/domestic.html'
            })
            .state('import_ZSIM', {
                url: '/import_ZSIM',
                templateUrl: 'views/Import-ZSIM.html'
            })
            .state('bonded_WH', {
                url: '/bonded_WH',
                templateUrl: 'views/Bonded_WH.html'
            })
            .state('service_ZSSR', {
                url: '/service_ZSSR',
                templateUrl: 'views/Service_ZSSR.html'
            })
            .state('home1', {
                url: '/home1',
                templateUrl: 'views/home1.html'
            })
            .state('gstInfo', {
                url: '/gstInfo',
                templateUrl: 'views/gstInfo.html'
            })
            .state('incoTerms', {
                url: '/incoTerms',
                templateUrl: 'views/incoTerms.html'
            })
            .state('vendorInvoices', {
                url: '/vendorInvoices',
                templateUrl: 'views/vendorInvoices.html'
            })
            .state('vendorPoInvoices', {
                url: '/vendorPoInvoices',
                templateUrl: 'views/vendorPoInvoices.html'
            })
            .state('createInvoice', {
                url: '/createInvoice/:poNumber/:invoiceNumber/:invoiceID',
                templateUrl: 'views/createInvoice.html'
            })
            .state('freshdeskTickets', {
                url: '/freshdeskTickets',
                templateUrl: 'views/freshdeskTickets.html'
            })
            

    }]);
