﻿prmApp
    .controller('contractUtilisationCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService",
        "storeService", "growlService", "PRMPRServices", "poService", "$rootScope", "catalogService",
        "fileReader",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService,
            storeService, growlService, PRMPRServices, poService, $rootScope, catalogService,
            fileReader) {

            $scope.userId = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.PC_ID = $stateParams.PC_ID;
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.filtersList = {
                contractList: [],
                prList: [],
                productList: [],
                vendorList: [],
                poList: []
            };

            $scope.filters = {
                contracts: {},
                pr: {},
                po: {},
                products: {},
                vendors: {},
                searchKeyword: '',
                fromDate: moment().add('days', -30).format('YYYY-MM-DD'),
                toDate: moment().format('YYYY-MM-DD')
            };

            $scope.filtersValues = {};
            if ($stateParams.contractProducts)
            {
                $scope.filtersValues = JSON.parse($stateParams.contractProducts);
            }

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getContractUtilisations(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);
                //$scope.getpendingPOlist(($scope.currentPage - 1), 10, $scope.filters.searchPRsKeyword, $scope.filters.searchRFQsKeyword);
            };


            $scope.setFilters = function (currentPage) {
                if ($scope.loadServices) {
                    $scope.getContractUtilisations(0, 10, $scope.filters.searchKeyword);
                }
            };

            $scope.getContractUtilisationsFilters = function () {
                var params =
                {
                    "compId": $scope.compId,
                    "userId": $scope.userId,
                    "sessionid": $scope.sessionID
                };

                let contractListTemp = [];
                let prListTemp = [];
                let productListTemp = [];
                let vendorListTemp = [];
                let poListTemp = [];
                $scope.loadServices = false;
                
                catalogService.getContractUtilisationsFilters(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.filterValues = response;
                            
                            if ($scope.filterValues && $scope.filterValues.length > 0) {
                                
                                $scope.filterValues.forEach(function (item, index) {
                                    if (item.TYPE === 'CONTRACT') {
                                        contractListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PR') {
                                        prListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PRODUCT') {
                                        productListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'VENDOR') {
                                        vendorListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PO') {
                                        poListTemp.push({ id: item.ID, name: item.NAME });
                                    }
                                });

                                $scope.filtersList.contractList = contractListTemp;
                                $scope.filtersList.prList = prListTemp;
                                $scope.filtersList.productList = productListTemp;
                                $scope.filtersList.vendorList = vendorListTemp;
                                $scope.filtersList.poList = poListTemp;


                                if ($scope.filtersValues.contracts || $scope.filtersValues.products || $scope.filtersValues.vendors)
                                {
                                    if ($scope.filtersValues.contracts && $scope.filtersValues.contracts.length > 0) {
                                        $scope.filters.contracts = [];
                                        $scope.filtersValues.contracts.forEach(function (item, i) {
                                            $scope.filtersList.contractList.forEach(function (subitem, index) {
                                                if (item.id == subitem.id) {
                                                    $scope.filters.contracts.push($scope.filtersList.contractList[index]);
                                                }
                                            })
                                        });
                                    };

                                    if ($scope.filtersValues.products && $scope.filtersValues.products.length > 0) {
                                        $scope.filters.products = [];
                                        $scope.filtersValues.products.forEach(function (item, i) {
                                            $scope.filtersList.productList.forEach(function (subitem, index) {
                                                if (item.id == subitem.id) {
                                                    $scope.filters.products.push($scope.filtersList.productList[index]);
                                                }
                                            })
                                        });
                                    };

                                    if ($scope.filtersValues.vendors && $scope.filtersValues.vendors.length > 0) {
                                        $scope.filters.vendors = [];
                                        $scope.filtersValues.vendors.forEach(function (item, i) {
                                            $scope.filtersList.vendorList.forEach(function (subitem, index) {
                                                if (item.id == subitem.id) {
                                                    $scope.filters.vendors.push($scope.filtersList.vendorList[index]);
                                                }
                                            })
                                        });
                                    };

                                }

                                $scope.getContractUtilisations(0, 10, '');
                            }
                        }
                    });

            };


            $scope.getContractUtilisations = function (recordsFetchFrom, pageSize, searchString)
            {
                $scope.contractUtilised = [];

                var contract, pr, product, vendor, po, fromDate, toDate;

                if (_.isEmpty($scope.filters.contracts)) {
                    contract = '';
                } else if ($scope.filters.contracts && $scope.filters.contracts.length > 0) {
                    var contracts = _($scope.filters.contracts)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    contract = contracts.join(',');
                }

                if (_.isEmpty($scope.filters.pr)) {
                    pr = '';
                } else if ($scope.filters.pr && $scope.filters.pr.length > 0) {
                    var prs = _($scope.filters.pr)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    pr = prs.join(',');
                }

                if (_.isEmpty($scope.filters.products)) {
                    product = '';
                } else if ($scope.filters.products && $scope.filters.products.length > 0) {
                    var products = _($scope.filters.products)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    product = products.join(',');
                }

                if (_.isEmpty($scope.filters.vendors)) {
                    vendor = '';
                } else if ($scope.filters.vendors && $scope.filters.vendors.length > 0) {
                    var vendors = _($scope.filters.vendors)
                        .filter(item => item.name)
                        .map('id')
                        .value();
                    vendor = vendors.join(',');
                }

                if (_.isEmpty($scope.filters.po)) {
                    po = '';
                } else if ($scope.filters.po && $scope.filters.po.length > 0) {
                    var pos = _($scope.filters.po)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    po = pos.join(',');
                }

                if (_.isEmpty($scope.filters.fromDate)) {
                    fromDate = '';
                } else {
                    fromDate = $scope.filters.fromDate;
                }

                if (_.isEmpty($scope.filters.toDate)) {
                    toDate = '';
                } else {
                    toDate = $scope.filters.toDate;
                }

                var params = {
                    "compId": $scope.compId,
                    "userId": $scope.userId,
                    "sessionid": $scope.sessionID,
                    "contracts": contract,
                    "prs": pr,
                    "products": product,
                    "vendors": vendor,
                    "pos": po,
                    "status": '',
                    "fromDate": fromDate,
                    "toDate": toDate,
                    "PageSize": recordsFetchFrom * pageSize,
                    "NumberOfRecords": pageSize,
                    "searchString": '',
                    "pcID": $scope.PC_ID
                };


                catalogService.getContractUtilisations(params)
                    .then(function (response) {
                        $scope.loadServices = true;
                        if (response && response.length > 0) {
                            response.forEach(function (item,index) {
                                item.DATE_CREATED = userService.toLocalDate(item.DATE_CREATED);
                            });
                            $scope.contractUtilised = response;

                            if ($scope.contractUtilised && $scope.contractUtilised.length > 0) {
                                $scope.totalItems = $scope.contractUtilised[0].TOTAL_ROWS;
                                $scope.filteredPRList = $scope.PRList;
                            }

                        }
                    });

            };

            $scope.getContractUtilisationsFilters();

        }]);
