﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
            .state('audit', {
                url: '/audit/:reqID',
                templateUrl: 'audit/views/Audit.html'
            })
                .state('paymentTracking', {
                    url: '/paymentTracking',
                    templateUrl: 'PendingPO/views/paymenttracking.html'
                })

        }]);