﻿prmApp
    .controller('storeitemdetailsCtrl', ["$scope", "$state", "$stateParams", "$log", "$window", "userService", "storeService", "fileReader", "growlService",
        function ($scope, $state, $stateParams, $log, $window, userService, storeService, fileReader, growlService) {
            //$scope.storeID = $stateParams.storeID;
            $scope.itemID = $stateParams.itemID;
            $scope.notDispatched = 0;

            $scope.sessionID = userService.getUserToken();

            $scope.StoreItemDetailsList = [];

            $scope.getStoreItemDetails = function () {
                storeService.getStoreItemDetails($scope.itemID, $scope.notDispatched)
                    .then(function (response) {
                        $scope.StoreItemDetailsList = response;
                    })
            }

            $scope.handleDate = function (date) {
                return new moment(date).format("DD-MMM-YYYY");
            }

            $scope.getStoreItemDetails();

            $scope.cancelClick = function () {
                $window.history.back();
            };

            $scope.deletestoreitemdetail = function (itemDetails) {
                var params = {
                    storeItemDetailsId: itemDetails.itemDetailID,
                    sessionID: $scope.sessionID
                };

                storeService.deleteStoreItemDetails(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Remove Item Successfully.", "success");
                            location.reload();
                        }
                    });
            };

            $scope.entityObj = {
                entityName: 'StoreItemDetails',
                attachment: [],
                userid: userService.getUserId(),
                sessionID: $scope.sessionID
            };

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {

                        if (id == "excelItemDetails") {
                            var bytearray = new Uint8Array(result);
                            $scope.entityObj.attachment = $.makeArray(bytearray);
                            $scope.importEntity();
                        }
                    });
            };

            $scope.importEntity = function () {

                var params = {
                    "entity": $scope.entityObj
                };

                storeService.importentity(params)
                    .then(function (response) {
                        if (response.errorMessage != '' || response.message != '') {
                            growlService.growl(response.errorMessage, "inverse");
                            growlService.growl(response.message, "inverse");
                        }
                        else {
                            growlService.growl("Saved Successfully.", "success");
                            location.reload();

                        }
                    });
            };

            $scope.exportItemsToExcel = function () {
                var mystyle = {
                    sheetid: 'StoreItemDetails',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                alasql.fn.handleDate = function (date) {
                    return new moment(date).format("MM/DD/YYYY");
                }

                alasql('SELECT itemDetailID as [ITEM_DETAIL_ID],  itemID as [ITEM_ID], serialNo as [SERIAL_NO], warrantyNo as [WARRANTY_NO], handleDate(warrantyDate) as [WARRANTY_DATE], ginID as [GIN_ID], grnID as [GRN_ID], poID as [PO_ID] INTO XLSX(?,{headers:true,sheetid: "StoreItemDetails", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["StoreItemDetails.xlsx", $scope.StoreItemDetailsList]);
            };

        }]);