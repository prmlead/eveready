﻿
prmApp
    .controller('profileReqsCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService", "$stateParams",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, $stateParams) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.totalVendors = 0;
            $scope.totalSubuser = 0;
            $scope.totalInactiveVendors = 0;
            $scope.totalLeads = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.PageSize = 10;
            $scope.itemsPerPage = 10;
            $scope.itemsPerPage2 = 8;
            $scope.maxSize = 8;
            $scope.myAuctions = [];
            $scope.categories = [];
            $scope.reqStatus = 'ALL';
            $scope.biddingType = 'ALL';
            $scope.category = 'ALL CATEGORIES';
            $scope.buyerFilter = 'ALL';
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.compId = userService.getUserCompanyId();
            $scope.searchKeyword = '';

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            // Clickable dashboard //
            $scope.NavigationFilters = $stateParams.filters;
            // Clickable dashboard //

            
            $scope.setPage = function (pageNo) {
                $scope.NavigationFilters = {};
                $scope.currentPage = pageNo;
                $scope.getAuctions(($scope.currentPage - 1), 10, $scope.searchKeyword);

            };

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };

            $scope.pageChanged = function () {
                //$scope.getAuctions();
            };

            $scope.filterDate = {
                reqToDate: '',
                reqFromDate: ''
            };

            $scope.filterDate.reqToDate = moment().format('YYYY-MM-DD');
            $scope.filterDate.reqFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            $scope.buyers = ['ALL'];
            $scope.getAuctions = function (recordsFetchFrom, pageSize, searchKeyword) {
                $log.info($scope.formRequest.isForwardBidding);
                if (!$scope.formRequest.isForwardBidding) {


                    //if (isfilter) {
                    //    $scope.totalItems = 0;
                    //    $scope.currentPage = 1;
                    //    $scope.currentPage2 = 1;
                    //    $scope.itemsPerPage = 8;
                    //    $scope.itemsPerPage2 = 8;
                    //    $scope.maxSize = 8;
                    //    $scope.myAuctions = [];
                    //}
                    if (!$scope.NavigationFilters) {
                        $scope.NavigationFilters = {};
                    }
                    auctionsService.getmyAuctions({ "userid": userService.getUserId(), "fromDate": $scope.filterDate.reqFromDate, "toDate": $scope.filterDate.reqToDate, "status": $scope.reqStatus, "search": searchKeyword ? searchKeyword : '', "allBuyer": $scope.buyerFilter, "page": recordsFetchFrom * pageSize, "pagesize": pageSize, "sessionid": userService.getUserToken(), "onlyrfq": 0, "onlyrfp": 0 })
                   // auctionsService.getmyAuctions({ "userid": userService.getUserId(), "reqstatus": $scope.reqStatus, "fromDate": $scope.filterDate.reqFromDate, "toDate": $scope.filterDate.reqToDate, "onlyrfq": false, "sessionid": userService.getUserToken(), "page": recordsFetchFrom * pageSize, "pagesize": pageSize, "search": searchKeyword ? searchKeyword : '' })
                        .then(function (response) {

                            if (response)
                            {
                                response.forEach(function (item,index) {
                                    item.qcsCodesArray = [];
                                    if (item.qcsCodes)
                                    {
                                        var values = item.qcsCodes.split(',');
                                        item.qcsCodesArray = values;
                                    }
                                });
                            }

                            $scope.myAuctions1 = [];
                            $scope.forReqFilter = [];
                           // $scope.buyers = ['ALL'];
                            //$scope.myAuctions1 = $scope.myAuctions.concat(response);
                            $scope.myAuctions1 = response;
                            $scope.forReqFilter = angular.copy(response);
                            $scope.myAuctions1 = $scope.myAuctions1.filter(function (auction) {
                                return (auction.biddingType !== 'TENDER');
                            });
                            $scope.myAuctions1.forEach(function (item, index) {
                                item.postedOn = $scope.GetDateconverted(item.postedOn);
                                item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                                item.expStartTime = $scope.GetDateconverted(item.expStartTime);
                                item.startTime = $scope.GetDateconverted(item.startTime);

                                //if ($scope.buyers.indexOf(item.userName) < 0) {
                                //    $scope.buyers.push(item.userName)
                                //}

                                if (String(item.startTime).includes('9999') || String(item.startTime).includes('10000')) {
                                    item.startTime = '-';
                                }


                                //if (item.status == "UNCONFIRMED" && (item.status != "NOTSTARTED" || item.status != "STARTED" || item.status != "Negotiation Ended")) {
                                //    if (item.noOfVendorsInvited === 0) {
                                //        item.status = "Saved As Draft";
                                //        item.expStartTime = "--";
                                //        item.startTime = "--";
                                //    } else {
                                //        if (item.noOfVendorsInvited == item.noOfVendorsParticipated) {
                                //            item.status = "Quotations Received";
                                //        }
                                //        else if (item.noOfVendorsInvited != item.noOfVendorsParticipated) {
                                //            item.status = "UNCONFIRMED";
                                //        }
                                //    }
                                //}


                                if (item.status == 'UNCONFIRMED') {
                                    item.statusColor = 'text-warning';
                                }
                                else if (item.status == 'NOT STARTED') {
                                    item.statusColor = 'text-warning';
                                }
                                else if (item.status == 'STARTED') {
                                    item.statusColor = 'text-danger';
                                }
                                else if (item.status == 'Negotiation Ended') {
                                    item.statusColor = 'text-success';
                                }
                                else if (item.status == 'Qcs Pending') {
                                    item.statusColor = 'text-danger';
                                }
                                else if (item.status == 'Qcs Created') {
                                    item.statusColor = 'text-danger';
                                }
                                else if (item.status == 'Qcs Approval Pending') {
                                    item.statusColor = 'text-danger';
                                }
                                else if (item.status == 'Qcs Approved') {
                                    item.statusColor = 'text-success';
                                }
                                else if (item.status == 'Quotations Received') {
                                    item.statusColor = 'text-success';
                                } else if (item.status == 'Closed') {
                                    item.statusColor = 'text-success';
                                }
                                else {
                                    item.statusColor = '';
                                }


                                if (item.isRFP) {
                                    item.rfpStatusColor = 'orangered';
                                } else {
                                    item.rfpStatusColor = 'darkgreen';
                                }



                            })

                            $scope.myAuctions = $scope.myAuctions1;
                            $scope.myAuctions2 = response;

                            //$scope.participatedVendors = $scope.auctionItem.auctionVendors.filter(function (vendor) {
                            //    return (vendor.runningPrice > 0 && vendor.companyName != 'PRICE_CAP');
                            //});

                            //if ($scope.myAuctions1 && $scope.myAuctions1.length > 0)
                            //{
                            //    $scope.totalItems = $scope.myAuctions1[0].totalCount;
                            //    for (var i = 0; i <= $scope.myAuctions1.length; i++) {
                            //        if ($scope.myAuctions1[i] && $scope.myAuctions1[i].requirementID > 0)
                            //        {
                            //            $scope.myAuctions[(($scope.currentPage - 1) * $scope.itemsPerPage) + i] = $scope.myAuctions1[i];
                            //        }

                            //    }
                            //}




                            if ($scope.myAuctions.length > 0) {
                                $scope.myAuctionsLoaded = true;
                                $scope.totalItems = $scope.myAuctions[0].totalCount;
                            } else {
                                $scope.myAuctionsLoaded = false;
                                $scope.totalItems = 0;
                                $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                            }



                            $scope.tempStatusFilter = angular.copy($scope.myAuctions);
                            $scope.tempCategoryFilter = angular.copy($scope.myAuctions);


                            // Clickable dashboard //

                            if ($scope.NavigationFilters && $scope.NavigationFilters.status) {
                                $scope.getStatusFilter($scope.NavigationFilters.status);
                                $scope.reqStatus = $scope.NavigationFilters.status;
                            }

                            // Clickable dashboard //

                        });
                }
                else {
                    fwdauctionsService.getmyAuctions({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                        .then(function (response) {
                            $scope.myAuctions = response;
                            if ($scope.myAuctions.length > 0) {
                                $scope.myAuctionsLoaded = true;
                                $scope.totalItems = $scope.myAuctions.length;
                            } else {
                                $scope.myAuctionsLoaded = false;
                                $scope.totalItems = 0;
                                $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                            }
                        });
                }
            };

            //$scope.getAuctions(0, 10, $scope.searchKeyword);

            $scope.GetRequirementsReport = function () {
                auctionsService.getrequirementsreport({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.reqReport = response;
                        alasql.fn.handleDate = function (date) {
                            return new moment(date).format("MM/DD/YYYY");
                        }
                        alasql('SELECT requirementID as RequirementID, title as RequirementTitle, handleDate(postedDate) as [PostedDate], status as Status, vendorID as VendorID, companyName as CompanyName, userPhone as Phone,userEmail as Email, itemID as [ItemID], productBrand as Brand, productQuantity as Quantity, productQuantityIn as [Units], costPrice as [InitialCostPrice], revCostPrice as [FinalCostPrice], netPrice as [InitialNetPrice], marginAmount as [InitialMarginAmount], unitDiscount as [MarginPercentage],revUnitDiscount as [RevisedMarginPercentage],unitMRP as [MRP],gst as [GST],savings as [Savings], handleDate(startTime) as [StartTime], status as [Status], othersBrands as [ManufacturerName] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xlsx", $scope.reqReport]);
                    });

                //alasql('SELECT requirementID as [RequirementID],title as [Title],vendorID as [VendorID],companyName as [Company],userPhone as [Phone],userEmail as [Email],itemID as [ItemID],productBrand as [Brand],productQuantity as [Quantity], productQuantityIn as [Units],costPrice as [InitialCostPrice],revCostPrice as [FinalCostPrice],netPrice as [NetPrice],marginAmount as [InitialMarginAmount],unitDiscount as [InitialMargin],revUnitDiscount as [FinalMargin],unitMRP as [MRP],gst as [GST],maxInitMargin as [MaximumInitialMargin],maxFinalMargin as [MaximumFinalMargin],savings as [Savings] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xslx", $scope.reqReport]);

            };
            
            $scope.goToReqReport = function (reqID) {
                    //$state.go("reports", { "reqID": reqID });

                    var url = $state.href('reports', { "reqID": reqID });
                    $window.open(url, '_blank');

            };

            $scope.cloneRequirement = function (requirement) {
                let tempObj = {};
                let stateView = 'save-requirementAdv';
                if (requirement.biddingType && requirement.biddingType === 'TENDER') {
                    stateView = 'save-tender';
                }
                
                tempObj.cloneId = requirement.requirementID;
                $state.go(stateView, { 'Id': requirement.requirementID, reqObj: tempObj });
            };

            $scope.chatRequirement = function (requirement) {
                var url = $state.href('req-chat', { "reqId": requirement.requirementID });
                $window.open(url, '_blank');
            };

            $scope.searchTable = function (str) {

                //$scope.category = 'ALL CATEGORIES';
                //$scope.reqStatus = 'ALL';
                //$scope.myAuctions = $scope.myAuctions1.filter(function (req) {
                //    return (String(req.requirementID).includes(str) == true || String(req.title).includes(str) == true || req.category[0].includes(str) == true );
                //});

                // RFQ Date Based Filter // 

                str = str.toLowerCase();
                $scope.category = 'ALL CATEGORIES';
                $scope.reqStatus = 'ALL';
                $scope.myAuctions = $scope.myAuctions1.filter(function (req) {
                    return (String(req.requirementID).includes(str)
                        || String(req.title.toLowerCase()).includes(str)
                        || String(req.userName.toLowerCase()).includes(str)
                        || String(req.projectName.toLowerCase()).includes(str)
                        || String(req.projectDetails.toLowerCase()).includes(str)
                        || String(req.postedOn).includes(str)
                        || req.category[0].toLowerCase().includes(str)
                        || (req.prNumbers ? req.prNumbers : '').includes(str)

                    );
                   
                });

                // RFQ Date Based Filter //

                $scope.totalItems = $scope.myAuctions.length;
            };

            
            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));

                            $scope.categories.push('ALL CATEGORIES');

                            //categories.splice(categories.indexOf("ALL"), 1);
                            //categories.unshift('ALL');

                            $scope.categories = $scope.categories.filter(item => item !== "ALL CATEGORIES" && item !== "");
                            $scope.categories.unshift("ALL CATEGORIES");

                            //console.log($scope.categories);

                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    }
                }, function (result) {
                });

            };


          //  $scope.getCategories();

            $scope.getStatusFilter = function (filterVal, str) {
                $scope.myAuctions = $scope.myAuctions1;
                $scope.filterArray = [];
                if (filterVal !== 'ALL') {
                    $scope.filterArray = $scope.myAuctions.filter(function (item) {
                        return $scope.prmStatus($scope.isCustomer, item.status.toLowerCase()) === $scope.prmStatus($scope.isCustomer, filterVal.toLowerCase());
                    });

                    $scope.myAuctions = $scope.filterArray;
                }
                if ($scope.buyerFilter!=='ALL') {
                    $scope.filterArray = $scope.myAuctions.filter(function (item) {
                        return item.userName === $scope.buyerFilter;
                    });

                    $scope.myAuctions = $scope.filterArray;
                }

                if (str) {
                    
                    $scope.myAuctions = $scope.myAuctions.filter(function (req) {
                        return (String(req.requirementID).includes(str.toLowerCase())
                            || String(req.title.toLowerCase()).includes(str.toLowerCase())
                            || String(req.userName.toLowerCase()).includes(str.toLowerCase())
                            || String(req.projectName.toLowerCase()).includes(str.toLowerCase())
                            || String(req.projectDetails.toLowerCase()).includes(str.toLowerCase())
                            || String(req.postedOn).includes(str.toLowerCase())
                            || req.category[0].toLowerCase().includes(str.toLowerCase())
                            || (req.prNumbers ? req.prNumbers : '').includes(str.toLowerCase())
                            || (req.requirementNumber ? req.requirementNumber.toLowerCase() : '').includes(str.toLowerCase())

                        );

                    });
                    //$scope.myAuctions = $scope.filterArray;
                }

                $scope.totalItems = $scope.myAuctions.length;

            };

            $scope.getCategoryFilter = function (filterCategoryVal) {
                $scope.filterCatArray = [];
                if (filterCategoryVal == 'ALL CATEGORIES') {
                    $scope.myAuctions = $scope.myAuctions1;
                } else {
                    $scope.filterCatArray = $scope.tempCategoryFilter.filter(function (item) {
                        return item.category[0] == filterCategoryVal;
                    });
                    $scope.myAuctions = $scope.filterCatArray;

                }

                
            };


            $scope.GetFilteredReqs = function (fromDate,toDate,text) {
                $scope.filterReqArray = [];

                if (text == 'CLEAR') {
                    $scope.myAuctions = $scope.myAuctions1;
                } else {
                    $scope.forReqFilter.filter(function (req) {
                        req.postedOn = $scope.GetDateconverted(req.postedOn);
                        req.quotationFreezTime = $scope.GetDateconverted(req.quotationFreezTime);
                        req.expStartTime = $scope.GetDateconverted(req.expStartTime);
                        req.startTime = $scope.GetDateconverted(req.startTime);
                        var from = Date.parse(fromDate);
                        var to = Date.parse(toDate);
                        var check = Date.parse(req.postedOn);


                        if (fromDate <= req.postedOn && req.postedOn <= toDate) {
                            $scope.filterReqArray.push(req);
                        }
                       
                    });
                    $scope.myAuctions = $scope.filterReqArray;
                }
                $scope.totalItems = $scope.myAuctions.length;
            };

            $scope.loadBuyers = function () {
                $scope.buyers = [];
                $scope.buyers.push('ALL');
                $http({
                    method: 'GET',
                    url: domain + 'getPostedRFQUsers?compid=' + +$scope.compId + '&sessionid=' + userService.getUserToken() + '&fromDate=' + $scope.filterDate.reqFromDate + '&toDate=' + $scope.filterDate.reqToDate,
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        //if (response.data.length > 0) {
                        //    $scope.categories.push('ALL CATEGORIES');
                        //}
                        if (response.data && response.data.length > 0) {
                            response.data.forEach(function (item) {
                                $scope.buyers.push(item);
                            });
                        }

                        $scope.getAuctions(0, 10, $scope.searchKeyword);
                    }
                }, function (result) {

                });

            };

            $scope.loadBuyers();


            $scope.goToQCS = function (reqId,qcsId)
            {
                return userService.goToQCS(0, userService.getUserToken(), +qcsId);
            };
            $scope.tableColumns = [];
            $scope.downloadQCS = function ()
            {
                auctionsService.downloadQCS({ "compId": +$scope.compId, "sessionId": userService.getUserToken() })
                    .then(function (response) {
                        if (response)
                        {
                            var arr = JSON.parse(response).Table;
                            if (arr && arr.length > 0) {
                                $scope.totalItems = arr[0].TOTAL_COUNT;
                                arr.forEach(a => delete a.TOTAL_COUNT);
                                $scope.tableColumnsTemp = angular.copy(_.keys(arr[0]));
                                $scope.tableColumnsTemp.forEach(function (item, index) {
                                    item = item.replaceAll("_", " ");
                                    var foundIndex = _.findIndex($scope.tableColumns, function (val) { return val === item; });
                                    if (foundIndex <= -1) {
                                        $scope.tableColumns.push(item);
                                    }
                                });
                                $scope.rows = arr;
                                arr.forEach(function (item, index) {
                                    var obj = angular.copy(_.values(item));
                                    if (obj) {
                                        item.tableValues = [];
                                        obj.forEach(function (value, valueIndex) {
                                            item.tableValues.push(value);
                                        });
                                    }
                                });


                                $scope.excelRows = [];
                                $scope.excelColumnsTemp = [];
                                $scope.excelColumns = [];
                                $scope.rows.forEach(function (rowItem, rowIndex) {
                                    var newObj = Object.assign({}, rowItem);
                                    delete newObj.tableValues;
                                    $scope.excelColumnsTemp.push(newObj);
                                });
                                if ($scope.excelColumnsTemp && $scope.excelColumnsTemp.length > 0) {
                                    Object.keys($scope.excelColumnsTemp[0]).forEach(function (key, index) {
                                        var value = key + ' as [' + key.replaceAll("_", " ") + ']';
                                        $scope.excelColumns.push(value);
                                    });
                                    $scope.excelColumns = $scope.excelColumns.join(',');

                                    $scope.excelColumnsTemp.forEach(function (item, index) {
                                        $scope.excelRows.push(item);
                                    });
                                }
                                execute();

                            } else {
                                swal("Status!", "No Records Found..!", "error");
                            }
                        }
                    });
            };

            function execute() {
                var mystyle = {
                    headers: true,
                    column: { style: { Font: { Color: "#FFFFFF" }, Interior: { Color: "#4F81BD", Pattern: "Solid" }, Alignment: { Horizontal: "Center" } } },
                    row: { style: { Alignment: { Horizontal: "Center" } } }
                };
                alasql('SELECT ' + $scope.excelColumns + ' INTO XLSXML("Reporting.xls",?) FROM ?', [mystyle, $scope.excelRows]);
                $scope.downloadExcel = false;
            }

        }]);