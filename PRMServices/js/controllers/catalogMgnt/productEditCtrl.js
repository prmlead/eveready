﻿//(function () { 
prmApp.controller('productEditCtrl', ['$scope', '$state', '$window', '$stateParams', '$filter', 'auctionsService', 'catalogService', 'userService', 'growlService', 'fileReader','$location',
    function ($scope, $state, $window, $stateParams, $filter, auctionsService, catalogService, userService, growlService, fileReader,$location) {

        $scope.productId = $stateParams.productId == "" || !$stateParams.productId ? 0 : $stateParams.productId;

        $scope.viewDetails = $stateParams.viewId;

        $scope.currentLoggedInUser = userService.getUserId()
        $scope.compId = userService.getUserCompanyId();
        $scope.sessionid = userService.getUserToken();
        $scope.vendorsLoading = false;
        $scope.SelectedVendorsCount = 0;
        $scope.companyItemUnits = [];
        $scope.companyPlants = [];
        $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
        $scope.isSuperUser = userService.getUserObj().isSuperUser;
        //$scope.stateName = $state.current.name;
        $scope.totalAttachmentMaxSize = 6291456;
        $scope.totalRequirementSize = 0;
        $scope.totalRequirementItemSize = 0;

        //if ($scope.stateName == 'editContract') {
        //    $location.path = 'ProductContractInfo';
        //    getProductContracts();
        //}

        $scope.InitProdEdit = function () {
            $scope.checkProdEdit = 0;
            $scope.productEditObj = {
                prodId: $scope.productId,
                compId: $scope.compId,
                isValid: 0,
                ModifiedBy: userService.getUserId()
            };
            var param = {
                reqProduct: $scope.productEditObj,
                sessionID: userService.getUserToken()
            }

            catalogService.isProdEditAllowed(param)
                .then(function (response) {
                    //if (response.errorMessage != '') {
                    //      growlService.growl(response.errorMessage, "inverse");
                    //     $state.go("products");
                    //    }
                    //    else {
                    if ($scope.viewDetails == "view") {
                        $scope.checkProdEdit = -1;
                    } else {
                        $scope.checkProdEdit = response.repsonseId;
                    }
                    //$scope.getCompanyQtyItems();
                    $scope.getProductDetails();
                    $scope.getcategories();
                    //$scope.getProperties();
                    //  $scope.getSelectedProductVendors($scope.productId);
                    //   }
                });
        } 

        auctionsService.GetCompanyConfiguration($scope.compId, "ITEM_UNITS", userService.getUserToken())
            .then(function (unitResponse) {
                $scope.companyItemUnits = unitResponse;
            });

        $scope.InitProdEdit();
        $scope.VendorsTemp1 = [];
        $scope.vendorsList = [];
        $scope.vendorsListProduct = [];



        //$scope.selectedA = [];
        //$scope.selectedB = [];
        //$scope.productVendObj = {
        //    companyCheckedVendors: [],
        //    // prodVendTemp: []
        //};
        //$scope.GetCompanyVendors = function () {
        //    $scope.params =
        //        {
        //            "userID": userService.getUserId(),
        //            "sessionID": userService.getUserToken()
        //        }

        //    userService.GetCompanyVendors($scope.params)
        //        .then(function (response) {
        //            $scope.vendorsList = response;

        //            $scope.VendorsTemp1 = $scope.vendorsList;


        //            $scope.VendorsTemp = $scope.vendorsList;
        //            //$scope.searchVendors('');
        //        });
        //};

        //$scope.GetCompanyVendors();


        //$scope.searchvendorstring = '';

        //$scope.searchVendors = function (value) {
        //    value = String(value).toUpperCase();
        //    $scope.vendorsList = $scope.VendorsTemp1.filter(function (item) {
        //        return (String(item.companyName).toUpperCase().includes(value) == true ||
        //            String(item.vendorCode).toUpperCase().includes(value) == true);
        //    });

        //    $scope.totalItems = $scope.vendorsList.length;
        //}


        $scope.GetDateconverted = function (dateBefore) {
            if (dateBefore) {
                return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
            }
        };

        $scope.productDetails = null;
        $scope.getProductDetails = function () {
            catalogService.getproductbyid($scope.compId, $scope.productId)
                .then(function (response) {
                    console.log(response);
                    $scope.productDetails = {
                        prodId: response.prodId,
                        prodName: response.prodName,
                        prodCode: response.prodCode,
                        prodDesc: response.prodDesc,
                        prodHSNCode: response.prodHSNCode,
                        prodQty: response.prodQty,
                        prodNo: response.prodNo,
                        dateModified: $scope.GetDateconverted(response.dateModified)
                        // companyCheckedVendors1:[]
                    };
                    //catalogService.GetCompanyConfiguration($scope.compId, "ITEM_UNITS", userService.getUserToken())
                    //    .then(function (unitResponse) {
                            //$scope.companyItemUnits = unitResponse;
                            $scope.prodEditDetails = response;
                            $scope.prodEditDetails.contractManagement.forEach(function (item, index) {
                                item.startTime = userService.toLocalDate(item.startTime);
                                item.endTime = userService.toLocalDate(item.endTime);
                                item.isOld = 1;
                            });

                            $scope.prodEditDetails.multipleAttachments = [];
                            var temp = response.itemAttachments;
                            if (temp != "") {
                                var attchArray = temp.split(',');
                            }

                            if (attchArray) {
                                attchArray.forEach(function (att, index) {

                                    var fileUpload = {
                                        fileStream: [],
                                        fileName: '',
                                        fileID: att
                                    };


                                    $scope.prodEditDetails.multipleAttachments.push(fileUpload);
                                });
                            }

                            if ($scope.prodEditDetails.contractManagement.length > 0) {
                                $scope.prodEditDetails.contractManagement.forEach(function (item) {
                                    item.isDisabled = true;
                                });
                            }
                        //});
                });
        };

        $scope.editThis = function (index) {
            $scope.prodEditDetails.contractManagement.forEach(function (item, idx) {
                if (index == idx) {
                    item.isDisabled = false;
                }
            });
        }

        $scope.pushvendors = function () {
            var checkedvendorIDs = _($scope.vendorsList)
                .filter(vendor => vendor.isAssignedToProduct)
                .map('userID')
                .value();
            $scope.checkedVendors = checkedvendorIDs.join(',');
        };

        $scope.addCatalogue = function () {
            $window.scrollBy(0, 50);
            $scope.itemnumber = $scope.prodEditDetails.contractManagement.length;
            $scope.contractDetailsList =
                {
                    pcId: 0,
                    number: 0,
                    value: 0,
                    quantity: 0,
                    availedQuantity: 0,
                    startTime: '',
                    endTime: '',
                    U_ID: '',
                    companyName: '',
                    documents: '',
                    isValid: 1,
                     isOld: 0,
                    PLANT_CODE : '',
                    errorColorNum: '1px solid #e4e7ea',
                    errorColorVal: '1px solid #e4e7ea',
                    errorColorQty: '1px solid #e4e7ea',
                    errorColorSt: '1px solid #e4e7ea',
                    errorColorNme: '1px solid #e4e7ea'
                }
            $scope.prodEditDetails.contractManagement.push($scope.contractDetailsList);
        }

        $scope.contractIndexValue = 0;
        $scope.contractIndex = function (value) {
            $scope.contractIndexValue = value;
        }

        $scope.prodVendorSelected = function (userId, companyName) {
            $scope.prodEditDetails.contractManagement.forEach(function (item, index) {
                if (index == $scope.contractIndexValue) {
                    item.companyName = companyName;
                    item.U_ID = userId;
                }
            })
        }

        $scope.Attachements = [];
        $scope.onFileSelect = function ($files, $item, $modal) {

            var obj = {
                Field: $item.Name,
                Files: []
            }
            $scope.Attachements.push()
            for (var i in $files) {
                fileReader.readAsDataUrl($files[i], $scope)
                    .then(function (result) {
                        var bytearray = new Uint8Array(result);
                        var fileobj = {};
                        fileobj.fileStream = $.makeArray(bytearray);
                        fileobj.fileType = $files[i].type;
                        fileobj.name = $files[i].name
                        fileobj.isVerified = 0;
                        //$scope.verificationObj.attachmentName=$scope.file.name;
                        obj.Files.push(fileobj);
                    });
            }
            $scope.Attachements.push(obj)

        }

        //$scope.companyCheckedVendors = [];

        //$scope.getSelectedProductVendors = function (productID) {
        //    catalogService.getproductVendors(productID,userService.getUserToken())
        //        .then(function (response) {
        //            $scope.companyCheckedVendors = response;

        //         //   $scope.vendorsList1 = [];
        //         //   $scope.filterArray1 = [];
        //            //$scope.companyCheckedVendors.forEach(function (item, index) {
        //            //    var data = [];
        //            //    data = $scope.vendorsList.filter(function (item1) {
        //            //        return item1.userID != item.userID;
        //            //    });
        //            //    if (data && data.length > 0) {
        //            //        data.push($scope.vendorsList1);

        //            //    }
        //            //});
        //            //$scope.vendorsList = $scope.vendorsList1;
        //            //console.log("$scope.vendorsList>>>" + $scope.vendorsList);

        //            for (var j in $scope.companyCheckedVendors) {
        //                for (var i in $scope.vendorsList) {
        //                    if ($scope.vendorsList[i].userID == $scope.companyCheckedVendors[j].userID) {
        //                        $scope.vendorsList.splice(i, 1);
        //                    }
        //                }
        //            }


        //        });
        //};

        //// in edit
        //$scope.selectForA = function (item) {
        //    var index = $scope.selectedA.indexOf(item);
        //    if (index > -1) {
        //        $scope.selectedA.splice(index, 1);
        //    } else {
        //        $scope.selectedA.splice($scope.selectedA.length, 0, item);
        //    }
        //    for (i = 0; i < $scope.selectedA.length; i++) {
        //        $scope.companyCheckedVendors.push($scope.selectedA[i]);
        //        $scope.vendorsList.splice($scope.vendorsList.indexOf($scope.selectedA[i]), 1);
        //        $scope.VendorsTemp.splice($scope.VendorsTemp.indexOf($scope.selectedA[i]), 1);
        //    }
        //    $scope.reset();
        //}

        //$scope.selectForB = function (item) {
        //    var index = $scope.selectedB.indexOf(item);
        //    if (index > -1) {
        //        $scope.selectedB.splice(index, 1);
        //    } else {
        //        $scope.selectedB.splice($scope.selectedA.length, 0, item);
        //    }
        //    //  var data = [];
        //    for (i = 0; i < $scope.selectedB.length; i++) {
        //        $scope.vendorsList.push($scope.selectedB[i]);
        //        $scope.VendorsTemp.push($scope.selectedB[i]);
        //        $scope.companyCheckedVendors.splice($scope.companyCheckedVendors.indexOf($scope.selectedB[i]), 1);
        //    }
        //    $scope.reset();
        //}

        //$scope.reset = function () {
        //    $scope.selectedA = [];
        //    $scope.selectedB = [];
        //}

        $scope.getCompanyPlants = function () {
            auctionsService.getCompanyPlants($scope.compId, userService.getUserId(), $scope.sessionid)
                .then(function (response) {
                    $scope.companyPlants = response;
                });
        };

        $scope.getCompanyPlants();



        $scope.getCompanyQtyItems = function () {
            catalogService.GetCompanyConfiguration($scope.compId, "ITEM_UNITS", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyItemUnits = unitResponse;
                });
        };
        //$scope.getCompanyQtyItems();
        var catNodes = [];
        $scope.nodes = [];
        $scope.selectedNodesView = [];
        $scope.getcategories = function () {
            //catalogService.getcategories($scope.compId)
            catalogService.GetProductSubCategories($scope.productId, 0, $scope.compId)
                .then(function (response) {
                    // console.log(response);
                    //$scope.companyCatalog = response;

                    //$scope.companyCatalog.forEach(function (item, index) {
                    //    var cat = {

                    //        "compId": item.compId,
                    //        "parentID": item.catParentId,
                    //        "id": item.catId,
                    //        "title": item.catName,
                    //        "catdesc": item.catDesc,
                    //        "subCatCount": item.subCatCount,
                    //        "childCollapsed": true,
                    //        "nodeChecked": item.catSelected > 0 ? true : false,
                    //        "nodes": []
                    //    }
                    //    catNodes.push(cat);
                    //    $scope.nodes = catNodes;
                    //})
                    $scope.nodes = response;
                    $scope.nodes1 = $scope.nodes;
                    $scope.getSelectedNodes();
                    //$scope.collapseAll();
                });

        };

        $scope.selectedNodes = '0';
        $scope.selectedCategories = '';
        $scope.selectChildNodes = function (childNode, ischecked) {
            childNode.nodeChecked = ischecked;
            childNode.nodes.forEach(function (item, index) {
                $scope.selectChildNodes(item, ischecked);
            });
        };

        $scope.selectParentNode = function (ischecked, selectedNodeScope) {
            if (selectedNodeScope) {
                var parentScope = selectedNodeScope.$parent;
                if (parentScope && parentScope.node && parentScope.node.catId > 0) {
                    if (ischecked) {
                        parentScope.node.nodeChecked = ischecked;
                    }
                    else {
                        if (parentScope.node.nodes && parentScope.node.nodes.length > 0) {
                            if ($filter('filter')(parentScope.node.nodes, { 'nodeChecked': true }).length <= 0) {
                                parentScope.node.nodeChecked = ischecked;
                            }
                        }
                        else {
                            parentScope.node.nodeChecked = ischecked;
                        }
                    }

                    $scope.selectParentNode(ischecked, parentScope);
                }
            }
        }
        $scope.checkChanged = function (selectedNode, selectedNodeScope) {
            $scope.selectChildNodes(selectedNode, selectedNode.nodeChecked);
            $scope.selectParentNode(selectedNode.nodeChecked, selectedNodeScope);
            $scope.getSelectedNodes();
        }
        $scope.toggle = function (scope) {
            scope.toggle()

        };

        $scope.productErrorMessage = '';
        $scope.productCodeErrorMessage = '';
        $scope.productNumberErrorMessage = '';
        $scope.productDescErrorMessage = '';

        $scope.errorValidation = false;
        $scope.SaveProductDetails = function () {
            $scope.productErrorShow = $scope.errorValidation = false;
            //var Test = $scope.compId;
            $scope.productErrorMessage = $scope.productCodeErrorMessage = '';
            $scope.productObj = {
                prodId: $scope.productDetails.prodId,
                compId: $scope.compId,
                prodCode: $scope.prodEditDetails.prodCode,
                prodName: $scope.prodEditDetails.prodName,
                prodDesc: $scope.prodEditDetails.prodDesc,
                prodNo: $scope.prodEditDetails.prodNo,
                prodHSNCode: $scope.prodEditDetails.prodHSNCode,
                prodQty: $scope.prodEditDetails.prodQty,
                isValid: 1,
                prodAlternateUnits: $scope.prodEditDetails.prodAlternateUnits,
                unitConversion: $scope.prodEditDetails.unitConversion,
                shelfLife: $scope.prodEditDetails.shelfLife,
                productVolume: $scope.prodEditDetails.productVolume,
                ModifiedBy: userService.getUserId(),

                productGST: $scope.prodEditDetails.productGST,
                prefferedBrand: $scope.prodEditDetails.prefferedBrand,
                alternateBrand: $scope.prodEditDetails.alternateBrand,
                totPurchaseQty: $scope.prodEditDetails.totPurchaseQty,
                inTransit: $scope.prodEditDetails.inTransit,
                leadTime: $scope.prodEditDetails.leadTime,
                departments: $scope.prodEditDetails.departments,
                deliveryTerms: $scope.prodEditDetails.deliveryTerms,
                termsConditions: $scope.prodEditDetails.termsConditions,
                casNumber: $scope.prodEditDetails.casNumber,
                mfcdCode: $scope.prodEditDetails.mfcdCode,

                contractManagement: $scope.prodEditDetails.contractManagement,
                multipleAttachments: $scope.prodEditDetails.multipleAttachments,

                listVendorDetails: []
            };


            $scope.productObj.contractManagement.forEach(function (item, index) {
                if (item.isValid == 1) {
                    if (!item.number) {
                        item.errorColorNum = '1px solid red';
                        $scope.errorValidation = true;
                    }
                    if (!item.value) {
                        item.errorColorVal = '1px solid red';
                        $scope.errorValidation = true;
                    }
                    if (!item.quantity) {
                        item.errorColorQty = '1px solid red';
                        $scope.errorValidation = true;
                    }
                    if (!item.startTime) {
                        item.errorColorSt = '1px solid red';
                        $scope.errorValidation = true;
                    }
                    if (!item.endTime) {
                        item.errorColorSe = '1px solid red';
                        $scope.errorValidation = true;
                    }
                    if (!item.companyName) {
                        item.errorColorNme = '1px solid red';
                        $scope.errorValidation = true;
                    }
                } else {
                    if (item.isValid == 0 && item.isOld == 0) {
                        $scope.productObj.contractManagement.splice(index, 1);
                    }
                }
                if (item.startTime) {
                    let ts = userService.toUTCTicks(item.startTime);
                    let m = moment(ts);
                    let contractStartDate = new Date(m);
                    let milliseconds = parseInt(contractStartDate.getTime() / 1000.0);
                    item.startTime = "/Date(" + milliseconds + "000+0530)/";
                } else {
                    item.startTime = null;
                }
                if (item.endTime) {
                    let ts = userService.toUTCTicks(item.endTime);
                    let m = moment(ts);
                    let contractEndDate = new Date(m);
                    let milliseconds = parseInt(contractEndDate.getTime() / 1000.0);
                    item.endTime = "/Date(" + milliseconds + "000+0530)/";
                } else {
                    item.endTime = null;
                }

            });

            if ($scope.productObj.prodName == null || $scope.productObj.prodName == undefined || $scope.productObj.prodName == '') {
                $scope.productErrorShow = true;
                $scope.productNumberErrorMessage = "Please Enter Product Name.";
                return;
            }

            if ($scope.productObj.prodNo == null || $scope.productObj.prodNo == undefined || $scope.productObj.prodNo == '') {
                $scope.productErrorShow = true;
                $scope.productNumberErrorMessage = "Please Enter Product Number.";
                return;
            }
            if ($scope.productObj.prodDesc == null || $scope.productObj.prodDesc == undefined || $scope.productObj.prodDesc == '') {
                $scope.productErrorShow = true;
                $scope.productDescErrorMessage = "Please Enter Product Description.";
                return;
            }

            //if ($scope.productObj.prodCode == null || $scope.productObj.prodCode == undefined || $scope.productObj.prodCode == '') {
            //    $scope.productErrorShow = true;
            //    $scope.productCodeErrorMessage = "please enter Product Code";
            //    return;
            //}

            if ($scope.errorValidation == true) {
                return false;
            }

            $scope.searchvendorstring = '';
            //$scope.searchVendors('');

            var selectedVendors = $scope.vendorsList.filter(function (vendor) {
                return (vendor.isAssignedToProduct == true || vendor.isAssignedToProduct > 0);
            });


            $scope.productObj.listVendorDetails = selectedVendors;

            //  $scope.productObj.listVendorDetails = $scope.companyCheckedVendors;
            var param = {
                reqProduct: $scope.productObj,
                sessionID: userService.getUserToken()
            }
            
            catalogService.updateProductDetails(param)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        $scope.SaveProductCategories();
                        growlService.growl("product updated Successfully.", "success");
                        //$scope.getProductDetails();
                        //$scope.GetProductVendors();
                        location.reload();
                    }
                });

        }

        $scope.validateMatry = function () {
            $scope.prodEditDetails.contractManagement.forEach(function (item, index) {

                if (item.number) {
                    item.errorColorNum = '1px solid #e4e7ea';
                }
                if (item.value) {
                    item.errorColorVal = '1px solid #e4e7ea';
                }
                if (item.quantity) {
                    item.errorColorQty = '1px solid #e4e7ea';
                }
                if (item.startTime) {
                    item.errorColorSt = '1px solid #e4e7ea';
                }
                if (item.endTime) {
                    item.errorColorSe = '1px solid #e4e7ea';
                }
                if (item.companyName) {
                    item.errorColorNme = '1px solid #e4e7ea';
                }
            });
        }

        $scope.deleteContract = function (val) {
            $scope.prodEditDetails.contractManagement.forEach(function (obj, index) {
                if (index == val) {
                    obj.isValid = 0;
                }
            })
        }

        $scope.deleteProduct = function (prod) {

            swal({
                title: "Are You Sure!",
                text: 'Do You want to Delete the Item Permanently',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function () {
                $scope.productdelObj = {
                    prodId: prod.prodId,
                    compId: $scope.compId,
                    prodCode: prod.prodCode,
                    prodName: prod.prodName,
                    isValid: 0,
                    ModifiedBy: userService.getUserId()
                };
                var param = {
                    reqProduct: $scope.productdelObj,
                    sessionID: userService.getUserToken()
                }

                catalogService.deleteProduct(param)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("product has been deleted.", "success");
                            $scope.goToProducts();
                        }
                    });
            });



        }

        $(document).ready(function () {
            //$('select').selectize({
            //    sortField: 'text'
            //});
        });

        $scope.InactiveProduct = function (prod) {
            swal({
                title: "Are You Sure!",
                text: 'Do You want to Inactivate the Item',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: true
            }, function () {
                $scope.productdelObj = {
                    prodId: prod.prodId,
                    compId: $scope.compId,
                    prodCode: prod.prodCode,
                    prodName: prod.prodName,
                    isValid: 2,
                    ModifiedBy: userService.getUserId()
                };
                var param = {
                    reqProduct: $scope.productdelObj,
                    sessionID: userService.getUserToken()
                }

                catalogService.deleteProduct(param)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("product has been Inactivated.", "success");
                            $scope.goToProducts();
                        }
                    });
            });

        }


        $scope.goToProducts = function () {
            //var url = $state.href("productEdit", { "productId": prodId });
            $state.go("products");
            //window.open(url, '_blank');
        };

        $scope.SaveProductCategories = function () {
            var param = {
                prodId: $scope.productId,
                compId: $scope.compId,
                catIds: $scope.selectedNodes,
                user: userService.getUserId(),
                sessionId: userService.getUserToken()
            }

            catalogService.updateProductCategories(param)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("product details saved Successfully.", "success");
                        $scope.getProductDetails();
                    }
                });

        }

        $scope.getProperties = function () {
            $scope.Properties = [];
            catalogService.getProperties($scope.compId, $scope.productId, 0)
                .then(function (response) {

                    $scope.PropertiesRaw = response;

                    $scope.PropertiesRaw.forEach(function (item, index) {
                        var Property = {
                            "propId": item.propId,
                            "propName": item.propName,
                            "propDesc": item.propDesc,
                            "propDataType": item.propDataType,
                            "propChecked": item.propValue == null ? false : true,
                            "propOptions": item.propOptions,
                            "propValue": item.propDataType == "multi" ? (item.propValue == null ? item.propValue : item.propValue.split("^^")) : item.propValue,
                            "propIsValid": item.isValid,
                            "propModified": false
                        }
                        $scope.Properties.push(Property);

                    })

                    //$scope.collapseAll();
                });
            /*
            $scope.Attributes.push({ attrName: 'test Text', attrType: 'Short Text', attrId: 123, attrDefault: '', attrChecked: false, attrOptions: '', attrValue:'' });
            $scope.Attributes.push({ attrName: 'test Paragraph', attrType: 'Paragraph', attrId: 234, attrDefault: '', attrChecked: false, attrOptions: '', attrValue:'' });
            $scope.Attributes.push({ attrName: 'test dropdown', attrType: 'Dropdown', attrId: 235, attrDefault: '', attrChecked: false, attrOptions: 't1$$t2$$t3$$t4', attrValue:'' });
            $scope.Attributes.push({ attrName: 'test Multiselect', attrType: 'Multiselect', attrId: 235, attrDefault: '', attrChecked: false, attrOptions: 'p1$$p2$$p3$$p4', attrValue:'' });
            */
        }

        $scope.propCheckChanged = function (selectedProp) {
            selectedProp.propModified = true;
            //selectedProp.propChecked = !selectedProp.propChecked;
            selectedProp.propIsValid = selectedProp.propChecked
        }

        $scope.propModified = function (selectedProp) {
            selectedProp.propModified = true;
            selectedProp.propIsValid = selectedProp.propChecked
        }
        $scope.saveProperties = function () {
            $scope.propertyobj = [];
            var callsave = false;
            $scope.Properties.forEach(function (item, index) {
                if (item.propChecked || item.propModified) {
                    callsave = true;
                    $scope.propertyobj.push({
                        entityId: $scope.productId,
                        companyId: $scope.compId,
                        propId: item.propId,
                        propValue: item.propValue != null ? (angular.isArray(item.propValue) ? item.propValue.join("^^") : item.propValue) : "",
                        isValid: item.propIsValid ? 1 : 0,
                        user: userService.getUserId()
                    });

                }
            })
            if (callsave) {
                var params = {
                    propertyobj: $scope.propertyobj,
                    sessionId: userService.getUserToken()
                };
                catalogService.saveEntityProperties(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("product attributes updated successfully.", "success");
                            $scope.getProductDetails();
                        }
                    });
            }

        }

        $scope.getSelectedNodes = function () {
            $scope.selectedCategories = [];
            $scope.selectedNodes = '0';

            $scope.nodes.forEach(function (item, index) {
                $scope.getSelectedNodesIteration(item, $scope.selectedCategories);
            })
        }

        $scope.getSelectedNodesIteration = function (selectedNode, selectedCat) {
            var selectedItem = { "catName": selectedNode.catName, "nodes": [] };
            if (selectedNode.nodeChecked) {

                $scope.selectedNodes += ',' + selectedNode.catId;

                if (selectedNode.nodes != null) {
                    selectedNode.nodes.forEach(function (item, index) {
                        if (item.nodeChecked) {
                            $scope.getSelectedNodesIteration(item, selectedItem.nodes);
                        }

                    })
                }
                selectedCat.push(selectedItem);
            }

        }

        $scope.multipleitemsAttachments = [];

        $scope.getFile = function () {
            $scope.progress = 0;

            //$scope.file = $("#attachement")[0].files[0];
            $scope.multipleitemsAttachments = $("#attachement")[0] ? $("#attachement")[0].files : null;
            $scope.contractAttachments = $("#contractAttachement")[0] ? $("#contractAttachement")[0].files : null;


            if ($scope.contractAttachments)
            {
                $scope.contractAttachments = Object.values($scope.contractAttachments);

                if ($scope.contractAttachments && $scope.contractAttachments.length > 0) {
                    $scope.contractAttachments.forEach(function (item, index) {
                        $scope.totalRequirementSize = $scope.totalRequirementSize + item.size;
                    });
                }

                if (($scope.totalRequirementSize + $scope.totalRequirementItemSize) > $scope.totalAttachmentMaxSize) {
                    swal({
                        title: "Attachment size!",
                        text: "Total Attachments size cannot exceed 6MB",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            return;
                        });
                    return;
                }


                $scope.contractAttachments.forEach(function (item, index) {

                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };

                            var bytearray = new Uint8Array(result);

                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;

                            if (!$scope.selectedProductContract.multipleAttachments) {
                                $scope.selectedProductContract.multipleAttachments = [];
                            }

                            var ifExists1 = _.findIndex($scope.selectedProductContract.multipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase(); });
                            if (ifExists1 < 0) {
                                $scope.selectedProductContract.multipleAttachments.push(fileUpload);
                            }

                        });
                });


            }

            if ($scope.multipleitemsAttachments)
            {
                $scope.multipleitemsAttachments = Object.values($scope.multipleitemsAttachments);

                $scope.multipleitemsAttachments.forEach(function (item, index) {

                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };

                            var bytearray = new Uint8Array(result);

                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;

                            if (!$scope.prodEditDetails.multipleAttachments) {
                                $scope.prodEditDetails.multipleAttachments = [];
                            }

                            $scope.prodEditDetails.multipleAttachments.push(fileUpload);

                        });
                });

            }
        };

        $scope.ProductVendors = [];
        $scope.count = 0;
        $scope.GetProductVendors = function () {
            $scope.params = { "userID": userService.getUserId(), "sessionID": userService.getUserToken() }
            catalogService.getproductVendors($scope.productId, userService.getUserToken())
                .then(function (response) {
                    $scope.ProductVendors = response;

                    $scope.vendorsList.forEach(function (vlItem, vlIndex) {
                        $scope.ProductVendors.forEach(function (plItem, plIIndex) {
                            //if (vlItem.userID == plItem.userID) {
                            //    vlItem.isAssignedToProduct = true;
                            //} else {
                            //    vlItem.isAssignedToProduct = false;
                            //}
                            if (vlItem.userID == plItem.userID) {
                                //$scope.count++;
                                //alert("1111111>>>>" + $scope.count);
                                vlItem.isAssignedToProduct = true;
                            }
                        })
                    })


                });
        };


        $scope.searchvendorstring = '';
        $scope.totalItems1 = -1;
        $scope.searchVendors = function (value) {

            if (value) {
                value = String(value).toUpperCase();
                $scope.searchingVendors = value;
                $scope.vendorsLoading = true;
                var output = [];
                output = $scope.GetCompanyVendors(0, $scope.searchingVendors);
            } else {
                $scope.searchingVendors = '';
                $scope.GetCompanyVendors(0, $scope.searchingVendors);
            }
        };

        $scope.searchvendorprodstring = '';
        $scope.totalItemsProd1 = -1;
        $scope.searchVendorsProd = function (value) {
            value = String(value).toUpperCase();
            $scope.vendorsListProduct = $scope.VendorsTemp1.filter(function (item) {
                return (String(item.companyName).toUpperCase().includes(value) == true); //  || String(item.vendorCode).toUpperCase().includes(value) == true
            });
            $scope.totalItemsProd1 = $scope.vendorsListProduct.length;
            $scope.totalItemsProd = $scope.vendorsListProduct.length;
        };


        // Pagination For Overall Vendors//
        $scope.page = 0;
        $scope.PageSize = 5000;
        $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
        $scope.totalCount = 0;
        // Pagination For Overall Vendors//

        $scope.VENDOR_CODE = "ALL";
        $scope.GetCompanyVendors = function (IsPaging, searchString) {
            $scope.deactiveParams = 0;
            if (searchString) {
                $scope.fetchRecordsFrom = 0;
            }

            $scope.vendorsLoading = true;
            $scope.params = { "userID": userService.getUserId(), "sessionID": userService.getUserToken(), "PageSize": $scope.fetchRecordsFrom, "NumberOfRecords": $scope.PageSize, "searchString": searchString ? searchString : "", "VENDOR_CODE": $scope.VENDOR_CODE, "DEACTIVE_PARAM": $scope.deactiveParams };
            if (IsPaging === 1) {

                if ($scope.page > -1) {
                    $scope.page++;
                    $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
                    $scope.params.PageSize = $scope.fetchRecordsFrom;
                    $scope.params.searchString = searchString ? searchString : "";
                }

                if ($scope.totalCount != $scope.vendorsList.length) {
                    userService.GetCompanyVendors($scope.params)
                        .then(function (response) {
                            if (response) {
                                for (var a in response) {
                                    $scope.vendorsList.push(response[+a]);
                                }
                                $scope.checkVendors($scope.vendorsList);
                                //$scope.vendorDisplayCollecton = [].concat($scope.vendorsList);
                                $scope.vendorsListProduct = $scope.vendorsList;
                                $scope.vendorsLoading = false;
                                $scope.totalCount = $scope.vendorsList[0].totalVendors;
                            }
                            $scope.VendorsTemp1 = $scope.vendorsList;
                        });
                }
            } else {
                userService.GetCompanyVendors($scope.params)
                    .then(function (response) {
                        $scope.vendorsList = response;
                        $scope.vendorsLoading = false;
                        $scope.vendorsListProduct = response;
                        $scope.VendorsTemp1 = $scope.vendorsList;
                        $scope.checkVendors($scope.vendorsList);
                    });
            }
        };

        $scope.GetCompanyVendors();


        //#region 

        $scope.ProductQuotationTemplate = [];

        $scope.hasSpecError = false;

        $scope.SaveProductQuotationTemplate = function (obj) {

            if (obj.HAS_SPECIFICATION) {
                obj.NAME = obj.NAME.replace(/\'/gi, "");
                obj.NAME = obj.NAME.replace(/\"/gi, "");
                obj.NAME = obj.NAME.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                obj.NAME = obj.NAME.replace(/(\r\n|\n|\r)/gm, "");
                obj.NAME = obj.NAME.replace(/\t/g, '');


                obj.DESCRIPTION = obj.DESCRIPTION.replace(/\'/gi, "");
                obj.DESCRIPTION = obj.DESCRIPTION.replace(/\"/gi, "");
                obj.DESCRIPTION = obj.DESCRIPTION.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                obj.DESCRIPTION = obj.DESCRIPTION.replace(/(\r\n|\n|\r)/gm, "");
                obj.DESCRIPTION = obj.DESCRIPTION.replace(/\t/g, '');
            }


            $scope.hasSpecError = false;
            if (obj.HAS_SPECIFICATION == 0) {
                $scope.hasSpecError = true;
                growlService.growl("Please Check Has Specification.", "inverse");
                return false;
            }

            var sameNameError = false;
            $scope.ProductQuotationTemplate.forEach(function (item, itemIndex) {
                if (obj.NAME.toUpperCase() == item.NAME.toUpperCase() && obj.T_ID == 0 && item.IS_VALID !== -1) {
                    sameNameError = true;
                }
            })

            if (sameNameError) { growlService.growl("Same name Error.", "inverse"); return false; }

            var params = {
                "productquotationtemplate": obj,
                "sessionid": userService.getUserToken()
            };

            catalogService.SaveProductQuotationTemplate(params)
                .then(function (response) {

                    if (response) {
                        $scope.GetProductQuotationTemplate();
                        growlService.growl("Saved Successfully.", "success");
                        $scope.resetTemplateObj();
                    }
                });
        };

        $scope.deleteProductQuotationTemplate = function (obj) {
            obj.IS_VALID = -1;
            var params = {
                "productquotationtemplate": obj,
                "sessionid": userService.getUserToken()
            };

            catalogService.SaveProductQuotationTemplate(params)
                .then(function (response) {
                    if (response) {
                        growlService.growl("Deleted Successfully.", "success");
                        $scope.resetTemplateObj();
                        if (obj.PRODUCT_ID > 0 && obj.index >= 0) {
                            $scope.GetProductQuotationTemplate(obj.PRODUCT_ID, obj.index, true);
                        }
                    }
                });
        };

        $scope.GetProductQuotationTemplate = function () {

            var params = {
                "catitemid": $scope.productId,
                "sessionid": userService.getUserToken()
            };

            catalogService.GetProductQuotationTemplate(params)
                .then(function (response) {

                    if (response) {
                        $scope.ProductQuotationTemplate = response;
                    }
                });
        };

        $scope.GetProductQuotationTemplate();


        $scope.resetTemplateObj = function () {
            $scope.QuotationTemplateObj = {
                T_ID: 0,
                PRODUCT_ID: $scope.productId,
                NAME: '',
                DESCRIPTION: '',
                HAS_SPECIFICATION: 0,
                HAS_PRICE: 0,
                HAS_QUANTITY: 0,
                CONSUMPTION: 1,
                UOM: '',
                HAS_TAX: 0,
                IS_VALID: 1,
                U_ID: userService.getUserId()
            };
        };

        $scope.resetTemplateObj();
        //#endregion


        $scope.productAlalysis = function (productId) {
            var url = $state.href("productAlalysis", { "productId": productId });
            window.open(url, '_blank');
        };

        $scope.checkVendors = function (vendorsArray) {
            vendorsArray.forEach(function (item, index) {
                if (item.IS_SELECTED === 1) {
                    item.isAssignedToProduct = true;
                }
            });
        };

        $scope.searchCategories = function (searchKeyword) {
            //$scope.filterArray = [];
            $scope.nodes = $scope.nodes1;
            if ($scope.searchCategories) {
                $scope.nodes = $scope.nodes.filter(function (node) {
                    return node.catName.toLowerCase().indexOf(searchKeyword.toLowerCase()) > -1;
                });

            }
        };




        $scope.selectedProductContract = {
            isDisabled: false,
            isNew: true
        };
        $scope.prodEditDetails = {};
        $scope.prodEditDetails.contractManagement = [];

        $scope.showContractForm = false;


        $scope.getVendorCodes = function (vendor) {
            if (!vendor.vendorCodeList || vendor.vendorCodeList.length <= 0) {
                let params = {
                    'uid': vendor.vendorId,
                    'compid': $scope.compId,
                    'sessionid': $scope.sessionid
                };

                auctionsService.getVendorCodes(params)
                    .then(function (response) {
                        vendor.vendorCodeList = response;
                        if (vendor.vendorCodeList && vendor.vendorCodeList.length > 0 && !$scope.selectedProductContract.selectedVendorCode) {
                            vendor.selectedVendorCode = vendor.vendorCodeList[0].VENDOR_CODE;
                            $scope.selectedProductContract.selectedVendorCode = vendor.vendorCodeList[0].VENDOR_CODE;
                        }
                    });
            } else {
                if (vendor.vendorCodeList && vendor.vendorCodeList.length > 0 && !$scope.selectedProductContract.selectedVendorCode) {
                    vendor.selectedVendorCode = vendor.selectedVendorCode ? vendor.selectedVendorCode : vendor.vendorCodeList[0].VENDOR_CODE;
                    $scope.selectedProductContract.selectedVendorCode = vendor.vendorCodeList[0].VENDOR_CODE;
                }
            }
        };

        $scope.editProductCotractInfo = function (contract) {
            $scope.selectedProductContract = contract;
            $scope.selectedProductContract.isNew = false;

            let vendor = {
                vendorId: contract.U_ID,
                selectedVendorCode: contract.selectedVendorCode
            };


            $scope.selectedProductContract.vendor = vendor;
            $scope.getVendorCodes(vendor);
            $scope.showContractForm = true;
        };


        $scope.deleteProductCotractInfo = function (contract) {
            contract.isValid = 0;
            contract.startTime = null;
            contract.endTime = null;
            contract.sessionId = $scope.sessionid;
            var params = {
                "contract": contract
            };

            catalogService.SaveProductContract(params)
                .then(function (response) {
                    if (response) {
                        growlService.growl("Deleted Successfully.", "success");
                        $scope.selectedProductContract = {
                            isDisabled: false,
                            isNew: true,
                            contractStatus : 'Active'
                        };
                        $scope.getProductContracts();
                    }
                });
        };

        $scope.addProductContractInfo = function () {
            $scope.getProductContracts();
            $scope.showContractSaveValidationError = '';
            $scope.selectedProductContract = {
                isDisabled: false,
                isNew: true
            };
            $scope.showContractForm = true;
        };

        $scope.SaveProductContractInfo = function () {
            $scope.showContractSaveValidationError = '';
            if (!($scope.selectedProductContract.value &&
                $scope.selectedProductContract.startTime && $scope.selectedProductContract.endTime && $scope.selectedProductContract.companyName
                && $scope.selectedProductContract.selectedVendorCode && $scope.selectedProductContract.quantity && $scope.selectedProductContract.PLANT_CODE)) {
                $scope.showContractSaveValidationError = 'Please fill in required fields marked *';
            } else {
                let startDate = userService.toUTCTicks($scope.selectedProductContract.startTime);
                let startDateMoment = moment(startDate);

                let endDate = userService.toUTCTicks($scope.selectedProductContract.endTime);
                let endDateMoment = moment(endDate);
                if (endDate < startDate) {
                    $scope.showContractSaveValidationError = 'Start Date cannot be Greater than End Date.';
                }
            }

            if (+$scope.selectedProductContract.quantity < +$scope.selectedProductContract.availedQuantity) {
                $scope.showContractSaveValidationError = 'Availed quantity cannot be greater than quantity.';
            }

            if (!$scope.showContractSaveValidationError) {
                if ($scope.selectedProductContract.startTime) {
                    let ts = userService.toUTCTicks($scope.selectedProductContract.startTime);
                    let m = moment(ts);
                    let contractStartDate = new Date(m);
                    let milliseconds = parseInt(contractStartDate.getTime() / 1000.0);
                    $scope.selectedProductContract.startTime = "/Date(" + milliseconds + "000+0530)/";
                } else {
                    $scope.selectedProductContract.startTime = null;
                }

                if ($scope.selectedProductContract.endTime) {
                    let ts = userService.toUTCTicks($scope.selectedProductContract.endTime);
                    let m = moment(ts);
                    let contractEndDate = new Date(m);
                    let milliseconds = parseInt(contractEndDate.getTime() / 1000.0);
                    $scope.selectedProductContract.endTime = "/Date(" + milliseconds + "000+0530)/";
                } else {
                    $scope.selectedProductContract.endTime = null;
                }

                $scope.selectedProductContract.ProductId = $scope.productId;
                $scope.selectedProductContract.vendorCode = $scope.selectedProductContract.selectedVendorCode;
                $scope.selectedProductContract.plantCode = ''//$scope.selectedProductContract.plantObj.id;
                $scope.selectedProductContract.plantName = '';//$scope.selectedProductContract.plantObj.name;
                $scope.selectedProductContract.USER = $scope.currentLoggedInUser;
                $scope.selectedProductContract.isValid = 1;
                $scope.selectedProductContract.sessionId = $scope.sessionid;
                $scope.isValid = 1;
                var params = {
                    "contract": $scope.selectedProductContract
                };

                catalogService.SaveProductContract(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.selectedProductContract = {
                                isDisabled: false,
                                isNew: true,
                                contractStatus: 'Active'
                            };
                            $scope.getProductContracts();
                        } else {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    });
            }
        };

        $scope.getProductContracts = function (isClicked) {
            if ($scope.productId) {
                $scope.prodEditDetails.contractManagement = [];
                catalogService.GetProductContracts($scope.productId,-1)
                    .then(function (response) {
                        if (response) {
                            $scope.prodEditDetails.contractManagement = _.filter(response, function (contractitem) {
                                return contractitem.contractStatus === 'Active';
                            });
                            if ($scope.prodEditDetails.contractManagement && $scope.prodEditDetails.contractManagement.length > 0) {
                                $scope.prodEditDetails.contractManagement.forEach(function (contract) {
                                    contract.multipleAttachments = [];
                                    if (contract.document) {
                                        contract.multipleAttachments = JSON.parse(contract.document);
                                    }

                                    contract.startTime = userService.toLocalDate(contract.startTime);
                                    contract.endTime = userService.toLocalDate(contract.endTime);
                                });
                            }
                        }

                        if (isClicked)
                        {
                            $scope.GetCompanyVendors();
                        }
                    });
            }
        };

        $scope.prodContractVendorSelected = function (vendor) {
            $scope.selectedProductContract.selectedVendorCode = '';
            $scope.selectedProductContract.companyName = vendor.companyName;
            $scope.selectedProductContract.vendorId = vendor.userID;
            $scope.selectedProductContract.vendor = vendor;
            $scope.selectedProductContract.vendor.vendorId = vendor.userID;
            $scope.getVendorCodes($scope.selectedProductContract.vendor);
        };

        $scope.downloadContractTemplate = function () {
            catalogService.downloadContractTemplate($scope.productId);
        };


        $scope.getFile1 = function (id, itemid, ext) {
            $scope.file = $("#" + id)[0].files[0];
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    if (id === "productContractAttachment") {
                        var bytearray = new Uint8Array(result);

                        var params = {
                            productid: +$scope.productId,
                            compid: $scope.compId,
                            user: userService.getUserId(),
                            sessionid: userService.getUserToken(),
                            attachment: $.makeArray(bytearray)
                        };

                        catalogService.UploadContractsFromExcel(params)
                            .then(function (response) {
                                if (response.errorMessage.contains("Total rows processed")) {
                                    swal({
                                        title: "Thanks!",
                                        text: "Contracts updated successfully",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            location.reload();
                                        });
                                } else {
                                    $scope.productContractUploadStatus = 'Refresh page to upload fresh excel.';
                                    $scope.productContractUploadStatus = $scope.productContractUploadStatus + '<br/><br/>' + response.errorMessage;
                                    swal("Warning", "Please check on main screen for more details.", 'warning');
                                }
                            });
                    }
                });
        };

        $scope.calculateNetPrice = function (contract) {
            if (!contract.value) {
                contract.value = 0;
            }

            if (!contract.DISCOUNT) {
                contract.DISCOUNT = 0;
            }

            contract.NET_PRICE = (+contract.value) * (1 - ((+contract.DISCOUNT) / 100));
        };

        $scope.calculateAllocation = function (currentContract)
        {
            $scope.showContractSaveValidationError = '';
            if (+currentContract.allocationPercentage > 0)
            {
                if (currentContract.allocationPercentage > 100)
                {
                    $scope.showContractSaveValidationError = 'Allocation Percentage Cannot Be Greater Than 100%.';
                    currentContract.allocationPercentage = 0;
                    return;
                }
                var validContracts = _.filter($scope.prodEditDetails.contractManagement, function (x) { return x.PC_ID != currentContract.PC_ID && x.contractStatus === 'Active' });
                if (validContracts && validContracts.length > 0)
                {
                    var totalAllocationPercentage = 0;
                    //_.sumBy(validContracts, 'allocationPercentage');
                    validContracts.forEach(function (contractItem,contractIndex) {
                        totalAllocationPercentage += +contractItem.allocationPercentage;
                    });

                    if (totalAllocationPercentage > 0 && (totalAllocationPercentage + (+currentContract.allocationPercentage) > 100)) {
                        $scope.showContractSaveValidationError = 'Sum Of All The Valid Contracts Cannot Be Greater Than 100%';
                        currentContract.allocationPercentage = 0;
                        return;
                    }

                    //if (totalAllocationPercentage > 0 && (+currentContract.allocationPercentage > totalAllocationPercentage)) {
                    //    $scope.showContractSaveValidationError = 'Allocation Percentage Cannot Be Greater Than Sum Of All The Valid Contracts.';
                    //    currentContract.allocationPercentage = 0;
                    //    return;
                    //}
                }
            }
        };

        $scope.removeAttach = function (index) {
            $scope.selectedProductContract.multipleAttachments.splice(index, 1);
        }


        $scope.CancelProductContractInfo = function () {
            $state.reload();
        }

    }]);
