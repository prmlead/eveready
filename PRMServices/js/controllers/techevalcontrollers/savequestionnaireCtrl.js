﻿prmApp
    .controller('savequestionnaireCtrl', ["$scope", "$stateParams", "$state", "$window", "$log", "$filter", "$http", "domain", "userService", "growlService", "techevalService",
        function ($scope, $stateParams, $state, $window, $log, $filter, $http, domain, userService, growlService, techevalService) {
            $scope.companyID = userService.getUserCompanyId();
            $scope.sessionID = userService.getUserToken();
            $scope.evalID = $stateParams.questionnaireID == "" || $stateParams.questionnaireID == null ? 0 : $stateParams.questionnaireID;
            $scope.Vendors = [];
            $scope.selectedA = [];
            $scope.selectedB = [];
            $scope.formRequest = {
                auctionVendors: []
            }
            $scope.categories = [];
            $scope.categoriesdata = [];
            $scope.selectedSubcategories = [];
            $scope.showCategoryDropdown = true;
            $scope.subcategories = [];
            $scope.questionnaire = {
                evalID: 0,
                reqID: 0,
                createdBy: userService.getUserId(),
                title: '',
                description: '',
                startDate: '',
                endDate: '',
                questions: []
            };

            $scope.sub = {
                selectedSubcategories: []
            };

            $scope.changeCategory = function () {
                $scope.questionnaire.vendors = [];
                $scope.loadSubCategories();
                //$scope.GetVendorsForTechEval();
                $scope.GetCompanyVendors();
            }


            $scope.selectSubcat = function (subcat) {
                if ($scope.evalID > 0) {
                    $scope.questionnaire.vendors = [];
                }
                $scope.vendorsLoaded = false;
                var category = [];
                var count = 0;
                var succategory = "";
                $scope.sub.selectedSubcategories = $filter('filter')($scope.subcategories, { ticked: true });
                selectedcount = $scope.sub.selectedSubcategories.length;
                if (selectedcount > 0) {
                    succategory = _.map($scope.sub.selectedSubcategories, 'id');
                    category.push(succategory);
                    //console.log(category);
                    //$scope.formRequest.category = category;
                    if ($scope.formRequest.category != undefined) {
                        var params = { 'Categories': succategory, 'sessionID': userService.getUserToken(), 'count': selectedcount, 'uID': userService.getUserId() };
                        $http({
                            method: 'POST',
                            url: domain + 'getvendorsbycatnsubcat',
                            encodeURI: true,
                            headers: { 'Content-Type': 'application/json' },
                            data: params
                        }).then(function (response) {
                            if (response && response.data) {
                                if (response.data.length > 0) {
                                    $scope.Vendors = response.data;
                                    $scope.vendorsLoaded = true;
                                    for (var j in $scope.questionnaire.vendors) {
                                        for (var i in $scope.Vendors) {
                                            if ($scope.Vendors[i].vendorName == $scope.questionnaire.vendors[j].vendorName) {
                                                $scope.Vendors.splice(i, 1);
                                            }
                                        }
                                    }
                                }
                                //$scope.questionnaire.vendors =[];
                            } else {
                                //console.log(response.data[0].errorMessage);
                            }
                        }, function (result) {
                            //console.log("there is no current auctions");
                        });
                    }
                } else {
                    $scope.GetCompanyVendors();
                }

            }

            $scope.loadSubCategories = function () {
                $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.questionnaire.category });
                /*$scope.subcategories = _.map($scope.subcategories, 'subcategory');*/
                //console.log($scope.subcategories);
            }

            if ($scope.evalID > 0) {
                techevalService.getquestionnaire($scope.evalID, 1)
                    .then(function (response) {
                        if (response) {
                            $scope.questionnaire = response;
                            $scope.questionnaire.startDate = new moment(response.startDate).format("DD-MM-YYYY");
                            $scope.questionnaire.endDate = new moment(response.endDate).format("DD-MM-YYYY");
                            if ($stateParams.cloneData) {
                                $scope.questionnaire.evalID = 0;
                                $scope.questionnaire.reqID = 0;
                                $scope.questionnaire.questions.forEach(function (item, index) {
                                    item.questionID = 0;
                                    item.isValid = 1;
                                });

                            }
                            $scope.questionnaire.vendors = $scope.questionnaire.vendors;
                            $scope.loadSubCategories();
                            $scope.GetCompanyVendors();
                        }
                    })
            }

            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));
                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    } else {
                        //console.log(response.data[0].errorMessage);
                    }
                }, function (result) {
                    //console.log("there is no current auctions");
                });
            }
            $scope.getCategories();

            $scope.savequestionnaire = function () {

                var ts = moment($scope.questionnaire.startDate, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var startDate = new Date(m);
                var milliseconds = parseInt(startDate.getTime() / 1000.0);
                $scope.questionnaire.startDate = "/Date(" + milliseconds + "000+0530)/";

                var ts = moment($scope.questionnaire.endDate, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var endDate = new Date(m);
                var milliseconds = parseInt(endDate.getTime() / 1000.0);
                $scope.questionnaire.endDate = "/Date(" + milliseconds + "000+0530)/";

                if (parseFloat($scope.questionnaire.totalMarks) < parseFloat($scope.questionnaire.qualifyingMarks)) {
                    growlService.growl("Qualifying marks cannot be greater than Total Marks.", "inverse");
                    return;
                }

                $scope.questionnaire.createdBy = userService.getUserId();
                $scope.questionnaire.sessionID = $scope.sessionID;
                var params = {
                    questionnaire: $scope.questionnaire
                };
                techevalService.savequestionnaire(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Questionnaire Saved Successfully.", "success");
                            $state.go("managetecheval");
                        }
                    })
            };

            $scope.selectForA = function (item) {
                var index = $scope.selectedA.indexOf(item);
                if (index > -1) {
                    $scope.selectedA.splice(index, 1);
                } else {
                    $scope.selectedA.splice($scope.selectedA.length, 0, item);
                }
                for (i = 0; i < $scope.selectedA.length; i++) {
                    $scope.questionnaire.vendors.push($scope.selectedA[i]);
                    $scope.Vendors.splice($scope.Vendors.indexOf($scope.selectedA[i]), 1);
                }
                $scope.reset();
            }

            $scope.selectForB = function (item) {
                var index = $scope.selectedB.indexOf(item);
                if (index > -1) {
                    $scope.selectedB.splice(index, 1);
                } else {
                    $scope.selectedB.splice($scope.selectedA.length, 0, item);
                }
                for (i = 0; i < $scope.selectedB.length; i++) {
                    $scope.Vendors.push($scope.selectedB[i]);
                    $scope.questionnaire.vendors.splice($scope.questionnaire.vendors.indexOf($scope.selectedB[i]), 1);
                }
                $scope.reset();
            }

            $scope.AtoB = function () {

            }

            $scope.BtoA = function () {

            }

            $scope.reset = function () {
                $scope.selectedA = [];
                $scope.selectedB = [];
            }

            $scope.GetCompanyVendors = function () {
                $scope.vendorsLoaded = false;
                var category = [];

                category.push($scope.questionnaire.category);
                if ($scope.questionnaire.category != undefined) {
                    var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId() };
                    $http({
                        method: 'POST',
                        url: domain + 'getvendors',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data) {
                            if (response.data.length > 0) {
                                $scope.Vendors = response.data;
                                $scope.vendorsLoaded = true;
                                for (var j in $scope.questionnaire.vendors) {
                                    for (var i in $scope.Vendors) {
                                        if ($scope.Vendors[i].vendorName == $scope.questionnaire.vendors[j].vendorName) {
                                            $scope.Vendors.splice(i, 1);
                                        }
                                    }
                                }
                            }
                            //$scope.questionnaire.vendors =[];
                        } else {
                            //console.log(response.data[0].errorMessage);
                        }
                    }, function (result) {
                        //console.log("there is no current auctions");
                    });
                }
            }


            $scope.saveVendorsForQuestionnaire = function () {
                var params = {
                    evalID: $scope.evalID,
                    vendors: $scope.questionnaire.vendors,
                    sessionID: userService.getUserToken()
                };

                techevalService.saveVendorsForQuestionnaire(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Questionnaire will be sent to the selected Vendors.", "success");
                        }
                    })
            }


        }]);