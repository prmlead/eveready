﻿using PRM.Core.Domain.Masters.GeneralTerms;
using PRM.Core.Pagers;
using System.Collections.Generic;

namespace PRM.Services.Masters.PaymentTerms
{
   public  interface IPaymentTermService
    {
        void InsertPaymentTerm(PaymentTerm entity);

        void UpdatePaymentTerm(PaymentTerm entity);

        void DeletePaymentTerm(PaymentTerm entity);

        PaymentTerm GetPaymentTermById(int id);

        IList<PaymentTerm> GetPaymentTermList(object filter);

        IPagedList<PaymentTerm> GetPaymentTermList(Pager pager,int companyId);
    }
}