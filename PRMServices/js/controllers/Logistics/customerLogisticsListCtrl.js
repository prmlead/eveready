﻿
prmApp
    .controller('customerLogisticsListCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService",
        "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService", "logisticServices",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService,
            fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, logisticServices) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;

            $scope.reportToDate = moment().format('YYYY-MM-DD');
            $scope.reportFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");
            /*pagination code*/
            $scope.totalItems = 0;
            $scope.totalVendors = 0;
            $scope.totalSubuser = 0;
            $scope.totalInactiveVendors = 0;
            $scope.totalLeads = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage = 8;
            $scope.itemsPerPage2 = 8;
            $scope.maxSize = 8;
            $scope.myAuctions = [];
            $scope.categories = [];
            $scope.reqStatus = 'ALL';
            $scope.category = 'ALL CATEGORIES';
            $scope.logisticConsalidatedReport = [];
            $scope.accountingConsalidatedReport = [];

           

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
            };
            $scope.getAuctions = function () {

                

                logisticServices.getmyAuctions({ "userid": userService.getUserId(),"reqstatus": $scope.reqStatus, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        
                        $scope.myAuctions = response;
                        $scope.myAuctions1 = response;
                        
                        if ($scope.myAuctions.length > 0) {
                            $scope.myAuctionsLoaded = true;
                            $scope.totalItems = $scope.myAuctions.length;
                        } else {
                            $scope.myAuctionsLoaded = false;
                            $scope.totalItems = 0;
                            $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                        }
                        $scope.tempStatusFilter = angular.copy($scope.myAuctions);
                        $scope.tempCategoryFilter = angular.copy($scope.myAuctions);
                    });
            }

            $scope.getAuctions();

            $scope.goToCustomerLogistic = function (id) {
                $state.go('customerlogistic', { 'Id': id });
            };

            $scope.cloneRequirement = function (reqID) {
                let tempObj = {};
                tempObj.cloneId = reqID;
                //$state.go('form.addnewrequirement', { 'Id': reqID, reqObj: tempObj });
                $state.go('form.addnewlogistic', { 'Id': reqID, reqObj: tempObj });
            };

            $scope.searchTable = function (str) {

                $scope.category = 'ALL CATEGORIES';
                $scope.reqStatus = 'ALL';
                $scope.myAuctions = $scope.myAuctions1.filter(function (req) {
                    return (String(req.requirementID).includes(str) == true || String(req.title).includes(str) == true || req.category[0].includes(str) == true || String(req.daNo).includes(str) == true);
                });

                $scope.totalItems = $scope.myAuctions.length;
            }


            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));

                            $scope.categories.push('ALL CATEGORIES');

                            //categories.splice(categories.indexOf("ALL"), 1);
                            //categories.unshift('ALL');

                            $scope.categories = $scope.categories.filter(item => item !== "ALL CATEGORIES" && item !== "");
                            $scope.categories.unshift("ALL CATEGORIES");

                            //console.log($scope.categories);

                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    } else {
                    }
                }, function (result) {
                });

            };


            $scope.getCategories();

            $scope.getStatusFilter = function (reqStatus) {
                $scope.filterArray = [];
                if (reqStatus == 'ALL') {
                    $scope.myAuctions = $scope.myAuctions1;

                } else {
                    $scope.filterArray = $scope.tempStatusFilter.filter(function (item) {
                        return item.status == reqStatus;
                    });
                    $scope.myAuctions = $scope.filterArray;
                }

                $scope.totalItems = $scope.myAuctions.length;

            }

            $scope.getCategoryFilter = function (filterCategoryVal) {
                $scope.filterCatArray = [];
                if (filterCategoryVal == 'ALL CATEGORIES') {
                    $scope.myAuctions = $scope.myAuctions1;
                } else {
                    $scope.filterCatArray = $scope.tempCategoryFilter.filter(function (item) {
                        return item.category[0] == filterCategoryVal;
                    });
                    $scope.myAuctions = $scope.filterCatArray;

                }

                $scope.totalItems = $scope.myAuctions.length;
            }



            $scope.getLogisticConsalidatedReport = function () {
                $scope.errMessage = '';
                reportingService.getLogisticConsalidatedReport($scope.reportFromDate, $scope.reportToDate)
                    .then(function (response) {
                        $scope.logisticConsalidatedReport = response;

                       // $scope.totalItems = $scope.logisticConsalidatedReport.length;
                        $scope.logisticConsalidatedReport.forEach(function (item, index) {
                           // item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                            item.reqPostedOn = $scope.GetDateconverted(item.reqPostedOn);
                          //  item.startTime = $scope.GetDateconverted(item.startTime);
                            item.chaCharges = 2500;
                            item.grandTotal = item.chaCharges + item.freightBidAmount;
                            item.invoiceDate = new moment(item.invoiceDate).format("YYYY-MM-DD");
                            if (String(item.startTime).includes('9999')) {
                                item.startTime = '';
                            }
                            if (String(item.invoiceDate).includes('9999')) {
                                item.invoiceDate = 'NA';
                            }

                            if (item.isPalletize == 1) {
                                item.grandTotal = (item.freightBidAmount + item.serviceCharges + item.customClearance + parseInt(item.terminalHandling) + parseInt(item.drawbackAmount) + parseInt(item.loadingUnloadingCharges) +
                                    item.warehouseStorages + item.additionalMiscExp + item.narcoticSubcharges + (item.PalletizeQty * item.PalletizeNumber));
                            }
                            else if (item.isPalletize == 0) {
                                item.grandTotal = (item.freightBidAmount + item.serviceCharges + item.customClearance + parseInt(item.terminalHandling) + parseInt(item.drawbackAmount) + parseInt(item.loadingUnloadingCharges) +
                                    item.warehouseStorages + item.additionalMiscExp + item.narcoticSubcharges);
                            }
                            //if ($scope.updatepaymentdateparams.date == '31/12/9999') {
                            //    $scope.updatepaymentdateparams.date = 'Payment Date';
                            //}
                        })

                        //alasql('SELECT requirementID as [Requirement ID],title as [Requirement Title],reqPostedOn as [Posted On], invoiceNumber as [Invoice Number], ' +
                        //    'invoiceDate as [Invoice Date], consigneeName as [Consignee Name], destination as Destination, ' +
                        //    'productName as [Product Name], shippingMode as [Shipping Mode AIR/SEA], ' +
                        //    'qty as [Quantity], chargeableWt as [Chargeable Wt], dgFee as [DG Fee], ams as [AMS], misc as[MISC], menzinesCharges as [Menzines Charges], ' +
                        //    'chaCharges as [CHA Charges], freightBidAmount as [Freight/Bid Amount], grandTotal as [Grand Total]' +
                        //    'INTO XLSX(?, { headers: true, sheetid: "LogisticConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                        //    ["LogisticConsolidatedReport.xlsx", $scope.logisticConsalidatedReport]);
                    })
            }
            $scope.getLogisticConsalidatedReport();


            $scope.getAccountingConsolidatedReports = function () {
                $scope.errMessage = '';
                reportingService.GetAccountingConsolidatedReports($scope.reportFromDate, $scope.reportToDate)
                    .then(function (response) {
                        $scope.accountingConsalidatedReport = response;

                       // $scope.totalItems = $scope.accountingConsalidatedReport.length;
                        $scope.accountingConsalidatedReport.forEach(function (item, index) {
                            // item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                            item.reqPostedOn = $scope.GetDateconverted(item.reqPostedOn);
                            //  item.startTime = $scope.GetDateconverted(item.startTime);
                           
                           
                            if (String(item.startTime).includes('9999')) {
                                item.startTime = '';
                            }
                           
                        })

                        //alasql('SELECT requirementID as [Requirement ID],invoiceNumber as [Invoice Number],reqPostedOn as [Posted On], ' +
                        //    'consigneeName as [Consignee Name], destination as [Final Destination],portOfLanding as [Port Of Destination], ' +
                        //    'productName as [Product Name], shippingMode as [Shipping Mode], ' +
                        //    'pallet as [Division],netWeight as [Net Weight], ' +
                        //    'grandTotal as [Grand Total]' +
                        //    'INTO XLSX(?, { headers: true, sheetid: "AccountingConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                        //    ["AccountingConsolidatedReport.xlsx", $scope.accountingConsalidatedReport]);
                    })
            }
            $scope.getAccountingConsolidatedReports();

            $scope.getReport = function (name) {
                reportingService.downloadConsolidatedTemplate(name, $scope.reportFromDate, $scope.reportToDate, userService.getUserId());
            }

            //$scope.getLogisticConsalidatedReport();

            //$scope.GetReport = function () {
            //    //alasql.fn.handleDate = function (date) {
            //    //    return new moment(date).format("MM/DD/YYYY");
            //    //}

            //    alasql('SELECT requirementID as [Requirement ID],title as [Requirement Title],reqPostedOn as [Posted On], invoiceNumber as [Invoice Number], ' +
            //        'invoiceDate as [Invoice Date], consigneeName as [Consignee Name], destination as Destination, ' +
            //        'productName as [Product Name], shippingMode as [Shipping Mode AIR/SEA], ' +
            //        'qty as [Quantity], chargeableWt as [Chargeable Wt], dgFee as [DG Fee], ams as [AMS], misc as[MISC], menzinesCharges as [Menzines Charges], ' +
            //        'chaCharges as [CHA Charges],savings as Savings, freightBidAmount as [Freight/Bid Amount], grandTotalas [Grand Total]' +
            //        'INTO XLSX(?, { headers: true, sheetid: "LogisticConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
            //        ["LogisticConsolidatedReport.xlsx", $scope.logisticConsalidatedReport]);

            //}

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
                }
            };




        }]);