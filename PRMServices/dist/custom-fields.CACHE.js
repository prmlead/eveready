﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('customfields', {
                    url: '/customfields',
                    templateUrl: 'custom-fields/views/custom-fields.html',
                    params: {
                        detailsObj: null
                    }
                }).state('savefield', {
                    url: '/savefield/:Id',
                    templateUrl: 'custom-fields/views/save-field.html',
                    params: {
                        detailsObj: null
                    }
                }).state('templates', {
                    url: '/templates',
                    templateUrl: 'custom-fields/views/templates.html',
                    params: {
                        detailsObj: null
                    }
                }).state('template-fields', {
                    url: '/template-fields/:Id/:name',
                    templateUrl: 'custom-fields/views/template-fields.html',
                    params: {
                        detailsObj: null
                    }
                });
        }]);﻿prmApp
    .controller('customFieldListCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMCustomFieldService",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMCustomFieldService) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.sessionID = userService.getUserToken();

        /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/


            $scope.customFieldList = [];
            $scope.filteredcustomFieldList = [];

            $scope.getCustomFieldList = function () {
                var params = {
                    "compid": userService.getUserCompanyId(),
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.getCustomFieldList(params)
                    .then(function (response) {
                        $scope.customFieldList = response;
                        $scope.filteredcustomFieldList = $scope.customFieldList;

                        /*PAGINATION CODE START*/
                        $scope.totalItems = $scope.customFieldList.length;
                        /*PAGINATION CODE END*/
                        document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                        document.documentElement.scrollTop = 0; // For IE and Firefox
                    });

            };

            $scope.getCustomFieldList();
            
            $scope.goToCustomFieldSave = function (field) {
                var url = $state.href("savefield", { "Id": field.CUST_FIELD_ID });
                window.open(url, '_self');
            };

        }]);﻿prmApp
    .controller('prmTemplateCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService",
        "PRMCustomFieldService",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMCustomFieldService) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;        
            $scope.prmTemplateList = [];
            $scope.prmTemplateList1 = [];
            $scope.selectedTemplate = {};

            $scope.getPRMTemplateList = function () {
                var params = {
                    "compid": userService.getUserCompanyId(),
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplates(params)
                    .then(function (response) {
                        $scope.prmTemplateList = response;
                        $scope.prmTemplateList1 = response;
                    });
            };

            $scope.getPRMTemplateList();
            
            $scope.goToTemplateFields = function (template) {
                var url = $state.href("template-fields", { "Id": template.TEMPLATE_ID, "name": template.TEMPLATE_NAME });
                window.open(url, '_self');
            };


            $scope.editTemplate = function (template) {
                createTemplateObj();
                $scope.selectedTemplate.TEMPLATE_ID = template.TEMPLATE_ID;
                $scope.selectedTemplate.TEMPLATE_NAME = template.TEMPLATE_NAME;
                $scope.selectedTemplate.TEMPLATE_DESC = template.TEMPLATE_DESC;
                $scope.selectedTemplate.IS_DEFAULT = template.IS_DEFAULT == 1 ? true : false;
            };

            $scope.newTemplate = function () {
                createTemplateObj();
            };

            $scope.saveTemplate = function () {
                angular.element('#showTemplateModal').modal('hide');
                let params = {
                    template: $scope.selectedTemplate
                };

                PRMCustomFieldService.SaveTemplate(params)
                    .then(function (response) {
                        if (response && +response.objectID) {
                            if (!$scope.selectedTemplate.TEMPLATE_ID) {
                                $scope.selectedTemplate.TEMPLATE_ID = response.objectID;
                                $scope.prmTemplateList.push($scope.selectedTemplate);
                            } else {
                                let temp = $scope.prmTemplateList.filter(function (item) {
                                    return item.TEMPLATE_ID === $scope.selectedTemplate.TEMPLATE_ID;
                                });

                                if (temp && temp.length > 0) {
                                    temp[0].TEMPLATE_NAME = $scope.selectedTemplate.TEMPLATE_NAME;
                                    temp[0].TEMPLATE_DESC = $scope.selectedTemplate.TEMPLATE_DESC;
                                }
                            }
                            
                            swal("Success!", "Successfully created.", "success");
                        } else {
                            swal("Error!", "Error saving data.", "error");
                        }
                    });
            };

            $scope.duplicateTemplate = function () {
                let isDuplicate = false;

                let temp = $scope.prmTemplateList.filter(function (item) {
                    return item.TEMPLATE_NAME === $scope.selectedTemplate.TEMPLATE_NAME && item.TEMPLATE_ID !== $scope.selectedTemplate.TEMPLATE_ID;
                });

                if (temp && temp.length > 0) {
                    isDuplicate = true;
                }

                return isDuplicate;
            };

            function createTemplateObj() {
                $scope.selectedTemplate = {
                    TEMPLATE_ID: 0,
                    COMP_ID: userService.getUserCompanyId(),
                    TEMPLATE_NAME: '',
                    TEMPLATE_DESC: '',
                    ModifiedBy: userService.getUserId(),
                    IS_DEFAULT: false
                };
            }
            $scope.searchTable = function () {

                if ($scope.searchKeyword) {
                    $scope.prmTemplateList = _.filter($scope.prmTemplateList1, function (item) {
                        return (item.TEMPLATE_NAME.toUpperCase().indexOf($scope.searchKeyword.toUpperCase()) > -1
                            
                        );
                    });
                } else {
                    $scope.prmTemplateList = $scope.prmTemplateList1;
                }

               

            };

        }]);﻿prmApp
    .controller('prmTemplateFieldsCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "growlService",
        "PRMCustomFieldService",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, growlService, PRMCustomFieldService) {

            $scope.templateId = $stateParams.Id;
            $scope.templateName = $stateParams.name;
            console.log($stateParams);
            $scope.userID = userService.getUserId();
            $scope.compID = userService.getUserCompanyId();
            $scope.sessionID = userService.getUserToken();
            $scope.prmTemplateFieldsList = [];
            $scope.selectedTemplateField = {};
            $scope.templateFieldKeys = [];
            $scope.productQuotationTemplateMaster = [];
            $scope.companyItemUnits = [];

            $scope.getPRMTemplateFieldsList = function () {
                var params = {
                    "templateid": $scope.templateId,
                    "templatename": '',
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params)
                    .then(function (response) {
                        $scope.prmTemplateFieldsList = response;
                    });
            };

            auctionsService.GetCompanyConfiguration($scope.compID, "TEMPLATE_FIELD_KEY", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.templateFieldKeys = _.filter(unitResponse, function (field) {
                        return field.configKey === 'TEMPLATE_FIELD_KEY';
                    });
                });

            $scope.getPRMTemplateFieldsList();
            
            $scope.goToTemplate = function (template) {
                var url = $state.href("template", { "Id": template.TEMPLATE_ID });
                window.open(url, '_self');
            };


            $scope.editTemplateField = function (templateField) {
                createTemplateFieldObj();
                $scope.selectedTemplateField = templateField;
            };

            $scope.newTemplateField = function () {
                createTemplateFieldObj();
            };

            $scope.saveTemplateField = function () {
                let values = [];
                $scope.selectedTemplateField.FIELD_READ_ONLY = $scope.selectedTemplateField.FIELD_READ_ONLY ? 1 : 0;
                values.push($scope.selectedTemplateField);
                angular.element('#showTemplatFieldModal').modal('hide');
                let params = {
                    fields: values
                };

                PRMCustomFieldService.SaveTemplateFields(params)
                    .then(function (response) {
                        if (response && +response.objectID) {
                            $scope.getPRMTemplateFieldsList();                            
                            swal("Success!", "Successfully created.", "success");
                        } else {
                            swal("Error!", "Error saving data.", "error");
                        }
                    });
            };

            $scope.isValidForm = function () {
                let isValid = false;

                if ($scope.selectedTemplateField) {
                    if ($scope.selectedTemplateField.FIELD_NAME && $scope.selectedTemplateField.FIELD_LABEL) {
                        isValid = true;
                    }
                }

                if ($scope.duplicateTemplateField()) {
                    isValid = false;
                }

                return isValid;
            };

            $scope.duplicateTemplateField = function () {
                let isDuplicate = false;

                let temp = $scope.prmTemplateFieldsList.filter(function (item) {
                    return item.FIELD_NAME === $scope.selectedTemplateField.FIELD_NAME && item.TEMPLATE_FIELD_ID !== $scope.selectedTemplateField.TEMPLATE_FIELD_ID;
                });

                if (temp && temp.length > 0) {
                    isDuplicate = true;
                }

                return isDuplicate;
            };

            function createTemplateFieldObj() {
                $scope.selectedTemplateField = {
                    TEMPLATE_FIELD_ID: 0,
                    TEMPLATE_ID: $scope.templateId,
                    FIELD_NAME: '',
                    FIELD_LABEL: '',
                    FIELD_PLACEHOLDER: '',
                    FIELD_DATA_TYPE: '',
                    FIELD_DEFAULT_VALUE: '',
                    FIELD_OPTIONS: '',
                    FIELD_READ_ONLY: 0,
                    FIELD_IS_VENDOR: 1,
                    FIELD_IS_CUSTOMER: 1,
                    ModifiedBy: userService.getUserId()
                };
            }

            auctionsService.GetCompanyConfiguration($scope.compID, 'ITEM_UNITS', userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyItemUnits = [];
                    unitResponse.forEach(function (item, index) {
                        if (item.configKey === 'ITEM_UNITS') {
                            $scope.companyItemUnits.push(item);
                        }
                    });
                });


            //Below product sub item
            $scope.SaveProductQuotationTemplate = function (obj) {
                var sameNameError = false;
                $scope.productQuotationTemplateMaster.forEach(function (item, itemIndex) {
                    if (obj.NAME.toUpperCase() === item.NAME.toUpperCase() && obj.T_ID === 0) {
                        sameNameError = true;
                    }
                });

                if (sameNameError) { growlService.growl("Same name Error.", "inverse"); return false; }

                var params = {
                    "subitemtemplate": obj,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.saveTemplateSubItem(params)
                    .then(function (response) {
                        if (response) {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.resetTemplateObj();
                            $scope.GetProductQuotationTemplate();
                            angular.element('#newSubItemAction').modal('hide');
                        }
                    });
            };

            $scope.resetTemplateObj = function () {
                $scope.QuotationTemplateObj = {
                    SUB_ITEM_ID: 0,
                    TEMPLATE_ID: $scope.templateId,
                    NAME: '',
                    DESCRIPTION: '',
                    HAS_SPECIFICATION: 0,
                    HAS_PRICE: 0,
                    HAS_QUANTITY: 0,
                    CONSUMPTION: 1,
                    UOM: '',
                    HAS_TAX: 0,
                    IS_VALID: 1,
                    U_ID: userService.getUserId()
                };
            };

            $scope.resetTemplateObj();

            $scope.GetProductQuotationTemplate = function () {

                var params = {
                    "templateid": $scope.templateId,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.getTemplateSubItem(params)
                    .then(function (response) {
                        if (response) {
                            $scope.productQuotationTemplateMaster = response;
                        }
                    });
            };

            $scope.GetProductQuotationTemplate();

            $scope.editSubItemTemplate = function (obj) {
                $scope.QuotationTemplateObj = obj;
            };

            $scope.deleteSubItemTemplate = function (obj) {
                obj.IS_VALID = 0;
                $scope.QuotationTemplateObj = obj;
                var params = {
                    "subitemtemplate": obj,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.saveTemplateSubItem(params)
                    .then(function (response) {
                        if (response) {
                            growlService.growl("Deleted Successfully.", "success");
                            $scope.resetTemplateObj();
                            $scope.GetProductQuotationTemplate();
                        }
                    });
            };
            //^Above product sub item

        }]);﻿prmApp
    .controller('saveCustomFieldCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMCustomFieldService",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMCustomFieldService) {

            $scope.customFieldId = $stateParams.Id;
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.sessionID = userService.getUserToken();
            $scope.customFieldObj = {};

            $scope.fielTypeList = [
                {
                    FIELD_NAME: 'TEXT',
                    FIELD_TYPE: 'TEXT'
                },
                {
                    FIELD_NAME: 'Date Time',
                    FIELD_TYPE: 'DATETIME'
                },
                {
                    FIELD_NAME: 'NUMBER',
                    FIELD_TYPE: 'NUMBER'
                },
                {
                    FIELD_NAME: 'Large Text',
                    FIELD_TYPE: 'LARGETEXT'
                }
            ];

            $scope.moduleList = [
                {
                    FIELD_NAME: 'Select Module',
                    FIELD_TYPE: ''
                },
                {
                    FIELD_NAME: 'REQUIREMENT',
                    FIELD_TYPE: 'REQUIREMENT'
                }
            ];

            $scope.getCustomFieldList = function () {
                if ($scope.customFieldId > 0) {
                    var params = {
                        "compid": userService.getUserCompanyId(),
                        "customfieldid": $scope.customFieldId,
                        "sessionid": userService.getUserToken()
                    };

                    PRMCustomFieldService.getCustomFieldList(params)
                        .then(function (response) {
                            if (response && response.length > 0) {
                                $scope.customFieldObj = response[0];
                            }
                        });
                }
            };

            $scope.getCustomFieldList();

            $scope.saveField = function () {
                $scope.customFieldObj.ModifiedBy = $scope.userID;
                var params = {
                    "details": $scope.customFieldObj,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.saveCustomField(params)
                    .then(function (response) {
                        if (response.errorMessage !== '' || response.objectID <= 0) {
                            growlService.growl(response.errorMessage, "inverse");
                        } else {
                            $scope.customFieldId = response.objectID;
                            //$scope.resetFieldObj();
                            growlService.growl("Saved Successfully.", "success");
                        }
                    });
            };

            $scope.resetFieldObj = function () {
                $scope.customFieldObj = {
                    CUST_FIELD_ID: 0,
                    COMP_ID: userService.getUserCompanyId(),
                    FIELD_NAME: '',
                    FIELD_HELP_TEXT: '',
                    FIELD_TYPE: 'TEXT',
                    FIELD_MODULE: '',
                    IS_REQUIRED: 0,
                    FIELD_VALUE: '',
                    FIELD_DEFAULT_VALUE: '',
                    IS_READ_ONLY: 0,
                    VISIBLE_TO_VENDOR: 1
                };
            };

            $scope.resetFieldObj();
        }]);prmApp.constant('PRMCustomFieldServiceDomain', 'custom-fields/svc/PRMCustomFieldService.svc/REST/');
prmApp.service('PRMCustomFieldService', ["PRMCustomFieldServiceDomain", "userService", "httpServices", "$q",
    function (PRMCustomFieldServiceDomain, userService, httpServices, $q) {
        var PRMCustomFieldService = this;
        PRMCustomFieldService.companyFieldTemplates = [];
        PRMCustomFieldService.getCustomFieldList = function (params) {
            params.customfieldid = params.customfieldid ? params.customfieldid : 0;
            params.fieldname = params.fieldname ? params.fieldname : '';
            params.fieldmodule = params.fieldmodule ? params.fieldmodule : '';
            let url = PRMCustomFieldServiceDomain + 'getcustomfields?compid=' + params.compid + '&customfieldid=' + params.customfieldid +
                '&fieldname=' + params.fieldname + '&fieldmodule=' + params.fieldmodule + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMCustomFieldService.GetCustomFieldsByModuleId = function (params) {
            let url = PRMCustomFieldServiceDomain + 'getcustomfieldsbymoduleid?compid=' + params.compid + '&moduleid=' + params.moduleid +
                '&fieldmodule=' + params.fieldmodule + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url, params);
        };

        //PRMCustomFieldService.GetPRMFieldMappingDetails = function (params) {
        //    params.templatename = params.templatename ? params.templatename : '';
        //    let url = PRMCustomFieldServiceDomain + 'getprmfieldmappingdetails?compid=' + params.compid + '&templatename=' + params.templatename + '&sessionid=' + userService.getUserToken();
        //    return httpServices.get(url, params);
        //};

        PRMCustomFieldService.GetPRMFieldMappingTemplates = function () {
            let url = PRMCustomFieldServiceDomain + 'getfieldmappingtemplates?compid=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMCustomFieldService.GetTemplates = function () {
            let url = PRMCustomFieldServiceDomain + 'templates?compid=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMCustomFieldService.GetTemplateFields = function (params) {
            let url = PRMCustomFieldServiceDomain + 'templatefields?templateid=' + params.templateid + '&templatename=' + params.templatename + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMCustomFieldService.GetPRMFieldMappingDetails = function () {
            var def = $q.defer();
            if (PRMCustomFieldService.companyFieldTemplates && PRMCustomFieldService.companyFieldTemplates.length > 0) {
                def.resolve(PRMCustomFieldService.companyFieldTemplates);
            } else {

                var params = {
                    compid: userService.getUserCatalogCompanyId(),
                    templatename: '',
                    sessionId: userService.getUserToken()
                };

                let url = PRMCustomFieldServiceDomain + 'getprmfieldmappingdetails?compid=' + params.compid + '&templatename=' + params.templatename + '&sessionid=' + userService.getUserToken();
                httpServices.get(url)
                    .then(function (response) {
                        def.resolve(response);
                        if (response && response.length > 0) {
                            PRMCustomFieldService.companyFieldTemplates = response;
                        }
                    });
            }

            return def.promise;
        };

        PRMCustomFieldService.getFieldAlaisName = function (fieldName, template, defaultName) {
            var aliasName = defaultName;
            if (!template && template === '') {
                template = 'DEFAULT';
            }
            //if ((userService.companyFieldTemplates && userService.companyFieldTemplates.length > 0) &&
            //    (PRMCustomFieldService.companyFieldTemplates && PRMCustomFieldService.companyFieldTemplates.length > 0)) {

            if (userService.companyFieldTemplates && userService.companyFieldTemplates.length > 0) {
                var objList = _.filter(userService.companyFieldTemplates, function (o) {
                    return o.TEMPLATE_NAME === template && o.FIELD_NAME === fieldName;
                });
                if (objList && objList.length > 0) {
                    aliasName = objList[0].FIELD_ALIAS_NAME;
                }
            }

            return aliasName;
        };

        PRMCustomFieldService.saveCustomField = function (params) {
            let url = PRMCustomFieldServiceDomain + 'savecustomfield';
            return httpServices.post(url, params);
        };

        PRMCustomFieldService.saveCustomFieldValue = function (params) {
            let url = PRMCustomFieldServiceDomain + 'savecustomfieldvalue';
            return httpServices.post(url, params);
        };

        PRMCustomFieldService.SaveTemplate = function (params) {
            let url = PRMCustomFieldServiceDomain + 'savetemplate';
            return httpServices.post(url, params);
        };

        PRMCustomFieldService.SaveTemplateFields = function (params) {
            let url = PRMCustomFieldServiceDomain + 'savetemplatefields';
            return httpServices.post(url, params);
        };

        PRMCustomFieldService.saveTemplateSubItem = function (params) {
            let url = PRMCustomFieldServiceDomain + 'savetemplatesubitem';
            return httpServices.post(url, params);
        };

        PRMCustomFieldService.getTemplateSubItem = function (params) {
            let url = PRMCustomFieldServiceDomain + 'gettemplatesubitem?templateid=' + params.templateid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        return PRMCustomFieldService;

    }]);