﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class ConsolidatedReport : Entity
    {
        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "requirementNumber")]
        public string RequirementNumber { get; set; }

        [DataMember(Name = "requirementCurrency")]
        public string RequirementCurrency { get; set; }

        [DataMember(Name = "uID")]
        public int UID { get; set; }

        string title = string.Empty;
        [DataMember(Name = "title")]
        public string Title
        {
            get
            {
                if (!string.IsNullOrEmpty(title))
                { return title; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { title = value; }
            }
        }

        [DataMember(Name = "reqPostedOn")]
        public DateTime? ReqPostedOn { get; set; }

        [DataMember(Name = "startTime")]
        public DateTime? StartTime { get; set; }

        [DataMember(Name = "endTime")]
        public DateTime? EndTime { get; set; }

        [DataMember(Name = "savings")]
        public double Savings { get; set; }

        [DataMember(Name = "noOfVendorsInvited")]
        public int NoOfVendorsInvited { get; set; }

        [DataMember(Name = "noOfvendorsParticipated")]
        public int NoOfvendorsParticipated { get; set; }


        [DataMember(Name = "quotationFreezTime")]
        public DateTime? QuotationFreezTime { get; set; }


        [DataMember(Name = "isNegotiationEnded")]
        public int IsNegotiationEnded { get; set; }


        [DataMember(Name = "closed")]
        public string Closed { get; set; }

        [DataMember(Name = "reqCategory")]
        public string ReqCategory { get; set; }


        string l1Name = string.Empty;
        [DataMember(Name = "L1Name")]
        public string L1Name
        {
            get
            {
                if (!string.IsNullOrEmpty(l1Name))
                { return l1Name; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { l1Name = value; }
            }
        }

        string l2Name = string.Empty;
        [DataMember(Name = "L2Name")]
        public string L2Name
        {
            get
            {
                if (!string.IsNullOrEmpty(l2Name))
                { return l2Name; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { l2Name = value; }
            }
        }


        [DataMember(Name = "l1RevPrice")]
        public double L1RevPrice { get; set; }


        [DataMember(Name = "l2RevPrice")]
        public double L2RevPrice { get; set; }

        [DataMember(Name = "l1InitialBasePrice")]
        public double L1InitialBasePrice { get; set; }

        [DataMember(Name = "l2InitialBasePrice")]
        public double L2InitialBasePrice { get; set; }


        [DataMember(Name = "initialL1BasePrice")]
        public double InitialL1BasePrice { get; set; }

        //[DataMember(Name = "initialUser")]
        //public RequirementUsersByRank InitialUser { get; set; }

        //[DataMember(Name = "revisedUserL1")]
        //public RequirementUsersByRank RevisedUserL1 { get; set; }

        //[DataMember(Name = "revisedUserL2")]
        //public RequirementUsersByRank RevisedUserL2 { get; set; }


        #region Initial L1 START

        [DataMember(Name = "IL1_uID")]
        public int IL1_UID { get; set; }

        [DataMember(Name = "IL1_revBasePrice")]
        public double IL1_RevBasePrice { get; set; }

        [DataMember(Name = "IL1_initBasePrice")]
        public double IL1_InitBasePrice { get; set; }

        [DataMember(Name = "IL1_l1InitialBasePrice")]
        public double IL1_L1InitialBasePrice { get; set; }

        [DataMember(Name = "IL1_vendTotalPrice")]
        public double IL1_VendTotalPrice { get; set; }

        [DataMember(Name = "IL1_rank")]
        public int IL1_Rank { get; set; }

        [DataMember(Name = "IL1_quantity")]
        public double IL1_Quantity { get; set; }

        //[DataMember(Name = "IL1_companyName")]
        //public string IL1_CompanyName { get; set; }

        string IL1_companyName = string.Empty;
        [DataMember(Name = "IL1_companyName")]
        public string IL1_CompanyName
        {
            get
            {
                if (!string.IsNullOrEmpty(IL1_companyName))
                { return IL1_companyName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { IL1_companyName = value; }
            }
        }

        [DataMember(Name = "IL1_selectedVendorCurrency")]
        public string IL1_SelectedVendorCurrency { get; set; }

        #endregion Initial L1 END

        #region Revised RL1 START

        [DataMember(Name = "RL1_uID")]
        public int RL1_UID { get; set; }

        [DataMember(Name = "RL1_revBasePrice")]
        public double RL1_RevBasePrice { get; set; }

        [DataMember(Name = "RL1_initBasePrice")]
        public double RL1_InitBasePrice { get; set; }

        [DataMember(Name = "RL1_l1InitialBasePrice")]
        public double RL1_L1InitialBasePrice { get; set; }

        [DataMember(Name = "RL1_revVendTotalPrice")]
        public double RL1_RevVendTotalPrice { get; set; }

        [DataMember(Name = "RL1_rank")]
        public int RL1_Rank { get; set; }

        [DataMember(Name = "RL1_quantity")]
        public double RL1_Quantity { get; set; }

        //[DataMember(Name = "RL1_companyName")]
        //public string RL1_CompanyName { get; set; }

        string RL1_companyName = string.Empty;
        [DataMember(Name = "RL1_companyName")]
        public string RL1_CompanyName
        {
            get
            {
                if (!string.IsNullOrEmpty(RL1_companyName))
                { return RL1_companyName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { RL1_companyName = value; }
            }
        }

        [DataMember(Name = "RL1_selectedVendorCurrency")]
        public string RL1_SelectedVendorCurrency { get; set; }

        #endregion Revised RL1 END

        #region Revised RL2 START

        [DataMember(Name = "RL2_uID")]
        public int RL2_UID { get; set; }

        [DataMember(Name = "RL2_revBasePrice")]
        public double RL2_RevBasePrice { get; set; }

        [DataMember(Name = "RL2_initBasePrice")]
        public double RL2_InitBasePrice { get; set; }

        [DataMember(Name = "RL2_l1InitialBasePrice")]
        public double RL2_L1InitialBasePrice { get; set; }

        [DataMember(Name = "RL2_revVendTotalPrice")]
        public double RL2_RevVendTotalPrice { get; set; }

        [DataMember(Name = "RL2_rank")]
        public int RL2_Rank { get; set; }

        [DataMember(Name = "RL2_quantity")]
        public double RL2_Quantity { get; set; }

        //[DataMember(Name = "RL2_companyName")]
        //public string RL2_CompanyName { get; set; }

        string RL2_companyName = string.Empty;
        [DataMember(Name = "RL2_companyName")]
        public string RL2_CompanyName
        {
            get
            {
                if (!string.IsNullOrEmpty(RL2_companyName))
                { return RL2_companyName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { RL2_companyName = value; }
            }
        }

        [DataMember(Name = "RL2_selectedVendorCurrency")]
        public string RL2_SelectedVendorCurrency { get; set; }

        #endregion Revised RL2 END


        [DataMember(Name = "basePriceSavings")]
        public double BasePriceSavings { get; set; }

        [DataMember(Name = "savingsPercentage")]
        public double SavingsPercentage { get; set; }

        [DataMember(Name = "selectedVendorCurrency")]
        public string SelectedVendorCurrency { get; set; }

        [DataMember(Name = "l1BasePrice")]
        public double L1BasePrice { get; set; }

        [DataMember(Name = "l2BasePrice")]
        public double L2BasePrice { get; set; }

        [DataMember(Name = "productQuantity")]
        public double ProductQuantity { get; set; }

        [DataMember(Name = "productQuantityIn")]
        public string ProductQuantityIn { get; set; }

        [DataMember(Name = "POSTED_BY_USER")]
        public string POSTED_BY_USER { get; set; }

        [DataMember(Name = "prNumbers")]
        public string PRNumbers { get; set; }

        [DataMember(Name = "prCreator")]
        public string PRCreator { get; set; }

        [DataMember(Name = "PLANT_CODES")]
        public string PLANT_CODES { get; set; }

        [DataMember(Name = "PLANTS")]
        public string PLANTS { get; set; }

        [DataMember(Name = "PURCHASE_GROUP_CODES")]
        public string PURCHASE_GROUP_CODES { get; set; }

        [DataMember(Name = "PR_ID")]
        public string PR_ID { get; set; }

        [DataMember(Name = "PR_ITEM_IDS")]
        public string PR_ITEM_IDS { get; set; }

        [DataMember(Name = "DATETIME_NOW")]
        public DateTime? DATETIME_NOW { get; set; }

        [DataMember(Name = "CB_END_TIME")]
        public DateTime? CB_END_TIME { get; set; }

        [DataMember(Name = "CB_TIME_LEFT")]
        public long CB_TIME_LEFT { get; set; }

        [DataMember(Name = "timeLeft")]
        public long TimeLeft { get; set; }
    }
}

