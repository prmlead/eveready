﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Models.Logistics
{
    public class LogisticQuotationUpdateModel
    {
        public int QuotationId { get; set; }

        public decimal CustomClearance { get; set; }

        public decimal ServiceCharges { get; set; }

        public decimal TerminalHandling { get; set; }

        public string InvoiceNumber { get; set; }

        public DateTime? InvoiceDate { get; set; }

        public string ConsigneeName { get; set; }

        public double netWeight { get; set; }

        public string natureOfGoods { get; set; }

        public string finalDestination { get; set; }

        public string productIDorName { get; set; }

        public double palletizeNumber { get; set; }

        public string AirwayBillNumber { get; set; }

        public string ShippingBillNumber { get; set; }

        public string LicenseNumber { get; set; }

        public double WarehouseStorages { get; set; }

        public string CustomerName { get; set; }

        public double DrawbackAmount { get; set; }

        public double LoadingUnloadingCharges { get; set; }

        public double AdditionalMiscExp { get; set; }

        public double NarcoticSubcharges { get; set; }
    }
}