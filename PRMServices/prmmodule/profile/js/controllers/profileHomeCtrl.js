angular = require('angular');

angular.module('profileModule')
.controller('profileHomeCtrl', ["$scope", "$http", "$state", "$log", "growlService", "loginService", "profileService", "domain", "fileReader", function ($scope, $http, $state, $log, growlService, loginService, profileService, domain, fileReader)
{																													 
    //$state need to change

	$scope.sessionid = loginService.getUserToken();
	$scope.userObj = {};
	$scope.editSummary = 0;
	$scope.editInfo = 0;
	$scope.editContact = 0;

	$scope.isnegotiationrunning = false;

	//$log.debug("siva");
	profileService.isnegotiationrunning()
        .then(function(response){
			$scope.isnegotiationrunning = response.data.IsNegotationRunningResult;
	})
	
	profileService.getUserDataNoCache()
		.then(function (response) {
			$scope.userObj = response;
		})

	var loginUserData = loginService.getUserObj();
	$scope.oldPhoneNum = loginUserData.phoneNum;

	$scope.isPhoneModifies = 0;
	$scope.isEmailModifies = 0;

	$scope.callGetUserDetails = function () {            
		profileService.getProfileDetails({ "userid": loginService.getUserId(), "sessionid": loginService.getUserToken() })
			.then(function (response) {
				if (response != undefined) {
					$scope.userDetails = response;
				}               
			});
	}
	$scope.callGetUserDetails();

	$scope.updateUserInfo = function () {
		if ($scope.oldPhoneNum != $scope.userObj.phoneNum) {
			$scope.isPhoneModifies = 1;
		}

		var params = {};            
		if ($scope.userObj.firstName.toString() == "") {
			growlService.growl("Name cannot be empty.", "inverse");
			return false;
		}
		if ($scope.userObj.lastName.toString() == "") {
			growlService.growl("Name cannot be empty.", "inverse");
			return false;
		}
		if ($scope.userObj.phoneNum.toString() == "") {
			growlService.growl("Phone Number cannot be empty.", "inverse");
			return false;
		}
		if (isNaN($scope.userObj.phoneNum)) {
			growlService.growl("Please Enter correct Mobile number.", "inverse");
			return false;
		}
		if ($scope.userObj.phoneNum.toString().length != 10) {
			growlService.growl("Phone Number Must be 10 digits.", "inverse");
			return false;
		}
		if ($scope.userObj.email.toString() == "") {
			growlService.growl("Email cannot be empty.", "inverse");
			return false;
		}
		
		params = $scope.userDetails;

		params.firstName = $scope.userObj.firstName;
		params.lastName = $scope.userObj.lastName;
		params.phoneNum = $scope.userObj.phoneNum;
	   
		params.isOTPVerified = $scope.isOTPVerified;
	  
		if (params.phoneNum != $scope.userObj.phoneNum) {
			params.isOTPVerified = 0;
		}
		params.email = $scope.userObj.email;
		params.isEmailOTPVerified = $scope.isEmailOTPVerified;
		if (params.email != $scope.userObj.email) {
			params.isEmailOTPVerified = 0;
		}
		params.sessionID = loginService.getUserToken();
		params.userID = loginService.getUserId();
		params.errorMessage = "";
		loginService.updateUser(params)
			.then(function (response) {
				if (response.toLowerCase().indexOf('already exists') > 0) {
					profileService.getUserDataNoCache().then(function (response) {

						var loginUserData = loginService.getUserObj();
						$scope.userObj.fullName = loginUserData.firstName + " " + loginUserData.lastName;
						$scope.userObj.firstName = loginUserData.firstName;
						$scope.userObj.lastName = loginUserData.lastName;
						$scope.userObj.phoneNum = loginUserData.phoneNum;
						$scope.userObj.email = loginUserData.email;
						$scope.oldPhoneNum = loginUserData.phoneNum;
						$scope.oldemail = loginUserData.email;
						$scope.userObj = loginUserData;
						$scope.callGetUserDetails();
						//$state.go('pages.profile.profile-about');
						$state.reload();
					});
				}

				//$state.go('pages.profile.profile-about');
				$state.reload();
			});

		$scope.editSummary = 0;
		$scope.editInfo = 0;
		$scope.editContact = 0;
		$scope.callGetUserDetails();
	};




	//////////////////////////
	//CredentialsController
	/////////////////////////

	$scope.pannumber = "";
	$scope.vatnumber = "";
	$scope.taxnumber = "";

	$scope.panObject = {};      
	$scope.tinObject = {};
	$scope.stnObject = {};

	$scope.taxnumberalpha = false;
	$scope.taxnumberlengthvalidation = false;
	$scope.taxnumberrequired = false;

	$scope.tinnumberrequired = false;

	$scope.pannumberalpha = false;
	$scope.pannumberlengthvalidation = false;
	$scope.pannumberrequired = false;
 
	$scope.uploadedCredentials = [{ "credentialID": '', "fileType": 'PAN', 'fileLink': "", 'isVerified': 0 }, { "credentialID": '', "fileType": 'TIN', 'fileLink': "", 'isVerified': 0 }, { "credentialID": '', "fileType": 'STN', 'fileLink': "", 'isVerified': 0 }];

	$scope.editDocVerification = 0;

	$scope.CredentialUpload = [];

	$scope.getUserCredentials = function () {
		$http({
			method: 'GET',
			url: domain + 'getusercredentials?sessionid=' + loginService.getUserToken() + "&userid=" + loginService.getUserId(),
			encodeURI: true,
			headers: { 'Content-Type': 'application/json' }
		}).then(function (response) {

			$scope.CredentialsResponce = response.data;
			
			if (response && response.data && response.data.length > 0) {
				if (response.data[0].errorMessage == "") {
					var panObj = _.filter($scope.CredentialsResponce, ['fileType', 'PAN']);
					$scope.pannumber = panObj[0].credentialID;
					$scope.vatnumber = _.filter($scope.CredentialsResponce, ['fileType', 'TIN'])[0].credentialID;
					$scope.taxnumber = _.filter($scope.CredentialsResponce, ['fileType', 'STN'])[0].credentialID;
					var verifiedDocsCount = 0;
					$.each(response.data, function (key, value) {
						$scope.PAN = value.credentialID;
						if (value.isVerified == 1) {
							verifiedDocsCount++;
						}
					});
					if (response.data.length == verifiedDocsCount + 1 || response.data.length == verifiedDocsCount) {
						profileService.updateVerified(1);
					} else {
						profileService.updateVerified(0);
					}
					$scope.uploadedCredentials = response.data;
					if (response.data.length > 0) {
						$scope.editDocVerification = 0;
						$scope.editDocVerification = 0;
					}
				}
			}
		}, function (result) {
			$log.error("error in request service");
		});
	};
	
	$scope.getUserCredentials();

	   
	$scope.panregx = new RegExp("/^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/");
	$scope.panValidations = function (pannumber) {
		
		
		if (pannumber != "" && !$scope.panregx.test(pannumber)) {
			$scope.pannumberalpha = false;
			$scope.pannumberlengthvalidation = false;
			$scope.pannumberrequired = true;
		} else if (('' + pannumber).length < 10) {
			$scope.pannumberalpha = false;
			$scope.pannumberlengthvalidation = true;
			$scope.pannumberrequired = false;
		} else if (!(/^[a-zA-Z0-9]*$/.test(pannumber))) {
			$scope.pannumberalpha = true;
			$scope.pannumberlengthvalidation = false;
			$scope.pannumberrequired = false;
		} else {
			$scope.pannumberalpha = false;
			$scope.pannumberlengthvalidation = false;
			$scope.pannumberrequired = false;
		}
	}
	
	$scope.tinValidations = function (vatnumber) {
		if (vatnumber == "") {
			$scope.tinnumberrequired = true;
		} else {
			$scope.tinnumberrequired = false;
		}
	}
	
	$scope.taxValidations = function (taxnumber) {
		if (taxnumber == "") {
			$scope.taxnumberalpha = false;
			$scope.taxnumberlengthvalidation = false;
			$scope.taxnumberrequired = true;
		} else if (("" + taxnumber).length < 15) {
			$scope.taxnumberalpha = false;
			$scope.taxnumberlengthvalidation = true;
			$scope.taxnumberrequired = false;
		} else if (!(/^[a-zA-Z0-9]*$/.test(taxnumber))) {
			$scope.taxnumberalpha = true;
			$scope.taxnumberlengthvalidation = false;
			$scope.taxnumberrequired = false;
		} else {
			$scope.taxnumberalpha = false;
			$scope.taxnumberlengthvalidation = false;
			$scope.taxnumberrequired = false;
		}
	}
	
	$scope.updateVerficationInfo = function (pannumber, vatnumber, taxnumber) {
		var panObj = _.filter($scope.CredentialsResponce, ['fileType', 'PAN'])[0];
		var tinObj = _.filter($scope.CredentialsResponce, ['fileType', 'TIN'])[0];
		var stnObj = _.filter($scope.CredentialsResponce, ['fileType', 'STN'])[0];

		if ((!$scope.panObject.fileType || pannumber === "" || !(/^[A-Z]{5}\d{4}[A-Z]{1}$/.test(pannumber.toUpperCase()))) && panObj.credentialID != pannumber) {
			$scope.pannumberalpha = false;
			$scope.pannumberlengthvalidation = false;
			$scope.pannumberrequired = false;

			if (pannumber === "") {
				$scope.pannumberrequired = true;
			}
			else {
				if (!(/^[A-Z]{5}\d{4}[A-Z]{1}$/.test(pannumber.toUpperCase()))) {
					$scope.pannumberlengthvalidation = true;
				}
			}
			
			return false;
		}
		else {
			$scope.pannumberalpha = false;
			$scope.pannumberlengthvalidation = false;
			$scope.pannumberrequired = false;
		}
		if ((!$scope.tinObject.fileType || vatnumber === "" || !(/^\d{11}$/.test(vatnumber))) && tinObj.credentialID != vatnumber) {
			$scope.tinnumberrequired = false;
			$scope.tinnumberlengthvalidation = false;
			if (vatnumber === "") {
				$scope.tinnumberrequired = true;
			}
			else {
				if (!(/^\d{11}$/.test(vatnumber.toUpperCase()))) {
					$scope.tinnumberlengthvalidation = true;
				}
			}

			return false;
		}
		else {
			$scope.tinnumberrequired = false;
			$scope.tinnumberlengthvalidation = false;
		}
		if ((!$scope.stnObject.fileType || taxnumber === "" || !(/^[a-zA-Z0-9]{15}$/.test(taxnumber))) && stnObj.credentialID != taxnumber) {
			$scope.taxnumberalpha = false;
			$scope.taxnumberlengthvalidation = false;
			$scope.taxnumberrequired = false;
			if (taxnumber === "") {
				$scope.taxnumberrequired = true;
			}
			else {
				if (!(/^[a-zA-Z0-9]{15}$/.test(taxnumber.toUpperCase()))) {
					$scope.taxnumberlengthvalidation = true;
				}
			}

			return false;
		}
		else {
			$scope.taxnumberalpha = false;
			$scope.taxnumberlengthvalidation = false;
			$scope.taxnumberrequired = false;
		}
		if (pannumber !== "" && (panObj.credentialID != pannumber || $scope.panObject.fileStream)) {
			$scope.panObject.credentialID = pannumber;
			if ($scope.panObject.fileStream) {
				$scope.CredentialUpload.push($scope.panObject);
			}
		}
		if (vatnumber !== "" && (tinObj.credentialID != vatnumber || $scope.tinObject.fileStream)) {
			$scope.tinObject.credentialID = vatnumber;
			if ($scope.tinObject.fileStream) {
				$scope.CredentialUpload.push($scope.tinObject);
			}
		}
		if (taxnumber !== "" && (stnObj.credentialID != taxnumber || $scope.stnObject.fileStream)) {
			$scope.stnObject.credentialID = taxnumber;
			if ($scope.stnObject.fileStream) {
				$scope.CredentialUpload.push($scope.stnObject);
			}
		}
		if ($scope.CredentialUpload.length == 0) {
			growlService.growl("Please enter at least one credential for upload", "inverse");
			return false;
		}
		
		var params = { 'userID': loginService.getUserId(), 'files': $scope.CredentialUpload, 'sessionID': loginService.getUserToken() };
		$http({
			method: 'POST',
			url: domain + 'updatecredentials',
			encodeURI: true,
			headers: { 'Content-Type': 'application/json' },
			data: params
		}).then(function (response) {
			if (response && response.data && response.data.errorMessage == "") {
			   
				$scope.editDocVerification = 0;
			   
				$scope.pannumberalpha = false;
				$scope.pannumberlengthvalidation = false;
				$scope.pannumberrequired = false;
				$scope.taxnumberalpha = false;
				$scope.taxnumberlengthvalidation = false;
				$scope.taxnumberrequired = false;
				$scope.tinnumberrequired = false;
				$scope.getUserCredentials();
				
				swal("Done!", 'Your credentials are being verified. Our associates will contact you as soon as it is done.', 'success');
			  
			} else {
				growlService.growl(response.data.errorMessage, "inverse");
				$log.info(response.data.errorMessage);
			}
		}, function (result) {
			$log.info(result);
		});
		$scope.editDocVerification = 0;	
	}
	
	
	$scope.getFile1 = function (id, doctype, ext) {
		$scope.progress = 0;
		$scope.file = $("#" + id)[0].files[0];
		$scope.docType = doctype + "." + ext;
		fileReader.readAsDataUrl($scope.file, $scope)
			.then(function (result) {
				if (id != "assocWithOEMFile" && id != 'storeLogo') {
					var bytearray = new Uint8Array(result);
					var fileobj = {};
					fileobj.fileStream = $.makeArray(bytearray);
					fileobj.fileType = $scope.docType;
					fileobj.isVerified = 0;
					if (doctype == "PAN") {
						fileobj.credentialID = $scope.pannumber;
						$scope.panObject = fileobj;
					}
					else if (doctype == "TIN") {
						fileobj.credentialID = $scope.vatnumber;
						$scope.tinObject = fileobj;
					}
					else if (doctype == "STN") {
						fileobj.credentialID = $scope.taxnumber;
						$scope.stnObject = fileobj;
					}
				}
			});
	};
		


}]);
	 
	 
	 
	 
	 
							