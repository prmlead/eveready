﻿using PRM.Core.Domain.Companies;
using PRM.Core.Domain.RealTimePrices;
using PRM.Core.Pagers;
using System;
using System.Collections.Generic;

namespace PRM.Services.RealTimePrices
{
    public interface IRealTimePriceService
    {

        void InsertPrice(RealTimePrice entity);

        void InsertPrice(IList<RealTimePrice> entity);

        void UpdatePrice(RealTimePrice entity);

        RealTimePrice GetPriceById(int id);

        IList<RealTimePrice> GetPriceList(object filters);

        IList<RealTimePrice> GetPriceList(string categoryName);

        IPagedList<RealTimePrice> GetPriceList(Pager  pager);

        IList<RealTimePriceReport> GetPriceReport(int typeId, string product, string market = "", object formDate = null, object toDate = null);
        string[] GetCategories(int typeId);

        string[] GetMarkets(int typeId);

        string[] GetProducts(int typeId);

        string[] GetProducts();


        RealTimePriceSetting GetRealTimeSettingByCompany(int companyId);

        void InsertRealTimeSetting(RealTimePriceSetting entity);
        void UpdateRealTimeSetting(RealTimePriceSetting entity);
        RealTimePriceSetting GetRealTimeSettingById(int id);

        IList<RealtimePriceDrop> GetPriceDrop(string[] products, DateTime? compareDate, DateTime? compareWith);
        IList<RealTimePrice> GetPriceThershold(string[] products, DateTime? date);


        IList<CompanyUser> GetCompanyUser(int companyId);

    }
}