﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class PRWorkflowTrack : ResponseAudit
    {
        [DataMember] [DataNames("WF_STATUS")] public string WF_STATUS { get; set; }
        [DataMember] [DataNames("APPROVER_NAME")] public string APPROVER_NAME { get; set; }
        [DataMember] [DataNames("WF_ID")] public int WF_ID { get; set; }
        [DataMember] [DataNames("MODULE_ID")] public int MODULE_ID { get; set; }
    }
}