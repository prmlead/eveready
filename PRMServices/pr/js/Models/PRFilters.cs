﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class PRFilters : Entity
    {
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("PR_NUMBER")] public string PR_NUMBER { get; set; }
        [DataMember] [DataNames("PLANT")] public string PLANT { get; set; }
        [DataMember] [DataNames("PROJECT_TYPE")] public string PROJECT_TYPE { get; set; }
        [DataMember] [DataNames("SECTION_HEAD")] public string SECTION_HEAD { get; set; }
        [DataMember] [DataNames("WBS_CODE")] public string WBS_CODE { get; set; }
        [DataMember] [DataNames("PROFIT_CENTRE")] public string PROFIT_CENTRE { get; set; }
        [DataMember] [DataNames("PURCHASE_GROUP")] public string PURCHASE_GROUP { get; set; }
        [DataMember] [DataNames("CREATOR_NAME")] public string CREATOR_NAME { get; set; }
        [DataMember] [DataNames("CLIENT_NAME")] public string CLIENT_NAME { get; set; }
        [DataMember] [DataNames("PR_STATUS")] public string PR_STATUS { get; set; }
        [DataMember] [DataNames("SEARCH")] public string SEARCH { get; set; }
        [DataMember] [DataNames("FROM_DATE")] public string FROM_DATE { get; set; }
        [DataMember] [DataNames("TO_DATE")] public string TO_DATE { get; set; }
        [DataMember] [DataNames("PAGE_SIZE")] public int PAGE_SIZE { get; set; }
        [DataMember] [DataNames("PAGE")] public int PAGE { get; set; }
        [DataMember] [DataNames("TYPE")] public string TYPE { get; set; }
        [DataMember] [DataNames("EXPORT_DATA")] public bool EXPORT_DATA { get; set; }
    }
}