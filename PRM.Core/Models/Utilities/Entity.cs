﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRM.Core.Models
{
    [DataContract]
    public class Entity
    {
        string eMessage = string.Empty;
        [DataMember(Name = "errorMessage")]
        public string ErrorMessage
        {
            get
            {
                return this.eMessage;
            }
            set
            {
                this.eMessage = value;
            }
        }

        string sessionId = string.Empty;
        [DataMember(Name = "sessionID")]
        public string SessionID
        {
            get
            {
                return this.sessionId;
            }
            set
            {
                this.sessionId = value;
            }
        }

        DateTime dateCreated = DateTime.MaxValue;
        [DataMember(Name = "dateCreated")]
        public DateTime DateCreated
        {
            get
            {
                return this.dateCreated;
            }
            set
            {
                this.dateCreated = value;
            }
        }

        DateTime dateModified = DateTime.MaxValue;
        [DataMember(Name = "dateModified")]
        public DateTime DateModified
        {
            get
            {
                return this.dateModified;
            }
            set
            {
                this.dateModified = value;
            }
        }

        [DataMember(Name = "CreatedBy")]
        public int CreatedBy { get; set; }

        [DataMember(Name = "ModifiedBy")]
        public int ModifiedBy { get; set; }
    }
}