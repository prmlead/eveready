﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class QCSVendorContractItems
    {
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("QCS_ID")] public int QCS_ID { get; set; }
        [DataMember] [DataNames("VENDOR_ID")] public int VENDOR_ID { get; set; }
        [DataMember] [DataNames("PRICE")] public double PRICE { get; set; }
        [DataMember] [DataNames("QUANTITY")] public double QUANTITY { get; set; }
        [DataMember] [DataNames("VENDOR_COMPANY_NAME")] public string VENDOR_COMPANY_NAME { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS")] public string PAYMENT_TERMS { get; set; }
        [DataMember] [DataNames("INCO_TERMS")] public string INCO_TERMS { get; set; }
        [DataMember] [DataNames("GST")] public double GST { get; set; }
        [DataMember] [DataNames("CURRENCY")] public string CURRENCY { get; set; }
        [DataMember] [DataNames("CURRENCY_FACTOR")] public double CURRENCY_FACTOR { get; set; }
        [DataMember] [DataNames("VENDOR_GST_ADDRESS")] public string VENDOR_GST_ADDRESS { get; set; }
        [DataMember] [DataNames("VENDOR_SITE_CODE")] public string VENDOR_SITE_CODE { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string VENDOR_CODE { get; set; }
        [DataMember] [DataNames("PRODUCT_ID")] public int PRODUCT_ID { get; set; }
        [DataMember] [DataNames("ITEM_NAME")] public string ITEM_NAME { get; set; }

    }

    [DataContract]
    public class QCSVendors
    {
        [DataMember] [DataNames("VENDOR_NAME")] public string VENDOR_NAME { get; set; }
        [DataMember] [DataNames("VENDOR_ID")] public int VENDOR_ID { get; set; }

        [DataMember] [DataNames("QCS_VENDOR_CONTRACT_ITEMS")] public List<QCSVendorContractItems> QCS_VENDOR_CONTRACT_ITEMS { get; set; }

    }
}