﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class TechEvalReport : Questionnaire
    {
        

        [DataMember(Name = "vendors")]
        public Answer[] Vendors { get; set; }
    }
}