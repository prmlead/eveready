﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using PRM.Core.Common;

namespace PRMServices.Models.Vendor
{
    [DataContract]
    public class VendorItemAssignment
    {

        [DataMember]
        [DataNames("QCS_VENDOR_ITEM_ID")]
        public int QCS_VENDOR_ITEM_ID { get; set; }

        [DataMember]
        [DataNames("QCS_ID")]
        public int QCS_ID { get; set; }

        [DataMember]
        [DataNames("REQ_ID")]
        public int REQ_ID { get; set; }

        [DataMember]
        [DataNames("VENDOR_ID")]
        public int VENDOR_ID { get; set; }

        [DataMember]
        [DataNames("ITEM_ID")]
        public int ITEM_ID { get; set; }

        [DataMember]
        [DataNames("ASSIGN_QTY")]
        public double ASSIGN_QTY { get; set; }

        [DataMember]
        [DataNames("ASSIGN_PRICE")]
        public double ASSIGN_PRICE { get; set; }

        [DataMember]
        [DataNames("TOTAL_PRICE")]
        public double TOTAL_PRICE { get; set; }

        [DataMember]
        [DataNames("PO_ID")]
        public string PO_ID { get; set; }

        [DataMember]
        [DataNames("REQ_TYPE")]
        public string REQ_TYPE { get; set; }

        [DataMember]
        [DataNames("IS_PROCESSED")]
        public int IS_PROCESSED { get; set; }

        [DataMember]
        [DataNames("REQ_CURRENCY_FACTOR")]
        public double REQ_CURRENCY_FACTOR { get; set; }

        [DataMember]
        [DataNames("PLANT_CODE")]
        public string PLANT_CODE { get; set; }

        [DataMember]
        [DataNames("VENDOR_SITE_CODE")]
        public string VENDOR_SITE_CODE { get; set; }

        [DataMember]
        [DataNames("VENDOR_CODE")]
        public string VENDOR_CODE { get; set; }

        [DataMember]
        [DataNames("PR_NUMBER")]
        public string PR_NUMBER { get; set; }

        [DataMember]
        [DataNames("PR_ITEM_ID")]
        public int PR_ITEM_ID { get; set; }
    }
}