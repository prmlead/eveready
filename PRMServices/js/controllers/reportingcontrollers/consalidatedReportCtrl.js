﻿
prmApp
    .controller('consalidatedReportCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;
            $scope.consalidatedReport = [];

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 8;
            $scope.revisedUserL1 = [];


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
            };

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            //$scope.reportFromDate = '';
            //$scope.reportToDate = '';

            $scope.reportToDate = moment().format('YYYY-MM-DD');
            $scope.reportFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");


            $scope.getConsalidatedReport = function () {
                $scope.errMessage = '';

                //var ts = userService.toUTCTicks($scope.reportFromDate);
                //var m = moment(ts);
                //var quotationDate = new Date(m);
                //var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                //var reportFromDate = "/Date(" + milliseconds + "000+0530)/";

                //var ts = userService.toUTCTicks($scope.reportToDate);
                //var m = moment(ts);
                //var quotationDate = new Date(m);
                //var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                //var reportToDate = "/Date(" + milliseconds + "000+0530)/";

                reportingService.getConsolidatedReport($scope.reportFromDate, $scope.reportToDate)
                    .then(function (response) {
                        $scope.consalidatedReport = response;

                        $scope.totalItems = $scope.consalidatedReport.length;
                        $scope.consalidatedReport.forEach(function (item, index) {
                            //item.PLANT_CODES_TEMP = item.PLANT_CODES;
                            if (item.PLANT_CODES && item.prNumbers) {
                                item.PLANT_CODES = Array.from(new Set(item.PLANT_CODES.split(','))).toString();
                            } else {
                                item.PLANT_CODES = "NA";
                            }
                            item.closed = $scope.prmStatus($scope.isCustomer, item.closed);
                            item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                            item.reqPostedOn = $scope.GetDateconverted(item.reqPostedOn);
                            item.startTime = $scope.GetDateconverted(item.startTime);

                            if (String(item.startTime).includes('9999') || String(item.startTime).includes('10000')) {
                                item.startTime = '';
                            }
                        });
                    });
            };

            $scope.getConsalidatedReport();

            $scope.GetReport = function () {
                
                

                alasql('SELECT requirementNumber as [Requirement Number],title as [Requirement Title],prNumbers as [PR Numbers], PLANT_CODES as [Plants],reqCategory as [Category], ' +
                    'closed as Status, ' +
                    'reqPostedOn as [Posted On], quotationFreezTime as [Freez Time], startTime as [Negotiation scheduled time],IL1_vendTotalPrice as [Initial Least Price], ' +
                    'RL1_companyName as [L1 Company Name],  RL1_revVendTotalPrice as [L1 Rev Price], ' + 
                    'RL2_companyName as [L2 Company Name],  RL2_revVendTotalPrice as [L2 Rev Price],  ' +
                    'basePriceSavings as [Savings],savingsPercentage as [Savings %] ' +
                    'INTO XLSX(?, { headers: true, sheetid: "ConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                    ["ConsolidatedReport.xlsx", $scope.consalidatedReport]);


            }
            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };


        }]);