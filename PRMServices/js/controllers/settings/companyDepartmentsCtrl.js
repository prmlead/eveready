﻿prmApp
    .controller('companyDepartmentsCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService","PRMPRServices",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();

            $scope.companyDepartments = [];

            $scope.CompanyDeptDesigTypes = [];

            $scope.displayCard = 0;

            $scope.companyBranches = [];

            $scope.compID = userService.getUserCompanyId();

            $scope.department = {
                userID: $scope.userID,
                deptID: 0,
                deptCode: '',
                deptDesc: '',
                deptAdmin: $scope.userID,
                sessionID: $scope.sessionID,
                selectedDeptType: {},
                deptBranch: '',
                deptPlants: []
            };


            $scope.PlantsList = [];


            $scope.addnewdeptView = false;
            //$scope.showPlants = true;

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/

            auctionsService.GetCompanyConfiguration($scope.compID, "BRANCH", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyBranches = unitResponse;
            });

            $scope.GetCompanyDepartments = function () {
                auctionsService.GetCompanyDepartments($scope.userID, $scope.sessionID)
                    .then(function (response) {
                        $scope.companyDepartments = response;
                        $scope.companyDepartments1 = response;

                        $scope.totalItems = $scope.companyDepartments.length;
                    })
            }

            $scope.GetCompanyDepartments();


            $scope.getprfieldmapping = function (type) {
                var params = {
                    //"type": "'" + type + "'"
                    "type": type
                };

                PRMPRServices.getprfieldmapping(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.PlantsList = $scope.PlantsList.concat(response.filter(function (item) { item.isValid = false; return item.FIELD_TYPE === 'PLANTS'; }));
                        }
                    });

            };

            $scope.getprfieldmapping("'PLANTS', 'STATUS', 'DOC_TYPE', 'MATERIAL_GROUP', 'PURCHASE_GROUP'");


            $scope.SaveCompanyDepartment = function () {
               
                if ($scope.department.deptCode == undefined || $scope.department.deptCode == "") {
                    growlService.growl("Please Enter Department Name.", 'inverse');
                    return;
                }

                if ($scope.department.deptBranch == undefined || $scope.department.deptBranch == "") {
                    growlService.growl("Please Select Department Branch.", 'inverse');
                    return;
                }

                if ($scope.department.selectedDeptType.typeID == undefined || $scope.department.selectedDeptType.typeID == "" || $scope.department.selectedDeptType.typeID <= 0) {
                    growlService.growl("Please Select Department Type.", 'inverse');
                    return;
                }

                //$scope.department.plants.push(plant);
                
                var params = {
                    "companyDepartment": $scope.department
                };
                
                auctionsService.SaveCompanyDepartment(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Department Saved Successfully.", "success");
                            //location.reload();
                            $scope.GetCompanyDepartments();
                            $scope.addnewdeptView = false;
                            $scope.department = {
                                userID: $scope.userID,
                                deptID: 0,
                                deptAdmin: $scope.userID,
                                sessionID: $scope.sessionID,
                                selectedDeptType: {},
                                deptBranch: '',
                                deptPlants : []
                            };
                        }
                    });

            };

            $scope.editDepartment = function (companyDepartment) {
                $scope.PlantsList.forEach(function (item, index) {
                    item.isValid = false;
                });
                $scope.addnewdeptView = true;
                //$scope.showPlants = false;
                $scope.department = companyDepartment;
                //$scope.department.selectedDeptType = companyDepartment.selectedDeptType.typeID;
                $scope.department.selectedDeptType.typeID = companyDepartment.selectedDeptType.typeID;
                $scope.department.sessionID = $scope.sessionID;
                $scope.deptAdmin = $scope.userID;
                $scope.department.userID = $scope.userID;
                if (companyDepartment.deptPlants && companyDepartment.deptPlants.length >0) {
                    if ($scope.PlantsList.length > 0) {
                        $scope.PlantsList.forEach(function (item, index) {
                            if (companyDepartment.deptPlants.indexOf(+item.FIELD_NAME) !== -1) {
                                item.isValid = true;
                            } else {
                                item.isValid = false;
                            }
                        });
                    }
                }

                document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                document.documentElement.scrollTop = 0; // For IE and Firefox
            };

            $scope.closeEditDepartment = function () {
                $scope.addnewdeptView = false;
                $scope.department = {
                    userID: $scope.userID,
                    deptID: 0,
                    deptCode: '',
                    deptAdmin: $scope.userID,
                    sessionID: $scope.sessionID,
                    selectedDeptType: {},
                    deptPlants: []
                };
            };


            $scope.DeleteDepartment = function (companyDepartment) {

                var params = {
                    deptID: companyDepartment.deptID,
                    sessionID: $scope.sessionID
                };

                auctionsService.DeleteDepartment(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Department Deleted Successfully.", "success");
                            $scope.GetCompanyDepartments();
                            //$window.history.back();
                        }
                    });
                $scope.addnewdeptView = false;
            };

            $scope.departmentUsers = [];
            $scope.deptUsersView = false;
            $scope.deptCode = '';

            $scope.GetDepartmentUsers = function (companyDepartment) {
                auctionsService.GetDepartmentUsers(companyDepartment.deptID, $scope.userID, $scope.sessionID)
                    .then(function (response) {
                        $scope.departmentUsers = response;
                    })
                $scope.deptCode = companyDepartment.deptCode;
                $scope.deptUsersView = true;

                document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                document.documentElement.scrollTop = 0; // For IE and Firefox
            }

            $scope.SaveUserDepartments = function () {

                var params = {
                    "listUserDepartments": $scope.departmentUsers,
                    "sessionID": userService.getUserToken()
                };

                auctionsService.SaveUserDepartments(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            //swal("Error!", 'Not Saved', 'error');

                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {

                            growlService.growl("Users Added to Department Successfully.", "success");
                            $scope.deptUsersView = false;
                        }
                    })
            };

            $scope.cancelDeptUsersView = function () {
                $scope.deptUsersView = false;
                $scope.deptCode = '';
            };

          $scope.GetCompanyDeptDesigTypes = function () {
                 auctionsService.GetCompanyDeptDesigTypes($scope.userID, 'DEPT', $scope.sessionID)
                     .then(function (response) {
                         $scope.CompanyDeptDesigTypes = response;
                         $log.info($scope.CompanyDeptDesigTypes);
                     })
             };
 
             $scope.GetCompanyDeptDesigTypes(); 

            $scope.WorkCategories = [];
            $scope.NewWorkCategories = [];

            $scope.WorkCat = {
                userID: $scope.userID,
                sessionID: $scope.sessionID,
                workCatID: 0,
                workCatCode: '',
                workCatDesc: '',
                isValid: 1,
                isNew: true,
                deptID: 0
            };

            $scope.NewWorkCategoriesLength = 0;

       
             $scope.GetWorkCategories = function () {
                 auctionsService.GetWorkCategories($scope.userID, $scope.sessionID)
                     .then(function (response) {
                         $scope.WorkCategories = response;
                         $scope.NewWorkCategoriesLength = 0;
                     })
             }
 
             $scope.GetWorkCategories();
 
           

            $scope.AddCat = function () {

                $scope.WorkCat = {
                    userID: $scope.userID,
                    sessionID: $scope.sessionID,
                    workCatID: 0,
                    workCatCode: '',
                    workCatDesc: '',
                    isValid: 1,
                    isNew: true,
                    deptID: 0
                };

                $scope.WorkCategories.push($scope.WorkCat);

                $scope.NewWorkCategories = $scope.WorkCategories.filter(function (cat) {
                    return (cat.isNew == true);
                });

                $scope.NewWorkCategoriesLength = $scope.NewWorkCategories.length;
            }



            $scope.DelCat = function (index) {

                $scope.WorkCategories.splice(index, 1);

                $scope.NewWorkCategories = $scope.WorkCategories.filter(function (cat) {
                    return (cat.isNew == true);
                });

                $scope.NewWorkCategoriesLength = $scope.NewWorkCategories.length;
            }


            $scope.SaveWorkCategories = function () {

                $scope.NewWorkCategories = $scope.WorkCategories.filter(function (cat) {
                    return (cat.isNew == true && cat.workCatCode != '' && cat.workCatCode != undefined && cat.deptID > 0);
                });

                if (!$scope.NewWorkCategories || $scope.NewWorkCategories.length <= 0) {
                    growlService.growl('Please check all the fields', "inverse");
                    return;
                }

                var params = {
                    "listWorkCategories": $scope.NewWorkCategories,
                    "sessionID": userService.getUserToken()
                };

                auctionsService.SaveWorkCategory(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {

                            growlService.growl("Saved Successfully.", "success");
                            $scope.GetWorkCategories();
                        }
                    })
            };














            $scope.BudgetCodes = [];
            $scope.NewBudgetCodes = [];

            $scope.BudgetCode = {
                userID: $scope.userID,
                sessionID: $scope.sessionID,
                budgetID: 0,
                budgetCode: '',
                budgetDesc: '',
                isValid: 1,
                isNew: true,
                deptID: 0,
                catID: 0
            };

            $scope.NewBudgetCodesLength = 0;

            $scope.GetBudgetCodes = function () {
                  auctionsService.GetBudgetCodes($scope.userID, $scope.sessionID)
                      .then(function (response) {
                          $scope.BudgetCodes = response;
                          $scope.NewBudgetCodesLength = 0;
                      })
              }
  
              //$scope.GetBudgetCodes();



            $scope.AddBudgetCode = function () {

                $scope.BudgetCode = {
                    userID: $scope.userID,
                    sessionID: $scope.sessionID,
                    budgetID: 0,
                    budgetCode: '',
                    budgetDesc: '',
                    isValid: 1,
                    isNew: true
                };

                $scope.BudgetCodes.push($scope.BudgetCode);

                $scope.NewBudgetCodes = $scope.BudgetCodes.filter(function (bc) {
                    return (bc.isNew == true);
                });

                $scope.NewBudgetCodesLength = $scope.NewBudgetCodes.length;
            }



            $scope.DelBudgetCode = function (index) {

                $scope.BudgetCodes.splice(index, 1);

                $scope.NewBudgetCodes = $scope.BudgetCodes.filter(function (bc) {
                    return (bc.isNew == true);
                });

                $scope.NewBudgetCodesLength = $scope.NewBudgetCodes.length;
            }


            $scope.SaveBudgetCode = function () {

                $scope.NewBudgetCodes = $scope.BudgetCodes.filter(function (bc) {
                    return (bc.isNew == true && bc.budgetCode != '' && bc.budgetCode != undefined && bc.deptID > 0);
                });

                if (!$scope.NewBudgetCodes || $scope.NewBudgetCodes.length <= 0) {
                    growlService.growl('Please check all the fields', "inverse");
                    return;
                }

                var params = {
                    "listBudgetCodes": $scope.NewBudgetCodes,
                    "sessionID": userService.getUserToken()
                };

                auctionsService.SaveBudgetCode(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {

                            growlService.growl("Saved Successfully.", "success");
                            $scope.GetBudgetCodes();
                        }
                    })
            };


            $scope.selectedPlants = function (validPlant, index) {
              
                if (validPlant && validPlant.isValid) {
                    $scope.department.deptPlants.push(+validPlant.FIELD_NAME);
                } else {
                    if (!validPlant.isValid) {
                        /***** Code is because of two different indexes *******/
                        $scope.PlantsList.forEach(function (item, plantIndex) {
                            $scope.department.deptPlants.forEach(function (item1, index1) {
                                if (+item.FIELD_VALUE.indexOf(+validPlant.FIELD_VALUE) > -1) {
                                    if (+validPlant.FIELD_VALUE.indexOf(item1) > -1) {
                                        index = index1;
                                    }
                                }
                            });
                        });
                        $scope.department.deptPlants.splice(index, 1);
                        /***** Code is because of two different indexes *******/
                    }
                }
            }

            $scope.searchTable = function () {

                if ($scope.searchKeyword) {
                    $scope.companyDepartments = _.filter($scope.companyDepartments1, function (item) {
                        return (item.deptCode.toUpperCase().indexOf($scope.searchKeyword.toUpperCase()) > -1
                            || item.deptDesc.toUpperCase().indexOf($scope.searchKeyword.toUpperCase()) > -1
                        );
                    });
                } else {
                    $scope.companyDepartments = $scope.companyDepartments1;
                }
                $scope.totalItems = $scope.companyDepartments.length;
                

            };
        }]);