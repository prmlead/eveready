﻿using System;
using System.Collections.Generic;
using PRMServices.Models;
using System.Net.Mail;
using System.ComponentModel;
using System.Xml;
using System.IO;

namespace PRMServices.Common
{
    public class PRMForwardNotifications
    {
        public void AuctionStartNotification(int reqID, int userID, string sessionID, List<Attachment> vendorAttachments, string watchers)
        {
            PRMForwardBid prm = new PRMForwardBid();
            string ScreenName = "NEGOTIATION_START_TIME";
            
            Requirement req = prm.GetRequirementDataOffline(reqID, userID, sessionID);
            req.Module = ScreenName;
            req.StartTime = Utilities.toLocal(req.StartTime);
            foreach (VendorDetails vendor in req.AuctionVendors)
            {
                string body = Utilities.GenerateEmailBodyForForwardAuction("VendoremailForAuctionStartUpdate"); 
                UserInfo user = Utilities.GetUserNew(vendor.VendorID);
                User altUser = prm.GetReqAlternateCommunications(reqID, vendor.VendorID);
                body = String.Format(body, user.FirstName, user.LastName, req.RequirementNumber, req.StartTime, req.Title, req.RequirementID);
                prm.SendEmail(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "Negotiation Start time has been updated for Req Number: " + req.RequirementNumber + " Title:  " + req.Title, body, reqID, vendor.VendorID, req.Module, sessionID, null, vendorAttachments, req.StartTime, 15, "NEGOTATION_START", string.Empty).ConfigureAwait(false);
                string message = Utilities.GenerateEmailBodyForForwardAuction("VendorsmsForAuctionStartUpdate");
                message = String.Format(message, user.FirstName, user.LastName, reqID, req.StartTime, req.Title);
                message = message.Replace("<br/>", "");
            }

            if (req.Status != Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString()))
            {
                string body1 = Utilities.GenerateEmailBodyForForwardAuction("CustomeremailForAuctionStartUpdate");
                UserInfo user1 = Utilities.GetUserNew(userID);
                body1 = String.Format(body1, user1.FirstName, user1.LastName, req.RequirementNumber, req.StartTime, req.Title, req.RequirementID);
                prm.SendEmail(user1.Email + "," + user1.AltEmail, "Negotiation Start time has been updated for Req Number: " + req.RequirementNumber + " Title: " + req.Title, body1, reqID,Convert.ToInt32(user1.UserID), req.Module, sessionID, null, null, req.StartTime, 15, "NEGOTATION_START", watchers).ConfigureAwait(false);

                string message1 = Utilities.GenerateEmailBodyForForwardAuction("CustomersmsForAuctionStartUpdate");
                message1 = String.Format(message1, user1.FirstName, user1.LastName, req.RequirementNumber, req.StartTime, req.Title);
                message1 = message1.Replace("<br/>", "");

                UserInfo superUser = Utilities.GetSuperUser(userID);

                string bodySuper = Utilities.GenerateEmailBodyForForwardAuction("CustomeremailForAuctionStartUpdate");
                bodySuper = String.Format(bodySuper, superUser.FirstName, superUser.LastName, req.RequirementNumber, req.StartTime, req.Title, req.RequirementID);

                string body2Super = Utilities.GenerateEmailBodyForForwardAuction("CustomersmsForAuctionStartUpdate");
                body2Super = String.Format(body2Super, superUser.FirstName, superUser.LastName, req.RequirementNumber, req.StartTime, req.Title);
                body2Super = body2Super.Replace("<br/>", "");
                string subUserID = req.CustomerID.ToString();               
                string bodyTelegram = Utilities.GenerateEmailBodyForForwardAuction("UpdateStartTimeTelegramsms");
                bodyTelegram = String.Format(bodyTelegram, req.CustomerCompanyName, req.CustFirstName, req.CustLastName, req.Title, req.Description, req.StartTime);
                bodyTelegram = bodyTelegram.Replace("<br/>", "");
            }
        }
    }
}