﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('moduleErrorList', {
                    url: '/moduleErrorList/:moduleName',
                    templateUrl: 'ExcelUpload/views/moduleErrorList.html'
                });

        }]);﻿prmApp
    .controller('moduleErrorListCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPOService", "poService",
        "PRMCustomFieldService", "fileReader", "$uibModal", "$filter", "$http", "poDomain","PRMUploadServices","$element",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter, $http, poDomain, PRMUploadServices, $element) {

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;
            $scope.moduleName = $stateParams.moduleName;

            $scope.getModuleBasedErrorDetails = function () {
                $scope.modulErrorList = [];
                PRMUploadServices.getModuleBasedErrorDetails({ "MODULE": $scope.moduleName }).then(function (response) {
                    $scope.modulErrorList = response;
                    $scope.totalItems = $scope.modulErrorList.length;
                });
            };

            $scope.getModuleBasedErrorDetails();


            $scope.getDate = function (value) {
                return userService.toLocalDate(value);
            }

        }]);prmApp.constant('PRMUploadServicesDomain', 'ExcelUpload/svc/PRMUploadService.svc/REST/');
prmApp.service('PRMUploadServices', ["PRMUploadServicesDomain", "userService", "httpServices","$window",
    function (PRMUploadServicesDomain, userService, httpServices, $window) {

        var PRMUploadServices = this;

        PRMUploadServices.GetExcelTemplate = function (templateName) {
            var url = PRMUploadServicesDomain + 'getExcelUploadTemplates?templateName=' + templateName + '&compid=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", templateName + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }


        PRMUploadServices.uploadTemplate = function (params) {
            params.U_ID = +userService.getUserId();
            var url = PRMUploadServicesDomain + 'UploadTemplate';
            return httpServices.post(url, params);
        };

        PRMUploadServices.getModuleBasedErrorDetails = function (params) {
            var url = PRMUploadServicesDomain + 'getModuleBasedErrorDetails?MODULE=' + params.MODULE + '&compid=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };
        
        return PRMUploadServices;

}]);