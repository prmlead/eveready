﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Owin;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Newtonsoft.Json;

[assembly: OwinStartup(typeof(PRMServices.SignalR.Startup))]

namespace PRMServices.SignalR
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //var config = new HttpConfiguration();
            //config.Routes.MapHubs(new HubConfiguration() { });
            //// For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            // Branch the pipeline here for requests that start with "/signalr"
            /*app.Map("/signalr", map =>
            {
                // Setup the CORS middleware to run before SignalR.
                // By default this will allow all origins. You can 
                // configure the set of origins and/or http verbs by
                // providing a cors options with a different policy.
                map.UseCors(CorsOptions.AllowAll);
                var hubConfiguration = new HubConfiguration()
                {
                    EnableDetailedErrors = true,
                    EnableJSONP = true,
                    EnableJavaScriptProxies = true
                };
                // Run the SignalR pipeline. We're not using MapSignalR
                // since this branch already runs under the "/signalr"
                // path.
                map.RunSignalR(hubConfiguration);
            });*/
            //app.MapSignalR("/~/signalr", new HubConfiguration()
            //{
            //    EnableDetailedErrors = true,
            //    EnableJSONP = true
            //});

            app.Map("/~/signalr", map =>
            {
                map.UseCors(CorsOptions.AllowAll);
                var hubConfiguration = new HubConfiguration () {
                    EnableDetailedErrors = true,
                    EnableJSONP = true
                };
                map.RunSignalR(hubConfiguration);
            });

            var jsonSerializer = new JsonSerializer();
            jsonSerializer.DateFormatHandling = DateFormatHandling.MicrosoftDateFormat;
            jsonSerializer.DateTimeZoneHandling = DateTimeZoneHandling.Local;
            jsonSerializer.NullValueHandling = NullValueHandling.Ignore;
            GlobalHost.DependencyResolver.Register(typeof(JsonSerializer), () => jsonSerializer);
        }
    }
}
