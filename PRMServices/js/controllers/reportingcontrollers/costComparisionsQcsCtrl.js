﻿
prmApp
    .controller('costComparisionsQcsCtrl', ["$scope", "$state", "$log", "$stateParams", "$q", "userService", "auctionsService", "$window",
        "$timeout", "reportingService", "growlService", "workflowService", "PRMCustomFieldService", "catalogService", "PRMPOServices", "fileReader", '$location', "$http", "domain", "PRMPRServices", "$q",
        function ($scope, $state, $log, $stateParams, $q, userService, auctionsService, $window,
            $timeout, reportingService, growlService, workflowService, PRMCustomFieldService, catalogService, PRMPOServices, fileReader, $location, $http, domain, PRMPRServices, $q) {

            $scope.reqId = $stateParams.reqID;
            $scope.qcsID = +$stateParams.qcsID;
            $scope.companyId = userService.getUserCompanyId();
            $scope.requirementDetails = {};
            $scope.qcsRequirementDetails;
            $scope.isUOMDifferent = false;
            $scope.isTechSpecExport = false;
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            // $scope.isSuperUser = userService.getUserObj().isSuperUser;
            if (!$scope.isCustomer) {
                $state.go('home');
            }
            $scope.isQcsSaveDisable = false;
            //$scope.includeGstInCal = true;
            $scope.userId = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.qcsVendors = [];
            $scope.qcsItems = [];
            $scope.qcsCoreItems = [];
            $scope.vendorSAPDetails = [];
            $scope.obj = {
                auctionVendors: []
            };
            $scope.objNew = {
                auctionVendors: []
            };
            $scope.editForm = false;
            $scope.deptIDs = [];
            $scope.desigIDs = [];
            $scope.vendorWidth = 12;
            $scope.requirementItemContracts = [];
            $scope.totalAttachmentMaxSize = 6291456;
            $scope.requirementSettings = [];
            $scope.selectedTemplate = {};
            $scope.prmFieldMappingDetails = {};
            $scope.DraftPOList = [];
            $scope.IncoTermsEditableForOtherCharges = [];
            $scope.qcsAllocateList = [];
            $scope.autoAllocatearray = [];
            $scope.objectID = 0;
            var qcsvendorAssignedItems = [];
            var qcsassignList = []


            $scope.vendorAssignmentList = [];
            $scope.vendorAssignmentList.push({
                qcsVendorItemId: 0,
                vendorID: 0,
                itemID: 0,
                item: null,
                assignedQty: 0,
                assignedPrice: 0,
                totalPrice: 0,
                poID: '',
                currency: ''
            });

            $scope.stateDetails = {
                poTemplate: 'po-domestic-zsdm'
            };



            $scope.getCompanyPlants = function () {
                auctionsService.getCompanyPlants($scope.companyId, $scope.userId, $scope.sessionID)
                    .then(function (response) {
                        $scope.companyPlants = response;
                    });
            };

            $scope.getCompanyPlants();

            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                    $scope.deptIDs.push(item.deptID);
                    item.listDesignation.forEach(function (item1, index1) {
                        if (item1.isAssignedToUser && item1.isValid) {
                            $scope.desigIDs.push(item1.desigID);
                        }
                    });
                });
            }

            $scope.listRequirementTaxes = [];
            $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
            $scope.uomDetails = [];

            $scope.QCSREQIDS = [];
            $scope.GetQCSIDS = function () {
                var params = {
                    "reqid": $scope.reqId,
                    "sessionid": userService.getUserToken()
                };
                reportingService.GetQCSIDS(params)
                    .then(function (response) {
                        $scope.QCSREQIDS = response;
                    });
            };
            $scope.GetQCSIDS();


            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.obj = {
                auctionVendors: []
            };
            $scope.objNew = {
                auctionVendors: []
            };
            $scope.workflowObj = {};
            $scope.workflowObj.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'QCS';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/

            $scope.vendorPrices = [];


            $scope.QCSDetails = {
                QCS_ID: 0,
                REQ_ID: $scope.reqId,
                U_ID: userService.getUserId(),
                QCS_CODE: '',
                PO_CODE: '',
                RECOMMENDATIONS: '',
                UNIT_CODE: '',
                IS_TAX_INCLUDED: '',
                IS_VALID: 1
            };

            if ($scope.qcsID <= 0) {
                $scope.QCSDetails.IS_VALID = 1;
            }

            $scope.doPrint = false;
            $scope.printReport = function () {
                $scope.doPrint = true;
                $timeout(function () {
                    $window.print();
                    $scope.doPrint = false;
                }, 1000);
            };

            $scope.htmlToCanvasSaveLoading = false;
            $scope.htmlToCanvasSave = function (format) {
                $scope.htmlToCanvasSaveLoading = true;
                setTimeout(function () {
                    try {
                        var name = "Comparisions-ReqID-" + $scope.reqId + "." + format;
                        var canvas = document.createElement("canvas");
                        if (format == 'pdf') {
                            document.getElementById("widget").style["display"] = "";
                        }
                        else {
                            document.getElementById("widget").style["display"] = "inline-block";
                        }

                        html2canvas($("#widget"), {
                            onrendered: function (canvas) {
                                theCanvas = canvas;

                                const a = document.createElement("a");
                                a.style = "display: none";
                                a.href = canvas.toDataURL();

                                // Add Image to HTML
                                //document.body.appendChild(canvas);

                                /* Save As PDF */
                                if (format == 'pdf') {
                                    var imgData = canvas.toDataURL();
                                    var pdf = new jsPDF();
                                    pdf.addImage(imgData, 'JPEG', 0, 0);
                                    pdf.save(name);
                                }
                                else {
                                    a.download = name;
                                    a.click();
                                }

                                // Clean up 
                                //document.body.removeChild(canvas);
                                document.getElementById("widget").style["display"] = "";
                                $scope.htmlToCanvasSaveLoading = false;
                            }
                        });
                    }
                    catch (err) {
                        document.getElementById("widget").style["display"] = "";
                        $scope.htmlToCanvasSaveLoading = false;
                    }

                }, 500);

            };

            $scope.getTotalTax = function (quotation) {
                if (quotation) {
                    if (!quotation.cGst) {
                        quotation.cGst = 0;
                    }

                    if (!quotation.sGst) {
                        quotation.sGst = 0;
                    }

                    if (!quotation.iGst) {
                        quotation.iGst = 0;
                    }

                    return quotation.cGst + quotation.sGst + quotation.iGst;

                } else {
                    return 0;
                }
            };

            $scope.SaveQCSDetails = function (val) {
                let attachmentPromises = [];
                $scope.requirementDetails.qcsMultipleAttachments.forEach(function (fileObj, index) {
                    if (fileObj && fileObj.fileStream && fileObj.fileStream.length > 0 && !fileObj.fileID) {
                        let param = {
                            file: fileObj,
                            user: $scope.userId,
                            sessionid: $scope.sessionID
                        };

                        var promise = auctionsService.saveAttachment(param);
                        attachmentPromises.push(promise);
                    }
                });

                $q.all(attachmentPromises).then(function (responses) {
                    if (responses && responses.length > 0) {
                        responses.forEach(function (file, idx) {
                            file.fileStream = [];
                        });

                        $scope.requirementDetails.qcsMultipleAttachments = responses;
                    }

                    $scope.QCSDetails.QCS_TYPE = 'DOMESTIC';
                    if (!$scope.QCSDetails.QCS_CODE) {
                        $scope.QCSDetails.QCS_CODE = new Date().getUTCMilliseconds();
                    }

                    $scope.QCSDetails.REQ_JSON = JSON.stringify($scope.requirementDetails);
                    if ($scope.QCSDetails.CREATED_USER == null) {
                        $scope.QCSDetails.CREATED_USER = $scope.userId;
                    }
                    if ($scope.QCSDetails.U_ID == 0 || $scope.QCSDetails.U_ID == null) {
                        $scope.QCSDetails.U_ID = $scope.userId;
                    }

                    $scope.QCSDetails.IS_TAX_INCLUDED = $scope.requirementDetails.includeGstInCal;
                    $scope.QCSDetails.REQ_TITLE = $scope.requirementDetails.title;

                    $scope.QCSDetails.APPROVER_RANGE = calculateApproverRangeFromLandedCost($scope.QCSDetails.REQ_JSON);

                    var vendorAssignedItems = [];
                    var requirementDetails_temp = JSON.parse($scope.QCSDetails.REQ_JSON);

                    if (requirementDetails_temp) {
                        requirementDetails_temp.auctionVendors.forEach(function (vendor, idx) {
                            vendor.listRequirementItems.forEach(function (item, itemIdx) {
                                if (item.qtyDistributed > 0) {
                                    vendorAssignedItems.push({
                                        QCS_ID: $stateParams.qcsID,
                                        REQ_ID: $stateParams.reqID,
                                        vendorID: vendor.vendorID,
                                        itemID: item.itemID,
                                        assignedQty: item.qtyDistributed,
                                        assignedPrice: item.revUnitPrice,
                                        totalPrice: (item.revUnitPrice * item.qtyDistributed),
                                        PLANT_CODE: vendor.selectedSiteCode.split('_')[0],
                                        VENDOR_SITE_CODE: vendor.selectedSiteCode,
                                        VENDOR_CODE: vendor.vendorCode
                                    });
                                }
                                item.calculateSavings = calculateValues(item, requirementDetails_temp, vendor);
                            });
                            vendor.VENDOR_PURCHASE_GROUP_SAVINGS = _.sumBy(vendor.listRequirementItems, 'calculateSavings');
                            vendor.VENDOR_TOTAL_VALUE = $scope.getVendorAssignedQtyTotalLandingPrice(vendor, $scope.requirementDetails.includeGstInCal, true);
                        });
                    }

                    var value = _.sumBy(requirementDetails_temp.auctionVendors, 'VENDOR_PURCHASE_GROUP_SAVINGS');
                    $scope.QCSDetails.TOTAL_PROFIT = +value > 0 ? +(value * $scope.getRequirementCurrencyFactor($scope.requirementDetails.currency)) : 0;
                    $scope.QCSDetails.TOTAL_LOSS = +value <= 0 ? +(value * $scope.getRequirementCurrencyFactor($scope.requirementDetails.currency)) : 0;
                    $scope.QCSDetails.SAVINGS = +value;
                    $scope.QCSDetails.SAVINGS_IN_REQUIRED_CURRENCY = $scope.requirementDetails.currency === 'INR' ? +value : (+value * $scope.getRequirementCurrencyFactor($scope.requirementDetails.currency));
                    $scope.QCSDetails.TOTAL_PROCUREMENT_VALUE = _.sumBy(requirementDetails_temp.auctionVendors, 'VENDOR_TOTAL_VALUE');
                    $scope.QCSDetails.TOTAL_PROCUREMENT_VALUE_IN_REQUIRED_CURRENCY = $scope.requirementDetails.currency === 'INR' ? _.sumBy(requirementDetails_temp.auctionVendors, 'VENDOR_TOTAL_VALUE') : (_.sumBy(requirementDetails_temp.auctionVendors, 'VENDOR_TOTAL_VALUE') * $scope.getRequirementCurrencyFactor($scope.requirementDetails.currency));
                    $scope.QCSDetails.APPROVAL_COUNT = $scope.workflowObj.workflowID ? calculateApproversCount($scope.workflowObj.workflowID) : 0;
                    if ($scope.objectID > 0) {
                        $scope.QCSDetails.VENDOR_ITEM_ASSIGNMENT = retrieveVendorItems(qcsvendorAssignedItems);
                    } else {
                        $scope.QCSDetails.VENDOR_ITEM_ASSIGNMENT = retrieveVendorItems(vendorAssignedItems);
                    }

                    var params = {
                        "qcsdetails": $scope.QCSDetails,
                        "sessionid": userService.getUserToken()
                    };

                    if ($scope.workflowObj.workflowID) {
                        params.qcsdetails.WF_ID = $scope.workflowObj.workflowID;
                    }

                    //if (!params.qcsdetails.WF_ID > 0) {
                    //    growlService.growl('Please select Workflow', "inverse");
                    //    return;
                    //}
                    reportingService.SaveQCSDetails(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //if (vendorAssignedItems.length > 0) {
                                //    $scope.saveVendorAssignments(vendorAssignedItems, response.objectID);
                                //}
                                if (val == 1) {
                                    $scope.goToQCSList($scope.reqId);
                                    growlService.growl("Details saved successfully.", "success");
                                } else {
                                    var url = $state.href("cost-comparisions-qcs", { "reqID": $scope.reqId, "qcsID": response.objectID });
                                    window.open(url, '_self');
                                }
                            }
                        });
                });
            };

            $scope.isQCSFormdisabled = true;

            $scope.checkIsFormDisable = function () {
                $scope.isQCSFormdisabled = false;
                if ($scope.itemWorkflow.length == 0) {
                    $scope.isQCSFormdisabled = true;
                } else {
                    if (($scope.QCSDetails.CREATED_BY == +userService.getUserId() || $scope.QCSDetails.MODIFIED_BY == +userService.getUserId()) && $scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
                        $scope.itemWorkflow[0].WorkflowTracks[0].status !== "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[0].order == 1 && $scope.itemWorkflow[0].workflowID > 0) {
                        $scope.isQCSFormdisabled = true;
                    }
                }

            };

            $scope.GetQCSDetails = function () {
                var params = {
                    "qcsid": $scope.qcsID,
                    "sessionid": userService.getUserToken()
                };
                reportingService.GetQCSDetails(params)
                    .then(function (response) {
                        $scope.QCSDetails = response;

                        if ($scope.QCSDetails.WF_ID > 0) {
                            $scope.workflowObj.workflowID = $scope.QCSDetails.WF_ID;
                        }

                        if ($scope.QCSDetails.REQ_JSON) {
                            $scope.qcsRequirementDetails = JSON.parse($scope.QCSDetails.REQ_JSON);
                        }

                        $scope.getRequirementData();
                        $scope.getItemWorkflow();
                    });
            };

            if ($scope.qcsID > 0) {
                $scope.GetQCSDetails();
               
            }

            $scope.goToQCSList = function (reqID) {
                var url = $state.href("list-qcs", { "reqID": $scope.reqId });
                window.open(url, '_self');
            };

            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                //createDomestic
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListDeptWise = [];
                        $scope.workflowListTemp = response;
                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule == $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                                $scope.workflowListDeptWise.push(item);
                            }
                        });

                        if (userService.getUserObj().isSuperUser) {
                            $scope.workflowList = $scope.workflowList;
                        }
                        else {
                            $scope.workflowList = [];
                            $scope.workflowListDeptWise.forEach(function (wf, idx) {
                                $scope.deptIDs.forEach(function (dep) {
                                    if (dep == wf.deptID) {
                                        $scope.workflowList.push(wf);
                                    }
                                });
                            });

                            //$scope.workflowList = $scope.workflowList.filter(function (item) {
                            //    return item.deptID == userService.getSelectedUserDepartmentDesignation().deptID;

                            //});
                        }
                    });
            };

            $scope.getWorkflows();

            $scope.getItemWorkflow = function () {
                workflowService.getItemWorkflow(0, $scope.qcsID, $scope.WorkflowModule)
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        $scope.checkIsFormDisable();
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                if (!track.multipleAttachments) {
                                    track.multipleAttachments = [];
                                }

                                if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                                if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status === 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }

                                if (track.status === 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                    count = count + 1;
                                    $scope.IsUserApproverForStage(track.approverID);
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });
            };

            $scope.step = {
                comments: '',
                status: '',
                multipleAttachments: []
            };

            $scope.updateTrack = function (step, status) {
                $scope.disableAssignPR = true;
                $scope.commentsError = '';
                if (step.comments != null || step.comments != "" || step.comments != undefined) {
                    step.comments = validateStringWithoutSpecialCharacters(step.comments);
                }
                var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                if (step.order == tempArray.order && status === 'APPROVED') {
                    $scope.disableAssignPR = false;
                } else {
                    $scope.disableAssignPR = true;
                }

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status === 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = userService.getUserId();

                step.moduleName = $scope.WorkflowModule;

                step.subModuleName = $scope.requirementDetails.title;
                step.subModuleID = $scope.reqId;
                step.moduleID = $scope.qcsID;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            //step.multipleAttachments = [];
                            //$state.go('workflow-pending-approvals', { 'userId': userService.getUserId() });
                            //$scope.SaveQCSDetails(1)
                            //$scope.getItemWorkflow();                          
                            location.reload();
                            //$state.go('approval-qcs-list');
                        }
                    });
            };

            $scope.assignWorkflow = function (moduleID) {
                workflowService.assignWorkflow(({ wID: $scope.workflowObj.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionID }))
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                            $scope.isSaveDisable = false;
                        }
                        else {
                            //  $state.go('list-pr');
                        }
                    });
            };

            $scope.IsUserApprover = false;

            $scope.functionResponse = false;

            $scope.IsUserApproverForStage = function (approverID) {
                workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                    .then(function (response) {
                        $scope.IsUserApprover = response;
                    });
            };

            $scope.isApproverDisable = function (index) {

                var disable = true;

                var previousStep = {};

                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                disable = true;
                            }
                            else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                });

                return disable;
            };

            /*region end WORKFLOW*/


            $scope.editFormPage = function () {
                //if ($scope.QCSDetails.STATUS == undefined || $scope.QCSDetails.STATUS == 'REJECTED') {
                    $scope.editForm = !$scope.editForm;
                    $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                        $scope.fieldValidation(vendor);
                    });
                //}
                
            };

            $scope.fieldValidation = function (vendor) {
                if ($scope.editForm && vendor.INCO_TERMS) {
                    auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                        .then(function (response) {
                            vendor.INCO_TERMS_CONFIG = response;

                        });
                } else {
                    vendor.isEdit = false;
                }
            };

            $scope.fieldValidationList = function (vendor) {
                if (vendor.INCO_TERMS) {
                    auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                        .then(function (response) {
                            vendor.INCO_TERMS_CONFIG = response;
                        });
                }
            };

            $scope.unitPriceCalculation = function (item) {
                //if (parseInt(item.revUnitPrice) > parseInt(item.unitPrice)) {
                item.revUnitPrice = item.unitPrice;
                //}
            };

            $scope.getRequirementData = function () {
                auctionsService.getReportrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId(), 'excludePriceCap': 1 })
                    .then(function (response) {
                        $scope.getVendorItemAssignments();
                        $scope.qcsItems = [];
                        $scope.qcsCoreItems = [];
                        if (response.CB_TIME_LEFT > 0) {
                            response.status = "STARTED";
                        }

                        let productIds = _.uniq(_.map(response.listRequirementItems, 'catalogueItemID')).join();
                        auctionsService.getLPPByProductIds({ "compid": $scope.companyId, "sessionid": userService.getUserToken(), 'productids': productIds, 'ignorereqid': $scope.reqId ? $scope.reqId : '0' })
                            .then(function (requirementItemsLPPData) {

                                response.listRequirementItems.forEach(function (reqItem, index) {
                                    if (!$scope.qcsID) {
                                        if (requirementItemsLPPData && requirementItemsLPPData.length > 0) {
                                            let filteredLPPItem = requirementItemsLPPData.filter(function (lppItem) {
                                                return lppItem.CATALOGUE_ITEM_ID === reqItem.catalogueItemID;
                                            });

                                            if (filteredLPPItem && filteredLPPItem.length > 0 && filteredLPPItem[0].REV_UNIT_PRICE && !reqItem.LPPValue) {
                                                reqItem.DATE_CREATED = filteredLPPItem[0].DATE_CREATED ? userService.toLocalDate(filteredLPPItem[0].DATE_CREATED) : '';
                                                reqItem.RFQ_UNITS = filteredLPPItem[0].RFQ_UNITS;
                                                reqItem.LPP_COMENTS = filteredLPPItem[0].LPP_COMENTS;
                                                reqItem.QUANTITY = filteredLPPItem[0].QUANTITY;
                                                if ($scope.getRequirementCurrencyFactor(filteredLPPItem[0].REQ_CURRENCY) === $scope.getRequirementCurrencyFactor($scope.requirementDetails.currency)) {
                                                    reqItem.LPPValue = filteredLPPItem[0].REV_UNIT_PRICE;
                                                } else {
                                                    reqItem.LPPValue = filteredLPPItem[0].REV_UNIT_PRICE * (filteredLPPItem[0].REQ_CURRENCY_FACTOR / $scope.getRequirementCurrencyFactor($scope.requirementDetails.currency));
                                                }
                                            }
                                        }

                                    } else if ($scope.qcsID > 0 && $scope.qcsRequirementDetails) {
                                        var tempItem = _.filter($scope.qcsRequirementDetails.listRequirementItems, function (qcsReqItem) { return qcsReqItem.itemID === reqItem.itemID; });
                                        if (tempItem && tempItem.length > 0) {
                                            reqItem.LPPValue = tempItem[0].LPPValue;
                                          //  reqItem.DATE_CREATED = tempItem[0].DATE_CREATED ? userService.toLocalDate(tempItem[0].DATE_CREATED) : '';
                                            reqItem.DATE_CREATED = tempItem[0].DATE_CREATED ? tempItem[0].DATE_CREATED : '';
                                            reqItem.RFQ_UNITS = tempItem[0].RFQ_UNITS;
                                            reqItem.LPP_COMENTS = tempItem[0].LPP_COMENTS;
                                            reqItem.QUANTITY = tempItem[0].QUANTITY;
                                        }
                                    }
                                });


                                //if (requirementItemsLPPData && requirementItemsLPPData.length > 0) {
                                //    response.listRequirementItems.forEach(function (reqItem, index) {
                                //        if (!$scope.qcsID) {
                                //            let filteredLPPItem = requirementItemsLPPData.filter(function (lppItem) {
                                //                return lppItem.CATALOGUE_ITEM_ID === reqItem.catalogueItemID;
                                //            });

                                //            if (filteredLPPItem && filteredLPPItem.length > 0 && filteredLPPItem[0].REV_UNIT_PRICE && !reqItem.LPPValue) {
                                //                reqItem.LPPValue = filteredLPPItem[0].REV_UNIT_PRICE;
                                //            }
                                //        } else if ($scope.qcsID > 0 && $scope.qcsRequirementDetails) {
                                //            var tempItem = _.filter($scope.qcsRequirementDetails.listRequirementItems, function (qcsReqItem) { return qcsReqItem.itemID === reqItem.itemID; });
                                //            if (tempItem && tempItem.length > 0) {
                                //                reqItem.LPPValue = tempItem[0].LPPValue;
                                //            }
                                //        }
                                //    });
                                //}
                            });

                        response.listRequirementItems.forEach(function (reqItem, index) {
                            reqItem.isVisible = true;
                            var qcsItem = {
                                productId: reqItem.catalogueItemID,
                                isCoreProduct: reqItem.isCoreProductCategory,
                                isCoreProductCategory: reqItem.isCoreProductCategory,
                                itemID: reqItem.itemID,
                                itemName: reqItem.productIDorName,
                                productQuantity: reqItem.productQuantity,
                                prQuantity: reqItem.ITEM_PR_QUANTITY,
                                prNumber: reqItem.ITEM_PR_NUMBER,
                                isSelected: true
                            };

                            $scope.qcsItems.push(qcsItem);
                        });

                        $scope.qcsCoreItems = $scope.qcsItems.filter(function (qcsitem) {
                            return qcsitem.isCoreProductCategory;
                        });

                        getProductContracts();

                        if (response) {
                            $scope.qcsVendors = [{
                                vendorID: 0,
                                vendorCompany: 'Select Vendor',
                                isSelected: true
                            }];

                            $scope.requirementDetails = response;
                            $scope.requirementDetails.qcsMultipleAttachments = [];
                            if ($scope.qcsRequirementDetails && $scope.qcsRequirementDetails.qcsMultipleAttachments && $scope.qcsRequirementDetails.qcsMultipleAttachments.length > 0) {
                                $scope.requirementDetails.qcsMultipleAttachments = $scope.qcsRequirementDetails.qcsMultipleAttachments;
                            }

                            $scope.requirementDetails.listRequirementItems.sort(function (a, b) {
                                return b.isCoreProductCategory - a.isCoreProductCategory;
                            });

                            $scope.vendorWidth = Math.floor(12 / ($scope.requirementDetails.auctionVendors.length + 1));
                            $scope.requirementDetails.auctionVendors = _.filter($scope.requirementDetails.auctionVendors, function (vendor) { return vendor.companyName !== 'PRICE_CAP'; });
                            if ($scope.qcsID > 0 && $scope.qcsRequirementDetails) {
                                $scope.requirementDetails.customerComment = $scope.qcsRequirementDetails.customerComment;
                                $scope.requirementDetails.includeGstInCal = $scope.qcsRequirementDetails.includeGstInCal;
                                $scope.requirementDetails.listRequirementItems.forEach(function (item, index) {
                                    item.maxHeight = '';
                                    //if (item.productQuotationTemplateJson) {
                                    //    item.productQuotationTemplateJsonObj = JSON.parse(item.productQuotationTemplateJson);
                                    //}

                                    var tempItem = _.filter($scope.qcsRequirementDetails.listRequirementItems, function (qcsReqItem) { return qcsReqItem.itemID === item.itemID; });
                                    if (tempItem && tempItem.length > 0) {
                                        item.qtyDistributed = tempItem[0].qtyDistributed;
                                        item.budget = tempItem[0].budget;
                                    }
                                });

                                $scope.requirementDetails.auctionVendors.forEach(function (vendor, index) {
                                    vendor.isVisible = true;
                                    var qcsVendor = {
                                        vendorID: vendor.vendorID,
                                        vendorCompany: vendor.companyName,
                                        vendorCurrency: vendor.selectedVendorCurrency,
                                        isSelected: true
                                    };

                                    $scope.qcsVendors.push(qcsVendor);

                                    vendor.listRequirementItems.forEach(function (vendorItem, index) {
                                        if (vendorItem.productQuotationTemplateJson) {
                                            vendorItem.productQuotationTemplateJson = JSON.parse(vendorItem.productQuotationTemplateJson);
                                        }
                                    });

                                    var tempQCSVendor = _.filter($scope.qcsRequirementDetails.auctionVendors, function (qcsVendor) { return qcsVendor.vendorID === vendor.vendorID; });
                                    if (tempQCSVendor && tempQCSVendor.length > 0) {
                                        vendor.payLoadFactor = tempQCSVendor[0].payLoadFactor;
                                        vendor.revPayLoadFactor = tempQCSVendor[0].revPayLoadFactor;
                                        vendor.revChargeAny = tempQCSVendor[0].revChargeAny;
                                        vendor.customerComment = tempQCSVendor[0].customerComment;
                                        vendor.payLoadFactorPercentage = tempQCSVendor[0].payLoadFactorPercentage;
                                        vendor.revPayLoadFactorPercentage = tempQCSVendor[0].revPayLoadFactorPercentage;
                                        auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                                            .then(function (response) {
                                                vendor.listRequirementItems.forEach(function (vendorItem, index) {
                                                    var newArray = response.filter(function (res) { return res.ProductId == vendorItem.catalogueItemID; });
                                                    var tempQCSVendorItem = _.filter(tempQCSVendor[0].listRequirementItems, function (qcsVendorItem) { return qcsVendorItem.itemID === vendorItem.itemID; });
                                                    if (newArray && newArray.length > 0) {
                                                        if (newArray[0].IS_CUSTOMER_EDIT == 1) {
                                                            if (tempQCSVendorItem && tempQCSVendorItem.length > 0) {
                                                                vendorItem.vendorID = vendor.vendorID;
                                                                $scope.IncoTermsEditableForOtherCharges.push(vendorItem);
                                                                vendorItem.unitPrice = tempQCSVendorItem[0].unitPrice;
                                                                vendorItem.revUnitPrice = tempQCSVendorItem[0].revUnitPrice;
                                                            }
                                                        }
                                                    }

                                                    vendorItem.qtyDistributed = tempQCSVendorItem[0].qtyDistributed;

                                                });
                                            });
                                    }
                                });
                            } else {
                                $scope.requirementDetails.includeGstInCal = true;
                                $scope.requirementDetails.listRequirementItems.forEach(function (item, index) {
                                    item.maxHeight = '';
                                    if (item.productQuotationTemplateJson) {
                                        item.productQuotationTemplateJsonObj = JSON.parse(item.productQuotationTemplateJson);
                                    }
                                });

                                $scope.requirementDetails.auctionVendors.forEach(function (vendor, index) {
                                    vendor.isVisible = true;
                                    var qcsVendor = {
                                        vendorID: vendor.vendorID,
                                        vendorCompany: vendor.companyName,
                                        vendorCurrency: vendor.selectedVendorCurrency,
                                        isSelected: true
                                    };

                                    $scope.qcsVendors.push(qcsVendor);

                                    vendor.listRequirementItems.forEach(function (vendorItem, index) {
                                        //$scope.displayLeastItemPriceColor();
                                        if (vendorItem.productQuotationTemplateJson && vendorItem.productQuotationTemplateJson != '' && vendorItem.productQuotationTemplateJson != null && vendorItem.productQuotationTemplateJson != undefined) {
                                            vendorItem.productQuotationTemplateJson = JSON.parse(vendorItem.productQuotationTemplateJson);
                                        }
                                    });

                                    auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                                        .then(function (response) {

                                            vendor.listRequirementItems.forEach(function (vendorItem, index) {
                                                var newArray = response.filter(function (res) { return res.ProductId == vendorItem.catalogueItemID; });
                                                //var tempQCSVendorItem = _.filter(tempQCSVendor[0].listRequirementItems, function (qcsVendorItem) { return qcsVendorItem.itemID === vendorItem.itemID; });
                                                if (newArray && newArray.length > 0) {
                                                    if (newArray[0].IS_CUSTOMER_EDIT == 1) {
                                                        //if (tempQCSVendorItem && tempQCSVendorItem.length > 0) {
                                                        vendorItem.vendorID = vendor.vendorID;
                                                        $scope.IncoTermsEditableForOtherCharges.push(vendorItem);
                                                        //}
                                                    }
                                                }
                                            });
                                        });
                                });
                            }
                        }
                    });
            };

            $scope.getItemRank = function (item, vendor) {
                var vendorItemPrices = _.filter(vendor.listRequirementItems, function (vendorItem) { return vendorItem.itemID === item.itemID; });
                if (vendorItemPrices && vendorItemPrices.length > 0) {
                    return vendorItemPrices[0].itemRank;
                }
            };

            if (!$scope.qcsID) {
                $scope.getRequirementData();
            }

            $scope.checkQuantity = function (item, vendor) {
                var assignedQTy = 0;
                let maxAssignQty = item.ITEM_PR_QUANTITY;
                item.error = '';
                $scope.requirementDetails.auctionVendors.forEach(function (vendor, vendIdx) {
                    vendor.listRequirementItems.forEach(function (vendorItem, idx) {
                        if (vendorItem.itemID == item.itemID && vendorItem.qtyDistributed != undefined) {
                            assignedQTy += parseFloat(vendorItem.qtyDistributed);
                        }
                    });
                });

                if (!maxAssignQty) {
                    maxAssignQty = item.productQuantity;
                }

                if (assignedQTy > maxAssignQty) {
                    var data = $scope.getVendorItemPrices(item, vendor);
                    $scope.requirementDetails.auctionVendors.forEach(function (vendor1, vendIdx) {
                        vendor1.listRequirementItems.forEach(function (vendorItem, idx) {
                            if (vendorItem.itemID == data.itemID && vendor.vendorID == vendor1.vendorID) {
                                vendorItem.qtyDistributed = 0;
                            }
                        });
                    });

                    swal("Warning!", "Distributing Quantity should not exceed more than PR Quantity " + maxAssignQty, "error");
                    item.error = "Distributing Quantity should not exceed more than PR Quantity" + maxAssignQty;
                    $scope.isQcsSaveDisable = true;
                    return;
                } else {
                    $scope.isQcsSaveDisable = false;
                }
            };

            $scope.getVendorItemPrices = function (item, vendor, val) {

                var emptyObj = {
                    unitPrice: 0,
                    revUnitPrice: 0
                };
                if (vendor) {
                    var vendorItemPrices = _.filter(vendor.listRequirementItems, function (vendorItem) { return vendorItem.itemID === item.itemID; });
                    if (vendorItemPrices && vendorItemPrices.length > 0) {
                        if (vendorItemPrices[0] && vendorItemPrices[0].productQuotationTemplateJson.length > 0) {
                            item.maxHeight = '200px';
                        }

                        return vendorItemPrices[0];
                    } else {
                        return emptyObj;
                    }
                }
                else {
                    return emptyObj;
                }
            };

            $scope.getVendorTotalPriceWithoutTax = function (vendor, isAssignedQty) {
                var price = 0;
                vendor.listRequirementItems.forEach(function (item, index) {
                    if (isItemVisible(item)) {
                        price += item.revUnitPrice * (isAssignedQty && item.isCoreProductCategory ? (item.qtyDistributed ? item.qtyDistributed : 0) : item.productQuantity);
                    }
                });

                return price;
            };

            $scope.getVendorTotalInitPriceWithoutTax = function (vendor) {
                var price = 0;
                vendor.listRequirementItems.forEach(function (item, index) {
                    if (isItemVisible(item)) {
                        price += item.unitPrice * item.productQuantity;
                    }
                });

                return price;
            };

            $scope.changePayload = function (vendor) {
                if (vendor.payLoadFactorPercentage > 100) {
                    swal("Error!", "Please enter valid percentage", 'error');
                    vendor.payLoadFactorPercentage = 0;
                    vendor.payLoadFactor = 0;
                    return;
                }
                var percentAmount = 0;
                var totalValueInINR = $scope.getVendorTotalInitPriceWithoutTax(vendor);
                if (vendor.payLoadFactorPercentage) {
                    var per = parseFloat(vendor.payLoadFactorPercentage);
                    percentAmount = (totalValueInINR * per) / 100;
                }
                vendor.payLoadFactor = percentAmount;
                
                vendor.revPayLoadFactor = vendor.payLoadFactor;
                vendor.revPayLoadFactorPercentage = vendor.payLoadFactorPercentage;
            };

            $scope.changeRevPayload = function (vendor) {
                if (vendor.revPayLoadFactorPercentage > 100) {
                    swal("Error!", "Please enter valid percentage", 'error');
                    vendor.revPayLoadFactorPercentage = 0;
                    vendor.revPayLoadFactor = 0;
                    return;
                }
                var percentAmount = 0;
                var totalValueInINR = $scope.getVendorTotalPriceWithoutTax(vendor);
                if (vendor.revPayLoadFactorPercentage) {
                    var per = parseFloat(vendor.revPayLoadFactorPercentage);
                    percentAmount = (totalValueInINR * per) / 100;
                }
                vendor.revPayLoadFactor = percentAmount;

            };

            $scope.getVendorTotalLandingPrice = function (vendor, includeGstInCal) {
                var price = 0, cGSTax = 0, sGSTax = 0, iGSTax = 0;

                price += $scope.getVendorTotalPriceWithoutTax(vendor);

                if (includeGstInCal) {
                    cGSTax = $scope.getVendorTotalTax(vendor, 'CGST');
                    iGSTax = $scope.getVendorTotalTax(vendor, 'IGST');
                    sGSTax = $scope.getVendorTotalTax(vendor, 'SGST');

                    price += cGSTax + iGSTax + sGSTax;
                }

                if (vendor.revPayLoadFactor) {
                    price += (+vendor.revPayLoadFactor);
                }
                if (vendor.revChargeAny) {
                    price -= (+vendor.revChargeAny);
                }
                vendor.landingPrice = price;
                return price;
            };

            $scope.getVendorAssignedQtyTotalLandingPrice = function (vendor, includeGstInCal, currencyConversion) {
                var price = 0, cGSTax = 0, sGSTax = 0, iGSTax = 0;
                price += $scope.getVendorTotalPriceWithoutTax(vendor, true);
                if (includeGstInCal) {
                    cGSTax = $scope.getVendorTotalTax(vendor, 'CGST', true);
                    iGSTax = $scope.getVendorTotalTax(vendor, 'IGST', true);
                    sGSTax = $scope.getVendorTotalTax(vendor, 'SGST', true);
                    price += cGSTax + iGSTax + sGSTax;
                }

                if (vendor.revPayLoadFactor) {
                    price += (+vendor.revPayLoadFactor);
                }

                if (vendor.revChargeAny) {
                    price -= (+vendor.revChargeAny);
                }

                vendor.assignedQtylandedCost = price;
                if (currencyConversion) {
                    const currencyConversion = vendor.vendorCurrencyFactor ? +vendor.vendorCurrencyFactor : 1;
                    vendor.assignedQtylandedCost = $scope.isVendorQtyDistributed(vendor.listRequirementItems) ? vendor.assignedQtylandedCost * currencyConversion : 0; 
                }

                return vendor.assignedQtylandedCost;
            };



            $scope.isVendorQtyDistributed = function (vendorItems) {
                let isValid = false;
                isValid = _.some(vendorItems, function (vendorReqItems) {
                    return (vendorReqItems.qtyDistributed && vendorReqItems.qtyDistributed > 0);
                });
                return isValid;
            };

            $scope.getOtherCharges = function (vendor) {
                var price = 0, otherCharges = 0;

                if (vendor) {
                    $scope.IncoTermsEditableForOtherCharges.forEach(function (incoItem, incoIndex) {
                        vendor.listRequirementItems.forEach(function (item, index) {
                            if (incoItem.catalogueItemID == item.catalogueItemID && vendor.vendorID == incoItem.vendorID && isItemVisible(item)) {
                                otherCharges += (+item.revUnitPrice);
                            }
                        });
                    });

                    price += otherCharges;
                }

                if (vendor.revPayLoadFactor) {
                    price += (+vendor.revPayLoadFactor);
                }
                if (vendor.revChargeAny) {
                    price -= (+vendor.revChargeAny);
                }

                return price;
            };

            $scope.getVendorTotalInitLandingPrice = function (vendor, includeGstInCal) {
                var price = 0, cGSTax = 0, sGSTax = 0, iGSTax = 0;

                price += $scope.getVendorTotalInitPriceWithoutTax(vendor);

                if (includeGstInCal) {
                    cGSTax = $scope.getVendorInitTotalTax(vendor, 'CGST');
                    iGSTax = $scope.getVendorInitTotalTax(vendor, 'IGST');
                    sGSTax = $scope.getVendorInitTotalTax(vendor, 'SGST');

                    price += cGSTax + iGSTax + sGSTax;
                }

                if (vendor.payLoadFactor) {
                    price += (+vendor.payLoadFactor);
                }
                if (vendor.revChargeAny) {
                    price -= (+vendor.revChargeAny);
                }
                vendor.initLandingPrice = price;
                return price;
            };

            $scope.getVendorTotalUnitItemPrices = function (vendor) {
                var price = 0;
                vendor.listRequirementItems.forEach(function (item, index) {
                    if (isItemVisible(item)) {
                        price += (+item.unitPrice);
                    }
                });

                if (vendor.payLoadFactor) {
                    price += (+vendor.payLoadFactor);
                }

                return price;
            };

            $scope.getVendorTotalRevUnitItemPrices = function (vendor) {
                var price = 0;
                vendor.listRequirementItems.forEach(function (item, index) {
                    if (isItemVisible(item)) {
                        price += (+item.revUnitPrice);
                    }
                });

                if (vendor.revPayLoadFactor) {
                    price += (+vendor.revPayLoadFactor);
                }

                return price;
            };

            $scope.getVendorTotalTax = function (vendor, taxType, isAssignedQty) {
                var totalTax = 0;
                vendor.listRequirementItems.forEach(function (item, index) {
                    if (isItemVisible(item)) {
                        if (taxType === 'CGST') {
                            totalTax += (item.revUnitPrice * (isAssignedQty && item.isCoreProductCategory ? (item.qtyDistributed ? item.qtyDistributed : 0) : item.productQuantity)) * (item.cGst / 100);
                        }

                        if (taxType === 'IGST') {
                            totalTax += (item.revUnitPrice * (isAssignedQty && item.isCoreProductCategory ? (item.qtyDistributed ? item.qtyDistributed : 0) : item.productQuantity)) * (item.iGst / 100);
                        }

                        if (taxType === 'SGST') {
                            totalTax += (item.revUnitPrice * (isAssignedQty && item.isCoreProductCategory ? (item.qtyDistributed ? item.qtyDistributed : 0) : item.productQuantity)) * (item.sGst / 100);
                        }
                    }
                });

                return totalTax;
            };

            $scope.getVendorInitTotalTax = function (vendor, taxType) {
                var totalTax = 0;
                vendor.listRequirementItems.forEach(function (item, index) {
                    if (isItemVisible(item)) {
                        if (taxType === 'CGST') {
                            totalTax += (item.unitPrice * item.productQuantity) * (item.cGst / 100);
                        }

                        if (taxType === 'IGST') {
                            totalTax += (item.unitPrice * item.productQuantity) * (item.iGst / 100);
                        }

                        if (taxType === 'SGST') {
                            totalTax += (item.unitPrice * item.productQuantity) * (item.sGst / 100);
                        }
                    }
                });

                return totalTax;
            };

            $scope.isNonCoreItemEditable = function (item, vendor) {
                var isEditable = false;

                if (item && !item.isCoreProductCategory && vendor && vendor.INCO_TERMS && vendor.INCO_TERMS_CONFIG) {
                    vendor.INCO_TERMS_CONFIG.forEach(function (incoItem) {
                        vendor.listRequirementItems.forEach(function (vendorItem, itemIndex) {
                            if (item.catalogueItemID === incoItem.ProductId && item.catalogueItemID === vendorItem.catalogueItemID) {
                                if (incoItem.IS_CUSTOMER_EDIT) {
                                    isEditable = true;
                                }
                            }
                        });
                    });
                }

                return isEditable;
            };

            $scope.exportTechSpec = function () {
                setTimeout(function () {
                    tableToExcel('testTable', 'Comparitives');
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                }, 3000);


            };

            $scope.isVendorVisible = function (qcsVendor) {
                qcsVendor.isSelected = !qcsVendor.isSelected;
                var vendorTemp = _.filter($scope.requirementDetails.auctionVendors, function (auctionVendor) { return auctionVendor.vendorID === qcsVendor.vendorID; });
                if (vendorTemp && vendorTemp.length > 0) {
                    vendorTemp[0].isVisible = qcsVendor.isSelected;
                }
            };

            $scope.isReqItemVisible = function (qcsItem) {
                qcsItem.isSelected = !qcsItem.isSelected;
                var itemTemp = _.filter($scope.requirementDetails.listRequirementItems, function (reqItem) { return reqItem.itemID === qcsItem.itemID; });
                if (itemTemp && itemTemp.length > 0) {
                    itemTemp[0].isVisible = qcsItem.isSelected;
                }
            };

            $scope.CalculateRankBasedOnLandingPrice = function (vendor) {
                vendor.landingPriceRank = 'NA';
                var validVendorsForRanking = _.filter($scope.requirementDetails.auctionVendors, function (auctionVendor) { return auctionVendor.landingPrice > 0 && auctionVendor.isQuotationRejected === 0; });

                if (validVendorsForRanking && validVendorsForRanking.length > 0) {
                    var sortedVendors = _.orderBy(validVendorsForRanking, ['landingPrice'], ['asc']);
                    var rank = _.findIndex(sortedVendors, function (vendor1) { return vendor1.vendorID === vendor.vendorID; });
                    if (rank >= 0) {
                        vendor.landingPriceRank = rank + 1;
                    }
                }

                return vendor.landingPriceRank;
            };

            $scope.CalculateSavingsBasedOnLandingPrice = function (vendor) {
                vendor.savings = 0;
                var validVendorsForSavings = _.filter($scope.requirementDetails.auctionVendors, function (auctionVendor) { return auctionVendor.initLandingPrice > 0; });
                if (validVendorsForSavings && validVendorsForSavings.length > 0) {
                    var sortedVendors = _.orderBy(validVendorsForSavings, ['initLandingPrice'], ['asc']);
                    var rank = _.findIndex(sortedVendors, function (vendor1) { return vendor1.vendorID === vendor.vendorID; });
                    if (rank >= 0) {
                        vendor.savings = sortedVendors[0].initLandingPrice - vendor.landingPrice;
                    }
                }

                return vendor.savings;
            };



            $scope.UpdateOtherCharges = function () {
                //domestic

                if ($scope.workflowObj.workflowID) {
                    var requirementItem = [];
                    $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                        vendor.listRequirementItems.forEach(function (reqItem) {
                            if (reqItem.isCoreProductCategory == 1) { requirementItem.push(reqItem); }
                        });
                    });

                    var isQtyDistributed = false;
                    var qtydis = [];
                    if (requirementItem.length > 0) {
                        qtydis = requirementItem.filter(function (item) {
                            if (item.qtyDistributed == undefined || item.qtyDistributed == null || item.qtyDistributed == "") { } else { return item.qtyDistributed; }
                        });
                    }
                    if (qtydis.length > 0) {
                        $scope.SaveQCSDetails(1);
                        if ($scope.QCSREQIDS.length == 0) {
                            $scope.getCalculatedOtherCharges();
                        } else if ($scope.QCSREQIDS.length > 0) {
                            let Requirement = _.filter($scope.QCSREQIDS, function (req) { return req.IS_PRIMARY_ID === 1; });
                            if (Requirement[0].IS_PRIMARY_ID == $scope.QCSDetails.IS_PRIMARY_ID && $scope.qcsID > 0) {
                                $scope.getCalculatedOtherCharges();
                            }
                        }
                    } else {
                        swal("Error!", "Please validate Quantity Distribution for existing items", 'error');
                        return;
                    }
                } else {
                    $scope.SaveQCSDetails(1);
                    if ($scope.QCSREQIDS.length == 0) {
                        $scope.getCalculatedOtherCharges();
                    } else if ($scope.QCSREQIDS.length > 0) {
                        let Requirement = _.filter($scope.QCSREQIDS, function (req) { return req.IS_PRIMARY_ID === 1; });
                        if (Requirement[0].IS_PRIMARY_ID == $scope.QCSDetails.IS_PRIMARY_ID && $scope.qcsID > 0) {
                            $scope.getCalculatedOtherCharges();
                        }
                    }
                }
            };

            $scope.saveVendorOtherChargesObject = {
                vendorID: 0,
                DIFFERENTIAL_FACTOR: 0,
                requirementID: 0
            };

            $scope.getCalculatedOtherCharges = function () {

                $scope.saveVendorOtherCharges = [];

                $scope.requirementDetails.auctionVendors.forEach(function (item, index) {
                    item.DIFFERENTIAL_FACTOR = 0;
                    item.DIFFERENTIAL_FACTOR = $scope.getOtherCharges(item);

                    $scope.saveVendorOtherChargesObject = {
                        vendorID: 0,
                        DIFFERENTIAL_FACTOR: 0,
                        requirementID: 0
                    };

                    $scope.saveVendorOtherChargesObject.vendorID = item.vendorID;
                    $scope.saveVendorOtherChargesObject.DIFFERENTIAL_FACTOR = item.DIFFERENTIAL_FACTOR;
                    $scope.saveVendorOtherChargesObject.requirementID = $scope.requirementDetails.requirementID;
                    $scope.saveVendorOtherCharges.push($scope.saveVendorOtherChargesObject);
                });

                var params =
                {
                    userID: userService.getUserId(),
                    vendorOtherChargesArr: $scope.saveVendorOtherCharges,
                    sessionID: userService.getUserToken()
                };


                auctionsService.saveVendorOtherCharges(params)
                    .then(function (response) {

                        //if (response.errorMessage === "") {
                        //    growlService.growl("Other Charges Updated Successfully", "success");
                        //} else {
                        //    growlService.growl("Other Charges Updation Failed", "inverse");
                        //}

                        //console.log("in update charges");

                    });



            };



            $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                var isEligible = true;

                if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                    isEligible = true;
                } else {
                    isEligible = false;
                }

                return isEligible;
            };


            $scope.routeToExcelView = function () {
                var url = $state.href("cost-comparisions-qcs-excel", { "reqID": $scope.reqId, "qcsID": $scope.qcsID });
                window.open(url, '_blank');
            };

            $scope.getRequirementSettings = function () {
                $scope.requirementSettings = [];
                $scope.selectedTemplate.TEMPLATE_NAME = 'PRM_DEFAULT';
                auctionsService.getRequirementSettings({ "reqid": $scope.reqId, "sessionid": $scope.sessionID })
                    .then(function (response) {
                        $scope.requirementSettings = response;
                        if ($scope.requirementSettings && $scope.requirementSettings.length > 0) {
                            var template = $scope.requirementSettings.filter(function (setting) {
                                return setting.REQ_SETTING === 'TEMPLATE_ID';
                            });

                            if (template && template.length > 0) {
                                $scope.selectedTemplate.TEMPLATE_ID = template[0].REQ_SETTING_VALUE;
                            }
                        }

                        if ($scope.selectedTemplate.TEMPLATE_ID || $scope.selectedTemplate.TEMPLATE_NAME) {
                            $scope.GetPRMTemplateFields();
                        }
                    });
            };

            $scope.getRequirementSettings();

            $scope.GetPRMTemplateFields = function () {
                $scope.prmFieldMappingDetails = {};
                var params = {
                    "templateid": $scope.selectedTemplate.TEMPLATE_ID ? $scope.selectedTemplate.TEMPLATE_ID : 0,
                    "templatename": $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : '',
                    "sessionid": $scope.sessionID
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        $scope.prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });
                });
            };

            $scope.selectVendorForItemVendor = function (vendor) {
                $scope.vendorAssignmentList.forEach(function (item, index) {
                    item.vendorID = vendor.vendorID;
                    item.assignedPrice = $scope.getItemVendorPrice({ itemID: item.itemID, vendorID: vendor.vendorID });
                });
            };

            $scope.cloneItemVendorAssignment = function (itemVendorObj) {
                $scope.vendorAssignmentList.push({
                    qcsVendorItemId: 0,
                    vendorID: 0,
                    vendorName: 'Select Vendor',
                    itemID: itemVendorObj.itemID,
                    assignedQty: itemVendorObj.assignedQty,
                    assignedPrice: itemVendorObj.assignedPrice,
                    poID: ''
                });
            };

            function retrieveVendorItems(items) {
                var itemsFinal = [];
                if (items && items.length > 0) {                    
                    items.forEach(function (item, index) {
                        itemsFinal.push({
                            QCS_ID: $stateParams.qcsID,
                            REQ_ID: $stateParams.reqID,
                            VENDOR_ID: item.vendorID,
                            ITEM_ID: item.itemID,
                            ASSIGN_QTY: item.assignedQty,
                            ASSIGN_PRICE: item.assignedPrice,
                            TOTAL_PRICE: item.totalPrice,
                            REQ_CURRENCY_FACTOR: $scope.getRequirementCurrencyFactor($scope.requirementDetails.currency),
                            PLANT_CODE: item.PLANT_CODE,
                            PR_NUMBER: item.PR_NUMBER,
                            VENDOR_SITE_CODE: item.VENDOR_SITE_CODE,
                            VENDOR_CODE: item.VENDOR_CODE,
                            PR_ITEM_ID: item.PR_ITEM_ID > 0 ? item.PR_ITEM_ID : 0
                        });
                    });
                }
                return itemsFinal;
            }

            $scope.getVendorItemAssignments = function (type) {
                $scope.qcsPOVendors = [];
                $scope.vendorAssignmentList = [];
                var params =
                {
                    qcsid: $stateParams.qcsID,
                    reqid: $stateParams.reqID,
                    userid: 0,
                    sessionid: userService.getUserToken()
                };

                auctionsService.getQCSVendorItemAssignments(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                let itemTemp = $scope.qcsItems.filter(function (qcsitem) {
                                    return qcsitem.itemID == item.ITEM_ID;
                                });

                                let vendorTemp = $scope.qcsVendors.filter(function (qcsvendor) {
                                    return qcsvendor.vendorID == item.VENDOR_ID;
                                });

                                $scope.vendorAssignmentList.push({
                                    qcsVendorItemId: item.QCS_VENDOR_ITEM_ID,
                                    vendorID: item.VENDOR_ID,
                                    vendorName: vendorTemp ? vendorTemp[0].vendorCompany : '',
                                    itemID: item.ITEM_ID,
                                    item: itemTemp ? itemTemp[0] : null,
                                    assignedQty: item.ASSIGN_QTY,
                                    assignedPrice: item.ASSIGN_PRICE,
                                    totalPrice: (item.ASSIGN_QTY * item.ASSIGN_PRICE), //item.TOTAL_PRICE,
                                    poID: item.PO_ID,
                                    currency: vendorTemp ? vendorTemp[0].vendorCurrency : '',
                                    REQ_TYPE: item.REQ_TYPE,
                                    IS_PROCESSED: item.IS_PROCESSED,
                                    PLANT_CODE: item.PLANT_CODE,
                                    VENDOR_SITE_CODE: item.VENDOR_SITE_CODE,
                                    VENDOR_CODE: item.VENDOR_CODE,
                                    PR_NUMBER: item.PR_NUMBER,
                                    vendorSitCodeList : []
                                });
                            });
                            if (type == 'PO') {
                                $scope.generatePO();
                            }
                            if ($scope.requirementDetails.listRequirementItems[0].productDeliveryDetails == 'Eveready HO') {
                                $scope.getvendorsbySiteCode();
                            }
                        } else {
                            $scope.requirementDetails.listRequirementItems.forEach(function (reqItem, index) {
                                if (reqItem.isCoreProductCategory > 0) {
                                    $scope.requirementDetails.auctionVendors.forEach(function (reqVendor, index) {
                                        if ($scope.getItemRank(reqItem, reqVendor) == 1) {
                                            let itemTemp = $scope.qcsItems.filter(function (qcsitem) {
                                                return qcsitem.itemID == reqItem.itemID;
                                            });

                                            let vendorTemp = $scope.qcsVendors.filter(function (qcsvendor) {
                                                return qcsvendor.vendorID == reqVendor.vendorID;
                                            });

                                            //$scope.vendorAssignmentList.push({
                                            //    qcsVendorItemId: 0,
                                            //    vendorID: reqVendor.vendorID,
                                            //    vendorName: vendorTemp ? vendorTemp[0].vendorCompany : '',
                                            //    itemID: reqItem.itemID,
                                            //    assignedQty: reqItem.productQuantity,
                                            //    assignedPrice: $scope.getVendorItemPrices(reqItem, reqVendor).revUnitPrice,
                                            //    totalPrice: ($scope.getVendorItemPrices(reqItem, reqVendor).revUnitPrice * item.productQuantity),
                                            //    poID: ''
                                            //});
                                        }
                                    });
                                }
                            });
                        }
                    });                
            };

            $scope.getItemRFQQuantity = function (itemId) {
                let quantity = 0;
                let itemTemp = $scope.qcsCoreItems.filter(function (item) {
                    return item.itemID == itemId;
                });

                if (itemTemp && itemTemp.length > 0) {
                    quantity = itemTemp[0].productQuantity;
                }

                return quantity;
            };

            $scope.qcsVendorItemQuantityChange = function (item) {
                let itemId = item.itemID;
                let assignedQty = 0;
                $scope.vendorAssignmentList.forEach(function (item1, index) {
                    if (item1.itemID == itemId) {
                        assignedQty = assignedQty + (+item.assignedQty);
                    }
                });


                let itemTemp = $scope.qcsCoreItems.filter(function (item) {
                    return item.itemID == itemId;
                });

                if (itemTemp && itemTemp.length > 0) {
                    itemTemp[0].usedQty = itemTemp[0].productQuantity - assignedQty;
                }

                console.log(itemTemp);
            };

            $scope.getItemVendorPrice = function (itemVendorObj) {
                if (itemVendorObj.vendorID && itemVendorObj.itemID) {

                    let reqItem = $scope.requirementDetails.listRequirementItems.filter(function (item) {
                        return item.itemID == itemVendorObj.itemID;
                    });

                    let reqVendor = $scope.requirementDetails.auctionVendors.filter(function (vendor) {
                        return vendor.vendorID == itemVendorObj.vendorID;
                    });

                    itemVendorObj.assignedPrice = $scope.getVendorItemPrices(reqItem[0], reqVendor[0]).revUnitPrice;
                }

                return itemVendorObj.assignedPrice;
            };

            $scope.goToVendorPo = function (vendor) {
                let itemsTemp = $scope.vendorAssignmentList.filter(function (qcsitem) {
                    return !qcsitem.poID && qcsitem.vendorID == vendor.vendorID;
                });

                if (itemsTemp && itemsTemp.length > 0) {
                    var url = $state.href("po", { "reqID": $stateParams.reqID, "vendorID": vendor.vendorID, "poID": 0 });
                    window.open(url, '_blank');
                } else {
                    growlService.growl("No items found to create PO.", "inverse");
                }
            };

            $scope.viewVendorPo = function (vendor) {
                var url = $state.href("po", { "reqID": $stateParams.reqID, "vendorID": vendor.vendorID, "poID": vendor.poID });
                window.open(url, '_blank');
            };


            $scope.generatePO = function () {

                if (qcsassignList.length > 0) {
                    $scope.vendorAssignmentList = qcsassignList;
                }
                var vendorAssignedItems = [];
                    $scope.vendorAssignmentList.forEach(function (itemassign) {
                        if (itemassign.REQ_TYPE == 'QCS' && itemassign.IS_PROCESSED == -1) {
                                itemassign.QCS_ID = $stateParams.qcsID,
                                itemassign.REQ_ID = $stateParams.reqID,
                                itemassign.VENDOR_ID = itemassign.vendorID,
                                itemassign.QCS_VENDOR_ITEM_ID = itemassign.qcsVendorItemId                                
                                itemassign.PLANT_CODE = itemassign.VENDOR_SITE_CODE ? itemassign.VENDOR_SITE_CODE.split('_')[0] : '';
                                vendorAssignedItems.push(itemassign)
                        }
                    })

                    $scope.QCSDetails.VENDOR_ITEM_ASSIGNMENT = vendorAssignedItems;
                    if ($scope.QCSDetails.VENDOR_ITEM_ASSIGNMENT.length > 0) {
                        var params = {
                            "qcsdetails": $scope.QCSDetails,
                            "sessionid": userService.getUserToken()
                        };

                        reportingService.GeneratePOProccess(params)
                            .then(function (response) {
                                if (response.errorMessage != '') {
                                    growlService.growl(response.errorMessage, "inverse");
                                }
                                else {
                                    swal({
                                        title: "Done!",
                                        text: "This QCS Information will be Pushed to ERP",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },

                                        function () {
                                            location.reload();
                                        });
                                }
                            });
                    }
                    else {
                        swal({
                            title: "Error!",
                            text: "This QCS Information is Already Processed",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },

                            function () {
                                location.reload();
                            });

                    }
                }
            

            $scope.routetoContract = function () {


                if ($scope.vendorAssignmentList && $scope.vendorAssignmentList.length > 0) {
                    $state.go("costProductContract", { 'REQ_ID': $scope.reqId, 'QCS_ID': $scope.qcsID, 'PC_ID': 0 });
                } else {
                    swal("Error!", 'Vendor details are missing.', "error");
                }
                //$scope.vendorAssignedDetails = [];
                //$scope.vendorIDs = [];
                //$scope.uniqueVendorIds = [];
                //$scope.requirementDetails.auctionVendors.forEach(function (item) {

                //    let PRarray = $scope.requirementDetails.PR_ID.split(",");

                //    //if (item.IS_QUOT_LATE == -1) {
                //        $scope.poVendorObj = {
                //            VENDOR_COMPANY: '',
                //            PAYMENT_TERMS: '',
                //            VENDOR_CODE: '',
                //            VENDOR_ID: 0,
                //            VENDOR_NAME: '',
                //            LOCATION: '',                          
                //            DELIVERY_DATE: '',
                //            START_DATE: '',
                //            END_DATE: '',
                //            INCO_TERMS: '',
                //            REQ_TYPE: '',
                //            VENDOR_CURRECY: '',
                //            CURRENCY_FACTOR: '',
                //            PRICE: 0,
                //            WARRANTY: '',
                //            poVendorItems: [{
                //                SKU: '',
                //                CATEGORY: '',
                //                ITEM_NAME: '',
                //                DESCRIPTION: '',
                //                ORDER_QTY: 0,
                //                PR_QTY: 0,
                //                UNIT_PRICE: 0,
                //                BASE_PRICE: 0,
                //                TOTAL_ITEM_PRICE: 0,
                //                UOM: '',
                //                SUB_CATEGORY: '',
                //                CGST: 0,
                //                SGST: 0,
                //                IGST: 0,
                //                HSN_CODE: '',
                //                ITEM_TYPE: '',
                //                PRODUCT_ID: 0
                //            }],

                //        };

                //        //if ($scope.poVendorDetails.length > 0) {
                //        //    $scope.poVendorDetails.forEach(function (vendorPOdetails) {

                //        //        if (vendorPOdetails.VENDOR_ID == item.vendorID) {
                //        //            $scope.vendorIDs.push(vendorPOdetails.VENDOR_ID)
                //        //        }
                //        //    })
                //        //}

                //        $scope.poVendorObj.VENDOR_COMPANY = item.companyName;
                //        $scope.poVendorObj.PAYMENT_TERMS = item.payment;
                //        $scope.poVendorObj.VENDOR_CODE = item.vendorCode;
                //        $scope.poVendorObj.VENDOR_SITE_CODE = item.selectedSiteCode;
                //        $scope.poVendorObj.VENDOR_ID = item.vendorID;
                //        $scope.poVendorObj.VENDOR_NAME = item.vendorName;
                //        $scope.poVendorObj.DEPARTMENT = $scope.requirementDetails.DEPARTMENT;
                //        $scope.poVendorObj.START_DATE = $scope.requirementDetails.contractStartTime;
                //        $scope.poVendorObj.END_DATE = $scope.requirementDetails.contractEndTime;
                //        $scope.poVendorObj.INCO_TERMS = $scope.requirementDetails.generalTC;
                //        $scope.poVendorObj.REQ_TYPE = $scope.requirementDetails.reqType;
                //        $scope.poVendorObj.LOCATION = $scope.requirementDetails.deliveryLocation;
                //        $scope.poVendorObj.VENDOR_CURRECY = item.selectedVendorCurrency;
                //        $scope.poVendorObj.CURRENCY_FACTOR = item.vendorCurrencyFactor;
                //        $scope.poVendorObj.PRICE = item.revPrice;
                //        $scope.poVendorObj.WARRANTY = item.warranty;
                //        $scope.poVendorObj.PR_ID = PRarray[0];
                //        $scope.poVendorObj.DELIVERY_DATE = moment($scope.requirementDetails.releaseDate).format("YYYY-MM-DD");                 

                //        $scope.poVendorObj.poVendorItems = [];

                //        item.listRequirementItems.forEach(function (listItem) {

                //            if (listItem.qtyDistributed > 0 && listItem.isCoreProductCategory == 1) {

                //                listItem.quantityPrice = listItem.qtyDistributed * listItem.revUnitPrice;
                //                //listItem.totalGst = listItem.cGst + listItem.sGst;

                //                if ($scope.requirementDetails.listRequirementItems[0].productDeliveryDetails == 'Eveready HO') {
                //                    var vendorSites = _.filter($scope.vendorAssignmentList, function (obj) { return obj.vendorID == item.vendorID && obj.REQ_TYPE == 'QCS' && obj.item.productId == listItem.catalogueItemID });
                //                }



                //                $scope.povendorItemsObj = {
                //                    PRODUCT_CODE: listItem.productCode,
                //                    PRODUCT_NO: listItem.productNo,
                //                    ITEM_NAME: listItem.productIDorName,
                //                    DESCRIPTION: listItem.productDescription,
                //                    ORDER_QTY: listItem.qtyDistributed,
                //                    PR_QTY: listItem.productQuantity,
                //                    UNIT_PRICE: listItem.revUnitPrice,
                //                    PRICE: listItem.quantityPrice,
                //                    ITEM_GST: listItem.Gst,
                //                    TOTAL_PRICE: listItem.quantityPrice + ((listItem.quantityPrice / 100) * listItem.Gst),
                //                    CGST: listItem.cGst > 0 ? listItem.cGst : 0,
                //                    SGST: listItem.sGst > 0 ? listItem.sGst : 0,
                //                    IGST: listItem.cGst == 0 && listItem.sGst == 0 ? listItem.iGst : 0,
                //                    UOM: listItem.productQuantityIn,
                //                    SUB_CATEGORY: listItem.subCategory,
                //                    HSN_CODE: listItem.hsnCode,
                //                    ITEM_TYPE: listItem.productBrand,
                //                    REQ_ID: listItem.requirementID,
                //                    QCS_ID: $scope.qcsID,
                //                    CURRENCY: item.vendorCurrency,
                //                    CATEGORY_ID: listItem.categoryID == null ? 0 : listItem.categoryID,
                //                    SUB_CATEGORY_ID: listItem.subCategoryID == null ? 0 : listItem.subCategoryID,
                //                    PRODUCT_ID: listItem.catalogueItemID == null ? 0 : listItem.catalogueItemID,
                //                    PR_ITEM_ID: $scope.requirementDetails.PR_ITEM_IDS == null ? 0 : $scope.requirementDetails.PR_ITEM_IDS,
                //                    PR_NUMBER: listItem.ITEM_PR_NUMBER,
                //                    BUDGET_ID: listItem.budgetID == null ? 0 : listItem.budgetID,
                //                    BC_ID: listItem.bcId == null ? 0 : listItem.bcId,
                //                    EXPENSE_CATEGORY: listItem.expenseCategory,
                //                    ENTITY: $scope.requirementDetails.entity,
                //                    BRANCH_LOCATION: $scope.requirementDetails.deliveryLocation,
                //                    VENDOR_EMAIL: item.vendor.email,
                //                    VENDOR_PHONE: item.vendor.phoneNum,
                //                    PRODUCT_ID: listItem.catalogueItemID,
                //                    PRODUCT_CODE: listItem.productCode,
                //                    DELIVERY_DETAILS: listItem.productDeliveryDetails,
                //                    PRODUCT_NAME: listItem.productIDorName,
                //                    DELIVERY_TERMS: $scope.requirementDetails.deliveryTime,
                //                    VENDOR_SITE_CODE: $scope.requirementDetails.listRequirementItems[0].productDeliveryDetails == 'Eveready HO' ? (vendorSites.length > 0 ? vendorSites[0].VENDOR_SITE_CODE : '') : (listItem.selectedSiteCode ? listItem.selectedSiteCode : ''),
                //                    VENDOR_CODE: $scope.requirementDetails.listRequirementItems[0].productDeliveryDetails == 'Eveready HO' ? (vendorSites.length > 0 ? vendorSites[0].VENDOR_CODE : '') : (listItem.vendorCode ? listItem.vendorCode : '')
                //                }

                //                $scope.poVendorObj.poVendorItems.push($scope.povendorItemsObj);
                //            }
                //        })
                //        //if ($scope.vendorIDs.indexOf($scope.poVendorObj.VENDOR_ID) != -1) {
                //        //    growlService.growl("PO is Generated for Vendor" + ' ' + item.companyName, "inverse")
                //        //} else {
                //        //    if ($scope.poVendorObj.poVendorItems.length > 0 && item.isQuotationRejected == 0) {
                //        //        $scope.vendorAssignedDetails.push($scope.poVendorObj)
                //        //    }
                //        //}

                //            if ($scope.poVendorObj.poVendorItems.length > 0 && item.isQuotationRejected == 0) {
                //                $scope.vendorAssignedDetails.push($scope.poVendorObj)
                //            }
                //    //}

                //    let foundIndex = _.findIndex($scope.uniqueVendorIds, function (vendor) { return item.vendorID === vendor.vendorID; });

                //    if (foundIndex < 0)
                //    {
                //        var obj =
                //        {
                //            "vendorId": item.vendorID,
                //            "selectedSite": item.selectedSiteCode.split("_")[0],
                //            "selectedVendorSiteCode": item.selectedSiteCode
                //        };

                //        $scope.uniqueVendorIds.push(obj);
                //    }
                //});

                //if ($scope.uniqueVendorIds != null && $scope.uniqueVendorIds.length > 0)
                //{
                //    let promises = [];

                //    $scope.uniqueVendorIds.forEach(function (vendorItem, vendorIndex) {
                //        promises.push($scope.getVendorCodes(vendorItem));
                //    });

                //    $q.all(promises)
                //        .then(function () {

                //            if ($scope.vendorAssignedDetails && $scope.vendorAssignedDetails.length > 0) {
                //                $state.go("costProductContract", { 'REQ_ID': $scope.reqId, 'QCS_ID': $scope.qcsID, 'PC_ID': 0});
                //            } else {
                //                swal("Error!", 'Vendor details are missing.', "error");
                //            }

                //        })
                //        .catch(function (error) {
                //            console.error('Error fetching vendor codes:', error);
                //            swal("Error!", 'Error occurred while fetching vendor codes, please contact support', "error");
                //        });
                //}
            }

            $scope.checkIsPODisable = function () {
                //let doShow = true;
                //if ($scope.itemWorkflow.length > 0) {
                //    if ($scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
                //        $scope.itemWorkflow[0].WorkflowTracks[0].status === "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[0].order == 1 && $scope.itemWorkflow[0].workflowID > 0) {
                //        doShow = false;
                //    }
                //}

                //return doShow;

                let doShow = false;
                var approverCount = 0;

                if ($scope.itemWorkflow.length > 0) {
                    approverCount = $scope.itemWorkflow[0].WorkflowTracks.length;
                    if ($scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
                        $scope.itemWorkflow[0].WorkflowTracks[approverCount - 1].status === "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[approverCount - 1].order == approverCount && $scope.itemWorkflow[0].workflowID > 0) {
                        doShow = true;
                    }
                }                


                //if ($scope.QCSDetails.STATUS == 'APPROVED') {
                //    doShow = true;
                //}

                if (doShow && $scope.requirementDetails && $scope.requirementDetails.listRequirementItems
                    && $scope.requirementDetails.listRequirementItems.length > 0) {
                    let prAssociatedItems = $scope.requirementDetails.listRequirementItems.filter(function (item) {
                        return item.ITEM_PR_NUMBER;
                    });

                    //if (!prAssociatedItems || prAssociatedItems.length <= 0) {
                    //    doShow = false;
                    //}
                }

                return doShow;
            };

            $scope.checkIsGeneratePOEnable = function () {
                let doShow = true;
                if ($scope.itemWorkflow.length > 0) {
                    if ($scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                        var notApproved = $scope.itemWorkflow[0].WorkflowTracks.filter(function (item) {
                            return item.status !== "APPROVED";
                        });

                        if (notApproved && notApproved.length > 0) {
                            doShow = false;
                        }
                    }
                }

                if (!$scope.itemWorkflow || $scope.itemWorkflow.length <= 0) {
                    doShow = false;
                }

                return doShow;
            };

            $scope.workflowChange = function () {
                let qcsTotalQuantity = 0;
                let reqTotalQuantity = 0;

                $scope.qcsItems.forEach(function (item1, index1) {
                    reqTotalQuantity = reqTotalQuantity + item1.productQuantity;
                });

                $scope.vendorAssignmentList.forEach(function (item1, index1) {
                    qcsTotalQuantity = qcsTotalQuantity + item1.assignedQty;
                });

                if (reqTotalQuantity !== qcsTotalQuantity) {
                    swal({
                        title: "Are you sure?",
                        text: "Please validate requirement quantity & Vendor assigned Quantity.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function () {

                    });
                }
            };

            function getProductContracts() {
                let coreItems = $scope.qcsItems.filter(function (item) {
                    return item.isCoreProductCategory;

                });
                let productIds = '';
                if (coreItems && coreItems.length > 0) {
                    productIds = _.map(coreItems, 'productId');
                }

                if (productIds) {
                    catalogService.GetProductContracts(productIds.join())
                        .then(function (response) {
                            $scope.requirementItemContracts = _.filter(response, function (contractitem) {
                                return contractitem.contractStatus === 'Active';
                            });
                        });
                }
            }

            function calculateApproverRangeFromLandedCost(json) {
                var approverObject = JSON.parse(json);

                //var vendor = _.orderBy(approverObject.auctionVendors, ['assignedQtylandedCost'], ['desc']);
                //var vendor = _.filter(approverObject.auctionVendors, function (vendor) { return vendor.landingPriceRank === 1; });
                //if (vendor.length > 0) {
                //const currencyConversion = vendor[0].vendorCurrencyFactor ? +vendor[0].vendorCurrencyFactor : 1;
                //var value = vendor[0].assignedQtylandedCost;// * currencyConversion;
                //return value;
                //}

                return _.sumBy(approverObject.auctionVendors, 'assignedQtylandedCost');
            }

            function isItemVisible(item) {
                let isVisible = true;
                var requirementItem = _.filter($scope.qcsItems, function (qcsItem) { return qcsItem.itemID === item.itemID; });
                if (requirementItem && requirementItem.length > 0 && !requirementItem[0].isSelected) {
                    isVisible = false;
                }

                return isVisible;
            }

            $scope.GetNewQuotations = function (newQuotations) {
                var params = {
                    "qcsdetails": $scope.QCSDetails,
                    "isNewQuotation": newQuotations,
                    "sessionid": userService.getUserToken()
                };

                reportingService.GetNewQuotations(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("New Quotations Requested successfully.", "success");
                        }
                    });
            };

            $scope.enableForFirstApprover = function (step, type) {

                var enable = false;
                if (type === 'WORKFLOW') {
                    if (step.order === 1 && (step.status === 'PENDING') && $scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID)) { // step.status === 'REJECTED' || 
                        enable = true;
                    }
                }
                return enable;
            };

            $scope.navigateToPOForm = function () {
                angular.element('#templateSelection').modal('hide');
                let selectedTemplate = '';
                if ($scope.stateDetails.poTemplate === 'po-domestic-zsdm') {
                    selectedTemplate = 'ZSDM';
                } else if ($scope.stateDetails.poTemplate === 'po-import-zsim') {
                    selectedTemplate = 'ZSIM';
                } else if ($scope.stateDetails.poTemplate === 'po-bonded-wh') {
                    selectedTemplate = 'ZSBW';
                } else if ($scope.stateDetails.poTemplate === 'po-service-zssr') {
                    selectedTemplate = 'ZSSR';
                }

                $state.go($scope.stateDetails.poTemplate, { 'reqId': $scope.reqId, 'qcsId': $scope.qcsID, 'requirementDetails': $scope.requirementDetails, 'detailsObj': $scope.vendorAssignmentList, 'templateName': selectedTemplate, 'quoteLink': $location.absUrl() });
                //let url = $state.href($scope.stateDetails.poTemplate, { 'reqId': $scope.reqId, 'qcsId': $scope.qcsID, 'requirementDetails': $scope.requirementDetails,  'detailsObj': null });                
                //$window.open(url, '_blank');
            };

            $scope.checkGMPValidity = function (item, vendor) {
                let isDisabled = true;
                if (item.ITEM_PR_NUMBER && item.IS_GMP_PR_ITEM > 0 && item.ITEM_PLANTS) {
                    if ($scope.vendorSAPDetails && $scope.vendorSAPDetails.length > 0) {
                        item.ITEM_PLANTS.split(",").forEach(function (itemPlant, index1) {
                            var filterSAPVendor = _.filter($scope.vendorSAPDetails, function (vendorTemp) { return vendorTemp.vendorId === vendor.vendorID && vendorTemp.plant === itemPlant; });
                            if (isDisabled && filterSAPVendor && filterSAPVendor.length > 0) {
                                isDisabled = false;
                            }
                        });
                    }
                } else {
                    isDisabled = false;
                }

                return isDisabled;
            };

            $scope.GetCurrencyFactors = function () {
                auctionsService.GetCurrencyFactors($scope.userId, $scope.sessionid, $scope.companyId)
                    .then(function (response) {
                        $scope.currencyFactors = [];
                        response.forEach(function (item) {
                            if (item.type === 'currencyfactor') {
                                $scope.currencyFactors.push(item);
                            }
                        });
                        //$scope.UpdateQCSSavings();
                    });
            };

            $scope.GetCurrencyFactors();

            $scope.getFile = function () {
                $scope.progress = 0;
                let totalQCSAttachmentsSize = 0;
                //$scope.file = $("#attachement")[0].files[0];
                let multipleAttachments = $("#attachement")[0].files;
                multipleAttachments = Object.values(multipleAttachments);
                if (multipleAttachments && multipleAttachments.length > 0) {
                    multipleAttachments.forEach(function (item, index) {
                        totalQCSAttachmentsSize = totalQCSAttachmentsSize + item.size;
                    });
                }

                if (totalQCSAttachmentsSize > $scope.totalAttachmentMaxSize) {
                    swal({
                        title: "Attachment size!",
                        text: "Total Attachments size cannot exceed 6MB",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            return;
                        });
                    return;
                }

                if (!$scope.requirementDetails.qcsMultipleAttachments) {
                    $scope.requirementDetails.qcsMultipleAttachments = [];
                }

                multipleAttachments.forEach(function (item, index) {
                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };

                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;
                            var ifExists = _.findIndex($scope.requirementDetails.qcsMultipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase(); });
                            if (ifExists < 0) {
                                $scope.requirementDetails.qcsMultipleAttachments.push(fileUpload);
                            }
                        });
                });
            };

            $scope.removeAttach = function (attachmentObj,type) {
                if (type) {
                    if (attachmentObj && attachmentObj.fileID) {
                        $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                            track.multipleAttachments = track.multipleAttachments.filter(function (fileObj) {
                                return fileObj.fileID !== attachmentObj.fileID;
                            });
                        })
                        
                    } else if (attachmentObj && attachmentObj.fileName) {
                        $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                            track.multipleAttachments = track.multipleAttachments.filter(function (fileObj) {
                                return fileObj.fileName !== attachmentObj.fileName;
                            });
                        })
                    }
                } else {
                    if (attachmentObj && attachmentObj.fileID) {
                        $scope.requirementDetails.qcsMultipleAttachments = $scope.requirementDetails.qcsMultipleAttachments.filter(function (fileObj) {
                            return fileObj.fileID !== attachmentObj.fileID;
                        });
                    } else if (attachmentObj && attachmentObj.fileName) {
                        $scope.requirementDetails.qcsMultipleAttachments = $scope.requirementDetails.qcsMultipleAttachments.filter(function (fileObj) {
                            return fileObj.fileName !== attachmentObj.fileName;
                        });
                    }
                }
                
            };

            $scope.viewPODetails = function (poObject) {
                var params = {
                    "ponumber": poObject.PO_NO,
                    "quotno": poObject.QUOT_NO,
                    "sessionid": userService.getUserToken()
                };

                PRMPOServices.getPOItems(params)
                    .then(function (response) {
                        poObject.POItems = response;
                        if (poObject.POItems && poObject.POItems.length > 0 && poObject.POItems[0].PO_TEMPLATE && poObject.POItems[0].PO_RAW_JSON) {
                            let selectedTemplate = poObject.POItems[0].PO_TEMPLATE;
                            let poRoute = ''
                            if (selectedTemplate === 'ZSDM') {
                                poRoute = 'po-domestic-zsdm';
                            } else if (selectedTemplate === 'ZSIM') {
                                poRoute = 'po-import-zsim';
                            } else if (selectedTemplate === 'ZSBW') {
                                poRoute = 'po-bonded-wh';
                            } else if (selectedTemplate === 'ZSSR') {
                                poRoute = 'po-service-zssr';
                            }

                            if (poRoute) {
                                $state.go(poRoute, {
                                    'reqId': poObject.POItems[0].REQ_ID, 'qcsId': poObject.POItems[0].QCS_ID,
                                    'quotId': poObject.QUOT_NO ? poObject.QUOT_NO : '',
                                    'requirementDetails': JSON.parse(poObject.POItems[0].QCS_REQUIREMENT_JSON),
                                    'detailsObj': JSON.parse(poObject.POItems[0].QCS_VENDOR_ASSIGNMENT_JSON),
                                    'templateName': selectedTemplate,
                                    'quoteLink': poObject.POItems[0].ZZQANO,
                                    'poRawJSON': JSON.parse(poObject.POItems[0].PO_RAW_JSON)
                                });
                            }
                        }
                    });
            };

            $scope.getPOList = function () {
                var params = {
                    'compid': $scope.companyId,
                    'template': '',
                    'vendorid': 0,
                    'status': 'DRAFT',
                    'creator': '',
                    'plant': '',
                    'purchasecode': '',
                    'search': '/cost-comparisions-qcs/' + $scope.reqId + '/' + $scope.qcsID,
                    'sessionid': $scope.sessionID,
                    'page': 0,
                    'pageSize': 10
                };

                PRMPOServices.getPOGenerateDetails(params)
                    .then(function (response) {
                        $scope.DraftPOList = [];
                        if (response && response.length > 0) {
                            $scope.DraftPOList = response;
                        }
                    });
            };

            if ($scope.reqId && $scope.qcsID) {
                $scope.getPOList();
            }

            function getVendorSAPDetails() {
                auctionsService.getVendorSAPDetails({ vendorid: 0, reqid: $scope.reqId, sessionid: userService.getUserToken() })
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.vendorSAPDetails = response;
                        }
                    });
            }

            getVendorSAPDetails();

            $scope.getRequirementCurrencyFactor = function (currency) {
                let convertToLocalCurrencyFactor = 1;
                if ($scope.currencyFactors && $scope.currencyFactors.length > 0) {
                    let reqCurrencyObj = $scope.currencyFactors.filter(function (curr) {
                        return curr.currencyCode === currency;
                    });

                    if (reqCurrencyObj && reqCurrencyObj.length > 0 && reqCurrencyObj[0].currencyFactor !== 1) {
                        convertToLocalCurrencyFactor = reqCurrencyObj[0].currencyFactor;
                    }
                }

                return convertToLocalCurrencyFactor;
            };

            $scope.showApprovedDate = function (date) {
                return userService.toLocalDate(date);
            };

            $scope.getleastRevUnitPrice = function (itemId, vendor) {
                var vendorItemPrices = _.filter(vendor.listRequirementItems, function (vendorItem) { return vendorItem.itemID === itemId; });
                if (vendorItemPrices && vendorItemPrices.length > 0) {
                    return vendorItemPrices[0].revUnitPrice;
                }
            };

            $scope.getValueLossorGain = function (item, vendors, budget) {
                let value = 0;
                let filterVendors = [];
                filterVendors = _.filter(vendors, function (vendor) { return vendor.isQuotationRejected === 0; });

                if (filterVendors && filterVendors.length > 0) {
                    filterVendors.forEach(function (vend, vendIdx) {
                        if (+(item.budget) > 0) {
                            var price = (item.budget) - (_.find(vend.listRequirementItems, { itemID: item.itemID }).revUnitPrice * vend.vendorCurrencyFactor);
                            vend.eachItemBudgetPrice = (price) * ((_.find(vend.listRequirementItems, { itemID: item.itemID }).qtyDistributed) > 0 ? (_.find(vend.listRequirementItems, { itemID: item.itemID }).qtyDistributed) : 0);
                        }
                    });
                }
                value = _.sumBy(filterVendors, 'eachItemBudgetPrice') / 100000;
                return value;
            };


            function calculateValues(vendorReqItem, requirementObject, vendor) {
                let itemSavings = 0;
                let purchaseCodes = ["001", "002", "003", "005", "006"];
                let value = 0;
                var isLPPAvailable = $scope.isLPPAvailableForItem(requirementObject.listRequirementItems, vendorReqItem);
                if (requirementObject.prNumbers && _.indexOf(purchaseCodes, $scope.requirementDetails.PURCHASE_GROUP_CODES) >= 0) {
                    if ($scope.requirementDetails.PURCHASE_GROUP_CODES === "001") {//Budget price X Qty distributed
                        if (_.find(requirementObject.listRequirementItems, { itemID: vendorReqItem.itemID }).productCode.startsWith("1")) {
                            if (vendorReqItem && requirementObject.listRequirementItems && requirementObject.listRequirementItems.length > 0) {
                                value = (_.find(requirementObject.listRequirementItems, { itemID: vendorReqItem.itemID }).budget ? ((_.find(requirementObject.listRequirementItems, { itemID: vendorReqItem.itemID }).budget) - (vendorReqItem.revUnitPrice * vendor.vendorCurrencyFactor)) : 0);
                                itemSavings = +(value) * +(vendorReqItem.qtyDistributed ? vendorReqItem.qtyDistributed : 0);
                            }
                        } else if (_.find(requirementObject.listRequirementItems, { itemID: vendorReqItem.itemID }).productCode.startsWith("2")) {
                            value = (_.find(requirementObject.listRequirementItems, { itemID: vendorReqItem.itemID }).LPPValue ? ((_.find(requirementObject.listRequirementItems, { itemID: vendorReqItem.itemID }).LPPValue) - (vendorReqItem.revUnitPrice)) : 0);
                            itemSavings = isLPPAvailable ? (+(value) * +(vendorReqItem.qtyDistributed ? vendorReqItem.qtyDistributed : 0)) : (vendorReqItem.isCoreProductCategory > 0 && vendorReqItem.qtyDistributed && +vendorReqItem.qtyDistributed > 0 ? ((vendorReqItem.unitPrice * vendor.vendorCurrencyFactor) - (vendorReqItem.revUnitPrice * vendor.vendorCurrencyFactor)) : 0);
                        }
                    } else {
                        if (vendorReqItem && requirementObject.listRequirementItems && requirementObject.listRequirementItems.length > 0) {
                            value = (_.find(requirementObject.listRequirementItems, { itemID: vendorReqItem.itemID }).LPPValue ? ((_.find(requirementObject.listRequirementItems, { itemID: vendorReqItem.itemID }).LPPValue) - (vendorReqItem.revUnitPrice * vendor.vendorCurrencyFactor)) : 0);
                            itemSavings = isLPPAvailable ? (+(value) * +(vendorReqItem.qtyDistributed ? vendorReqItem.qtyDistributed : 0)) : (vendorReqItem.isCoreProductCategory > 0 && vendorReqItem.qtyDistributed && +vendorReqItem.qtyDistributed > 0 ? ((vendorReqItem.unitPrice * vendor.vendorCurrencyFactor) - (vendorReqItem.revUnitPrice * vendor.vendorCurrencyFactor)) : 0);
                        }
                    }
                } else if (!requirementObject.prNumbers) {
                    if (_.find(requirementObject.listRequirementItems, { itemID: vendorReqItem.itemID }).productCode.startsWith("1")) { // IF PR's are not available and if the product code starts with 1 then we have to calculate with ((budget amount) - (revUnitprice) * qty)
                        value = (_.find(requirementObject.listRequirementItems, { itemID: vendorReqItem.itemID }).budget ? ((_.find(requirementObject.listRequirementItems, { itemID: vendorReqItem.itemID }).budget) - (vendorReqItem.revUnitPrice * vendor.vendorCurrencyFactor)) : 0);
                        itemSavings = +(value) * +(vendorReqItem.qtyDistributed ? vendorReqItem.qtyDistributed : 0);
                    } else {
                        value = (_.find(requirementObject.listRequirementItems, { itemID: vendorReqItem.itemID }).LPPValue ? ((_.find(requirementObject.listRequirementItems, { itemID: vendorReqItem.itemID }).LPPValue) - (vendorReqItem.revUnitPrice * vendor.vendorCurrencyFactor)) : 0);
                        itemSavings = isLPPAvailable ? (+(value) * +(vendorReqItem.qtyDistributed ? vendorReqItem.qtyDistributed : 0)) : (vendorReqItem.isCoreProductCategory > 0 && vendorReqItem.qtyDistributed && +vendorReqItem.qtyDistributed > 0 ? ((vendorReqItem.unitPrice * vendor.vendorCurrencyFactor) - (vendorReqItem.revUnitPrice * vendor.vendorCurrencyFactor)) : 0);
                    }
                }

                return itemSavings;
            }

            $scope.isLPPAvailableForItem = function (reqItems, vendorReqItem) {
                let isValid = false;
                isValid = (vendorReqItem.isCoreProductCategory > 0 && +_.find(reqItems, { itemID: vendorReqItem.itemID }).LPPValue && +(_.find(reqItems, { itemID: vendorReqItem.itemID }).LPPValue) > 0) ? true : false;
                return isValid;
            };

            function calculateApproversCount(selectedWorkflowID) {
                let count = 0;
                count = _.find($scope.workflowListTemp, { workflowID: selectedWorkflowID }).WorkflowStages.length;
                return count;
            }
            $scope.QCSDetailsTest = [];

            $scope.UpdateQCSSavings = function () {

                var params = {
                    "dbname": "hi",
                    "compid": $scope.companyId,
                    "sessionid": $scope.sessionID,
                    "type":'DOMESTIC'
                };
                reportingService.UpdateQCSSavings(params)
                    .then(function (response) {
                        $scope.finalArray = [];
                        $scope.QCSDetailsTest = response;

                        $scope.QCSDetailsTest.forEach(function (qcsItem, qcsIndex) {
                            if (qcsItem.REQ_JSON && !qcsItem.SAVINGS) {
                                $scope.qcsRequirementDetailstest = JSON.parse(qcsItem.REQ_JSON);

                                $scope.qcsRequirementDetailstest.listRequirementItems.forEach(function (reqItem, reqItemIndex) {
                                    if (!reqItem.LPPValue) {
                                        reqItem.LPPValue = _.find(qcsItem.LPP_VALUE, { CATALOGUE_ITEM_ID: reqItem.catalogueItemID }) ? _.find(qcsItem.LPP_VALUE, { CATALOGUE_ITEM_ID: reqItem.catalogueItemID }).REV_UNIT_PRICE : 0;
                                    }
                                    calcVendorLevelSavings();
                                });

                                var value = _.sumBy($scope.qcsRequirementDetailstest.auctionVendors, 'VENDOR_QCS_SAVINGS');

                                $scope.finalArray.push({
                                    QCS_ID: qcsItem.QCS_ID,
                                    REQ_ID: qcsItem.REQ_ID,
                                    TOTAL_PROFIT: +value > 0 ? +(value * $scope.getRequirementCurrencyFactor($scope.qcsRequirementDetailstest.currency)) : 0,
                                    TOTAL_LOSS: +value <= 0 ? +(value * $scope.getRequirementCurrencyFactor($scope.qcsRequirementDetailstest.currency)) : 0,
                                    SAVINGS: +value,
                                    SAVINGS_IN_REQUIRED_CURRENCY: $scope.qcsRequirementDetailstest.currency === 'INR' ? +value : (+value * $scope.getRequirementCurrencyFactor($scope.qcsRequirementDetailstest.currency)),
                                    TOTAL_PROCUREMENT_VALUE: _.sumBy($scope.qcsRequirementDetailstest.auctionVendors, 'VENDOR_TOTAL_VALUE_1'),
                                    TOTAL_PROCUREMENT_VALUE_IN_REQUIRED_CURRENCY: $scope.qcsRequirementDetailstest.currency === 'INR' ? _.sumBy($scope.qcsRequirementDetailstest.auctionVendors, 'VENDOR_TOTAL_VALUE_1') : (_.sumBy($scope.qcsRequirementDetailstest.auctionVendors, 'VENDOR_TOTAL_VALUE_1') * $scope.getRequirementCurrencyFactor($scope.qcsRequirementDetailstest.currency)),
                                    APPROVAL_COUNT: qcsItem.WF_ID ? calculateApproversCount(qcsItem.WF_ID) : 0
                                });


                            }
                        });
                        if ($scope.QCSDetailsTest && $scope.QCSDetailsTest.length > 0) {
                            $scope.saveQCSSAVINGS();
                        }
                    });
            };
            
            function calcVendorLevelSavings()
            {
                $scope.qcsRequirementDetailstest.auctionVendors.forEach(function (vendor, idx) {
                    vendor.listRequirementItems.forEach(function (item, itemIdx) {
                        item.calculateSavings1 = calculateValues(item, $scope.qcsRequirementDetailstest, vendor);
                    });
                    vendor.VENDOR_QCS_SAVINGS = _.sumBy(vendor.listRequirementItems, 'calculateSavings1');
                    vendor.VENDOR_TOTAL_VALUE_1 = $scope.getVendorAssignedQtyTotalLandingPrice(vendor, $scope.qcsRequirementDetailstest.includeGstInCal, true);
                });
            }


            $scope.saveQCSSAVINGS = function () {

                if ($scope.finalArray && $scope.finalArray.length > 0) {
                    var params = {
                        "qcsdetails": $scope.finalArray,
                        "sessionid": userService.getUserToken()
                    };

                    reportingService.saveQCSSAVINGS(params)
                        .then(function (response) {


                        });
                }
            };



            $scope.displayFirstItemRankColors = function (reqItem, vendor) {
                reqItem.isFirstItemRankedItem = false;
                $scope.firstRankedItemPrices = [];
                var temp = $scope.requirementDetails.auctionVendors;
                if (temp && temp.length > 0) {
                    temp.forEach(function (vendor1, vendorIndex1) {
                        var allVendoritemPrices = (_.find(vendor1.listRequirementItems, { itemID: reqItem.itemID }).revUnitPrice * vendor1.vendorCurrencyFactor);
                        if (allVendoritemPrices > 0) {
                            $scope.firstRankedItemPrices.push(allVendoritemPrices);
                        }
                    });
                }
                $scope.firstRankedItemPrices = _.min($scope.firstRankedItemPrices);
                var currentVendorItemPrice = (_.find(vendor.listRequirementItems, { itemID: reqItem.itemID }).revUnitPrice * vendor.vendorCurrencyFactor);
                if (currentVendorItemPrice > 0) {
                    if ($scope.firstRankedItemPrices === currentVendorItemPrice) {
                        reqItem.isFirstItemRankedItem = true;
                    }
                }
                return reqItem.isFirstItemRankedItem;
            };


            $scope.getFile1 = function (id, itemid, ext) {
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);
                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];
                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;
                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                var ifExists1 = _.findIndex(track.multipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase(); });
                                if (ifExists1 < 0) {
                                    track.multipleAttachments.push(fileUpload);
                                }

                            })

                        });
                })
                setTimeout(function () {
                }, 10000);
            }

            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            }

            $scope.isPlantModified = function (qsAssignmentList) {
                $scope.qcsVendorassignmentTemp = qsAssignmentList;
                qsAssignmentList = [];
                qcsassignList = [];

                var qsAssignmentListTemp =[]

                $scope.qcsVendorassignmentTemp.forEach(function (item) {
                    $scope.companyGSTInfo.forEach(function (gst) {
                        if (item.vendorID == gst.vendorId && item.VENDOR_SITE_CODE == gst.vendorSiteCode) {
                            item.VENDOR_CODE = gst.vendorCode;
                            qsAssignmentListTemp.push(item)
                        }
                    })
                });


                qcsassignList = _.filter(qsAssignmentListTemp, function (temp) {
                    return temp.REQ_TYPE == 'QCS'
                });


                //$scope.requirementDetails.auctionVendors.forEach(function (item) {
                //    qsAssignmentListTemp.forEach(function (site) {
                //        if (site.REQ_TYPE == 'QCS' && item.vendorID == site.vendorID) {
                //            item.selectedSiteCode = qsAssignmentobj.VENDOR_SITE_CODE;
                //            item.vendorCode = qsAssignmentobj.VENDOR_CODE;
                //        }
                //    })

                //})


                $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                        vendor.listRequirementItems.forEach(function (item) {
                            qcsassignList.forEach(function (site) {
                                if (item.catalogueItemID == site.item.productId && site.vendorID == vendor.vendorID) {
                                    item.selectedSiteCode = site.VENDOR_SITE_CODE;
                                    item.vendorCode = site.VENDOR_CODE;
                                }
                            });
                        })                            
                })

            }


            $scope.vendorsBySiteCodeList = [];

            $scope.getvendorsbySiteCode = function () {
                $scope.vedorCompanyID = [];
                $scope.companyGSTInfo = [];
                var vendorcompID = '';

                $scope.requirementDetails.auctionVendors.forEach(function (vendor, index1) {
                    if (vendor.compID > 0) {
                        $scope.vedorCompanyID.push(vendor.compID);
                        vendorcompID = $scope.vedorCompanyID.join(',')

                    }
                    
                });

                let params = {
                    companyId: vendorcompID, //userService.getUserCompanyId()
                    sessionid: userService.getUserToken() //userService.getUserCompanyId()
                };
                auctionsService.getCompanyGSTInfo(params)
                    .then(function (response) {
                        if (response) {
                            response.forEach(function (item) {
                                item.vendorSiteStatus = item.vendorSiteStatus == 1 ? 'Active' : 'Inactive';
                            })
                            $scope.companyGSTInfo = response;
                            $scope.vendorAssignmentList.forEach(function (item) {
                                $scope.companyGSTInfo.forEach(function (gst) {
                                    if (item.vendorID == gst.vendorId && gst.vendorSiteStatus == 'Active') {
                                        var obj = {};
                                        obj.VENDOR_SITE_CODE = gst.vendorSiteCode
                                        item.vendorSitCodeList.push(obj)
                                    }
                                })
                            });
                        }
                       // $scope.vendorSiteCode = _.uniqBy($scope.companyGSTInfo, 'VENDOR_SITE_CODE')
                    });
            };



            $scope.autoAllocate = function () {
                //$scope.linkContract()
                $scope.linkContractArray = [];
                    $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                        vendor.listRequirementItems.forEach(function (item) {
                            if (item.qtyDistributed > 0 && item.isCoreProductCategory == 1) {
                                var obj = {};
                                obj.listUtilisationDetails = [],
                                obj.vendorId = vendor.vendorID;
                                obj.PR_NUMBER = item.PR_NUMBER;
                                obj.REQ_ID = $stateParams.reqID,
                                obj.QCS_ID = $stateParams.qcsID,
                                obj.ITEM_ID = item.itemID;
                                obj.editQuantity = item.qtyDistributed;
                                obj.price = item.revUnitPrice;
                                obj.EXCHANGE_RATE = $scope.getRequirementCurrencyFactor($scope.requirementDetails.currency);
                                obj.VENDOR_SITE_CODE = vendor.selectedSiteCode ? vendor.selectedSiteCode : vendor.selectedSiteCode;
                                obj.PLANT_CODE = vendor.selectedSiteCode ? vendor.selectedSiteCode.split('_')[0] : vendor.selectedSiteCode.split('_')[0];
                                obj.VENDOR_CODE = vendor.vendorCode;
                                obj.REQ_TYPE = 'QCS';


                                var prs = _.uniq(item.PR_ITEM_ID.split(','))
                                if (prs && prs.length > 0) {
                                    prs.forEach(function (prItem, prIndex) {
                                        var qtyAllocation =
                                        {
                                            COMP_ID: +$scope.companyId,
                                            U_ID: +$scope.userId,
                                            VENDOR_ID: vendor.vendorID,
                                            REQUIRED_QUANTITY: 0,
                                            PR_ITEM_ID: 0,
                                            PRODUCT_ID : 0
                                        }
                                        qtyAllocation.PR_NUMBER = prItem.split('$^^$')[0];
                                        qtyAllocation.QTY_UTILISED = +item.qtyDistributed;
                                        qtyAllocation.REQUIRED_QUANTITY = prItem.split('$^^$')[2];
                                        qtyAllocation.PR_ITEM_ID = prItem.split('$^^$')[1];
                                        qtyAllocation.PRODUCT_ID = prItem.split('$^^$')[3];
                                        obj.listUtilisationDetails.push(qtyAllocation);
                                    });
                                }

                                $scope.linkContractArray.push(obj);
                            }
                        });
                    });


                    var params = {
                        "contracts": $scope.linkContractArray,
                        "sessionID": $scope.sessionID,
                    };

                PRMPRServices.autoAllocate(params)
                    .then(function (response) {
                        $scope.autoAllocatearray = response;
                        if ($scope.autoAllocatearray.length > 0 && $scope.autoAllocatearray) {
                                $scope.autoAllocatearray.forEach(function (vendor, idx) {
                                    vendor.allocationDetails.forEach(function (item, itemIdx) {
                                        if (item.ASSIGN_QTY > 0) {
                                            qcsvendorAssignedItems.push({
                                                QCS_ID: item.QCS_ID,
                                                REQ_ID: item.REQ_ID,
                                                vendorID: item.VENDOR_ID,
                                                itemID: item.ITEM_ID,
                                                assignedQty: item.ASSIGN_QTY,
                                                assignedPrice: item.ASSIGN_PRICE,
                                                totalPrice: (item.ASSIGN_QTY * item.ASSIGN_PRICE),
                                                PLANT_CODE: item.PLANT_CODE,
                                                VENDOR_SITE_CODE: item.VENDOR_SITE_CODE,
                                                VENDOR_CODE: item.VENDOR_CODE,
                                                PR_NUMBER: item.PR_NUMBER,
                                                PR_ITEM_ID: item.PR_ITEM_ID
                                            });
                                        }
                                    });
                                });

                            if (qcsvendorAssignedItems.length > 0) {
                                $scope.objectID = 1;
                                $scope.UpdateOtherCharges();
                            }


                            } else {
                                growlService.growl(response.errorMessage, 'inverse');
                            }
                        })
            };


            $scope.allocate = function (prNumber) {

                if (prNumber) {
                    $scope.autoAllocate();
                } else {
                    $scope.UpdateOtherCharges();
                }

            }

            $scope.getVendorCodes = function (vendor) {
                if (!vendor.vendorCodeList || vendor.vendorCodeList.length <= 0) {
                    let params = {
                        'uid': vendor.vendorId,
                        'compid': $scope.companyId,
                        'siteCodes': vendor.selectedSite,
                        'sessionid': $scope.sessionID
                    };

                    // Return the promise directly, no need for manual deferred
                    return auctionsService.getVendorCodes(params)
                        .then(function (response) {
                            vendor.vendorCodeList = response;

                            var filteredArray = vendor.vendorCodeList.filter(function (item) {
                                return item.VENDOR_SITE_CODE == vendor.selectedVendorSiteCode && item.VENDOR_ID == vendor.vendorId;
                            });

                            vendor.vendorCodeList = filteredArray;

                            if (vendor.vendorCodeList && vendor.vendorCodeList.length > 0) {
                                vendor.selectedVendorCode = vendor.vendorCodeList[0].VENDOR_CODE;
                            }

                            // Update the vendorAssignedDetails array
                            $scope.vendorAssignedDetails.forEach(function (vendorItem, vendorIndex) {
                                let foundIndex = _.findIndex($scope.uniqueVendorIds, function (vendor) {
                                    return vendorItem.VENDOR_ID === vendor.vendorId;
                                });
                                vendorItem.vendorCodesList = $scope.uniqueVendorIds[foundIndex].vendorCodeList;
                            });
                        })
                        .catch(function (error) {
                            console.error('Error fetching vendor codes:', error);
                            throw error; // Re-throw the error to be caught by $q.all
                        });
                } else {
                    return $q.resolve(); // Resolve immediately if codes are already present
                }
            };
        }]);