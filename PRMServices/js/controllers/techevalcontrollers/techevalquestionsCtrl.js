﻿prmApp
    .controller('techevalquestionsCtrl', ["$scope", "$state", "$stateParams", "$log", "$window", "userService", "growlService", "techevalService", "fileReader",
        function ($scope, $state, $stateParams, $log, $window, userService, growlService, techevalService, fileReader) {
            $scope.reqID = $stateParams.reqID == "" ? 0 : $stateParams.reqID;
            $scope.evalID = $stateParams.evalID == "" || !$stateParams.evalID ? 0 : $stateParams.evalID;
            $scope.configValue = "";
            $scope.showPopUp = true;
            $scope.currentOption = "";
            $scope.questionChoices = [];
            $scope.isValidToSubmit = true;

            $scope.compID = userService.getUserCompanyId();
            $scope.sessionID = userService.getUserToken();

            $scope.questionDetails = {
                options: "",
                text: "",
                type: "DESC",
                sessionID: $scope.sessionID,
                questionID: 0,
                marks: 0,
                isValid: 1,
                evalID: $scope.evalID
            }

            $scope.questionnaire = {};
            $scope.load = false;
            $scope.totalMarks = 0;

            $scope.answers = [];
            $scope.responses = [];
            $scope.userResponse = '';

            $scope.showPopUpClick = function (display) {

                $scope.questionDetails.attachment = [];
                $scope.questionDetails.attachmentName = '';

                $scope.showPopUp = display;
            };

            $scope.handleOption = function (value, doAddOption) {
                var contains = $.inArray(value, $scope.questionChoices);
                if (doAddOption) {
                    if (contains < 0 && value != '' && value != undefined) {
                        $scope.questionChoices.push(value);
                    }
                }
                else {
                    if (contains >= 0) {
                        $scope.questionChoices.splice(contains, 1);
                    }
                }

                $scope.currentOption = "";
            };

            $scope.deleteQuestion = function (questionItem) {
                var params = {
                    question: questionItem
                };

                techevalService.deletequestion(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Question Deleted Successfully.", "success");
                            $scope.getResponses();
                        }
                    });
            }

            $scope.editQuestion = function (questionItem) {
                if (questionItem.type == 'CHECK_BOX' || questionItem.type == 'RADIO_BUTTON') {
                    $scope.questionChoices = questionItem.options.split("$~");
                }
                else {
                    $scope.questionChoices = [];
                }

                $scope.questionDetails = questionItem;
                $scope.showPopUp = false;
            }

            $scope.saveQuestionDetails = function () {
                var options = "";
                if ($scope.questionChoices && $scope.questionChoices.length > 0) {
                    $scope.questionChoices.forEach(function (item2, index2) {
                        options += item2 + '$~';
                    });

                    options = options.slice(0, -2);
                }

                if (!$scope.questionDetails.type || $scope.questionDetails.type == "") {
                    $scope.questionDetails.type = "DESC";
                }

                $scope.questionDetails.options = options;

                var params = {
                    question: $scope.questionDetails
                };

                validateData($scope.questionDetails);
                if (!$scope.isValidToSubmit) {
                    growlService.growl("Please fill all required fields marked (*).", "inverse");
                    return false;
                }

                techevalService.savequestion(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Question Saved Successfully.", "success");
                            $scope.getResponses();
                            $scope.showPopUp = true;
                        }
                    });


                $scope.questionDetails = {
                    options: "",
                    text: "",
                    type: "DESC",
                    sessionID: $scope.sessionID,
                    questionID: 0,
                    marks: 0,
                    isValid: 1,
                    evalID: $scope.evalID
                }

                $scope.currentOption = "";
                $scope.questionChoices = [];
            };

            $scope.getResponses = function () {
                techevalService.getquestionnaire($scope.evalID, 1)
                    .then(function (response) {
                        $scope.questionnaire = response;
                        $scope.totalMarks = 0;
                        $scope.questionnaire.questions.forEach(function (item, index) {

                            item.sessionID = $scope.sessionID;

                            $scope.totalMarks = $scope.totalMarks + item.marks;
                            item.answerArray = [];
                            if (item.type == 'CHECK_BOX' || item.type == 'RADIO_BUTTON') {
                                item.optionsArray = item.options.split("$~");
                            }
                            else {
                                item.optionsArray = [];
                            }
                        });

                        $scope.load = true;
                    });
            };

            $scope.getResponses();

            $scope.cancelResponse = function (isApproved) {
                $window.history.back();
            };

            function validateData(questionObj) {
                if (questionObj.text == null || questionObj.text == "") {
                    $scope.isValidToSubmit = false;
                }
                else if (questionObj.type == null || questionObj.type == "") {
                    $scope.isValidToSubmit = false;
                }
                else if (questionObj.type != 'DESC' && (questionObj.options == null || questionObj.options == "")) {
                    $scope.isValidToSubmit = false;
                }
                else {
                    $scope.isValidToSubmit = true;
                }
            }




            $scope.getFile = function () {

                $scope.file = $("#QuestionAttachement")[0].files[0];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        var bytearray = new Uint8Array(result);
                        $scope.questionDetails.attachment = $.makeArray(bytearray);
                        $scope.questionDetails.attachmentName = $scope.file.name;
                    });
            };

            $scope.sendquestionnairetovendors = function () {

                var params = {
                    evalID: $scope.evalID,
                    sessionID: $scope.sessionID
                };

                techevalService.sendquestionnairetovendors(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Questions Sent Successfully to the Vendors.", "success");
                            $scope.getResponses();
                            $scope.showPopUp = true;
                        }
                    });
            };
        }]);