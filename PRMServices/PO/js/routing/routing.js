﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('po-domestic-zsdm', {
                    url: '/po-domestic-zsdm/:Id',
                    templateUrl: 'po/views/po-domestic-zsdm.html',
                    params: {
                        reqId: null,
                        qcsId: null,
                        quotId: null,
                        requirementDetails: null,
                        detailsObj: null,
                        templateName: null,
                        quoteLink: null,
                        poRawJSON: null
                    }
                })
                .state('po-bonded-wh', {
                    url: '/po-bonded-wh/:Id',
                    templateUrl: 'po/views/po-bonded-wh.html',
                    params: {
                        reqId: null,
                        qcsId: null,
                        quotId: null,
                        requirementDetails: null,
                        detailsObj: null,
                        templateName: null,
                        quoteLink: null,
                        poRawJSON: null
                    }
                })
                .state('po-import-zsim', {
                    url: '/po-import-zsim',
                    templateUrl: 'po/views/po-import-zsim.html',
                    params: {
                        reqId: null,
                        qcsId: null,
                        quotId: null,
                        requirementDetails: null,
                        detailsObj: null,
                        templateName: null,
                        quoteLink: null,
                        poRawJSON: null
                    }
                })
                .state('po-service-zssr', {
                    url: '/po-service-zssr',
                    templateUrl: 'po/views/po-service-zssr.html',
                    params: {
                        reqId: null,
                        qcsId: null,
                        quotId: null,
                        requirementDetails: null,
                        detailsObj: null,
                        templateName: null,
                        quoteLink: null,
                        poRawJSON: null
                    }
                })
                .state('list-po', {
                    url: '/list-po',
                    templateUrl: 'po/views/poManagement.html',
                    params: {
                        detailsObj: null,
                        quoteLink: null
                    }
                })
                .state('po-contract-domestic-zsdm', {
                    url: '/po-contact-domestic-zsdm/:Id',
                    templateUrl: 'po/views/po-contract-domestic-zsdm.html',
                    params: {
                        contractDetails: null,
                        productDetails: null,
                        templateName: null,
                        quoteLink: null,
                        poRawJSON: null,
                        prDetails: null,
                        detailsObj: null
                    }
                });
        }]);