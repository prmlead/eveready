﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class MngtMeetings : Entity
    {
        [DataMember(Name="meetingId")]
        public int MeetingId { get; set; }

        [DataMember(Name = "userId")]
        public int UserId { get; set; }

        [DataMember(Name = "meetingDate")]
        public DateTime? MeetingDate { get; set; }

        [DataMember(Name = "meetingTitle")]
        public string MeetingTitle { get; set; }

        [DataMember(Name = "meetingDescription")]
        public string MeetingDescription { get; set; }

        [DataMember(Name = "meetingStatus")]
        public string MeetingStatus { get; set; }

        [DataMember(Name = "createdDate")]
        public DateTime? CreatedDate { get; set; }
    }
}