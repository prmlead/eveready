﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class ActiveUsers : Entity
    {
        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "altUserID")]
        public int AltUserID { get; set; }

        string firstName = string.Empty;
        [DataMember(Name = "firstName")]
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    firstName = value;
                }
            }
        }

        string lastName = string.Empty;
        [DataMember(Name = "lastName")]
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    lastName = value;
                }
            }
        }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        string phoneNum = string.Empty;
        [DataMember(Name = "phoneNum")]
        public string PhoneNum
        {
            get
            {
                return phoneNum;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    phoneNum = value;
                }
            }
        }

        //[DataMember(Name = "altEmail")]
        //public string AltEmail { get; set; }

        //[DataMember(Name = "altPhoneNum")]
        //public string AltPhoneNum { get; set; }


        string altEmail = string.Empty;
        [DataMember(Name = "altEmail")]
        public string AltEmail
        {
            get
            {
                if (!string.IsNullOrEmpty(altEmail))
                { return altEmail; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { altEmail = value; }
            }
        }

        string altPhoneNum = string.Empty;
        [DataMember(Name = "altPhoneNum")]
        public string AltPhoneNum
        {
            get
            {
                if (!string.IsNullOrEmpty(altPhoneNum))
                { return altPhoneNum; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { altPhoneNum = value; }
            }
        }


        [DataMember(Name = "userInfo")]
        public UserInfo UserInfo { get; set; }

        [DataMember(Name = "additionalInfo")]
        public string AdditionalInfo { get; set; }

        [DataMember(Name = "regStatus")]
        public int Status { get; set; }


        [DataMember(Name = "company")]
        public string Company { get; set; }

        [DataMember(Name = "regComment")]
        public string Comment { get; set; }

        [DataMember(Name = "regScore")]
        public decimal Score { get; set; }

        [DataMember(Name = "vendorStatus")]
        public string VendorStatus { get; set; }

        

        [DataMember(Name = "message")]
        public string Message { get; set; }


        [DataMember(Name = "buyerName")]
        public string BuyerName { get; set; }

        [DataMember(Name = "category")]
        public string Category { get; set; }

     

        [DataMember(Name = "activeTime")]
        public string ActiveTime { get; set; }

        [DataMember(Name = "dateCreated")]
        public DateTime? DateCreated { get; set; }

        [DataMember(Name = "dateCreatedLocal")]
        public DateTime? DateCreatedLocal { get; set; }
    }
}