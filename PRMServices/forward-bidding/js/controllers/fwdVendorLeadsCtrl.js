﻿
prmApp
    .controller('fwdVendorLeadsCtrl', function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, PRMForwardBidService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {

        var loginUserData = userService.getUserObj();

        $scope.isOTPVerified = loginUserData.isOTPVerified;
        $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
        $scope.formRequest = {};
        $scope.formRequest.isForwardBidding = false;
        $scope.myActiveLeads = [];
        $scope.currentPage = 1;
        $scope.currentPage2 = 1;
        $scope.itemsPerPage = 10;
        $scope.itemsPerPage2 = 10;
        $scope.maxSize = 10;
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        ///* CLIENT STATUS MAPPING TO PRM STATUS */
        $scope.isCustomer = userService.getUserType();
        $scope.prmStatus = function (type, status) {
            return userService.NegotiationStatus(type, status);
        };
        /* CLIENT STATUS MAPPING TO PRM STATUS */

        $scope.pageChanged = function () {
            //$scope.getLeads();
        };
        $scope.quotationAttachment = null;

        $scope.GetDateconverted = function (dateBefore) {
            if (dateBefore) {
                return userService.toLocalDate(dateBefore);
            }
        };

        $scope.maxSize = 10;
        $scope.totalLeads = 0;
        $scope.itemsPerPage = 10;
        $scope.myAuctionsLoaded = false;
        $scope.myAuctions = [];
        $scope.myActiveLeads = [];
        $scope.myAuctionsMessage = '';

        $scope.getAuctions = function () {
            $log.info($scope.formRequest.isForwardBidding);
            if (!$scope.formRequest.isForwardBidding) {
                PRMForwardBidService.getmyAuctions({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.myAuctions = response;
                        if ($scope.myAuctions.length > 0) {
                            $scope.myAuctionsLoaded = true;
                            $scope.totalItems = $scope.myAuctions.length;
                        } else {
                            $scope.myAuctionsLoaded = false;
                            $scope.totalItems = 0;
                            $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                        }
                    });
            }
        };

        $scope.GetRequirementsReport = function () {

            PRMForwardBidService.getrequirementsreport({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.reqReport = response;

                    //alasql('SELECT requirementID as [RequirementID],title as [Title],vendorID as [VendorID],companyName as [Company],userPhone as [Phone],userEmail as [SEmail],itemID as [ItemID],productBrand as [Brand],productQuantity as [Quantity], productQuantityIn as [Units],costPrice as [InitialCostPrice],revCostPrice as [FinalCostPrice],netPrice as [NetPrice],marginAmount as [InitialMarginAmount],unitDiscount as [InitialMargin],revUnitDiscount as [FinalMargin],unitMRP as [MRP],gst as [GST],maxInitMargin as [MaximumInitialMargin],maxFinalMargin as [MaximumFinalMargin],savings as [Savings] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xslx", $scope.reqReport]);
                    alasql.fn.handleDate = function (date) {
                        return new moment(date).format("MM/DD/YYYY");
                    };

                    alasql('SELECT requirementID as RequirementID, title as RequirementTitle, handleDate(postedDate) as [PostedDate], status as Status, vendorID as VendorID, companyName as CompanyName, itemID as [ItemID], productBrand as Brand, productQuantity as Quantity, productQuantityIn as [Units], costPrice as [InitialCostPrice], revCostPrice as [FinalCostPrice], netPrice as [InitialNetPrice], marginAmount as [InitialMarginAmount], unitDiscount as [MarginPercentage],revUnitDiscount as [RevisedMarginPercentage],unitMRP as [MRP],gst as [GST] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xlsx", $scope.reqReport]);

                });
        };
        

        
        //$scope.getAuctions();
        $scope.getLeads = function () {
            PRMForwardBidService.getactiveleads({ "userid": userService.getUserId(), "page": ($scope.currentPage - 1) * $scope.itemsPerPage, "limit": $scope.itemsPerPage, "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.myActiveLeads1 = response;
                    $scope.myActiveLeads = response;

                    $scope.myActiveLeads.forEach(function (item, index) {
                        if (String(item.startTime).includes('9999')) {
                            item.startTime = '';
                        }
                        if (String(item.expStartTime).includes('9999')) {
                            item.expStartTime = '';
                        }
                        item.postedOn = $scope.GetDateconverted(item.postedOn);
                        item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                        item.expStartTime = $scope.GetDateconverted(item.expStartTime);
                        item.startTime = $scope.GetDateconverted(item.startTime);
                    });

                    if ($scope.myActiveLeads.length > 0) {

                        $scope.myAuctionsLoaded = true;
                        $scope.totalLeads = $scope.myActiveLeads.length;
                    } else {
                        $scope.totalLeads = 0;
                        $scope.myAuctionsLoaded = false;
                        $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                    }
                });
        };
            
        if ($scope.isOTPVerified && $scope.isEmailOTPVerified) {
            $scope.getLeads();
        }

        $scope.downloadTemplate = function (name) {
            reportingService.downloadTemplate(name, userService.getUserId(), 0);
        };

        $scope.filters = {
            reqStatus: '',
            priority: ''
        };

        $scope.postedOnDate = '';
        $scope.getStatusFilter = function () {
            $scope.myActiveLeads = $scope.myActiveLeads1;
            $scope.filterArray = [];
            if ($scope.filters.reqStatus) {
                $scope.filterArray = $scope.myActiveLeads.filter(function (item) {
                    return $scope.prmStatus($scope.isCustomer, item.status.toLowerCase()) === $scope.prmStatus($scope.isCustomer, $scope.filters.reqStatus.toLowerCase());
                });

                $scope.myActiveLeads = $scope.filterArray;
            }
            if ($scope.filters.priority) {
                $scope.filterArray = $scope.myActiveLeads.filter(function (item) {
                    return item.urgency.split(' ')[0].toLowerCase() === $scope.filters.priority.toLowerCase()
                });
                $scope.myActiveLeads = $scope.filterArray
            }
            if ($scope.postedFromDate) {
                $scope.filterArray = $scope.myActiveLeads.filter(function (item) {
                    return ((item.postedOn.split(' ')[0]) >= ($scope.postedFromDate))

                });
                $scope.myActiveLeads = $scope.filterArray;
            }
            if ($scope.postedToDate) {
                $scope.filterArray = $scope.myActiveLeads.filter(function (item) {
                    return ((item.postedOn.split(' ')[0]) <= ($scope.postedToDate))

                });
                $scope.myActiveLeads = $scope.filterArray;
            }
            if ($scope.searchKeyword) {
                $scope.myActiveLeads = $scope.myActiveLeads.filter(function (req) {
                    return (String(req.requirementID).includes($scope.searchKeyword)
                        || String(req.title.toLowerCase()).includes($scope.searchKeyword.toLowerCase())
                        || String(req.quotationFreezTime).includes($scope.searchKeyword)
                        || String(req.startTime).includes($scope.searchKeyword)
                        || String(req.postedOn).includes($scope.searchKeyword)
                        || String(req.biddingType.toLowerCase()).includes($scope.searchKeyword.toLowerCase())
                        || String(req.requirementNumber.toLowerCase()).includes($scope.searchKeyword.toLowerCase())
                    );
                });
            }

            $scope.totalLeads = $scope.myActiveLeads.length;
        };

        $scope.getFile1 = function (id, itemid, ext) {
            $scope.file = $("#" + id)[0].files[0];
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    if (id === "excelquotation") {
                        var bytearray = new Uint8Array(result);
                        $scope.quotationAttachment = $.makeArray(bytearray);
                        $scope.uploadquotationsfromexcel();
                    }
                });
        };

        $scope.uploadquotationsfromexcel = function (status) {
            var params = {
                reqID: 0,
                userID: userService.getUserId(),
                sessionID: userService.getUserToken(),
                quotationAttachment: $scope.quotationAttachment
            };

            PRMForwardBidService.uploadquotationsfromexcel(params)
                .then(function (response) {
                    if (!response.errorMessage) {
                        swal("Success", "Uploaded Successfully!", 'success');
                        location.reload();
                    } else {
                        swal("Error", response.errorMessage, 'error');
                    }
                });
        };

        $scope.chatRequirement = function (requirement) {
            var url = $state.href('req-chat', { "reqId": requirement.requirementID });
            $window.open(url, '_blank');
        };
    });
