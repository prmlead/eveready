﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider

                //////////////
                //  CATALOG MGMT   //
                //////////////
            .state('catalogMgmtMainPage', {
                url: '/catalogMgmtMainPage',
                templateUrl: 'views/CatalogMgmt/catalogMgmtMainPage.html'
            })
            .state('productAttribute', {
                url: '/productAttribute',
                templateUrl: 'views/CatalogMgmt/productAttribute.html'
            })
             .state('general', {
                 url: '/general',
                 templateUrl: 'views/CatalogMgmt/general.html'
             })

            .state('catalogMgmt', {
                url: '/catalogMgmt',
                templateUrl: 'views/CatalogMgmt/catalogMgmt.html'
            })
            .state('Attribute', {
                url: '/Attribute',
                templateUrl: 'views/CatalogMgmt/Attribute.html'
                })
                .state('CategoryProducts', {
                url: '/CategoryProducts',
                    templateUrl: 'views/CatalogMgmt/CategoryProducts.html'
                })
                .state('products', {
                    url: '/products',
                    templateUrl: 'views/CatalogMgmt/products.html'
                })
                .state('productAdd', {
                    url: '/product/add',
                    templateUrl: 'views/CatalogMgmt/product.html'
                })
                .state('productView', {
                    url: '/product/:viewId/:productId',
                    templateUrl: 'views/CatalogMgmt/productEdit.html'
                })
                .state('productAlalysis', {
                    url: '/productAlalysis/:productId',
                    templateUrl: 'views/CatalogMgmt/productAnalysis.html'
                })

                .state('createContract', {
                    url: '/createContract/:contractID',
                    templateUrl: 'views/CatalogMgmt/createContract.html',
                    params: {
                        vendorAssignedDetails: null,
                        prItemsList: null
                    }
                })

                .state('productEdit', {
                    url: '/product/:viewId/:productId',
                    templateUrl: 'views/CatalogMgmt/productEdit.html'
                })

                //.state('editContract', {
                //    url: '/product/:viewId/:productId',
                //    templateUrl: 'views/CatalogMgmt/productEdit.html'
                //})

                .state('CatalogCompleteView', {
                    url: '/CatalogCompleteView',
                    templateUrl: 'views/CatalogReports/CatalogCompleteView.html'
                })

                .state('createProductContract', {
                    url: '/createProductContract',
                    templateUrl: 'views/CatalogMgmt/createProductContract.html',
                    params: {
                        vendorAssignedDetails: null
                    }

                })


                .state('viewProductContract', {
                    url: '/viewProductContract/:PC_ID/:productId',
                    templateUrl: 'views/CatalogMgmt/createProductContract.html',
                    params: {
                        vendorAssignedDetails: null
                    }

                })

                .state('costProductContract', {
                    url: '/costProductContract/:REQ_ID/:QCS_ID/:PC_ID',
                    templateUrl: 'views/CatalogMgmt/createProductContract.html',
                    params: {
                        vendorAssignedDetails: null
                    }

                })

                .state('importProductContract', {
                    url: '/importProductContract/:REQ_ID/:QCS_ID/:PC_ID',
                    templateUrl: 'views/CatalogMgmt/createProductContract.html',
                    params: {
                        vendorAssignedDetails: null
                    }

                })

     
        }]);