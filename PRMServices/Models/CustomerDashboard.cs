﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class CustomerDashboard : Entity
    {
        [DataMember(Name = "months")]
        public string[] Months { get; set; }
        [DataMember(Name = "chartSeries")]
        public ChartData[] ChartSeries { get; set; }

        [DataMember(Name = "trendMonths")]
        public List<string> TrendMonths { get; set; }

        [DataMember(Name = "myReqCount")]
        public List<int> MyReqCount { get; set; }

        [DataMember(Name = "totalReqCount")]
        public List<int> TotalReqCount { get; set; }

        [DataMember(Name = "topPerformers")]
        public List<KeyValuePair> TopPerformers { get; set; }

        public List<KeyValuePair> CategoryTopPerformers { get; set; }
    }

    [DataContract]
    public class ChartData
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "data")]
        public List<int> Data { get; set; }
    }
}