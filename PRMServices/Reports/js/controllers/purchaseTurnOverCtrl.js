﻿prmApp

    .controller('purchaseTurnOverCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "PRMLotReqService", "PRMCustomFieldService", "workflowService",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices,
            PRMLotReqService, PRMCustomFieldService, workflowService) {
            $scope.years = [{ year: 2020 }, { year: 2021 }, { year: 2022 }, { year: 2023 }, { year: 2024 }, { year: 2025 }];

            $scope.purchaseTurnOver = function () {
                Highcharts.setOptions({
                    colors: ['#01BAF2', '#71BF45', '#FAA74B']
                });
                Highcharts.chart('procureVolume', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Procurement Volumes'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Procurement Volumes',
                        colorByPoint: true,
                        data: [{
                            name: '268.943',
                            y: 268.943,
                        }, {
                                name: '243.987',
                            y: 243.987,
                            sliced: true,
                            selected: true
                        }, {
                                name: '260.205',
                            y: 260.205
                            }, {
                                name: '385.145',
                                y: 385.145
                            }]
                    }]
                });


                Highcharts.chart('maverick', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Maverick Buying'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        },
                        series: {
                           
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    series: [{
                        name: 'Procurement Volumes',
                        colorByPoint: true,
                        data: [{
                            name: 'Spend not in Control',
                            y: 30,
                        }, {
                            name: 'Contract-Based',
                            y: 20,
                            sliced: true,
                            selected: true
                        }, {
                            name: 'Po-Based',
                            y: 70
                        }]
                    }]
                });


                Highcharts.chart('frameWork', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Framework Contract Rate'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {

                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        max: 6000,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Contract Rate: <b>{point.y:.1f} millions</b>'
                    },
                    series: [{
                        name: '',
                        data: [
                            ['Product 1', 4000],
                            ['Product 2', 4348],
                            ['Product 3', 5674],
                            ['Product 4', 5000],
                            ['Product 5', 4567]
                        ],
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    }]
                });


                Highcharts.chart('deliveryQuota', {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Mis-Delivery Quota'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: ['Buy', 'Components', 'Clothing', 'Accessories'],
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Delivery Quota',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: ''
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: false
                            }
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 80,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor:
                            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                        shadow: false
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Total Deliveries',
                        data: [107, 31, 635, 203, 2]
                    }, {
                        name: 'Incorrect Deliveries',
                            data: [133, 156, 947, 408, 6]
                    }]
                });

                Highcharts.chart('noOfOrders', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Number Of Orders'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {

                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Contract Rate: <b>{point.y:.1f} millions</b>'
                    },
                    series: [{
                        name: '',
                        data: [
                            ['Jan', 5],
                            ['Feb', 20],
                            ['Mar', 28],
                            ['Apr', 45],
                            ['May', 15],
                            ['Jun', 20]
                        ],
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    }]
                });

                Highcharts.chart('PurchaseBuyer', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Purchase Volume Per Buyer'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {

                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Contract Rate: <b>{point.y:.1f} millions</b>'
                    },
                    series: [{
                        name: '',
                        data: [
                            ['Buyer 1', 15],
                            ['Buyer 2', 20],
                            ['Buyer 3', 28],
                            ['Buyer 4', 45],
                            ['Buyer 5', 15],
                            ['Buyer 6', 20]
                        ],
                        dataLabels: {
                            enabled: false,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    }]
                });

                Highcharts.chart('flexibility', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Delivery Flexibility'
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                formatter: function () {
                                    return this.key + ': ' + this.y + '%';
                                }
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Flexibility',
                        colorByPoint: true,
                        innerSize: '70%',
                        data: [{
                            name: 'Flexibility',
                            // color: '#01BAF2',
                            y: 60,
                        }, {
                                name: 'Flexibility',
                            // color: '#71BF45',
                            y: 40
                            }],
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    }]
                });
            }
            $scope.purchaseTurnOver();
        }
    ]);