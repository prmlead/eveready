@echo off
del /q "..\dist\*"
REM DEL ..\dist\fonts /q
for /f "tokens=1-4 delims=/ " %%i in ("%date%") do (
     set dow=%%i
     set month=%%j
     set day=%%k
     set year=%%l
)

for /f "tokens=1-3 delims=: " %%i in ("%time%") do (
     set H=%%i
     set M=%%j
     set S=%%k
)

for /f "tokens=1-2 delims=. " %%i in ("%S%") do (
     set S1=%%i
     set T=%%j
)

set datestr=%year%%month%%day%
set mytime=%H%%M%%S1%
set cachetoken=%datestr%%mytime%
echo newtoken is %cachetoken%

COPY app.js ..\dist\app.%cachetoken%.js /y /b
COPY config.js ..\dist\config.%cachetoken%.js /y /b
COPY modulesRouting.js ..\dist\modulesRouting.%cachetoken%.js /y /b
COPY catalogMgmtRouting.js ..\dist\catalogMgmtRouting.%cachetoken%.js /y /b
COPY cijIndentRouting.js ..\dist\cijIndentRouting.%cachetoken%.js /y /b
COPY templates.js ..\dist\templates.%cachetoken%.js /y /b
COPY custom.js ..\dist\custom.%cachetoken%.js /y /b
COPY user\user.controller.js ..\dist\user.controller.%cachetoken%.js /y /b
copy controllers\*.js+controllers\storecontrollers\*.js+controllers\techevalcontrollers\*.js+controllers\reportingcontrollers\*.js+controllers\pocontrollers\*.js+controllers\fwdrequirement\*.js+controllers\workflows\*.js+controllers\requestIndent\*.js+controllers\profilecontrollers\*.js+controllers\auction\*.js+controllers\common\*.js+controllers\settings\*.js+controllers\postpo\*.js+controllers\logistics\*.js+controllers\APMC\*.js+controllers\configuration\* +controllers\realtimeprice\* +controllers\catalogMgnt\*.js+controllers\pr\*.js+controllers\CounterBid\*.js+controllers\CatalogReports\*.js ..\dist\Controllers.%cachetoken%.js /y /b
copy modules\*.js ..\dist\prmModules.%cachetoken%.js /y /b
copy services\*Services.js ..\dist\prmServices.%cachetoken%.js /y /b

COPY ..\json-requirement\js\routing\*.js + ..\json-requirement\js\controllers\*.js + ..\json-requirement\js\services\*.js ..\dist\json-requirement.%cachetoken%.js /y /b

COPY ..\audit\js\routing\*.js + ..\audit\js\controllers\*.js + ..\audit\js\services\*.js ..\dist\audit.%cachetoken%.js /y /b
COPY ..\pr\js\routing\*.js + ..\pr\js\controllers\*.js + ..\pr\js\services\*.js ..\dist\pr.%cachetoken%.js /y /b
COPY ..\chat\js\routing\*.js + ..\chat\js\controllers\*.js + ..\chat\js\services\*.js ..\dist\chat.%cachetoken%.js /y /b


COPY ..\lot-bidding\js\routing\*.js + ..\lot-bidding\js\controllers\*.js + ..\lot-bidding\js\services\*.js ..\dist\lot-bidding.%cachetoken%.js /y /b

COPY ..\tender\js\routing\*.js + ..\tender\js\controllers\*.js + ..\tender\js\services\*.js ..\dist\tender.%cachetoken%.js /y /b

COPY ..\communication\js\routing\*.js + ..\communication\js\controllers\*.js + ..\communication\js\services\*.js ..\dist\communication.%cachetoken%.js /y /b

COPY ..\custom-fields\js\routing\*.js + ..\custom-fields\js\controllers\*.js + ..\custom-fields\js\services\*.js ..\dist\custom-fields.%cachetoken%.js /y /b

COPY ..\surrogate-bidding\js\routing\*.js + ..\surrogate-bidding\js\controllers\*.js + ..\surrogate-bidding\js\services\*.js ..\dist\surrogate-bidding.%cachetoken%.js /y /b

COPY ..\prpo\js\routing\*.js + ..\prpo\js\controllers\*.js + ..\prpo\js\services\*.js ..\dist\prpo.%cachetoken%.js /y /b

COPY ..\Reports\js\routing\*.js + ..\Reports\js\controllers\*.js + ..\Reports\js\services\*.js ..\dist\Reports.%cachetoken%.js /y /b

COPY ..\po\js\routing\*.js + ..\po\js\controllers\*.js + ..\po\js\services\*.js ..\dist\po.%cachetoken%.js /y /b

COPY ..\PendingPO\js\routing\*.js + ..\PendingPO\js\controllers\*.js + ..\PendingPO\js\services\*.js ..\dist\PendingPO.%cachetoken%.js /y /b

COPY ..\ExcelUpload\js\routing\*.js + ..\ExcelUpload\js\controllers\*.js + ..\ExcelUpload\js\services\*.js ..\dist\ExcelUpload.%cachetoken%.js /y /b


COPY ..\GRN\js\routing\*.js + ..\GRN\js\controllers\*.js + ..\GRN\js\services\*.js ..\dist\GRN.%cachetoken%.js /y /b

COPY ..\forward-bidding\js\routing\*.js + ..\forward-bidding\js\controllers\*.js + ..\forward-bidding\js\services\*.js ..\dist\forward-bidding.%cachetoken%.js /y /b

xcopy resources\fonts ..\fonts /E /y
xcopy resources\css\icons\material-design-iconic-font\fonts ..\fonts /E /y

set "list=resources\js\jquery.min.js resources\js\angular.min.js resources\js\angular-messages.js resources\js\angular-resource.min.js resources\js\angular-sanitize.js resources\js\angular-ui-router.min.js resources\js\jquery.signalR-2.2.0.js  resources\js\angular-signalr-hub.js resources\js\angular-animate.min.js resources\js\angularjs-datetime-picker.js resources\js\angular-storage.min.js resources\js\angular-timer.min.js resources\js\angular-input-stars.js resources\js\loading-bar.js resources\js\bootstrap.min.js resources\js\custom.js resources\js\isteven-multi-select.js resources\js\jquery.slimscroll.js resources\js\moment.min.js resources\js\moment-tz.min.js resources\js\angular-moment-picker.min.js resources\js\moment-timezone-with-data-1970-2030.js resources\js\ngDialog.min.js resources\js\ng-table.min.js resources\js\sidebar-nav.min.js resources\js\switchery.min.js resources\js\ui-bootstrap-tpls.min.js resources\js\waves.js resources\js\xlsx.core.min.js resources\js\alasql.min.js resources\js\nouislider.min.js resources\js\jquery.nouislider.min.js resources\js\ocLazyLoad.min.js resources\js\highcharts.js resources\js\highcharts-more.js resources\js\solid-gauge.js resources\js\funnel.js resources\js\exporting.js resources\js\sweet-alert.min.js resources\js\bootstrap-growl.min.js resources\js\tablesaw.js resources\js\dashboard3.js resources\js\bootstrap-datetimepicker.min.js resources\js\double-scroll-bars.min.js resources\js\angular-ui-tree.min.js  resources\js\angular-tree-widget.min.js resources\js\angular-recursion.min.js resources\js\angular-animate.min.js resources\js\angular-recursion.js resources\js\jstree.min.js resources\js\tableToExcel.js resources\js\angular-filter.min.js resources\js\cdnjs-cloudflare-humanize-duration.min.js resources\js\cdnjs-cloudflare-jquery-ui.min.js resources\js\cdnjs-cloudflare-lodash.js resources\js\cdnjs-cloudflare-ng-file-upload-all.min.js resources\js\cdnjs-cloudflare-smart-table.min.js resources\js\cdnjs-cloudflare-sortable.min.js resources\js\cdnjs-cloudflare-jspdf.min.js resources\js\ng-pattern-restrict.min.js resources\js\toastr.min.js"
(
 for %%i in (%list%) do (
	echo %%i
	type %%i >> ..\dist\vendor.%cachetoken%.js
	echo. >> ..\dist\vendor.%cachetoken%.js
  )
)

set "list=resources\css\prmCustom.css resources\css\bootstrap.min.css resources\css\sidebar-nav.min.css resources\css\materialdesignicons.min.css resources\css\animate.css resources\css\fontawesome-all.css resources\css\style.css resources\css\angular-moment-picker.min.css resources\css\lodash.min.css resources\css\steps.css resources\css\sweet-alert.css resources\css\isteven-multi-select.css resources\css\ngDialog.min.css resources\css\tablesaw.css resources\css\default.css resources\css\bootstrap-datetimepicker.min.css resources\css\prmCustomAdhok.css resources\css\angular-ui-tree.min.css resources\css\angular-ui-tree.css resources\css\app.css resources\css\tabs.min.css resources\css\tabs.min.css resources\css\style.min.css resources\css\Apmcstyle.css resources\css\cdnjs-cloudflare-ng-sortable.min.css"
(
 for %%i in (%list%) do (
	echo %%i
	type %%i >> ..\dist\prm360.min.%cachetoken%.css
	echo. >> ..\dist\prm360.min.%cachetoken%.css
  )
)

REM copy controllers\profileCtrl.js ..\dist\profileCtrl.%cachetoken%.js /y /b

copy controllers\auction\itemCtrl.js ..\dist\itemCtrl.%cachetoken%.js /y /b

copy controllers\common\loginCtrl.js ..\dist\loginCtrl.%cachetoken%.js /y /b

copy ..\prm360.html ..\dist\prm360.html /y /b

powershell.exe -NoProfile -ExecutionPolicy Bypass -File cache.ps1 %cachetoken%