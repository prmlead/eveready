﻿using PRMServices.Common;
using PRMServices.Models;
using PRMServices.SQLHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.ReplyMarkups;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class PRMBotService : IPRMBotService, IDisposable
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static readonly TelegramBotClient Bot = new TelegramBotClient("987312262:AAGHekj0JOGShHYLqCL4BbXStQCo1L96pSk");
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        private PRMServices prmServices = new PRMServices();
        public PRMBotService()
        {
            var me = Bot.GetMeAsync().Result;
            //Console.Title = me.Username;

            Bot.OnMessage += BotOnMessageReceived;
            Bot.OnMessageEdited += BotOnMessageReceived;
            Bot.OnCallbackQuery += BotOnCallbackQueryReceived;
            Bot.OnInlineQuery += BotOnInlineQueryReceived;
            Bot.OnInlineResultChosen += BotOnChosenInlineResultReceived;
            Bot.OnReceiveError += BotOnReceiveError;

            Bot.StartReceiving(new List<UpdateType>().ToArray());
            //Console.WriteLine($"Start listening for @{me.Username}");
            //Console.ReadLine();
            //Bot.StopReceiving();
        }

        public string GetHello(string msg)
        {
            return msg;
        }
        private async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;
            bool isPhone = false;
            bool isEmail = false;
            if (message == null || message.Type != MessageType.Text) return;
            var tempMsg = message.Text.Split(' ').First().ToLower().Trim('/');
            if (tempMsg.Contains("otp"))
            {
                await Bot.SendTextMessageAsync(
                         message.Chat.Id,
                         "Thanks you, please give us some time to validate.", ParseMode.Html,
                         replyMarkup: new ReplyKeyboardRemove());
                await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
                await ValidateUserOTP(message.Chat.Id, message.From.Id, tempMsg.Replace("otp", string.Empty));
                await GetRequirements(message.Chat.Id, message.From.Id);
                return;
            }
            else if (tempMsg.Contains("req"))
            {
                await Bot.SendTextMessageAsync(
                         message.Chat.Id,
                         "We are retrieving details of requirment: " + tempMsg.Replace("req", ""), ParseMode.Html,
                         replyMarkup: new ReplyKeyboardRemove());
                await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
                await GetRequirementDetails(message.Chat.Id, message.From.Id, tempMsg.Replace("req", ""));
                return;
            }
            else
            {                
                isEmail = Regex.IsMatch(tempMsg, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                if (!isEmail)
                {
                    isPhone = Regex.IsMatch(tempMsg, @"^\d{10}$");
                }

                if (isEmail || isPhone)
                {
                    await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
                    await ValidateUser(tempMsg, message.Chat.Id, message.From.Id, isEmail);
                    await Bot.SendTextMessageAsync(
                         message.Chat.Id,
                         "OTP has been sent, please enter here as OTP<YOUROTP>", ParseMode.Default,
                         replyMarkup: new ReplyKeyboardRemove());
                    return;
                }
            }

            if (!isPhone && !isEmail && new Regex(@"^[0-9]+$").IsMatch(tempMsg))
            {
                string greeting = $@"Please wait I bring up details of the requirement.";
                await Bot.SendTextMessageAsync(
                     message.Chat.Id,
                     greeting, ParseMode.Html,
                     replyMarkup: new ReplyKeyboardRemove());
                await GetRequirementDetails(message.Chat.Id, message.From.Id, Convert.ToInt32(tempMsg));
                return;
            }

            switch (message.Text.Split(' ').First().ToLower())
            {
                case "hi":
                case "hello":
                    string greeting = $@"Hello, <b>{message.From.FirstName}</b> welcome to PRM360 Chatbot.
Please give us some time while we bring your details.";
                    await Bot.SendTextMessageAsync(
                         message.Chat.Id,
                         greeting, ParseMode.Html,
                         replyMarkup: new ReplyKeyboardRemove());
                    await GetUserDetails(message.Chat.Id, 0);
                    break;
                // send inline keyboard
                case "/requirements":
                    await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

                    await Task.Delay(500); // simulate longer running task

                    var inlineKeyboard = new InlineKeyboardMarkup(new[]
                    {
                        new [] // first row
                        {
                            InlineKeyboardButton.WithCallbackData("OPEN"),
                            InlineKeyboardButton.WithCallbackData("CLOSED"),
                        }
                    });

                    await Bot.SendTextMessageAsync(
                        message.Chat.Id,
                        "Choose",
                        replyMarkup: inlineKeyboard);
                    break;

                // send custom keyboard
                case "/keyboard":
                    ReplyKeyboardMarkup ReplyKeyboard = new[]
                    {
                        new[] { "1.1", "1.2" },
                        new[] { "2.1", "2.2" },
                    };

                    await Bot.SendTextMessageAsync(
                        message.Chat.Id,
                        "Choose",
                        replyMarkup: ReplyKeyboard);
                    break;

                // send a photo
                case "/photo":
                    await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.UploadPhoto);

                    const string file = @"Files/tux.png";

                    var fileName = file.Split(Path.DirectorySeparatorChar).Last();

                    using (var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        await Bot.SendPhotoAsync(
                            message.Chat.Id,
                            fileStream,
                            "Nice Picture");
                    }
                    break;

                // request location or contact
                case "/request":
                    var RequestReplyKeyboard = new ReplyKeyboardMarkup(new[]
                    {
                        KeyboardButton.WithRequestLocation("Location"),
                        KeyboardButton.WithRequestContact("Contact"),
                    });

                    await Bot.SendTextMessageAsync(
                        message.Chat.Id,
                        "Who or Where are you?",
                        replyMarkup: RequestReplyKeyboard);
                    break;

                default:
                    const string usage = @"
<b>Requirements:</b>
/requirements   - send inline keyboard
/keyboard - send custom keyboard
/photo    - send a photo
/request  - request location or contact

<b>Vendors:</b>
/requirements   - send inline keyboard
/keyboard - send custom keyboard
/photo    - send a photo
/request  - request location or contact";

                    await Bot.SendTextMessageAsync(
                        message.Chat.Id,
                        usage, ParseMode.Html,
                        replyMarkup: new ReplyKeyboardRemove());
                    break;
            }
        }

        private async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        {
            var callbackQuery = callbackQueryEventArgs.CallbackQuery;

            await Bot.AnswerCallbackQueryAsync(
                callbackQuery.Id,
                $"Received {callbackQuery.Data}");

            await Bot.SendTextMessageAsync(
                callbackQuery.Message.Chat.Id,
                $"Received {callbackQuery.Data}");
        }

        private async void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs inlineQueryEventArgs)
        {
            //Console.WriteLine($"Received inline query from: {inlineQueryEventArgs.InlineQuery.From.Id}");

            InlineQueryResultBase[] results = {
                new InlineQueryResultLocation(
                    id: "1",
                    latitude: 40.7058316f,
                    longitude: -74.2581888f,
                    title: "New York")   // displayed result
                    {
                        InputMessageContent = new InputLocationMessageContent(
                            latitude: 40.7058316f,
                            longitude: -74.2581888f)    // message if result is selected
                    },

                new InlineQueryResultLocation(
                    id: "2",
                    latitude: 13.1449577f,
                    longitude: 52.507629f,
                    title: "Berlin") // displayed result
                    {
                        InputMessageContent = new InputLocationMessageContent(
                            latitude: 13.1449577f,
                            longitude: 52.507629f)   // message if result is selected
                    }
            };

            await Bot.AnswerInlineQueryAsync(
                inlineQueryEventArgs.InlineQuery.Id,
                results,
                isPersonal: true,
                cacheTime: 0);
        }

        public async Task GetUserDetails(long chatId, int chatUserId)
        {
            await Bot.SendChatActionAsync(chatId, ChatAction.Typing);
            //await Task.Delay(20000);
            //Call PRM to find out UID and details
            int prmUid = 0;
            if (prmUid <= 0)
            {
                await Task.Run(() =>
                {
                    string greeting = $@"Apologies we are not able to identitify you,
please enter your login-id (phone/email), we will be sending OTP to authorize you.
Thank you, PRM360 Support";
                    Bot.SendTextMessageAsync(
                         chatId,
                         greeting, ParseMode.Html,
                         replyMarkup: new ReplyKeyboardRemove());
                });
            }
            else
            {
                await Task.Run(() =>
                {
                    string greeting = $@"Thanks we identify you as PRM Vendor user.
Below are requirement currently running or upcoming.
<b>Current Running Requirements:</b>
1. Laptop for IT dept. (ID: /12457)
<b>Upcoming Requirements:</b>
1. Laptop for IT dept. (ID: /245)

Please click on requirement id to get more details";
                    Bot.SendTextMessageAsync(
                         chatId,
                         greeting, ParseMode.Html,
                         replyMarkup: new ReplyKeyboardRemove());
                });
            }
        }

        public async Task GetRequirementDetails(long chatId, long chatUserId, string reqId)
        {
            await Bot.SendChatActionAsync(chatId, ChatAction.Typing);
            await Task.Run(() =>
            {
                string message = PRMBotUtility.GetUserRequirementDetails(chatId, chatUserId, Convert.ToInt32(reqId));

                Bot.SendTextMessageAsync(
                     chatId,
                     message, ParseMode.Html,
                     replyMarkup: new ReplyKeyboardRemove());
            });
        }

        public async Task ValidateUser(string userLoginId, long chatId, long chatUserId, bool isEmail)
        {
            await Task.Run(() =>
            {
                string greeting = $@"OTP has been sent, please enter here to validate.";

                Bot.SendTextMessageAsync(
                     chatId,
                     greeting, ParseMode.Html,
                     replyMarkup: new ReplyKeyboardRemove());

                PRMBotUtility.SendUserBotOTP(userLoginId, chatId, chatUserId, isEmail);
            });
        }

        public async Task ValidateUserOTP(long chatId, long chatUserId, string otp)
        {
            await Task.Run(() =>
            {
                Response response = PRMBotUtility.ValidateUserBotOTP(chatId, chatUserId, otp);

                Bot.SendTextMessageAsync(
                    chatId,
                    response.Message, ParseMode.Html,
                    replyMarkup: new ReplyKeyboardRemove());
            });

            await Bot.SendChatActionAsync(chatId, ChatAction.Typing);
        }

        public async Task GetRequirements(long chatId, long chatUserId)
        {
            await Bot.SendChatActionAsync(chatId, ChatAction.Typing);
            await Task.Run(() =>
            {
                string message = PRMBotUtility.GetUserRequirements(chatId, chatUserId);

                Bot.SendTextMessageAsync(
                    chatId,
                    message, ParseMode.Html,
                    replyMarkup: new ReplyKeyboardRemove());
            });
        }

        public async Task GetRequirementDetails(long chatId, long chatUserId, int reqId)
        {
            await Bot.SendChatActionAsync(chatId, ChatAction.Typing);
            await Task.Run(() =>
            {
                string message = PRMBotUtility.GetUserRequirements(chatId, chatUserId);

                Bot.SendTextMessageAsync(
                    chatId,
                    message, ParseMode.Html,
                    replyMarkup: new ReplyKeyboardRemove());
            });
        }

        private void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs chosenInlineResultEventArgs)
        {
            //Console.WriteLine($"Received inline result: {chosenInlineResultEventArgs.ChosenInlineResult.ResultId}");
        }

        private void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
        {
            //Console.WriteLine("Received error: {0} — {1}",
            //    receiveErrorEventArgs.ApiRequestException.ErrorCode,
            //    receiveErrorEventArgs.ApiRequestException.Message);
        }

        public void Dispose()
        {
            Bot.StopReceiving();
        }
    }
}