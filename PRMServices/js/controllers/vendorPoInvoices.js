﻿prmApp
    .controller('vendorPoInvoicesCtrl', function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter) {

        console.log('1234');

        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
        $scope.userID = userService.getUserId();
        $scope.compID = userService.getUserCompanyId();
        //$scope.customerCompanyId = userService.getCustomerCompanyId();

        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;
        $scope.invoiceDetails = [];
        $scope.searchKeyword = '';
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
            $scope.getInvoiceList(($scope.currentPage - 1), 10, $scope.searchKeyword);
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };


        $scope.filters = {
            searchKeyword: '',
            type: 'ALL',
            status: 'ALL',
            fromDate: moment().subtract(30, "days").format('YYYY-MM-DD'),
            toDate: moment().format('YYYY-MM-DD')
           
        }

        
        $scope.getInvoiceList = function () {
            $scope.params = {
                "COMP_ID": +$scope.compID,
                "U_ID": +$scope.userID,
                "fromDate": $scope.filters.fromDate,
                "toDate": $scope.filters.toDate,
                "PO_NUMBER": '',
                "INVOICE_NUMBER":''
                
            }

            PRMPOService.getInvoiceList($scope.params)
                .then(function (response) {
                    $scope.invoiceDetails = response;
                    $scope.invoiceDetails1 = $scope.invoiceDetails;
                    $scope.invoiceDetails.forEach(function (item) {
                        item.INVOICE_DATE = new moment(item.INVOICE_DATE).format("DD-MM-YYYY");
                    })
                    $scope.totalItems = $scope.invoiceDetails.length;

                });
        }
        $scope.getInvoiceList();
        $scope.invoiceDetails1 = [];

        $scope.getStatusFilter = function (type, str, status) {
            $scope.filterArray = [];
            $scope.invoiceDetails1 = $scope.invoiceDetails;
            if (status !== 'ALL' && status != '') {
                $scope.filterArray = $scope.invoiceDetails1.filter(function (item) {
                    return item.STATUS === status;
                });

                $scope.invoiceDetails1 = $scope.filterArray;
            }
            if (type !== 'ALL' && type != '') {
                $scope.filterArray = $scope.invoiceDetails1.filter(function (item) {
                    return item.INVOICE_TYPE === type;
                });
                $scope.invoiceDetails1 = $scope.filterArray;
            }
            if (str) {
                $scope.invoiceDetails1 = $scope.invoiceDetails1.filter(function (invoice) {
                    return (String(invoice.INVOICE_NUMBER).includes(str.toLowerCase())
                        || (invoice.PO_NUMBER ? invoice.PO_NUMBER.toLowerCase() : '').includes(str.toLowerCase())
                        || String(invoice.CUSTOMER_NAME.toLowerCase()).includes(str.toLowerCase())
                        || String(invoice.INVOICE_TYPE.toLowerCase()).includes(str.toLowerCase())
                        || String(invoice.INVOICE_DATE ? invoice.INVOICE_DATE : '').includes(str.toLowerCase())
                    );

                });
            };
            $scope.totalItems = $scope.invoiceDetails1.length;

        };

        $scope.createInvoice = function (invoice) {
            var url = $state.href("createInvoice", { "poNumber": invoice.PO_NUMBER, "invoiceNumber": invoice.INVOICE_NUMBER, "invoiceID": invoice.INVOICE_ID });
            $window.open(url, '_blank');
        }


    });