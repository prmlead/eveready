prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('auditCtrl', ["$state", "$stateParams", "$scope", "auctionsService", "userService",
        "$http", "$window", "domain", "fileReader", "growlService", "$log", "$filter", "ngDialog", "auditreportingServices",
        function ($state, $stateParams, $scope, auctionsService, userService,
            $http, $window, domain, fileReader, growlService, $log, $filter, ngDialog, auditreportingServices) {
            $scope.auditLogs = [];
            $scope.reqId = $stateParams.reqID;
            $scope.requirementDetails = {};
            $scope.auditData = [];
            $scope.auditDetails = [];
            $scope.filteredDetails = [];
            $scope.getAuditData = function () {
                if ($scope.reqId) {
                    auditreportingServices.getrequirementaudit($scope.reqId)
                        .then(function (response) {
                            console.log(response);
                            if (response.errorMessage) {
                                console.log(response.errorMessage);
                            } else {
                                //$scope.createAuditTable(response);
                                $scope.auditData = response[0].Value;
                                $scope.auditDetails = response[1].Value;
                            }

                        });
                }
            };

            $scope.getAuditData();

            //$scope.createAuditTable = function (audits) {
            //    audits.forEach(function (item, index) {
            //        item.dateCreated = userService.toLocalDate(item.dateCreated);
            //        if (item.actionType === "I") {
            //            item.actionType = "INSERT";
            //        } else if (item.actionType === "D") {
            //            item.actionType = "DELETE";
            //        } else if (item.actionType === "U") {
            //            item.actionType = "UPDATE";
            //        }
            //    });
            //    $scope.auditLogs = audits;
            //};

            $scope.getAuditDetails = function (audit) {
                $scope.auditData.forEach(function (audit1, vI) {
                    audit1.expanded = false;
                });

                audit.expanded = true;
                $scope.filteredDetails = _.filter($scope.auditDetails, function (detail) {
                    return detail.auditVersion === audit.auditVersion;
                });

                if ($scope.filteredDetails) {
                    $scope.filteredDetails.forEach(function (val, vI) {
                        val.dateCreated1 = userService.toLocalDate(val.dateCreated);
                    });
                } else {
                    $scope.filteredDetails = [];
                }
            };

            

            $scope.getRequirementDetails = function () {
                auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        if (response) {
                            $scope.requirementDetails = response;
                        }
                    });
            };
            
            $scope.getRequirementDetails();

        }]);